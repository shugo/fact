#include "stdio.h"

void Empty(){}
int Return1() { return 1; }
int Return0() { return 0; }
int Add1(int value) { return 1 + value; }
int Add(int A, int B) { return A + B; }
int Print(char* ptr) { printf("%s", ptr); }
int PrintFloat(float A) { printf("%f", A); }
int PrintFloatAndInt(float A, int B) { printf("%f %d", A, B); }
void While1() { while (1) { } }
void* CallMalloc(int Size) { malloc(Size); }
void CallFree(void* Ptr) { free(Ptr); }



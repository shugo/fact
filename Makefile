#Simple wrapper for xbuild
MSBUILD:=xbuild

SLN_FILE=src/Fact.sln

DOC_DIR=doc
DOC_DOXYFILE_USER=$(DOC_DIR)/Doxyfile.user
DOC_DOXYFILE_DEV=$(DOC_DIR)/Doxyfile.dev

all: build

build: deploy

deploy:
	${MSBUILD} $(SLN_FILE) /p:configuration=Deploy

debug:
	${MSBUILD} $(SLN_FILE) /p:configuration=Debug

release:
	${MSBUILD} $(SLN_FILE) /p:configuration=Release

$(DOC_TARGET): deploy

doc: $(DOC_DIR)/dev $(DOC_DIR)/user

$(DOC_DIR)/user:
	doxygen $(DOC_DOXYFILE_USER)
$(DOC_DIR)/dev:
	doxygen $(DOC_DOXYFILE_DEV)

clean:
	${MSBUILD} $(SLN_FILE) /target:clean /p:configuration=Deploy
	${MSBUILD} $(SLN_FILE) /target:clean /p:configuration=Release
	${MSBUILD} $(SLN_FILE) /target:clean /p:configuration=Debug

clean-doc:
	$(RM) -rf $(DOC_DIR)/dev $(DOC_DIR)/user

distclean: clean clean-doc

install: install_deploy

install_deploy: deploy
	./bin/Deploy/FactExe.exe System Install --output ./bin/Deploy/Fact.ff --create-package 'this'
	./bin/Deploy/FactExe.exe System Install --package ./bin/Deploy/Fact.ff

install_debug: debug
	./bin/Debug/FactExe.exe System Install --output ./bin/Deploy/Fact.ff --create-package 'this'
	./bin/Debug/FactExe.exe System Install --package ./bin/Deploy/Fact.ff

install_release: release
	./bin/Release/FactExe.exe System Install --output ./bin/Deploy/Fact.ff --create-package 'this'
	./bin/Release/FactExe.exe System Install --output ./bin/Deploy/Fact.ff --create-package 'this'

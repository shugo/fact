﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    public class Server
    {
        static System.Security.Cryptography.SHA512 SHA512 = System.Security.Cryptography.SHA512.Create();
        internal class User
        {
            internal string _HostAddress = "";
            public string HostAddress { get { return _HostAddress; } }
            internal byte[] _Token = new byte[0];
            internal ulong _IncToken = 0;
            internal Fact.Directory.User _User = null;
            internal string _UserId = "";
            internal byte[] _Password = new byte[0];
            internal DateTime _LastMessage = DateTime.UtcNow;
            static long _Id = 0;

            internal List<Directory.Credential> _TempCredential = new List<Directory.Credential>();

            static HashSet<string> _UsersId = new HashSet<string>();

            public void GenerateUserId() 
            {
                if (_UserId != "") { return; }
            retry:
                long value = 0;
                try
                {
                    unchecked
                    {
                        value = System.Threading.Interlocked.Increment(ref _Id);
                        if (value < 0) { value = 0; }
                    }
                }
                catch { }

                lock (_UsersId)
                {
                    _UserId = _User.Login + "_" + DateTime.UtcNow.Millisecond.ToString() + "_" + DateTime.UtcNow.Second.ToString() + "_" + value.ToString();
                    if (_UsersId.Contains(_UserId))
                    {
                        goto retry;
                    }
                    _UsersId.Add(_UserId);
                }
            }

            ~User()
            {
                lock (_UsersId)
                {
                    _UsersId.Remove(_UserId);
                }
            }
        }
        public class UserEventArgs : EventArgs
        {
            internal Server _Server = null; public Server Server { get { return _Server; } }
            internal string _UserId = ""; public string UserID { get { return _UserId; } }
        }
        public class ServiceNotLoadedEventArgs : EventArgs
        {
            internal Server _Server = null; public Server Server { get { return _Server; } }
            internal string _ServiceName = ""; public string ServiceName { get { return _ServiceName; } }
        }
        public event EventHandler<UserEventArgs> Connected;
        public event EventHandler<UserEventArgs> Disconnected;
        public event EventHandler<ServiceNotLoadedEventArgs> ServiceNotLoaded;


        System.Net.HttpListener _Listener = new System.Net.HttpListener();
        delegate void Action(object[] Input, Service.RequestInfo RequestInfo, out object Output);
        Dictionary<string, Action> _Actions = new Dictionary<string, Action>();
        Dictionary<string, System.Reflection.MethodInfo> _ActionsInfo = new Dictionary<string, System.Reflection.MethodInfo>();

        Dictionary<string, User> _Users = new Dictionary<string, User>();
        Directory.Group _AllowedUser = new Fact.Directory.Group();
        Directory.AuthenticationModule _AuthenticationModule = null;
        Dictionary<string, Fact.Directory.Group> _ActionAllowedUser = new Dictionary<string, Directory.Group>();

        public Dictionary <string, Fact.Directory.Group> ActionAllowedUser
        {
            get { return _ActionAllowedUser; }
        }

        public List<KeyValuePair<string, System.Reflection.MethodInfo>> ListActions()
        {
            List<KeyValuePair<string, System.Reflection.MethodInfo>> lst = new List<KeyValuePair<string, System.Reflection.MethodInfo>>();
            lock (_ActionsInfo)
            {
                foreach (KeyValuePair<string, System.Reflection.MethodInfo> pair in _ActionsInfo)
                {
                    lst.Add(new KeyValuePair<string, System.Reflection.MethodInfo>(pair.Key, pair.Value));
                }
            }
            return lst;
        }

        static Random rnd = new Random();
        int _InterfacePort = 0;
        int _SSLInterfacePort = 0;
        public Server(int InterfacePort, int SSLInterfacePort)
        {
            Directory.Directory Directory = new Fact.Directory.Directory();
            Directory.MainGroup = _AllowedUser;
            _AuthenticationModule = new Directory.AuthenticationModule(Directory);
            Connected += new EventHandler<UserEventArgs>(Default_UserEvent);
            Disconnected += new EventHandler<UserEventArgs>(Default_UserEvent);
            _InterfacePort = InterfacePort;
            _SSLInterfacePort = SSLInterfacePort;
            if (_SSLInterfacePort == _InterfacePort)
            {
                throw new Exception("HTTP and HTTPS connections can't be made on the same port");
            }
            Map("/",
                (System.Net.HttpListenerContext Context) => { return ParseRequest(Context); }
                );
        }

        void Default_UserEvent(object sender, Server.UserEventArgs e) { }

        public bool IsListening { get { if (_Listener == null) { return false; } return _Listener.IsListening; } }

        Dictionary<string, Service> _Services = new Dictionary<string, Service>();
        public void LoadService(Service Service)
        {
            if (Service == null) { return; }
            List<System.Reflection.MethodInfo> methods = Service.GetExposedMethodsInvoke();
            foreach (System.Reflection.MethodInfo method in methods)
            {
                if (method.GetParameters().Length == 0 || method.GetParameters()[0].ParameterType != typeof(Network.Service.RequestInfo))
                {
                    throw new Exception("The method " + method.Name + " is not a valid method.");
                }
                // Microsoft VS Scope bug WAR (fixed in the last VS version)
                System.Reflection.MethodInfo _WAR_method = method;
                string actionName = Service.Name + "." + method.Name;
                lock (_Actions)
                {
                    if (_Actions.ContainsKey(actionName))
                    {
                        _Actions.Remove(actionName);
                    }
                    _Actions.Add(actionName, (object[] parameters, Network.Service.RequestInfo RequestInfo, out object output) =>
                    {
                        List<object> plist = new List<object>();
                        plist.Add(RequestInfo);
                        plist.AddRange(parameters);
                        if (parameters == null) { parameters = new object[0]; }
                        try
                        {
                            output = _WAR_method.Invoke(Service, plist.ToArray());
                        }
                        catch { output = null; }
                    });
                }
                lock (_ActionsInfo)
                {
                    if (_ActionsInfo.ContainsKey(actionName))
                    {
                        _ActionsInfo.Remove(actionName);
                    }
                    _ActionsInfo.Add(actionName, _WAR_method);
                }
            }
            try
            {
                Service.OnServiceLoaded(this);
            }
            catch { }
            try
            {
                lock (_Services)
                {
                    _Services.Add(Service.Name, Service);
                }
            }
            catch { }
        }
        public object InvokeAction(Directory.User User, string Action, params object[] Parameters)
        {
            Service.RequestInfo requestInfo = new Service.RequestInfo();
            requestInfo._User = User;
            requestInfo._UserId = "";
            requestInfo._Server = this;
            return InvokeAction(requestInfo, Action, Parameters);
        }

        public object InvokeAction(Service.RequestInfo Info, string Action, params object[] Parameters)
        {
            try
            {
                if (Info.User != null)
                {
                    lock (_ActionAllowedUser)
                    {
                        if (_ActionAllowedUser.ContainsKey(Action) &&
                            !_ActionAllowedUser[Action].Contains(Info.User))
                        {
                            return null;
                        }
                    }
                }
            }
            catch { return null; }
            try
            {
                lock (_Actions)
                {
                    if (!_Actions.ContainsKey(Action)) { return null; }
                    object response = null;
                    _Actions[Action](Parameters, Info, out response);
                    return response;
                }
            }
            catch { return null; }
        }
        volatile bool _Started = true;
        volatile bool _Running = false;
        public Directory.Group AllowedUsers { get { return _AllowedUser; } set { _AllowedUser = value; } }
        public void Restart()
        {
            if (_Listener != null)
            {
                Stop();
            }
            lock (this)
            {
                while (_Running) { _Started = false; System.Threading.Monitor.Wait(this); }
                _Listener = new System.Net.HttpListener();
                if (_InterfacePort > 0) { _Listener.Prefixes.Add("http://*:" + _InterfacePort + "/"); }
                if (_SSLInterfacePort > 0) { _Listener.Prefixes.Add("https://*:" + _SSLInterfacePort + "/"); }
                _Listener.Start();
                _Started = true;
                new System.Threading.Thread(Run) { IsBackground = true }.Start();
                while (!_Running) { System.Threading.Monitor.Wait(this); }
            }
        }

        public void Start() 
        {
            // Yes start is just an alias for restart beause restart
            // will also start the server if it is not started
            Restart();
        }
        public void Stop() 
        {
            lock (this)
            {
                _Started = false;
                try
                {
                    _Listener.Stop();
                    _Listener.Close();
                }
                catch { }
                System.Threading.Monitor.PulseAll(this);
            }

            lock (this)
            {
                while (_Running)
                {
                    try
                    {
                        _Listener.Abort();
                    }
                    catch { }
                    _Started = false;
                    System.Threading.Monitor.Wait(this);
                }
                _Listener = null;
            }
        }

        void Run()
        {
            try
            {
                lock (this)
                {
                    _Running = true;
                    System.Threading.Monitor.PulseAll(this);
                }

                while (_Started)
                {
                    try
                    {
                        Dispatcher(_Listener.GetContext());
                    }
                    catch 
                    {
                        if (!_Listener.IsListening) { goto Exit; }
                    }
                }
                Exit:
                bool mustRestart = false;
                lock (this)
                {
                    _Running = false;

                    System.Threading.Monitor.PulseAll(this);
                    if (_Started)
                    {
                        mustRestart = true;
                    }
                    else
                    {
                        try
                        {
                            _Listener.Close();
                            _Listener.Stop();
                            _Listener.Close();
                        }
                        catch { }
                    }
                }
                Restart();
            }
            catch { }
        }

        DateTime _LastDeadUserCheck = DateTime.UtcNow;
        void RemoveDeadUsers()
        {
            List<string> deadUsers = null;
            lock (_Users)
            {
                DateTime now = DateTime.UtcNow;
                if ((now - _LastDeadUserCheck).TotalMinutes < 1) { return; }
                {
#if DEBUG
                    Fact.Log.Debug("Start checking for dead users");
#endif
                    deadUsers = new List<string>();
                    foreach (KeyValuePair<string, User> user in _Users)
                    {
                        if ((now - user.Value._LastMessage).TotalSeconds >= 30) { deadUsers.Add(user.Key); }
                    }
                    _LastDeadUserCheck = now;
                }
            }
            foreach (string dead in deadUsers)
            {
#if DEBUG
                Fact.Log.Debug("User " + dead + " is dead and will be removed from the registered users");
#endif
                try { Disconnected(this, new UserEventArgs() { _UserId = dead, _Server = this }); }
                catch { }
                lock (_Users) { _Users.Remove(dead); }
            }
        }

        public string GetHostAddressFromUserId(string UserId)
        {
            lock (_Users)
            {
                if (!_Users.ContainsKey(UserId)) { return ""; }
                return _Users[UserId]._HostAddress;
            }
            return "";
        }

        void DispatcherAgnostic(object Context)
        {
            Dispatcher(Context as System.Net.HttpListenerContext);
        }

        void Dispatcher(System.Net.HttpListenerContext Context)
        {
#if DEBUG
            Fact.Log.Debug("Incoming http request");
#endif
            if (Context == null) { return; }
            RemoveDeadUsers();
            try
            {
                Context.Response.KeepAlive = Context.Request.KeepAlive;
                Ressource ressource = _SolveMapping(Context.Request.Url.PathAndQuery);
                if (ressource != null) 
                {
                    byte[] array = ressource.Invoke(Context);
                    if (array != null)
                    {
                        if (Context.Response.ContentType == null ||
                            Context.Response.ContentType == "")
                        {
                            Context.Response.ContentType = "application/xml";
                        }
                        Context.Response.ContentLength64 = array.Length;
                        Threading.AsyncStream AsyncStream = new Threading.AsyncStream(Context.Response.OutputStream);
                        AsyncStream.Write(array, 0, array.Length,
                            () =>
                            {
                                try
                                {
                                    try { AsyncStream.Close(); } catch { }
                                    Context.Response.Close();
                                }
                                catch (Exception e)
                                {
#if DEBUG
                                    Fact.Log.Debug("Abort connection because of exception");
                                    Fact.Log.Exception(e);
#endif
                                    Context.Response.Abort();
                                }
                            });
                    }
                    else
                    {
                        try { Context.Response.OutputStream.Close(); } catch { }
                        Context.Response.Close();
                    }
                }
            }
            catch (Exception e)
            {
                try
                {
                    string message = e.Message;
                    message = message.Replace("<", "(");
                    message = message.Replace(">", ")");
#if DEBUG
                    Fact.Log.Error("Error while processing a request");
                    Fact.Log.Exception(e);
#endif
                    byte[] responsebyte = System.Text.Encoding.UTF8.GetBytes("<Error><Message>Error while processing the request: " + message + "</Message></Error>");
                    Context.Response.ContentType = "application/xml";
                    Context.Response.ContentLength64 = responsebyte.Length;
                    try {  Context.Response.OutputStream.Write(responsebyte, 0, responsebyte.Length); } catch { }
                    try { Context.Response.OutputStream.Close(); } catch { }

                    try { Context.Response.KeepAlive = false; } catch { }
                    Context.Response.Close();
                }
                catch (Exception e2)
                {
#if DEBUG
                    Fact.Log.Debug("Abort connection because of exception");
                    Fact.Log.Exception(e2);
#endif
                    Context.Response.Abort();
                }
            }
        }

        public delegate byte[] Ressource(System.Net.HttpListenerContext Context);
        Dictionary<string, Ressource> _Ressources = new Dictionary<string, Ressource>();

        public void Map(string Path, Ressource Ressource, Fact.Directory.Group AllowedUsers)
        {
            lock (_Ressources)
            {
                if (_Ressources.ContainsKey(Path))
                {
                    _Ressources[Path] = Ressource;
                }
                else
                {
                    _Ressources.Add(Path, Ressource);
                }
            }
        }

        public void MapLocalDirectory(string Path, string LocalDirectory)
        {
            Network.Protocols.WebDAV webDAV = new Protocols.WebDAV(this, Path);
            webDAV.Map("/", LocalDirectory);
        }

        public void Map(string Path, Ressource Ressource)
        {
            Map(Path, Ressource, null);
        }

        public void Unmap(string Path)
        {
            lock (_Ressources)
            {
                if (_Ressources.ContainsKey(Path)) { _Ressources.Remove(Path); }
            }
        }

        Ressource _SolveMapping(string Path)
        {
            lock (_Ressources)
            {
                try
                {
                    if (_Ressources.ContainsKey(Path)) { return _Ressources[Path]; }
                    while (Path != "" &&
                           Path != "/" &&
                           Path != "\\")
                    {
                        Path = System.IO.Path.GetDirectoryName(Path).Replace("\\", "/");
                        if (_Ressources.ContainsKey(Path)) { return _Ressources[Path]; }
                    }
                }
                catch { return null; }
            }
            return null;
        }
        bool CheckToken(User User, byte[] Content, byte[] Action, DateTime TimeCode, byte[] Token)
        {
            List<byte> expectedTokenBuilder = new List<byte>(User._Token);
            expectedTokenBuilder.AddRange(User._Password);
            expectedTokenBuilder.AddRange(BitConverter.GetBytes((Int64)User._IncToken));
            expectedTokenBuilder.AddRange(Fact.Internal.Information.TimeCode(TimeCode));
            expectedTokenBuilder.AddRange(Content);
            expectedTokenBuilder.AddRange(Action);
            byte[] expectedToken = null;
            lock (SHA512) { expectedToken = SHA512.ComputeHash(expectedTokenBuilder.ToArray()); }
            if (expectedToken.Length != Token.Length)
                return false;
            for (int n = 0; n < expectedToken.Length; n++)
            { if (expectedToken[n] != Token[n]) { return false; } }
            return true;
        }

        byte[] ParseFactRequest(System.Net.HttpListenerContext Context)
        {
            string response = "<Error/>";
            byte[] fullmessage = new byte[Context.Request.ContentLength64];
            Threading.AsyncStream asyncstream = new Threading.AsyncStream(Context.Request.InputStream);
            object message = Internal.StreamTools.Deserialize(asyncstream);
            if (!(message is Dictionary<string, object>))
            {
                response = "<Error>Broken stream</Error>";
                goto sendresponse;
            }

            Dictionary<string, object> Variables = new Dictionary<string, object>();
            if (Variables.ContainsKey("action"))
            {
                string Action = Variables["action"] as string;
                if (Action.ToLower() == "@@authenticate")
                {
                    if (Variables.ContainsKey("username") && Variables.ContainsKey("password"))
                    {
                        if (Variables.ContainsKey("credential"))
                        {
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        response = "<Error><message>Invalid parameters</message></Error>";
                        goto sendresponse;
                    }
                }
                else if (Action.ToLower() == "@@ping")
                {
                    response = "<Pong/>";
                    goto sendresponse;
                }
                else if (Action.ToLower() == "@@version")
                {
                    response = "<Version>" + Fact.Internal.Information.FactVersionName() + "</Version>";
                    goto sendresponse;
                }
                else
                {

                }
            }

            sendresponse:
            byte[] responsebyte = System.Text.Encoding.UTF8.GetBytes(response);
            return responsebyte;
        }

        void Reset()
        {
            try
            {
                _Listener.Abort();
            } catch { }
            lock (_Users)
            {

            }
        }

        byte[] ParseRequest(System.Net.HttpListenerContext Context)
        {
            string response = "<Error/>";

            if (Context.Request.HttpMethod.ToLower() == "post")
            {
                response = "<Error/>";
                byte[] fullmessage = null;
                Dictionary<string, object> Variables = new Dictionary<string, object>();

                if (Context.Request.ContentType == "application/octet-stream")
                {
#if DEBUG
                    Fact.Log.Debug("Incoming raw byte stream");
#endif
                    try
                    {
                        Threading.AsyncStream asyncstream = new Threading.AsyncStream(Context.Request.InputStream, Context.Request.ContentLength64);
                        Variables = (Dictionary<string, object>)Internal.StreamTools.Deserialize(asyncstream);
                    }
                    catch (Exception e)
                    {
                        string message = e.Message;
                        message = message.Replace("<", "(");
                        message = message.Replace(">", ")");
                        response = "<Error>Unexpected error while reading the input stream: " + message + "</Error>";
                        goto sendresponse;
                    }
                }
                else
                {
#if DEBUG
                    Fact.Log.Debug("Incoming post stream");
#endif
                    try
                    {
                        fullmessage = new byte[Context.Request.ContentLength64];
                        Threading.AsyncStream asyncstream = new Threading.AsyncStream(Context.Request.InputStream);
                        int length = asyncstream.Read(fullmessage, 0, fullmessage.Length);
                        if (length != fullmessage.Length) { response = "<Error>Broken stream</Error>"; goto sendresponse; }

                    }
                    catch (Exception e)
                    {
                        string message = e.Message;
                        message = message.Replace("<", "(");
                        message = message.Replace(">", ")");
                        response = "<Error>Unexpected error while reading the input stream: " + message + "</Error>";
                        goto sendresponse;
                    }

                    string text = System.Text.Encoding.UTF8.GetString(fullmessage);

                {
                    int n = 0;
                    while (n < text.Length)
                    {
                        StringBuilder key = new StringBuilder();
                        StringBuilder value = new StringBuilder();
                        for (; n < text.Length; n++)
                        {
                            if (text[n] == '=') { n++; break; } else { key.Append(text[n]); }
                        }
                        for (; n < text.Length; n++)
                        {
                            if (text[n] == '&') { n++; break; } else { value.Append(text[n]); }
                        }
                        try
                        {
                            // FIXME Recode the UnescapeFunction
                            string ukey = "";
                            try { ukey = System.Uri.UnescapeDataString(key.ToString()); }
                            catch { ukey = key.ToString(); }
                            string udata = "";
                            try { udata = System.Uri.UnescapeDataString(value.ToString()); }
                            catch { udata = value.ToString(); }
                            Variables.Add(ukey, udata);
                        }
                        catch { }
                    }
                }
                }

                
                if (Variables.ContainsKey("action") && Variables["action"] is String)
                {
                    string Action = Variables["action"] as String;

#if DEBUG
                    Fact.Log.Debug("Incoming action " + Action);
#endif
                    if (Action == "@@authenticate")
                    {
                        if (Variables.ContainsKey("username") &&
                            Variables.ContainsKey("password") &&
                            Variables["username"] is String)
                        {
                            string Username = Variables["username"] as String;
                            byte[] Password;
                            if (Variables["password"] is String) { Password = Tools.FromBase64URL(Variables["password"] as String); }
                            else if (Variables["password"] is byte[]) { Password = Variables["password"] as byte[]; }
                            else
                            {
                                response = "<Error><message>Invalid password data</message></Error>";
                                goto sendresponse;
                            }
                            Fact.Directory.User authenticatedUser = null;
#if DEBUG
                            Fact.Log.Debug("Trying authenticated user " + Username);
#endif
                            if (Variables.ContainsKey("credential"))
                            {
#if DEBUG
                                Fact.Log.Debug("Authentication method for user " + Username + " is: Credential based");
#endif
                                byte[] CredentialId;
                                if (Variables["credential"] is String) { CredentialId = Tools.FromBase64URL(Variables["credential"] as String); }
                                else if (Variables["credential"] is byte[]) { CredentialId = Variables["credential"] as byte[]; }
                                else
                                {
                                    response = "<Error><message>Invalid credential data</message></Error>";
                                    goto sendresponse;
                                }
                                Directory.Credential credential = new Fact.Directory.Credential();
                                if (_AllowedUser.AuthenticateWithCredential(Username, CredentialId, Password, Fact.Directory.User.AuthenticationMethod.Fact_TimeSafe, out authenticatedUser, out credential))
                                {
                                    string UserId = Username;
                                    User User = new User();
                                    lock (_Users)
                                    {
                                        User._IncToken = 0;
                                        User._Token = new byte[64];
                                        User._User = authenticatedUser;
                                        User._Password = credential._Password;
                                        try { User._HostAddress = Context.Request.RemoteEndPoint.Address.ToString(); }
                                        catch { }
                                        rnd.NextBytes(User._Token);

                                        User.GenerateUserId();
                                        UserId = User._UserId;
                                        _Users.Add(UserId, User);
                                    }
                                    if (authenticatedUser.Password_SHA512_Salt == null)
                                    {
                                        response = "<Error><message>The user object is not compatible to Fact time safe</message></Error>";
                                        goto sendresponse;
                                    }
                                    else
                                    {
                                        try { if (Connected != null) { Connected(this, new UserEventArgs() { _UserId = UserId, _Server = this }); } }
                                        catch { }
                                        response = "<Response><Token>" + Tools.ToBase64URL(_Users[UserId]._Token) + "</Token><UserId>" + UserId + "</UserId></Response>";
                                    }
                                }
                                else
                                {
                                    response = "<Error><message>Authentication failure</message></Error>";
                                    goto sendresponse;
                                }
                            }
                            else
                            {
#if DEBUG
                            Fact.Log.Debug("Authentication method for user " + Username + " is: Token based");
#endif
                                if (_AllowedUser.Authenticate(Username, Password, Fact.Directory.User.AuthenticationMethod.Fact_TimeSafe, out authenticatedUser))
                                {
                                    string UserId = Username;
                                    User User = new User();
                                    lock (_Users)
                                    {
                                        User._IncToken = 0;
                                        User._Token = new byte[64];
                                        User._User = authenticatedUser;
                                        User._Password = authenticatedUser.Password_SHA512_Salt;
                                        try { User._HostAddress = Context.Request.RemoteEndPoint.Address.ToString(); }
                                        catch { }
                                        rnd.NextBytes(User._Token);

                                        User.GenerateUserId();
                                        UserId = User._UserId;
                                        _Users.Add(UserId, User);
                                    }
                                    if (authenticatedUser.Password_SHA512_Salt == null)
                                    {
                                        response = "<Error><message>The user object is not compatible to Fact time safe</message></Error>";
                                    }
                                    else
                                    {
                                        try { if (Connected != null) { Connected(this, new UserEventArgs() { _UserId = UserId, _Server = this }); } }
                                        catch { }
                                        response = "<Response><Token>" + Tools.ToBase64URL(_Users[UserId]._Token) + "</Token><UserId>" + UserId + "</UserId></Response>";
                                    }

                                }
                                else
                                {
#if DEBUG
                                    Fact.Log.Debug("Authentication for user " + Username + " failed: Invalid Token");
#endif
                                    response = "<Error><message>Authentication failure</message></Error>";
                                    goto sendresponse;
                                }
                            }
                        }
                        else
                        {
                            response = "<Error><message>Invalid parameters</message></Error>";
                            goto sendresponse;
                        }
                    }
                    else
                    {
                       
                        if (Variables.ContainsKey("userid") &&
                            Variables.ContainsKey("token") &&
                            Variables["userid"] is String)
                        {
                            string userid = Variables["userid"] as string;
                            byte[] content = new byte[0];
                            if (Variables.ContainsKey("content") &&
                                Variables["content"] is String &&
                                (Variables["content"] as String).Length > 0)
                            {
                                try
                                {
                                    content = Tools.FromBase64URL(Variables["content"] as String);
                                }
                                catch (Exception e)
                                {
#if DEBUG
                                    Fact.Log.Debug("Invalid parameters data sent by " + Variables["userid"]);
                                    Fact.Log.Exception(e);
#endif
                                    response = "<Error><message>Invalid data encoding in the parameters sent to the method</message></Error>";
                                    goto sendresponse;
                                }
                            }
                            else if (Variables.ContainsKey("content") &&
                                     Variables["content"] is byte[] &&
                                     (Variables["content"] as byte[]).Length > 0)
                            {
                                try
                                {
                                    content = (Variables["content"] as byte[]);
                                }
                                catch (Exception e)
                                {
#if DEBUG
                                    Fact.Log.Debug("Invalid parameters data sent by " + Variables["userid"]);
                                    Fact.Log.Exception(e);
#endif
                                    response = "<Error><message>Invalid data encoding in the parameters sent to the method</message></Error>";
                                    goto sendresponse;
                                }
                            }
                            byte[] token;
                            if (Variables["token"] is String) { token = Tools.FromBase64URL(Variables["token"] as String); }
                            else if (Variables["token"] is byte[]) { token = Variables["token"] as byte[]; }
                            else
                            {
                                response = "<Error><message>Invalid token data</message></Error>";
                                goto sendresponse;
                            }
                            User user = null;
                            lock (_Users)
                            {
                                if (!_Users.ContainsKey(userid))
                                {
                                    response = "<Error><message>Invalid auth data</message></Error>";
                                    goto sendresponse;
                                }
                                else
                                {
                                    user = _Users[userid];
                                }
                            }

                            byte[] actionAsArray = System.Text.Encoding.UTF8.GetBytes(Action);

                            if (CheckToken(user, content, actionAsArray, DateTime.UtcNow, token)) { goto end; }
                            if (CheckToken(user, content, actionAsArray, DateTime.UtcNow + new TimeSpan(0, 1, 0), token)) { goto end; }
                            if (CheckToken(user, content, actionAsArray, DateTime.UtcNow - new TimeSpan(0, 1, 0), token)) { goto end; }
                            if (CheckToken(user, content, actionAsArray, DateTime.UtcNow + new TimeSpan(0, 2, 0), token)) { goto end; }
                            if (CheckToken(user, content, actionAsArray, DateTime.UtcNow - new TimeSpan(0, 2, 0), token)) { goto end; }
                            lock (_Users) { _Users.Remove(userid); }
                            try { Disconnected(this, new UserEventArgs() { _UserId = userid }); }
                            catch { }
#if DEBUG
                            Fact.Log.Debug("Message validation failed for user " + Variables["userid"] + " failed: Invalid Token");
#endif
                            response = "<Error><message>Invalid token</message></Error>";
                            goto sendresponse;
                        end:
                            user._IncToken++;
                            user._LastMessage = DateTime.UtcNow;
                            List<object> parameters = new List<object>();
                            if (content.Length > 0)
                            {
                                System.IO.MemoryStream stream = new System.IO.MemoryStream(content);
                                int Count = Internal.StreamTools.ReadInt32(stream);
                                for (int n = 0; n < Count; n++)
                                {
                                    parameters.Add(Internal.StreamTools.Deserialize(stream));
                                }
                            }

                            if (Action.ToLower() == "@@postauthenticate") 
                            {
                                response = "<Authenticated/>"; 
                            }
                            else if (Action.ToLower() == "@@ping")
                            {
                                response = "<Pong/>";
                            }
                            else if (Action.ToLower() == "@@createtempcred")
                            {
                                Fact.Directory.Credential credential = new Directory.Credential(user._User, DateTime.MaxValue);
                                user._TempCredential.Add(credential);
                            }
                            else if (Action.ToLower() == "@@ping")
                            {
                                response = "<Pong/>";
                            }
                            else
                            {
                            replayAllowedAction:
                                bool containsAction = false;
                                lock (_Actions)
                                {
                                    containsAction = _Actions.ContainsKey(Action);
                                }
                                if (containsAction)
                                {
                                    bool allowed = true;

                                    lock (_ActionAllowedUser)
                                    {
                                        if (_ActionAllowedUser.ContainsKey(Action) &&
                                            !_ActionAllowedUser[Action].Contains(user._User))
                                        {
                                            allowed = false;
                                        }
                                    }
                                    if (allowed)
                                    {
#if DEBUG
                                        Fact.Log.Debug("Action " + Action + " has been requested by " + user._User.Login);
#endif
                                        object obj_response = null;
                                        Service.RequestInfo Request = new Service.RequestInfo();
                                        Request._User = user._User;
                                        Request._Server = this;
                                        Request._UserId = user._UserId;
                                        Request._UserHostAddress = user._HostAddress;
                                        lock (_Actions)
                                        {
                                            try
                                            {
                                                _Actions[Action](parameters.ToArray(), Request, out obj_response);
                                            }
                                            catch (Exception e)
                                            {
#if DEBUG
                                                Fact.Log.Debug("Exception occurs while processing " + Action + " for user " + user._User.Login);
                                                Fact.Log.Exception(e);
#endif
                                                response = "<Error><message>Exception in method " + Action + ": " + e.Message + " </message></Error>"; goto sendresponse;
                                            }
                                        }
                                        System.IO.MemoryStream stream = new System.IO.MemoryStream();
                                        Internal.StreamTools.Serialize(stream, obj_response);
                                        //response = "<Content>" + Tools.ToBase64URL(stream.ToArray()) + "</Content>";
                                        // Send a raw response to  avoid using Base64
                                        Context.Response.ContentType = "application/octet-stream";
                                        byte[] array = stream.ToArray();
                                        return array;
                                    }
                                    else
                                    {
#if DEBUG
                                        Fact.Log.Debug("Action " + Action + " not allowed for user " + user._User.Login);
#endif
                                        response = "<Error><message>Action not allowed</message></Error>"; goto sendresponse;
                                    }
                                }
                                else
                                {
                                    string Service = Action;
                                    try
                                    {
                                        string[] parts = Action.Split('.');
                                        if (parts.Length > 0) { Service = parts[0]; }
                                        bool serviceLoaded = false;
                                        lock (_Services)
                                        {
                                            serviceLoaded = _Services.ContainsKey(Service);
                                        }
                                        if (serviceLoaded)
                                        {
#if DEBUG
                                            Fact.Log.Debug("Invalid action " + Action + " from user " + user._User);
#endif
                                            response = "<Error><message>Action not present in loaded service</message></Error>"; goto sendresponse;
                                        }
                                        else
                                        {
#if DEBUG
                                            Fact.Log.Debug("Service " + Service + " not loaded");
#endif
                                            if (ServiceNotLoaded != null)
                                            {
                                                ServiceNotLoadedEventArgs events = new ServiceNotLoadedEventArgs();
                                                events._Server = this;
                                                events._ServiceName = Service;
                                                ServiceNotLoaded(this, events);
                                                lock (_Services)
                                                {
                                                    serviceLoaded = _Services.ContainsKey(Service);
                                                }
                                                if (serviceLoaded)
                                                {
#if DEBUG
                                                    Fact.Log.Debug("Service has been dynamically loaded, replaying action ...");
#endif
                                                    goto replayAllowedAction;
                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {

                                    }
#if DEBUG
                                    Fact.Log.Debug("Invalid action " + Action + " from user " + user._User);
#endif
                                    response = "<Error><message>Invalid action</message></Error>"; goto sendresponse;
                                }
                            }
                        }
                        else if (Action.ToLower() == "@@ping")
                        {
                            response = "<Pong/>";
                        }
                        else
                        {
#if DEBUG
                            Fact.Log.Debug("Missing authentication data");
#endif
                            response = "<Error><message>Missing auth data</message></Error>"; goto sendresponse;
                        }
                    }
                }
                else
                {
#if DEBUG
                    Fact.Log.Debug("No action specified");
#endif
                    response = "<Error><message>No action specified</message></Error>"; goto sendresponse;
                }
            }
            else
            {
                response = "<Error><message>A request can only be a post request</message></Error>"; goto sendresponse;
            }
            sendresponse:
            byte[] responsebyte = System.Text.Encoding.UTF8.GetBytes(response);
            return responsebyte;
        }
    }
}

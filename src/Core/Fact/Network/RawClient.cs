﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    public class RawClient
    {
        string _ServerUrl = "";
        string _UserName = "";
        string _UserId = "";
        byte[] _Password = new byte[0];
        byte[] _CredentialId = null;
        static System.Security.Cryptography.SHA512 SHA512 = System.Security.Cryptography.SHA512.Create();

        bool _Authenticated = false;

        public bool Authenticated { get { return _Authenticated; } }

        public bool Started { get { lock (this) { return backgroundJob != null; } } }

        public RawClient(string Server, string UserName, string Password)
        {
            if (Server.Trim() == "") { throw new Exception("Invalid server name"); }
            if (UserName.Trim() == "") { throw new Exception("Invalid username"); }
            _ServerUrl = Server;
            _UserName = UserName;
            lock (SHA512)
            {
                _Password = SHA512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Password + "__fact__" + UserName));
            }
        }

        public RawClient(string Server, Directory.Credential Credential)
        {
            if (Server.Trim() == "") { throw new Exception("Invalid server name"); }
            _ServerUrl = Server;
            _UserName = Credential._Username;
            _Password = Credential._Password;
            _CredentialId = Credential._CredId;
        }

        Threading.Job.Status backgroundJob = null;

        public void Restart()
        {
            lock (this)
            {
                _Authenticated = false;
                _PostAuthenticate = false;
                tokenCount = 0;
                if (backgroundJob != null) { backgroundJob.Expired = true; }
                backgroundJob = Threading.Job.CreateTimer((ref Threading.Job.Status Status) => { Run(false); }, 5000, true);
                Threading.Job.CreateJob(() => { Run(false); });
            }
        }

        public void Start()
        {
            // Yes start is just an alias for restart beause restart
            // will also start the server if it is not started
            Restart();
        }

        public void Stop()
        {
            lock (this)
            {
                if (backgroundJob != null)
                {
                    backgroundJob.Expired = true;
                    backgroundJob = null;
                }
            }
        }

        public class Action
        {
            internal string ActionName = "";
            internal byte[] Content = new byte[0];
            internal Callback Callback = (object Response) => { };
            internal int Timeout = 5000;
            internal bool _Completed = false;
            public void WaitForCompletion()
            {
                lock (this) { while (!_Completed) { System.Threading.Monitor.Wait(this); } }
            }
        }

        Queue<Action> _ActionQueue = new Queue<Action>();
        public delegate void Callback(object Response);
        public Action Do(string Action, Callback Callback, bool RecycleThread, params object[] Parameters)
        {
            return DoWithTimeout(Action, 5000, Callback, RecycleThread, Parameters);
        }
        public Action DoWithTimeout(string Action, int Timeout, Callback Callback, bool RecycleThread, params object[] Parameters)
        {
            Action ActionObj = null;
            if (Parameters.Length == 0 || Parameters == null)
            {
                lock (_ActionQueue)
                {
                    ActionObj = new Action() { ActionName = Action, Content = new byte[0], Callback = Callback, Timeout = Timeout };
                    _ActionQueue.Enqueue(ActionObj);
                }
            }
            else
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                Internal.StreamTools.WriteInt32(stream, Parameters.Length);
                for (int n = 0; n < Parameters.Length; n++)
                {
                    Internal.StreamTools.Serialize(stream, Parameters[n]);
                }
                lock (_ActionQueue)
                {
                    ActionObj = new Action() { ActionName = Action, Content = stream.ToArray(), Callback = Callback, Timeout = Timeout };
                    _ActionQueue.Enqueue(ActionObj);
                }
            }
            if (RecycleThread) { Run(true); }
            else { Threading.Job.CreateJob(() => { Run(false); }); }
            return ActionObj;
        }

        public void Reset()
        {
            _Authenticated = false;
            _PostAuthenticate = false;
            tokenCount = 0;
        }

        DateTime lastPing = DateTime.UtcNow;
        bool _PostAuthenticate = false;
        ulong tokenCount = 0;
        byte[] token = null;
        string userId = "";

        void Run(bool RecycleThread)
        {
            lock (this)
            {
                Action action = null;
                bool reauthentitace = false;
                DateTime now = DateTime.UtcNow;
                try
                {
                    if (!_Authenticated && !_PostAuthenticate)
                    {
                        tokenCount = 0;
                        List<byte> password_timecode = new List<byte>(_Password);
                        password_timecode.AddRange(Internal.Information.TimeCode(DateTime.UtcNow));
                        string base64Password = "";
                        lock (SHA512) { base64Password = Tools.ToBase64URL(SHA512.ComputeHash(password_timecode.ToArray())); }

                        Dictionary<string, string> Variables = new Dictionary<string, string>();
                        Variables.Add("action", "@@authenticate");
                        Variables.Add("username", _UserName);
                        Variables.Add("password", base64Password);
                        if (_CredentialId != null)
                            Variables.Add("credential", Tools.ToBase64URL(_CredentialId));
                        if (reauthentitace)
                        {
                        }
                        reauthentitace = false;
                        string response = Tools.HttpPostRequest(_ServerUrl, Variables, 10000);
                        Fact.Parser.XML parser = new Fact.Parser.XML();
                        parser.Parse(response);
                        Fact.Parser.XML.Node TokenNode = parser.Root.FindNode("token", false);
                        Fact.Parser.XML.Node UserIdNode = parser.Root.FindNode("userid", false);
                        if (TokenNode != null && TokenNode.Children.Count > 0 &&
                            UserIdNode != null && UserIdNode.Children.Count > 0)
                        {
                            _PostAuthenticate = true;
                            token = Tools.FromBase64URL(TokenNode.Children[0].Text);
                            userId = UserIdNode.Children[0].Text.Trim();
                            if (RecycleThread) { Run(true); }
                            else { Threading.Job.CreateJob(() => { Run(false); }); }
                        }
                        else
                        {
                            userId = "";
                            _Authenticated = false;
                            _PostAuthenticate = false;
#if DEBUG
                            Fact.Log.Debug("Authentication failure for user: " + userId);
#endif
                        }
                    }
                    else
                    {

                        List<byte> currenttokenBuilder = new List<byte>(token);
                        currenttokenBuilder.AddRange(_Password);
                        currenttokenBuilder.AddRange(BitConverter.GetBytes((Int64)tokenCount));
                        currenttokenBuilder.AddRange(Internal.Information.TimeCode(DateTime.UtcNow));
                        Dictionary<string, object> Variables = new Dictionary<string, object>();
                        Variables.Add("userid", userId);
                        if (_PostAuthenticate)
                        {
                            currenttokenBuilder.AddRange(System.Text.Encoding.UTF8.GetBytes("@@postauthenticate"));
                            byte[] currenttoken = null;
                            lock (SHA512) { currenttoken = SHA512.ComputeHash(currenttokenBuilder.ToArray()); }
                            Variables.Add("token", currenttoken);
                            Variables.Add("action", "@@postauthenticate");
                            string response = Tools.HttpPostRequest(_ServerUrl, Internal.StreamTools.Serialize(Variables), 10000);
                            tokenCount++;
                            if (response != "")
                            {
                                bool valid = false;
                                Fact.Parser.XML parser = new Fact.Parser.XML();
                                parser.Parse(response);
                                Fact.Parser.XML.Node Node = parser.Root.FindNode("authenticated", false);
                                if (Node != null)
                                {
                                    _Authenticated = true; _PostAuthenticate = false;
                                    if (RecycleThread) { Run(true); }
                                    else { Threading.Job.CreateJob(() => { Run(false); }); }
                                }
                                else
                                {
                                    _Authenticated = false; _PostAuthenticate = false;
#if DEBUG
                                    Fact.Log.Debug("Authentication failure for user: " + userId);
#endif
                                }
                            }
                        }
                        else
                        {
                            bool mustPing = (DateTime.UtcNow - lastPing).TotalSeconds > 10;
                            lock (_ActionQueue)
                            {
                                if (_ActionQueue.Count != 0)
                                {
                                    action = _ActionQueue.Dequeue();
                                }
                            }
                            if (action != null) { mustPing = false; }
                            if (action == null && !mustPing)
                            {
                                return;
                            }
                            else if (action == null && mustPing)
                            {
                                currenttokenBuilder.AddRange(new byte[0]);
                                currenttokenBuilder.AddRange(System.Text.Encoding.UTF8.GetBytes("@@ping"));
                                byte[] currenttoken = null;
                                lock (SHA512) { currenttoken = SHA512.ComputeHash(currenttokenBuilder.ToArray()); }
                                Variables.Add("token", currenttoken);
                                Variables.Add("action", "@@ping");
                                string responsetype = "";
                                byte[] rawresponse = Tools.HttpPostRequestRawResult(_ServerUrl, Internal.StreamTools.Serialize(Variables), 5000, out responsetype);
                                tokenCount++;
                                Fact.Parser.XML.Node Node = null;
                                if (responsetype == "application/xml" && rawresponse != null && rawresponse.Length > 0)
                                {
                                    Fact.Parser.XML parser = new Fact.Parser.XML();
                                    string response = System.Text.Encoding.UTF8.GetString(rawresponse);
                                    parser.Parse(response);
                                    Node = parser.Root.FindNode("pong", false);
                                }
                                if (Node == null)
                                {
                                    _PostAuthenticate = false; _Authenticated = false;
                                }
                                lastPing = DateTime.UtcNow;
                            }
                            else if (action != null)
                            {
                                currenttokenBuilder.AddRange(action.Content);
                                currenttokenBuilder.AddRange(System.Text.Encoding.UTF8.GetBytes(action.ActionName));
                                byte[] currenttoken = null;
                                lock (SHA512) { currenttoken = SHA512.ComputeHash(currenttokenBuilder.ToArray()); }
                                Variables.Add("token", currenttoken);
                                Variables.Add("action", action.ActionName);

                                if (action.Content.Length > 0)
                                    Variables.Add("content", action.Content);
                                string responsetype = "";

                                byte[] rawresponse = Tools.HttpPostRequestRawResult(_ServerUrl, Internal.StreamTools.Serialize(Variables), action.Timeout, out responsetype);

                                tokenCount++;
                                if (rawresponse == null)
                                {
                                    _PostAuthenticate = false; _Authenticated = false;
                                }
                                else
                                {
                                    if (responsetype == "application/xml")
                                    {
                                        string response = System.Text.Encoding.UTF8.GetString(rawresponse);
                                        if (response != "")
                                        {
                                            lastPing = DateTime.UtcNow;
                                            Fact.Parser.XML parser = new Fact.Parser.XML();
                                            parser.Parse(response);
                                            Fact.Parser.XML.Node Node = parser.Root.FindNode("content", false);
                                            if (Node != null && Node.Children.Count > 0)
                                            {
                                                byte[] responseArray = Tools.FromBase64URL(Node.Children[0].Text);
                                                System.IO.MemoryStream responseStream = new System.IO.MemoryStream(responseArray);
                                                action.Callback(Internal.StreamTools.Deserialize(responseStream));

                                            }
                                            else
                                            {
                                                string error = "Unknown server error";
                                                try
                                                {
                                                    Fact.Parser.XML.Node ErrorNode = parser.Root.FindNode("error", false);
                                                    if (ErrorNode != null)
                                                    {
                                                        Fact.Parser.XML.Node MessageNode = parser.Root.FindNode("message", false);
                                                        if (MessageNode != null)
                                                        {
                                                            error = "";
                                                            foreach (Fact.Parser.XML.Node node in MessageNode.Children)
                                                            {
                                                                error += node.Text + " ";
                                                            }
                                                        }
                                                    }
                                                }
                                                catch { }
                                                _PostAuthenticate = false; _Authenticated = false;
                                                try
                                                {
                                                    action.Callback(new Exception(error.Trim()));
                                                }
                                                catch { }
                                            }
                                        }
                                        else
                                        {
                                            _PostAuthenticate = false; _Authenticated = false;
                                        }
                                    }
                                    else
                                    {
                                        lastPing = DateTime.UtcNow;
                                        System.IO.MemoryStream responseStream = new System.IO.MemoryStream(rawresponse);
                                        action.Callback(Internal.StreamTools.Deserialize(responseStream));
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
#if DEBUG
                    Fact.Log.Exception(e);
#endif
                    _Authenticated = false;
                    _PostAuthenticate = false;
                    tokenCount = 0;
                }
                if (action != null)
                {
                    lock (action) { action._Completed = true; System.Threading.Monitor.PulseAll(action); }
                }
            }
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    public class Client
    {
        string _server = "";
        public string Server { get { return _server; } }
        RawClient _Client = null;
        public Client(string Server, string UserName, string Password)
        {
            if (!Server.ToLower().StartsWith("http://") &&
                !Server.ToLower().StartsWith("https://"))
            {
                Server = "http://" + Server;
            }
            _server = Server;
            _Client = new RawClient(Server, UserName, Password);
        }

        public Client(string Server, Fact.Directory.Credential Credential)
        {
            if (!Server.ToLower().StartsWith("http://") &&
                !Server.ToLower().StartsWith("https://"))
            {
                Server = "http://" + Server;
            }
            _server = Server;
            _Client = new RawClient(Server, Credential);
        }

        public static bool Ping(string Server, int Timeout)
        {
            if (!Server.ToLower().StartsWith("http://") &&
                !Server.ToLower().StartsWith("https://"))
            {
                Server = "http://" + Server;
            }
            try
            {
                Dictionary<string, string> variables = new Dictionary<string, string>();
                variables.Add("action", "@@ping");
                string response = Tools.HttpPostRequest(Server, variables, Timeout);
                Parser.XML xml = new Parser.XML();
                xml.Parse(response);
                if (xml.Root.FindNode("pong", false) != null) { return true; }
                return false;
            }
            catch { return false; }
        }

        public static Version Version(string Server, int Timeout)
        {
            if (!Server.ToLower().StartsWith("http://") &&
                !Server.ToLower().StartsWith("https://"))
            {
                Server = "http://" + Server;
            }
            try
            {
                Dictionary<string, string> variables = new Dictionary<string, string>();
                variables.Add("action", "@@version");
                string response = Tools.HttpPostRequest(Server, variables, Timeout);
                Parser.XML xml = new Parser.XML();
                xml.Parse(response);
                Parser.XML.Node node = xml.Root.FindNode("version", false);
                if (node != null)
                {
                    string version = "";
                    foreach (Fact.Parser.XML.Node child in node.Children)
                    {
                        version += child.Text;
                    }
                    version = version.Trim();

                    string major = "";
                    string minor = "";
                    string build = "";

                    int n = 0;
                    for (; n < version.Length; n++) { if (!char.IsDigit(version[n])){ break; } major += version[n]; } n++;
                    for (; n < version.Length; n++) { if (!char.IsDigit(version[n])) { break; } minor += version[n]; } n++;
                    for (; n < version.Length; n++) { if (!char.IsDigit(version[n])) { break; } build += version[n]; } n++;
                    if (major == "") { major = "0"; }
                    if (minor == "") { minor = "0"; }
                    if (build == "") { build = "0"; }

                    return new System.Version(int.Parse(major), int.Parse(minor), int.Parse(build));

                }
                return new Version(0, 0, 0);
            }
            catch { return new Version(0, 0, 0); }
        }

        /// <summary>
        /// Create a Credential that can be used as long as this client is alive
        /// </summary>
        /// <returns>The created credential</returns>
        public Fact.Directory.Credential CreateTempCredential()
        {
            return CallWithTimeout("@@createtempcred", 5000) as Fact.Directory.Credential;
        }

        public object Call(string Method, params object[] Parameters)
        {
            return CallWithTimeout(Method, 5000, Parameters);
        }
        public object CallWithTimeout(string Method, int Timeout, params object[] Parameters)
        {
            if (_Client.Started == false) { _Client.Start(); }
            object syncResponse = null;
            bool responded = false;
            RawClient.Action action = _Client.DoWithTimeout(Method, Timeout, (object response) => { responded = true; syncResponse = response; }, true, Parameters);
            action.WaitForCompletion();
            if (!responded)
            {
                if (!_Client.Authenticated) { throw new Exception("Authentication failure"); }
                throw new Exception("Timeout");
            }
            if (syncResponse != null && syncResponse is Exception) { throw syncResponse as Exception; }
            return syncResponse;
        }

        /// <summary>
        ///  Force the client to close the link with the server
        /// </summary>
        public void Close()
        {
            _Client.Stop();
        }
    }
}

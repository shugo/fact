﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    public class Proxy
    {
        System.Net.HttpListener _Listener = new System.Net.HttpListener();
        volatile bool _Running = false;
        volatile bool _Started = false;

        int _InterfacePort = 0;
        int _SSLInterfacePort = 0;

        public Proxy(Fact.Network.Client Client, int InterfacePort, int SSLInterfacePort)
        {
            _InterfacePort = InterfacePort;
            _SSLInterfacePort = SSLInterfacePort;

        }

        public void Restart()
        {
            if (_Listener != null)
            {
                Stop();
            }
            lock (this)
            {
                while (_Running) { _Started = false; System.Threading.Monitor.Wait(this); }
                _Listener = new System.Net.HttpListener();
                if (_InterfacePort > 0) { _Listener.Prefixes.Add("http://*:" + _InterfacePort + "/"); }
                if (_SSLInterfacePort > 0) { _Listener.Prefixes.Add("https://*:" + _SSLInterfacePort + "/"); }
                _Listener.Start();
                _Started = true;
                new System.Threading.Thread(Run) { IsBackground = true }.Start();
                while (!_Running) { System.Threading.Monitor.Wait(this); }
            }
        }

        public void Start()
        {
            // Yes start is just an alias for restart beause restart
            // will also start the server if it is not started
            Restart();
        }
        public void Stop()
        {
            lock (this)
            {
                _Started = false;
                try
                {
                    _Listener.Stop();
                    _Listener.Close();
                }
                catch { }
                System.Threading.Monitor.PulseAll(this);
            }

            lock (this)
            {
                while (_Running)
                {
                    try
                    {
                        _Listener.Abort();
                    }
                    catch { }
                    _Started = false;
                    System.Threading.Monitor.Wait(this);
                }
                _Listener = null;
            }
        }

        void Run()
        {
            try
            {
                lock (this)
                {
                    _Running = true;
                    System.Threading.Monitor.PulseAll(this);
                }

                while (_Started)
                {
                    try
                    {
                        Dispatcher(_Listener.GetContext());
                    }
                    catch
                    {
                        if (!_Listener.IsListening) { goto Exit; }
                    }
                }
                Exit:
                bool mustRestart = false;
                lock (this)
                {
                    _Running = false;

                    System.Threading.Monitor.PulseAll(this);
                    if (_Started)
                    {
                        mustRestart = true;
                    }
                    else
                    {
                        try
                        {
                            _Listener.Close();
                            _Listener.Stop();
                            _Listener.Close();
                        }
                        catch { }
                    }
                }
                Restart();
            }
            catch { }
        }

        void Dispatcher(System.Net.HttpListenerContext Context)
        {
#if DEBUG
            Fact.Log.Debug("Incoming http request");
#endif
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ServiceExposedMethod : System.Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class ServiceExposedWebcomponent : System.Attribute
    {
        string _Address = "";
        public string Address { get { return _Address; } }
        public ServiceExposedWebcomponent(string Address) { _Address = Address; }
    }

    public abstract class Service
    {
        public class RequestInfo
        {
            internal Fact.Directory.User _User = null; public Fact.Directory.User User { get { return _User; } }
            internal Fact.Network.Server _Server = null; public Fact.Network.Server Server { get { return _Server; } }
            internal string _UserId = ""; public string UserID { get { return _UserId; } }
            internal string _UserHostAddress = ""; public string UserHostAddress { get { return _UserHostAddress; } }
        }
        string _Name = "";
        public string Name { get { return _Name; } }
        public Service(string Name) { _Name = Name; }

        public virtual void OnServiceLoaded(Server Server) { }
        internal List<System.Reflection.MethodInfo> _GetExposedMethods() 
        {
            List<System.Reflection.MethodInfo> methods = new List<System.Reflection.MethodInfo>();
            Type type = this.GetType();
            foreach (System.Reflection.MethodInfo method in type.GetMethods())
            {
                foreach (object attr in method.GetCustomAttributes(true))
                {
                    if (attr is ServiceExposedMethod)
                    {
                        methods.Add(method);
                    }
                }
            }
            return methods;
        }

        internal List<System.Reflection.MethodInfo> GetExposedMethodsInvoke()
        {
            return GetExposedMethods();
        }

        protected List<System.Reflection.MethodInfo> GetExposedMethods()
        {
            return _GetExposedMethods();
        }

        internal Dictionary<string, System.Reflection.MethodInfo> _GetExposedWebComponent()
        {
            Dictionary<string, System.Reflection.MethodInfo> comp = new Dictionary<string, System.Reflection.MethodInfo>();
            Type type = this.GetType();
            foreach (System.Reflection.MethodInfo method in type.GetMethods())
            {
                foreach (object attr in method.GetCustomAttributes(true))
                {
                    if (attr is ServiceExposedWebcomponent)
                    {
                        comp.Add((attr as ServiceExposedWebcomponent).Address, method);
                    }
                }
            }
            return comp;
        }
    }
}

/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Text;
using System.Collections.Generic;

namespace Fact
{
	namespace Network
	{
		public static class Tools
		{
            public static string HttpGetRequest(string URL, int Timeout)
            {
                byte[] data = HttpGetRequestRawResult(URL, Timeout);
                if (data == null) { return ""; }
                return System.Text.Encoding.UTF8.GetString(data);
            }

            public static byte[] HttpGetRequestRawResult(string URL, int Timeout)
            {
                string type = "";
                return HttpRequestRawResult("GET", URL, null, Timeout, "", out type);
            }

            public static string EscapeURIString(string URI)
            {
                //FIX ME: Should be implemented correctly
                try
                { return System.Uri.EscapeDataString(URI); }
                catch { return URI; }
            }
            public static string ToBase64(byte[] Array)
            {
                return System.Convert.ToBase64String(Array);
            }
            public static string ToBase64URL(byte[] Array)
            {
                string base64 = ToBase64(Array);
                return base64.Replace('+', '-').Replace('/', '_');
            }
            public static byte[] FromBase64(string Text)
            {
                return System.Convert.FromBase64String(Text);
            }
            public static byte[] FromBase64URL(string Text)
            {
                return FromBase64(Text.Replace('_', '/').Replace('-', '+'));
            }

            public static string HttpPostRequest(string URL, Dictionary<string, string> Variables, int Timeout)
            {
                string useless = "";
                return HttpPostRequest(URL, Variables, Timeout, out useless);
            }

            public static string HttpPostRequest(string URL, Dictionary<string, string> Variables, int Timeout, out string ContentType)
            {
                return System.Text.Encoding.UTF8.GetString(HttpPostRequestRawResult(URL, Variables, Timeout, out ContentType));
            }

            public static byte[] HttpPostRequestRawResult(string URL, Dictionary<string, string> Variables, int Timeout, out string ContentType)
            {
                bool first = true;
                List<byte> fullMessage = new List<byte>();
                foreach (string entry in Variables.Keys)
                {
                    string entryText = (first ? "" : "&") +
                        EscapeURIString(entry) +
                        "=" +
                        EscapeURIString(Variables[entry]);

                    first = false;
                    fullMessage.AddRange(System.Text.Encoding.UTF8.GetBytes(entryText));
                }
                return HttpRequestRawResult("POST", URL, fullMessage.ToArray(), Timeout, "", out ContentType);
            }

            public static string HttpPostRequest(string URL, byte[] Data, int Timeout)
            {
                string useless = "";
                return System.Text.Encoding.UTF8.GetString(HttpPostRequestRawResult(URL, Data, Timeout, out useless));
            }

            public static string HttpPostRequest(string URL, byte[] Data, int Timeout, out string ContentType)
            {
                return System.Text.Encoding.UTF8.GetString(HttpPostRequestRawResult(URL, Data, Timeout, out ContentType));
            }

            public static byte[] HttpPostRequestRawResult(string URL, byte[] Data, int Timeout, out string ContentType)
            {
                return HttpRequestRawResult("POST", URL, Data, Timeout, "application/octet-stream", out ContentType);
            }

            static byte[] HttpRequestRawResult(string Type, string URL, byte[] Message, int Timeout, string PostContentType, out string ContentType)
            {
                System.Net.HttpWebRequest webRequest = System.Net.HttpWebRequest.Create(URL) as System.Net.HttpWebRequest;
                if (PostContentType != "") { webRequest.ContentType = PostContentType; }
                webRequest.Method = Type;

                System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
                stopwatch.Start();
                if (Message == null)
                {
                    webRequest.ContentLength = 0;
                }
                else
                {
                    webRequest.ContentLength = Message.Length;
                    webRequest.GetRequestStream().Write(Message, 0, Message.Length);
                }
                System.Net.WebResponse webResponse = webRequest.GetResponse();
                ContentType = webResponse.ContentType;
                // FIXME: Handle request of more than 4GB
                int remaining = (int)webResponse.ContentLength;
                int offset = 0;
                byte[] buffer = new byte[1024];
                byte[] data = new byte[remaining];
                while (remaining > 0 && (Timeout < 0 || stopwatch.ElapsedMilliseconds < Timeout))
                {
                    int length = webResponse.GetResponseStream().Read(data, offset, remaining);
                    remaining -= length;
                    offset += length;
                }
                stopwatch.Stop();
                try { webResponse.Close(); } catch { }
                if (stopwatch.ElapsedMilliseconds > Timeout)
                {
                    throw new Exception("Timeout while doing the web resquest");
                }
                return data;
            }

			public static Runtime.Process.Result WebRequest(string URL, int Timeout)
			{
				if (URL.StartsWith ("http://"))
				{
					try
					{
						System.Net.HttpWebRequest Request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create (URL);
						Request.Timeout = Timeout;
						Request.UserAgent = "Fact";
						System.Net.WebResponse response = Request.GetResponse();
						Runtime.Process.Result result = new Fact.Runtime.Process.Result ();
						byte[] array = new byte[response.ContentLength];
						int offset = 0;
                        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
                        stopwatch.Start();
						while (offset < array.Length && (Timeout < 0 || stopwatch.ElapsedMilliseconds < Timeout))
						{
                            int length = response.GetResponseStream().Read(array, offset, array.Length);
                            offset += length;
                            if (length < 4096) { System.Threading.Thread.Sleep(10); }
						}
                        stopwatch.Stop();
                        if (stopwatch.ElapsedMilliseconds > Timeout)
						{
							result.TimeOutExit = true;
							return result;
						}
						result.StdOut = "content type = " + response.ContentType + "\n" + System.Text.Encoding.UTF8.GetString(array);
						return result;
					}
					catch (Exception e)
					{
						Runtime.Process.Result result = new Fact.Runtime.Process.Result ();
						result.StdErr = e.Message; 
						return result;
					}
				}
				{
					Runtime.Process.Result result = new Fact.Runtime.Process.Result ();
					result.StdErr = "Protocol not supported";
					return result;
				}
			}

			public static int FindAvailablePort(int From, int To)
			{
				for (int x = 0; x < 10; x++)
				{
					for (int n = From; n <= To; n++)
					{
						if (IsPortAvailable (n))
							return n;
					}
				}
				return -1;
			}

			public static bool IsPortAvailable(int Port)
			{
				try
				{
					System.Net.Sockets.TcpListener listener = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Loopback, Port);
					listener.ExclusiveAddressUse = false;
					try
					{
						listener.Start();
					}
					catch 
					{
						try
						{
							listener.Stop();
						}
						catch 
						{
						}
						return false;
					}
					try
					{
						listener.Stop();
					}
					catch 
					{
						return false;
					}
					System.Threading.Thread.Sleep(10);
					return true;
				}
				catch 
				{
					return false;
				}
			}
		}
	}
}


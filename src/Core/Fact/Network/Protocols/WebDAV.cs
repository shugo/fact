﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network.Protocols
{
    public class WebDAV
    {
        public class FileSystem
        {
            public class File : Element
            {
                public virtual byte[] GetBytes(Fact.Directory.User User) { return null; }
            }

            public class Element
            {
                public virtual string GetName(Fact.Directory.User User) { return ""; }
            }

            public class Directory : Element
            {
                public virtual List<File> GetAllFiles(Fact.Directory.User User) { return null; }
                public virtual File GetFile(Fact.Directory.User User, string Name)
                {
                    List<File> files = GetAllFiles(User);
                    if (files != null)
                    {
                        foreach (File file in files)
                        {
                            if (file.GetName(User) == Name) { return file; }
                        }
                    }
                    return null;
                }

                public virtual List<Directory> GetAllDirectories(Fact.Directory.User User) { return null; }
                public virtual Directory GetDirectory(Fact.Directory.User User, string Name)
                {
                    List<Directory> directories = GetAllDirectories(User);
                    if (directories != null)
                    {
                        foreach (Directory directory in directories)
                        {
                            if (directory.GetName(User) == Name) { return directory; }
                        }
                    }
                    return null;
                }
            }

            public virtual Directory GetRoot(Fact.Directory.User User) { return null; }
        }
        FileSystem _Filesystem = null;
        Server _Server = null;
        byte[] _Options = new byte[0];
        string _Path = "";
        public WebDAV(Server Server, string Path)
        {
            _Server = Server;
            _Server.Map(Path, Mapper);
            _Options = System.Text.Encoding.UTF8.GetBytes(
                "Allow: OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, COPY, MOVE\r\nAllow: MKCOL, PROPFIND, PROPPATCH, LOCK, UNLOCK, ORDERPATCH\r\nDAV: 1, 2, 3");
            _Path = Path;
        }

        public void Map(string RemoteDirectory, string LocalDirectory)
        {
            if (!System.IO.Directory.Exists(LocalDirectory)) { throw new Exception("Invalid Local Directory"); }
        }

        byte[] Mapper(System.Net.HttpListenerContext Context)
        {
            try
            {
                switch (Context.Request.HttpMethod.ToLower())
                {
                    case "option":
                    case "options": return DoOption(Context);
                    case "propfind": return DoPropfind(Context);
                    case "put": return DoPut(Context);
                    case "lock": return DoLock(Context);
                    case "unlock": return DoUnlock(Context);
                    case "get": return DoGet(Context);
                    default:
                        Context.Response.StatusCode = 404;
                        break;
                }
            }
            catch { Context.Response.StatusCode = 500; }
            return new byte[0];
        }
  
        FileSystem.Element SolveMapping(string Path, Fact.Directory.User User)
        {
            if (!Path.StartsWith("/")) { Path = "/" + Path; }
            if (Path.StartsWith(_Path)) { Path = Path.Substring(_Path.Length); }

            FileSystem.Directory current = _Filesystem.GetRoot(User);
            if (current == null) { return null; }

            {
                string[] fragment = Path.Split('/');
                for (int n = 0; n < fragment.Length; n++)
                {
                    if (fragment[n] == null || fragment[n].Trim() == "") { continue; }
                    if (n == fragment.Length - 1)
                    {
                        FileSystem.File file = current.GetFile(User, fragment[n]);
                        if (file != null) { return file; }
                        FileSystem.Directory directory = current.GetDirectory(User, fragment[n]);
                        if (directory != null) { return directory; }
                        return null;
                    }
                    current = current.GetDirectory(User, fragment[n]);
                    if (current == null) { return null; }
                    return null;
                }
            }
            return null;
        }

        Fact.Parser.XML.Node ParseRequest(System.Net.HttpListenerContext Context)
        {
            try
            {
                Fact.Parser.XML XML = new Parser.XML();
                byte[] array = new byte[Context.Request.ContentLength64];
                int count = 0;
                do
                {
                    count += Context.Request.InputStream.Read(array, count, 4096);
                } while (count < array.Length);
                string text = System.Text.Encoding.UTF8.GetString(array);
                XML.Parse(text);
                return XML.Root;
            }
            catch { return null; }
        }

        byte[] DoOption(System.Net.HttpListenerContext Context)
        {
            Context.Response.Headers.Add("Allow: OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, COPY, MOVE, MKCOL, PROPFIND, PROPPATCH, LOCK, UNLOCK");
            Context.Response.Headers.Add("DAV: 1, 2, 3");
            return new byte[0];
        }

        byte[] DoPropfind(System.Net.HttpListenerContext Context)
        {
            return null;
        }

        byte[] DoPut(System.Net.HttpListenerContext Context)
        {
            return null;
        }

        byte[] DoLock(System.Net.HttpListenerContext Context)
        {
            return null;
        }

        byte[] DoUnlock(System.Net.HttpListenerContext Context)
        {
            return null;
        }

        byte[] DoGet(System.Net.HttpListenerContext Context)
        {
            FileSystem.Element element = SolveMapping(Context.Request.Url.AbsolutePath, null);
            if (element is FileSystem.File)
            {
                return (element as FileSystem.File).GetBytes(null);
            }
            return null;
        }
    }
}

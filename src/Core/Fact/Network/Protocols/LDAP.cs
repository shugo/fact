﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network.Protocols
{
    public class LDAP
    {
        class LDAPDirectory : Directory.Directory
        {

        }

        class Requests
        {
            public enum Protocol
            {
                LDAP,
                LDAPS
            }
            string _Server = "";
            Protocol _RequestProtocol = Protocol.LDAPS;

            public string Server { get { return _Server; } set { _Server = value; } }
            public Protocol RequestProtocol { get { return _RequestProtocol; } set { _RequestProtocol = value; } }
        }

        string DoRequest(Requests Requests)
        {
            System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient(Requests.Server, 386);
            System.IO.Stream ldapStream = client.GetStream();
            if (Requests.RequestProtocol == Requests.Protocol.LDAPS)
            {
                ldapStream = new System.Net.Security.SslStream(ldapStream);
            }

            int messageId = 1;
            try
            {
                // FIX ME: Do the LDAP request
            }
            catch
            {
            }
            finally
            {
                try { ldapStream.Close(); } catch { }
                try { client.Close(); } catch { }
            }
            return "";
        }


        LDAPDirectory _LDAPDirectory = null;
        public Directory.Directory Directory { get { return _LDAPDirectory; } }
    }
}

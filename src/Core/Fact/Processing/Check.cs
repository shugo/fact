﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing
{
    public abstract class Check : Step
    {
        public virtual Test.Result.Result Verify(Character.MetaChar MetaChar) { return null; }
        public virtual Test.Result.Result Verify(List<Character.MetaChar> Text) { return null; }
        public virtual Test.Result.Result Verify(Character.MetaLine MetaLine) { return null; }
        public virtual Test.Result.Result Verify(Character.MetaToken MetaToken) { return null; }
        public virtual Test.Result.Result Verify(Lexer.Lexer Lexer) { return null; }
        public virtual Test.Result.Result Verify(File File) { return null; }
        public virtual Test.Result.Result Verify(Project Project) { return null; }

        public virtual List<Test.Result.Result> MultiVerify(File File) { return null; }
        public virtual List<Test.Result.Result> MultiVerify(Project Project) { return null; }

        List<Test.Result.Result> _Results = new List<Fact.Test.Result.Result>();

        public List<Test.Result.Result> Results
        {
            get { return _Results; }
            set { _Results = value; }
        }

        public override void Process(Fact.Character.MetaChar MetaChar)
        {
            Test.Result.Result result = Verify(MetaChar);
            
            if (result != null)
            {
                if (result.FromLine == result.ToLine && result.FromLine == -1)
                { result.FromLine = (int)MetaChar.Line; result.ToLine = (int)MetaChar.Line; }
                _Results.Add(result);
                if (result.File != null) { result.File.AddTestResult(result); }
                else if (MetaChar.File != null) { MetaChar.File.AddTestResult(result); }
            }
        }

        public override void Process(Fact.Character.MetaLine MetaLine)
        {
            Test.Result.Result result = Verify(MetaLine);
            
            if (result != null)
            {
                if (result.FromLine == result.ToLine && result.FromLine == -1)
                { result.FromLine = MetaLine.Line; result.ToLine = MetaLine.Line; }
                _Results.Add(result);
                if (result.File != null) { result.File.AddTestResult(result); }
                else if (MetaLine.File != null) { MetaLine.File.AddTestResult(result); }
            }
        }

        public override void Process(Fact.Character.MetaToken MetaToken)
        {
            Test.Result.Result result = Verify(MetaToken);
            if (result != null)
            {
                _Results.Add(result);
                if (result.File != null) { result.File.AddTestResult(result); }
				else if (MetaToken.File != null) { MetaToken.File.AddTestResult(result); }
                
            }
        }

        public override void Process(List<Fact.Character.MetaChar> Text)
        {
            Test.Result.Result result = Verify(Text);
            if (result != null)
            {
                _Results.Add(result);
                if (result.File != null) { result.File.AddTestResult(result); }
            }
        }

        public override void Process(Lexer.Lexer Lexer)
        {
            Test.Result.Result result = Verify(Lexer);
            if (result != null)
            {
                _Results.Add(result);
                if (result.File != null) { result.File.AddTestResult(result); }
            }
        }

        public override void Process(File File)
        {
            Test.Result.Result result = Verify(File);
            if (result != null)
            {
                _Results.Add(result);
                if (result.File != null) { result.File.AddTestResult(result.TopOfTree()); }
                else if (File != null) { File.AddTestResult(result.TopOfTree()); }
            }
            List<Test.Result.Result> multiResult = MultiVerify(File);
            if (multiResult != null)
            {
                foreach (Test.Result.Result subresult in multiResult)
                {
                    _Results.Add(subresult);
                    if (subresult.File != null) { subresult.File.AddTestResult(subresult.TopOfTree()); }
                   else if (File != null){ File.AddTestResult(subresult.TopOfTree()); }
                }
            }
        }

        public override void Process(Project Project)
        {
            Test.Result.Result result = Verify(Project);
            if (result != null)
            {
                _Results.Add(result);
                if (result.File != null) { result.File.AddTestResult(result.TopOfTree()); }
                else if (Project != null) { Project.AddTestResult(result.TopOfTree()); }
            }
            List<Test.Result.Result> multiResult = MultiVerify(Project);
            if (multiResult != null)
            {
                foreach (Test.Result.Result subresult in multiResult)
                {
                    if (subresult != null)
                    {
                        _Results.Add(subresult);
                        if (subresult.File != null) { subresult.File.AddTestResult(subresult.TopOfTree()); }
                        else if (Project != null) { Project.AddTestResult(subresult.TopOfTree()); }
                    }
                }
            }
        }
      

        public delegate Test.Result.Result Check_On_Char(Character.MetaChar MetaChar);
        public delegate Test.Result.Result Check_On_Line(Character.MetaLine MetaLine);
        public delegate Test.Result.Result Check_On_Text(List<Character.MetaChar> MetaChar);
        public delegate Test.Result.Result Check_On_Token(Character.MetaToken MetaToken);
        public delegate Test.Result.Result Check_On_Lexer(Lexer.Lexer Lexer);
        public delegate Test.Result.Result Check_On_File(File File);
        public delegate Test.Result.Result Check_On_Project(Project Project); 

        public delegate List<Test.Result.Result> Multi_Check_On_File(File File);
        public delegate List<Test.Result.Result> Multi_Check_On_Project(Project Project);

        class Internal_Check_On_Char : Check
        {
            Check_On_Char _Function;
            public Internal_Check_On_Char(Check_On_Char Function) { Level = StepLevel.Character; _Function = Function; }
            public override Test.Result.Result Verify(Character.MetaChar MetaChar) { return _Function(MetaChar); }
        }

        class Internal_Check_On_Line : Check
        {
            Check_On_Line _Function;
            public Internal_Check_On_Line(Check_On_Line Function) { Level = StepLevel.Line; _Function = Function; }
            public override Test.Result.Result Verify(Character.MetaLine MetaLine) { return _Function(MetaLine); }
        }

        class Internal_Check_On_Text : Check
        {
            Check_On_Text _Function;
            public Internal_Check_On_Text(Check_On_Text Function) { Level = StepLevel.Text; _Function = Function; }
            public override Test.Result.Result Verify(List<Character.MetaChar> MetaLine) { return _Function(MetaLine); }
        }


        class Internal_Check_On_Token : Check
        {
            Check_On_Token _Function;
            public Internal_Check_On_Token(Check_On_Token Function) { Level = StepLevel.Token; _Function = Function; }
            public override Test.Result.Result Verify(Character.MetaToken MetaToken) { return _Function(MetaToken); }
        }

        class Internal_Check_On_Lexer : Check
        {
            Check_On_Lexer _Function;
            public Internal_Check_On_Lexer(Check_On_Lexer Function) { Level = StepLevel.Lexer; _Function = Function; }
            public override Test.Result.Result Verify(Lexer.Lexer Lexer) { return _Function(Lexer); }
        }

        class Internal_Check_On_File : Check
        {
            Check_On_File _Function;
            public Internal_Check_On_File(Check_On_File Function) { Level = StepLevel.File; _Function = Function; }
            public override Test.Result.Result Verify(File File) { return _Function(File); }
        }

        class Internal_Check_On_Project : Check
        {
            Check_On_Project _Function;
            public Internal_Check_On_Project(Check_On_Project Function) { Level = StepLevel.Project; _Function = Function; }
            public override Test.Result.Result Verify(Project Project) { return _Function(Project); }
        }

        class Internal_Multi_Check_On_File : Check
        {
            Multi_Check_On_File _Function;
            public Internal_Multi_Check_On_File(Multi_Check_On_File Function) { Level = StepLevel.MultiFile; _Function = Function; }
            public override List<Test.Result.Result> MultiVerify(File File) { return _Function(File); }
        }

        class Internal_Multi_Check_On_Project : Check
        {
            Multi_Check_On_Project _Function;
            public Internal_Multi_Check_On_Project(Multi_Check_On_Project Function) { Level = StepLevel.MultiProject; _Function = Function; }
            public override List<Test.Result.Result> MultiVerify(Project Project) { return _Function(Project); }
        }

        public static implicit operator Check(Check_On_Char Function)
        { return new Internal_Check_On_Char(Function); }
        public static implicit operator Check(Check_On_Line Function)
        { return new Internal_Check_On_Line(Function); }
        public static implicit operator Check(Check_On_Text Function)
        { return new Internal_Check_On_Text(Function); }
        public static implicit operator Check(Check_On_Token Function)
        { return new Internal_Check_On_Token(Function); }
        public static implicit operator Check(Check_On_File Function)
        { return new Internal_Check_On_File(Function); }
        public static implicit operator Check(Check_On_Lexer Function)
        { return new Internal_Check_On_Lexer(Function); }
        public static implicit operator Check(Check_On_Project Function)
        { return new Internal_Check_On_Project(Function); }

        public static implicit operator Check(Multi_Check_On_File Function)
        { return new Internal_Multi_Check_On_File(Function); }
        public static implicit operator Check(Multi_Check_On_Project Function)
        { return new Internal_Multi_Check_On_Project(Function); }
    }
}

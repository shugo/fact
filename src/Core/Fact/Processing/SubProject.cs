﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing
{
    public class SubProject : Project
    {
        Project _Project = null;
        string _NewRoot = "";
        public SubProject(Project Project, string NewRoot)
            : this (Project, NewRoot, Project.Name)
        { }
        public SubProject(Project Project, string NewRoot, string Name) : base(Name)
        {
            _Project = Project;
            _NewRoot = NewRoot;
        }
        public override void AddFile(string Directory, File File, bool KeepPrevious)
        {
            _Project.AddFile(_NewRoot + "/" + Directory, File, KeepPrevious);
        }
        public override void AddTestResult(Fact.Test.Result.Result Result)
        {
            _Project.AddTestResult(Result);
        }
        public override bool Contains(File file)
        {
            foreach (File subfile in GetFiles("", true))
            {
                if (subfile == file) { return true; }
            }
            return false;
        }
        public override List<File> GetFiles(string DirectoryName, bool Recursive, params File.FileType[] FileType)
        {
            return _Project.GetFiles(_NewRoot + "/" + DirectoryName, Recursive, FileType);
        }
        public override bool Contains(string DirectoryName, string FileName)
        {
            return _Project.Contains(_NewRoot + "/" + DirectoryName, FileName);
        }
        public override bool RemoveDirectory(string Name)
        {
            return _Project.RemoveDirectory(_NewRoot + "/" + Name);
        }
        public override bool RemoveFile(string DirectoryName, string FileName)
        {
            return _Project.RemoveFile(_NewRoot + "/" + DirectoryName, FileName);
        }
        public override List<Fact.Test.Result.Result> GetTestResults()
        {
            return _Project.GetTestResults();
        }
        public override bool Empty
        {
            get
            {
                return GetFiles("", true).Count == 0;
            }
        }
        public override bool Contains(string Name)
        {
            return _Project.Contains(_NewRoot + "/" + Name);
        }
        public override IEnumerator<File> GetEnumerator()
        {
            foreach (File file in GetFiles("", true))
            {
                yield return file;
            }
        }
        public override File GetFile(string DirectoryName, string FileName)
        {
            return _Project.GetFile(_NewRoot + "/" + DirectoryName, FileName);
        }
        public override List<File> GetFiles(string Pattern)
        {
            return _Project.GetFiles(_NewRoot + "/" + Pattern);
        }
        public override File GetFile(string Name)
        {
            return _Project.GetFile(_NewRoot + "/" + Name);
        }
        public override List<string> ExtractFiles(string Pattern, string Where)
        {
            return _Project.ExtractFiles(_NewRoot + "/" + Pattern, Where);
        }
        public override List<string> ExtractDirectory(string DirectoryName, string Where, params File.FileType[] FileType)
        {
            return _Project.ExtractDirectory(_NewRoot + "/" + DirectoryName, Where, FileType);
        }
        public override IEnumerable<string> GetSubDirectories(string DirectoryName, bool Recursive)
        {
            return _Project.GetSubDirectories(_NewRoot + "/" + DirectoryName, Recursive);
        }
        public override Fact.Test.Result.Group GetTestGroup(string Name)
        {
            return _Project.GetTestGroup(Name);
        }
        public override void Save(System.IO.Stream stream, bool compress)
        {
            _Project.Save(stream, compress);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Lexer
{
    /// <summary>
    /// A Class to create lexers from a file
    /// </summary>
    public class Lexer
    {
        File _File = null;
        int _Index = 0;
        int _Limit = -1;
        List<Character.MetaToken> _Tokens = new List<Character.MetaToken>();
        bool _IgnoreSpaces = false;
        public bool IgnoreSpaces { get { return _IgnoreSpaces; } set { _IgnoreSpaces = value; } }
        bool _IgnoreNewLines = false;
        public bool IgnoreNewLines { get { return _IgnoreNewLines; } set { _IgnoreNewLines = value; } }
        bool _IgnorePreprocessor = false;
        public bool IgnorePreprocessor { get { return _IgnorePreprocessor; } set { _IgnorePreprocessor = value; } }
        public Lexer(File File)
        {
            _File = File;
            _Tokens = _File.GetAsToken();
        }

        bool _IsValidToken(int Offset)
        {
            if (_Limit >= 0 && _Index + Offset >= _Limit) { return false; }
            if (_Index + Offset >= _Tokens.Count)
                return false;
            if (_Tokens[_Index + Offset].Type == Fact.Character.MetaChar.BasicCharType.Comment)
                return false;
            if (_IgnoreSpaces)
            {
                if (_Tokens[_Index + Offset].Type == Fact.Character.MetaChar.BasicCharType.Space)
                    return false;
            }
            if (_IgnoreNewLines)
            {
                if (_Tokens[_Index + Offset].Type == Fact.Character.MetaChar.BasicCharType.EndOfLine)
                    return false;
            }
            if (_IgnorePreprocessor)
            {
                if (_Tokens[_Index + Offset].Type == Fact.Character.MetaChar.BasicCharType.Preprocessor)
                    return false;
            }
            return true;
        }

        public Character.MetaToken EatToken() 
        {
            Character.MetaToken token = null;
            while (!_IsValidToken(0) && _Index < _Tokens.Count &&
                   (_Limit < 0 || _Index < _Limit) )
            {
                _Index++;
            }
            if (_Limit >= 0 && _Index >= _Limit)
            { return null; }
            if (_Index >= _Tokens.Count)
            { return null; }
            token = _Tokens[_Index];
            _Index++;
            return token;
        }

        public Character.MetaToken GetToken(int At)
        {
            int offset = 0;
            while (_Index + offset < _Tokens.Count)
            {
                if (_IsValidToken(offset))
                {
                    if (At == 0) 
                    {
                        break;
                    }
                    At--;   
                }
                offset++;
            }
            if (_Limit >= 0 && _Index + offset >= _Limit)
            { return null; }
            if (_Index + offset >= _Tokens.Count)
            { return null; }
            return _Tokens[_Index + offset];
        }

        public int RemainingTokens
        {
            get
            {
                if (_Index >= _Tokens.Count) { return 0; }
                if (_Limit >= 0 && _Index >= _Limit) { return 0; }
                int remaining = 0;
                int offset = 0;
                while (offset + _Index < _Tokens.Count &&
                     (_Limit < 0 || offset + _Index  < _Limit))
                {
                    if (_IsValidToken(offset)) { remaining++; }
                    offset++;
                }
                return remaining;
            }
        }

        public Lexer Clone()
        {
            Lexer clone = new Lexer(_File);
            clone._Index = _Index;
            clone._Limit = _Limit;
            clone._IgnoreNewLines = _IgnoreNewLines;
            clone._IgnoreSpaces = _IgnoreSpaces;
            clone._IgnorePreprocessor = _IgnorePreprocessor;
            clone._Tokens = new List<Character.MetaToken>(_Tokens);
            return clone;
        }
        public Lexer CreateSubLexer(int Offset)
        {
            Lexer lexer = Clone();
            int realoffset = 0;
            while (Offset > 0 && realoffset < lexer._Tokens.Count)
            {
                if (lexer._IsValidToken(realoffset))
                {
                    Offset--;
                }
                realoffset++;
            }

            lexer._Index += realoffset;
            if (_Limit >= 0 && lexer._Limit > _Limit) { lexer._Limit = _Limit; }
            return lexer;
        }
        public Lexer CreateSubLexer(int Offset, int Limit)
        {
            Lexer lexer = Clone();
            int reallimit = 0;
            while (Limit > 0 && reallimit < lexer._Tokens.Count)
            {
                if (lexer._IsValidToken(reallimit))
                {
                    Limit--;
                }
                reallimit++;
            }
            lexer._Limit = _Index + reallimit;
            int realoffset = 0;
            while (Offset > 0 && realoffset < lexer._Tokens.Count)
            {
                if (lexer._IsValidToken(realoffset))
                {
                    Offset--;
                }
                realoffset++;
            }

            lexer._Index += realoffset;
            if (_Limit >= 0 && lexer._Limit > _Limit) { lexer._Limit = _Limit; }
            return lexer;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            int inc = 0;
            try
            {
                while ((object)GetToken(inc) != null)
                {
                    try
                    {
                        builder.Append(GetToken(inc).ToString());
                        builder.Append(" ");
                        inc++;
                    }
                    catch { break; }
                }
            }
            catch { }
            return builder.ToString();
        }
    }
}

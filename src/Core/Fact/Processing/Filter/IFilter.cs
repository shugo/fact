﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter
{
    public interface IFilter
    {
        bool Valid(Character.MetaChar Char);
        bool Valid(Character.MetaLine Line);
        bool Valid(Character.MetaToken Token);
    }
}

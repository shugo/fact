﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter.Is
{
    public class IsSpace : IFilter
    {
        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            return Char.Type == Fact.Character.MetaChar.BasicCharType.Space;
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return false;
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            return Token.Type == Fact.Character.MetaChar.BasicCharType.Space;
        }

        #endregion
    }
}

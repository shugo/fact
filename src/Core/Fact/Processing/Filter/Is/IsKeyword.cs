﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter.Is
{
    public class IsKeyword : IFilter
    {
        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            return Char.TokenType == Fact.Character.MetaChar.BasicTokenType.KeyWord;
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return false;
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            return Token.TokenType != Fact.Character.MetaChar.BasicTokenType.KeyWord;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter.Is
{
    public class IsSeparator : IFilter
    {
        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            return Char.SeparatorType != Fact.Character.MetaChar.BasicSeparatorType.None;
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return false;
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            return Token.SeparatorType != Fact.Character.MetaChar.BasicSeparatorType.None;
        }

        #endregion
    }
}

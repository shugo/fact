﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter.Arround
{
    public class Previous<Filter> : IFilter
        where Filter : IFilter, new()
    {
        Filter F = new Filter();

        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            return !Char.Previous.Equals(null) && F.Valid(Char.Previous);
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return false;
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            return !Token.Previous.Equals(null) && F.Valid(Token.Previous);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter.Arround
{
    public class PreviousMatching<Match, Filter> : IFilter
        where Match : IFilter, new()
        where Filter : IFilter, new()
    {
        Match M = new Match();
        Filter F = new Filter();

        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            Character.MetaChar C = Char.Previous;
            while (!C.Equals(null) && !M.Valid(C)) 
            {
                C = C.Previous;
            }

            if (C.Equals(null)) { return false; }

            return !F.Valid(C);
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return false;
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            Character.MetaToken T = Token.Previous;
            while (!T.Equals(null) && !M.Valid(T))
            {
                T = T.Previous;
            }

            if (T.Equals(null)) { return false; }

            return !F.Valid(T);
        }

        #endregion
    }
}

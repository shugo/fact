﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter.Arround
{
    public class NextMatching<Match, Filter> : IFilter
        where Match : IFilter, new()
        where Filter : IFilter, new()
    {
        Match M = new Match();
        Filter F = new Filter();

        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            Character.MetaChar C = Char.Next;
            while (!C.Equals(null) && !M.Valid(C)) 
            {
                C = C.Next;
            }

            if (C.Equals(null)) { return false; }

            return !F.Valid(C);
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return false;
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            Character.MetaToken T = Token.Next;
            while (!T.Equals(null) && !M.Valid(T))
            {
                T = T.Next;
            }

            if (T.Equals(null)) { return false; }

            return !F.Valid(T);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter.Arround
{
    public class Next<Filter> : IFilter
        where Filter : IFilter, new()
    {
        Filter F = new Filter();

        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            return !Char.Next.Equals(null) && F.Valid(Char.Next);
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return false;
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            return !Token.Next.Equals(null) && F.Valid(Token.Next);
        }
        #endregion
    }
}

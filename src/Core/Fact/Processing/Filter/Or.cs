﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter
{
    public class Or<FilterA, FilterB> : IFilter
        where FilterA : IFilter, new()
        where FilterB : IFilter, new()
    {
        FilterA A = new FilterA();
        FilterB B = new FilterB();

        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            return A.Valid(Char) || B.Valid(Char);
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return A.Valid(Line) || B.Valid(Line);
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            return A.Valid(Token) || B.Valid(Token);
        }

        #endregion
    }
}

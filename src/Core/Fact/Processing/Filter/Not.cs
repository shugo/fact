﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter
{
    public class Not<SubFilter> : IFilter where SubFilter : IFilter, new()
    {

        SubFilter subFilter = new SubFilter();

        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            return !subFilter.Valid(Char);
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            return !subFilter.Valid(Line);
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            return !subFilter.Valid(Token);
        }

        #endregion
    }
}

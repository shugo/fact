﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Filter
{
    public class Xor<FilterA, FilterB> : IFilter
        where FilterA : IFilter, new()
        where FilterB : IFilter, new()
    {
        FilterA A = new FilterA();
        FilterB B = new FilterB();

        #region IFilter Membres

        public bool Valid(Fact.Character.MetaChar Char)
        {
            bool AValue = A.Valid(Char);
            bool BValue = B.Valid(Char);

            return  (AValue && !BValue) || (!AValue && BValue);
        }

        public bool Valid(Fact.Character.MetaLine Line)
        {
            bool AValue = A.Valid(Line);
            bool BValue = B.Valid(Line);

            return (AValue && !BValue) || (!AValue && BValue);
        }

        public bool Valid(Fact.Character.MetaToken Token)
        {
            bool AValue = A.Valid(Token);
            bool BValue = B.Valid(Token);

            return (AValue && !BValue) || (!AValue && BValue);
        }

        #endregion
    }
}

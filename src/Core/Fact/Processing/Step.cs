﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing
{
    public abstract class Step
    {
        public enum StepLevel
        {
           Character,
           Line,
           Text,
           Token,
           Lexer,
           File,
           Project,
           MultiFile,
           MultiProject,
        }
        StepLevel _Level;

        public StepLevel Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
        public virtual void Process(Character.MetaChar MetaChar) { }
        public virtual void Process(List<Character.MetaChar> Text) { }
        public virtual void Process(Character.MetaLine MetaLine) { }
        public virtual void Process(Character.MetaToken MetaToken) { }
        public virtual void Process(Lexer.Lexer Lexer) { }
        public virtual void Process(Project Project) { }
        public virtual void Process(File File) { }

        public delegate void Step_On_Char(Character.MetaChar MetaChar);
        public delegate void Step_On_Line(Character.MetaLine MetaLine);
        public delegate void Step_On_Text(List<Character.MetaChar> MetaLine);
        public delegate void Step_On_Token(Character.MetaToken MetaToken);
        public delegate void Step_On_Lexer(Lexer.Lexer Lexer);
        public delegate void Step_On_File(File File);
        public delegate void Step_On_Project(Project Project);

        class Internal_Step_On_Char : Step
        {
            Step_On_Char _Function;
            public Internal_Step_On_Char(Step_On_Char Function) { Level = StepLevel.Character; _Function = Function; }
            public override void Process(Character.MetaChar MetaChar) { _Function(MetaChar); }
        }

        class Internal_Step_On_Line : Step
        {
            Step_On_Line _Function;
            public Internal_Step_On_Line(Step_On_Line Function) { Level = StepLevel.Line; _Function = Function; }
            public override void Process(Character.MetaLine MetaLine) { _Function(MetaLine); }
        }

        class Internal_Step_On_Text : Step
        {
            Step_On_Text _Function;
            public Internal_Step_On_Text(Step_On_Text Function) { Level = StepLevel.Text; _Function = Function; }
            public override void Process(List<Character.MetaChar> MetaLine) { _Function(MetaLine); }
        }


        class Internal_Step_On_Token : Step
        {
            Step_On_Token _Function;
            public Internal_Step_On_Token(Step_On_Token Function) { Level = StepLevel.Token; _Function = Function; }
            public override void Process(Character.MetaToken MetaToken) { _Function(MetaToken); }
        }

        class Internal_Step_On_Lexer : Step
        {
            Step_On_Lexer _Lexer;
            public Internal_Step_On_Lexer(Step_On_Lexer Lexer) { Level = StepLevel.Lexer; _Lexer = Lexer; }
            public override void Process(Lexer.Lexer Lexer) { _Lexer(Lexer); }
        }

        class Internal_Step_On_File : Step
        {
            Step_On_File _File;
            public Internal_Step_On_File(Step_On_File File) { Level = StepLevel.File; _File = File; }
            public override void Process(File File) { _File(File); }
        }

        class Internal_Step_On_Project : Step
        {
            Step_On_Project _Project;
            public Internal_Step_On_Project(Step_On_Project Project) { Level = StepLevel.Project; _Project = Project; }
            public override void Process(Project Project) { _Project(Project); }
        }

        public static implicit operator Step(Step_On_Char Function) 
        { return new Internal_Step_On_Char(Function); }
        public static implicit operator Step(Step_On_Line Function)
        { return new Internal_Step_On_Line(Function); }
        public static implicit operator Step(Step_On_Text Function)
        { return new Internal_Step_On_Text(Function); }
        public static implicit operator Step(Step_On_Token Function)
        { return new Internal_Step_On_Token(Function); }
        public static implicit operator Step(Step_On_Lexer Function)
        { return new Internal_Step_On_Lexer(Function); }
        public static implicit operator Step(Step_On_File Function)
        { return new Internal_Step_On_File(Function); }
        public static implicit operator Step(Step_On_Project Function)
        { return new Internal_Step_On_Project(Function); }

        public static Step CreateStep(Step_On_Char Function) { return new Internal_Step_On_Char(Function); }
        public static Step CreateStep(Step_On_Line Function) { return new Internal_Step_On_Line(Function); }
        public static Step CreateStep(Step_On_Text Function) { return new Internal_Step_On_Text(Function); }
        public static Step CreateStep(Step_On_Token Function) { return new Internal_Step_On_Token(Function); }
        public static Step CreateStep(Step_On_Lexer Function) { return new Internal_Step_On_Lexer(Function); }
        public static Step CreateStep(Step_On_File Function) { return new Internal_Step_On_File(Function); }
        public static Step CreateStep(Step_On_Project Function) { return new Internal_Step_On_Project(Function); }

        public static Step CreateStep(Step_On_Char Function, Filter.IFilter Filter) { return new Internal_Step_On_Char(Function); }
        public static Step CreateStep(Step_On_Line Function, Filter.IFilter Filter) { return new Internal_Step_On_Line(Function); }
        public static Step CreateStep(Step_On_Text Function, Filter.IFilter Filter) { return new Internal_Step_On_Text(Function); }
        public static Step CreateStep(Step_On_Token Function, Filter.IFilter Filter) { return new Internal_Step_On_Token(Function); }
        public static Step CreateStep(Step_On_Lexer Function, Filter.IFilter Filter) { return new Internal_Step_On_Lexer(Function); }
        public static Step CreateStep(Step_On_File Function, Filter.IFilter Filter) { return new Internal_Step_On_File(Function); }
        public static Step CreateStep(Step_On_Project Function, Filter.IFilter Filter) { return new Internal_Step_On_Project(Function); }
    }

}

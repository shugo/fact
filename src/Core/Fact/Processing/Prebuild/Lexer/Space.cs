﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Prebuild.Lexer
{
    static public class Space
    {
        static void TrimMethod(Character.MetaLine Line)
        {

            for (int n = 0; n < Line.Length; n++)
            {
                if (Line[n].Type == Character.MetaChar.BasicCharType.Space)
                {
                    Line[n].Type = Character.MetaChar.BasicCharType.Ignored;
                }
                else
                {
                    break;
                }
            }

            for (int n = Line.Length - 1; n >= 0; n--)
            {
                if (Line[n].Type == Character.MetaChar.BasicCharType.Space)
                {
                    Line[n].Type = Character.MetaChar.BasicCharType.Ignored;
                }
                else
                {
                    break;
                }
            }
        }

        static void RemoveMultipleSpacesMethod(Character.MetaLine Line)
        {
            bool Space = false;
            for (int n = 0; n < Line.Length; n++)
            {
                if (Line[n].Type == Character.MetaChar.BasicCharType.Space)
                {
                    if (Space)
                    {
                        Line[n].Type = Character.MetaChar.BasicCharType.Ignored;
                    }
                    else
                    {
                        Space = true;
                    }
                }
                else
                {
                    Space = false;
                }
            }
        }

        class SingleLineStep : Step
        {
            string[] _Chars;
            int _Chars_Count;
            public SingleLineStep(char[] Chars)
            {
                _Chars = new string[Chars.Length];
                for (int n = 0; n < Chars.Length; n++)
                {
                    _Chars[n] = Chars[n].ToString();
                }
                _Chars_Count = _Chars.Length;
                Level = StepLevel.Line;
            }
            public override void Process(Character.MetaLine Line)
            {
                if (Line.Length > 1)
                {
                    for (int n = 0; n < Line.Length; n++)
                    {
                        if (Line[n].Type != Character.MetaChar.BasicCharType.Comment &&
                            Line[n].Type != Character.MetaChar.BasicCharType.String)
                        {
                            for (int c = 0; c < _Chars_Count; c++)
                            {
                                if (Line[n].Value == _Chars[c])
                                {
                                    if (n == Line.Length - 1)
                                        Line[n].Value = "\n" + Line[n].Value;
                                    else
                                        Line[n].Value = "\n" + Line[n].Value + "\n";
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        class NewLineAfterStep : Step
        {
            string[] _Chars;
            int _Chars_Count;
            public NewLineAfterStep(char[] Chars)
            {
                _Chars = new string[Chars.Length];
                for (int n = 0; n < Chars.Length; n++)
                {
                    _Chars[n] = Chars[n].ToString();
                }
                _Chars_Count = _Chars.Length;
                Level = StepLevel.Line;
            }
            public override void Process(Character.MetaLine Line)
            {
                if (Line.Length > 1)
                {
                    for (int n = 0; n < Line.Length; n++)
                    {
                        if (Line[n].Type != Character.MetaChar.BasicCharType.Comment &&
                            Line[n].Type != Character.MetaChar.BasicCharType.String &&
                            Line[n].Type != Character.MetaChar.BasicCharType.Preprocessor)
                        {
                            for (int c = 0; c < _Chars_Count; c++)
                            {
                                if (Line[n].Value == _Chars[c])
                                {
                                    if (n != Line.Length - 1)
                                        Line[n].Value = Line[n].Value + "\n";
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        class NewLineOnlyAfterStep : Step
        {
            string[] _Chars;
            int _Chars_Count;
            public NewLineOnlyAfterStep(char[] Chars)
            {
                _Chars = new string[Chars.Length];
                for (int n = 0; n < Chars.Length; n++)
                {
                    _Chars[n] = Chars[n].ToString();
                }
                _Chars_Count = _Chars.Length;
                Level = StepLevel.Character;
            }
            public override void Process(Character.MetaChar Char)
            {
                if (Char.Type != Character.MetaChar.BasicCharType.Comment &&
                    Char.Type != Character.MetaChar.BasicCharType.String &&
                    Char.Type != Character.MetaChar.BasicCharType.Preprocessor)
                {

                    if (Char.Value == "\n")
                    {
                        Char.Value = " ";
                        Char.Type = Character.MetaChar.BasicCharType.Space;
                    }
                    else
                    {
                        for (int c = 0; c < _Chars_Count; c++)
                        {
                            if (Char.Value == _Chars[c])
                            {
                                Char.Value = Char.Value + "\n";
                                break;
                            }
                        }
                    }
                }

            }
        }

        class ReplaceTabularStep : Step
        {
            int _Alignement = 8;
            public ReplaceTabularStep(int Alignement)
            {
                if (Alignement > 0)
                { _Alignement = Alignement; }
                else
                { _Alignement = 1; }
            }
            public override void Process(Character.MetaChar MetaChar)
            {
                if (MetaChar.Value == "\t")
                {
                    int cb = ((int)MetaChar.Column) % _Alignement;
                    if (cb == 0) { cb = _Alignement; }
                    string val = "";
                    while (cb > 0)
                    {
                        val += " ";
                        cb--;
                    }
                    MetaChar.Value = val;
                }
            }
        }


        static void TagSpacesMethod(Fact.Character.MetaChar MetaChar)
        {
            if (
                (MetaChar.Value == " " || MetaChar.Value == "\t") &&
                MetaChar.Type == Character.MetaChar.BasicCharType.None)
            { MetaChar.Type = Fact.Character.MetaChar.BasicCharType.Space; }
        }

        static void TagNewLinesMethod(Fact.Character.MetaChar MetaChar)
        {
            if (MetaChar.Value == "\n"
                && MetaChar.Type != Character.MetaChar.BasicCharType.String
                && MetaChar.Type != Character.MetaChar.BasicCharType.Comment
                && MetaChar.Type != Character.MetaChar.BasicCharType.Ignored)
            { MetaChar.Type = Fact.Character.MetaChar.BasicCharType.EndOfLine; }
        }

        static void RemoveCommentsMethods(Fact.Character.MetaChar MetaChar)
        {
            if (MetaChar.Type == Character.MetaChar.BasicCharType.Comment)
            { MetaChar.Type = Fact.Character.MetaChar.BasicCharType.Ignored; }
        }

        public static Step Trim() { return (Step.Step_On_Line)TrimMethod; }
        public static Step RemoveMultipleSpaces() { return (Step.Step_On_Line)RemoveMultipleSpacesMethod; }
        public static Step SingleLine(params char[] Chars) { return new SingleLineStep(Chars); }

        /// <summary>
        /// Add a new line after some character.
        /// DO NOT AFFECT : Preprocessor, Comment, String
        /// </summary>
        public static Step NewLineAfter(params char[] Chars) { return new NewLineAfterStep(Chars); }

        /// <summary>
        /// Add a new line after some character. If a new line already exist after
        /// a character that in not present in the character set the new
        /// line is replaced by a space already tagged as a space.
        /// DO NOT AFFECT : Preprocessor, Comment, String
        /// </summary>
        public static Step NewLineOnlyAfter(params char[] Chars) { return new NewLineOnlyAfterStep(Chars); }

        /// <summary>
        /// Tag all the spaces as space this include the tabulations
        /// </summary>
        /// <returns></returns>
        public static Step TagSpaces() { return (Step.Step_On_Char)TagSpacesMethod; }
        public static Step TagNewLines() { return (Step.Step_On_Char)TagNewLinesMethod; }
        public static Step ReplaceTabular(int Alignement) { return new ReplaceTabularStep(Alignement); }
        public static Step RemoveComments() { return (Step.Step_On_Char)RemoveCommentsMethods; }
    }
}

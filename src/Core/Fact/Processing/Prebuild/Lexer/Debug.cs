﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Prebuild.Lexer
{
    public static class Debug
    {
        static void DebugColorMethod(List<Character.MetaChar> MetaChar)
        {
            for (int n = 0; n < MetaChar.Count; n++)
            {
                if (MetaChar[n].SeparatorType != Fact.Character.MetaChar.BasicSeparatorType.None)
                {
                    if (MetaChar[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkYellow;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkMagenta;
                    }
                }
                if (MetaChar[n].Type == Character.MetaChar.BasicCharType.Preprocessor)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.Write(MetaChar[n].Value);
                }
                else if (MetaChar[n].Type == Character.MetaChar.BasicCharType.Comment)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.Write(MetaChar[n].Value);
                }
                else if (MetaChar[n].Type == Character.MetaChar.BasicCharType.EndOfLine)
                { Console.WriteLine(""); }
                else if (MetaChar[n].Type == Character.MetaChar.BasicCharType.String)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.Write(MetaChar[n].Value);
                }
                else if (MetaChar[n].TokenType == Fact.Character.MetaChar.BasicTokenType.KeyWord)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write(MetaChar[n].Value);
                }
                else if (MetaChar[n].TokenType == Fact.Character.MetaChar.BasicTokenType.Type)
                {
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.Write(MetaChar[n].Value);
                }
                else if (MetaChar[n].Type == Character.MetaChar.BasicCharType.Ignored)
                { }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(MetaChar[n].Value);
                }

                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        static public Step DebugColor() { return (Step.Step_On_Text)DebugColorMethod; }
    }
}

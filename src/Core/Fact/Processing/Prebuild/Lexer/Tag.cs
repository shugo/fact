﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Prebuild.Lexer
{
    public static class Tag
    {
        class TagAsStep : Step
        {
            Character.MetaChar[] _SourceChar;
            Character.MetaChar.BasicCharType _Type;
            public TagAsStep(Character.MetaChar[] SourceChar, Character.MetaChar.BasicCharType NewType)
            {
                _SourceChar = SourceChar;
                _Type = NewType;
            }
            public override void Process(Character.MetaChar MetaChar)
            {
                for (int n = 0; n < _SourceChar.Length; n++)
                {
                    if (MetaChar == _SourceChar[n]) { MetaChar.Type = _Type; break; }
                }
                
            }
        }

        class TagAsSeparatorStep : Step
        {
            Character.MetaChar[] _SourceChar;
            Character.MetaChar.BasicSeparatorType _Type;
            public TagAsSeparatorStep(Character.MetaChar[] SourceChar, Character.MetaChar.BasicSeparatorType NewType)
            {
                _SourceChar = SourceChar;
                _Type = NewType;
            }
            public override void Process(Character.MetaChar MetaChar)
            {
                for (int n = 0; n < _SourceChar.Length; n++)
                {
                    if (MetaChar == _SourceChar[n]) { MetaChar.SeparatorType = _Type; break; }
                }

            }
        }

        class ReTagStep : Step
        {
            Character.MetaChar.BasicCharType _SourceType;
            Character.MetaChar.BasicCharType _NewType;
            public ReTagStep(Character.MetaChar.BasicCharType SourceType, Character.MetaChar.BasicCharType NewType)
            {
                _SourceType = SourceType;
                _NewType = NewType;
            }
            public override void Process(Character.MetaChar MetaChar)
            {
                if (MetaChar.Type == _SourceType) { MetaChar.Type = _NewType; }
            }
        }

        class ReTagSeparatorStep : Step
        {
            Character.MetaChar.BasicCharType _SourceType;
            Character.MetaChar.BasicSeparatorType _NewType;
            public ReTagSeparatorStep(Character.MetaChar.BasicCharType SourceType, Character.MetaChar.BasicSeparatorType NewType)
            {
                _SourceType = SourceType;
                _NewType = NewType;
            }
            public override void Process(Character.MetaChar MetaChar)
            {
                if (MetaChar.Type == _SourceType) 
                {
                    MetaChar.Type = Character.MetaChar.BasicCharType.None;
                    MetaChar.SeparatorType = _NewType;
                }
            }
        }
        public static Step TagAs(Character.MetaChar.BasicSeparatorType Type, params Character.MetaChar[] Character)
        {
            return new TagAsSeparatorStep(Character, Type);
        }
        public static Step TagAs(Character.MetaChar.BasicCharType Type, params Character.MetaChar[] Character)
        {
            return new TagAsStep(Character, Type);
        }
        public static Step ReTag(Character.MetaChar.BasicCharType SourceType, Character.MetaChar.BasicCharType NewType)
        {
            return new ReTagStep(SourceType, NewType);
        }
        public static Step ReTag(Character.MetaChar.BasicCharType SourceType, Character.MetaChar.BasicSeparatorType NewType)
        {
            return new ReTagSeparatorStep(SourceType, NewType);
        }
    }
}

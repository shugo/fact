﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Prebuild.Lexer
{
    public static class Clike
    {
        static void TagOneLineCommentsMethod(Character.MetaLine Line) 
        {
            for (int n = 0; n < Line.Length - 1; n++)
            {
                if (Line[n].Value == "/" &&
                    Line[n + 1].Value == "/" &&
                    Line[n].Type != Character.MetaChar.BasicCharType.String &&
                    Line[n + 1].Type != Character.MetaChar.BasicCharType.String)
                {
                    for (int c = n; c < Line.Length; c++)
                    {
                        Line[c].Type = Character.MetaChar.BasicCharType.Comment;
                    }
                    return;
                }
            }
        }

        static void TagMultiLineCommentsMethod(List<Character.MetaChar> Text)
        {
            for (int n = 0; n < Text.Count - 1; n++)
            {
                if (Text[n].Value == "/" &&
                    Text[n + 1].Value == "*" &&
                    Text[n].Type != Character.MetaChar.BasicCharType.String &&
                    Text[n + 1].Type != Character.MetaChar.BasicCharType.String)
                {
                    for (; n < Text.Count; n++)
                    {
                        Text[n].Type = Character.MetaChar.BasicCharType.Comment;
                        if (Text[n].Value == "*" &&
                            Text[n + 1].Value == "/")
                        {
                            Text[n + 1].Type = Character.MetaChar.BasicCharType.Comment;
                            n++;
                            break;
                        }
                    }
                }
            }
        }

        static void TagPreprocessorMethod(List<Character.MetaChar> Text)
        {
            bool new_line = true;
            for (int n = 0; n < Text.Count; n++)
            {
                if (new_line)
                {
                    if (Text[n].Value == " " || Text[n].Value == "\t") { continue; }
                    if (Text[n].Value != "#") { new_line = false; }
                    else
                    {
                        for (; n < Text.Count; n++)
                        {
                            Text[n].Type = Character.MetaChar.BasicCharType.Preprocessor;
                            if (Text[n].Value == "\n" || Text[n].Type == Character.MetaChar.BasicCharType.EndOfLine)
                            { new_line = true; break; }
                        }
                    }
                }
                else
                {
                    if (Text[n].Type == Character.MetaChar.BasicCharType.EndOfLine ||
                        Text[n].Value == "\n") { new_line = true; }
                }
            }
        }

        public static Step TagOneLineComment() { return (Step.Step_On_Line)TagOneLineCommentsMethod; }
        public static Step TagMultiLineComment() { return (Step.Step_On_Text)TagMultiLineCommentsMethod; }

        /// <summary>
        /// Tag all the C-like preprocessor from a text.
        /// </summary>
        /// <returns></returns>
        public static Step TagPreprocessor() { return (Step.Step_On_Text)TagPreprocessorMethod; }
    }
}

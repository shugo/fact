﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Fact.Test.Result;

namespace Fact.Processing
{
    internal class RemoteContextProject_ServerSide
    {
        internal class Imposter : Project
        {
            public Imposter(string SafeHandle) : base("") { _SafeHandle = SafeHandle; }
            internal string _SafeHandle = "";
        }

        public Imposter Project { get { return _Imposter; } }

        Imposter _Imposter = null;
        Project _Project = null;
        string _SafeHandle = "";
        internal string SafeHandle { get { return _SafeHandle; } }
        public RemoteContextProject_ServerSide(Project Project)
        {
            _SafeHandle = Internal.Information.GenerateUUID();
            _Project = Project;
            _Imposter = new Imposter(_SafeHandle);
        }

        public object Do(string Command)
        {
            return null;
        }
    }
    internal class RemoteContextProject_ClientSide : Project
    {
        Context.LocalContext _LocalContext;
        string _SafeHandle;

        public object CallProjectMethod(string Method, object Parameters)
        {
            _LocalContext.sendCommand("project." + Method + "^" + _SafeHandle, Parameters);
            _LocalContext.checkMessage();
            return null;
        }

        internal RemoteContextProject_ClientSide(Context.LocalContext LocalContext, string SafeHandle, string Name) : base(Name)
        {
            _LocalContext = LocalContext;
            _SafeHandle = SafeHandle;
        }

        public override void AddFile(string Directory, File File, bool KeepPrevious)
        {
            throw new Exception("The project is read only in this context");
        }

        public override void AddTestResult(Result Result)
        {
            throw new Exception("The project is read only in this context");
        }

        public override bool RemoveDirectory(string Name)
        {
            throw new Exception("The project is read only in this context");
        }

        public override bool RemoveFile(string DirectoryName, string FileName)
        {
            throw new Exception("The project is read only in this context");
        }

        public override File GetFile(string Name)
        {
            string safe_handle = CallProjectMethod("getfile_name", Name) as string;
            return null;
        }

        public override bool Contains(string DirectoryName, string FileName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("directory", DirectoryName);
            parameters.Add("file", FileName);

            return (bool)CallProjectMethod("contains_dir_file", parameters);
        }

        public override bool Contains(string Name)
        {
            return (bool)CallProjectMethod("contains_name", Name);
        }

        public override bool Contains(File file)
        {
            return (bool)CallProjectMethod("contains_hash", file.Hash);
        }

        public override void Save(Stream stream, bool compress)
        {
            byte[] serialized = CallProjectMethod("save", compress) as byte[];
            stream.Write(serialized, 0, serialized.Length);
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing
{
    public class Chain
    {
        List<File> _Files = new List<File>();
        List<Step> _Steps = new List<Step>();

        Processing.Project _Project = null;

        public List<Step> Steps { get { return _Steps; } }

        public Chain(List<File> Files) : this(Files.ToArray()) { }
        public Chain(params File[] Files)
        {
            _Files = new List<File>(Files); 
        }

        public Chain(Project Project)
        {
            _Project = Project;
            if (Project != null)
            {
                foreach (File a_file in Project) { _Files.Add(a_file); }
            }
        }

        public void Add(Step.Step_On_Line Step) { _Steps.Add(Step); }
        public void Add(Step.Step_On_Char Step) { _Steps.Add(Step); }
        public void Add(Step.Step_On_Token Step) { _Steps.Add(Step); }
        public void Add(Step.Step_On_Lexer Step) { _Steps.Add(Step); }
        public void Add(Step.Step_On_File Step) { _Steps.Add(Step); }
        public void Add(Step.Step_On_Project Step) { _Steps.Add(Step); }
        public void Add(Step Step) { _Steps.Add(Step); }

        public void AddCheck(Check.Check_On_Line Check) { _Steps.Add((Check)Check); }
        public void AddCheck(Check.Check_On_Char Check) { _Steps.Add((Check)Check); }
        public void AddCheck(Check.Check_On_Token Check) { _Steps.Add((Check)Check); }
        public void AddCheck(Check.Check_On_Text Check) { _Steps.Add((Check)Check); }
        public void AddCheck(Check.Check_On_File Check) { _Steps.Add((Check)Check); }
        public void AddCheck(Check.Check_On_Project Check) { _Steps.Add((Check)Check); }
        public void AddMultiCheck(Check.Multi_Check_On_File Check) { _Steps.Add((Check)Check); }
        public void AddMultiCheck(Check.Multi_Check_On_Project Check) { _Steps.Add((Check)Check); }
        public void AddCheck(Check Check) { _Steps.Add(Check); }

        public void Process()
        {
            Process(null);
        }

        public void Process(List<File.FileType> FileType)
        {
            foreach (Step a_step in _Steps)
            {
                switch (a_step.Level)
                {
                    case Step.StepLevel.Character:
                        foreach (File a_file in _Files)
                        {
                            if (FileType == null || FileType.Contains(a_file.Type))
                            {
                                if (!a_file.Binary)
                                {
                                    if (a_file._MetaChar == null) { a_file._BuildMetachar(); }
                                    if (a_file._MetaChar != null)
                                    {
                                        foreach (Character.MetaChar a_char in a_file._MetaChar)
                                        { a_step.Process(a_char); }
                                        a_file.Rebuild();
                                    }
                                }
                            }
                        }
                        break;
                    case Step.StepLevel.Line:
                        foreach (File a_file in _Files)
                        {
                            if (FileType == null || FileType.Contains(a_file.Type))
                            {
                                if (!a_file.Binary)
                                {
                                    foreach (Character.MetaLine a_line in a_file.GetAsLine())
                                    { a_step.Process(a_line); }
                                    a_file.Rebuild();
                                }
                            }
                        }
                        break;
                    case Step.StepLevel.Text:
                        foreach (File a_file in _Files)
                        {
                            if (FileType == null || FileType.Contains(a_file.Type))
                            {
                                if (!a_file.Binary)
                                {
                                    a_step.Process(a_file.GetAsChar());
                                    a_file.Rebuild();
                                }
                            }
                        }
                        break;
                    case Step.StepLevel.Token:
                        foreach (File a_file in _Files)
                        {
                            if (FileType == null || FileType.Contains(a_file.Type))
                            {
                                if (!a_file.Binary)
                                {
                                    foreach (Character.MetaToken a_token in a_file.GetAsToken())
                                        a_step.Process(a_token);
                                    a_file.Rebuild();
                                }
                            }
                        }
                        break;
                    case Step.StepLevel.Lexer:
                        foreach (File a_file in _Files)
                        {
                            if (FileType == null || FileType.Contains(a_file.Type))
                            {
                                if (!a_file.Binary)
                                {
                                    Lexer.Lexer lexer = new Fact.Processing.Lexer.Lexer(a_file);
                                    a_step.Process(lexer);
                                    a_file.Rebuild();
                                }
                            }
                        }
                        break;
                    case Step.StepLevel.File:
                        foreach (File a_file in _Files)
                        {
                            if (FileType == null || FileType.Contains(a_file.Type))
                            {
                                a_step.Process(a_file);
                            }
                        }
                        break;
                    case Step.StepLevel.MultiFile:
                        foreach (File a_file in _Files)
                        {
                            if (FileType == null || FileType.Contains(a_file.Type))
                            {
                                a_step.Process(a_file);
                            }
                        }
                        break;

                    case Step.StepLevel.Project:
                        a_step.Process(_Project);
                        break;
                    case Step.StepLevel.MultiProject:
                        a_step.Process(_Project);
                        break;
                }
            }
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing
{
    /// <summary>
    /// This class can be used to store information associated to a project.
    /// </summary>
    public class Project
    {
        Dictionary<string, List<File>> _Files = new Dictionary<string, List<File>>();
        Dictionary<string, List<Project>> _SubProjects = new Dictionary<string, List<Project>>();

        HashSet<File> _StoredFiles = new HashSet<File>();
        string _ProjectName = "";
        public Project(string Name) { _ProjectName = Name; }
        Project() { _ProjectName = ""; }

        Build.Target _BuildSystem = new Build.Target("@root");
        public Build.Target BuidSystem { get { return _BuildSystem; } internal set { _BuildSystem = value; } }

        public byte[] BinaryHash
        {
            get
            {
                List<byte> array = new List<byte>();
                foreach (KeyValuePair<string, List<File>> dir in _Files)
                {
                    try
                    {
                        if (dir.Key != null && dir.Key != "")
                        {
                            array.AddRange(System.Text.Encoding.UTF8.GetBytes(dir.Key));
                        }
                        foreach (File file in dir.Value)
                        {
                            array.AddRange(file.BinaryHash);
                        }
                    }
                    catch (Exception e)
                    {
                        Fact.Log.Exception(e);
                    }
                }
                foreach (KeyValuePair<string, List<Project>> dir in _SubProjects)
                {
                    try
                    {
                        if (dir.Key != null && dir.Key != "")
                        {
                            array.AddRange(System.Text.Encoding.UTF8.GetBytes(dir.Key));
                        }
                        foreach (Project project in dir.Value)
                        {
                            array.AddRange(project.BinaryHash);
                        }
                    }
                    catch (Exception e)
                    {
                        Fact.Log.Exception(e);
                    }
                }
                System.Security.Cryptography.SHA512 sha = System.Security.Cryptography.SHA512.Create();
                byte[] hash = sha.ComputeHash(array.ToArray());
                byte[] namehash = sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_ProjectName));
                for (int n = 0; n < hash.Length; n++)
                {
                    hash[n] = (byte)(hash[n] ^ namehash[n]);
                }
                return hash;
            }
        }

        public string Hash
        {
            get
            {
                byte[] hash = BinaryHash;
                string result = "";
                for (int n = 0; n < hash.Length; n++)
                {
                    result += hash[n].ToString("X2");
                }

                return result;
            }
        }

        /// <summary>
        /// Get the name of the project
        /// </summary>
        public virtual string Name { get { return _ProjectName; } }

        public virtual bool Contains(File file)
        {
            return _StoredFiles.Contains(file);
        }

        public virtual bool Empty 
        {
            get 
            {
                foreach (List<File> f in _Files.Values)
                {
                    if (f.Count > 0) { return false; }
                }
                foreach (List<Project> p in _SubProjects.Values)
                {
                    if (p.Count > 0) { return false; }
                }
                return true;
            }
        }

        Test.Result.Group _TestResult = new Fact.Test.Result.Group("");
        //List<Test.Result.Result> _TestResult = new List<Fact.Test.Result.Result>();

        public virtual List<Test.Result.Result> GetTestResults()
        { return _TestResult._results; }

        /// <summary>
        /// Get a group according to its name (store in the text field)
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public virtual Test.Result.Group GetTestGroup(string Name)
        {
            foreach (Test.Result.Result _ in _TestResult._results)
            {
                if (_ is Test.Result.Group)
                {
                    if ((_ as Test.Result.Group).Text == Name)
                        return _ as Test.Result.Group;
                }
            }
            return null;
        }

        /// <summary>
        /// Add a test result to the project. The test result is stored in a global
        /// project nide?
        /// </summary>
        /// <param name="Result">The test that must be added</param>
        public virtual void AddTestResult(Test.Result.Result Result)
        {
            _TestResult.AddTestResult(Result);
        }

        /// <summary>
        /// Save the project into the specified fact file.
        /// </summary>
        /// <param name="file">the target fact File.</param>
        public void Save(File file)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream(); 
            Save(stream, false);
            file.Type = File.FileType.FactPackage;
            file._RawData = stream.ToArray();
            stream.Close();
        }

        /// <summary>
        /// Save the project into the specified fact file.
        /// </summary>
        /// <param name="file">the target fact File.</param>
        /// <param name="Compress">A flag inficating if the file must be compressed or not.</param>
        public void Save(File file, bool Compress)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            Save(stream, Compress);
            file.Type = File.FileType.FactPackage;
            file._RawData = stream.ToArray();
            stream.Close();
        }

        /// <summary>
        /// Save the project inside a file.
        /// </summary>
        /// <param name="file">The output file where the project will be saved.</param>
        public void Save(string file)
        {
            Save(file, false);
        }

        /// <summary>
        /// Save the project inside a file.
        /// </summary>
        /// <param name="file">The output file where the project will be saved.</param>
        public void Save(string file, bool compress)
        {
            try
            {
                string dir = System.IO.Path.GetDirectoryName(file);
                if (!System.IO.Directory.Exists(dir))
                {
                    Tools.RecursiveMakeDirectory(dir);
                }
            }
            catch
            { }
            try
            {
                if (System.IO.File.Exists(file))
                {
                    Tools.RecursiveDelete(file);
                }
            }
            catch { }

            try
            {
                System.IO.Stream stream = System.IO.File.Open(file, System.IO.FileMode.OpenOrCreate);
                Save(stream, compress);
                try { stream.Close(); } catch { }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Save the project inside the specified stream.
        /// </summary>
        /// <param name="stream">The stream that must be used to save the project.</param>
        public void Save(System.IO.Stream stream)
        {
            Save(stream, false);
        }

        /// <summary>
        /// Save the project inside the specified stream.
        /// </summary>
        /// <param name="stream">The stream that must be used to save the project.</param>
        /// <param name="compress">Indicate that we must compress the file.</param>
        public virtual void Save(System.IO.Stream stream, bool compress)
        {
#if DEBUG
            Fact.Log.Debug("Save fact package '" + _ProjectName + "'");
#endif
            if (compress)
            {
                Internal.StreamTools.WriteUInt32(stream, 0xFAC70002);
                stream.Flush();
                System.IO.Compression.GZipStream gzip = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Compress);
                stream = gzip;
            }
            else
            {
                Internal.StreamTools.WriteUInt32(stream, 0xFAC70001);
            }
            Internal.StreamTools.WriteUTF8String(stream, _ProjectName);
            System.IO.MemoryStream rawtests = new System.IO.MemoryStream();
#if DEBUG
            Fact.Log.Debug("Save testresults in '" + _ProjectName + "'");
#endif
            _TestResult.Save(rawtests);
            Internal.Security.SnowfallCipher cipher = new Internal.Security.SnowfallCipher("fact-project");
            Internal.StreamTools.WriteByteArray(stream, cipher.Encrypt(rawtests.ToArray()));
            _BuildSystem.Save(stream);

            Internal.StreamTools.WriteInt32(stream, _Files.Count);
            foreach (string key in _Files.Keys)
            {
#if DEBUG
                Fact.Log.Debug("Save directory '" + key + "' '" + _ProjectName + "'");
#endif
                Internal.StreamTools.WriteUTF8String(stream, key);
                List<File> list = _Files[key];
                Internal.StreamTools.WriteInt32(stream, list.Count);
                foreach (File file in list)
                {
                    file.Save(stream);
                }
            }
            if (compress)
            {
                stream.Flush();
                stream.Close();
            }
#if DEBUG
            Fact.Log.Debug("Packaged '" + _ProjectName + "' saved correctly");
#endif
        }

        public static Fact.Processing.Project Load(File file)
        {
            
            System.IO.MemoryStream stream = new System.IO.MemoryStream(file.GetBytes());
            Fact.Processing.Project project = Load(stream);
            stream.Close();
            return project;
        }

        /// <summary>
        /// Load a project from a file or a directory.
        /// </summary>
        /// <param name="file">The file or the directory that contains the project</param>
        public static Fact.Processing.Project Load(string file)
        {
            if (System.IO.Directory.Exists(file))
            {
                if (!ProjectInFileSystem.IsPhysicalDirectoryInProject(file))
                {
                    throw new Exception("The directory " + file + " is not in a fact project");
                }
                return new ProjectInFileSystem(file, "");
            }
            else
            {
                System.IO.Stream stream = System.IO.File.Open(file, System.IO.FileMode.OpenOrCreate);
                Fact.Processing.Project project = Load(stream);
                stream.Close();
                return project;
            }
        }

        /// <summary>
        /// Load a project from a stream. The data inside the current project are simply erased.
        /// </summary>
        /// <param name="stream">the stream that holds the data.</param>
        public static Fact.Processing.Project Load(System.IO.Stream stream)
        {
            Fact.Processing.Project project = new Project();
            UInt32 MagickNumber = Internal.StreamTools.ReadUInt32(stream);
            if (MagickNumber == 0xFAC70002 || MagickNumber == 0x0200C7FA)
            {
                System.IO.Compression.GZipStream Gzip =
                    new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Decompress);
                stream = Gzip;
            }
            else if (MagickNumber != 0xFAC70001 && MagickNumber != 0x0100C7FA)
            {
                // This is not a fact archive let's try other common archive format
                if (MagickNumber == 0x213C6172 || MagickNumber == 0x72613C21)
                {
                    MagickNumber = Internal.StreamTools.ReadUInt32(stream);
                    if (MagickNumber == 0x213C6172 || MagickNumber == 0x72613C21)
                    {
                    }
                }
                // Invalid Magick Number
                throw new Exception("Package format not supported");
            }

            project._ProjectName = Internal.StreamTools.ReadUTF8String(stream);
            byte[] encryptedTests = Internal.StreamTools.ReadByteArray(stream);
            Internal.Security.SnowfallCipher cipher = new Internal.Security.SnowfallCipher("fact-project");
            System.IO.MemoryStream encryptedstream = new System.IO.MemoryStream(cipher.Decrypt(encryptedTests));
            project._TestResult = Test.Result.Result.Load(encryptedstream) as Test.Result.Group;
            project._BuildSystem = Build.Target.Load(stream);
            int keyCount = Internal.StreamTools.ReadInt32(stream);
            if (keyCount < 0) { throw new Exception("The package '" + project._ProjectName + "' is corrupted"); }
            while (keyCount > 0)
            {
                string key = Internal.StreamTools.ReadUTF8String(stream);
                project._Files[key] = new List<File>();
                int fileCount = Internal.StreamTools.ReadInt32(stream);
                if (fileCount < 0) { throw new Exception("The package '" + project._ProjectName + "' is corrupted"); }
                while (fileCount > 0)
                {
                    File file = new File((byte[])null);
                    file.Load(stream);
                    file._Directory = key;
                    file.Project = project;
                    project._Files[key].Add(file);
                    project._StoredFiles.Add(file);
                    fileCount--;
                }
                keyCount--;
            }
            return project;
        }

        public virtual bool Contains(string DirectoryName, string FileName)
        {
            DirectoryName = FormatPath(DirectoryName);
            while (DirectoryName.EndsWith("/"))
            { DirectoryName = DirectoryName.Substring(0, DirectoryName.Length - 1); }
            while (DirectoryName.StartsWith("/"))
            { DirectoryName = DirectoryName.Substring(1); }
            if (_Files.ContainsKey(DirectoryName))
            {
                foreach (File file in _Files[DirectoryName])
                { if (file.Name == FileName) { return true; } }
            }
            if (_Files.ContainsKey("/" + DirectoryName))
            {
                foreach (File file in _Files["/" + DirectoryName])
                { if (file.Name == FileName) { return true; } }
            }
            return false;
        }

		/// <summary>
		/// Removes the file.
		/// </summary>
		/// <returns><c>true</c>, if file was removed, <c>false</c> otherwise.</returns>
		/// <param name="DirectoryName">Directory name.</param>
		/// <param name="FileName">File name.</param>
        public virtual bool RemoveFile(string DirectoryName, string FileName)
		{
            DirectoryName = FormatPath(DirectoryName);
			while (DirectoryName.EndsWith("/"))
			{ DirectoryName = DirectoryName.Substring(0, DirectoryName.Length - 1); }
			while (DirectoryName.StartsWith("/"))
			{ DirectoryName = DirectoryName.Substring(1); }
			if (_Files.ContainsKey(DirectoryName))
			{

				for (int n = 0; n < _Files[DirectoryName].Count; n++) 
				{
					if (_Files[DirectoryName][n].Name == FileName) 
					{ 
						_Files [DirectoryName].RemoveAt (n);
						return true;
					}
				}
			}
			if (_Files.ContainsKey("/" + DirectoryName))
			{
				for (int n = 0; n < _Files["/" + DirectoryName].Count; n++) 
				{
					if (_Files[DirectoryName][n].Name == FileName) 
					{ 
						_Files [DirectoryName].RemoveAt (n);
						return true;
					}
				}
			}
			return false;
        }

		/// <summary>
		/// Removes the specified file.
		/// </summary>
		/// <returns><c>true</c>, if file was removed, <c>false</c> otherwise.</returns>
		/// <param name="Name">Name.</param>
        public virtual bool RemoveDirectory(string Name)
		{
			try
			{
				foreach (string _ in GetSubDirectories(Name, true))
				{
					try
					{
						for (int n = 0; n < _Files[_].Count; n++)
						{
							File __  = _Files[_][n];
							string name = __.Directory + "/" + __.Name;
							RemoveFile(name);
						}
						_Files.Remove(_);

					}
					catch (Exception e)
					{
						Fact.Log.Exception(e);
					}
				}
			}
			catch 
			{
			}

			try
			{
				for (int n = 0; n < _Files[Name].Count; n++)
				{
					File __  = _Files[Name][n];
					string name = __.Directory + "/" + __.Name;
					RemoveFile(name);
				}
			}
			catch
			{
			}

			try
			{
				_Files.Remove(Name);
			}
			catch
			{
			}

			return true;
		}

		/// <summary>
		/// Removes the specified file.
		/// </summary>
		/// <returns><c>true</c>, if file was removed, <c>false</c> otherwise.</returns>
		/// <param name="Name">Name.</param>
        public bool RemoveFile(string Name)
		{
			try
			{
				string fileName = System.IO.Path.GetFileName(Name);
			

				if (fileName.Length == Name.Length) { return RemoveFile("", Name); }
				string directory = Name.Substring(0, Name.Length - fileName.Length);
				return RemoveFile(directory, fileName);
			}
			catch 
			{
				try
				{
					return RemoveFile("", Name);
				}
				catch 
				{
				}
			}
			return false;
		}

		/// <summary>
		/// Gets the file.
		/// </summary>
		/// <returns>The file.</returns>
		/// <param name="DirectoryName">Directory name.</param>
		/// <param name="FileName">File name.</param>
        public virtual File GetFile(string DirectoryName, string FileName)
        {

            DirectoryName = FormatPath(DirectoryName);
            while (DirectoryName.EndsWith("/"))
            { DirectoryName = DirectoryName.Substring(0, DirectoryName.Length - 1); }
            while (DirectoryName.StartsWith("/"))
            { DirectoryName = DirectoryName.Substring(1); }
            if (_Files.ContainsKey(DirectoryName))
            {
                foreach (File file in _Files[DirectoryName])
                { if (file.Name == FileName) { return file; } }
            }
            if (_Files.ContainsKey("/" + DirectoryName))
            {
                foreach (File file in _Files["/" + DirectoryName])
                { if (file.Name == FileName) { return file; } }
            }
            return null;
        }

        bool ContainType(File.FileType Type, params File.FileType[] FileType)
        {
            if (FileType == null || FileType.Length == 0) { return true; }
            foreach (File.FileType a_type in FileType)
            { if (a_type == Type) { return true; } }
            return false;
        }

        /// <summary>
        /// Get all the file in the package that match the specified pattern.
        /// </summary>
        /// <param name="Pattern">The patern to match files</param>
        /// <returns></returns>
        public virtual List<File> GetFiles(string Pattern)
        {
            List<File> files = new List<File>();
            System.Text.RegularExpressions.Regex regex =
                new System.Text.RegularExpressions.Regex(Pattern);
            
            foreach (string key in _Files.Keys)
            {
           
                foreach (File file in _Files[key])
                {
                    string dir = key;
                    if (dir.StartsWith("/")) { dir = dir.Substring(1); }
                    if (!key.EndsWith("/")) { dir += "/"; }
                    dir += file.Name;

                    if (regex.Match(dir).Success)
                    {
                        files.Add(file);
                    }
                }
            }
            return files;
        }

        /// <summary>
        /// Get all the file in the package that match the specified requierements.
        /// </summary>
        /// <param name="DirectoryName"></param>
        /// <param name="Recursive">Indicate if the matching must be done recursively.s</param>
        /// <param name="FileType">All the file returned by the fuction will match the provided type.
        /// If no type are specified all the files will be returned.</param>
        /// <returns></returns>
        public virtual List<File> GetFiles(string DirectoryName, bool Recursive, params File.FileType[] FileType)
        {
            DirectoryName = FormatPath(DirectoryName);
            while (DirectoryName.EndsWith("/"))
            { DirectoryName = DirectoryName.Substring(0, DirectoryName.Length - 1); }
            while (DirectoryName.StartsWith("/"))
            { DirectoryName = DirectoryName.Substring(1); }
            List<File> Result = new List<File>();
            if (_Files.ContainsKey(DirectoryName))
            {
                foreach (File file in _Files[DirectoryName])
                { if (ContainType(file.Type, FileType)) { Result.Add(file); } }
            }
            if (_Files.ContainsKey("/" + DirectoryName))
            {
                foreach (File file in _Files["/" + DirectoryName])
                { if (ContainType(file.Type, FileType)) { Result.Add(file); } }
            }
            if (Recursive)
            {
                foreach (string dir in _Files.Keys)
                {
                    if (DirectoryName == "/" || DirectoryName == "")
                    {
                        if (dir != "" && dir != "/" && dir != "./")
                        {
                            foreach (File file in _Files[dir])
                            { if (ContainType(file.Type, FileType)) { Result.Add(file); } }
                        }
                    }
                    else
                    {
                        if (dir != DirectoryName &&
                            dir != "/" + DirectoryName &&
                            (dir.StartsWith(DirectoryName + "/") || dir.StartsWith("/" + DirectoryName + "/")))
                        {
                            foreach (File file in _Files[dir])
                            { if (ContainType(file.Type, FileType)) { Result.Add(file); } }
                        }
                    }
                }
            }
            return Result;
        }

        /// <summary>
        /// Enumerate recursively or not all the directory located in the specified
        /// directory.
        /// </summary>
        /// <param name="DirectoryName">The source directory here the request is performed</param>
        /// <param name="Recursive">Indicate if the request must be done recursively or not</param>
        /// <returns></returns>
        public virtual IEnumerable<string> GetSubDirectories(string DirectoryName, bool Recursive)
        {
            DirectoryName = FormatPath(DirectoryName);
            while (DirectoryName.EndsWith("/"))
            { DirectoryName = DirectoryName.Substring(0, DirectoryName.Length - 1); }
            while (DirectoryName.StartsWith("/"))
            { DirectoryName = DirectoryName.Substring(1); }

            List<string> result = new List<string>();
            List<string> hidden = new List<string>();
            if (DirectoryName == "" ||
                DirectoryName == "/")
            {
                    foreach (string directory in _Files.Keys)
                    {
                        result.Add(directory);
                        if (!Recursive)
                        {
                            hidden.AddRange(GetSubDirectories(directory, true));
                        }
                    }
                if (!Recursive)
                {
                    return result.Except(hidden);
                }
                return result;
            }
            else
            {
                if (!DirectoryName.EndsWith("/"))
                {
                    DirectoryName += "/";
                }


                foreach (string directory in _Files.Keys)
                {
				
                    if (directory == DirectoryName ||
                        directory == "/" + DirectoryName ||
                        directory + "/" == DirectoryName)
                    { continue; }
                    else
                    {
                        if (directory.StartsWith(DirectoryName))
                        {

                            result.Add(directory);
                            if (!Recursive)
                            {
                                hidden.AddRange(GetSubDirectories(directory, true));
                            }
                        }
                    }
                }
                if (!Recursive)
                {
                    return result.Except(hidden);
                }
                return result;
            }
        }

        /// <summary>
        /// Extracts the files.
        /// </summary>
        /// <returns>The files.</returns>
        /// <param name="Pattern">Pattern.</param>
        /// <param name="Where">Location where the files are extracted.</param>
        public virtual List<string> ExtractFiles(string Pattern, string Where)
        {
            List<string> files = new List<string>();
            System.Text.RegularExpressions.Regex regex =
                new System.Text.RegularExpressions.Regex(Pattern);
            foreach (string key in _Files.Keys)
            {
                foreach (File file in _Files[key])
                {
                    string dir = key;
                    string path = "";
                    if (dir.StartsWith("/")) { dir = dir.Substring(1); }
                    if (!key.EndsWith("/")) { dir += "/"; }
                    path = dir + file.Name;

                    if (regex.Match(path).Success ||
                        regex.Match("/" + path).Success)
                    {
                        files.Add(path);
                        string realdirectory = Where + "/" + dir;
                        Tools.RecursiveMakeDirectory(realdirectory);
                        if (file.Binary)
                            System.IO.File.WriteAllBytes(realdirectory + file.Name, file.GetBytes());
                        else
                            System.IO.File.WriteAllLines(realdirectory + file.Name, file.GetLines(false).ToArray());
                    }
                }
            }
            return files;
        }

        public virtual List<string> ExtractDirectory(string DirectoryName, string Where, params File.FileType[] FileType)
        {
            DirectoryName = FormatPath(DirectoryName);
            while (DirectoryName.EndsWith("/"))
            { DirectoryName = DirectoryName.Substring(0, DirectoryName.Length - 1); }
            while (DirectoryName.StartsWith("/"))
            { DirectoryName = DirectoryName.Substring(1); }
            List<string> extracted = new List<string>();
            Tools.RecursiveMakeDirectory(Where);

            if (DirectoryName == "" || DirectoryName == ".")
            {
                foreach (string dir in _Files.Keys)
                {
                    Tools.RecursiveMakeDirectory(Where + "/" + dir);
                    foreach (File file in _Files[dir])
                    {
                        if (ContainType(file.Type, FileType))
                        {
                            if (file.Binary)
                                System.IO.File.WriteAllBytes(Where + "/" + dir + "/" + file.Name, file.GetBytes());
                            else
                                System.IO.File.WriteAllLines(Where + "/" + dir + "/" + file.Name, file.GetLines(false).ToArray());
                            extracted.Add(Where + "/" + file.Name);
                        }
                    }
                }
                return extracted;
            }

            if (_Files.ContainsKey(DirectoryName))
            {
                foreach (File file in _Files[DirectoryName])
                {
                    if (ContainType(file.Type, FileType))
                    {
                        if (file.Binary)
                            System.IO.File.WriteAllBytes(Where + "/" + file.Name, file.GetBytes());
                        else
                            System.IO.File.WriteAllLines(Where + "/" + file.Name, file.GetLines(false).ToArray());
                        extracted.Add(Where + "/" + file.Name);
                    }
                }
            }
            if (_Files.ContainsKey("/" + DirectoryName))
            {
                foreach (File file in _Files["/" + DirectoryName])
                { 
                    if (ContainType(file.Type, FileType)) 
                    {
                        if (file.Binary)
                            System.IO.File.WriteAllBytes(Where + "/" + file.Name, file.GetBytes());
                        else
                            System.IO.File.WriteAllLines(Where + "/" + file.Name, file.GetLines(false).ToArray());
                        extracted.Add(Where + "/" + file.Name);
                    } 
                }
            }

            while (DirectoryName.EndsWith("/"))
            { DirectoryName = DirectoryName.Substring(0, DirectoryName.Length - 1); }

            foreach (string dir in _Files.Keys)
            {
                if (dir != DirectoryName &&
                    dir != "/" + DirectoryName &&
                    (dir.StartsWith(DirectoryName + "/") || dir.StartsWith("/" + DirectoryName + "/")))
                {
                    string extract = "";
                    if (dir.StartsWith(DirectoryName + "/"))
                    {
                        extract = Where + "/" + dir.Substring(DirectoryName.Length + 1);
                    }
                    else if (dir.StartsWith("/" + DirectoryName + "/"))
                    {
                        extract = Where + "/" + dir.Substring(DirectoryName.Length + 2);
                    }

                    Tools.RecursiveMakeDirectory(extract);

                    foreach (File file in _Files[dir])
                    {
                        if (ContainType(file.Type, FileType)) 
                        {
                            if (file.Binary)
                                System.IO.File.WriteAllBytes(extract + "/" + file.Name, file.GetBytes());
                            else
                                System.IO.File.WriteAllLines(extract + "/" + file.Name, file.GetLines(false).ToArray());
                            extracted.Add(extract + "/" + file.Name);
                        } 
                    }
                }
            }

            foreach (string dir in _SubProjects.Keys)
            {
                if (dir != DirectoryName &&
                    dir != "/" + DirectoryName &&
                    (dir.StartsWith(DirectoryName + "/") || dir.StartsWith("/" + DirectoryName + "/")))
                {
                    string extract = "";
                    if (dir.StartsWith(DirectoryName + "/"))
                    {
                        extract = Where + "/" + dir.Substring(DirectoryName.Length + 1);
                    }
                    else if (dir.StartsWith("/" + DirectoryName + "/"))
                    {
                        extract = Where + "/" + dir.Substring(DirectoryName.Length + 2);
                    }

                    Tools.RecursiveMakeDirectory(extract);

                    foreach (Project project in _SubProjects[dir])
                    {
                        project.ExtractDirectory("", extract + "/" + project.Name, FileType);
                    }
                }
            }
            return extracted;
        }

        public virtual bool Contains(string Name)
        {
            try
            {
                if (_Files.ContainsKey(Name)) { return true; }
                string fileName = System.IO.Path.GetFileName(Name);
                if (fileName.Length == Name.Length) { return Contains("", Name); }
                string directory = Name.Substring(0, Name.Length - fileName.Length);
                return Contains(directory, fileName);
            }
            catch { }
            return false;
        }

        public virtual File GetFile(string Name)
        {
            try
            {
                string fileName = System.IO.Path.GetFileName(Name);
                if (fileName.Length == Name.Length) { return GetFile("", Name); }
                string directory = Name.Substring(0, Name.Length - fileName.Length);
                return GetFile(directory, fileName);
            }
            catch 
            {
                try
                {
                    return GetFile("", Name);
                }
                catch 
                {
                }
            }
            return null;
        }

        public void AddFile(File File)
        {
            AddFile("", File, false);
        }

	    public void AddFile(File File, bool KeepPrevious)
        {
            AddFile("", File, KeepPrevious);
        }

        public void AddFile(string Directory, File File)
	    {
		    AddFile(Directory, File, false);
	    }

        public virtual void AddFile(string Directory, File File, bool KeepPrevious)
        {
            if (File == null) { return; }
#if DEBUG
            Fact.Log.Debug("Adding file '" + File.Name + "' to package '" + _ProjectName + "' in directory '" + Directory + "'");
#endif
            Directory = FormatPath(Directory);
            File.Project = this;
            while (Directory.StartsWith("/"))
            { Directory = Directory.Substring(1); }
            while (Directory.EndsWith("/"))
            { Directory = Directory.Substring(0, Directory.Length - 1); }
            if (!_Files.ContainsKey(Directory)) { _Files.Add(Directory, new List<File>()); }
            else
            {
                if (_SubProjects.ContainsKey(Directory))
                {
                    foreach (Fact.Processing.Project project in _SubProjects[Directory])
                    {
                        if (project.Name == File.Name) { throw new Exception("The file conflict with a sub project name"); }
                    }
                }
            }
            List<File> dir = _Files[Directory];
            _StoredFiles.Add(File);
            for (int n = 0; n < dir.Count; n++)
            {
                if (dir[n].Name == File.Name)
                {
		    if (KeepPrevious)
		       return;
                    // FIXME A REFCOUNT SHOULD BE ADDED ON STORED FILE
                    try { _StoredFiles.Remove(dir[n]); }
                    catch { }
                    dir[n] = File;
                    return;
                }
            }
            _Files[Directory].Add(File);
            if (File.Directory == "")
                File._Directory = Directory;
        }

        /// <summary>
        /// Add a real file into a fact package.
        /// </summary>
        /// <param name="Directory">The directory in the fact package that will hold the new file</param>
        /// <param name="RealPath">The path of the real file</param>
        public void AddRealFile(string Directory, string RealPath)
	    {
	       AddRealFile(Directory, RealPath, false);
	    }

        public void AddRealFile(string Directory, string RealPath, bool KeepPrevious)
        {
            if (System.IO.File.Exists(RealPath))
            {
#if DEBUG
                Fact.Log.Debug("Adding real file file '" + RealPath + "' to package '" + _ProjectName + "'");
#endif
                string name = System.IO.Path.GetFileName(RealPath);
                string ext = System.IO.Path.GetExtension(RealPath);
                File file = null;
                File.FileType filetype = Internal.File.GetFileType(RealPath);
                if (Internal.File.IsTextFile(filetype))
                {
                    file = new File(System.IO.File.ReadAllLines(RealPath), name);
                }
                else
                {
                    file = new File(System.IO.File.ReadAllBytes(RealPath), name);
                }
                file.Type = filetype;
                AddFile(Directory, file, KeepPrevious);
            }
        }

	    public void AddRealDirectory(string Directory, string RealDirectory)
        {
	        AddRealDirectory(Directory, RealDirectory, false);
	    }

        public void AddRealDirectory(string Directory, string RealDirectory, bool KeepPrevious)
        {
            if (System.IO.Directory.Exists(RealDirectory))
            {
                foreach (string dir in System.IO.Directory.GetDirectories(RealDirectory))
                {
                    string lastFolder = System.IO.Path.GetFileName(dir);
                    AddRealDirectory(Directory + "/" + lastFolder, dir, KeepPrevious);
                }
                foreach (string file in System.IO.Directory.GetFiles(RealDirectory))
                {
                    try
                    {
                        AddRealFile(Directory, file, KeepPrevious);
                    }
                    catch { Fact.Log.Error("Impossible to add file " + file); }
                }
            }
        }

        public void Merge(Project ProjectB)
        {
            foreach (KeyValuePair<string, List<File>> directory in ProjectB._Files)
            {
                if (!_Files.ContainsKey(directory.Key))
                {
                    List<File> clone = new List<File>();
                    foreach (File file in directory.Value)
                    {
                        clone.Add(file.Copy());
                    }
                    _Files.Add(directory.Key, directory.Value);
                }
                else
                {
                    foreach (File fileB in directory.Value)
                    {
                        bool mustAdd = true;
                        foreach (File fileA in _Files[directory.Key])
                        {
                            if (fileB.Name == fileA.Name)
                            {
                                mustAdd = false;
                                fileA._TestResult.Merge(fileB._TestResult);
                            }
                        }
                        if (mustAdd)
                        {
                            _Files[directory.Key].Add(fileB.Copy());
                        }
                    }
                }
            }

            _TestResult.Merge(ProjectB._TestResult);
        }

        /// <summary>
        /// Remove data from the package
        /// </summary>
        /// <param name="KeepTests">Information related to the tests must be stored</param>
        /// <param name="KeepFiles">Information related to the files must be stored</param>
        public void Purge(bool KeepTests, bool KeepFiles)
        {
            if (KeepFiles && KeepFiles) { return; }
            if (!KeepFiles && !KeepTests)
            {
                _TestResult = new Test.Result.Group("");
                _Files = new Dictionary<string, List<File>>();
                _StoredFiles = new HashSet<File>();
                return;
            }
            if (KeepFiles)
            {
                _TestResult = new Test.Result.Group("");
                foreach (KeyValuePair<string, List<File>> directory in _Files)
                {
                    foreach (File file in directory.Value)
                    {
                        file._TestResult = new Test.Result.Group("");
                    }
                }
                return;
            }
            if (KeepTests)
            {
                List<string> directoryToRemove = new List<string>();
                foreach (KeyValuePair<string, List<File>> directory in _Files)
                {
                    for (int n = 0; n < directory.Value.Count; n++)
                    {
                        if (directory.Value[n]._TestResult._results.Count == 0)
                        {
                            if (_StoredFiles.Contains(directory.Value[n]))
                            {
                                _StoredFiles.Remove(directory.Value[n]);
                            }
                            directory.Value.RemoveAt(n);
                            n--;
                        }
                        else
                        {
                            directory.Value[n]._RawData = new byte[0];
                        }
                    }
                    if (directory.Value.Count == 0) { directoryToRemove.Add(directory.Key); }
                }
                foreach (string dir in directoryToRemove)
                {
                    _Files.Remove(dir);
                }
                return;
            }
        }

        public void Update()
        {

        }

        public virtual IEnumerator<File> GetEnumerator()
        {
            foreach (List<File> directory in _Files.Values)
            {
                foreach (File file in directory)
                {
                    yield return file;
                }
            }
        }

        static string FormatPath(string Path)
        {
            StringBuilder output = new StringBuilder();
            bool slash = false;
            for (int n = 0; n < Path.Length; n++)
            {
                if (slash && Path[n] == '/') { continue; }
                slash = false;
                if (Path[n] == '/' || Path[n] == '\\') { slash = true; output.Append('/'); }
                else { output.Append(Path[n]); }
            }
            return output.ToString();
        }
    }

}

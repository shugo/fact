﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing
{
    public class File : IDisposable
    {
        public enum ExecutableFormat
        {
            None,

            AdotOut,
            CommonObjectFileFormat,
            MachObject,
            Com,
            PortableExecutable,
            ExecutableAndLinkableFormat,
            JavaClass,
            Python,
            PythonBytecode,
        }

        public enum FileType
        {
            None,

            Executable,
            StaticLibrary,
            DynamicLibrary,

            Binary,

            PDF,
            XPF,

            JavaPackage,
            JavaClass,

            Image,

            Sound,

            Text,
            Latex,

            Code,
            CHeader,
            CSource,
            CPPHeader,
            CPPSource,
            CSSource,
            JavaSource,
            LuaSource,
            PerlSource,
            PythonSource,
            ShSource,
			GdbScript,

            PythonBytecode,

            Trash,

            SedFile,
            GrepFile,

            Todo,
            Readme,
            ChangeLog,
            Gitignore,

            Makefile,
            CMakeLists,
            Configure,
            Authors,
            MicrosoftSolution,
            Ant,
            Maven,

            FactPackage,
            FactDirectory,
            FactCredential,

			Object,

            XML,
        }

        Project _Project = null;
        List<Code.Element> _Elements = new List<Code.Element>();

        public List<Code.Element> CodeElements { get { return _Elements; } }


		Permissions _Permissions = new Permissions();

        public ulong Size
        {
            get 
            {
                if (_RawData != null) { return (ulong)_RawData.Length; }
                else if (_MetaChar != null) { return (ulong)_MetaChar.Length; }
                return 0;
            }
        }

        public string Hash
        {
            get
            {
                byte[] hash = BinaryHash;
                string result = "";
                for (int n = 0; n < hash.Length; n++)
                {
                    result += hash[n].ToString("X2");
                }
                
                return result;
            }
        }

        public byte[] BinaryHash
        {
            get
            {
                try
                {
                    System.Security.Cryptography.SHA512 sha = System.Security.Cryptography.SHA512.Create();
                    byte[] data = null;
                    if (Binary) { data = GetBytes(); }
                    else
                    {
                        string text = GetText(false);
                        if (text != "")
                        {
                            data = System.Text.Encoding.UTF8.GetBytes(text);
                        }
                    }
                    if (data != null && data.Length != 0)
                    {
                        byte[] array = sha.ComputeHash(data);
                        byte[] namearray = sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_FileName));
                        for (int n = 0; n < array.Length; n++)
                        {
                            array[n] = (byte)(array[n] ^ namearray[n]);
                        }
                        return array;
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Exception(e);
                }
                byte[] emptyhash = new byte[64];
                for (int n = 0; n < emptyhash.Length; n++)
                {
                    emptyhash[n] = 0;
                }
                return emptyhash;
            }
        }

        public byte[] GitBlob
        {
            get
            {
                try
                {
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    byte[] data = null;
                    data = GetBytes();
                    byte[] text = System.Text.Encoding.UTF8.GetBytes("blob " + data.Length.ToString().Trim());
                    stream.Write(text, 0, text.Length);
                    stream.WriteByte(0);
                    stream.Write(data, 0, data.Length);
                    return stream.ToArray();
                }
                catch (Exception e)
                {
#if DEBUG
                    Fact.Log.Exception(e);
#endif
                    throw e;
                }
            }
        }

        public byte[] GitBinaryHash
        {
            get
            {
                try
                {
                    System.Security.Cryptography.SHA1 sha = System.Security.Cryptography.SHA1.Create();
                    return sha.ComputeHash(GitBlob);
                }
                catch (Exception e)
                {
#if DEBUG
                    Fact.Log.Exception(e);
#endif
                }
                byte[] emptyhash = new byte[64];
                for (int n = 0; n < emptyhash.Length; n++)
                {
                    emptyhash[n] = 0;
                }
                return emptyhash;
            }
        }

        public string GitHash
        {
            get
            {
                byte[] hash = GitBinaryHash;
                string result = "";
                for (int n = 0; n < hash.Length; n++)
                {
                    result += hash[n].ToString("X2");
                }

                return result.ToLower().Trim();
            }
        }

        public string ContentHash
        {
            get
            {
                byte[] hash = ContentBinaryHash;
                string result = "";
                for (int n = 0; n < hash.Length; n++)
                {
                    result += hash[n].ToString("X2");
                }

                return result;
            }
        }

        public byte[] ContentBinaryHash
        {
            get
            {
                try
                {
                    System.Security.Cryptography.SHA512 sha = System.Security.Cryptography.SHA512.Create();
                    byte[] data = null;
                    if (Binary) { data = GetBytes(); }
                    else
                    {
                        string text = GetText(false);
                        if (text != "")
                        {
                            data = System.Text.Encoding.UTF8.GetBytes(text);
                        }
                    }
                    if (data != null && data.Length != 0)
                    {
                        byte[] array = sha.ComputeHash(data);
                        return array;
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Exception(e);
                }
                byte[] emptyhash = new byte[64];
                for (int n = 0; n < emptyhash.Length; n++)
                {
                    emptyhash[n] = 0;
                }
                return emptyhash;
            }
        }

		public Permissions FilePermissions 
		{
			get { return _Permissions; }
			set { _Permissions = value; }
		}
		public class Permissions
		{
			bool _Execution = false;
			bool _Read = true;
			bool _Write = true;
			bool _Auto = true;

			public Permissions(bool Execution,
			                  bool Read,
		   	                   bool Write)
			{ 
				_Execution = Execution;
				_Read = Read;
				_Write = Write;
				_Auto = false;
			}

			public Permissions()
			{

			}

			public bool Execution { get { return _Execution; } set { _Read = value; _Auto = false; } }
			public bool Read { get { return _Read; } set { _Read = value; _Auto = false; } }
			public bool Write { get { return _Write; } set { _Write = value; _Auto = false; } }


			public void Save(System.IO.Stream stream)
			{
				Internal.StreamTools.WriteBoolean(stream, _Auto);
				Internal.StreamTools.WriteBoolean(stream, _Execution);
				Internal.StreamTools.WriteBoolean(stream, _Read);
				Internal.StreamTools.WriteBoolean(stream, _Write);
			}

			public static void Load(System.IO.Stream stream)
			{
				Permissions p = new Permissions ();
				p._Auto = Internal.StreamTools.ReadBoolean (stream);
				p._Execution = Internal.StreamTools.ReadBoolean (stream);
				p._Read = Internal.StreamTools.ReadBoolean (stream);
				p._Write = Internal.StreamTools.ReadBoolean (stream);
			}
		}

        public Project Project
        {
            get { return _Project; }
            set { _Project = value; }
        }

		/// <summary>
		/// Extract the file with its original name in Directory
		/// </summary>
		/// <param name="Directory">Directory.</param>
		public string ExtractAt(string Directory)
		{
			Extract(Directory + "/" + _FileName);
			return Directory + "/" + _FileName;
		}

		/// <summary>
		/// Extract the specified File.
		/// </summary>
		/// <param name="File">The path where the file will be extracted.</param>
		public void Extract(string File)
		{
			try
			{
				string dir = System.IO.Path.GetDirectoryName(File);
				Tools.RecursiveMakeDirectory(dir);
			}
			catch { }

			try
			{
                try
                {
                    if (System.IO.File.Exists(File))
                    {
                        Tools.RecursiveDelete(File);
                    }
                }
                catch { }
				if (Binary)
					System.IO.File.WriteAllBytes(File, _RawData);
				else
					System.IO.File.WriteAllText(File, GetText(false));
			}
			catch { }
		}

        public void Save(System.IO.Stream stream) 
        {
            Internal.StreamTools.WriteUTF8String(stream, _FileName);
            Internal.StreamTools.WriteInt32(stream, (Int32)_Type);


			//ACL

            System.IO.MemoryStream rawtest = new System.IO.MemoryStream();
#if DEBUG
            Fact.Log.Debug("Save file '" + _FileName + "' testdata");
#endif
            _TestResult.Save(rawtest);
            Internal.Security.SnowfallCipher cipher = new Internal.Security.SnowfallCipher("fact-project");
            Internal.StreamTools.WriteByteArray(stream, cipher.Encrypt(rawtest.ToArray()));

            // Check if the file is a rawData File
            if (_RawData != null)
            {
#if DEBUG
                Fact.Log.Debug("Save file '" + _FileName + "' as raw data");
#endif
                Internal.StreamTools.WriteBoolean(stream, true);
                Internal.StreamTools.WriteByteArray(stream, _RawData);
            }
            else
            {
                Internal.StreamTools.WriteBoolean(stream, false);
                if (_MetaChar != null)
                {
#if DEBUG
                    Fact.Log.Debug("Save file '" + _FileName + "' as tree");
#endif
                    Internal.StreamTools.WriteBoolean(stream, false);
                    Internal.StreamTools.WriteInt32(stream, _MetaChar.Length);
                    foreach (Character.MetaChar metaChar in _MetaChar)
                    {
                        metaChar.Save(stream);
                    }
                }
                else
                {
#if DEBUG
                    Fact.Log.Debug("Save file '" + _FileName + "' as raw text");
#endif
                    Internal.StreamTools.WriteBoolean(stream, true);
                    Internal.StreamTools.WriteInt32(stream, _RawText.Length);
                    for (int n = 0; n < _RawText.Length; n++) { Internal.StreamTools.WriteUTF8String(stream, _RawText[n]); }
                }
            }

            // Save the code elements
            Internal.StreamTools.WriteInt32(stream, _Elements.Count);
            foreach (Code.Element element in _Elements)
            {
                element.Save(stream);
            }
        }

        public void Load(System.IO.Stream stream)
        {
            _FileName = Internal.StreamTools.ReadUTF8String(stream);
            _Type = (FileType)Internal.StreamTools.ReadInt32(stream);

            // Load the test result
            byte[] encryptedtests = Internal.StreamTools.ReadByteArray(stream);
            Internal.Security.SnowfallCipher cipher = new Internal.Security.SnowfallCipher("fact-project");
            System.IO.MemoryStream rawtests = new System.IO.MemoryStream(cipher.Decrypt(encryptedtests));
            _TestResult = Fact.Test.Result.Result.Load(rawtests) as Fact.Test.Result.Group;

            // Check if the file is a rawData File
            bool rawData = Internal.StreamTools.ReadBoolean(stream);
            if (rawData)
            {
                _RawData = Internal.StreamTools.ReadByteArray(stream);
            }
            else
            {
                bool rawText = Internal.StreamTools.ReadBoolean(stream);
                if (rawText)
                {
                    _MetaChar = null;
                    int Linecount = Internal.StreamTools.ReadInt32(stream);
                    if (Linecount < 0) { throw new Exception("The file data for '" + _FileName + "' are corrupted"); }
                    _RawText = new string[Linecount];
                    for (int n = 0; n < Linecount; n++)
                    { _RawText[n] = Internal.StreamTools.ReadUTF8String(stream); }
                }
                else
                {
                    _RawData = null;
                    int count = Internal.StreamTools.ReadInt32(stream);
                    if (count < 0) { throw new Exception("The file data for '" + _FileName + "' are corrupted"); }
                    _MetaChar = new Character.MetaChar[count];
                    _RawText = null;
                    if (count > 0)
                    {
                        byte[] trashbuffer = new byte[8];
                        {
                            Character.MetaChar metaChar = new Fact.Character.MetaChar("", 0, 0, this);
                            metaChar.Load(stream, trashbuffer);
                            _MetaChar[0] = metaChar;
                        }
                        for (int n = 1; n < count; n++)
                        {
                            Character.MetaChar metaChar = new Fact.Character.MetaChar("", 0, 0, this);
                            metaChar.Load(stream, trashbuffer);
                            _MetaChar[n] = metaChar;
                            _MetaChar[n]._Previous = _MetaChar[n - 1];
                            _MetaChar[n - 1]._Next = _MetaChar[n];
                        }
                    }
                    RebuildLine();
                    RebuildToken();
                }
            }
            // Load the code elements
            int elementCount = Internal.StreamTools.ReadInt32(stream);
            if (elementCount < 0) { throw new Exception("The file data for '" + _FileName + "' are corrupted"); }
            while (elementCount > 0)
            {
                _Elements.Add(Code.Element.LoadGeneric(stream));
                elementCount--;
            }
        }

        FileType _Type = FileType.None;
        public FileType Type { get { return _Type; } set { _Type = value; } }

        internal byte[] _RawData = null;
        internal Character.MetaChar[] _MetaChar = null;
        List<Character.MetaLine> _MetaLine = new List<Character.MetaLine>();
        List<Character.MetaToken> _MetaToken = new List<Character.MetaToken>();
        internal Fact.Test.Result.Group _TestResult = new Fact.Test.Result.Group("");
        string _FileName = "";
        internal string _Directory = "";


        public List<Test.Result.Result> GetTestResults()
        { return _TestResult.GetTestResults(); }

        public File(string[] Text) : this(Text, "") { }
        public File(string[] Text, string FileName)
        {
            _FileName = FileName;
            _RawText = (string[])Text.Clone();
            _MetaChar = null;
        }
        internal void _BuildMetachar()
        {
            _BuildMetachar(_RawText);
            _RawText = null;
        }
        void _BuildMetachar(string[] Text)
        {
            int count = 0;
            for (uint l = 0; l < Text.Length; l++) { count += (Text[l].Length + 1); }
            _MetaChar = new Character.MetaChar[count];
            int index = 0;
            int textlength = Text.Length;
            for (int l = 0; l < textlength; l++)
            {
                int linelength = Text[l].Length;
                for (int c = 0; c < linelength; c++)
                {
                    _MetaChar[index] = (new Character.MetaChar(Text[l][c].ToString(), (uint)(l + 1), (uint)(c + 1), this));
                    if (index > 0)
                    {
                        _MetaChar[index - 1]._Next = _MetaChar[index];
                        _MetaChar[index]._Previous = _MetaChar[index - 1];
                    }
                    index++;
                }
                _MetaChar[index] = (new Character.MetaChar("\n", (uint)(l + 1), (uint)linelength, this));
                if (index > 0)
                {
                    _MetaChar[index - 1]._Next = _MetaChar[index];
                    _MetaChar[index]._Previous = _MetaChar[index - 1];
                }
                index++;
            }
        }

        internal File()
        {
        }
        public File(byte[] RawData) : this(RawData, "") { }
        public File(byte[] RawData, string FileName)
        {
            _FileName = FileName;
            _RawData = RawData;
        }

        public bool Binary { get { return _RawData != null; } }

        public void Append(File file)
        {
            if (file.Binary)
            {
                if (Binary)
                {
                    List<byte> array = new List<byte>(_RawData);
                    foreach (byte b in file._RawData)
                    {
                        array.Add(b);
                    }
                    _RawData = array.ToArray();
                }
                else
                {
                    throw new Exception("Impossible to append a text file to a binary file");
                }
            }
            else
            {
                if (!Binary)
                {
                    if (_MetaChar == null) { _MetaChar = new Character.MetaChar[0]; }
                    int offset = _MetaChar.Length;
                    int count = file._MetaChar.Length;
                    System.Array.Resize<Character.MetaChar>(ref _MetaChar, _MetaChar.Length + file._MetaChar.Length);
                    for (int n = 0; n < count; n++) { _MetaChar[n + offset] = file._MetaChar[n]; }
                    foreach (Character.MetaLine l in file._MetaLine) { _MetaLine.Add(l); }
                    foreach (Character.MetaToken t in file._MetaToken) { _MetaToken.Add(t); }
                }
                else
                {
                    throw new Exception("Impossible to append a binary file to a text file");
                }
            }
        }

        public byte[] GetBytes()
        {
            if (_RawData != null) { return _RawData; }
            return System.Text.Encoding.UTF8.GetBytes(GetText(false));
        }

        public File Copy()
        {
            return Copy("");
        }
        public File Copy(string NewName)
        {
            File file = new File();
            file.Type = _Type;
            file._FileName = NewName;
            file._RawData = (byte[])_RawData.Clone();
            // FIXME Implement a codeblock duplication
            //file._MainBlock = _MainBlock;
            file._MetaChar = (Character.MetaChar[])_MetaChar.Clone();
            file._MetaLine = new List<Character.MetaLine>(_MetaLine);
            file._MetaToken = new List<Character.MetaToken>(_MetaToken);
            file._TestResult = new Test.Result.Group("");
            for (int n = 0; n < file._TestResult.GetTestResults().Count; n++)
            {
                file._TestResult.AddTestResult(file._TestResult.GetTestResults()[n]);
            }
            return file;
        }

        static string GetEol()
        {
            Internal.Information.OperatingSystem OS = Fact.Internal.Information.CurrentOperatingSystem();
            string eol = "";
            if (OS == Fact.Internal.Information.OperatingSystem.MicrosoftDOS ||
                        OS == Fact.Internal.Information.OperatingSystem.MicrosoftWindows ||
                        OS == Fact.Internal.Information.OperatingSystem.MicrosoftWindowsCE ||
                        OS == Fact.Internal.Information.OperatingSystem.MicrosoftXBox360)
            {
                eol = ("\r\n");
            }
            else if (OS == Fact.Internal.Information.OperatingSystem.AppleMacOSX ||
                     OS == Fact.Internal.Information.OperatingSystem.Unix)
            {
                eol = ("\n");
            }
            else if (OS == Fact.Internal.Information.OperatingSystem.AppleMacOSOld)
            {
                eol = ("\r");
            }
            else
            {
                eol = ("\n");
            }
            return eol;
        }

        string[] _RawText = new string[0];
        public string GetText(bool ApplyFormat)
        {
            if (Binary)
            {
                try
                {
                    return System.Text.Encoding.UTF8.GetString(_RawData);
                }
                catch { return ""; }
            }
            StringBuilder Builder = new StringBuilder();
            if (_MetaChar == null)
            {
                if (ApplyFormat)
                {
                    string eol = GetEol();
                    for (int n = 0; n < _RawText.Length; n++)
                    {
                        Builder.Append(_RawText[n]);
                        Builder.Append(eol);
                    }
                }
                else
                {
                    for (int n = 0; n < _RawText.Length; n++)
                    {
                        Builder.Append(_RawText[n]);
                        Builder.Append("\n");
                    }
                }
            }
            else
            {
                int length = _MetaChar.Length;
                if (ApplyFormat)
                {
                    string eol = GetEol();

                    for (int n = 0; n < length; n++)
                    {
                        if (_MetaChar[n].Type == Fact.Character.MetaChar.BasicCharType.Ignored) { }
                        else if (_MetaChar[n].Type == Fact.Character.MetaChar.BasicCharType.EndOfLine) { Builder.Append(eol); }
                        else { Builder.Append(_MetaChar[n].Value); }
                    }
                }
                else
                {
                    for (int n = 0; n < length; n++)
                    { Builder.Append(_MetaChar[n].Value); }
                }
            }
            return Builder.ToString();
        }


        public string[] GetLines(bool Format)
        {
            string text = GetText(Format);
            return text.Split('\n');
        }

        public void Rebuild()
        {
            RebuildChar();
            RebuildLine();
            RebuildToken();
        }

        public void AddTestResult(Test.Result.Result Result)
        {
            if (Result == null) { return; }
            _TestResult.AddTestResult(Result);
        }
        /// <summary>
        /// Recompute the character list the list in the file.
        /// Only the MetaChar line is real, the orthers are only mappings.
        /// </summary>
        void RebuildChar()
        {
            List<Character.MetaChar> newMetaChar = new List<Character.MetaChar>();
            int length = _MetaChar.Length;
            for (int n = 0; n < length; n++)
            {
                if (_MetaChar[n].Value.Length > 1)
                {
                    for (int c = 0; c < _MetaChar[n].Value.Length; c++)
                    {
                        newMetaChar.Add(
                            new Character.MetaChar(
                                _MetaChar[n].Value[c].ToString(),
                                _MetaChar[n].Line,
                                _MetaChar[n].Column,
                                _MetaChar[n].File));
                        newMetaChar[newMetaChar.Count - 1]._Previous = null;
                        newMetaChar[newMetaChar.Count - 1]._Next = null;

                        if (newMetaChar.Count > 1)
                        {
                            newMetaChar[newMetaChar.Count - 1]._Previous = newMetaChar[newMetaChar.Count - 2];
                            newMetaChar[newMetaChar.Count - 1]._Previous._Next = newMetaChar[newMetaChar.Count - 1];
                        }

                        newMetaChar[newMetaChar.Count - 1].Type = _MetaChar[n].Type;
                    }
                }
                else if (_MetaChar[n].Value.Length > 0)
                {
                    newMetaChar.Add(_MetaChar[n]);
                    if (newMetaChar.Count > 1)
                    {
                        newMetaChar[newMetaChar.Count - 1]._Previous = newMetaChar[newMetaChar.Count - 2];
                        newMetaChar[newMetaChar.Count - 2]._Next = newMetaChar[newMetaChar.Count - 1];
                    }
                    else
                    {
                        newMetaChar[newMetaChar.Count - 1]._Previous = null;
                        newMetaChar[newMetaChar.Count - 1]._Next = null;
                    }
                }
            }
            _MetaChar = newMetaChar.ToArray();
        }

        /// <summary>
        /// Recompute the line list the list in the file.
        /// Only the MetaChar line is real, the orthers are only mappings.
        /// </summary>
        void RebuildLine()
        {
            _MetaLine = new List<Character.MetaLine>();
            Character.MetaLine line = new Character.MetaLine(this);
            int metaCharlength = _MetaChar.Length;
            for (int n = 0; n < metaCharlength; n++)
            {
                if (_MetaChar[n].Type == Character.MetaChar.BasicCharType.EndOfLine)
                {
                    _MetaLine.Add(line);
                    line = new Character.MetaLine(this);
                }
                else
                {
                    if (_MetaChar[n].Type != Character.MetaChar.BasicCharType.Ignored)
                    {
                        line.Add(_MetaChar[n]);
                        _MetaChar[n]._AssociatedLine = line;
                    }
                }
            }
            _MetaLine.Add(line);
        }

        /// <summary>
        /// Recompute the token list the list in the file.
        /// Only the MetaChar line is real, the orthers are only mappings.
        /// </summary>
        void RebuildToken()
        {
            _MetaToken = new List<Character.MetaToken>();
            List<Character.MetaChar> token = new List<Character.MetaChar>();
            int metaCharlength = _MetaChar.Length;
            for (int n = 0; n < metaCharlength; n++)
            {
                if (_MetaChar[n].SeparatorType != Character.MetaChar.BasicSeparatorType.None)
                {
                    if (_MetaChar[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit)
                    {
                        token.Add(_MetaChar[n]);
                        _MetaToken.Add(new Character.MetaToken(token));
                        if (_MetaToken.Count > 1)
                        { 
                            _MetaToken[_MetaToken.Count - 1]._Previous = _MetaToken[_MetaToken.Count - 2];
                            _MetaToken[_MetaToken.Count - 2]._Next = _MetaToken[_MetaToken.Count - 1];
                        }
                        token = new List<Character.MetaChar>();
                    }
                    else
                    {
                        if (token.Count > 0)
                        {
                            _MetaToken.Add(new Character.MetaToken(token));
                            if (_MetaToken.Count > 1)
                            {
                                _MetaToken[_MetaToken.Count - 1]._Previous = _MetaToken[_MetaToken.Count - 2];
                                _MetaToken[_MetaToken.Count - 2]._Next = _MetaToken[_MetaToken.Count - 1];
                            }
                            token = new List<Character.MetaChar>();
                        }
                        token.Add(_MetaChar[n]);
                        _MetaToken.Add(new Character.MetaToken(token));
                        if (_MetaToken.Count > 1)
                        {
                            _MetaToken[_MetaToken.Count - 1]._Previous = _MetaToken[_MetaToken.Count - 2];
                            _MetaToken[_MetaToken.Count - 2]._Next = _MetaToken[_MetaToken.Count - 1];
                        }
                        token = new List<Character.MetaChar>();
                    }
                }
                else
                {
                    if (_MetaChar[n].Type != Character.MetaChar.BasicCharType.Ignored)
                        token.Add(_MetaChar[n]);
                }
            }
        }

        /// <summary>
        /// Get the character list.
        /// </summary>
        public List<Character.MetaChar> GetAsChar()
        {
            if (_MetaChar == null) { _BuildMetachar(); }
            return new List<Character.MetaChar>(_MetaChar);
        }

        /// <summary>
        /// Get the line list.
        /// </summary>
        public List<Character.MetaLine> GetAsLine() { return _MetaLine; }

        /// <summary>
        /// Get the token list.
        /// </summary>
        public List<Character.MetaToken> GetAsToken() { return _MetaToken; }

        public string Name { get { return _FileName; } }

        /// <summary>
        /// This method can be used only if the file is not yet used. This must be reserved to advanced user for custom loading operations
        /// </summary>
        /// <param name="Name"></param>
        public void ForceChangeName(string Name)
        {
            _FileName = Name;
        }

        public string Directory { get { return _Directory; } }

        /// <summary>
        /// Write the metadata of the file in the package
        /// </summary>
        public void Update()
        {
            if (_Project != null)
            {
                if (_MetaChar == null && _TestResult._results.Count == 0) { return; }

            }
        }

        public override string ToString()
        {
            string formated = "";
            formated += "File : < " + _Directory + "/" + _FileName + " >";
            return formated;
        }

        ~File()
        {
            Dispose();
        }

        public void Dispose()
        {
            Update();
        }
    }
}

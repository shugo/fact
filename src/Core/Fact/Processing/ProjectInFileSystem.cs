﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing
{
    public class ProjectInFileSystem : Project
    {
        string _Path = "";
        string _Root = "";
        string _Name = "";

        Dictionary<string, WeakReference> _OpenFiles = new Dictionary<string, WeakReference>();

        public override string Name { get { return _Name; } }

        public ProjectInFileSystem(string Path, string Name) : base(Name)
        {
            _Name = Name;
            _Path = Path;
            _Root = FindRootOfProject(Path);
            Fact.Tools.RecursiveMakeDirectory(_Path);
            Fact.Tools.RecursiveMakeDirectory(_Path + "/.factdata");
            Fact.Tools.RecursiveMakeDirectory(_Path + "/.factdata/files");

            if (System.IO.File.Exists(_Path + "/.factdata/Project.xml"))
            {
                Fact.Parser.XML xml = new Fact.Parser.XML();
                xml.Parse_File(_Path + "/.factdata/Project.xml");
                Fact.Parser.XML.Node node = xml.Root.FindNode("project", false);
                _Name = node["name"];
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<project name=\"" + Name + "\">");
                builder.Append("</project>");
                System.IO.File.WriteAllText(_Path + "/.factdata/Project.xml", builder.ToString());
            }
            if (System.IO.File.Exists(_Path + "/.factdata/Tests.bin"))
            {

            }
            if (System.IO.File.Exists(_Path + "/.factdata/Build.xml"))
            {
                Fact.Parser.XML XML = new Fact.Parser.XML();
                XML.Parse_File(_Path + "/.factdata/Build.xml");
                BuidSystem = Build.Target.LoadXML(XML.Root);
            }
        }

        public static string FindRootOfProject(string Directory)
        {
            if (!System.IO.Directory.Exists(Directory)) { return ""; }
            List<string> vcsPath = new List<string>();
            try
            {
                string p4clientpathList = Internal.Information.GetEnvVariable("P4CLIENTPATH");
                List<string> p4clientpaths = new List<string>();
                // FIXME Support several p4 client path
                p4clientpaths.Add(p4clientpathList);

                foreach (string p4path in p4clientpaths)
                {
                    try
                    {
                        if (p4path != null && p4path != "" && System.IO.Directory.Exists(p4path))
                        {
                            vcsPath.Add(System.IO.Path.GetFullPath(p4path));
                        }
                    }
                    catch { }
                }
            }
            catch { }


            Directory = System.IO.Path.GetFullPath(Directory);
            Queue<string> path = new Queue<string>();
            path.Enqueue(Directory);
            while ((Directory != null) &&
                   (Directory != "/") &&
                   (Directory.ToLower() != "/") &&
                   !(Directory.Length == 3 && (Directory.EndsWith(":/") && !Directory.EndsWith(":\\"))) &&
                   (Directory.ToLower() != "/cygdrive/"))
            {
                try
                {
                    string old = Directory;
                    Directory = System.IO.Path.GetDirectoryName(Directory);
                    if (Directory == old) { break; }
                    path.Enqueue(Directory);
                }
                catch { break; }
            }

            while (path.Count > 0)
            {
                Directory = path.Dequeue();
                if (System.IO.Directory.Exists(Directory + "/.factdata"))
                {
                    return Directory;
                }
                else if (System.IO.Directory.Exists(Directory + "/.git"))
                {
                    return Directory;
                }
                else
                {
                    foreach (string vcs in vcsPath)
                    {
                        if (vcs == Directory)
                        {
                            return Directory;
                        }
                    }
                }
            }

            return "";
        }

        public static bool IsPhysicalDirectoryInProject(string Directory)
        {
            try
            {
                return FindRootOfProject(Directory) != "";
            }
            catch
            {
                return false;
            }
        }

        string _GetMetadataFolder(string FilePath)
        {
            return _Path + "/.factdata/" + FilePath;
        }

        bool _MustUpdateMetatadata(File file)
        {
            try
            {
                string folder = _GetMetadataFolder(file.Directory);
                if (!System.IO.File.Exists(folder)) { return true; }
                if (!System.IO.File.Exists(folder + "/hash.bin")) { return true; }
                string hashstr = System.IO.File.ReadAllText(folder + "/hash.bin").Trim();
                return (file.ContentHash != hashstr);
            }
            catch { return true; }
        }

        void _UpdateFile(File file)
        {
            if (_MustUpdateMetatadata(file))
            {
                string folder = _GetMetadataFolder(file.Directory);
                if (!System.IO.File.Exists(folder)) { System.IO.Directory.CreateDirectory(folder); }
                if (System.IO.File.Exists(folder + "/hash.bin")) { System.IO.File.Delete(folder + "/hash.bin"); }
                if (System.IO.File.Exists(folder + "/tests.bin")) { System.IO.File.Delete(folder + "/tests.bin"); }
                System.IO.Stream stream = System.IO.File.Open(folder + "/tests.bin", System.IO.FileMode.OpenOrCreate);
                file._TestResult.Save(stream);
                stream.Flush();
                stream.Close();
                System.IO.File.WriteAllText(folder + "/hash.bin", file.ContentHash);
            }
        }

        public override void AddFile(string Directory, File File, bool KeepPrevious)
        {
            _OpenFiles[Directory + File.Name] = new WeakReference(File);
            if (System.IO.File.Exists(_Path + "/" + Directory + File.Name))
            {
                System.IO.File.Delete(_Path + "/" + Directory + File.Name);
            }
            Fact.Tools.RecursiveMakeDirectory(_Path + "/" + Directory);
            File.ExtractAt(_Path + "/" + Directory);
        }

        public override List<File> GetFiles(string DirectoryName, bool Recursive, params File.FileType[] FileType)
        {
            if (DirectoryName.ToLower().EndsWith("/.factdata")) { return new List<File>(); }
            if (DirectoryName.ToLower() == ".factdata") { return new List<File>(); }
            if (DirectoryName.ToLower().StartsWith(".factdata/")) { return new List<File>(); }


            while (DirectoryName.StartsWith("/")) { DirectoryName = DirectoryName.Substring(1); }
            List<File> result = new List<File>();
            if (!System.IO.Directory.Exists(_Path + "/" + DirectoryName))
            {
                return result;
            }
            foreach (string filePath in System.IO.Directory.GetFiles(_Path + "/" + DirectoryName))
            {
                File.FileType fileType = Internal.File.GetFileType(filePath);
                if (FileType != null && FileType.Length > 0)
                {
                    bool found = false;
                    foreach (File.FileType type in FileType) { if (fileType == type) { found = true; } }
                    if (!found) { continue; }
                }
                File file = null;
                if (Internal.File.IsTextFile(fileType))
                {
                    file = new File(System.IO.File.ReadAllLines(filePath), System.IO.Path.GetFileName(filePath));
                }
                else
                {
                    file = new File(System.IO.File.ReadAllBytes(filePath), System.IO.Path.GetFileName(filePath));
                }
                file.Type = fileType;
                file._Directory = DirectoryName;

                string metadata = _GetMetadataFolder(file.Directory + file.Name);
                if (System.IO.Directory.Exists(metadata))
                {
                    try
                    {
                        // Load metadata
                    }
                    catch { Tools.RecursiveDelete(metadata); }
                }

                _OpenFiles[DirectoryName + file.Name] = new WeakReference(file);
                result.Add(file);
            }
            if (Recursive)
            {
                foreach (string directory in System.IO.Directory.GetDirectories(_Path + "/" + DirectoryName))
                {
                    string directoryName = DirectoryName + "/" + System.IO.Path.GetFileName(directory);
                    if (directoryName.ToLower().EndsWith("/.factdata")) { continue; }
                    while (directoryName.StartsWith("/")) { directoryName = directoryName.Substring(1); }
                    result.AddRange(GetFiles(DirectoryName + "/" + System.IO.Path.GetFileName(directory), true, FileType));
                }
            }
            return result;
        }

        public override File GetFile(string Name)
        {
            string directory = System.IO.Path.GetDirectoryName(Name);
            string name = System.IO.Path.GetFileName(Name);


            foreach (File file in GetFiles(directory, false))
            {
                if (file.Name == name) { return file; }
            }
            return null;
        }

        public override bool RemoveDirectory(string Name)
        {
            if (!System.IO.Directory.Exists(_Path + "/" + Name)) { return false; }
            Fact.Tools.RecursiveDelete(_Path + "/" + Name);
            return !(System.IO.Directory.Exists(_Path + "/" + Name));
        }

        public override bool RemoveFile(string DirectoryName, string FileName)
        {
            if (System.IO.File.Exists(_Path + "/" + DirectoryName + "/" + FileName))
            {
                Fact.Tools.RecursiveDelete(_Path + "/" + DirectoryName + "/" + FileName);
                return !(System.IO.File.Exists(_Path + "/" + DirectoryName + "/" + FileName));
            }
            else
            {
                return false;
            }
        }

        public override bool Contains(string DirectoryName, string FileName)
        {
            return (System.IO.File.Exists(_Path + "/" + DirectoryName + "/" + FileName));
        }

        public override IEnumerable<string> GetSubDirectories(string DirectoryName, bool Recursive)
        {
            yield return "";
        }

        public override IEnumerator<File> GetEnumerator()
        {
            foreach (File file in GetFiles("/", true, null))
            {
                yield return file;
            }
        }

        public void UpdateProjectData()
        {
            if (_Results.Count > 0)
            {
                string tmp = Fact.Tools.CreateTempDirectory();
                string tmpfile = tmp + "/tests.bin";
                System.IO.Stream stream = System.IO.File.Open(tmpfile, System.IO.FileMode.OpenOrCreate);
                Internal.StreamTools.WriteUInt64(stream, (ulong)_Results.Count);
                foreach (Test.Result.Result result in _Results)
                {
                    result.Save(stream);
                }
                stream.Close();
                try
                {
                    System.IO.File.Replace(tmpfile, _Path + "/.factdata/tests.bin", tmp + "/backup.bin", true);
                }
                catch
                {
                    if (System.IO.File.Exists(_Path + "/.factdata/tests.bin"))
                    {
                        System.IO.File.Delete(_Path + "/.factdata/tests.bin");
                    }
                    System.IO.File.Move(tmpfile, _Path + "/.factdata/tests.bin");
                }
                Tools.RecursiveDelete(tmp);
            }
        }
        List<Test.Result.Result> _Results = new List<Test.Result.Result>();
        public override void AddTestResult(Test.Result.Result Result)
        {
            if (Result != null)
            {
                _Results.Add(Result);
            }
        }

        ~ProjectInFileSystem()
        {
            foreach (WeakReference reference in _OpenFiles.Values)
            {
                if (reference.IsAlive)
                {
                    _UpdateFile(reference.Target as File);
                }
            }
        }
    }
}

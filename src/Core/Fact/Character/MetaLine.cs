﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Character
{
    public class MetaLine
    {
        List<MetaChar> _MetaChar = new List<MetaChar>();
        Processing.File _File = null;

        public MetaChar this[int index] { get { return _MetaChar[index]; } }
        public int Length { get { return _MetaChar.Count; } }
        public void Add(MetaChar Char) { _MetaChar.Add(Char); }
        public MetaLine() { }
        public MetaLine(Processing.File File) { _File = File; }

        public Processing.File File { get { return _File; } }
        public int Line
        {
            get
            {
                if (_MetaChar.Count == 0) { return -1; }
                uint min = _MetaChar[0].Line;
                uint max = _MetaChar[0].Line;

                foreach (MetaChar _ in _MetaChar)
                {
                    if (_.Line > max) { max = _.Line; }
                    else if (_.Line < min) { min = _.Line; }
                }
                return (int)min;
            }
        }
        public string FileLines
        {
            get
            {
                if (_MetaChar.Count == 0) { return ""; }
                uint min = _MetaChar[0].Line;
                uint max = _MetaChar[0].Line;

                foreach (MetaChar _ in _MetaChar)
                {
                    if (_.Line > max) { max = _.Line; }
                    else if (_.Line < min) { min = _.Line; }
                }
                if (min == max) { return min.ToString(); }
                return min.ToString() + "-" + max.ToString();
            }
        }

        public string SybolicBasicTypes
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                for (int n = 0; n < _MetaChar.Count; n++)
                {
                    if (_MetaChar[n].Type == MetaChar.BasicCharType.Space) { builder.Append(" "); }
                    else if (_MetaChar[n].Type == MetaChar.BasicCharType.String) { builder.Append("s"); }
                    else if (_MetaChar[n].SeparatorType == MetaChar.BasicSeparatorType.Separator) { builder.Append("|"); }
                    else if (_MetaChar[n].Type == MetaChar.BasicCharType.Ignored) { }
                    else if (_MetaChar[n].Type == MetaChar.BasicCharType.EndOfLine) { builder.Append("\n"); }
                    else if (_MetaChar[n].Type == MetaChar.BasicCharType.Comment) { builder.Append("c"); }
                    else if (_MetaChar[n].Type == MetaChar.BasicCharType.Preprocessor) { builder.Append("p"); }
                    else { builder.Append("?"); }
                }
                return builder.ToString();
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for (int n = 0; n < _MetaChar.Count; n++)
            {
                builder.Append(_MetaChar[n].Value);
            }
            return builder.ToString();
        }

        public bool StartWith(string text)
        {
            if (_MetaChar.Count < text.Length) { return false; }
            int min = text.Length < _MetaChar.Count ? text.Length : _MetaChar.Count;
            for (int n = 0; n < text.Length; n++)
            {
                if (text[n].ToString() != _MetaChar[n].ToString()) { return false; }
            }
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Character
{
    public class MetaToken
    {
        List<MetaChar> _Token = new List<MetaChar>();

        public MetaChar this[int index] { get { return _Token[index]; } }
        public int Length { get { return _Token.Count; } }

        internal MetaToken _Next = null;
        internal MetaToken _Previous = null;

        public MetaToken Next { get { return _Next; } }
        public MetaToken NextEffective
        {
            get
            {
                MetaToken tmp = _Next;
                while ((object)tmp != null)
                {
                    if (tmp.Type != MetaChar.BasicCharType.Comment &&
                        tmp.Type != MetaChar.BasicCharType.Space &&
                        tmp.Type != MetaChar.BasicCharType.EndOfLine &&
                        tmp.Type != MetaChar.BasicCharType.Ignored)
                    {
                        return tmp;
                    }
                    tmp = tmp.Next;
                }
                return null;
            }
        }
        public MetaToken Previous { get { return _Previous; } }
        public MetaToken PreviousEffective
        {
            get
            {
                MetaToken tmp = _Previous;
                while ((object)tmp != null)
                {
                    if (tmp.Type != MetaChar.BasicCharType.Comment &&
                        tmp.Type != MetaChar.BasicCharType.Space &&
                        tmp.Type != MetaChar.BasicCharType.EndOfLine &&
                        tmp.Type != MetaChar.BasicCharType.Ignored)
                    {
                        return tmp;
                    }
                    tmp = tmp.Previous;
                }
                return null;
            }
        }
		public Processing.File File 
		{
			get 
			{ 
				if (_Token.Count == 0) { return null;}
				return _Token [0].File; 
			}
		}

        public MetaToken(List<MetaChar> Token)
        {
            _Token = Token;
        }

        public MetaChar.BasicTokenType TokenType
        {
            get
            {
                if (_Token.Count == 0) { return MetaChar.BasicTokenType.None; }

                MetaChar.BasicTokenType Type = _Token[0].TokenType;
                foreach (MetaChar a_char in _Token)
                { if (Type != a_char.TokenType) return MetaChar.BasicTokenType.None; }
                return Type;
            }
            set 
            {
                foreach (MetaChar a_char in _Token)
                { a_char.TokenType = value; }
            }
        }

        public MetaChar.BasicSeparatorType SeparatorType
        {
            get
            {
                if (_Token.Count == 0) { return MetaChar.BasicSeparatorType.None; }

                MetaChar.BasicSeparatorType Type = _Token[0].SeparatorType;
                foreach (MetaChar a_char in _Token)
                { if (Type != a_char.SeparatorType) return MetaChar.BasicSeparatorType.None; }
                return Type;
            }
            set
            {
                foreach (MetaChar a_char in _Token)
                { a_char.SeparatorType = value; }
            }
        }

        public MetaChar.BasicCharType Type
        {
            get
            {
                if (_Token.Count == 0) { return MetaChar.BasicCharType.None; }

                MetaChar.BasicCharType Type = _Token[0].Type;
                foreach (MetaChar a_char in _Token)
                { if (Type != a_char.Type) return MetaChar.BasicCharType.None; }
                return Type;
            }
            set
            {
                foreach (MetaChar a_char in _Token)
                { a_char.Type = value; }
            }
        }

        // Operators

        public static bool operator ==(MetaToken Left, MetaToken Right) 
		{
			if ((object)Left == null) return ((object)Right) == null;
			return Left.ToString() == Right.ToString();
		}

        public static bool operator !=(MetaToken Left, MetaToken Right) 
		{
			if ((object)Left == null) return ((object)Right) != null;
			return Left.ToString() != Right.ToString(); 
		}
        public static bool operator ==(MetaToken Left, string Right) 
		{ 
			if ((object)Left == null) return Right == "";
			return Left.ToString() == Right; 
		}
        public static bool operator !=(MetaToken Left, string Right) 
		{
			if ((object)Left == null) return Right != "";
			return Left.ToString() != Right; 
		}
        public static bool operator ==(string Left, MetaToken Right) 
		{
			if ((object)Left == null) return Left == "";
			return Left == Right.ToString(); 
		}
        public static bool operator !=(string Left, MetaToken Right) 
		{
			if ((object)Right == null) return Left != "";
			return Left != Right.ToString(); 
		}

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (MetaChar a_char in _Token)
            { builder.Append(a_char); }
            return builder.ToString();
        }
    }
}

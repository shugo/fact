﻿/* FACT : Fact is an Automatic Correction Tools
 * Authors : Raphael 'Shugo' Boissel
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Character
{
    public class MetaChar
    {
        public enum BasicCharType
        {
            None,
            Space,
            Comment,
            String,
            EndOfLine,
            Ignored,
            Preprocessor,
        }

        public enum BasicSeparatorType
        {
            None,
            Separator,
            Implicit,
            Single,
        }

        public enum BasicTokenType
        {
            None,
            String,
            KeyWord,
            Type,
            Number,
            Boolean,
            Variable,
            Null,
            Const,
            Cast
        }

        public enum ProtectionLevel
        {
            None,
            Public,
            Private,
            Protected,
            Internal
        }

        public enum RegexpCharType
        {
            None,
            Any,
            One
        }

        uint _Line = 0;
        uint _Col = 0;
        Processing.File _File;
        BasicCharType _Type = BasicCharType.None;
        BasicTokenType _TokenType = BasicTokenType.None;
        BasicSeparatorType _SeparatorType = BasicSeparatorType.None;
        ProtectionLevel _ProtectionLevel = ProtectionLevel.None;

        RegexpCharType _RegexpType = RegexpCharType.None;

        List<MetaChar> _SubRegexp = null;

        internal MetaLine _AssociatedLine = null;

        internal MetaChar _Next = null;
        internal MetaChar _Previous = null;


        string _Value = "";

        public MetaChar(string Value, uint Line, uint Column, Processing.File File)
        {
            _Value = Value;
            _Line = Line;
            _Col = Column;
            _File = File;
        }
        public string Location 
        {
            get
            {
                if (_File.Name != "") { return _File.Name + ":l_" + _Line + ":c_" + _Col; }
                else { return "l_" + _Line + ":c_" + _Col; } 
            }
        }

        public Processing.File File { get { return _File; } }

        public string Value { get { return _Value; } set { _Value = value; } }

        public uint Line { get { return _Line; } set { _Line = value; } }
        public uint Column { get { return _Col; } set { _Col = value; } }

        public MetaChar Next{ get { return _Next; } }
        public MetaChar Previous { get { return _Previous; } }
        public MetaLine AssociatedLine { get { return _AssociatedLine; } }


        public BasicCharType Type { get { return _Type; } set { _Type = value; } }
        public BasicSeparatorType SeparatorType { get { return _SeparatorType; } set { _SeparatorType = value; } }
        public BasicTokenType TokenType { get { return _TokenType; } set { _TokenType = value; } }

        public RegexpCharType RegexpType { get { return _RegexpType; } set { _RegexpType = value; } }
        public List<MetaChar> SubRegexp { get { return _SubRegexp; } set { _SubRegexp = value; } }

        // Operators
        public static implicit operator string(MetaChar MetaChar) { return MetaChar.Value; }
        public static implicit operator MetaChar(string String) { return new MetaChar(String, 0, 0, null); }
        public static implicit operator MetaChar(char Char) { return new MetaChar(Char.ToString(), 0, 0, null); }

        public static bool operator ==(MetaChar Left, MetaChar Right)
		{
			if ((object)Left == null)
				return (object)Right == null;
			if ((object)Right == null)
				return false;
			return Left.Value == Right.Value; 
		}
        public static bool operator !=(MetaChar Left, MetaChar Right)
		{
			if ((object)Left == null)
				return (object)Right != null;
			if ((object)Right == null)
				return true;
			return Left.Value != Right.Value;
		}
        public static bool operator ==(MetaChar Left, string Right) 
		{
			if ((object)Left == null)
				return (object)Right == null;
			if ((object)Right == null)
				return false;
			return Left.Value == Right;
		}
        public static bool operator !=(MetaChar Left, string Right)
		{
			if ((object)Left == null)
				return (object)Right != null;
			if ((object)Right == null)
				return true;
			return Left.Value != Right;
		}
        public static bool operator ==(string Left, MetaChar Right)
		{
			if ((object)Left == null)
				return (object)Right == null;
			if ((object)Right == null)
				return false;
			return Left == Right.Value; 
		}
        public static bool operator !=(string Left, MetaChar Right)
		{
			if ((object)Left == null)
				return (object)Right != null;
			if ((object)Right == null)
				return true;
			return Left != Right.Value;
		}

        public override string ToString() { return _Value; }

        public void Save(System.IO.Stream stream)
        {
            Internal.StreamTools.WriteUTF8ShortString(stream, _Value);
            Internal.StreamTools.WriteUInt32(stream, _Line);
            Internal.StreamTools.WriteUInt32(stream, _Col);

            Internal.StreamTools.WriteInt16(stream, (Int16)_Type);
            Internal.StreamTools.WriteInt16(stream, (Int16)_TokenType);
            Internal.StreamTools.WriteInt16(stream, (Int16)_SeparatorType);
            Internal.StreamTools.WriteInt16(stream, (Int16)_ProtectionLevel);
        }

        internal static void SaveList(System.IO.Stream stream, List<MetaChar> List)
        {

        }
        internal static void LoadList(System.IO.Stream stream, List<MetaChar> List)
        {
            int count = List.Count;
            for (int n = 0; n < count; n++)
            {

            }
        }

        public void Load(System.IO.Stream stream)
        {
            _Value = Internal.StreamTools.ReadUTF8ShortString(stream);
            _Line = Internal.StreamTools.ReadUInt32(stream);
            _Col = Internal.StreamTools.ReadUInt32(stream);

            _Type = (BasicCharType)Internal.StreamTools.ReadInt16(stream);
            _TokenType = (BasicTokenType)Internal.StreamTools.ReadInt16(stream);
            _SeparatorType = (BasicSeparatorType)Internal.StreamTools.ReadInt16(stream);
            _ProtectionLevel = (ProtectionLevel)Internal.StreamTools.ReadInt16(stream);
        }

        public void Load(System.IO.Stream stream, byte[] Buffer)
        {
            _Value = Internal.StreamTools.ReadUTF8ShortString(stream);
            _Line = Internal.StreamTools.ReadUInt32(stream, Buffer);
            _Col = Internal.StreamTools.ReadUInt32(stream, Buffer);

            _Type = (BasicCharType)Internal.StreamTools.ReadInt16(stream, Buffer);
            _TokenType = (BasicTokenType)Internal.StreamTools.ReadInt16(stream, Buffer);
            _SeparatorType = (BasicSeparatorType)Internal.StreamTools.ReadInt16(stream, Buffer);
            _ProtectionLevel = (ProtectionLevel)Internal.StreamTools.ReadInt16(stream, Buffer);
        }
    }                   
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Build
{
    public abstract class Step
    {
        public abstract Test.Result.Result Do(Target Target, Computer.Configuration Configuration);
        internal virtual void Save(System.IO.Stream Stream) { }
        internal virtual void SaveXML(StringBuilder Builder) { }
        internal static Step Load(System.IO.Stream Stream) { return null; }
        internal static Step LoadXML(Fact.Parser.XML.Node Node) { return null; }
    }
}

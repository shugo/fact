﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Build.Steps
{
    class Command : Step
    {
        string _Executable = "";
        string _Arguments = "";

        public Command(string Executable, string Arguments)
        {
            _Executable = Executable;
            _Arguments = Arguments;
        }

        List<Test.Result.Result> ParseOutput(string output)
        {
            List<Test.Result.Result> results = new List<Test.Result.Result>();
            return results;
        }
        List<Test.Result.Result> ParseErrorOutput(string output)
        {
            List<Test.Result.Result> results = new List<Test.Result.Result>();
            return results;
        }

        List<Test.Result.Result> ParseOutput(Runtime.Process.Result output)
        {
            List<Test.Result.Result> results = new List<Test.Result.Result>();
            return results;
        }

        public override Test.Result.Result Do(Target Target, Computer.Configuration Configuration)
        {
           
            Runtime.Process.Result result = Runtime.Process.Execute(_Executable, _Arguments);
            List<Test.Result.Result> results = ParseOutput(result);
         
            if (results.Count == 1) { return results[0]; }
            else
            {
                Test.Result.Group group = new Test.Result.Group(_Executable, _Arguments);
                foreach (Test.Result.Result testresult in results)
                {
                    group.AddTestResult(testresult);
                }
                return group;
            }
        }

        internal override void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteInt32(Stream, 0x10);
            Internal.StreamTools.WriteUTF8String(Stream, _Executable);
            Internal.StreamTools.WriteUTF8String(Stream, _Arguments);
        }

        internal override void SaveXML(StringBuilder Builder)
        {
            Builder.AppendLine("<command executable=\"" + _Executable + "\" arguments=\"" + _Arguments + "\" />");
        }
    }
}

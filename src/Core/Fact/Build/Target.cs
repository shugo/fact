﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Build
{
    public class Target
    {
        Target _Parent = null;
        string _Name = "";
        public class Property<T>
        {
            Property<T> _Parent = null;
            T _Value = default(T);
            bool _IsSet = false;
            public Property() { }
            public Property(T Value) { Set(Value); }
            public Property(Property<T> Parent) { _Parent = Parent; }

            public T Get() { if (!_IsSet && _Parent != null) { return _Parent.Get(); } return _Value; }
            public void Set(T Value) { _Value = Value; _IsSet = true; }
            public void Reset() { _IsSet = false; }
        }

        Property<string> _CCompiler = new Property<string>();
        public Property<string> CCompiler { get { return _CCompiler; } }

        Property<string> _CxxCompiler = new Property<string>();
        public Property<string> CxxCompiler { get { return _CxxCompiler; } }

        Property<string> _CsharpCompiler = new Property<string>();
        public Property<string> CsharpCompiler { get { return _CsharpCompiler; } }

        Property<string> _CCompilationFlags= new Property<string>();
        public Property<string> CCompilationFlags { get { return _CCompilationFlags; } }

        Property<string> _CxxCompilationFlags = new Property<string>();
        public Property<string> CxxCompilationFlags { get { return _CxxCompilationFlags; } }

        Property<string> _CsharpCompilationFlags = new Property<string>();
        public Property<string> CsharpCompilationFlags { get { return _CsharpCompilationFlags; } }

        List<Target> _SubTarget = new List<Target>();
        List<Target> _Dependencies = new List<Target>();

        List<Step> _PreBuildSteps = new List<Step>();
        List<Step> _BuildSteps = new List<Step>();
        List<Step> _PostBuildSteps = new List<Step>();

        bool _Abstract = false;
        public bool Abastract { get { return _Abstract; } set { _Abstract = value; } }

        bool CheckName(string Name)
        {
            return !(Name.Contains(".") ||
                     Name.Contains(",") ||
                     Name.Contains(";") ||
                     Name.Contains("?") ||
                     Name.Contains("$") ||
                     Name.Contains("%") ||
                     Name.Contains("*") ||
                     Name.Contains("|") ||
                     Name.Contains("'") ||
                     Name.Contains("\"") ||
                     Name.Contains("/") ||
                     Name.Contains("\\") ||
                     Name.Trim() == "");
        }

        public Target(string Name) { _Name = Name; if (!CheckName(Name)) { throw new Exception("Invalid target name"); } }
        Target() { }

        public void AddDependency(Target Target)
        {
            _Dependencies.Add(Target);
        }

        public Target CreateSubTarget(string Name)
        {
            Target target = new Target();
            target._CCompiler = new Property<string>(_CCompiler);
            target._CxxCompiler = new Property<string>(_CxxCompiler);
            target._CsharpCompiler = new Property<string>(_CsharpCompiler);
            target._CCompilationFlags = new Property<string>(_CCompilationFlags);
            target._CxxCompilationFlags = new Property<string>(_CxxCompilationFlags);
            target._CsharpCompilationFlags = new Property<string>(_CsharpCompilationFlags);
            target._Name = Name;
            target._Parent = this;
            return target;
        }

        Test.Result.Result _InternalBuild(string Name, List<Step> steps, Computer.Configuration configuration)
        {
            Test.Result.Group group = new Test.Result.Group(Name);
            foreach (Step step in steps)
            {
                Test.Result.Result substatus = step.Do(this, configuration);
                if (substatus != null)
                {
                    group.AddTestResult(substatus);
                }
            }
            return group;
        }

        public Test.Result.Result _PreBuild(Computer.Configuration Configuration) { return _InternalBuild(Name + ": Prebuild", _PreBuildSteps, Configuration); }
        public Test.Result.Result _Build(Computer.Configuration Configuration) { return _InternalBuild(Name, _BuildSteps, Configuration); }
        public Test.Result.Result _PostBuild(Computer.Configuration Configuration) { return _InternalBuild(Name + ": Postbuild", _PostBuildSteps, Configuration); }

        public Test.Result.Result Build()
        {
            return Build(Computer.Configuration.Current);
        }

        public Test.Result.Result Build(Fact.Computer.Configuration Configuration)
        {
            return Build(Configuration, false);
        }

        Test.Result.Result Build(Fact.Computer.Configuration Configuration, bool AbstractAllowed)
        {
            if (_Abstract && !AbstractAllowed) { throw new Exception("Trying to build an abstract target"); }
            Test.Result.Group group = new Test.Result.Group(Name);

            {
                if (_Parent != null)
                {
                    Test.Result.Result parentStatus = _Parent.Build(Configuration, true);
                    group.AddTestResult(group);
                }
            }

            {
                List<Test.Result.Result> depresults = new List<Test.Result.Result>();
                foreach (Target step in _Dependencies)
                {
                    Test.Result.Result depstatus = step.Build(Configuration, false);
                    depresults.Add(depstatus);
                }
                if (depresults.Count != 0)
                {
                    Test.Result.Group depgroup = new Test.Result.Group("Dependencies");
                    foreach (Test.Result.Result result in depresults)
                    {
                        depgroup.AddTestResult(result);
                    }
                    group.AddTestResult(depgroup);
                }
            }

            Test.Result.Result preBuildStatus = _PreBuild(Configuration);
            group.AddTestResult(preBuildStatus);
            Test.Result.Result buildStatus = _Build(Configuration);
            group.AddTestResult(buildStatus);
            Test.Result.Result postBuildStatus = _PostBuild(Configuration);
            group.AddTestResult(postBuildStatus);

            return group;
        }

        Test.Result.Result Build(string[] Path, int PathOffset, Fact.Computer.Configuration Configuration)
        {
            foreach (Target target in _SubTarget)
            {
                if (target.Name == Path[PathOffset])
                {
                    if (PathOffset >= Path.Length - 1) { return target.Build(Configuration); }
                    else { return target.Build(Path, PathOffset, Configuration); }
                }
            }
            throw new Exception("Target not found");
        }

        public Test.Result.Result Build(string Target, Fact.Computer.Configuration Configuration)
        {
            string[] path = Target.Split('.');
            return Build(path, 0, Configuration);
        }

        public List<string> ListSubTargets()
        {
            List<string> result = new List<string>();
            foreach (Target target in _SubTarget)
            {
                if (!target.Abastract) { result.Add(target.FullName); }
                result.AddRange(target.ListSubTargets());
            }
            return result;
        }

        public string FullName
        {
            get
            {
                string value = "";
                Target target = this;
                while (target != null)
                {
                    value = target._Name + (value == "" ? "" : "." + value);
                    value = value.Trim();
                    target = target._Parent;
                }
                return value;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }
        }

        internal void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, _Name);
            Internal.StreamTools.WriteInt32(Stream, _SubTarget.Count);
            Internal.StreamTools.WriteInt32(Stream, _Dependencies.Count);
            Internal.StreamTools.WriteInt32(Stream, _PreBuildSteps.Count);
            Internal.StreamTools.WriteInt32(Stream, _BuildSteps.Count);
            Internal.StreamTools.WriteInt32(Stream, _PostBuildSteps.Count);
            foreach (Target t in _SubTarget) { t.Save(Stream); }
            foreach (Target t in _Dependencies) { t.Save(Stream); }
            foreach (Step s in _PreBuildSteps) { s.Save(Stream); }
            foreach (Step s in _BuildSteps) { s.Save(Stream); }
            foreach (Step s in _PostBuildSteps) { s.Save(Stream); }
        }

        internal static Target Load(System.IO.Stream Stream)
        {
            Target target = new Target();
            target._Name = Internal.StreamTools.ReadUTF8String(Stream);
            int _SubTargetCount = Internal.StreamTools.ReadInt32(Stream);
            int _DependenciesCount = Internal.StreamTools.ReadInt32(Stream);
            int _PreBuildStepsCount = Internal.StreamTools.ReadInt32(Stream);
            int _BuildStepsCount = Internal.StreamTools.ReadInt32(Stream);
            int _PostBuildStepsCount = Internal.StreamTools.ReadInt32(Stream);
            for (int n = 0; n < _SubTargetCount; n++) { target._SubTarget.Add(Target.Load(Stream)); }
            for (int n = 0; n < _DependenciesCount; n++) { target._Dependencies.Add(Target.Load(Stream)); }
            for (int n = 0; n < _PreBuildStepsCount; n++) { target._PreBuildSteps.Add(Step.Load(Stream)); }
            for (int n = 0; n < _BuildStepsCount; n++) { target._BuildSteps.Add(Step.Load(Stream)); }
            for (int n = 0; n < _PostBuildStepsCount; n++) { target._PostBuildSteps.Add(Step.Load(Stream)); }
            return target;
        }

        internal static Target LoadXML(Fact.Parser.XML.Node Node)
        {
            if (Node.Type.ToLower() != "target") { Node = Node.FindNode("target", false); }
            if (Node == null) { return null; }

            string name = Node["name"];
            Target target = new Target(name);

            Fact.Parser.XML.Node Subtargets = Node.FindNode("subtargets", false);
            foreach (Fact.Parser.XML.Node targetNode in Subtargets.Children) { target._SubTarget.Add(LoadXML(targetNode)); }
            Fact.Parser.XML.Node Dependencies = Node.FindNode("dependencies", false);
            foreach (Fact.Parser.XML.Node targetNode in Dependencies.Children) { target._Dependencies.Add(LoadXML(targetNode)); }
            Fact.Parser.XML.Node Prebuild = Node.FindNode("prebuild", false);
            foreach (Fact.Parser.XML.Node stepNode in Prebuild.Children) { target._PreBuildSteps.Add(Step.LoadXML(stepNode)); }
            Fact.Parser.XML.Node Build = Node.FindNode("build", false);
            foreach (Fact.Parser.XML.Node stepNode in Build.Children) { target._PreBuildSteps.Add(Step.LoadXML(stepNode)); }
            Fact.Parser.XML.Node Postbuild = Node.FindNode("postbuild", false);
            foreach (Fact.Parser.XML.Node stepNode in Postbuild.Children) { target._PreBuildSteps.Add(Step.LoadXML(stepNode)); }

            return target;
        }

        internal void SaveXML(StringBuilder builder)
        {
            if (_Name == "") { builder.Append("<target>"); }
            else { builder.AppendLine("<target name=\"" + _Name + "\">"); }
            if (_SubTarget.Count > 0)
            {
                builder.AppendLine("  <subtargets>");
                foreach (Target t in _SubTarget) { t.SaveXML(builder); }
                builder.AppendLine("  </subtargets>");
            }
            if (_Dependencies.Count > 0)
            {
                builder.AppendLine("  <dependencies>");
                foreach (Target t in _Dependencies) { t.SaveXML(builder); }
                builder.AppendLine("  </dependencies>");
            }
            if (_PreBuildSteps.Count > 0)
            {
                builder.AppendLine("<prebuild>");
                foreach (Step s in _PreBuildSteps) { s.SaveXML(builder); }
                builder.AppendLine("</prebuild>");
            }
            if (_BuildSteps.Count > 0)
            {
                builder.AppendLine("<build>");
                foreach (Step s in _BuildSteps) { s.SaveXML(builder); }
                builder.AppendLine("</build>");
            }
            if (_PostBuildSteps.Count > 0)
            {
                builder.AppendLine("<postbuild>");
                foreach (Step s in _PostBuildSteps) { s.SaveXML(builder); }
                builder.AppendLine("</postbuild>");
            }
            builder.AppendLine("</target>");

        }
    }
}

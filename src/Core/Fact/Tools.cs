﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact
{
    public static class Tools
    {

		internal static string Substring(string Source, int Startindex, int Length)
		{
			StringBuilder builder = new StringBuilder ();

			for (int n = Startindex; n < Source.Length; n++) 
			{
				builder.Append (Source [n]);
			}
			return builder.ToString ();
		}

        /// <summary>
		/// Creates a temp directory in a specific folder.
		/// </summary>
		/// <returns>The path of temp directory.</returns>
        public static string CreateTempDirectory(string Folder)
        {
            try
            {
                string temp = Internal.Information.GetTempFileName();
                if (!System.IO.Directory.Exists(Folder + "/" + temp))
                {
                    RecursiveMakeDirectory(Folder + "/" + temp);
                    if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
                    {
                        AddExecutionPermission(Folder + "/" + temp);
                    }
                    return Folder + "/" + temp;
                }

            }
            catch 
            {
                try
                {
                    // Give back the hand to the system and try again to check if the filesystem was busy
                    System.Threading.Thread.Sleep(0);
                    string temp = Internal.Information.GetTempFileName();
                    if (!System.IO.Directory.Exists(Folder + "/" + temp))
                    {
                        RecursiveMakeDirectory(Folder + "/" + temp);
                        if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
                        {
                            AddExecutionPermission(Folder + "/" + temp);
                        }
                        return Folder + "/" + temp;
                    }
                }
                catch { }
            }
            return "";
        }


		/// <summary>
		/// Creates a temp directory.
		/// </summary>
		/// <returns>The path of temp directory.</returns>
		public static string CreateTempDirectory()
		{
            string directory = "";
            if (Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.AppleMacOSX ||
                Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.Unix)
            {
                if (System.IO.Directory.Exists("/tmp")) { directory = CreateTempDirectory("/tmp"); }
                if (directory != "" && System.IO.Directory.Exists(directory)) { return directory; }
                // really ?
                if (System.IO.Directory.Exists("/temp")) { directory = CreateTempDirectory("/temp"); }
                if (directory != "" && System.IO.Directory.Exists(directory)) { return directory; }
                // No way
                if (System.IO.Directory.Exists("tmp")) { directory = CreateTempDirectory("tmp"); }
                if (directory != "" && System.IO.Directory.Exists(directory)) { return directory; }
            }
            if (Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                try
                {
                    // Yes we are on windows so let .NET do the dirty job for us
                    string path = System.IO.Path.GetTempPath();
                    directory = CreateTempDirectory(path);
                    if (directory != "" && System.IO.Directory.Exists(directory)) { return directory; }
                }
                catch { }
            }
            directory = CreateTempDirectory(".");
            if (directory != "" && System.IO.Directory.Exists(directory)) { return directory; }
			return "";
		}

        /// <summary>
        /// Delete the specified directories recursively.
        /// </summary>
        /// <param name="Paths">An array containing all the directories that must be deleted.</param>
        public static void RecursiveDelete(params string[] Paths)
        {
            foreach (string Path in Paths)
            {
                if (System.IO.File.Exists(Path))
                {
                    try
                    {
                        System.IO.File.Delete(Path);
                        continue;
                    }
                    catch 
                    {
                        // The file system may be busy just sleep and retry
                        try
                        {
                            System.Threading.Thread.Sleep(0);
                            System.IO.File.Delete(Path);
                        }
                        catch { }
                    }
                }
                try
                {
                    if (System.IO.Directory.Exists(Path))
                    {
                        foreach (string file in System.IO.Directory.GetFiles(Path))
                        { RecursiveDelete(file); }
                        foreach (string dir in System.IO.Directory.GetDirectories(Path))
                        { RecursiveDelete(dir); }
                    }
                }
                catch { }

                try
                {
                    System.IO.Directory.Delete(Path);
                    continue;
                }
                catch 
                {
                    // The file system may be busy just sleep and retry
                    try
                    {
                        if (System.IO.Directory.Exists(Path))
                        {
                            System.Threading.Thread.Sleep(0);
                            System.IO.Directory.Delete(Path);
                        }
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Create the specified directories recursively.
        /// </summary>
        /// <param name="Paths">An array containing all the directories that must be created.</param>
        public static void RecursiveMakeDirectory(params string[] Paths)
        {
            foreach (string Path in Paths)
            {
                if (System.IO.Directory.Exists(Path) || System.IO.File.Exists(Path)) { continue; }
                try
                {
                    try
                    {
                        string parentDirectory = "";
                        parentDirectory = System.IO.Path.GetDirectoryName(parentDirectory);
                        if (parentDirectory != "" && !System.IO.Directory.Exists(parentDirectory))
                        {
                            RecursiveMakeDirectory(parentDirectory);
                        }
                    }
                    catch { }
                    try
                    {
                        System.IO.Directory.CreateDirectory(Path);
                    }
                    catch { }
                }
                catch { }
            }
        }

        /// <summary>
        /// Copy recursively a directory into another.
        /// </summary>
        /// <param name="Source">The path containing the directory that must be copied.</param>
        /// <param name="Destination">The destination directory.</param>
        /// 
        public static void RecursiveCopyDirectory(string Source, string Destination)
        {
            RecursiveMakeDirectory(Destination);

            foreach (string dir in System.IO.Directory.GetDirectories(Source))
            {
                try
                {
                    string name = System.IO.Path.GetFileName(dir);
                    RecursiveCopyDirectory(dir, Destination + "/" + name);
                }
                catch { }
            }



            foreach (string file in System.IO.Directory.GetFiles(Source))
            {
                try
                {
                    string name = System.IO.Path.GetFileName(file);
                    System.IO.File.Copy(Source + "/" + file, Destination + "/" + name);
                }
                catch{ }
            }
        }

        public static List<string> GetFiles(params string[] Paths)
        {
            List<string> Files = new List<string>();
            foreach (string path in Paths)
            {
                try
                {
                    foreach (string file in System.IO.Directory.GetFiles(path))
                    {
                        string fullname = file;
                        try
                        {
                            fullname = new System.IO.FileInfo(fullname).FullName;
                        }
                        catch{}
                        Files.Add(fullname);
                    }
                }
                catch { }
            }
            return Files;
        }

        static bool Match(string pattern, string value)
        {
            for (int n = 0; n < pattern.Length; n++)
            {
                if (n >= value.Length) { return false; }
                if (pattern[n] == '?') { }
                if (pattern[n] == '*')
                { 
                    if ( (n + 1) < value.Length && Match(pattern.Substring(n), value.Substring(n + 1))) { return true; }
                }
                else if (pattern[n] != value[n]) { return false; }
            }
            return true;
        }


        public static List<string> ExpandPath(string Path)
        {
            List<string> result = new List<string>();
            if (Path.EndsWith(":\\"))
            {
                result.Add(Path);
                return result;
            }
            string dir;
            try
            {
                dir = System.IO.Path.GetDirectoryName(Path);
            }
            catch { dir = ""; }
            if (dir == "/" || dir == "")
            {
                result.Add(Path);
                return result;
            }
            List<string> Subresult;
            if (dir == Path)
            {
                Subresult = new List<string>();
                Subresult.Add(dir);
            }
            else
            {
                Subresult = ExpandPath(dir);
            }

            string FileName = System.IO.Path.GetFileName(Path);
            foreach (string subdir in Subresult)
            {
                if (System.IO.Directory.Exists(subdir) || subdir == ".")
                {
                    if (System.IO.Directory.Exists(subdir + "/" + FileName))
                    {
                        result.Add(subdir + "/" + FileName);
                    }
                    else if (System.IO.File.Exists(subdir + "/" + FileName))
                    {
                        result.Add(subdir + "/" + FileName);
                    }
                    else
                    {
                        foreach (string subfile in System.IO.Directory.GetFiles(subdir))
                        {
                            string name = System.IO.Path.GetFileName(subfile);
                            if (Match(FileName, name)) { result.Add(subdir + "/" + name); }
                        }
                    }
                }
            }
            return result;
        }

        static public bool AddExecutionPermission(params string[] files)
        {
	            bool Success = true;


	            foreach (string file in files)
	            {
					string fullpath = file;
					try
					{
						System.IO.FileInfo info = new System.IO.FileInfo (file);
						fullpath = info.FullName;
					}
					catch { }

	                if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX ||
	                    Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
	                {
						string restoreLd_Preload = "";
						if (Internal.Information.GetEnvVariable("LD_PRELOAD") != "")
						{
							restoreLd_Preload = Internal.Information.GetEnvVariable ("LD_PRELOAD");
							Internal.Information.SetEnvVariable("LD_PRELOAD", "");
						}

						string escstring = "\"" + fullpath + "\"";
	                    int test = 10;
	                    while (test > 0)
	                    {
	                        try
	                        {
	                            System.Diagnostics.Process ChmodProcess = new System.Diagnostics.Process();
	                            ChmodProcess.StartInfo.FileName = Internal.Information.GetChmodBinary();
								ChmodProcess.StartInfo.CreateNoWindow = true;
	                            ChmodProcess.StartInfo.UseShellExecute = false;
	                            ChmodProcess.StartInfo.RedirectStandardOutput = true;
	                            ChmodProcess.StartInfo.RedirectStandardInput = true;
	                            ChmodProcess.StartInfo.RedirectStandardError = true;
								ChmodProcess.StartInfo.Arguments = " +rx " + escstring;
	                            ChmodProcess.Start();
	                            ChmodProcess.WaitForExit(3000);
                                if (ChmodProcess.ExitCode == 0)
                                {
                                    ChmodProcess.Close();
                                    goto end;
                                }
								ChmodProcess.Close();
	                        }
	                        catch { }
	                        test--;
	                    }
	                    test = 10;
	                    while (test > 0)
	                    {
	                        try
	                        {
	                            System.Diagnostics.Process ChmodProcess = new System.Diagnostics.Process();
	                            ChmodProcess.StartInfo.FileName = Internal.Information.GetChmodBinary();
	                            ChmodProcess.StartInfo.CreateNoWindow = true;
	                            ChmodProcess.StartInfo.UseShellExecute = false;
	                            ChmodProcess.StartInfo.RedirectStandardOutput = true;
	                            ChmodProcess.StartInfo.RedirectStandardInput = true;
	                            ChmodProcess.StartInfo.RedirectStandardError = true;
								ChmodProcess.StartInfo.Arguments = " +x " + escstring;
	                            ChmodProcess.Start();
	                            ChmodProcess.WaitForExit(3000);
                                if (ChmodProcess.ExitCode == 0)
                                {
                                    ChmodProcess.Close();
                                    goto end;
                                }
								ChmodProcess.Close();
	                        }
	                        catch { }
	                        test--;
	                    }
	                    test = 10;
	                    while (test > 0)
	                    {
	                        try
	                        {
	                            System.Diagnostics.Process ChmodProcess = new System.Diagnostics.Process();
	                            ChmodProcess.StartInfo.FileName = Internal.Information.GetChmodBinary();
	                            ChmodProcess.StartInfo.CreateNoWindow = true;
	                            ChmodProcess.StartInfo.UseShellExecute = false;
	                            ChmodProcess.StartInfo.RedirectStandardOutput = true;
	                            ChmodProcess.StartInfo.RedirectStandardInput = true;
	                            ChmodProcess.StartInfo.RedirectStandardError = true;
								ChmodProcess.StartInfo.Arguments = " u+rx " + escstring;
	                            ChmodProcess.Start();
	                            ChmodProcess.WaitForExit(3000);
                                if (ChmodProcess.ExitCode == 0)
                                {
                                    ChmodProcess.Close();
                                    goto end;
                                }
								ChmodProcess.Close();
	                        }
	                        catch { }
	                        test--;
	                    }
	                    test = 10;
	                    while (test > 0)
	                    {
	                        try
	                        {
	                            System.Diagnostics.Process ChmodProcess = new System.Diagnostics.Process();
	                            ChmodProcess.StartInfo.FileName = Internal.Information.GetChmodBinary();
	                            ChmodProcess.StartInfo.CreateNoWindow = true;
	                            ChmodProcess.StartInfo.UseShellExecute = false;
	                            ChmodProcess.StartInfo.RedirectStandardOutput = true;
	                            ChmodProcess.StartInfo.RedirectStandardInput = true;
	                            ChmodProcess.StartInfo.RedirectStandardError = true;
								ChmodProcess.StartInfo.Arguments = " u+x " + escstring;
	                            ChmodProcess.Start();
	                            ChmodProcess.WaitForExit(3000);
                                if (ChmodProcess.ExitCode == 0)
                                {
                                    ChmodProcess.Close();
                                    goto end;
                                }
								ChmodProcess.Close();
	                        }
	                        catch { }
	                        test--;
	                    }

	                    Success = false;
	                end: ;

					if (restoreLd_Preload != "")
					{
						Internal.Information.SetEnvVariable("LD_PRELOAD", restoreLd_Preload);
					}

	                }
	                else
	                { }
	            }
	            return Success;

        }

        static public void Copy(string source, string destination)
        {
            
            try
            {
                if (System.IO.Directory.Exists(source))
                {
                    RecursiveMakeDirectory(destination);
                    foreach (string dir in System.IO.Directory.GetDirectories(source))
                    {
                        Copy(dir, destination + "/" + System.IO.Path.GetFileName(dir));
                    }
                    foreach (string file in System.IO.Directory.GetFiles(source))
                    {
                        try
                        {
                            System.IO.File.Copy(file, destination + "/" + System.IO.Path.GetFileName(file));
                        }
                        catch 
						{

						}
                    }
                }
                else if (System.IO.File.Exists(source))
                {
                    System.IO.File.Copy(source, destination); 
                }
            }
            catch { }
        }

        static public void @UnrestrictedMove(string source, string destination)
        {
            if (Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                if (destination.StartsWith("C:/Windows/"))
                {
                    // Very dark forces. If you are at this point you may be haking fact to
                    // bypass Windows Ressource Protection. It is possible and we will do it
                    // but it should only be used for debug purposes and kernel dev.

                    // I'll assume you are knowing what you are doing if you use this Method.

                    {
                        System.Security.AccessControl.FileSecurity oldacl = System.IO.File.GetAccessControl(destination);
                        System.Security.AccessControl.FileSecurity acl = System.IO.File.GetAccessControl(source);
                        System.Security.AccessControl.FileSecurity takeownership_acl = acl;

                        System.IO.File.SetAccessControl(source, acl);
                        try
                        {
                            Move(source, destination);
                        }
                        catch { }
                        System.IO.File.SetAccessControl(source, oldacl);

                    }
                }
            }
            else
            {
                Move(source, destination);
            }
        }

		static public void Move(string source, string destination)
		{

			try
			{
				if (System.IO.Directory.Exists(source))
				{
					RecursiveMakeDirectory(destination);
					foreach (string dir in System.IO.Directory.GetDirectories(source))
					{
						Move(dir, destination + "/" + System.IO.Path.GetFileName(dir));
					}
					foreach (string file in System.IO.Directory.GetFiles(source))
					{
						try
						{
							System.IO.File.Move(file, destination + "/" + System.IO.Path.GetFileName(file));
						}
						catch { }
					}
				}
				else if (System.IO.File.Exists(source))
				{
					System.IO.File.Move(source, destination); 
				}
			}
			catch { }
        }

    }
}

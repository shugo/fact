﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor
{
    public class OpCodeTree
    {
        OpCodeTree[] _Subtrees = new OpCodeTree[256];
        OpCode[] _OpCodes = new OpCode[256];

        public void Add(OpCode OpCode)
        {
            Add(0, OpCode);
        }

        void Add(int Offset, OpCode OpCode)
        {
            if (OpCode.Length - 1 == Offset)
            {
                byte[] value = OpCode[Offset];
                for (int n = 0; n < value.Length; n++)
                {
                    _OpCodes[value[n]] = OpCode;
                }
            }
            else
            {
                byte[] value = OpCode[Offset];
                for (int n = 0; n < value.Length; n++)
                {
                    if (_Subtrees[value[n]] == null) { _Subtrees[value[n]] = new OpCodeTree(); }
                    _Subtrees[value[n]].Add(Offset + 1, OpCode);
                }
            }
        }

        static System.Reflection.Emit.AssemblyBuilder _AssemblyBuilder;
        static System.Reflection.Emit.ModuleBuilder _ModuleBuilder;
        static OpCodeTree()
        {
            _AssemblyBuilder = System.AppDomain.CurrentDomain.DefineDynamicAssembly(new System.Reflection.AssemblyName("___fact___internal___emulation___"), System.Reflection.Emit.AssemblyBuilderAccess.Run);
            _ModuleBuilder = _AssemblyBuilder.DefineDynamicModule("___jitted___");
        }

#if DEBUG
        static public void DebugInstruction(ulong opcode, string method)
        {
            Fact.Log.Debug("Executing instruction 0x" + opcode.ToString("X") + " -> " + method);
        }
#endif

        static public void RaiseInvalidInstruction(ulong opcode)
        {
            throw new Model.InvalidInstruction(opcode);
        }

        void ConvForParameter(System.Reflection.Emit.ILGenerator ILGenerator, System.Reflection.ParameterInfo param)
        {
            if (param.ParameterType == typeof(SByte)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_I1); }
            if (param.ParameterType == typeof(Int16)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_I2); }
            if (param.ParameterType == typeof(Int32)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_I4); }
            if (param.ParameterType == typeof(Int64)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_I8); }
            if (param.ParameterType == typeof(Byte)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U1); }
            if (param.ParameterType == typeof(UInt16)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U2); }
            if (param.ParameterType == typeof(UInt32)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U4); }
            if (param.ParameterType == typeof(UInt64)) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8); }
        }

        void BuildEmulationTreeIL(System.Reflection.Emit.ILGenerator ILGenerator, System.Reflection.MethodInfo FetchMethod)
        {
            System.Reflection.MethodInfo mRaiseInvalidInstruction = typeof(OpCodeTree).GetMethod("RaiseInvalidInstruction");
#if DEBUG
            System.Reflection.MethodInfo mDebugInstruction = typeof(OpCodeTree).GetMethod("DebugInstruction");
#endif
            bool bigEndian = false;
            int count = 0;
            for (int n = 0; n < 256; n++)
            {
                if (_Subtrees[n] == null && _OpCodes[n] == null) { continue; }
                count++;
            }
            System.Reflection.Emit.Label endOfChunk = ILGenerator.DefineLabel();
            System.Reflection.Emit.Label[] jumpTable = new System.Reflection.Emit.Label[256];
            for (int n = 0; n < 256; n++)
            {
                jumpTable[n] = ILGenerator.DefineLabel();
            }
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldarg_0);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_0);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Dup);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I8, (long)1);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_0);

            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Callvirt, FetchMethod);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Dup);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_1);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4_8);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Shl);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_1);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_I4);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Switch, jumpTable);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_1);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Call, mRaiseInvalidInstruction);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Br, endOfChunk);
            for (int n = 0; n < 256; n++)
            {
                ILGenerator.MarkLabel(jumpTable[n]);
                if (_Subtrees[n] != null)
                {
                    _Subtrees[n].BuildEmulationTreeIL(ILGenerator, FetchMethod);
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Br, endOfChunk);
                }
                else if (_OpCodes[n] != null)
                {
                    System.Reflection.ParameterInfo[] methodParameters = _OpCodes[n]._Method.GetParameters();
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldarg_0);
                    if (methodParameters.Length > 0)
                    {
                        int methodParameterIndex = 0;
                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_1);
                        if (_OpCodes[n].Length == 1) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U1); }
                        if (_OpCodes[n].Length == 2) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U2); }
                        if (_OpCodes[n].Length == 4) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U4); }
                        if (_OpCodes[n].Length == 8) { ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8); }

                        ConvForParameter(ILGenerator, methodParameters[methodParameterIndex]);
                        methodParameterIndex++;
#if DEBUG
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Dup);
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldstr, _OpCodes[n]._Method.Name);

                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Call, mDebugInstruction);

#endif

                        if (_OpCodes[n]._NextEIP)
                        {
                            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_0);
                            if (_OpCodes[n]._ParameterSize > 0)
                            {
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I8, (long)_OpCodes[n]._ParameterSize);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
                            }
                            ConvForParameter(ILGenerator, methodParameters[methodParameterIndex]);
                            methodParameterIndex++;
                        }

                        if (_OpCodes[n]._ParameterSize > 0)
                        {
                            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4_0);
                            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8);
                            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_2);

                            for (int x = 0; x < _OpCodes[n]._ParameterSize; x++)
                            {
                                if (bigEndian)
                                {
                                    if (x != 0)
                                    {
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_2);

                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4_8);
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Shl);
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);

                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_2);
                                    }
                                }
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldarg_0);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_0);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Dup);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I8, (long)1);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_0);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Callvirt, FetchMethod);
                                ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8);

                                if (bigEndian)
                                {
                                    if (x == _OpCodes[n]._ParameterSize - 1)
                                    {
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_2);
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
                                    }
                                }
                                else
                                {

                                    if (x != 0)
                                    {
                                        switch (x)
                                        {
                                            case 1: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4_8); break;
                                            case 2: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4, 16); break;
                                            case 3: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4, 24); break;
                                            case 4: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4, 32); break;
                                            case 5: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4, 40); break;
                                            case 6: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4, 48); break;
                                            case 7: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4, 56); break;
                                            case 8: ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4, 64); break;
                                        }
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Shl);
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_2);
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
                                    }
                                    else
                                    {
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_2);
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
                                    }
                                    if (x != _OpCodes[n]._ParameterSize - 1)
                                    {
                                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_2);
                                    }
                                }
                            }

                            System.Reflection.ParameterInfo[] parameters = _OpCodes[n]._Method.GetParameters();
                            ConvForParameter(ILGenerator, methodParameters[methodParameterIndex]);
                            methodParameterIndex++;

                        }
                        else
                        {
                            System.Reflection.ParameterInfo[] parameters = _OpCodes[n]._Method.GetParameters();
                            if (parameters.Length != 1 && !(parameters.Length == 2 && _OpCodes[n]._NextEIP))
                            {
                                throw new Exception("Method signature does not match the emulation tags: '" + _OpCodes[n]._Method.Name + "'");
                            }
                        }
                    }
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Callvirt, _OpCodes[n]._Method);


                    if (_OpCodes[n]._RelativeJump)
                    {
                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_0);
                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Add);
                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_0);
                    }
                    if (_OpCodes[n]._Jump)
                    {
                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_0);
                    }
                    if (_OpCodes[n]._Return)
                    {
                        ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ret);
                    }
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Br, endOfChunk);

                }
                else
                {
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldloc_1);
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Call, mRaiseInvalidInstruction);
                    ILGenerator.Emit(System.Reflection.Emit.OpCodes.Br, endOfChunk);
                }
            }

            ILGenerator.MarkLabel(endOfChunk);
        }

        static string vprocessorName()
        {
            return "vprocessor_" + Internal.Information.GenerateUUID().Replace("{", "").Replace("}", "").Replace("-", "_").Replace(" ", "_");
        }

        public System.Reflection.MethodInfo BuildEmulationTree(System.Reflection.MethodInfo FetchMethod)
        {
           System.Reflection.Emit.TypeBuilder type = _ModuleBuilder.DefineType(vprocessorName(), System.Reflection.TypeAttributes.Class | System.Reflection.TypeAttributes.Public);
           System.Reflection.Emit.MethodBuilder method = type.DefineMethod("run", System.Reflection.MethodAttributes.Public | System.Reflection.MethodAttributes.Static, null, new Type[] { typeof(object), typeof(ulong) } );

            System.Reflection.Emit.ILGenerator ILGenerator = method.GetILGenerator();
            ILGenerator.DeclareLocal(typeof(ulong));
            ILGenerator.DeclareLocal(typeof(ulong));
            ILGenerator.DeclareLocal(typeof(ulong));


            System.Reflection.Emit.Label exit = ILGenerator.DefineLabel();
            System.Reflection.Emit.Label start = ILGenerator.DefineLabel();
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldarg_1);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_0);
            ILGenerator.MarkLabel(start);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ldc_I4_0);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Conv_U8);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Stloc_1);
            BuildEmulationTreeIL(ILGenerator, FetchMethod);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Br, start);
            ILGenerator.MarkLabel(exit);
            ILGenerator.Emit(System.Reflection.Emit.OpCodes.Ret);
            try
            {

                return type.CreateType().GetMethod("run");
            }
            catch (Exception e)
            {
                Fact.Log.Error("Error while building the model: " + e.Message);
                return null;
            }
        }

        public OpCode Disassemble(byte[] Program, ref int Offset)
        {
            if (_OpCodes[Program[Offset]] != null)
            {
                return _OpCodes[Program[Offset++]];
            }
            else
            {
                if (_Subtrees[Offset] != null)
                {
                    OpCodeTree tree = _Subtrees[Offset];
                    Offset++;
                    return tree.Disassemble(Program, ref Offset);
                }
            }
            return null;
        }
    }
}

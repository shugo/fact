﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor
{
    public class Instructions
    {
        [AttributeUsage(AttributeTargets.Method)]
        public class Jump : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class RelativeJump : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter1Byte : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter2Byte : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter3Byte : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter4Byte : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter8Byte : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class OpCode1Byte : System.Attribute
        {
            byte _OpCode = 0;
            byte _Mask = 0xFF;
            public byte OpCode { get { return _OpCode; } }
            public byte Mask { get { return _Mask; } }

            public OpCode1Byte(byte OpCode) { _OpCode = OpCode; }
            public OpCode1Byte(byte OpCode, byte Mask) { _OpCode = OpCode; _Mask = Mask; }
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class OpCode2Bytes : System.Attribute
        {
            UInt16 _OpCode = 0;
            UInt16 _Mask = 0xFFFF;
            public UInt16 OpCode { get { return _OpCode; } }
            public UInt16 Mask { get { return _Mask; } }

            public OpCode2Bytes(UInt16 OpCode) { _OpCode = OpCode; }
            public OpCode2Bytes(UInt16 OpCode, UInt16 Mask) { _OpCode = OpCode; _Mask = Mask; }
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class OpCode4Bytes : System.Attribute
        {
            UInt32 _OpCode = 0;
            UInt32 _Mask = 0xFFFFFFFF;
            public UInt32 OpCode { get { return _OpCode; } }
            public UInt32 Mask { get { return _Mask; } }

            public OpCode4Bytes(UInt32 OpCode) { _OpCode = OpCode; }
            public OpCode4Bytes(UInt32 OpCode, UInt32 Mask) { _OpCode = OpCode; _Mask = Mask; }
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class OpCode3Bytes : System.Attribute
        {
            UInt32 _OpCode = 0;
            UInt32 _Mask = 0xFFFFFFFF;
            public UInt32 OpCode { get { return _OpCode; } }
            public UInt32 Mask { get { return _Mask; } }

            public OpCode3Bytes(UInt32 OpCode) { _OpCode = OpCode; }
            public OpCode3Bytes(UInt32 OpCode, UInt32 Mask) { _OpCode = OpCode; _Mask = Mask; }
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class OpCode8Bytes : System.Attribute
        {
            UInt64 _OpCode = 0;
            UInt64 _Mask = 0xFFFFFFFFFFFFFFFF;
            public UInt64 OpCode { get { return _OpCode; } }
            public UInt64 Mask { get { return _Mask; } }

            public OpCode8Bytes(UInt64 OpCode) { _OpCode = OpCode; }
            public OpCode8Bytes(UInt64 OpCode, UInt64 Mask) { _OpCode = OpCode; _Mask = Mask; }
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class ParameterNextInstructionPointer : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class ExecuteAndReturn : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Fetch : System.Attribute { }
    }
}

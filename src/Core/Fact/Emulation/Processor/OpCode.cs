﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor
{
    public class OpCode
    {
        internal bool[] _UseOpCode = new bool[0]; internal byte[] _OpCode = new byte[0]; internal byte[] _Mask = new byte[0];

        internal bool _Return = false;
        internal bool _Jump = false;
        internal bool _RelativeJump = false;
        internal int _ParameterSize = 0;
        internal bool _NextEIP = false;

        internal System.Reflection.MethodInfo _Method = null;

        public int Length
        {
            get
            {
                for (int n = _UseOpCode.Length - 1; n >= 0; n++)
                {
                    if (_UseOpCode[n]) { return n + 1; }
                }
                return 0;
            }
        }
        public byte[] this[int Index]
        {
            get
            {
                if (!_UseOpCode[Index] || _Mask[Index] == 0x00)
                {
                    byte[] array = new byte[255];
                    for (int n = 0; n < array.Length; n++) { array[n] = (byte)n; }
                }
                else
                {
                    if (_Mask[Index] == 0xFF) { byte[] array = new byte[1]; array[0] = _OpCode[Index]; return array; }
                    else
                    {
                        List<byte> codes = new List<byte>();
                        for (int n = 0; n < 256; n++)
                        {
                            if ((_OpCode[Index] & _Mask[Index]) == (n & _Mask[Index])) { codes.Add((byte)n); }
                        }
                        return codes.ToArray();
                    }
                }
                return new byte[0];
            }
        }

        public void Add(int Index, byte Opcode, byte Mask)
        {
            if (_UseOpCode.Length >= Index)
            {
                System.Array.Resize<bool>(ref _UseOpCode, Index + 1);
                System.Array.Resize<byte>(ref _OpCode, Index + 1);
                System.Array.Resize<byte>(ref _Mask, Index + 1);
            }

            _UseOpCode[Index] = true;
            _OpCode[Index] = Opcode;
            _Mask[Index] = Mask;

        }

        public OpCode(bool Return, bool Jump, bool RelativeJump, bool NextEIP, int ParameterSize, System.Reflection.MethodInfo Method)
        {
            _Return = Return;
            _Jump = Jump;
            _RelativeJump = RelativeJump;
            _ParameterSize = ParameterSize;
            _Method = Method;
            _NextEIP = NextEIP;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor.Models
{
    unsafe class SPARC64_Generic
    {
        [Fact.Emulation.Processor.Instructions.Fetch]
        public byte Fetch(ulong Location)
        { return *((byte*)(Location)); }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor.Models
{
    unsafe public class x86_64_Generic
    {
        public x86_64_Generic() { }

        public enum Register
        {
            rax = 0,
            rcx = 1,
            rdx = 2,
            rbx = 3,
            rsp = 4,
            rbp = 5,
            rsi = 6,
            rdi = 7,
            r8,
            r9,
            r10,
            r11,
            r12,
            r13,
            r14,
        }

        bool zf = false;
        bool sf = false;
        bool pf = false;
        bool cf = false;
        bool of = false;
        bool af = false;





        Reflection.Memory.VirtualAddressSpace _VASpace;
        public void SetVirtualAddressSpace(Reflection.Memory.VirtualAddressSpace VirtualAddressSpace)
        {
            _VASpace = VirtualAddressSpace;
        }

        ulong[] registers = new ulong[14];
        ulong ip = 0;
        public uint GetRegister_32Bits(Register Register)
        {
            try
            {
                return (uint)(registers[(int)Register] & 0xFFFFFFFF);
            }
            catch { }
            return 0x0;
        }
        public ulong GetRegister(Register Register)
        {
            try
            {
                return registers[(int)Register];
            }
            catch { }
            return 0x0;
        }
        public void SetRegister_32Bits(Register Register, uint Value)
        {
            try
            {
                registers[(int)Register] = (registers[(int)Register] & 0xFFFFFFFF00000000) | (ulong)(Value & 0xFFFFFFFF);
            }
            catch { }
        }
        public void SetRegister(Register Register, ulong Value)
        {
            try
            {
                registers[(int)Register] = Value;
            }
            catch { }
        }

        public void Push(Register Register)
        {
            ulong* ptr = (ulong*)registers[(int)Register.rsp];
            *ptr = registers[(int)Register];
            registers[(int)Register.rsp] -= sizeof(ulong);
        }

        public void PushValue(ulong Value)
        {
            ulong* ptr = (ulong*)registers[(int)Register.rsp];
            *ptr = Value;
            registers[(int)Register.rsp] -= sizeof(ulong);
        }

        public void Pop(Register Register)
        {
            registers[(int)Register.rsp] += sizeof(ulong);
            ulong* ptr = (ulong*)registers[(int)Register.rsp];
            registers[(int)Register] = *ptr;
        }

        public ulong Pop()
        {
            registers[(int)Register.rsp] += sizeof(ulong);
            ulong* ptr = (ulong*)registers[(int)Register.rsp];
            ulong value = *ptr;
            return value;
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x50, 0xF8)]
        public void Push_R(ulong Instr)
        {
            Register destination = (Register)(Instr & 0x07);

            Push(destination);
#if DEBUG
            Fact.Log.Debug("push " + destination);
#endif
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x58, 0xF8)]
        public void Pop_R(ulong Instr)
        {
            Register destination = (Register)(Instr & 0x07);

            Pop(destination);
#if DEBUG
            Fact.Log.Debug("pop " + destination);
#endif
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0x8980, 0xFF80)]
        public void Mov_RR(ulong Instr)
        {
            Register destination = (Register)(Instr & 0x07);
            Register source = (Register)((Instr >> 3) & 0x07);
            SetRegister_32Bits(destination, GetRegister_32Bits(source));
#if DEBUG
            Fact.Log.Debug("mov e" + destination.ToString().Substring(1) + ", e" + source.ToString().Substring(1));
#endif
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0x8900, 0xFF80)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public void Mov_DerefR_R(ulong Instr, byte Offset)
        {
            Register destination = (Register)(Instr & 0x07);
            Register source = (Register)((Instr >> 3) & 0x07);
            unchecked
            {
                long soffset = (sbyte)(Offset);
                ((ulong*)((long)GetRegister(destination) + soffset))[0] = GetRegister_32Bits(source);
#if DEBUG
            Fact.Log.Debug("mov [" + destination.ToString() + " + " + soffset + "], e" + source.ToString().Substring(1));
#endif
            }

        }

        [Fact.Emulation.Processor.Instructions.OpCode3Bytes(0x488980, 0xFFFF80)]
        public void REX_Mov_RR(ulong Instr)
        {
            Register destination = (Register)(Instr & 0x07);
            Register source = (Register)((Instr >> 3) & 0x07);
            SetRegister(destination, GetRegister(source));
#if DEBUG
            Fact.Log.Debug("mov " + destination.ToString() + ", " + source.ToString());
#endif
        }

        [Fact.Emulation.Processor.Instructions.OpCode3Bytes(0x488900, 0xFFFF80)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public ulong REX_Mov_DerefR_R(ulong Instr, ulong NextIP, byte Offset)
        {
            Register destination = (Register)(Instr & 0x07);
            Register source = (Register)((Instr >> 3) & 0x07);

            if (destination == Register.rsp)
            {
                // Stack op with move have one extra opcode
                unchecked
                {
                    long soffset = (sbyte)(Fetch(NextIP));
                    ((ulong*)((long)GetRegister(destination) + soffset))[0] = GetRegister(source);
#if DEBUG
                    Fact.Log.Debug("mov [" + destination.ToString() + " + " + soffset + "], " + source.ToString());
#endif
                }
                return NextIP + 1;
            }
            else
            {

                unchecked
                {
                    long soffset = (sbyte)(Offset);
                    ((ulong*)((long)GetRegister(destination) + soffset))[0] = GetRegister(source);
#if DEBUG
                    Fact.Log.Debug("mov [" + destination.ToString() + " + " + soffset + "], " + source.ToString());
#endif
                }
            }
            return NextIP;
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0x48B8, 0xFF80)]
        [Fact.Emulation.Processor.Instructions.Parameter8Byte]
        public void REX_Mov_R_Const(ulong Instr, ulong Value)
        {
            Register destination = (Register)(Instr & 0x07);
            SetRegister(destination, Value);
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("mov " + destination.ToString() + ", 0x" + Value.ToString("X"));
#endif
            }

        }


        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xB8, 0x80)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void Mov_R_Const(ulong Instr, uint Value)
        {
            Register destination = (Register)(Instr & 0x07);
            SetRegister_32Bits(destination, Value);
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("mov e" + destination.ToString().Substring(1) + ", 0x" + Value.ToString("X"));
#endif
            }

        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0xC740, 0xFFF0)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public ulong Mov_Deref_R_Const(ushort Instr, ulong NextIP, sbyte Offset)
        {
            Register destination = (Register)((Instr & 0x0700) >> 8);
            ulong value = Fetch_32(NextIP);
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("mov [" + destination.ToString() + " + " + Offset + "], 0x" + value.ToString("X"));
#endif
            }

            return NextIP + 4;
        }

        [Fact.Emulation.Processor.Instructions.OpCode3Bytes(0x4C8900, 0xFFFF80)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public ulong REXWR_Mov_DerefR_R(ulong Instr, ulong NextIP, byte Offset)
        {
            Register destination = (Register)(Instr & 0x07);
            Register source = (Register)(((Instr >> 3) & 0x07) | 0x8);

            if (destination == Register.rsp)
            {
                // Stack op with move have one extra opcode
                unchecked
                {
                    long soffset = (sbyte)(Fetch(NextIP));
                    ((ulong*)((long)GetRegister(destination) + soffset))[0] = GetRegister(source);
#if DEBUG
                    Fact.Log.Debug("mov [" + destination.ToString() + " + " + soffset + "], " + source.ToString());
#endif
                }
                return NextIP + 1;
            }
            else
            {

                unchecked
                {
                    long soffset = (sbyte)(Offset);
                    ((ulong*)((long)GetRegister(destination) + soffset))[0] = GetRegister(source);
#if DEBUG
                    Fact.Log.Debug("mov [" + destination.ToString() + " + " + soffset + "], " + source.ToString());
#endif
                }
            }
            return NextIP;
        }

        [Fact.Emulation.Processor.Instructions.OpCode3Bytes(0x488B00, 0xFFFF80)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public ulong REX_Mov_R_DerefR(ulong Instr, ulong NextIP, byte Offset)
        {
            Register destination = (Register)((Instr >> 3) & 0x07);
            Register source = (Register)(Instr & 0x07);

            if (source == Register.rsp)
            {
                // Stack op with move have one extra opcode
                unchecked
                {
                    long soffset = (sbyte)(Fetch(NextIP));
                    SetRegister(destination, ((ulong*)((long)GetRegister(source) + soffset))[0]);
#if DEBUG
                    Fact.Log.Debug("mov " + destination + ", [" + source.ToString() + " + " + soffset + "]");
#endif
                }
                return NextIP + 1;
            }
            else
            {

                unchecked
                {
                    long soffset = (sbyte)(Offset);
                    SetRegister(destination, ((ulong*)((long)GetRegister(source) + soffset))[0]);
#if DEBUG
                    Fact.Log.Debug("mov " + destination + ", [" + source.ToString() + " + " + soffset + "]");
#endif
                }
            }
            return NextIP;

        }

        [Fact.Emulation.Processor.Instructions.OpCode3Bytes(0x4C8B00, 0xFFFF80)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public ulong REXWR_Mov_R_DerefR(ulong Instr, ulong NextIP, byte Offset)
        {
            Register destination = (Register)(((Instr >> 3) & 0x07) | 0x8);
            Register source = (Register)(Instr & 0x07);

            if (source == Register.rsp)
            {
                // Stack op with move have one extra opcode
                unchecked
                {
                    long soffset = (sbyte)(Fetch(NextIP));
                    SetRegister(destination, ((ulong*)((long)GetRegister(source) + soffset))[0]);
#if DEBUG
                    Fact.Log.Debug("mov " + destination + ", [" + source.ToString() + " + " + soffset + "]");
#endif
                }
                return NextIP + 1;
            }
            else
            {

                unchecked
                {
                    long soffset = (sbyte)(Offset);
                    SetRegister(destination, ((ulong*)((long)GetRegister(source) + soffset))[0]);
#if DEBUG
                    Fact.Log.Debug("mov " + destination + ", [" + source.ToString() + " + " + soffset + "]");
#endif
                }
            }
            return NextIP;

        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0x8B00, 0xFF80)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public ulong Mov_R_DerefR(ulong Instr, ulong NextIP, byte Offset)
        {
            Register destination = (Register)(((Instr >> 3) & 0x07));
            Register source = (Register)(Instr & 0x07);

            if (source == Register.rsp)
            {
                // Stack op with move have one extra opcode
                unchecked
                {
                    long soffset = (sbyte)(Fetch(NextIP));
                    SetRegister_32Bits(destination, ((uint*)((long)GetRegister(source) + soffset))[0]);
#if DEBUG
                    Fact.Log.Debug("mov e" + destination.ToString().Substring(1) + ", [" + source.ToString() + " + " + soffset + "]");
#endif
                }
                return NextIP + 1;
            }
            else
            {

                unchecked
                {
                    long soffset = (sbyte)(Offset);
                    SetRegister_32Bits(destination, ((uint*)((long)GetRegister(source) + soffset))[0]);
#if DEBUG
                    Fact.Log.Debug("mov e" + destination.ToString().Substring(1) + ", [" + source.ToString() + " + " + soffset + "]");
#endif
                }
            }
            return NextIP;

        }

        [Fact.Emulation.Processor.Instructions.OpCode3Bytes(0x4881E0, 0xFFFF80)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void REX_ALU_RC16_32(ulong Instr, int Value)
        {
            unchecked
            {
                Register source = (Register)((Instr) & 0x07);
                ulong mode = (ulong)((Instr & 0x38) >> 3);
                switch (mode)
                {
                    case 0x2:
                        SetRegister(source, (ulong)((long)GetRegister(source) + (long)Value));
#if DEBUG
                        Fact.Log.Debug("add " + source.ToString() + ", 0x" + Value.ToString("X"));
#endif
                        break;
                    case 0x4:
                        SetRegister(source, (ulong)((ulong)GetRegister(source) & (ulong)Value));
#if DEBUG
                        Fact.Log.Debug("and " + source.ToString() + ", 0x" + Value.ToString("X"));
#endif
                        break;
                    case 0x5:
                        SetRegister(source, (ulong)((long)GetRegister(source) - (long)Value));
#if DEBUG
                        Fact.Log.Debug("sub " + source.ToString() + ", 0x" + Value.ToString("X"));
#endif
                        break;
                    default:
                        throw new Exception("Invalid ALU operation (" + mode + ")");
                }
            }
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0x4883)]
        [Fact.Emulation.Processor.Instructions.Parameter2Byte]
        public void REX_ALU_RC8(ulong Instr, ushort Params)
        {
            Register source = (Register)((Params) & 0x07);
            ulong mode = (ulong)((Params & 0x38) >> 3);
            ulong value = (ulong)(Params & 0xFF00) >> 8;
           
            switch (mode)
            {
                case 0x2:
                    SetRegister(source, GetRegister(source) + value);
#if DEBUG
                    Fact.Log.Debug("add " + source.ToString() + ", 0x" + value.ToString("X"));
#endif
                    break;
                case 0x4:
                    SetRegister(source, (ulong)((ulong)GetRegister(source) & (ulong)value));
#if DEBUG
                    Fact.Log.Debug("and " + source.ToString() + ", 0x" + value.ToString("X"));
#endif
                    break;
                case 0x5:
                    SetRegister(source, GetRegister(source) - value);
#if DEBUG
                    Fact.Log.Debug("sub " + source.ToString() + ", 0x" + value.ToString("X"));
#endif
                    break;
                default:
                    throw new Exception("Invalid ALU operation (" + mode + ")");
            }
        }


        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x83)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        [Fact.Emulation.Processor.Instructions.Parameter2Byte]
        public ulong ALU_RC8(ulong Instr, ulong NextIP, ushort Params)
        {
            Register source = (Register)((Params) & 0x07);
            ulong mode = (ulong)((Params & 0x38) >> 3);

            if (source == Register.rbp)
            {
                sbyte offset = (sbyte)((Params & 0xFF00) >> 8);
                ulong value = Fetch(NextIP);

                switch (mode)
                {
                    case 0x2:
                        SetRegister(source, GetRegister(source) + value);
#if DEBUG
                        Fact.Log.Debug("add [" + source.ToString() + " + " + offset + "], 0x" + value.ToString("X"));
#endif
                        break;
                    case 0x4:
                        SetRegister(source, (ulong)((ulong)GetRegister(source) & (ulong)value));
#if DEBUG
                        Fact.Log.Debug("and [" + source.ToString() + " + " + offset + "], 0x" + value.ToString("X"));
#endif
                        break;
                    case 0x5:
                        SetRegister(source, GetRegister(source) - value);
#if DEBUG
                        Fact.Log.Debug("sub [" + source.ToString() + " + " + offset + "], 0x" + value.ToString("X"));
#endif
                        break;
                    case 0x7:
#if DEBUG
                        Fact.Log.Debug("cmp [" + source.ToString() + " + " + offset + "], 0x" + value.ToString("X"));
#endif
                        break;
                    default:
                        throw new Exception("Invalid ALU operation (" + mode + ")");
                }

                return NextIP + 1;
            }
            else
            {

                ulong value = (ulong)(Params & 0xFF00) >> 8;

                switch (mode)
                {
                    case 0x2:
                        SetRegister(source, GetRegister(source) + value);
#if DEBUG
                        Fact.Log.Debug("add " + source.ToString() + ", 0x" + value.ToString("X"));
#endif
                        break;
                    case 0x4:
                        SetRegister(source, (ulong)((ulong)GetRegister(source) & (ulong)value));
#if DEBUG
                        Fact.Log.Debug("and " + source.ToString() + ", 0x" + value.ToString("X"));
#endif
                        break;
                    case 0x5:
                        SetRegister(source, GetRegister(source) - value);
#if DEBUG
                        Fact.Log.Debug("sub " + source.ToString() + ", 0x" + value.ToString("X"));
#endif
                        break;
                    default:
                        throw new Exception("Invalid ALU operation (" + mode + ")");
                }
                return NextIP;
            }
        }

        [Fact.Emulation.Processor.Instructions.OpCode3Bytes(0x488D00, 0xFFFF80)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void REX_Lea(ulong Instr, uint Base)
        {
            Register destination = (Register)((Instr >> 3) & 0x07);
            Register source = (Register)(Instr & 0x07);
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("lea " + destination + ", " + Base.ToString() + " + " + source + " (not implemented)");
#endif
            }

        }

        bool TryCallSpecialFuntion(ulong Address, bool directJump, ref ulong NextIP)
        {
            if (_VASpace != null && Reflection.Memory.MMU._SpecialAddress.ContainsKey(Address))
            {
                if (directJump)
                {
                    NextIP = Pop();
                }
                Delegate function = Reflection.Memory.MMU._SpecialAddress[Address];
                System.Reflection.MethodInfo method = function.Method;
#if DEBUG
                Fact.Log.Debug("call " + method.Name);
#endif
                System.Reflection.ParameterInfo[] parametersInfo = method.GetParameters();
                object[] parameters = new object[parametersInfo.Length];
                int IntRegisterOffset = 0;

                for (int n = 0; n < parameters.Length; n++)
                {
                    if (parametersInfo[n].ParameterType.FullName == typeof(int).FullName) { }
                }

                object result = method.Invoke(function.Target, parameters);

                unchecked
                {
                    if (method.ReturnType.FullName == typeof(void).FullName) { return true; }
                    else if (method.ReturnType.FullName == typeof(ulong).FullName) { SetRegister(Register.rax, (ulong)result); }
                    else if (method.ReturnType.FullName == typeof(long).FullName) { SetRegister(Register.rax, (ulong)((long)result)); }
                    else if (method.ReturnType.FullName == typeof(int).FullName) { SetRegister(Register.rax, (ulong)((int)result)); }
                    else if (method.ReturnType.FullName == typeof(uint).FullName) { SetRegister(Register.rax, (ulong)((int)result)); }
                    else if (method.ReturnType.FullName == typeof(IntPtr).FullName) { SetRegister(Register.rax, (ulong)((IntPtr)result).ToPointer()); }
                    else if (method.ReturnType.FullName == typeof(void*).FullName) { SetRegister(Register.rax, (ulong)System.Reflection.Pointer.Unbox(result)); }

                    else { throw new Exception("Invalid return type for special function: " + method.Name); }
                }



                return true;
            }
            else
            {
                return false;
            }
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xE8)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Call(ulong Instr, ulong NextIP, int Offset)
        {
            if (TryCallSpecialFuntion((ulong)((long)NextIP + (long)Offset), false, ref NextIP)) { return NextIP; }
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("call 0x" + ((ulong)((long)NextIP + (long)Offset)).ToString("X"));
#endif
            }
            PushValue(NextIP);
            return (ulong)(NextIP + (ulong)Offset);
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0xFF25)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jump_Deref(ulong Instr, ulong NextIP, int Address)
        {
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("jmp dword [rel 0x" + ((long)NextIP + (long)Address).ToString("X") + "]");
#endif
            }

            ulong addr =
                (ulong)Fetch((ulong)((long)NextIP + (long)Address)) +
                (ulong)Fetch((ulong)((long)NextIP + (long)Address + 1)) * 0x100ul +
                (ulong)Fetch((ulong)((long)NextIP + (long)Address + 2)) * 0x10000ul +
                (ulong)Fetch((ulong)((long)NextIP + (long)Address + 3)) * 0x1000000ul +
                (ulong)Fetch((ulong)((long)NextIP + (long)Address + 4)) * 0x100000000ul +
                (ulong)Fetch((ulong)((long)NextIP + (long)Address + 5)) * 0x10000000000ul;
            if (TryCallSpecialFuntion(addr, true, ref NextIP)) { return NextIP; }

            return addr;
        }


        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x75)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jump_Neq(ulong Instr, ulong NextIP, sbyte Offset)
        {
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("jne 0x" + ((long)NextIP + (long)Offset).ToString("X"));
#endif
            }
            if (zf) { return NextIP; }
            else { return (ulong)((long)NextIP + Offset); }
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xEB)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jump_Short(ulong Instr, ulong NextIP, sbyte Offset)
        {
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("jmp 0x" + ((long)NextIP + (long)Offset).ToString("X"));
#endif
            }
            return (ulong)((long)NextIP + Offset);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x74)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jump_eq(ulong Instr, ulong NextIP, sbyte Offset)
        {
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("je 0x" + ((long)NextIP + (long)Offset).ToString("X"));
#endif
            }
            if (zf) { return (ulong)((long)NextIP + Offset); }
            else { return NextIP; }
        }


        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x7C)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jump_lw(ulong Instr, ulong NextIP, sbyte Offset)
        {
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("jl 0x" + ((long)NextIP + (long)Offset).ToString("X"));
#endif
            }
            if (sf != of) { return (ulong)((long)NextIP + Offset); }
            else { return NextIP; }
        }


        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x7F)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jump_ge(ulong Instr, ulong NextIP, sbyte Offset)
        {
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("jge 0x" + ((long)NextIP + (long)Offset).ToString("X"));
#endif
            }
            if ((sf == of) || zf) { return (ulong)((long)NextIP + Offset); }
            else { return NextIP; }
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0xFFD0)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Call_Register(ulong Instr, ulong NextIP)
        {
            Register register = (Register)(Instr & 0x07);
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("call " + register.ToString());
#endif
            }
            if (TryCallSpecialFuntion(GetRegister(register), false, ref NextIP)) { return NextIP; }
            PushValue(NextIP);
            return GetRegister(register);

        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0xFFE0)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jump_Register(ulong Instr, ulong NextIP)
        {
            Register register = (Register)(Instr & 0x07);
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("jump " + register.ToString());
#endif
            }
            if (TryCallSpecialFuntion(GetRegister(register), true, ref NextIP)) { return NextIP; }
            return GetRegister(register);

        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x85)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public void Test(ulong Instr, byte Params)
        {
            Register registerA = (Register)(Params & 0x07);
            Register registerB = (Register)((Params >> 3) & 0x07);

            unchecked
            {
#if DEBUG
                Fact.Log.Debug("test e" + registerA.ToString().Substring(1) + ", e" + registerB.ToString().Substring(1));
#endif
                uint a = GetRegister_32Bits(registerA);
                uint b = GetRegister_32Bits(registerB);
                uint result = a & b;
                cf = false;
                of = false;
                sf = (result & 0x80000000) != 0;
                zf = result == 0;
            }

        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0x3B40, 0xFFC0)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public void Cmp_R_Deref_R_Offset(ulong Instr, sbyte Offset)
        {
            Register registerB = (Register)((Instr) & 0x07);
            Register registerA = (Register)((Instr >> 3) & 0x07);
            unchecked
            {
#if DEBUG
                Fact.Log.Debug("cmp e" + registerA.ToString().Substring(1) + ", [" + registerB.ToString() + " + " + Offset + "]");
#endif
                int a = (int)GetRegister_32Bits(registerA);
                int b = *((int*)((long)GetRegister(registerB) + Offset));
                int result = b - a;
                cf = false;
                of = ((long)b - (long)a) != (long)result;
                sf = ((uint)result & 0x80000000) != 0;
                zf = result == 0;
            }

        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x90)]
        public void Nop(ulong Instr) { }

        uint Fetch_32(ulong Location)
        {
            return
                ((uint)Fetch((ulong)((long)Location)) +
                (uint)Fetch((ulong)((long)Location + 1)) * 0x100 +
                (uint)Fetch((ulong)((long)Location + 2)) * 0x10000 +
                (uint)Fetch((ulong)((long)Location + 3)) * 0x1000000);
        }

        [Fact.Emulation.Processor.Instructions.Fetch]
        public byte Fetch(ulong Location)
        { return *((byte*)(Location)); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xC3)]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Ret(byte OpCode)
        {
            return Pop();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor.Models
{
    public unsafe class MSIL_Generic
    {
        Internal.Reflection.MSILResolver resolver;

        object[] args = new object[256];
        object[] locals = new object[256];

        int methodId = 0;
        byte[] methodData = new byte[0];
        Stack<ulong> retptr = new Stack<ulong>();

        Stack<Internal.Reflection.MSILResolver> resolverstack = new Stack<Internal.Reflection.MSILResolver>();
        Stack<object> stack = new Stack<object>();
        Stack<Stack<object>> stackstack = new Stack<Stack<object>>();

        System.Reflection.MethodBase currentMethod = null;
        Stack<System.Reflection.MethodBase> methodstack = new Stack<System.Reflection.MethodBase>();


        Stack<object[]> argsstack = new Stack<object[]>();
        Stack<object[]> localstack = new Stack<object[]>();

        internal class ModuleResolver : Internal.Reflection.MSILResolver
        {
            System.Reflection.Module _Module;
            public ModuleResolver(System.Reflection.Module Module) { _Module = Module; }
            public override System.Reflection.FieldInfo ResolveField(int Metadatatoken)
            {
                return _Module.ResolveField(Metadatatoken);
            }
            public override System.Reflection.MethodBase ResolveMethod(int Metadatatoken)
            {
                return _Module.ResolveMethod(Metadatatoken);
            }
            public override Type ResolveType(int Metadatatoken)
            {
                return _Module.ResolveType(Metadatatoken);
            }
            public override string ResolveString(int Metadatatoken)
            {
                return _Module.ResolveString(Metadatatoken);
            }
        }

        public void PrepareFirstCall(System.Reflection.MethodBase Method, object Thisptr, params object[] Parameters)
        {
            methodId = Method.MetadataToken;
            methodData = Method.GetMethodBody().GetILAsByteArray();
            currentMethod = Method;
            resolver = new ModuleResolver(Method.Module);
            retptr.Clear();
            stack.Clear();
            resolverstack.Clear();
            argsstack.Clear();
            localstack.Clear();
            stackstack.Clear();
            methodstack.Clear();
            args = new object[Parameters.Length + (Thisptr != null ? 1 : 0)];
            int argn = 0;
            if (Thisptr != null) { args[argn] = Thisptr; argn++; }
            for (int n = 0; n < Parameters.Length; n++)
            {
                args[argn] = Parameters[n];
                argn++;
            }
            locals = new object[Method.GetMethodBody().LocalVariables.Count];
        }

        public MSIL_Generic() { }

        [Fact.Emulation.Processor.Instructions.Fetch]
        public byte Fetch(ulong Location)
        {
            uint method = (uint)(Location >> 32);
            if (method == 0) { return 0xFF; }
            uint offset = (uint)(Location & 0xFFFFFFFF);
            return methodData[offset];
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x00)]
        public void Nop(ulong Instr) { }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x01)]
        public void Break(ulong Instr) { System.Diagnostics.Debugger.Break(); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x02)]
        public void LdArg0(ulong Instr) { stack.Push(args[0]); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x03)]
        public void LdArg1(ulong Instr) { stack.Push(args[1]); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x04)]
        public void LdArg2(ulong Instr) { stack.Push(args[2]); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x05)]
        public void LdArg3(ulong Instr) { stack.Push(args[3]); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x06)]
        public void LdLoc0(ulong Instr) { LdLoc(Instr, 0); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x07)]
        public void LdLoc1(ulong Instr) { LdLoc(Instr, 1); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x08)]
        public void LdLoc2(ulong Instr) { LdLoc(Instr, 2); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x09)]
        public void LdLoc3(ulong Instr) { LdLoc(Instr, 3); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x0A)]
        public void StLoc0(ulong Instr) { StLoc(Instr, 0); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x0B)]
        public void StLoc1(ulong Instr) { StLoc(Instr, 1); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x0C)]
        public void StLoc2(ulong Instr) { StLoc(Instr, 2); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x0D)]
        public void StLoc3(ulong Instr) { StLoc(Instr, 3); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x0E)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public void LdArgS(ulong Instr, byte Arg) { LdArg(Instr, Arg); }


        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x12)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public void LdLocS(ulong Instr, byte Local) { LdLoc(Instr, Local); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x13)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public void StLocS(ulong Instr, byte Local) { StLoc(Instr, Local); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x14)]
        public void LdNull(ulong Instr) { stack.Push(null); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x15)]
        public void LdcI4_M1(ulong Instr) { LdcI4(Instr, -1); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x16)]
        public void LdcI4_0(ulong Instr) { LdcI4(Instr, 0); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x17)]
        public void LdcI4_1(ulong Instr) { LdcI4(Instr, 1); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x18)]
        public void LdcI4_2(ulong Instr) { LdcI4(Instr, 2); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x19)]
        public void LdcI4_3(ulong Instr) { LdcI4(Instr, 3); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x1A)]
        public void LdcI4_4(ulong Instr) { LdcI4(Instr, 4); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x1B)]
        public void LdcI4_5(ulong Instr) { LdcI4(Instr, 5); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x1C)]
        public void LdcI4_6(ulong Instr) { LdcI4(Instr, 6); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x1D)]
        public void LdcI4_7(ulong Instr) { LdcI4(Instr, 7); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x1E)]
        public void LdcI4_8(ulong Instr) { LdcI4(Instr, 8); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x1F)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        public void LdcI4_S(ulong Instr, SByte value) { LdcI4(Instr, value); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x20)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void LdcI4(ulong Instr, Int32 value) { stack.Push(value); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x25)]
        public void Dup(ulong Instr) { stack.Push(stack.Peek()); }
        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x26)]
        public void Pop(ulong Instr) { stack.Pop(); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x27)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Jmp(ulong Instr, ulong NextEIP, UInt32 Method)
        {

            ulong result = Call(Instr, NextEIP, Method);
            // erase the ret site
            argsstack.Pop();
            localstack.Pop();
            resolverstack.Pop();
            stackstack.Pop();
            methodstack.Pop();
            retptr.Pop();
            return result;
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x28)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Call(ulong Instr, ulong NextEIP, UInt32 Method)
        {
            methodstack.Push(currentMethod);
            currentMethod = resolver.ResolveMethod((int)Method);
            resolverstack.Push(resolver);

            resolver = new ModuleResolver(currentMethod.Module);
            if (currentMethod == null)
            {
                throw new Exception("Method not found: 0x" + Method.ToString("X"));
            }
            methodId = (int)Method;
            argsstack.Push(args);
            int offset = (currentMethod.IsStatic || (currentMethod is System.Reflection.ConstructorInfo) ? 0 : 1);
            System.Reflection.ParameterInfo[] parameters = currentMethod.GetParameters();
            args = new object[parameters.Length + offset];
            for (int n = args.Length - 1; n >= 0; n--)
            {
                args[n] = stack.Pop();
            }

            // Constructor will perform first data allocation and then pass this ptr to arg0
            // However since we interop with the host .NET we have to let it doing the allocation
            if (currentMethod.GetMethodBody() == null ||
                currentMethod is System.Reflection.ConstructorInfo)
            {
#if DEBUG
                Fact.Log.Debug("No bytecode for method '" + currentMethod.Name + "' direct call will be made" );
#endif
                object fakeThisptr = null;
                List<object> fakeArgs = new List<object>();
                if (!currentMethod.IsStatic && !(currentMethod is System.Reflection.ConstructorInfo)) { fakeThisptr = args[0]; }

                if (currentMethod.DeclaringType.Name == "String" && fakeThisptr is char)
                {
                    fakeThisptr = ((char)fakeThisptr).ToString();
                }

                for (int n = offset; n < args.Length;  n++)
                {
                    object arg = args[n];
                    if (currentMethod.DeclaringType.Name == "String" && arg is char) { arg = ((char)arg).ToString(); }
                    fakeArgs.Add(arg);
                }
                object result = null;
                if (currentMethod is System.Reflection.ConstructorInfo)
                {
                    result = (currentMethod as System.Reflection.ConstructorInfo).Invoke(fakeArgs.ToArray());
                }
                else
                {
                    result = currentMethod.Invoke(fakeThisptr, fakeArgs.ToArray());
                }
                args = argsstack.Pop();
                if (currentMethod is System.Reflection.ConstructorInfo)
                {
                    stack.Push(result);
                }
                else
                {
                    if ((currentMethod as System.Reflection.MethodInfo).ReturnType != null &&
                        (currentMethod as System.Reflection.MethodInfo).ReturnType != typeof(void))
                    {
                        stack.Push(result);
                    }
                }
                currentMethod = methodstack.Pop();
                resolver = resolverstack.Pop();
                return NextEIP;
            }

            localstack.Push(locals);
            locals = new object[currentMethod.GetMethodBody().LocalVariables.Count];
            stackstack.Push(stack);
            stack = new Stack<object>();
            retptr.Push(NextEIP);
            methodData = currentMethod.GetMethodBody().GetILAsByteArray();
            ulong ptr = (((ulong)Method) << 32);
#if DEBUG
            if (currentMethod is System.Reflection.ConstructorInfo)
            {
                Fact.Log.Debug("call " + currentMethod.DeclaringType + ".ctor");
            }
            else
            {
                Fact.Log.Debug("call " + currentMethod.Name);
            }
#endif
            return ptr;
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x2A)]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Ret(ulong Instr)
        {
            if (retptr.Count == 0) { return 0; }
            ulong retvalue = retptr.Pop();
            uint method = (uint)(retvalue >> 32);
            uint offset = (uint)(retvalue & 0xFFFFFFFF);
            System.Reflection.MethodBase oldMethod = currentMethod;
            methodId = (int)method;
            currentMethod = methodstack.Pop();
            methodData = currentMethod.GetMethodBody().GetILAsByteArray();
            resolver = resolverstack.Pop();
            args = argsstack.Pop();
            locals = localstack.Pop();

            if (oldMethod is System.Reflection.MethodInfo)
            {
                if ((oldMethod as System.Reflection.MethodInfo).ReturnType != null &&
                    (oldMethod as System.Reflection.MethodInfo).ReturnType != typeof(void))
                {
                    object returnValue = stack.Pop();
                    stack = stackstack.Pop();
                    stack.Push(returnValue);
                }
                else
                {
                    stack = stackstack.Pop();
                }
            }
            else if (oldMethod is System.Reflection.ConstructorInfo)
            {
                object returnValue = stack.Pop();
                stack = stackstack.Pop();
                stack.Push(returnValue);
            }
            else
            {
                throw new InvalidProgramException("Unknown method type");
            }

            return retvalue;
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x2B)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BrS(ulong Instr, SByte Target) { return Br(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x2C)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BrZeroS(ulong Instr, SByte Target) { return BrZero(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x2D)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BrInstS(ulong Instr, SByte Target) { return BrInst(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x2E)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BeqS(ulong Instr, SByte Target) { return Beq(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x2F)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BgeS(ulong Instr, SByte Target) { return Bge(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x31)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BleS(ulong Instr, SByte Target) { return Ble(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x32)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BltS(ulong Instr, SByte Target) { return Blt(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x33)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BneUnS(ulong Instr, SByte Target) { return BneUn(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x34)]
        [Fact.Emulation.Processor.Instructions.Parameter1Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BgeUnS(ulong Instr, SByte Target) { return BgeUn(Instr, Target); }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x38)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long Br(ulong Instr, int Target) { return (long)Target; }

        long CompareAndBranch(object ValueA, object ValueB, int ExpectedValue1, int ExpectedValue2, int Target)
        {
            if ((ExpectedValue1 + ExpectedValue2) == 0)
            {
                if (ValueA == null)
                {
                    if (ValueB == null)
                    {
                        if (ExpectedValue1 == 0) { return Target; }
                        else { return 0; }
                    }
                    else
                    {
                        if (ExpectedValue1 == 0) { return 0; }
                        else { return Target; }
                    }
                }
                else if (ValueB == null)
                {
                    if (ExpectedValue1 == 0) { return 0; }
                    else { return Target; }
                }

                System.Reflection.MethodInfo equal = ValueA.GetType().GetMethod("Equals", new Type[] { ValueB.GetType() });
                if (equal != null)
                {
                    if ((bool)equal.Invoke(ValueA, new object[] { ValueB }))
                    {
                        if (ExpectedValue1 == 0) { return Target; }
                        else { return 0; }
                    }
                    else
                    {
                        if (ExpectedValue1 == 0) { return 0; }
                        else { return Target; }
                    }
                }
            }

            System.Reflection.MethodInfo compareTo = ValueA.GetType().GetMethod("CompareTo", new Type[] { ValueB.GetType() });
            if (compareTo == null)
            {
                throw new Exception("No suitable compareTo Method found for: '" + ValueA.GetType().Name + "' and '" + ValueB.GetType().Name + "'");
            }
            if ((int)compareTo.Invoke(ValueA, new object[] { ValueB }) == ExpectedValue1)
            {
                return Target;
            }
            if ((int)compareTo.Invoke(ValueA, new object[] { ValueB }) == ExpectedValue2)
            {
                return Target;
            }
            return 0;
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x39)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BrZero(ulong Instr, int Target)
        {
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " == 0");
#endif
            if (value1 is bool) { return (bool)value1 ? 0 : Target; }
            return CompareAndBranch(value1, null, 0, 0, Target);

        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x3A)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BrInst(ulong Instr, int Target)
        {
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " != 0");
#endif
            if (value1 is bool) { return (bool)value1 ? Target : 0; }
            return CompareAndBranch(value1, null, -1, 1, Target);

        }


        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x3B)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long Beq(ulong Instr, int Target)
        {
            object value2 = stack.Pop();
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " == " + value2);
#endif
            return CompareAndBranch(value1, value2, 0, 0, Target);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x3C)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long Bge(ulong Instr, int Target)
        {
            object value2 = stack.Pop();
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " >= " + value2);
#endif
            return CompareAndBranch(value1, value2, 0, 1, Target);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x3E)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long Ble(ulong Instr, int Target)
        {
            object value2 = stack.Pop();
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " <= " + value2);
#endif
            return CompareAndBranch(value1, value2, -1, 0, Target);

        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x3F)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long Blt(ulong Instr, int Target)
        {
            object value2 = stack.Pop();
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " < " + value2);
#endif
            return CompareAndBranch(value1, value2, -1, -1, Target);

        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x40)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BneUn(ulong Instr, int Target)
        {
            object value2 = stack.Pop();
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " != " + value2);
#endif
            return CompareAndBranch(value1, value2, -1, 1, Target);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x41)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.RelativeJump]
        public long BgeUn(ulong Instr, int Target)
        {
            object value2 = stack.Pop();
            object value1 = stack.Pop();

#if DEBUG
            Fact.Log.Debug("br " + Target.ToString() + " if " + value1 + " >= " + value2);
#endif
            return CompareAndBranch(value1, value2, 0, 1, Target);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x58)]
        public void Add(ulong Instr)
        {
            object A = stack.Pop();
            object B = stack.Pop();

#if DEBUG
            Fact.Log.Debug("push(" + A.ToString() + " + " + B.ToString() + ")");
#endif

            if (A is int)
            {
                if (B is int) { stack.Push(((int)A) + ((int)B)); }
                else if (B is uint) { stack.Push(((int)A) + ((uint)B)); }
                else if (B is long) { stack.Push(((int)A) + ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid addition between an int and an ulong"); }
                else { throw new Exception("Invalid type in addition operation: " + (B.GetType().Name)); }

            }
            else if (A is uint)
            {
                if (B is int) { stack.Push(((uint)A) + ((int)B)); }
                else if (B is uint) { stack.Push(((uint)A) + ((uint)B)); }
                else if (B is long) { stack.Push(((uint)A) + ((long)B)); }
                else if (B is ulong) { stack.Push(((uint)A) + ((ulong)B)); }
                else { throw new Exception("Invalid type in addition operation: " + (B.GetType().Name)); }

            }
            else if (A is long)
            {
                if (B is int) { stack.Push(((long)A) + ((int)B)); }
                else if (B is uint) { stack.Push(((long)A) + ((uint)B)); }
                else if (B is long) { stack.Push(((long)A) + ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid addition between an long and an ulong"); }
                else { throw new Exception("Invalid type in addition operation: " + (B.GetType().Name)); }

            }
            else if (A is ulong)
            {
                if (B is int) { throw new Exception("Invalid addition between an ulong and an int"); }
                else if (B is uint) { stack.Push(((ulong)A) + ((uint)B)); }
                else if (B is long) { throw new Exception("Invalid addition between an ulong and a long"); }
                else if (B is ulong) { stack.Push(((ulong)A) + ((ulong)B)); }
                else { throw new Exception("Invalid type in addition operation: " + (B.GetType().Name)); }

            }
            else { throw new Exception("Invalid type in addition operation: " + (A.GetType().Name)); }
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x59)]
        public void Sub(ulong Instr)
        {
            object A = stack.Pop();
            object B = stack.Pop();

            if (A is int)
            {
                if (B is int) { stack.Push(((int)A) - ((int)B)); }
                else if (B is uint) { stack.Push(((int)A) - ((uint)B)); }
                else if (B is long) { stack.Push(((int)A) - ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid substraction between an int and an ulong"); }
                else { throw new Exception("Invalid type in sub operation: " + (B.GetType().Name)); }


            }
            else if (A is uint)
            {
                if (B is int) { stack.Push(((uint)A) - ((int)B)); }
                else if (B is uint) { stack.Push(((uint)A) - ((uint)B)); }
                else if (B is long) { stack.Push(((uint)A) - ((long)B)); }
                else if (B is ulong) { stack.Push(((uint)A) - ((ulong)B)); }
                else { throw new Exception("Invalid type in sub operation: " + (B.GetType().Name)); }

            }
            else if (A is long)
            {
                if (B is int) { stack.Push(((long)A) - ((int)B)); }
                else if (B is uint) { stack.Push(((long)A) - ((uint)B)); }
                else if (B is long) { stack.Push(((long)A) - ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid substraction between an long and an ulong"); }
                else { throw new Exception("Invalid type in sub operation: " + (B.GetType().Name)); }

            }
            else if (A is ulong)
            {
                if (B is int) { throw new Exception("Invalid substraction between an ulong and an int"); }
                else if (B is uint) { stack.Push(((ulong)A) - ((uint)B)); }
                else if (B is long) { throw new Exception("Invalid substraction between an ulong and a long"); }
                else if (B is ulong) { stack.Push(((ulong)A) - ((ulong)B)); }
                else { throw new Exception("Invalid type in sub operation: " + (B.GetType().Name)); }

            }
            else { throw new Exception("Invalid type in sub operation: " + (A.GetType().Name)); }
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x5F)]
        public void And(ulong Instr)
        {
            object A = stack.Pop();
            object B = stack.Pop();

            if (A is int)
            {
                if (B is int) { stack.Push(((int)A) & ((int)B)); }
                else if (B is uint) { stack.Push(((int)A) & ((uint)B)); }
                else if (B is long) { stack.Push(((int)A) & ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid substraction between an int and an ulong"); }
                else { throw new Exception("Invalid type in and operation: " + (B.GetType().Name)); }

            }
            else if (A is uint)
            {
                if (B is int) { stack.Push(((uint)A) & ((int)B)); }
                else if (B is uint) { stack.Push(((uint)A) & ((uint)B)); }
                else if (B is long) { stack.Push(((uint)A) & ((long)B)); }
                else if (B is ulong) { stack.Push(((uint)A) & ((ulong)B)); }
                else { throw new Exception("Invalid type in and operation: " + (B.GetType().Name)); }

            }
            else if (A is long)
            {
                if (B is int) { stack.Push(((long)A) & ((int)B)); }
                else if (B is uint) { stack.Push(((long)A) & ((uint)B)); }
                else if (B is long) { stack.Push(((long)A) & ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid substraction between an long and an ulong"); }
                else { throw new Exception("Invalid type in and operation: " + (B.GetType().Name)); }

            }
            else if (A is ulong)
            {
                if (B is int) { throw new Exception("Invalid substraction between an ulong and an int"); }
                else if (B is uint) { stack.Push(((ulong)A) & ((uint)B)); }
                else if (B is long) { throw new Exception("Invalid substraction between an ulong and a long"); }
                else if (B is ulong) { stack.Push(((ulong)A) & ((ulong)B)); }
                else { throw new Exception("Invalid type in and operation: " + (B.GetType().Name)); }

            }
            else { throw new Exception("Invalid type in and operation: " + (A.GetType().Name)); }
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x5F)]
        public void Or(ulong Instr)
        {
            object A = stack.Pop();
            object B = stack.Pop();

            if (A is int)
            {
                if (B is int) { stack.Push(((int)A) | ((int)B)); }
                else if (B is uint) { stack.Push(((int)A) | ((uint)B)); }
                else if (B is long) { stack.Push(((int)A) | ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid substraction between an int and an ulong"); }
                else { throw new Exception("Invalid type in or operation: " + (B.GetType().Name)); }


            }
            else if (A is uint)
            {
                if (B is int) { stack.Push(((uint)A) | ((int)B)); }
                else if (B is uint) { stack.Push(((uint)A) | ((uint)B)); }
                else if (B is long) { stack.Push(((uint)A) | ((long)B)); }
                else if (B is ulong) { stack.Push(((uint)A) | ((ulong)B)); }
                else { throw new Exception("Invalid type in or operation: " + (B.GetType().Name)); }

            }
            else if (A is long)
            {
                if (B is int) { stack.Push(((long)A) | ((int)B)); }
                else if (B is uint) { stack.Push(((long)A) | ((uint)B)); }
                else if (B is long) { stack.Push(((long)A) | ((long)B)); }
                else if (B is ulong) { throw new Exception("Invalid substraction between an long and an ulong"); }
                else { throw new Exception("Invalid type in or operation: " + (B.GetType().Name)); }

            }
            else if (A is ulong)
            {
                if (B is int) { throw new Exception("Invalid substraction between an ulong and an int"); }
                else if (B is uint) { stack.Push(((ulong)A) | ((uint)B)); }
                else if (B is long) { throw new Exception("Invalid substraction between an ulong and a long"); }
                else if (B is ulong) { stack.Push(((ulong)A) | ((ulong)B)); }
                else { throw new Exception("Invalid type in or operation: " + (B.GetType().Name)); }

            }
            else { throw new Exception("Invalid type in or operation: " + (A.GetType().Name)); }
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x69)]
        public void ConvI4(ulong Instr)
        {
            stack.Push((Int32)stack.Pop());
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x6F)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Callvirt(ulong Instr, ulong NextEIP, UInt32 Method)
        {
            return Call(Instr, NextEIP, Method);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x72)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void Ldstr(ulong Instr, UInt32 Str)
        {
            string result = resolver.ResolveString((int)Str);
#if DEBUG
            Fact.Log.Debug("push(\"" + result + "\")");
#endif
            stack.Push(result);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x73)]
        [Fact.Emulation.Processor.Instructions.ParameterNextInstructionPointer]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        [Fact.Emulation.Processor.Instructions.Jump]
        public ulong Newobj(ulong Instr, ulong NextEIP, UInt32 Constructor)
        {
            return Call(Instr, NextEIP, Constructor);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x7A)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void Throw(ulong Instr, UInt32 Method)
        {
            Exception e = stack.Pop() as Exception;
#if DEBUG
            Fact.Log.Debug("throw exception");
            Fact.Log.Exception(e);
#endif
            throw e;
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x7B)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void Ldfld(ulong Instr, UInt32 Method)
        {
            System.Reflection.FieldInfo field = resolver.ResolveField((int)Method);
            object obj = stack.Pop();
            stack.Push(field.GetValue(obj));

#if DEBUG
            if (obj != null)
            {
                Fact.Log.Debug("push(" + obj.ToString() + "." + field.Name + ")");
            }
            else
            {
                Fact.Log.Debug("push(" + field.Name + ")");
            }
#endif
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x7D)]
        [Fact.Emulation.Processor.Instructions.Parameter4Byte]
        public void Stfld(ulong Instr, UInt32 Method)
        {
            System.Reflection.FieldInfo field = resolver.ResolveField((int)Method);
            object value = stack.Pop();
            object obj = stack.Pop();

#if DEBUG
            if (obj != null)
            {
                Fact.Log.Debug(obj.ToString() + "." + field.Name + " = " + value);
            }
            else
            {
                Fact.Log.Debug(field.Name + " = " + value);
            }
#endif

            field.SetValue(obj, value);
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x8E)]
        public void Ldlen(ulong Instr)
        {
            object value = stack.Pop();
            if (value.GetType().IsArray)
            {
                throw new Exception("Type " + value.GetType() + " is not an array type");
            }
            stack.Push(value.GetType().GetProperty("Length").GetValue(value, null));
        }

        [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x9A)]
        public void LdelemRef(ulong Instr)
        {
            object genericIndex = stack.Pop();
            long index = 0;

            if (genericIndex is int) { index = (int)genericIndex; }
            else if (genericIndex is long) { index = (long)genericIndex; }
            else { throw new Exception("Invalid index type: '" + genericIndex.GetType().Name + "'"); }

            object genericObject = stack.Pop();
            if (genericObject is string)
            {
                string str = (string)genericObject;
#if DEBUG
                Fact.Log.Debug("push('" + str[(int)index] + "' : pop()[" + index.ToString() + "])");
#endif
                stack.Push(str[(int)index]);
            }
            else if (genericObject is Array)
            {
                Array array = (Array)genericObject;
#if DEBUG
                Fact.Log.Debug("push(" + array.GetValue(index) + " : pop()[" + index.ToString() + "])");
#endif
                stack.Push(array.GetValue(index));
            }
            else
            {
                throw new Exception("Invalid array type: '" + genericObject.GetType().Name + "'");
            }
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0xFE09)]
        [Fact.Emulation.Processor.Instructions.Parameter2Byte]
        public void LdArg(ulong Instr, ushort Local)
        {
            stack.Push(args[Local]);
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0xFE0C)]
        [Fact.Emulation.Processor.Instructions.Parameter2Byte]
        public void LdLoc(ulong Instr, ushort Local)
        {
#if DEBUG
                Fact.Log.Debug("push(" + locals[Local] + ":loc" + Local + ")");
#endif
            stack.Push(locals[Local]);
        }

        [Fact.Emulation.Processor.Instructions.OpCode2Bytes(0xFE0E)]
        [Fact.Emulation.Processor.Instructions.Parameter2Byte]
        public void StLoc(ulong Instr, ushort Local)
        {
            locals[Local] = stack.Pop();
#if DEBUG
            Fact.Log.Debug("loc" + Local + " =  pop():" + locals[Local]);
#endif
        }

        [Fact.Emulation.Processor.Instructions.OpCode4Bytes(0xFFFFFFFF)]
        [Fact.Emulation.Processor.Instructions.ExecuteAndReturn]
        public void _FakeRet(ulong Instr) { }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor
{
    unsafe public class Model
    {
        internal object _Instance = null;
        Dictionary<byte, System.Reflection.MethodInfo> _Method = new Dictionary<byte, System.Reflection.MethodInfo>();
        Dictionary<byte, System.Reflection.MethodInfo> _RetMethod = new Dictionary<byte, System.Reflection.MethodInfo>();
        Dictionary<byte, System.Reflection.MethodInfo> _MethodWithMask = new Dictionary<byte, System.Reflection.MethodInfo>();

        List<_8BOpCode> _8BOpCodes = new List<_8BOpCode>();

        // 0 -> IP
        ulong* _Scratchpad = null;

        class _8BOpCode
        {
            internal byte OpCode = 0x00;
            internal byte Mask = 0xFF;
            internal bool Return = false;
            internal bool Jump = false;
            internal bool RelativeJump = false;
            internal int ParameterSize = 0;
            internal System.Reflection.MethodInfo Method = null;
            public bool Match(byte Code)
            {
                return ((Code & Mask) == OpCode);
            }
        }

        internal System.Reflection.MethodInfo _FetchMethod = null;
        internal System.Reflection.MethodInfo _RunMethod = null;

        OpCodeTree codeTree = new OpCodeTree();

        public Model(object Processor)
        {
            _Instance = Processor;
            Type proctype = Processor.GetType();
            _Scratchpad = (ulong*)System.Runtime.InteropServices.Marshal.AllocHGlobal(256 * sizeof(ulong)).ToPointer();
            foreach (System.Reflection.MethodInfo method in proctype.GetMethods())
            {
                System.Reflection.MethodInfo methodcpy = method;
                bool _ExecAndReturn = false;
                int _ParameterSize = 0;
                bool Jump = false;
                bool RelativeJump = false;
                bool Fetch = false;
                bool NextEIP = false;

                foreach ( object attr in method.GetCustomAttributes(false))
                {
                    if (attr is Instructions.ExecuteAndReturn) { _ExecAndReturn = true; }
                    if (attr is Instructions.Jump) { Jump = true; }
                    if (attr is Instructions.RelativeJump) { RelativeJump = true; }
                    if (attr is Instructions.Parameter1Byte) { _ParameterSize++; }
                    if (attr is Instructions.Parameter2Byte) { _ParameterSize += 2; }
                    if (attr is Instructions.Parameter4Byte) { _ParameterSize += 4; }
                    if (attr is Instructions.Parameter8Byte) { _ParameterSize += 8; }

                    if (attr is Instructions.ParameterNextInstructionPointer) { NextEIP = true; }

                    if (attr is Instructions.Fetch) { Fetch = true; }
                }
                foreach (object attr in method.GetCustomAttributes(false))
                {
                    if (attr is Instructions.OpCode1Byte)
                    {
                        OpCode opcode = new OpCode(_ExecAndReturn, Jump, RelativeJump, NextEIP, _ParameterSize, method);
                        opcode.Add(0, (attr as Instructions.OpCode1Byte).OpCode, (attr as Instructions.OpCode1Byte).Mask);
                        codeTree.Add(opcode);
                    }
                    if (attr is Instructions.OpCode2Bytes)
                    {
                        OpCode opcode = new OpCode(_ExecAndReturn, Jump, RelativeJump, NextEIP, _ParameterSize, method);
                        opcode.Add(0, (byte)(((attr as Instructions.OpCode2Bytes).OpCode & 0xFF00) >> 8), (byte)(((attr as Instructions.OpCode2Bytes).Mask & 0xFF00) >> 8));
                        opcode.Add(1, (byte)((attr as Instructions.OpCode2Bytes).OpCode & 0xFF), (byte)((attr as Instructions.OpCode2Bytes).Mask & 0xFF));
                        codeTree.Add(opcode);
                    }
                    if (attr is Instructions.OpCode3Bytes)
                    {
                        OpCode opcode = new OpCode(_ExecAndReturn, Jump, RelativeJump, NextEIP, _ParameterSize, method);
                        opcode.Add(0, (byte)(((attr as Instructions.OpCode3Bytes).OpCode & 0xFF0000) >> 16), (byte)(((attr as Instructions.OpCode3Bytes).Mask & 0xFF0000) >> 16));
                        opcode.Add(1, (byte)(((attr as Instructions.OpCode3Bytes).OpCode & 0xFF00) >> 8), (byte)(((attr as Instructions.OpCode3Bytes).Mask & 0xFF00) >> 8));
                        opcode.Add(2, (byte)((attr as Instructions.OpCode3Bytes).OpCode & 0xFF), (byte)((attr as Instructions.OpCode3Bytes).Mask & 0xFF));
                        codeTree.Add(opcode);
                    }
                    if (attr is Instructions.OpCode4Bytes)
                    {
                        OpCode opcode = new OpCode(_ExecAndReturn, Jump, RelativeJump, NextEIP, _ParameterSize, method);
                        opcode.Add(0, (byte)(((attr as Instructions.OpCode4Bytes).OpCode & 0xFF000000) >> 24), (byte)(((attr as Instructions.OpCode4Bytes).Mask & 0xFF000000) >> 24));
                        opcode.Add(1, (byte)(((attr as Instructions.OpCode4Bytes).OpCode & 0xFF0000) >> 16), (byte)(((attr as Instructions.OpCode4Bytes).Mask & 0xFF0000) >> 16));
                        opcode.Add(2, (byte)(((attr as Instructions.OpCode4Bytes).OpCode & 0xFF00) >> 8), (byte)(((attr as Instructions.OpCode4Bytes).Mask & 0xFF00) >> 8));
                        opcode.Add(3, (byte)((attr as Instructions.OpCode4Bytes).OpCode & 0xFF), (byte)((attr as Instructions.OpCode4Bytes).Mask & 0xFF));
                        codeTree.Add(opcode);
                    }
                }
                if (Fetch)
                {
                    _FetchMethod = methodcpy;
                }
            }
            //BuildOpCodeMap();
            if (_FetchMethod == null) { throw new Exception("Missing fetch method"); }
            _RunMethod = codeTree.BuildEmulationTree(_FetchMethod);
        }

        delegate void Action();
        Action[] _OpcodeMap = new Action[256];

        public List<OpCode> disassemble(byte[] Program)
        {
            List<OpCode> list = new List<OpCode>();
            for (int n = 0; n < Program.Length;)
            {
                OpCode opcode = codeTree.Disassemble(Program, ref n);
                if (opcode == null) { list.Add(null); n++; }
                else
                {
                    n += opcode._ParameterSize;
                }
            }
            return list;
        }

        public class InvalidInstruction : Exception
        {
            ulong _Instruction = 0;
            public InvalidInstruction(ulong Instruction) { _Instruction = Instruction; }
            public override string Message
            {
                get
                {
                    return "The virtual processor has jump on an invalid instruction: 0x" + _Instruction.ToString("X");
                }
            }
        }
        public void DynamicExecution(ulong EntryPoint)
        {
            try
            {
                _RunMethod.Invoke(null, new object[] { _Instance, EntryPoint });
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    Fact.Log.Debug("Exception while executing emulated code: " + e.InnerException.ToString());
                    throw e.InnerException;
                }
                throw e;
            }
            /*
            _Scratchpad[0] = EntryPoint;
            _Scratchpad[1] = 0;
            while (true)
            {
                byte opcode = (byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] });
                _OpcodeMap[opcode]();
                if (_Scratchpad[1] != 0) { return; }
            }
             */
        }
    }
}

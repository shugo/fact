﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation
{
    public static class Execute
    {
        static ulong x86_64_Stack_Size = 1024 * 1024 * 1024; // 1MB
        public enum ProcessorType
        {
            x86_64,
            ARM64,
            SPARC64,
            MSIL
        }

        public enum CallingConvention
        {
            MSIL,
            MicrosoftAMD64,
            SytemVAMD64,
        }

        class ProcessorModel
        {
            internal bool busy = false;
            internal Processor.Model model = default(Processor.Model);
        }

        static List<ProcessorModel> _MSIL_Generic = new List<ProcessorModel>();
        static List<ProcessorModel> _x86_64_Generic = new List<ProcessorModel>();


        public static void Call(IntPtr MethodPtr, Processor.Model Processor)
        {
        }

        public static ReturnType Call<ReturnType>(IntPtr MethodPtr, ProcessorType Processor, CallingConvention CallingConvention, object Thisptr, params object[] Parameters)
        {
            if (Processor == Execute.ProcessorType.MSIL)
            {
                throw new Exception("MSIL virtual processor can't be called without specifying a .NET method");
            }
            if (Processor == ProcessorType.x86_64)
            {
                Callx86_64(null, MethodPtr, CallingConvention, x86_64_Stack_Size, Thisptr, Parameters);
            }
            return default(ReturnType);
        }

        public static ReturnType Call<ReturnType>(Reflection.Memory.VirtualAddressSpace VASpace, IntPtr MethodPtr, ProcessorType Processor, CallingConvention CallingConvention, object Thisptr, params object[] Parameters)
        {
            if (Processor == Execute.ProcessorType.MSIL)
            {
                throw new Exception("MSIL virtual processor can't be called without specifying a .NET method");
            }
            if (Processor == ProcessorType.x86_64)
            {
                Callx86_64(VASpace, MethodPtr, CallingConvention, x86_64_Stack_Size, Thisptr, Parameters);
            }
            return default(ReturnType);
        }

        static ulong Callx86_64(Reflection.Memory.VirtualAddressSpace VASpace, IntPtr MethodPtr, CallingConvention Calling, ulong StackSize, object Thisptr, params object[] Parameters)
        {
            ulong returnValue = 0;
            lock (_x86_64_Generic)
            {
                ProcessorModel pmodel = null;
                foreach (ProcessorModel model in _x86_64_Generic)
                {
                    lock (model)
                    {
                        if (!model.busy)
                        {
                            model.busy = true;
                            pmodel = model;
                            break;
                        }
                    }
                }

                if (pmodel == null)
                {
                    pmodel = new ProcessorModel();
                    pmodel.busy = true;
                    pmodel.model = new Processor.Model(new Processor.Models.x86_64_Generic());
                    _x86_64_Generic.Add(pmodel);
                }

                try
                {
                    IntPtr stack = Internal.Os.Generic.MapMemory((int)StackSize, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Write);
                    Processor.Models.x86_64_Generic x86_64 = pmodel.model._Instance as Processor.Models.x86_64_Generic;
                    x86_64.SetRegister(Processor.Models.x86_64_Generic.Register.rbp, (ulong)stack.ToInt64() + StackSize - sizeof(ulong));
                    x86_64.SetRegister(Processor.Models.x86_64_Generic.Register.rsp, (ulong)stack.ToInt64() + StackSize - sizeof(ulong));

                    x86_64.Push(Processor.Models.x86_64_Generic.Register.rbp);
                    switch (Calling)
                    {
                        case CallingConvention.MicrosoftAMD64:
                            break;
                        case CallingConvention.SytemVAMD64:
                            break;
                        default:
                            throw new Exception("Unsupported calling convention " + Calling.ToString() + " on x86_64");
                    }
                    x86_64.SetVirtualAddressSpace(VASpace);
                    pmodel.model.DynamicExecution((ulong)MethodPtr.ToInt64());
                    returnValue = x86_64.GetRegister(Processor.Models.x86_64_Generic.Register.rax);
                    Internal.Os.Generic.UnmapMemory(stack, (int)StackSize);

                }
                catch (Exception e)
                {
                    pmodel.busy = false;
                    throw e;
                }
                pmodel.busy = false;
            }
            return returnValue;
        }

        public static ReturnType Call<ReturnType>(Reflection.Method Method, ProcessorType Processor, CallingConvention CallingConvention, object Thisptr, params object[] Parameters)
        {
            IntPtr ptr = Method.GetFunctionPointer();
            return Call<ReturnType>(Method, Processor, CallingConvention, Parameters);
        }

        public static ReturnType Call<ReturnType>(System.Reflection.MethodBase Method, object Thisptr, params object[] Parameters)
        {
            ulong methodptr = ((ulong)Method.MetadataToken) << 32;
            lock (_MSIL_Generic)
            {
                ProcessorModel pmodel = null;
                foreach (ProcessorModel model in _MSIL_Generic)
                {
                    lock (model)
                    {
                        if (!model.busy)
                        {
                            model.busy = true;
                            pmodel = model;
                            break;
                        }
                    }
                }
                if (pmodel == null)
                {
                    pmodel = new ProcessorModel();
                    pmodel.busy = true;
                    pmodel.model = new Processor.Model(new Processor.Models.MSIL_Generic());
                    _MSIL_Generic.Add(pmodel);
                }

                try
                {
                    Processor.Models.MSIL_Generic msil = pmodel.model._Instance as Processor.Models.MSIL_Generic;
                    msil.PrepareFirstCall(Method, Thisptr, Parameters);
                    pmodel.model.DynamicExecution(methodptr);
                }
                catch (Exception e)
                {
                    pmodel.busy = false;
                    throw e;
                }
                pmodel.busy = false;
            }
            return default(ReturnType);
        }
    }
}

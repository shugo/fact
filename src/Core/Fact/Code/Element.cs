﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public abstract class Element
    {
        internal virtual void Save(System.IO.Stream stream)
        {
        }

        internal static Element LoadGeneric(System.IO.Stream stream)
        {
            switch(Internal.StreamTools.ReadInt16(stream))
            {
                case 0x1000: return Class.Load(stream);
                case 0x1001: return Struct.Load(stream);
                case 0x2000: return Function.Load(stream);
                case 0x3000: return VariableDeclaration.Load(stream);
            }

            return null;
        }
    }
}

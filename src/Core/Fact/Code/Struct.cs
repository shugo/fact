﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class Struct : Element
    {
        string _Name = "";

        public Struct(string Name)
        {
            _Name = Name;
        }

        List<Element> _Public = new List<Element>();
        List<Element> _Private = new List<Element>();
        List<Element> _Protected = new List<Element>();
        List<Element> _Internal = new List<Element>();

        public List<Element> Public { get { return _Public; } }
        public List<Element> Private { get { return _Private; } }
        public List<Element> Protected { get { return _Protected; } }
        public List<Element> Internal { get { return _Internal; } }

        internal override void Save(System.IO.Stream stream)
        {
            Fact.Internal.StreamTools.WriteInt16(stream, 0x1001);
            Fact.Internal.StreamTools.WriteUTF8String(stream, _Name);
            Fact.Internal.StreamTools.WriteInt32(stream, _Public.Count);
            Fact.Internal.StreamTools.WriteInt32(stream, _Private.Count);
            Fact.Internal.StreamTools.WriteInt32(stream, _Protected.Count);
            Fact.Internal.StreamTools.WriteInt32(stream, _Internal.Count);
            foreach (Element e in _Public) { e.Save(stream); }
            foreach (Element e in _Private) { e.Save(stream); }
            foreach (Element e in _Protected) { e.Save(stream); }
            foreach (Element e in _Internal) { e.Save(stream); }
        }

        static internal Struct Load(System.IO.Stream stream)
        {
            Struct str = new Struct("");
            str._Name = Fact.Internal.StreamTools.ReadUTF8String(stream);
            int publicCount = Fact.Internal.StreamTools.ReadInt32(stream);
            int privateCount = Fact.Internal.StreamTools.ReadInt32(stream);
            int protectedCount = Fact.Internal.StreamTools.ReadInt32(stream);
            int internalCount = Fact.Internal.StreamTools.ReadInt32(stream);
            while (publicCount > 0) { publicCount--; str._Public.Add(Element.LoadGeneric(stream)); }
            while (privateCount > 0) { privateCount--; str._Private.Add(Element.LoadGeneric(stream)); }
            while (protectedCount > 0) { protectedCount--; str._Protected.Add(Element.LoadGeneric(stream)); }
            while (internalCount > 0) { internalCount--; str._Internal.Add(Element.LoadGeneric(stream)); }
            return str;
        }
    }
}

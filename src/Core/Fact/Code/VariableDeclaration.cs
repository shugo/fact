﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class VariableDeclaration : Code.Element
    {
        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; } }
        string _Type = "";
        public string Type { get { return _Type; } set { _Type = value; } }

        internal override void Save(System.IO.Stream stream)
        {
            Fact.Internal.StreamTools.WriteInt16(stream, 0x3000);
            Fact.Internal.StreamTools.WriteUTF8String(stream, _Name);
            Fact.Internal.StreamTools.WriteUTF8String(stream, _Type);
        }

        internal static VariableDeclaration Load(System.IO.Stream stream)
        {
            VariableDeclaration vardec = new VariableDeclaration();
            vardec._Name = Fact.Internal.StreamTools.ReadUTF8String(stream);
            vardec._Type = Fact.Internal.StreamTools.ReadUTF8String(stream);
            return vardec;
        }
    }
}

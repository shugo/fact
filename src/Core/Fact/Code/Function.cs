﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class Function : Code.Element
    {
        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; } }
        List<Element> _Body = new List<Element>();
        public List<Element> Body { get { return _Body; } }

        internal override void Save(System.IO.Stream stream)
        {
            Fact.Internal.StreamTools.WriteInt16(stream, 0x2000);
            Fact.Internal.StreamTools.WriteUTF8String(stream, _Name);
            Fact.Internal.StreamTools.WriteInt32(stream, _Body.Count);
            foreach (Element e in _Body) { e.Save(stream); }
        }

        internal static Function Load(System.IO.Stream stream)
        {
            Function func = new Function();
            func._Name = Fact.Internal.StreamTools.ReadUTF8String(stream);
            int elementCount = Fact.Internal.StreamTools.ReadInt32(stream);
            while (elementCount > 0)
            {
                func._Body.Add(Element.LoadGeneric(stream));
                elementCount--;
            }
            return func;
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact
{
    public static class Log
    {
        static bool _displayInfo = true;
        static bool _displayVerbose = true;
        static bool _displayWarning = true;
        static bool _displayDebug = true;

        /// <summary>
        /// Display an information on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>
        public static void Info(string message) 
        {
            if (_displayInfo)
            {
                lock (Console.Out)
                {
                    Terminal.WriteLine("[INFO]      " + message);
                }
            }
        }

        /// <summary>
        /// Display a message on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>
        public static void Verbose(string message) 
        {
            if (_displayVerbose)
            {
                lock (Console.Out)
                {
                    Terminal.WriteLine("[VERB]      " + message);
                }
            }
        }

        /// <summary>
        /// Display a warning message on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>

        public static void Warning(string message)
        {
            Context.Context.Current.LogWarning(message);
        }
        public static void WarningInternal(string message)
        {
            if (_displayWarning)
            {
                lock (Console.Out)
                {
                    Terminal.Write("[");
                    Terminal.ForegroundColor = ConsoleColor.Yellow;
                    Terminal.Write("WARNING");
                    Terminal.ResetColor();
                    Terminal.Write("]   ");
                    Terminal.WriteLine(message);
                }
            }
        }

        /// <summary>
        /// Display an error message on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>
        public static void Error(string message)
        {
            Context.Context.Current.LogError(message);
        }
        internal static void ErrorInternal(string message) 
        {
            lock (Console.Out)
            {
                Terminal.Write("[");
                Terminal.ForegroundColor = ConsoleColor.Red;
                Terminal.Write("ERROR");
                Terminal.ResetColor();
                Terminal.Write("]     ");
                Terminal.ForegroundColor = ConsoleColor.Red;
                Terminal.WriteLine(message);
                Terminal.ResetColor();
            }
        }

        /// <summary>
        /// Display a debug message on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>

        public static void Debug(string message)
        {
            if (_displayDebug)
            {
                string funname = "";
                try
                {
                    System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(1);
                    funname = trace.GetFrame(0).GetMethod().DeclaringType.FullName + "." + trace.GetFrame(0).GetMethod().Name + ":";
                    while (funname.Length % 4 != 0) { funname += " "; }
                }
                catch { }
                Context.Context.Current.LogDebug(funname + " " + message);
            }
        }

        internal static void DebugInternal(string message)
        {
            lock (Console.Error)
            {
                Console.Error.WriteLine("[DEBUG]   " + message);
            }
        }


        /// <summary>
        /// Display an test log message on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>
        public static void TestFail(string message)
        {
            TestFail(message, "", 0);
        }

        /// <summary>
        /// Display an test log message on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>
        public static void TestPass(string message)
        {
            TestPass(message, "", 0);
        }

        /// <summary>
        /// Display an test log message on stdout followed by a new line.
        /// </summary>
        /// <param name="message">the message that must be displayed.</param>
        public static void TestOmitted(string message)
        {
            TestOmitted(message, "");
        }

		/// <summary>
		/// Display an test log message on stdout followed by a new line.
		/// </summary>
		/// <param name="name">the name that must be displayed.</param>
		/// <param name="message">the message that must be displayed.</param>
        public static void TestFail(string name, string message, long duration)
		{
            lock (Console.Out)
            {
                Terminal.Write("[TEST]    ");
                Terminal.Write("[");
                Terminal.ForegroundColor = ConsoleColor.Red;
                Terminal.Write("FAIL");
                Terminal.ResetColor();
                Terminal.Write("]   ");
                if (duration > 0)
                {
                    Terminal.Write(name);
                    Terminal.Write(" (");
                    Duration(duration);
                    Terminal.WriteLine(")");
                }
                else
                {
                    Terminal.WriteLine(name);
                }
                if (message != "")
                {
                    DisplayInfo(message);
                }
            }
		}

		/// <summary>
		/// Display an test log message on stdout followed by a new line.
		/// </summary>
		/// <param name="name">the name that must be displayed.</param>
		/// <param name="message">the message that must be displayed.</param>
        /// <param name="message">the duration of the test.</param>
		public static void TestPass(string name, string message, long duration)
		{
            lock (Console.Out)
            {
                Terminal.Write("[TEST]    ");
                Terminal.Write("[");
                Terminal.ForegroundColor = ConsoleColor.Green;
                Terminal.Write("PASS");
                Terminal.ResetColor();
                Terminal.Write("]   ");
                if (duration > 0)
                {
                    Terminal.Write(name);
                    Terminal.Write(" (");
                    Duration(duration);
                    Terminal.WriteLine(")");
                }
                else
                {
                    Terminal.WriteLine(name);
                }
                if (message != "")
                {
                    DisplayInfo(message);
                }
            }
        }


        /// <summary>
        /// Display an test log message on stdout followed by a new line.
        /// </summary>
        /// <param name="name">the name that must be displayed.</param>
        /// <param name="message">the message that must be displayed.</param>
        public static void TestOmitted(string name, string message)
        {
            lock (Console.Out)
            {
                Terminal.Write("[TEST]    ");
                Terminal.Write("[");
                Terminal.ForegroundColor = ConsoleColor.DarkGray;
                Terminal.Write("SKIP");
                Terminal.ResetColor();
                Terminal.Write("]   ");
                Terminal.WriteLine(name);
                if (message != "")
                {
                    DisplayInfo(message);
                }
            }
        }

        /// <summary>
        /// Display an test log message on stdout followed by a new line.
        /// </summary>
        /// <param name="name">the name that must be displayed.</param>
        public static void TestIntermittent(string name)
        {
            lock (Console.Out)
            {
                Terminal.Write("[TEST]    ");
                Terminal.Write("[");
                Terminal.ForegroundColor = ConsoleColor.DarkYellow;
                Terminal.Write("INTERMITTENT");
                Terminal.ResetColor();
                Terminal.Write("]   ");
                Terminal.WriteLine(name);
            }
        }

		public static void FatalInternalError(Exception Except)
		{
			Terminal.DoubleLine ();
            Terminal.ForegroundColor = ConsoleColor.Red;
			Terminal.CenterText ("Critical Internal Error");
			Terminal.CenterText ("Please report this error and the following log as soon as possible");
            Terminal.ResetColor();
			Terminal.DoubleLine ();
            Terminal.ForegroundColor = ConsoleColor.Magenta;
            Log.Exception(Except);
            Terminal.ResetColor();
			Terminal.DoubleLine ();
			System.Environment.Exit (151);
		}

        public static void Login(out string Username, out string Password)
        {
            Terminal.Write("Username:"); Username = Terminal.ReadLine().Replace("\n", "").Replace("\r", "");
            Terminal.Write("Password:"); Password = Terminal.ReadLine().Replace("\n", "").Replace("\r", "");
        }

		public static void Duration(long Millisecond)
        {
			if (Millisecond < 0)
			{
				Terminal.Write("Invalid time");
			}
			if (Millisecond < 1000) 
            {
				Terminal.Write(Millisecond.ToString()); Terminal.Write("ms");
            }
            else
            {
				long second = Millisecond / 1000;
				Terminal.Write(second.ToString()); Terminal.Write("s ");
				Terminal.Write((Millisecond - (second * 1000)).ToString()); Terminal.Write("ms");
            }
        }

        public static void Exception(Exception Except)
        {
            Context.Context.Current.LogException(Except);
        }

        internal static void ExceptionInternal(Exception Except)
        {
            lock (Console.Out)
            {
                Terminal.Write("[");
                Terminal.ForegroundColor = ConsoleColor.Magenta;
                Terminal.Write("EXCEPTION");
                Terminal.ResetColor();
                Terminal.Write("] ");
                Terminal.ForegroundColor = ConsoleColor.Magenta;
                Terminal.WriteLine(Except.Message);
                Terminal.ResetColor();

                DisplayInfo(Except.ToString());
            }
        }

        public static T Auto<T>(T Object)
        {
            return Auto(Object, true);
        }

        public static T Auto<T>(T Object, bool Verbose)
        {
            if (Object == null) { Fact.Log.Info("NULL"); }
            else if (Object is Fact.Test.Result.Error) 
            {
                Fact.Test.Result.Error error = Object as Fact.Test.Result.Error;
                TestFail(error.Text, error.Info, error.EnlapsedTime);
            }
            else if (Object is Fact.Test.Result.Passed)
            {
                Fact.Test.Result.Passed pass = Object as Fact.Test.Result.Passed;
                if (Verbose)
                {
                    TestPass(pass.Text, pass.Info, pass.EnlapsedTime);
                }
                else
                {
                    TestPass(pass.Text, "", pass.EnlapsedTime);
                }
            }
            else if (Object is Fact.Test.Result.Omitted)
            {
                Fact.Test.Result.Omitted skip = Object as Fact.Test.Result.Omitted;
                TestOmitted(skip.Text);
            }
            else if (Object is Fact.Test.Result.Intermittent)
            {
                Fact.Test.Result.Intermittent intermittent = Object as Fact.Test.Result.Intermittent;
                TestIntermittent(intermittent.Text);
            }
            else if (Object is Fact.Test.Result.Group)
            {
                Fact.Test.Result.Group group = Object as Fact.Test.Result.Group;
                foreach (Fact.Test.Result.Result result in group.GetTestResults())
                {
                    Auto(result, Verbose);
                }
            }
            else if (Object is List<Fact.Test.Result.Result>)
            {
                List<Fact.Test.Result.Result> list = Object as List<Fact.Test.Result.Result>;
                foreach (Fact.Test.Result.Result result in list)
                {
                    Auto(result, Verbose);
                }
            }
            else
            {
                Fact.Log.Info(Object.ToString());
            }
            return Object;
        }


        public static void Trace(params object[] Args)
        {
            bool first = true;
            string disp = "";
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();
            System.Diagnostics.StackFrame frame = trace.GetFrame(1);
            
            foreach (object obj in Args)
            {
                if (first) { first = false; }
                else { disp += ", "; }
                disp += obj.ToString();

            }

            lock (Console.Out)
            {
                Terminal.WriteLine("[TRACE]     " + frame.GetMethod().Name + "(" + disp + ")");
            }
        }


		public static class Terminal
		{

            public static System.ConsoleColor ForegroundColor
            {
                get { try { return Console.ForegroundColor; } catch { return ConsoleColor.White; } }
                set { try { Console.ForegroundColor = value; } catch { } }
            }

            public static System.ConsoleColor BackgroundColor
            {
                get { try { return Console.BackgroundColor; } catch { return ConsoleColor.Black; } }
                set { try { Console.BackgroundColor = value; } catch { } }
            }

            public static void ResetColor()
            {
                try
                {
                    Console.ResetColor();
                }
                catch { }
            
            }

            public static int Width { get { try { return Console.BufferWidth; } catch { return 0; } } }
            public static string ReadLine()
            {
                try
                {
                    return Console.ReadLine();
                }
                catch { }
                return "";
            }
            public static void WriteLine()
            {
                try
                {
                    Console.WriteLine();
                }
                catch { }
            }

            public static void Write(string Text)
            {
                try
                {
                    Console.Write(Text);
                }
                catch { }
            }

            public static void WriteLine(string Text)
            {
                try
                {
                    Write(Text);
                    WriteLine();
                }
                catch { }
            }

            public static void Bar(ConsoleColor Color)
            {
                int width = Terminal.Width;
                if (width < 5) { width = 80 - 2; }
                Terminal.BackgroundColor = Color;
                for (int n = 0; n < width - 1; n++)
                    Write(" ");
                ResetColor();
                WriteLine();
            }
			public static void Line()
			{
                int width = Terminal.Width;
				if (width < 5) { width = 80 - 2; }
				for (int n = 0; n < width - 1; n++)
					Write ("-");
				WriteLine ();
			}

			public static void DoubleLine()
			{
                int width = Terminal.Width;
				if (width < 5) { width = 80 - 2; }
				for (int n = 0; n < width - 1; n++)
					Write ("=");
				WriteLine ();
			}

			public static void CenterText(string Text)
			{
                try
                {
                    Text = Text.Trim();
                    int width = Width;
                    if (width < 5) { width = 80 - 2; }
                    int half = (width - Text.Length) / 2;
                    int n = 0;
                    for (n = 0; n < half; n++)
                        Console.Write(' ');
                    n += Text.Length;
                    Console.Write(Text);
                    Console.WriteLine();
                }
                catch { }
			}
			public static void Clear()
			{
                try
                {
                    Console.Clear();
                }
                catch { }
			}
		}

        static void DisplayInfo(string text)
        {
            int width = Terminal.Width - 2;
            if (width < 5) { width = 80 - 2; }
            string[] lines = text.Split('\n', '\r');
            Console.WriteLine("          |");
            for (int n = 0; n < lines.Length; n++)
            {
                string line = lines[n].Trim();
                
                if (line == "") { continue; }
                if (line.Length + "          | ".Length >= width)
                {
                    string shortline = "          | ";
                    for (int x = 0; x < line.Length; x++)
                    {
                        shortline += line[x];
                        if (shortline.Length >= width)
                        {
                            Console.WriteLine(shortline);
                            shortline = "          | ";
                        }
                    }
                    if (shortline != "          | ")
                    {
                        Console.WriteLine(shortline);
                    }
                }
                else
                {
                    Console.WriteLine("          | " + line);
                }
                if (n != lines.Length - 1)
                {
                    Console.WriteLine("          |");
                }
            }
			Console.WriteLine("          |");

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Parser
{
    public class AR
    {
        class File
        {
            internal static File Read(System.IO.Stream Stream)
            {
                byte[] name = new byte[16];
                int readSize = Stream.Read(name, 0, name.Length);
                if (name.Length != readSize)
                {
                    if (readSize <= 0) { return null; }
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                byte[] timestamp = new byte[16];
                if (timestamp.Length != Stream.Read(timestamp, 0, timestamp.Length))
                {
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                byte[] ownerID = new byte[6];
                if (ownerID.Length != Stream.Read(ownerID, 0, ownerID.Length))
                {
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                byte[] groupID = new byte[6];
                if (groupID.Length != Stream.Read(groupID, 0, groupID.Length))
                {
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                byte[] fileMode = new byte[8];
                if (fileMode.Length != Stream.Read(fileMode, 0, fileMode.Length))
                {
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                byte[] fileSize = new byte[10];
                if (fileSize.Length != Stream.Read(fileSize, 0, fileSize.Length))
                {
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                byte[] fileMagick = new byte[2];
                if (fileMagick.Length != Stream.Read(fileMagick, 0, fileMagick.Length))
                {
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                if (fileMagick[0] != 0x60 || fileMagick[1] != 0x0A)
                {
                    throw new Exception("Invalid or corrupted file header in Archive");
                }
                return null;
            }
        }

        internal static AR ReadNoMagick(System.IO.Stream Stream)
        {
            List<File> files = new List<File>();
            File current = null;
            do
            {
                current = File.Read(Stream);
            } while (current != null);

            return null;
        }
        public static AR Read(System.IO.Stream Stream)
        {
            byte[] magick = new byte[8];
            Stream.Read(magick, 0, magick.Length);
            if (magick[0] != '!' ||
                magick[1] != '<' ||
                magick[2] != 'a' ||
                magick[3] != 'r' ||
                magick[4] != 'c' ||
                magick[5] != 'h' ||
                magick[6] != '>')
            {
                throw new Exception("Invalid or corrupted Archive");
            }
            return ReadNoMagick(Stream);
        }
    }
}

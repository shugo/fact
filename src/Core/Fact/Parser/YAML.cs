﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Parser
{
    public class YAML
    {
        static int ParseSpace(string text, ref int at)
        {
            int length = text.Length;
            // Count the number of space from the begining of the line
            int count = 0;
            for (; at < length; at++)
            {
                if (!Char.IsWhiteSpace(text, at))
                {
                    if (text[at] == '#')
                    {
                        for (; at < length && text[at] != '\n' && text[at] != '\r'; at++) ;
                        count = 0;
                        continue;
                    }
                    return count;
                }

                if (text[at] == '\n' || text[at] == '\r') { count = 0; }
                else if (text[at] == '\t') { count += 4; }
                else { count++; }
            }
            return count;
        }
        static void AddToList(Node List, Node Value)
        {
            while (Value != null && Value.Key == "" && Value.Children.Count == 1)
            { Value = Value.Children[0]; }
            if (Value == null || List == null) { return; }
            if (Value.Key == "")
            {
                foreach (Node child in Value.Children)
                { List.Children.Add(child); }
            }
            else
            {
                List.Children.Add(Value);
            }
        }

        static string ParseListName(string text, ref int at)
        {
            StringBuilder builder = new StringBuilder();
            int length = text.Length;
            for (; at < length; at++)
            { if (text[at] == ',' || text[at] == ']') { break; } builder.Append(text[at]); }
            ParseSpace(text, ref at);
            return builder.ToString();
        }

        static string ParseListOneLineName(string text, ref int at)
        {
            StringBuilder builder = new StringBuilder();
            int length = text.Length;
            for (; at < length; at++)
            { if (text[at] == '\n' || text[at] == '\r' || text[at] == '#') { break; } builder.Append(text[at]); }
            ParseSpace(text, ref at);
            return builder.ToString();
        }

        static Node ParseList(string text, ref int at)
        {
            Node List = new Node();
            ParseSpace(text, ref at);
            if (text[at] == '[')
            {
                at++;
                while (at < text.Length)
                {
                    int oldAt = at;
                    ParseSpace(text, ref at);
                    Node Item = null;
                    if (text[at] == '[')
                    {
                        Item = ParseList(text, ref at);
                    }
                    else if (text[at] == '{')
                    {
                        Item = ParseKeyValueList(text, ref at);
                    }
                    else if (text[at] == ',')
                    {
                        at++;
                        continue;
                    }
                    else if (text[at] == '~')
                    {
                        List.Children.Add(new Node());
                        ParseSpace(text, ref at);
                    }
                    else
                    {
                        string name = ParseListName(text, ref at);
                        if (name != "")
                        { Item = new Node(name); }
                        ParseSpace(text, ref at);
                    }

                    if (Item != null)
                    { AddToList(List, Item); }

                    if (text[at] == ',')
                    {
                        at++;
                        continue;
                    }
                    else if (text[at] == ']')
                    {
                        at++;
                        ParseSpace(text, ref at);
                        return List;
                    }
                    if (oldAt == at) { at++; } // Oups error try to skip it
                }
                return List;
            }
            else if (text[at] == '-')
            {
                at++;
                while (at < text.Length)
                {
                    int oldAt = at;
                    ParseSpace(text, ref at);
                    Node Item = null;
                    if (text[at] == '[')
                    {
                        Item = ParseList(text, ref at);
                    }
                    else if (text[at] == '{')
                    {
                        Item = ParseKeyValueList(text, ref at);
                    }
                    else if (text[at] == '~')
                    {
                        List.Children.Add(new Node());
                        ParseSpace(text, ref at);
                    }
                    else
                    {
                        string name = ParseListOneLineName(text, ref at);
                        if (name != "")
                        { Item = new Node(name); }
                        ParseSpace(text, ref at);
                    }

                    AddToList(List, Item);

                    ParseSpace(text, ref at);
                    if (at >= text.Length) { return List; }
                    if (text[at] != '-')
                    {
                        break;
                    }
                    if (oldAt == at) { at++; } // Oups error try to skip it
                }
                return List;
            }
            return null;
        }

        static Node ParseRelationalTree(string text, ref int at)
        {
            Node List = new Node();
            ParseSpace(text, ref at);
            if (text[at] == '-')
            {
                int offset = DetectOffset(text, at);
                at++;
                while (at < text.Length)
                {
                    int oldAt = at;
                    ParseSpace(text, ref at);
                    Node Item = ParseKeyValue(text, ref at);
                    AddToList(List, Item);
                    ParseSpace(text, ref at);
                    if (at >= text.Length) { return List; }
                    if (text[at] == '-')
                    {
                        at++;
                        continue;
                    }
                    if (Item != null)
                    {
                        ParseSpace(text, ref at);
                        int innerOffset = DetectOffset(text, at);
                        while (at < text.Length)
                        {
                            int oldAt2 = at;

                            ParseSpace(text, ref at);

                            Node Child = ParseKeyValue(text, ref at);
                            AddToList(Item, Child);
                            
                            ParseSpace(text, ref at);
                            if (at >= text.Length) { return List; }
                            if (text[at] == '-')
                            {
                                
                                at++;
                                break;
                            }
                            if (Child != null)
                            {
                                int newInnerOffset = DetectOffset(text, at);
                                if (newInnerOffset > innerOffset)
                                {
                                    Node Result = ParseItem(text, ref at);
                                    while (Result != null && Result.Key == "" && Result.Children.Count == 1)
                                    {
                                        Result = Result.Children[0];
                                    }
                                    AddToList(Child, Result);
                                    if (text[at] == '-')
                                    {
                                        at++;
                                        break;
                                    }
                                }
                            }
                            if (oldAt2 == at) { at++; } // Oups error try to skip it
                        }

                    }
                    if (oldAt == at) { at++; } // Oups error try to skip it
                }
                return List;
            }
            return null;
        }

        static int DetectOffset(string text, int at)
        {
            if (at >= text.Length)
                at = text.Length - 1;
            int count = 0;
            if (!Char.IsWhiteSpace(text, at))
                at--;
            while (at >= 0)
            {
                if (!Char.IsWhiteSpace(text, at))
                    return count;
                if (text[at] == '\n' || text[at] == '\r')
                    return count;
                if (text[at] == '\t')
                    count += 4;
                else
                    count++;
                at--;
            }
            return count;
        }

        static Node ParseKeyValueList(string text, ref int at)
        {
            Node List = new Node();
            ParseSpace(text, ref at);
            if (at >= text.Length) { return new Node(""); }
            if (text[at] == '{')
            {
                at++;
                while (at < text.Length)
                {
                    ParseSpace(text, ref at);
                    int oldAt = at;
                    Node Item = ParseKeyValue(text, ref at);
                    AddToList(List, Item);
                    ParseSpace(text, ref at);
                    if (at >= text.Length) { return List; }
                    if (text[at] == ',') { at++; continue; }
                    if (text[at] == '}') { at++; ParseSpace(text, ref at); return List; }
                    if (oldAt == at) { at++; } // Oups error try to skip it
                }
            }
            else
            {
                int offset = DetectOffset(text, at);
                Node LastItem = null;
                while (at < text.Length)
                {
                    
                    ParseSpace(text, ref at);
                    int newoffset = DetectOffset(text, at);
                    if (newoffset > offset)
                    {
                        Node Result = ParseItem(text, ref at);
                        while (Result != null && Result.Key == "" && Result.Children.Count == 1)
                        {
                            Result = Result.Children[0];
                        }
                        if (LastItem != null)
                        { AddToList(LastItem, Result); }
                        else
                        { AddToList(List, Result); }
                        continue;
                    }
                    else if (newoffset < offset)
                    {
                        return List;
                    }
                    int oldAt = at;
                    Node Item = ParseKeyValue(text, ref at);
                    AddToList(List, Item);
                    if (Item != null)
                        LastItem = Item;
                    ParseSpace(text, ref at);
                    if (at >= text.Length) { return List; }
                    if (text[at] == '-') { return List; }
                    if (oldAt == at) { at++; } // Oups error try to skip it
                }
            }
            return List;
        }

        static string ParseRawBlock(string text, ref int at)
        {
            ParseSpace(text, ref at);
            if (at >= text.Length)
                return "";
            if (text[at] != '|')
                return "";
            at++;
            ParseSpace(text, ref at);
            int offset = DetectOffset(text, at);
            StringBuilder block = new StringBuilder();
            for (; at < text.Length; at++)
            {

                if (text[at] == '\n' || text[at] == '\r')
                {
                    block.AppendLine();
                    ParseSpace(text, ref at);
                    if (DetectOffset(text, at) < offset) { return block.ToString(); }

                    at--;
                }
                else
                {
                    block.Append(text[at]);
                }

            }
            return block.ToString();
        }

        static Node ParseKeyValue(string text, ref int at)
        {
            StringBuilder key = new StringBuilder();
            ParseSpace(text, ref at);
            int length = text.Length;
            for (; at < length; at++)
            { if (text[at] == ':') { break; } key.Append(text[at]); }
            ParseSpace(text, ref at);
            if (text[at] != ':') { return null; }
            at++;
            Node keyNode = new Node(key.ToString().Trim());
            if (text[at] == '\n' || text[at] == '\r' || text[at] == '#')
            {
                ParseSpace(text, ref at); if (text[at] == '[') { keyNode.Children.Add(ParseList(text, ref at)); }
                else if (text[at] == '-')
                {
                    if (IsRelationalTree(text, at)) { keyNode.Children.Add(ParseRelationalTree(text, ref at)); }
                    else { keyNode.Children.Add(ParseList(text, ref at)); }
                }
                else if (text[at] == '{') { keyNode.Children.Add(ParseKeyValueList(text, ref at)); }
                else if (text[at] == '~') { keyNode.Children.Add(new Node()); }
                else
                {
                    keyNode.Children.Add(ParseKeyValueList(text, ref at));
                }
                return keyNode;
            }
            ParseSpace(text, ref at);
            if (at >= length) { return new Node(key.ToString()); }
            if (text[at] == '[') { keyNode.Children.Add(ParseList(text, ref at)); }
            else if (text[at] == '-')
            {
                if (IsRelationalTree(text, at)) { keyNode.Children.Add(ParseRelationalTree(text, ref at)); }
                else { keyNode.Children.Add(ParseList(text, ref at)); }
            }
            else if (text[at] == '{') { keyNode.Children.Add(ParseKeyValueList(text, ref at)); }
            else if (text[at] == '~') { keyNode.Children.Add(new Node()); }
            else if (text[at] == '|') { keyNode.Children.Add(new Node(ParseRawBlock(text, ref at))); }
            else
            {
                StringBuilder builder = new StringBuilder();

                for (; at < length; at++)
                {
                    if (text[at] == '"')
                    {
                        at++;
                        for (; at < length; at++)
                        { if (text[at] == '"') { break; } builder.Append(text[at]); }
                    }
                    else
                    {
                        if (text[at] == '}' || text[at] == ',' || text[at] == '#' || text[at] == '\n' || text[at] == '\r') { break; } builder.Append(text[at]);
                    }
                }
                keyNode.Children.Add(new Node(builder.ToString()));
            }
            return keyNode;
        }

        static bool IsRelationalTree(string text, int at)
        {
            if (at >= text.Length) { return false; }
            if (text[at] != '-') { return false; }
            int length = text.Length;
            for (; at < length; at++)
            { if (!Char.IsWhiteSpace(text, at)) { break; } }
            for (; at < length; at++)
            {
                if (text[at] == ':') { return true; }
                if (text[at] == '\n' ||
                    text[at] == '\r' ||
                    text[at] == '[' ||
                    text[at] == '{' ||
                    text[at] == '|' ||
                    text[at] == '>' ||
                    text[at] == ',' ||
                    text[at] == ';') { return false; }
            }
            return false;
        }

        static Node ParseItem(string text, ref int at)
        {
            ParseSpace(text, ref at);
            if (at >= text.Length) { return new Node(""); }
            if (text[at] == '[') { return ParseList(text, ref at); }
            if (text[at] == '-')
            {
                if (IsRelationalTree(text, at)) { return ParseRelationalTree(text, ref at); }
                else { return ParseList(text, ref at); }
            }
            if (text[at] == '{') { return ParseKeyValueList(text, ref at); }
            else { return ParseKeyValueList(text, ref at); }
            ParseSpace(text, ref at);
            return null;
        }

        static Node ParseDocument(string text, ref int at)
        {
            Node _Root = new Node();
            Node _Current = null;

            while (at < text.Length)
            {
                ParseSpace(text, ref at);
                if (at < text.Length - 3 && text[at] == '.' && text[at + 1] == '.' && text[at + 2] == '.')
                {
                    at = at + 3;
                    continue;
                }
                else if (at < text.Length - 3 && text[at] == '-' && text[at + 1] == '-' && text[at + 2] == '-')
                {
                    at = at + 3;
                    _Current = new Node();
                    _Root.Children.Add(_Current);

                    continue;
                }
                else
                {
                    int oldAt = at;
                    Node result = ParseItem(text, ref at);
                    if (result != null)
                    {
                        if (_Current == null)
                        {
                            _Current = new Node();
                            _Root.Children.Add(_Current);
                        }
                        _Current.Children.Add(result);
                    }
                    if (oldAt == at) { at++; } // Oups error try to skip it
                }
            }
            while (_Root != null && _Root.Key == "" && _Root.Children.Count == 1)
            { _Root = _Root.Children[0]; }
            return _Root;
        }

        public static Node Parse(string Text)
        {
            int at = 0;
            return ParseDocument(Text, ref at);
        }
    }
}

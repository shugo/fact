﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Parser
{
    public class Python
    {

        public class Element
        {

        }

        public class Scope : Element
        {
            internal int _Spacing = 0;
            internal List<Element> _Elements = new List<Element>();
            internal Scope _Parent = null;
            internal bool _Modifier = false;

        }

        public class Import : Element
        {
            internal string _Library = "";
            public Import(string Library) { _Library = Library; }
        }

        public class If : Scope
        {
            public If() { _Modifier = true; }
        }

        private class Else : Scope
        {
            public Else() { _Modifier = true; }
        }

        private class ElseIf : Scope
        {
            public ElseIf() { _Modifier = true; }
        }

        public class For : Scope
        {
            public For() { _Modifier = true; }

        }

        public class While : Scope
        {
            public While() { _Modifier = true; }
        }

        public class Def : Scope
        {
            public class Argument
            {
                string _Name = "";
                public Argument(string Name) { _Name = Name; }
                public override string ToString()
                {
                    return _Name;
                }
            }
            internal string _Name = "";
            internal List<Argument> _Arguments = new List<Argument>();
            public Def(string Name) { _Modifier = true; _Name = Name; }
            public override string ToString()
            {
                StringBuilder builder = new StringBuilder();
                for (int n = 0; n < _Arguments.Count; n++)
                {
                    if (n == 0) { builder.Append('('); }
                    else { builder.Append(','); }
                    builder.Append(_Arguments[n]);
                    if (n == _Arguments.Count - 1) { builder.Append(')'); }
                }
                return "def " + _Name +  builder.ToString() + ":";
            }
        }

        public class Class : Scope
        {
            string _Name = "";
            public Class(string Name) { _Modifier = true; _Name = Name; }
            public override string ToString()
            {
                return "class " + _Name + ":";
            }
        }

        public class Instruction : Element
        {
            List<string> _line = new List<string>();
            public Instruction(List<string> Line)
            {
                _line = new List<string>(Line);
            }

            public override string ToString()
            {
                string txt = "";
                foreach (string word in _line) { txt += " " + word; }
                return txt.Trim();
            }
        }

        public class Assignation : Element
        {
            internal Element _Left = null;
            internal Element _Right = null;

            public Assignation(Element Left, Element Right)
            {
                _Left = Left;
                _Right = Right;
            }

            public override string ToString()
            {
                if (_Left != null && _Right != null) { return _Left.ToString() + " = " + _Right.ToString(); }
                return "<invalid assignation>";
            }
        }

        public class Dictionary : Element
        {
            public Dictionary() { }

            public override string ToString()
            {
                return "";
            }
        }

        public class Expression : Element
        {
            public Expression() { }
        }

        static Element ParseExpression(List<string> Line)
        {
            Stack<string> op = new Stack<string>();
            Stack<string> rpn = new Stack<string>();
            for (int n = 0; n < Line.Count; n++)
            {
                if (Line[n] == "&")
                {
                    string tos = "";
                    do
                    {
                        if (tos != "") { rpn.Push(tos); }
                        if (op.Count > 0) { tos = op.Peek(); }
                        else { tos = ""; }
                    }
                    while (tos == "<<" || tos == ">>" || tos == "+" || tos == "-" || tos == "*" || tos == "/" || tos == "//" || tos == "%");
                    op.Push(Line[n]);
                }
                else if (Line[n] == "<<" || Line[n] == ">>")
                {
                    string tos = "";
                    do
                    {
                        if (tos != "") { rpn.Push(tos); }
                        if (op.Count > 0) { tos = op.Peek(); }
                        else { tos = ""; }
                    }
                    while (tos == "+" || tos == "-" || tos == "*" || tos == "/" || tos == "//" || tos == "%");
                    op.Push(Line[n]);
                }
                else if (Line[n] == "+" || Line[n] == "-")
                {
                    string tos = "";
                    do
                    {
                        if (tos != "") { rpn.Push(tos); }
                        if (op.Count > 0) { tos = op.Peek(); }
                        else { tos = ""; }
                    }
                    while (tos == "*" || tos == "/" || tos == "//" || tos == "%");
                    op.Push(Line[n]);
                }
                else
                {
                    rpn.Push(Line[n]);
                }
            }
            return new Instruction(Line);

        }

        static Element ParseLine(List<string> Line)
        {
            if (Line.Count > 0)
            {
                if (Line[0] == "import")
                {
                    
                }
                switch (Line[0])
                {
                    case "import": if (Line.Count >= 2) { return new Import(Line[1]); } else { throw new Exception("Python: Invalid import syntax"); }
                    case "from":
                        if (Line.Count >= 4 && Line[2] == "import")
                        {
                            if (Line[1] == "__future__") { }
                            return new Import(Line[3]);
                        }
                        else
                        {
                            throw new Exception("Python: Invalid from/import syntax");
                        }
                    case "if": return new If();
                    case "else": return new Else();
                    case "elif": return new ElseIf();
                    case "while": return new While();



                    case "for": return new For();
                    case "def":
                        if (Line.Count >= 2)
                        {
                            Def def = new Def(Line[1]);
                            if (!CheckVariableName(def._Name)) { throw new Exception("Python: '" + def._Name + "' is not a valid definition name"); }
                            if (Line[2] != ":" && Line[2] != "(")
                            {
                                throw new Exception("Python: Invalid definition");
                            }
                            if (Line[2] == "(")
                            {
                                if (Line[Line.Count - 1] != ":" || Line[Line.Count - 2] != ")") { throw new Exception("Python: Invalid definition"); }
                                for (int n = 3; n < Line.Count - 2; n++)
                                {
                                    bool multArg = false;
                                    bool keyArg = false;

                                    if (Line[n] == "*") { multArg = true; n++; }
                                    else if (Line[n] == "**") { keyArg = true; n++; }

                                    if (n >= Line.Count - 2) { throw new Exception("Python: Invalid definition"); }

                                    if (Line[n + 1] == "=")
                                    {
                                        List<string> expression = new List<string>();
                                        string name = Line[n];
                                        if (!CheckVariableName(name)) { throw new Exception("Python: '" + name + "' is not a valid argument name"); }
                                        n += 2;
                                        int pcount = 0;
                                        for (; n < Line.Count - 2; n++)
                                        {
                                            if (Line[n] == "(" || Line[n] == "[" || Line[n] == "{") { pcount++; }
                                            else if (Line[n] == "}" || Line[n] == "]" || Line[n] == ")") { pcount--; }
                                            if (pcount == 0 && Line[n] == ",") { break; }
                                            expression.Add(Line[n]);
                                        }
                                        if(Line[n] != "," && Line[n] != ")") { throw new Exception("Python: Invalid definition argument expected ',' or ')' after '" + Line[n - 1] + " = <default>'"); }

                                        def._Arguments.Add(new Def.Argument(name));

                                    }
                                    else
                                    {
                                        if (!CheckVariableName(Line[n])) { throw new Exception("Python: '" + Line[n] + "' is not a valid argument name"); }
                                        def._Arguments.Add(new Def.Argument(Line[n]));
                                        n++;
                                        if (Line[n] != "," && Line[n] != ")") { throw new Exception("Python: Invalid definition argument expected ',' or ')' after '" + Line[n - 1] + "'"); }
                                    }
                                }
                            }
                            return def;
                        }
                        else
                        {
                            throw new Exception("Python: Invalid definition");
                        }
                    case "class":
                        if (Line.Count >= 2)
                        {
                            return new Class(Line[1]);
                        }
                        else
                        {
                            throw new Exception("Python: Invalid class definition");
                        }

                    case "try":
                        break;
                        
                }
            }
            return ParseExpression(Line);
        }


        public static string ParseString(string Data, int Line, ref int Offset)
        {
            bool escape = false;
            string str = "";

            for (Offset = Offset + 1; Offset < Data.Length && Data[Offset] != '"'; Offset++)
            {
                if (escape)
                {
                    switch (Data[Offset])
                    {
                        case 'n': str += "\n"; break;
                        case 'r': str += "\r"; break;
                        case '0': str += "\0"; break;
                        case 'a': str += "\a"; break;
                        case 't': str += "\t"; break;
                        case 'f': str += "\f"; break;
                        case 'v': str += "\v"; break;
                        case 'b': str += "\b"; break;
                        case 'x':
                            {
                                string hexvalue = "";
                                Offset++;
                                if (Offset < Data.Length && Data[Offset] != '"' && (Data[Offset] >= '0' && Data[Offset] <= '9') || (Data[Offset] >= 'a' && Data[Offset] <= 'f') || (Data[Offset] >= 'A' && Data[Offset] <= 'F'))
                                {
                                    hexvalue += Data[Offset]; Offset++;
                                    if (Offset < Data.Length && Data[Offset] != '"' && (Data[Offset] >= '0' && Data[Offset] <= '9') || (Data[Offset] >= 'a' && Data[Offset] <= 'f') || (Data[Offset] >= 'A' && Data[Offset] <= 'F'))
                                    {
                                        hexvalue += Data[Offset]; Offset++;
                                    }
                                }

                                if (hexvalue == "") { throw new Exception("Python(l." + Line.ToString() + "): invalid escape char format in string"); }
                                uint value = Convert.ToUInt32(hexvalue);
                                str += (char)value;
                            }
                            break;
                        default: str += "\\" + Data[Offset]; break;
                    }
                    escape = false;
                }
                else
                {
                    if (Data[Offset] == '\\') { escape = true; }
                    else { str += Data[Offset]; }
                }
            }
            return str;
        }

        public static Dictionary ParseDictionary(string Data, ref int Line, ref int Offset)
        {
            Dictionary dictionary = new Dictionary();
 
            for (Offset = Offset + 1; Offset < Data.Length && Data[Offset] != '"'; Offset++)
            {
                if (Data[Offset] == '"')
                {
                    string key = ParseString(Data, Line, ref Offset);
                    for (; Offset < Data.Length; Offset++)
                    {
                        if (Data[Offset] == ':')
                        {
                            for (; Offset < Data.Length; Offset++)
                            {
                            }
                            break;
                        }

                        if (Data[Offset] == '\n' ) { Line++; }
                        if (Data[Offset] == '\t' || Data[Offset] == ' ' || Data[Offset] == '\r') { continue; }
                        if (Data[Offset] == '}') { throw new Exception("Python (l." + Line.ToString() + "): expected a value for the key '" + key + "'"); }
                        throw new Exception("Python (l." + Line.ToString() + "): unexpected character '" + Data[Offset] + "'");
                    }
                }
            }

            return dictionary;
        }

        public static Scope Parse(string Data)
        {
            return Parse(Data, "<input>");
        }

        public static bool CheckVariableName(string Name)
        {
            if (Name.Length == 0) { return false; }
            if (!char.IsLetter(Name[0]) && Name[0] != '_') { return false; }
            for (int n = 1; n < Name.Length; n++) { if (!char.IsLetterOrDigit(Name[0]) && Name[0] != '_') { return false; } }
            return true;
        }

        public static Scope Parse(string Data, string Filename)
        {
            // First  skip the #!
            int n = 0;
            int spaceoffset = 0;
            string word = "";
            Scope scope = new Scope();
            Scope root = scope;
            Assignation assignation = null;
            int line = 1;
            List<string> pendingLine = new List<string>();
            bool pendingOneliner = false;
            bool consumedOneliner = false;
            Stack<char> pstack = new Stack<char>();
            bool startofline = false;
            for (; n < Data.Length; n++)
            {
                if (Data[n] == '\n' || Data[n] == '\r')
                {
                    if (word != "") { pendingLine.Add(word); }
                    if (pstack.Count > 0)
                    {
                        if (Data[n] == '\n') { line++; }
                        continue;
                    }
                    else
                    {
                        if (!startofline)
                        {
                            if (pendingOneliner && consumedOneliner)
                            {
                                scope._Elements.Add(ParseLine(pendingLine));
                                pendingLine.Clear();
                                scope = scope._Parent;
                            }
                            else
                            {
                                if (spaceoffset > scope._Spacing)
                                {
                                    Scope subscope = new Scope();
                                    subscope._Spacing = spaceoffset;
                                    scope._Elements.Add(subscope);
                                    subscope._Parent = scope;
                                    scope = subscope;
                                }
                                else if (spaceoffset < scope._Spacing)
                                {
                                    if (scope._Parent == null) { throw new Exception("Python (" + Filename + " l." + line.ToString() + "): indentation error"); }
                                    if (scope._Parent._Spacing < spaceoffset)
                                    {
                                        scope = scope._Parent;
                                        Scope subscope = new Scope();
                                        subscope._Spacing = spaceoffset;
                                        scope._Elements.Add(subscope);
                                        subscope._Parent = scope;
                                        scope = subscope;
                                    }
                                    else
                                    {
                                        while (scope._Parent._Spacing > spaceoffset || (scope._Parent._Modifier && scope._Parent._Spacing >= spaceoffset))
                                        {
                                            scope = scope._Parent;
                                        }
                                        scope = scope._Parent;
                                        if (scope._Spacing != spaceoffset) { throw new Exception("Python (" + Filename + " l." + line.ToString() + "): indentation error"); }
                                    }

                                }
                                if (pendingLine.Count > 0)
                                {
                                    if (assignation != null)
                                    {
                                        assignation._Right = ParseLine(pendingLine);
                                        scope._Elements.Add(assignation);
                                        pendingLine.Clear();
                                        assignation = null;
                                    }
                                    else
                                    {
                                        Element element = ParseLine(pendingLine);
                                        scope._Elements.Add(element);

                                        if (element is Scope)
                                        {
                                            (element as Scope)._Spacing = spaceoffset;
                                            (element as Scope)._Parent = scope;
                                            scope = (element as Scope);
                                        }

                                        pendingLine.Clear();
                                    }
                                }
                                else
                                {
                                    if (assignation != null)
                                    {
                                        throw new Exception("Python (" + Filename + " l." + line.ToString() + "): missing right opperand after '=' ");
                                    }
                                }
                            }
                        }
                        startofline = true;
                        pendingOneliner = false;
                        word = "";
                        if (Data[n] == '\n') { line++; }
                        spaceoffset = 0;
                        continue;
                    }
                }

                if (Data[n] == ' ')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    if (startofline) { spaceoffset++; }
                    continue;
                }

                if (Data[n] == '\t')
                {
                    if (word != "") { pendingLine.Add(word); }
                    if (startofline) { spaceoffset += 4; }
                    continue;
                }

                if (Data[n] == '#')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    for (; n < Data.Length && Data[n] != '\n'; n++) { }
                    // replay the \n
                    n--;
                    continue;
                }

                startofline = false;
                consumedOneliner = true;

                if (n < Data.Length - 2 && Data[n] == '"' && Data[n + 1] == '"' && Data[n + 2] == '"')
                {
                    n += 3;
                    StringBuilder builder = new StringBuilder();
                    builder.Append("\"");
                    for (; n < Data.Length; n++)
                    {

                        if (Data[n] == '\n' || Data[n] == '\r')
                        {
                            builder.Append(Data[n]);
                            spaceoffset = 0;
                            if (Data[n] == '\n') { line++; }
                            continue;
                        }
                        else
                        {

                            if (n < Data.Length - 2 && Data[n] == '"' && Data[n + 1] == '"' && Data[n + 2] == '"')
                            {
                                spaceoffset += 3;
                                n += 2;
                                break;
                            }
                            spaceoffset += 1;
                            builder.Append(Data[n]);
                        }
                    }
                    builder.Append("\"");

                    if (pendingLine.Count > 0 || assignation != null)
                    {
                        pendingLine.Add(builder.ToString());
                    }
                    continue;

                }

                if (Data[n] == ';')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    scope._Elements.Add(ParseLine(pendingLine));
                    pendingLine.Clear();
                    continue;
                }

                if (Data[n] == ':')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    pendingLine.Add(":");
                    if (pstack.Count == 0)
                    {
                        consumedOneliner = false;
                        pendingOneliner = true;
                    }
                    continue;
                }

                if (Data[n] == '+' || Data[n] == '-' || Data[n] == '*' || Data[n] == '/')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    if (n == Data.Length - 1)
                    {
                        pendingLine.Add(Data[n].ToString());
                    }
                    else
                    {
                        if (Data[n + 1] == Data[n]) { pendingLine.Add(Data[n].ToString() + Data[n].ToString()); n++; }
                        else { pendingLine.Add(Data[n].ToString()); }
                    }
                    continue;
                }

                if (Data[n] == '!' || Data[n] == '<' || Data[n] == '>')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    if (n == Data.Length - 1)
                    {
                        pendingLine.Add(Data[n].ToString());
                    }
                    else
                    {
                        if (Data[n + 1] == '=') { pendingLine.Add(Data[n].ToString() + "="); n++; }
                        else if (Data[n + 1] == '>' && Data[n] == '>') { pendingLine.Add(">>"); n++; }
                        else if (Data[n + 1] == '<' && Data[n] == '<') { pendingLine.Add("<<"); n++; }
                        else { pendingLine.Add(Data[n].ToString()); }
                    }
                    continue;
                }

                if (Data[n] == ',' || Data[n] == '|' || Data[n] == '&')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    pendingLine.Add(Data[n].ToString());
                    continue;
                }

                if (Data[n] == '"')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    pendingLine.Add("\"" + ParseString(Data, line, ref n) + "\"");
                    continue;
                }

                if (Data[n] == '=')
                {
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    if (n + 1 < Data.Length && Data[n + 1] == '=')
                    {
                        pendingLine.Add("==");
                        n = n + 1;
                    }
                    else
                    {
                        if (pstack.Count > 0)
                        {
                            pendingLine.Add("=");
                        }
                        else
                        {
                            if (pendingLine.Count == 0)
                            {
                                throw new Exception("Python (" + Filename + " l." + line.ToString() + "): missing left opperand before '=' ");
                            }
                            Element left = ParseLine(pendingLine);
                            pendingLine.Clear();
                            assignation = new Assignation(left, null);
                        }
                    }
                    continue;
                }

                if (Data[n] == '[' || Data[n] == '{' || Data[n] == '(')
                {
                    string str = "";
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    pendingLine.Add(Data[n].ToString());
                    pstack.Push(Data[n]);
                    continue;
                }

                if (Data[n] == ']')
                {
                    string str = "";
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    if (pstack.Count == 0 || pstack.Pop() != '[') { throw new Exception("Python (" + Filename + " l." + line.ToString() + "): missmatching square brackets"); }
                    pendingLine.Add("]");
                    continue;
                }

                if (Data[n] == '}')
                {
                    string str = "";
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    if (pstack.Count == 0 || pstack.Pop() != '{') { throw new Exception("Python (" + Filename + " l." + line.ToString() + "): missmatching curly brackets"); }
                    pendingLine.Add("}");

                    continue;
                }

                if (Data[n] == ')')
                {
                    string str = "";
                    if (word != "") { pendingLine.Add(word); }
                    word = "";
                    if (pstack.Count == 0 || pstack.Pop() != '(') { throw new Exception("Python (" + Filename + " l." + line.ToString() + "): missmatching parenthesis"); }
                    pendingLine.Add(")");
                    continue;
                }

                if (Data[n] >= '0' && Data[n] <= '9' && word == "")
                {
                    string number = Data[n].ToString();
                    n++;
                    if (n < Data.Length && (Data[n] == 'x' || Data[n] == 'X'))
                    {
                        for (; n < Data.Length; n++)
                        {
                            if ((Data[n] < '0' || Data[n] > '9') && (Data[n] < 'a' || Data[n] > 'f') && (Data[n] < 'A' || Data[n] > 'F')) { n--; break; }

                            number += Data[n];
                        }
                    }
                    else
                    {
                        for (; n < Data.Length; n++)
                        {
                            if (Data[n] < '0' || Data[n] > '9') { n--; break; }
                            number += Data[n];
                        }
                    }
                    pendingLine.Add(number);
                    continue;
                }

                word += Data[n];
            }

            return root;
        }
    }
}

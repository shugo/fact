﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Parser
{
    public class CommandLine
    {

        public Value this[string Key]
        {
            get 
            {
                if (StringInputs.ContainsKey(Key)) { return StringInputs[Key].Value; }
                if (BooleanInputs.ContainsKey(Key)) { return BooleanInputs[Key].Value; }
                if (StringListInputs.ContainsKey(Key)) { return StringListInputs[Key].Value; }
                return null;
            }
        }
        string _ProgramName = "";
        public string ProgramName { get { return _ProgramName; } set { _ProgramName = value; } }
        public class Value
        {
            public Value() { }
            public Value(string Default) { _RawValue = Default; }
            public Value(List<string> Default) { _RawListValue = Default; }

            internal string _RawValue = "";
            internal List<string> _RawListValue = new List<string>();
            internal Fact.Processing.Project _RawProject = null;
            internal bool _IsDefault = true;
            public bool IsDefault { get { return _IsDefault; } }
            public Fact.Processing.Project AsProject() { return _RawProject; }
            public bool AsBoolean() { return _RawValue.ToLower() == "true" || _RawValue.ToLower() == "yes"; }
            public string AsString() { return _RawValue; }
            public List<string> AsStringList() { return _RawListValue; }

            public static implicit operator string(Value value) { return value.AsString(); }
            public static implicit operator bool(Value value) { return value.AsBoolean(); }

        }

        public CommandLine()
        { }

        Dictionary<string, string> ShortForm = new Dictionary<string, string>();

        public void AddShortInput(string ShortInput, string FullInput) 
        {
            if (!ShortForm.ContainsKey(ShortInput)) { ShortForm.Add(ShortInput, FullInput); }
        }

        Dictionary<string, KeyValuePair<string, Value>> BooleanInputs = new Dictionary<string, KeyValuePair<string, Value>>();

        public void AddBooleanInput(string Key, string Description, bool Default)
        {
            if (!BooleanInputs.ContainsKey(Key))
            {
                BooleanInputs.Add(Key, new KeyValuePair<string, Value>(Description, new Value(Default ? "true" : "false")));
            }
        }
        Dictionary<string, KeyValuePair<string, Value>> StringInputs = new Dictionary<string, KeyValuePair<string, Value>>();
        Dictionary<string, KeyValuePair<string, Value>> StringListInputs = new Dictionary<string, KeyValuePair<string, Value>>();

        public void AddStringInput(string Key, string Description, string Default)
        {
            if (!StringInputs.ContainsKey(Key))
            {
                StringInputs.Add(Key, new KeyValuePair<string, Value>(Description, new Value(Default)));
            }
        }


        public void AddStringListInput(string Key, string Description, List<string> Default)
        {
            if (!StringListInputs.ContainsKey(Key))
            {
                StringListInputs.Add(Key, new KeyValuePair<string, Value>(Description, new Value(Default)));
            }
        }

        Dictionary<string, KeyValuePair<string, Value>> ProjectInputs = new Dictionary<string, KeyValuePair<string, Value>>();
        public void AddProjectInput(string Key, string Description)
        {
            if (!ProjectInputs.ContainsKey(Key))
            {
                ProjectInputs.Add(Key, new KeyValuePair<string, Value>(Description, new Value("")));
            }
        }

        public string GetShortOptionDisplay(string key)
        {
            foreach (KeyValuePair<string, string> reverse in ShortForm)
            {
                if (reverse.Value == key) { return " -" + reverse.Key + " /"; }
            }
            return "";
        }

        public void PrintUsage()
        {
            Console.WriteLine(" Usage: " + _ProgramName + " [OPTIONS] ");
            Console.WriteLine(" * Options:");
            if (BooleanInputs.Count > 0)
            {
                Console.WriteLine(" ** Switch:");
                foreach (string key in BooleanInputs.Keys)
                {
                    Console.WriteLine("    " + GetShortOptionDisplay(key) + " --" + key + ": " + BooleanInputs[key].Key);
                }
            }
            if (StringInputs.Count > 0)
            {
                Console.WriteLine(" ** Input strings:");
                foreach (string key in StringInputs.Keys)
                {
                    Console.WriteLine("    " + GetShortOptionDisplay(key) + " --" + key + " 'something': " + StringInputs[key].Key);
                }
            }
            if (StringListInputs.Count > 0)
            {
                Console.WriteLine(" ** Input string list:");
                foreach (string key in StringListInputs.Keys)
                {
                    Console.WriteLine("    " + GetShortOptionDisplay(key) + " --" + key + " 'something' 'something' ... : " + StringListInputs[key].Key);
                }
            }

            if (ProjectInputs.Count > 0)
            {
                Console.WriteLine(" ** Input project:");
                foreach (string key in ProjectInputs.Keys)
                {
                    if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
                    {
                        Console.WriteLine("    " + " --" + key + " '~/one/fact/project.ff': " + ProjectInputs[key].Key);
                    }
                    else
                    {
                        Console.WriteLine("    " + " --" + key + " 'C:\\one\\fact\\package.ff': " + ProjectInputs[key].Key);
                    }
                }
            }
        }

        public void PrintErrors()
        {
            lock (_Errors)
            {
                Console.WriteLine("Command line error:");
                foreach (string _ in _Errors)
                {
                    Console.WriteLine(_); 
                }
            }
        }

        List<string> _Errors = new List<string>();
        public List<string> Errors { get { return _Errors; } }

        public bool Parse(params string[] Arguments)
        {
            string ExpectValue = "";
            for (int n = 0; n < Arguments.Length; n++)
            {
                string key = Arguments[n];
                if (key.StartsWith("/")) { key = key.Substring(1); }
                else if (key.StartsWith("--")) { key = key.Substring(2); }
                else if (key.StartsWith("-")) { key = key.Substring(1); }
                else
                {
                    lock (_Errors)
                    {
                        _Errors.Add("Invalid argument " + key);
                    }
                    return false;
                }
                if (ShortForm.ContainsKey(key)) { key = ShortForm[key]; }
                if (BooleanInputs.ContainsKey(key))
                { BooleanInputs[key].Value._IsDefault = false; BooleanInputs[key].Value._RawValue = "true"; }
                else if (StringInputs.ContainsKey(key))
                {
                    n++;
                    if (n >= Arguments.Length) { lock (_Errors) { _Errors.Add("Missing argument after " + key); } return false; }
                    StringInputs[key].Value._IsDefault = false; StringInputs[key].Value._RawValue = Arguments[n];
                }
                else if (StringListInputs.ContainsKey(key))
                {
                    n++;
                    if (n >= Arguments.Length) { lock (_Errors) { _Errors.Add("Missing argument after " + key); } return false; }
                    StringListInputs[key].Value._IsDefault = false;
                    for (; n < Arguments.Length; n++)
                    {
                        if (Arguments[n].StartsWith("-") ||
                            Arguments[n].StartsWith("--") ||
                            Arguments[n].StartsWith("/"))
                        {
                            string possiblekey = Arguments[n];
                            if (possiblekey.StartsWith("/")) { possiblekey = possiblekey.Substring(1); }
                            else if (possiblekey.StartsWith("--")) { possiblekey = possiblekey.Substring(2); }
                            else if (possiblekey.StartsWith("-")) { possiblekey = possiblekey.Substring(1); }
                            if (ShortForm.ContainsKey(possiblekey) ||
                                BooleanInputs.ContainsKey(possiblekey) ||
                                StringInputs.ContainsKey(possiblekey) ||
                                ProjectInputs.ContainsKey(possiblekey) ||
                                StringListInputs.ContainsKey(possiblekey))
                            {
                                n--; break;
                            }
                        }
                        StringListInputs[key].Value._RawListValue.Add(Arguments[n]);
                    }

                }
                else if (ProjectInputs.ContainsKey(key))
                {
                    n++;
                    if (n >= Arguments.Length) { lock (_Errors) { _Errors.Add("Missing project path after " + key); } return false; }
                    ProjectInputs[key].Value._IsDefault = false;
                    ProjectInputs[key].Value._RawValue = Arguments[n];
                    if (System.IO.File.Exists(Arguments[n]) &&
                        (Arguments[n].ToLower().EndsWith(".ff") || Arguments[n].ToLower().EndsWith(".ffz")))
                    {
                        try
                        {
                            ProjectInputs[key].Value._RawProject = Fact.Processing.Project.Load(Arguments[n]);
                        }
                        catch
                        {
                            lock (_Errors) { _Errors.Add("Error while loading the project " + key + " from the file " + Arguments[n]); }
                        }
                    }
                    else if (System.IO.Directory.Exists(Arguments[n]))
                    {
                        ProjectInputs[key].Value._RawProject = new Fact.Processing.Project("");
                        try
                        {
                            ProjectInputs[key].Value._RawProject = new Fact.Processing.Project(System.IO.Path.GetFileName(Arguments[n]));
                            ProjectInputs[key].Value._RawProject.AddRealDirectory("/", Arguments[n]);
                        }
                        catch
                        {
                            lock (_Errors) { _Errors.Add("Error while loading the project " + key + " from the directory " + Arguments[n]); }
                        }
                    }

                }
                else
                {
                    lock (_Errors)
                    {
                        _Errors.Add("Invalid argument " + key);
                    }
                    return false;
                }
            }
            return true;
        }
    }
}

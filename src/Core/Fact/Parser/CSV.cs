﻿/*                                                                           *
 * Copyright © 2015, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Parser
{
    public class CSV
    {
        public CSV() { }
        public static string[][] ParseFile(string File, char separator)
        {
            if (!System.IO.File.Exists(File)) { throw new Exception("CSV not found '" + File + "'"); }
            return Parse(System.IO.File.ReadAllText(File), separator);
        }
        public static string[][] Parse(string Data, char separator)
        {
            string cell = "";
            List<string[]> rows = new List<string[]>();
            List<string> row = new List<string>();
            bool sawone = false;
            for (int n = 0; n < Data.Length; n++)
            {
                if (Data[n] == separator) { row.Add(cell.Trim()); cell = ""; sawone = true; }
                else if (Data[n] == '\n' || Data[n] == '\r')
                {
                    if (cell != "" || sawone) { row.Add(cell.Trim()); cell = ""; }
                    if (row.Count > 0) { rows.Add(row.ToArray()); row = new List<string>(); }
                    rows.Add(row.ToArray());
                    sawone = false;
                }
                else
                {
                    cell += Data[n];
                }
            }
            if (cell != "" || sawone) { row.Add(cell.Trim()); cell = ""; }
            if (row.Count > 0) { rows.Add(row.ToArray()); }
            return rows.ToArray();
        }
    }
}

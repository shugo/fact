/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Parser
{
	public class XML
	{
		public class Node
		{
            public Structures.Tree<Node> ToGenericTree()
            {
                Structures.Tree<Node> Root = new Fact.Structures.Tree<Node>(this);
                foreach (Node child in _Nodes) { Root.AddChild(child.ToGenericTree()); }
                return Root;
            }

			string _Type = "";
			string _Text = "";
			List<string> _AttributeName = new List<string>();
			List<string> _AttributeValue = new List<string>();
			List<Node> _Nodes = new List<Node>();
			Node _Parent = null;
			public string this[string Attribute_Name]
			{
				get 
				{
					for (int N = 0; N < _AttributeName.Count; N++)
					{
						if (_AttributeName[N] == Attribute_Name) { return _AttributeValue[N]; } 
					}
					return "";
				}
				set
				{
					for (int N = 0; N < _AttributeName.Count; N++)
					{
						if (_AttributeName[N] == Attribute_Name) { _AttributeValue[N] = value; return; }
					}
					_AttributeName.Add(Attribute_Name);
					_AttributeValue.Add(value);
				}
			}
            public List<string> AttributesName { get { return _AttributeName; } }
			public string Type
			{ get { return _Type; } }
			public string Text
			{ get { return _Text; } }
			public Node Parent
			{
				get { return _Parent; }
			}

            public Node FindNode(string Type, bool CaseSensitive)
            {
                if (CaseSensitive)
                {
                    if (_Type == Type) { return this; }
                }
                else
                {
                    if (_Type.ToLower() == Type.ToLower()) { return this; }
                }
                Node result = null;
                foreach (Node _ in _Nodes)
                { result = _.FindNode(Type, CaseSensitive); if (result != null) { return result; } }
                return null;
            }

			public Node Set_Text(string Text)
			{ _Text = Text; return this; }
			public Node()
			{
				_Type = "";
			}
			public Node(string Type)
			{
				_Type = Type;
			}
			public Node Add_Attribute(string Name, string Value)
			{
				for (int N = 0; N < _AttributeName.Count; N++)
				{
					if (_AttributeName[N] == Name)
					{
						_AttributeValue[N] = Value;
						return this;
					}
				}
				_AttributeName.Add(Name);
				_AttributeValue.Add(Value);
				return this;
			}
			public Node Add_Child(Node Node)
			{
				if (Node._Parent != null)
				{ throw new Exception("This Node has already been linked"); }
				_Nodes.Add(Node);
				Node._Parent = this;
				return this;
			}
			public List<Node> Children
			{ get { return _Nodes; } }
			public override string ToString()
			{
				if (_Type == "[CDATA[") { return "<[CDATA[" + _Text + "]]>"; }
				if (_Type == "") { return " " + _Text + " "; }
				string Text = "\n<" + _Type;
				for(int N = 0; N < _AttributeName.Count; N++)
				{
					Text += " " + _AttributeName[N] + "=\"" + _AttributeValue[N] + "\"";
				}
				if (_Nodes.Count == 0)
				{ return Text + "/>\n"; }
				Text += ">\n";
				for (int N = 0; N < _Nodes.Count; N++)
				{
					Text += _Nodes[N].ToString();
				}
				Text += "\n</" + _Type + ">\n";
				return Text;
			}
		}
		public XML()
		{ 
		}
		public Node Root
		{
			get { return _Root; }
		}
		Node _Root = null;
		public void Parse_File(string File)
		{
			Parse_File(File, "", "");
		}
		public void Parse_File(string File, string UserName, string Password)
		{
			_Root = null;
			if (File.StartsWith("http://") || File.StartsWith("HTTP://"))
			{
				System.Net.WebResponse Response;
				System.Net.WebRequest Request = System.Net.HttpWebRequest.Create(File);
				if(UserName != "")
					Request.Credentials = new System.Net.NetworkCredential(UserName, Password);
				Response = Request.GetResponse();              
				int Count = 0; byte[] Buffer = new byte[Response.ContentLength];
				while (Count < Response.ContentLength)
				{ Count += Response.GetResponseStream().Read(Buffer, Count, (int)Response.ContentLength - Count); }
				Parse(System.Text.Encoding.UTF8.GetString(Buffer));
				return;
			}
			if (File.StartsWith("ftp://") || File.StartsWith("FTP://"))
			{
				System.Net.WebResponse Response;
				System.Net.WebRequest Request = System.Net.FtpWebRequest.Create(File);
				if (UserName != "")
					Request.Credentials = new System.Net.NetworkCredential(UserName, Password);
				Response = Request.GetResponse();
				int Count = 0; byte[] Buffer = new byte[Response.ContentLength];
				while (Count < Response.ContentLength)
				{ Count += Response.GetResponseStream().Read(Buffer, Count, (int)Response.ContentLength); }
				Parse(System.Text.Encoding.UTF8.GetString(Buffer));
				return;
			}
			if (System.IO.File.Exists(File))
			{
				Parse(System.IO.File.ReadAllText(File));
			}
		}
		public void Parse(string Text)
		{
			_Root = null;
			bool Misc = false;
			bool DescribeNode = false;
			StringBuilder BuilderTextualNode = new StringBuilder();
			string TextualNode = "";
			bool TypeDef = false;
			bool DescribeTextual = false;
			string Type = "";
			bool AttribName = false;
			string AttribNameName = "";
			bool AttribValue = false;
			string AttribValueValue = "";
			bool Closure = false;
			bool NodeClosure = false;
			bool Bang = false;
			bool Special = false;
			char SpecialEsc = '"';
			bool Ignore = false;
			bool AttributeValue = false;
			bool Ask = false;
			bool CDATA = false;
			bool DOCTYPE = false;
			int Marker = 0;
			Stack<Node> Nodes = new Stack<Node>();
			Node Current = null;
			for(int n = 0; n < Text.Length; n++)
			{
				char c = Text[n];
				if (!DescribeNode)
				{
					if (DescribeTextual)
					{
						if (c == '<')
						{
							DescribeNode = true;
							if (Current != null)
							{
								Current.Add_Child((new Node()).Set_Text(BuilderTextualNode.ToString()));
								BuilderTextualNode = new StringBuilder();
								DescribeTextual = false;
							}
							continue;
						}
						if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
						{
							if (Current != null)
							{
								Current.Add_Child((new Node()).Set_Text(BuilderTextualNode.ToString()));
								BuilderTextualNode = new StringBuilder();
								DescribeTextual = false;
							}
							continue; 
						}
						BuilderTextualNode.Append(c);
					}
					else
					{
						if (c == '<')
						{ DescribeNode = true; continue; }
						if (c == ' ' || c == '\t' || c == '\r' || c == '\n') { continue; }
						DescribeTextual = true;
						BuilderTextualNode.Append(c);
					}
				}
				else
				{
					if (DOCTYPE)
					{
						if (Misc) { Misc = c != '"'; continue; }
						else 
						{
							if (c == '"') { Misc = true; continue; } 
							else if (c == '>') { TypeDef = false; Type = ""; DOCTYPE = false; DescribeNode = false; }
						}
						continue;
					}
					if (CDATA)
					{
						if (Type == "]]")
						{
							if (c == '>')
							{
								if (Current != null)
								{
									Current.Add_Child(new Node("[CDATA[").Set_Text(TextualNode.Substring(0, TextualNode.Length - 2)));
								}
								TextualNode = "";
								Type = "";
								CDATA = false;
								DescribeNode = false;
							}
							else
							{
								TextualNode += ">";
							}
						}
						else
						{
							if (Type == "") { if (c == ']') { Type = "]"; } }
							else if (Type == "]") { if (c == ']') { Type = "]]"; } else { Type = ""; } }
							TextualNode += c; 
						}
						continue;
					}
					if (Ask)
					{
						if (Type == "?")
						{
							if (c == '>')
							{
								Ask = false;
								Type = "";
								DescribeNode = false;
								continue;
							}
						}
						else
						{
							Type = c.ToString();
						}
						continue;
					}
					if (Ignore)
					{
						if (Type == "--")
						{
							if (c == '>')
							{ Ignore = false; DescribeNode = false; Type = ""; continue; }
							if (c != ' ' && c != '\t')
							{ Type = ""; }
						}
						if (c == '-')
							Type += '-';
						else
							Type = "";
						continue;
					}
					if (NodeClosure)
					{
						if (c == '>')
						{
							if (Nodes.Count == 0) { DescribeNode = false; Type = ""; Closure = false; continue; }
							Nodes.Pop();
							if (Nodes.Count == 0)
							{
								if (_Root == null)
									_Root = Current;
							}
							else
								Current = Nodes.Peek();
							DescribeNode = false;
							Type = ""; 
							NodeClosure = false;
							TextualNode = "";
						}
						continue;
					}
					if (Closure)
					{

						if (c == '>')
						{
							if (Nodes.Count != 0)
							{
								Nodes.Peek().Add_Child(Current);
								Current = Nodes.Peek();
							}
							else
							{
								if (_Root == null)
									_Root = Current;
							}
							DescribeNode = false; 
							Type = ""; 
							Closure = false; 
							TextualNode = "";
							TypeDef = false;
							continue; 
						}
						continue;
					}
					if (TypeDef)
					{

						if (AttributeValue)
						{
							if (Marker == -1) { Marker = n; }
							if (c == '"' || c == '\'')
							{
								if (Special) 
								{
									if (c != SpecialEsc)
									{
										continue;
									}
									AttribValueValue = Text.Substring(Marker, (n - Marker));
									Current.Add_Attribute(AttribNameName, AttribValueValue); AttributeValue = false; Special = false; AttribNameName = ""; AttribValueValue = ""; continue; 
								}
								else { Marker = n + 1; Special = true; SpecialEsc = c; continue; }
							}
							if (!Special && (c == ' ' || c == '\t' || c == '\r' || c == '\n'))
							{
								AttribValueValue = Text.Substring(Marker, (n - Marker));
								Current.Add_Attribute(AttribNameName, AttribValueValue); AttributeValue = false; Special = false; AttribNameName = ""; AttribValueValue = ""; continue; 
							}
							continue;
						}
						if (AttribName)
						{
							if (c == ' ' || c == '\t' || c == '\r' || c == '\n') { AttribName = false; Current.Add_Attribute(AttribNameName, ""); continue; }
							if (c == '=') { AttribName = false; AttributeValue = true; continue; }
							AttribNameName += c;
							continue;
						}
						if (c == ' ' || c == '\t' || c == '\r' || c == '\n') { continue; }
						if (c == '>')
						{
							TypeDef = false;
							DescribeNode = false; 
							Type = ""; 
							TextualNode = "";

							if (Nodes.Count == 0)
								_Root = Current;
							else    
								Nodes.Peek().Add_Child(Current);
							Nodes.Push(Current);
							continue;
						}
						if (c == '/')
						{
							Closure = true;
							continue;
						}
						AttribName = true;
						AttribNameName = c.ToString();
					}
					else
					{
						if (Type == "")
						{
							if (c == '!') 
							{ Bang = true; TypeDef = false; continue; }
							if (c == '/') 
							{ NodeClosure = true; TypeDef = false; continue; }
							if (c == '?')
							{ Ask = true; TypeDef = false; continue; }
						}
						if (Type == "--" & Bang)
						{ Ignore = true; Type = ""; continue; }
						if (Type == "[CDATA[" & Bang)
						{
							Type = "";
							TypeDef = false;
							CDATA = true;
							continue;
						}
						if (Type == "DOCTYPE" & Bang)
						{
							Type = "";
							TypeDef = false;
							DOCTYPE = true;
							Misc = false;
							continue;
						}
						if (c == ' ' || c == '\t' || c == '\r' || c == '\n') { Current = new Node(Type); Type = ""; TypeDef = true; continue; }
						if (c == '/') { Current = new Node(Type); Type = ""; TypeDef = false; Closure = true; continue; }
						if (c == '>') 
						{
							Current = new Node(Type);
							Type = "";
							TypeDef = false;
							DescribeNode = false;
							if (Nodes.Count == 0)
								_Root = Current;
							else
								Nodes.Peek().Add_Child(Current);
							Nodes.Push(Current);
							continue;
						}
						Type += c;
					}
				}
			}
			if (Current != null && _Root == null) { _Root = Current; }
		}
	}
}
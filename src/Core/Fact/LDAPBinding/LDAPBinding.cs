﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDAPBinding
{
    public class LDAPBinding : Fact.Network.Service
    {
        Fact.Network.Server _Server = null;
        public LDAPBinding(): base("LDAPBinding")
        {

        }

        public override void OnServiceLoaded(Fact.Network.Server Server)
        {
            _Server = Server;
        }

        Dictionary<string, Dictionary<string, Fact.Network.Protocols.LDAP>> _LDAPServers = new Dictionary<string, Dictionary<string, Fact.Network.Protocols.LDAP>>();

        bool _AddServerNotThreadsafe(string Server, string Username, string Password)
        {
            if (_LDAPServers.ContainsKey(Server) && _LDAPServers[Server].ContainsKey(Username))
            {
                return true;
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool Map(string Server, string OU, string Group, string Username, string Password)
        {
            lock (this)
            {
                if (_Server == null) { return false; }
                if (!_AddServerNotThreadsafe(Server, Username, Password)) { return false; }
                return true;
            }
            return false;
        }
    }
}

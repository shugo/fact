﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Context
{
    class LocalContext : Context
    {
        public override Context Parent
        {
            get
            {
                return base.Parent;
            }

            set
            {
                base.Parent = value;
            }
        }


        public LocalContext() { }
        System.IO.Stream _input; System.IO.Stream _output;
        public LocalContext(System.IO.Stream input, System.IO.Stream output)
        {
            _input = input;
            _output = output;

        }

        internal void SetupRemote()
        {
            Console.SetOut(new ContextStdOutStream(this));

            Fact.Threading.Job.CreateTimer(
                ((ref Fact.Threading.Job.Status status) =>
                {
                    checkMessage();
                }), 100, true);
            Fact.Threading.Job.CreateTimer(
                ((ref Fact.Threading.Job.Status status) =>
                {
                    if ((DateTime.Now - _LastPing).TotalMinutes > 1) { System.Environment.Exit(0); }
                }), 1000, true);
            checkMessage();
        }

        protected override object Execute(Delegate DelegateToExecute, int Timeout, params object[] Parameters)
        {
            return DelegateToExecute.Method.Invoke(DelegateToExecute.Target, Parameters);
        }

        public override Context CreateSubContext(string Name)
        {
            if (Internal.Information.IsMonoInUse())
            {
                return new RemoteProcessContext(Name, RemoteProcessContext.Link.TcpSocket);
            }
            else
            {
                return new RemoteProcessContext(Name, RemoteProcessContext.Link.AnonymousPipe);
            }
        }

        internal override void LogDebug(string Message)
        {
            if (_output != null)
            {
                sendCommand("log_debug", Message);
                checkMessage();
            }
            else
            {
                Fact.Log.DebugInternal(Message);
            }
        }

        internal override void LogError(string Message)
        {
            if (_output != null)
            {
                sendCommand("log_error", Message);
                checkMessage();
            }
            else
            {
                Fact.Log.ErrorInternal(Message);
            }
        }

        internal override void LogWarning(string Message)
        {
            if (_output != null)
            {
                sendCommand("log_warning", Message);
                checkMessage();
            }
            else
            {
                Fact.Log.WarningInternal(Message);
            }
        }

        internal override void LogException(Exception e)
        {
            if (_output != null)
            {
                sendCommand("log_exception", e);
                checkMessage();
            }
            else
            {
                Fact.Log.ExceptionInternal(e);
            }
        }

        class ContextStdOutStream : System.IO.TextWriter
        {
            LocalContext _context = null;
            public ContextStdOutStream(LocalContext contex)
            {
                _context = contex;
            }
            public override Encoding Encoding
            {
                get
                {
                    return System.Text.Encoding.UTF8;
                }
            }

            public override void Write(string value)
            {
                _context.sendCommand("write", value);
                _context.checkMessage();
            }

            public override void WriteLine()
            {
                _context.sendCommand("writeline");
                _context.checkMessage();
            }

            public override void Flush()
            {
                _context.sendCommand("flush");
                _context.checkMessage();
            }
        }

        Queue<Command> _CommandQueue = new Queue<Command>();

        class Command
        {
            internal string _Command = "";
            internal object _Parameters = null;
            internal bool _HasParameters = false;

            internal Command(string Command) { _Command = Command; _HasParameters = false; }
            internal Command(string Command, object Parameters) { _Command = Command; _Parameters = Parameters; _HasParameters = true; }
        }



        internal void sendCommand(string Command)
        {
            lock (this) { _CommandQueue.Enqueue(new LocalContext.Command(Command)); }
        }

        internal void sendCommand(string Command, object Parameters)
        {
            lock (this)
            {
                lock (this) { _CommandQueue.Enqueue(new LocalContext.Command(Command, Parameters)); }
            }
        }

        bool _pendingChecking = false;

        void LoadAssembly(byte[] Assembly)
        {
            try
            {
                RegisterAssemblyBytes(System.AppDomain.CurrentDomain.Load(Assembly), Assembly);
            }
            catch (Exception e)
            {
#if DEBUG
                Fact.Log.Exception(e);
#endif
            }
        }

        System.Reflection.MethodInfo FindDelegateFromRemoteExecutionMessage(Dictionary<string, object> ExecutionParameters)
        {
            string assembly_name = ExecutionParameters["assembly"] as string;
            System.Reflection.Assembly assembly = null;
            foreach (System.Reflection.Assembly candidate in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                if (candidate.FullName == assembly_name)
                {
                    assembly = candidate;
                }
            }

            if (assembly == null) { return null; }
            Type type = assembly.GetType(ExecutionParameters["declaring_type"] as string);

            System.Reflection.MethodInfo method = type.GetMethod((ExecutionParameters["method"] as string), System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            return method;
        }

        internal DateTime _LastPing = DateTime.Now;
        object CreateTargetFromRemoteExecutionMessage(Dictionary<string, object> ExecutionParameters)
        {
            string assembly_name = ExecutionParameters["target_assembly"] as string;
            System.Reflection.Assembly assembly = null;
            foreach (System.Reflection.Assembly candidate in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                if (candidate.FullName == assembly_name)
                {
                    assembly = candidate;
                }
            }

            if (assembly == null) { return null; }
            Type type = assembly.GetType(ExecutionParameters["target_type"] as string);

            System.Reflection.ConstructorInfo constructor = type.GetConstructor(new Type[] { });
            object target = constructor.Invoke(new object[] { });
            foreach (KeyValuePair<string, object> field in ExecutionParameters["target"] as Dictionary<string, object>)
            {
                target.GetType().GetField(field.Key, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(target, field.Value);
            }

            return target;
        }

        internal void checkMessage()
        {
            bool mustRequeue = false;
            string serverCommand = "";
            Command command = null;

            string tokenToRelease = "";
            System.Reflection.MethodInfo methodToInvoke = null;
            object target = null;
            object[] parameters = null;

            lock (this) { if (_pendingChecking) { return; } _pendingChecking = true; }
            try
            {
                lock (this) { if (_CommandQueue.Count > 0) { command = _CommandQueue.Dequeue(); } }
                if (command == null) { Internal.StreamTools.WriteUTF8String(_output, "ping"); }
                else
                {
                    Internal.StreamTools.WriteUTF8String(_output, command._Command);
                    if (command._HasParameters) { Internal.StreamTools.Serialize(_output, command._Parameters); }
                }
                _output.Flush();
                string message = Internal.StreamTools.ReadUTF8String(_input);
                switch (message)
                {
                    case "pong":
                    case "done": break;
                    default:
                        serverCommand = message;
                        mustRequeue = true;
                        break;
                }
                lock (this) { _LastPing = DateTime.Now; }
                if (serverCommand != "")
                {
                    switch (serverCommand)
                    {
                        case "load_assembly":
                            LoadAssembly(Internal.StreamTools.Deserialize(_input) as byte[]);
                            break;
                        case "reserve_va":
                            Dictionary<string, object> reserveVAParams = Internal.StreamTools.Deserialize(_input) as Dictionary<string, object>;
                            Reflection.Memory.MMU.ReserveVa(new IntPtr((Int64)(UInt64)reserveVAParams["address"]), (UInt64)reserveVAParams["size"]);
                            break;
                        case "execute":
                            Dictionary<string, object> executionMessage = Internal.StreamTools.Deserialize(_input) as Dictionary<string, object>;
                            methodToInvoke = FindDelegateFromRemoteExecutionMessage(executionMessage);
                            parameters = executionMessage["parameters"] as object[];
                            target = CreateTargetFromRemoteExecutionMessage(executionMessage);
                            tokenToRelease = executionMessage["done_callback"] as string;

                            break;

                    }
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Fact.Log.Exception(e);
                Fact.Log.Debug(e.StackTrace);
#endif
            }
            lock (this) { _pendingChecking = false; }



            if (!mustRequeue) { lock (this) { if (_CommandQueue.Count > 0) { mustRequeue = true; } } }
            if (mustRequeue) { Fact.Threading.Job.CreateJob(() => { checkMessage(); }); }

            try
            {
                object result = null;
                if (methodToInvoke != null) { result = methodToInvoke.Invoke(target, parameters); }
                if (tokenToRelease != "")
                {
                    Dictionary<string, object> messageResult = new Dictionary<string, object>();
                    messageResult.Add("token", tokenToRelease);
                    messageResult.Add("result", result);
                    sendCommand("done", messageResult);
                }
            }
            catch (Exception e)
            {
                if (e.InnerException != null) { e = e.InnerException; }
#if DEBUG
                Fact.Log.Exception(e);
                Fact.Log.Debug(e.StackTrace);
#endif
                Dictionary<string, object> exceptionParams = new Dictionary<string, object>();
                exceptionParams.Add("token", tokenToRelease);
                exceptionParams.Add("exception", e);

                sendCommand("exception", exceptionParams);
            }
        }
    }
}

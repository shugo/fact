﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fact.Threading;

namespace Fact.Context
{
    public class Slave
    {
        string _Directory = "";
        System.IO.Pipes.AnonymousPipeServerStream _ServerOut = null;
        System.IO.Pipes.AnonymousPipeServerStream _ServerIn = null;

        public Slave()
        {
            _ServerOut = new System.IO.Pipes.AnonymousPipeServerStream(System.IO.Pipes.PipeDirection.Out, System.IO.HandleInheritability.Inheritable);
            _ServerIn = new System.IO.Pipes.AnonymousPipeServerStream(System.IO.Pipes.PipeDirection.In, System.IO.HandleInheritability.Inheritable);
            _Directory = Tools.CreateTempDirectory();
            if (!System.IO.Directory.Exists(_Directory)) { Fact.Log.Error("Impossible to create a directory for the context slave"); }
        }

        public static void SlaveMain(string ServerIn, string ServerOut)
        {
            System.IO.Pipes.AnonymousPipeClientStream _ClientIn = null;
            System.IO.Pipes.AnonymousPipeClientStream _ClientOut = null;
            _ClientIn = new System.IO.Pipes.AnonymousPipeClientStream(System.IO.Pipes.PipeDirection.In, ServerOut);
            _ClientOut = new System.IO.Pipes.AnonymousPipeClientStream(System.IO.Pipes.PipeDirection.Out, ServerIn);
        }

        public void CreateProxy(string Name, object Object)
        {

        }

        public static object GetProxy(string Name)
        {
            return null;
        }

        public void CallMethod(byte[] Code)
        {

        }
    }
}

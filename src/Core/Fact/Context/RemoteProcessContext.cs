﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Context
{
    public static class @InternalContext
    {
        public static int SetupPipe(string[] args)
        {
            try
            {
                System.IO.Pipes.AnonymousPipeClientStream inClient = new System.IO.Pipes.AnonymousPipeClientStream(System.IO.Pipes.PipeDirection.In, args[0]);
                System.IO.Pipes.AnonymousPipeClientStream outClient = new System.IO.Pipes.AnonymousPipeClientStream(System.IO.Pipes.PipeDirection.Out, args[1]);

                LocalContext localContext = new LocalContext(inClient, outClient);
                Context.SetCurrentContext(localContext);
                localContext.SetupRemote();
#if DEBUG
                Fact.Log.Debug("Remote local context started using pipe: " + inClient + "/" + outClient);
#endif
                while (true) { System.Threading.Thread.Sleep(100); }
            }
            catch (Exception e)
            {
            }
            return 0;
        }

        public static int SetupTcp(string[] args)
        {
            try
            {
                string[] address = args[0].Split(':');
                System.Net.Sockets.TcpClient TcpClient = new System.Net.Sockets.TcpClient(address[0], int.Parse(address[1]));
                LocalContext localContext = new LocalContext(TcpClient.GetStream(), TcpClient.GetStream());
                Context.SetCurrentContext(localContext);
                localContext.SetupRemote();
#if DEBUG
                Fact.Log.Debug("Remote local context started using tcp: " + args[0].ToString());
#endif
                while (true) { System.Threading.Thread.Sleep(100); }
            }
            catch (Exception e)
            {
            }
            return 0;
        }
    }

    class RemoteProcessContext : Context
    {
        public enum Link
        {
            AnonymousPipe,
            TcpSocket
        }

        Link _Link;

        // Pipe Link
        System.IO.Pipes.AnonymousPipeServerStream _OutServer;
        System.IO.Pipes.AnonymousPipeServerStream _InServer;

        // Socket Link
        System.Net.Sockets.TcpListener _TcpListerner;
        System.Net.Sockets.TcpClient _TcpClient;

        System.IO.Stream _InStream;
        System.IO.Stream _OutStream;


        bool _Exit = false;
        string _TempDirectory = "";
        System.Diagnostics.Process _Process = new System.Diagnostics.Process();
        System.Threading.Thread _BackgroundThread = null;


        Command getNextCommand()
        {
            lock (this)
            {
                if (_CommandQueue.Count > 0) { return _CommandQueue.Dequeue(); }
                return null;
            }
        }

        string _BackgroundExtractHandle(string method)
        {
            string handle = "";
            for (int n = method.Length - 1; n > 0 && method[n] != '^'; n--)
            {
                handle = method[n] + handle;
            }
            return handle;
        }

        void _BackgroundDispatchProject(string method)
        {
            method = method.Substring("project.".Length);
            string handle = _BackgroundExtractHandle(method);
            method = method.Substring(0, method.Length - 1 - handle.Length);
            object parameters = Internal.StreamTools.Deserialize(_InServer) as object;
            if (_Projects.ContainsKey(handle))
            {
                Internal.StreamTools.Serialize(_Projects[handle].Do(method));
            }
            else
            {
                Dispose();
            }
        }

        delegate void Callback(object result);
        delegate void ExceptionCallback(Exception e);

        Dictionary<string, Callback> _PendingWork = new Dictionary<string, Callback>();
        Dictionary<string, ExceptionCallback> _PendingException = new Dictionary<string, ExceptionCallback>();

        void _BackgroundDispatch()
        {
            Callback callback = null;
            ExceptionCallback exceptionCallback = null;
            while (!_Destroyed && !_Exit)
            {
                string message = Internal.StreamTools.ReadUTF8String(_InStream);
                switch (message)
                {
                    case "ping": break;
                    case "write":
                        System.Console.Write(Internal.StreamTools.Deserialize(_InStream) as string);
                        break;
                    case "log_debug":
                        System.Console.WriteLine("[DEBUG]   " + Internal.StreamTools.Deserialize(_InStream) as string);
                        break;
                    case "log_error":
                        Fact.Log.Error(Internal.StreamTools.Deserialize(_InStream) as string);
                        break;
                    case "log_warning":
                        Fact.Log.Warning(Internal.StreamTools.Deserialize(_InStream) as string);
                        break;
                    case "log_info":
                        Fact.Log.Info(Internal.StreamTools.Deserialize(_InStream) as string);
                        break;
                    case "log_verbose":
                        Fact.Log.Verbose(Internal.StreamTools.Deserialize(_InStream) as string);
                        break;
                    case "log_exception":
                        Fact.Log.Exception(Internal.StreamTools.Deserialize(_InStream) as Exception);
                        break;
                    case "writeline":
                        System.Console.WriteLine();
                        break;
                    case "flush":
                        System.Console.Out.Flush();
                        break;
                    case "done":
                        Dictionary<string, object> result = Internal.StreamTools.Deserialize(_InStream) as Dictionary<string, object>;
                        lock (this)
                        {
                            callback = null;
                            if (_PendingWork.ContainsKey(result["token"] as string))
                            {
                                callback = _PendingWork[result["token"] as string];
                                _PendingWork.Remove(result["token"] as string);
                                _PendingException.Remove(result["token"] as string);
                            }
                        }

                        if (callback != null) { callback(result["result"]); }
                        break;
                    case "exception":
                        Dictionary<string, object> exceptionParams = Internal.StreamTools.Deserialize(_InStream) as Dictionary<string, object>;
                        string exkey = exceptionParams["token"] as string;
                        Exception exception = exceptionParams["exception"] as Exception;
                        lock (this)
                        {
                            exceptionCallback = null;
                            if (_PendingException.ContainsKey(exkey))
                            {
                                exceptionCallback = _PendingException[exkey];
                                _PendingWork.Remove(exkey);
                                _PendingException.Remove(exkey);
                            }
                        }
                        if (exceptionCallback != null) { exceptionCallback(exception); }
                        break;

                    default:
                        if (message.StartsWith("project.")) { _BackgroundDispatchProject(message); }
                        else { Dispose(); }
                        break;
                }
                Command command = getNextCommand();
                if (command != null)
                {
#if DEBUG
                    Fact.Log.Debug("New message to the context: " + command._Command);
#endif
                    Internal.StreamTools.WriteUTF8String(_OutStream, command._Command);
                    if (command._HasParameters)
                    {
                        Internal.StreamTools.Serialize(_OutStream, command._Parameters);
                    }
                }
                else
                {
                    Internal.StreamTools.WriteUTF8String(_OutStream, "done");
                }
            }
        }

        void _BackgroundTcpServer()
        {
            try
            {
                while (!_Destroyed && !_Exit)
                {
                    if (_TcpListerner.Pending())
                    {
                        _TcpClient = _TcpListerner.AcceptTcpClient();
                        _InStream = _TcpClient.GetStream();
                        _OutStream = _TcpClient.GetStream();
                        break;
                    }
                }
                _BackgroundDispatch();
            }
            catch (Exception e)
            {
#if DEBUG
                Fact.Log.Exception(e);
#endif
            }
            Dispose();
        }

        void _BackgroundPipeServer()
        {
            _InStream = _InServer;
            _OutStream = _OutServer;

            try
            {
                _BackgroundDispatch();
            }
            catch (Exception e)
            {
#if DEBUG
                Fact.Log.Exception(e);
#endif
            }
            Dispose();
        }

        Queue<Command> _CommandQueue = new Queue<Command>();

        class Command
        {
            internal string _Command = "";
            internal object _Parameters = null;
            internal bool _HasParameters = false;

            internal Command(string Command) { _Command = Command; _HasParameters = false; }
            internal Command(string Command, object Parameters) { _Command = Command; _Parameters = Parameters; _HasParameters = true; }
        }



        internal void sendCommand(string Command)
        {
            lock (this) { _CommandQueue.Enqueue(new RemoteProcessContext.Command(Command)); }
        }

        internal void sendCommand(string Command, object Parameters)
        {
            lock (this)
            {
                lock (this) { _CommandQueue.Enqueue(new RemoteProcessContext.Command(Command, Parameters)); }
            }
        }

        void LoadAssembly(System.Reflection.Assembly Assembly)
        {
            if (Assembly == null) { return; }
            byte[] data = Context.Current.GetAssemblyBytes(Assembly);
            if (data != null)
            {
                sendCommand("load_assembly", data);
                return;
            }
            if (Assembly.Location != null && Assembly.Location != "" && System.IO.File.Exists(Assembly.Location))
            {
                byte[] array = System.IO.File.ReadAllBytes(Assembly.Location);
                sendCommand("load_assembly", array);
                return;
            }

        }

        Dictionary<string, Processing.RemoteContextProject_ServerSide> _Projects = new Dictionary<string, Processing.RemoteContextProject_ServerSide>();


        void _SerializeObjectFactAssembliesRec(object Object, Dictionary<string, object> Assemblies, HashSet<int> VisitedObject)
        {
            if (Object == null) { return; }
            if (VisitedObject.Contains(Object.GetHashCode())) { return; }
            VisitedObject.Add(Object.GetHashCode());
            foreach (System.Reflection.FieldInfo member in Object.GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance))
            {
                object targetObject = member.GetValue(Object);
                if (targetObject is Fact.Reflection.Assembly)
                {
                    if (!Assemblies.ContainsKey((targetObject as Fact.Reflection.Assembly)._UUID))
                    {
                        Assemblies.Add((targetObject as Fact.Reflection.Assembly)._UUID, (targetObject as Fact.Reflection.Assembly));
                    }
                }
                else if (targetObject is Fact.Reflection.Method)
                {
                    if (!Assemblies.ContainsKey((targetObject as Fact.Reflection.Method).ParentAssembly._UUID))
                    {
                        Assemblies.Add((targetObject as Fact.Reflection.Method).ParentAssembly._UUID, (targetObject as Fact.Reflection.Method).ParentAssembly);
                    }
                }
                else
                {
                    try
                    {
                        _SerializeObjectFactAssembliesRec(targetObject, Assemblies, VisitedObject);
                    }
                    catch { }
                }
            }
        }

        Dictionary<string, object> SerializeObjectFactAssemblies(object Object)
        {
            Dictionary<string, object> assemblies = new Dictionary<string, object>();
            _SerializeObjectFactAssembliesRec(Object, assemblies, new HashSet<int>());
            return assemblies;
        }

        Dictionary<string, object> SerializeDelegateTarget(Delegate DelegateToExecute)
        {
#if DEBUG
            Fact.Log.Debug("Serialize delegate target");
#endif
            Dictionary<string, Fact.Reflection.Assembly> assemblies = new Dictionary<string, Fact.Reflection.Assembly>();
            Dictionary<string, object> delegateTarget = new Dictionary<string, object>();
            foreach (System.Reflection.FieldInfo member in DelegateToExecute.Target.GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance))
            {
                object targetObject = member.GetValue(DelegateToExecute.Target);
                if (targetObject is Fact.Processing.File) { }
                if (targetObject is Fact.Reflection.Namespace) { }
                if (targetObject is Fact.Reflection.Type) { }

#if DEBUG
                if (targetObject != null)
                {
                    Fact.Log.Debug("Serialize object " + member.Name + " of type " + targetObject.GetType());
                }
#endif
                delegateTarget.Add(member.Name, targetObject);
            }
            return delegateTarget;
        }

        class PendingExecution
        {
            internal volatile bool _Done = false;
            internal volatile Exception _Exception = null;
            internal volatile object _Result = null;

        }

        protected override object Execute(Delegate DelegateToExecute, int Timeout, params object[] Parameters)
        {

            LoadAssembly(DelegateToExecute.Method.DeclaringType.Assembly);

            Dictionary<string, object> data = new Dictionary<string, object>();
            
            data.Add("parameters", Parameters);
            data.Add("assembly", DelegateToExecute.Method.DeclaringType.Assembly.FullName);
            data.Add("declaring_type", DelegateToExecute.Method.DeclaringType.FullName);
            data.Add("method", DelegateToExecute.Method.Name);
            data.Add("target_assembly", DelegateToExecute.Target.GetType().Assembly.FullName);
            data.Add("target_type", DelegateToExecute.Target.GetType().FullName);
            data.Add("target", SerializeDelegateTarget(DelegateToExecute));
            data.Add("fact_assemblies", SerializeObjectFactAssemblies(DelegateToExecute.Target));
            PendingExecution pending = new PendingExecution();
            lock (this)
            {
                string done_uuid = Internal.Information.GenerateUUID();
                string exception_uuid = Internal.Information.GenerateUUID();

                data.Add("done_callback", done_uuid);

                _PendingWork.Add(done_uuid, (object result) => { pending._Result = result; pending._Done = true;  });
                _PendingException.Add(done_uuid, (Exception e) => { pending._Exception = e; pending._Done = true; });
            }

            sendCommand("execute", data);
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            while (!pending._Done)
            {
                if (Threading.Job.CurrentJob != null) { Threading.Job.Yield(); }
                else { System.Threading.Thread.Sleep(0); }

                if (Timeout > 0)
                {
                    if (stopwatch.ElapsedMilliseconds > Timeout)
                    {
                        try { Destroy(); } catch { }
                        throw new TimeoutException("The context took too much time to execute the specified method");
                    }
                }

                if (_Exit)
                {
                    if (pending._Exception != null) { throw pending._Exception; }
                    throw new ContextDestroyedException("The context exited while executing the function");
                }
            }
            if (pending._Exception != null) { throw pending._Exception; }
            return pending._Result;
        }

        public override Context CreateSubContext(string Name)
        {
            throw new Exception("Permission denied");
        }

        public RemoteProcessContext(string Name, Link Link)
        {
            _Link = Link;
            _TempDirectory = Tools.CreateTempDirectory();
            switch (Link)
            {
                case Link.AnonymousPipe:
                    _OutServer = new System.IO.Pipes.AnonymousPipeServerStream(System.IO.Pipes.PipeDirection.Out, System.IO.HandleInheritability.Inheritable);
                    _InServer = new System.IO.Pipes.AnonymousPipeServerStream(System.IO.Pipes.PipeDirection.In, System.IO.HandleInheritability.Inheritable);
                    break;
                case Link.TcpSocket:
                    _TcpListerner = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Loopback, 0);
                    _TcpListerner.Start();
                    break;
            }

            if (!System.IO.Directory.Exists(_TempDirectory)) { throw new OutOfMemoryException("No space available to create the context on the file system"); }
            System.Reflection.AssemblyName name = new System.Reflection.AssemblyName("fact.context." + Name + "");

            System.Reflection.Emit.AssemblyBuilder assembly = System.AppDomain.CurrentDomain.DefineDynamicAssembly(name, System.Reflection.Emit.AssemblyBuilderAccess.Save, _TempDirectory);
            System.Reflection.Emit.ModuleBuilder contextModule = assembly.DefineDynamicModule("fact.context." + Name + ".exe");
            System.Reflection.Emit.TypeBuilder contextType = contextModule.DefineType("___fact___internal___context___T", System.Reflection.TypeAttributes.Class | System.Reflection.TypeAttributes.Public | System.Reflection.TypeAttributes.Sealed);

            System.Reflection.Emit.MethodBuilder entryPoint = contextType.DefineMethod("main", System.Reflection.MethodAttributes.Public | System.Reflection.MethodAttributes.Static | System.Reflection.MethodAttributes.HideBySig, typeof(int), new Type[] { typeof(string[]) });
            System.Reflection.Emit.ILGenerator entryPointCode = entryPoint.GetILGenerator();
            entryPointCode.Emit(System.Reflection.Emit.OpCodes.Ldarg_0);
            switch (Link)
            {
                case Link.AnonymousPipe: entryPointCode.Emit(System.Reflection.Emit.OpCodes.Call, typeof(@InternalContext).GetMethod("SetupPipe")); break;
                case Link.TcpSocket: entryPointCode.Emit(System.Reflection.Emit.OpCodes.Call, typeof(@InternalContext).GetMethod("SetupTcp")); break;
            }

            entryPointCode.Emit(System.Reflection.Emit.OpCodes.Ret);
            contextType.CreateType();
            assembly.SetEntryPoint(entryPoint, System.Reflection.Emit.PEFileKinds.ConsoleApplication);

            assembly.Save("fact.context." + Name + ".exe");
            string libfact = Internal.Information.GetLibFact();
            Tools.Copy(libfact, _TempDirectory + "/Fact.dll");
            _Process.StartInfo.WorkingDirectory = System.IO.Path.GetFullPath(_TempDirectory);
            _Process.StartInfo.UseShellExecute = false;
            _Process.StartInfo.FileName = System.IO.Path.GetFullPath(_TempDirectory + "/fact.context." + Name + ".exe");
            switch (Link)
            {
                case Link.AnonymousPipe: _Process.StartInfo.Arguments = _OutServer.GetClientHandleAsString() + " " + _InServer.GetClientHandleAsString(); break;
                case Link.TcpSocket: _Process.StartInfo.Arguments = _TcpListerner.LocalEndpoint.ToString(); break;
            }

            _Process.Start();
#if DEBUG
            Fact.Log.Debug("Remote context " + Name + " created pid:" + _Process.Id.ToString());
#endif

            Threading.Job.CreateTimer((ref Threading.Job.Status status) => { if (_Process.HasExited) { status.Expired = true; Dispose(); } }, 100, true);

            switch (Link)
            {
                case Link.AnonymousPipe: _BackgroundThread = new System.Threading.Thread(_BackgroundPipeServer); break;
                case Link.TcpSocket: _BackgroundThread = new System.Threading.Thread(_BackgroundTcpServer); break;
            }


            _BackgroundThread.Start();
        }

        bool _Destroyed = false;

        protected override void Destroy()
        {
            if (!_Destroyed)
            {
#if DEBUG
                Fact.Log.Debug("Destroying remote context ...");
#endif
                Tools.RecursiveDelete(_TempDirectory);
                try { _Process.Kill(); } catch { }
                try { _Process.Close(); } catch { }
                switch (_Link)
                {
                    case Link.AnonymousPipe:
                        try { _OutServer.Dispose(); } catch { }
                        try { _InServer.Dispose(); } catch { }
                        break;
                    case Link.TcpSocket:
                        try { _TcpClient.Close(); } catch { }
                        try { _TcpListerner.Stop(); } catch { }
                        break;
                }

                _Destroyed = true;
                _Exit = true;
                try { _BackgroundThread.Abort(); } catch { }
            }
        }

        public void Dispose()
        {
            Destroy();
        }

        ~RemoteProcessContext()
        {
            Destroy();
        }
    }
}

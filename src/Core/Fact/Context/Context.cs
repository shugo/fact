﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Context
{
    public class Context : IDisposable
    {
        public class ContextDestroyedException : Exception
        {
            public ContextDestroyedException(string Message) : base(Message) { }
            public ContextDestroyedException() : base() { }
        }

        static Context _Current = null;
        internal static void SetCurrentContext(Context Context)
        {
            _Current = Context;
        }
        static Context()
        {
            _Current = new LocalContext();
        }

        public virtual Context Parent { get; set; }
        static public Context Current
        {
            get
            {
                return _Current;
            }
        }

        internal virtual void LogDebug(string Message) { Fact.Log.Debug(Message); }
        internal virtual void LogError(string Message) { Fact.Log.Error(Message); }
        internal virtual void LogWarning(string Message) { Fact.Log.Warning(Message); }
        internal virtual void LogException(Exception e) { Fact.Log.Exception(e); }




        Dictionary<string, byte[]> _Assemblies = new Dictionary<string, byte[]>();
        internal byte[] GetAssemblyBytes(System.Reflection.Assembly Assembly)
        {

            lock (_Assemblies)
            {
                if (_Assemblies.ContainsKey(Assembly.FullName)) { return _Assemblies[Assembly.FullName]; }
            }

            return null;
        }

        internal void RegisterAssemblyBytes(System.Reflection.Assembly Assembly, byte[] Bytes)
        {
            lock (_Assemblies)
            {
                if (!_Assemblies.ContainsKey(Assembly.FullName)) { _Assemblies.Add(Assembly.FullName, Bytes); }
            }
        }

        public virtual Context CreateSubContext(string Name)
        {
            return null;
        }
        public Context CreateSubContext()
        {
            return CreateSubContext("anonymous");
        }

        public delegate void Action();

        public void Execute(Action Action)
        {
            Execute(Action, -1);
        }

        public void Execute(Action Action, int Timeout)
        {
            Execute(Action as Delegate, Timeout, new object[] { });
        }

        class InternalReference<T> { internal T _; }

        public T Execute<T>(Func<T> Function)
        {
            return Execute(Function, -1);
        }

        public T Execute<T>(Func<T> Function, int Timeout)
        {
            return (T)Execute(Function, Timeout, new object[] { });
        }

        protected virtual object Execute(Delegate DelegateToExecute, int Timeout, params object[] Parameters)
        {
            throw new Exception("Execution not supported on this context");
        }

        protected virtual void Destroy()
        {

        }

        public void Dispose()
        {
            Destroy();
        }

        ~Context()
        {
            Destroy();
        }
    }
}

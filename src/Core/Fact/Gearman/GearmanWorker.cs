﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gearman
{
    class GearmanWorker
    {

        public class GearmanClient
        {
            public delegate void Function(string args, string handle, GearmanClient client);

            System.Net.Sockets.TcpClient _client;

            System.Threading.Thread _thread;

            Dictionary<string, Function> _Functions = new Dictionary<string, Function>();

            public GearmanClient(string address, int port)
            {
                try
                {
                    _client = new System.Net.Sockets.TcpClient(address, port);
                    System.Threading.Thread.Sleep(100);
                    int timeout = 10000;
                    while (!_client.Connected && timeout > 0)
                    {
                        System.Threading.Thread.Sleep(100);
                        timeout -= 100;
                    }
                    if (!_client.Connected)
                    {
                        Fact.Log.Error(" [GEARMAN] Impossible to connect to " + address);
                    }
                    _thread = new System.Threading.Thread(GearmanMainLoop);
                    _thread.IsBackground = true;
                    _thread.Start();
                }
                catch
                {
                }
            }

            void GearmanMainLoop()
            {
                bool init = false;
                bool sleep = false;
                try
                {
                    Fact.Log.Verbose(" [GEARMAN] Registering as fact task manager ...");
                    SendRequest(22, "FactTaskManager");
                    int waited = 0;
                    while (true)
                    {
                        while (_client.Available >= 4 * 3)
                        {
                            byte[] buffer = new byte[4 * 3];

                            _client.GetStream().Read(buffer, 0, buffer.Length);
                            int length = buffer[11] + buffer[10] * 256 + buffer[9] * 256 * 256 + buffer[8] * 256 * 256 * 256;
                            //00 52 45 53 -> \0REQ  
                            if (buffer[0] == 0x00 && buffer[1] == 0x52 && buffer[2] == 0x45 && buffer[3] == 0x53)
                            {
                                if (buffer[4] == 0x00 && buffer[5] == 0x00 && buffer[6] == 0x00 && (buffer[7] == 0x0a ||
                                                                                                    buffer[7] == 0x04))
                                {
                                    if (!init)
                                    {
                                        Fact.Log.Verbose(" [GEARMAN] Gearman job server is responding");
                                        init = true;
                                    }
                                    // Do Nothing
                                }
                                else if (buffer[4] == 0x00 && buffer[5] == 0x00 && buffer[6] == 0x00 && buffer[7] == 0x0b)
                                {
                                    try
                                    {
                                        Fact.Log.Verbose(" [GEARMAN] New gearman job request ...");
                                        byte[] req = new byte[length];
                                        int offset = 0;
                                        int templength = length;
                                        while (templength > 0)
                                        {
                                            if (_client.Available > 0)
                                            {
                                                int count = _client.GetStream().Read(req, offset, templength);
                                                if (count > 0)
                                                {
                                                    offset += count;
                                                    templength -= count;
                                                }
                                            }
                                            else
                                            {
                                                System.Threading.Thread.Sleep(10);
                                            }
                                        }
                                        //_client.GetStream().Read(req, 0, length);
                                        List<byte> id = new List<byte>();
                                        List<byte> name = new List<byte>();
                                        List<byte> args = new List<byte>();
                                        int n = 0;
                                        for (n = 0; req[n] != 0; n++) { id.Add(req[n]); } n++;
                                        for (; n < length && req[n] != 0; n++) { name.Add(req[n]); } n++;
                                        for (; n < length && req[n] != 0; n++) { args.Add(req[n]); }
                                        string sid = System.Text.Encoding.UTF8.GetString(id.ToArray());
                                        string sname = System.Text.Encoding.UTF8.GetString(name.ToArray());
                                        string sargs = System.Text.Encoding.UTF8.GetString(args.ToArray());
                                        lock (_Functions)
                                        {
                                            if (!_Functions.ContainsKey(sname))
                                            {
                                                Fact.Log.Error(" [GEARMAN] Invalid request " + sname);
                                                SendRequest(13, sid, "[ERROR] Invalid request " + sname);
                                            }
                                            else
                                            {
                                                Fact.Log.Verbose(" [GEARMAN] Call " + sname + "(" + sargs + ")");
                                                _Functions[sname](sargs, sid, this);
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Fact.Log.Error(" [GEARMAN] Runtime error");
                                        Fact.Log.Exception(e);
                                    }
                                }
                                else
                                {
                                    Fact.Log.Warning(" [GEARMAN] Instruction not supported " + buffer[7].ToString());
                                }
                            }
                            else
                            {
                                Fact.Log.Warning(" [GEARMAN] Invalid message");
                                Fact.Log.Error("[0]:" + buffer[0] + " [1]:" + buffer[1] + " [2]:" + buffer[2] + " [3]:" + buffer[3]);
                                Fact.Log.Error("[4]:" + buffer[4] + " [5]:" + buffer[5] + " [6]:" + buffer[6] + " [7]:" + buffer[7]);
                                Fact.Log.Error("[8]:" + buffer[8] + " [9]:" + buffer[9] + " [10]:" + buffer[10] + " [11]:" + buffer[11]);

                                int Timeout = 1000;
                                if (length >= 1024)
                                {
                                    length = 1024;
                                }
                                byte[] req = new byte[length];
                                while (_client.Available < length)
                                {
                                    System.Threading.Thread.Sleep(10);
                                    Timeout -= 10;
                                    if (Timeout > 0)
                                        break;
                                }
                                _client.GetStream().Read(req, 0, length);

                            }
                        }
                        System.Threading.Thread.Sleep(100);
                        waited += 100;
                        if (waited > 1000)
                        {
                            SendRequest(9 /* GRAB_JOB */, "");
                            waited = 0;
                        }
                    }
                }
                catch
                {
                }
            }

            public void SendRequest(byte Request, string Data)
            {
                List<byte> data = new List<byte>();
                data.Add(0x00); data.Add(0x52); data.Add(0x45); data.Add(0x51);
                data.Add(0x00); data.Add(0x00); data.Add(0x00); data.Add(Request);

                byte[] text = System.Text.Encoding.UTF8.GetBytes(Data);
                WInt(text.Length, data);
                for (int n = 0; n < text.Length; n++)
                    data.Add(text[n]);
                _client.GetStream().Write(data.ToArray(), 0, data.Count);

            }

            public void SendRequest(byte Request, string Data1, string Data2)
            {
                List<byte> data = new List<byte>();
                data.Add(0x00); data.Add(0x52); data.Add(0x45); data.Add(0x51);
                data.Add(0x00); data.Add(0x00); data.Add(0x00); data.Add(Request);

                byte[] text1 = System.Text.Encoding.UTF8.GetBytes(Data1);
                byte[] text2 = System.Text.Encoding.UTF8.GetBytes(Data2);

                WInt(text1.Length + text2.Length, data);
                for (int n = 0; n < text1.Length; n++)
                    data.Add(text1[n]);
                data.Add(0);
                for (int n = 0; n < text2.Length; n++)
                    data.Add(text2[n]);
                _client.GetStream().Write(data.ToArray(), 0, data.Count);

            }

            public void SendRequest(byte Request, string Data1, string Data2, string Data3)
            {
                List<byte> data = new List<byte>();
                data.Add(0x00); data.Add(0x52); data.Add(0x45); data.Add(0x51);
                data.Add(0x00); data.Add(0x00); data.Add(0x00); data.Add(Request);

                byte[] text1 = System.Text.Encoding.UTF8.GetBytes(Data1);
                byte[] text2 = System.Text.Encoding.UTF8.GetBytes(Data2);
                byte[] text3 = System.Text.Encoding.UTF8.GetBytes(Data3);

                WInt(text1.Length + text2.Length + text3.Length, data);
                for (int n = 0; n < text1.Length; n++)
                    data.Add(text1[n]);
                data.Add(0);
                for (int n = 0; n < text2.Length; n++)
                    data.Add(text2[n]);
                data.Add(0);
                for (int n = 0; n < text3.Length; n++)
                    data.Add(text3[n]);
                _client.GetStream().Write(data.ToArray(), 0, data.Count);

            }

            public void RegisterFunction(string Name, Function funct)
            {
                SendRequest(1/* REGISTER */, Name);
                lock (_Functions)
                {
                    _Functions.Add(Name, funct);
                }
            }

            public void WInt(int value, List<byte> data)
            {
                int v1 = value % 256;
                int v2 = (value / 256) % 256;
                int v3 = (value / (256 * 256)) % 256;
                int v4 = (value / (256 * 256 * 256)) % 256;
                data.Add((byte)v4);
                data.Add((byte)v3);
                data.Add((byte)v2);
                data.Add((byte)v1);
            }

            public int RInt()
            {
                int v1 = _client.GetStream().ReadByte();
                int v2 = _client.GetStream().ReadByte();
                int v3 = _client.GetStream().ReadByte();
                int v4 = _client.GetStream().ReadByte();

                return v1 + v2 * 256 + v3 * 256 * 256 + v4 * 256 * 256 * 256;
            }
        }

        int _port = 0;
        List<GearmanClient> _clients = new List<GearmanClient>();
        Fact.Directory.User _Owner = null;
        public Fact.Directory.User Owner { get { return _Owner; } }

        public GearmanWorker(int port, Fact.Directory.User Owner)
        {
            _port = port;
            _Owner = Owner;
        }

        public void Connect(string JobServer)
        {
            _clients.Add(new GearmanClient(JobServer, _port));
        }

        public void RegisterFunction(string Name, GearmanClient.Function funct)
        {
            foreach (GearmanClient client in _clients)
            {
                client.RegisterFunction(Name, funct);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gearman
{
    public class Gearman : Fact.Network.Service
    {
        Dictionary<string, GearmanWorker> _Workers = new Dictionary<string, GearmanWorker>();
        public Gearman()
            : base("Gearman")
        { }

        void RegisterServiceFunction(Fact.Network.Server Server, GearmanWorker Worker, string Action)
        {
            Worker.RegisterFunction(Action,
                (string args, string handle, GearmanWorker.GearmanClient Client) =>
                {
                    string str_result = "";
                    try
                    {
                        object result = Server.InvokeAction(Worker.Owner, Action, args);
                        if (result == null) { str_result = ""; }
                        else { str_result = result.ToString(); }
                    }
                    catch { str_result = ""; }
                });
        }

        [Fact.Network.ServiceExposedMethod]
        public void Connect(Fact.Network.Service.RequestInfo Info, string server) 
        {
            string servername = "";
            int port = 0;
            lock (_Workers)
            {
                GearmanWorker _Worker = new GearmanWorker(port, Info.User);
                _Worker.Connect(servername);
            }
        }
    }
}

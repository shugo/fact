﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Test.Result
{
    public class Omitted : Result
    {
        public Omitted(string Text)
        {
            base.Text = Text;
        }

        public Omitted(string Text, string Info)
        {
            base.Text = Text;
            base.Info = Info;
        }


        public override string ToString()
        {
            return "<omitted text=\"" + base.Text + "\" info=\"" + base.Info + "\"/>";
            return base.ToString();
        }

        protected override Result LightCopy()
        {
            return new Omitted(Text, Info);
        }

        protected override void SaveSpecific(System.IO.Stream stream)
        {
#if DEBUG
            Fact.Log.Debug("Save test '" + Text + "' OMITTED");
#endif
            Internal.StreamTools.WriteUInt32(stream, 0x06);
        }
    }
}

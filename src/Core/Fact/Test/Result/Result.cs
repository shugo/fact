﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Test.Result
{
    public abstract class Result
    {
        public enum VisibilityLevel
        {
            Public = 0,
            Staging = 1,
            Internal = 2,
        }

        VisibilityLevel _Visibility = VisibilityLevel.Public;
        public VisibilityLevel Visibility { get { return _Visibility; } set { _Visibility = value; } }

        internal Fact.Test.Result.Group _Parent = null;
        public Fact.Test.Result.Group Parent
        {
            get { return _Parent; }
        }

        public Fact.Test.Result.Group RootParent
        {
            get
            {
                if (Parent == null) { return null; }
                if (Parent.Parent == null) { return Parent; }
                return Parent.RootParent;
            }
        }

        internal Fact.Test.Result.Result TopOfTree()
        {
            if (RootParent == null) { return this; }
            return RootParent;
        }

        string _text = "";
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        string _info = "";
        public string Info
        {
            get { return _info; }
            set { _info = value; }
        }

        Dictionary<string, string> _metadata = new Dictionary<string, string>();

        public Dictionary<string, string> Metadata
        {
            get { return _metadata; }
            set { _metadata = value; }
        }

        int _FromLine = -1;
        public int FromLine
        {
            get { return _FromLine; }
            set { _FromLine = value; }
        }

        int _FromColumn = -1;
        public int FromColumn
        {
            get { return _FromColumn; }
            set { _FromColumn = value; }
        }

        int _ToLine = -1;
        public int ToLine
        {
            get { return _ToLine; }
            set { _ToLine = value; }
        }

        int _ToColumn = -1;
        public int ToColumn
        {
            get { return _ToColumn; }
            set { _ToColumn = value; }
        }

        Processing.File _File = null;

        public Processing.File File
        {
            get { return _File; }
            set { _File = value; }
        }

        int _Score = 0;

        public virtual int Score
        {
            get { return _Score; }
            set { _Score = value; }
        }

        long _EnlapsedUserTime = 0;
        public virtual long EnlapsedUserTime
        {
            get { return _EnlapsedUserTime; }
            set { _EnlapsedUserTime = value; }
        }

        long _EnlapsedKernelTime = 0;
        public virtual long EnlapsedKernelTime
        {
            get { return _EnlapsedKernelTime; }
            set { _EnlapsedKernelTime = value; }
        }

        /// <summary>
        /// The ponderation is used to manage the intermitency.
        /// We need this extra variable to count how many time the tes has been rewriten
        /// </summary>
        internal int _Ponderation = 0;

        public long EnlapsedTime
        {
            get { return EnlapsedKernelTime + EnlapsedUserTime; }
        }
        protected virtual void SaveSpecific(System.IO.Stream stream)
        {
            throw new Exception("Try to save an unknow result type");
        }

        public void Save(System.IO.Stream stream)
        {
            Internal.StreamTools.WriteUTF8String(stream, Text);
            Internal.StreamTools.WriteUTF8String(stream, Info);
            Internal.StreamTools.WriteInt32(stream, _metadata.Keys.Count);
            foreach (string key in _metadata.Keys)
            {
                Internal.StreamTools.WriteUTF8String(stream, key);
                Internal.StreamTools.WriteUTF8String(stream, _metadata[key]);
            }
            Internal.StreamTools.WriteInt32(stream, _FromLine);
            Internal.StreamTools.WriteInt32(stream, _FromColumn);
            Internal.StreamTools.WriteInt32(stream, _ToLine);
            Internal.StreamTools.WriteInt32(stream, _ToColumn);
            Internal.StreamTools.WriteInt32(stream, _Score);
            Internal.StreamTools.WriteInt64(stream, _EnlapsedUserTime);
            Internal.StreamTools.WriteInt64(stream, _EnlapsedKernelTime);
            Internal.StreamTools.WriteInt32(stream, _Ponderation);
            Internal.StreamTools.WriteInt32(stream, (int)_Visibility);

            SaveSpecific(stream);
        }

        protected virtual Fact.Test.Result.Result LightCopy()
        {
            return null;
        }

        /// <summary>
        /// Create a copy of the current test result with all its properties. The duplicated tests results
        /// is not attached to a test tree and can be attached to a new test tree.
        /// </summary>
        /// <returns>The duplicated test result</returns>
        public virtual Fact.Test.Result.Result Copy()
        {
            Fact.Test.Result.Result result = LightCopy();
            if (result != null)
            {
                try { result.EnlapsedKernelTime = EnlapsedKernelTime; } catch { }
                try { result.EnlapsedUserTime = EnlapsedUserTime; } catch { }
                try { result.File = File; } catch { }
                result.FromColumn = FromColumn;
                result.ToColumn = ToColumn;
                result.FromLine = FromLine;
                result.ToLine = ToLine;
                result.Visibility = Visibility;
                result.Metadata = new Dictionary<string, string>(Metadata);
                try { result.Score = Score; } catch { }
                result.Text = Text;
                result.Info = Info;
            }
            return result;
        }

        public static Result Cast(Object obj)
        {
            // Implicit cast from obj while handling the implicit cast from Fact.Runtime.Diff.Result
            if (obj is Fact.Runtime.Diff.Result)
                return (Fact.Runtime.Diff.Result) obj;
            return (Fact.Test.Result.Result) obj;
        }

        public static Result Load(System.IO.Stream stream)
        {
            string Text = Internal.StreamTools.ReadUTF8String(stream);
            string Info = Internal.StreamTools.ReadUTF8String(stream);
            int metadatacount = Internal.StreamTools.ReadInt32(stream);
            Dictionary<string, string> metadata = new Dictionary<string, string>();
            while (metadatacount > 0)
            {
                string key = Internal.StreamTools.ReadUTF8String(stream);
                string value = Internal.StreamTools.ReadUTF8String(stream);
                metadata.Add(key, value);
                metadatacount--;
            }
            int FromLine = Internal.StreamTools.ReadInt32(stream);
            int FromColumn = Internal.StreamTools.ReadInt32(stream);
            int ToLine = Internal.StreamTools.ReadInt32(stream);
            int ToColumn = Internal.StreamTools.ReadInt32(stream);
            int Score = Internal.StreamTools.ReadInt32(stream);
            long UserTime = Internal.StreamTools.ReadInt64(stream);
            long KernelTime = Internal.StreamTools.ReadInt64(stream);
            int Ponderation = Internal.StreamTools.ReadInt32(stream);
            VisibilityLevel visibility = (VisibilityLevel)Internal.StreamTools.ReadInt32(stream);
			uint Type = Internal.StreamTools.ReadUInt32(stream);
            Result result;

            switch (Type)
            {
                case 0x00:
                    result = new Fact.Test.Result.Error(Text); break;
                case 0x01:
                    result = new Fact.Test.Result.Note(Text); break;
                case 0x02:
                    result = new Fact.Test.Result.Passed(Text); break;
                case 0x03:
                    result = new Fact.Test.Result.Warning(Text); break;
                case 0x04:
                    result = new Fact.Test.Result.Timeout(Text); break;
                case 0x05:
                    result = new Fact.Test.Result.Group(Text); break;
                case 0x06:
                    result = new Fact.Test.Result.Omitted(Text); break;
                case 0x07:
                    result = new Fact.Test.Result.Intermittent(Text); break;
                default:
                    throw new Exception("Try to load an unknow result type");
            }

            result.Text = Text;
            result.Info = Info;
            result.FromLine = FromLine;
            result.FromColumn = FromColumn;
            result.ToLine = ToLine;
            result.ToColumn = ToColumn;
            result._Score = Score;
            result._metadata = metadata;
            result._EnlapsedUserTime = UserTime;
            result._EnlapsedKernelTime = KernelTime;
            result._Ponderation = Ponderation;
            result._Visibility = visibility;
            if (result is Group)
            {
                int SubTestCount = Internal.StreamTools.ReadInt32(stream);
                Group group = result as Group;
                while (SubTestCount > 0)
                {
                    group.AddTestResult(Load(stream));
                    SubTestCount--;
                }
            }
            if (result is Intermittent)
            {
                int SubTestCount = Internal.StreamTools.ReadInt32(stream);
                Intermittent intermittent = result as Intermittent;
                while (SubTestCount > 0)
                {
                    intermittent._AddTest(Load(stream));
                    SubTestCount--;
                }
            }
            return result;
        }
    }
}

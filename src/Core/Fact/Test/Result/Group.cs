﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Test.Result
{
    public class Group : Result
    {
        public Group(string Text)
        {
            base.Score = 1;
            base.Text = Text;
        }

        public Group(string Text, string Info)
        {
            base.Score = 1;
            base.Text = Text;
            base.Info = Info;
        }

        protected override Result LightCopy()
        {
            Group group = new Group(Text, Info);
            foreach (Result result in _results)
            {
                group.AddTestResult(result.Copy());
            }
            return group;
        }

        public override string ToString()
        {
            string str = "";
            if (File != null)
            {
                str = "<group text=\"" + base.Text + "\" info=\"" + base.Info + "\" file=\"" + File.Directory + "/" + File.Name + "\" fromline=\"" + FromLine.ToString() + "\" fromcolumn=\"" + FromColumn.ToString() + "\" toline=\"" + ToLine.ToString() + "\" tocolumn=\"" + ToColumn.ToString() + "\">";
            }
            else
            {
                str = "<group text=\"" + base.Text + "\" info=\"" + base.Info + "\" fromline=\"" + FromLine.ToString() + "\" fromcolumn=\"" + FromColumn.ToString() + "\" toline=\"" + ToLine.ToString() + "\" tocolumn=\"" + ToColumn.ToString() + "\">";
            }
            foreach (Result _ in _results)
            {
                str += _.ToString() + "\n";
            }
            str += "</group>";
            return str;
        }

        internal List<Test.Result.Result> _results = new List<Result>();
        internal List<Group> _dependencies = new List<Group>();

        public override int Score
        {
            get
            {
                int sum = 0;
                for (int n = 0; n < _results.Count; n++)
                {
                    sum += _results[n].Score; 
                }
                return sum;
            }
            set
            {
                throw new Exception("Impossible to set the score of a group");
            }
        }

        public override long EnlapsedKernelTime
        {
            get
            {
                long sum = 0;
                for (int n = 0; n < _results.Count; n++)
                {
                    sum += _results[n].EnlapsedKernelTime;
                }
                return sum;
            }
            set
            {
                throw new Exception("Impossible to set the time enlapsed of a group");
            }
        }


        public override long EnlapsedUserTime
        {
            get
            {
                long sum = 0;
                for (int n = 0; n < _results.Count; n++)
                {
                    sum += _results[n].EnlapsedUserTime;
                }
                return sum;
            }
            set
            {
                throw new Exception("Impossible to set the time enlapsed of a group");
            }
        }

		public int TotalTest()
		{
			int score = 0;
			foreach (Result result in _results)
			{
				if (result is Passed) { score++; }
				if (result is Error)  { score++; }
                if (result is Timeout) { score++; }
                if (result is Group) { score += (result as Group).TotalTest(); }
			}
			return (score);
		}

		public int CountTestPassed()
		{
			int score = 0;
			foreach (Result result in _results)
			{
				if (result is Passed) { score++; }
                if (result is Group) { score += (result as Group).CountTestPassed(); }
			}
			return (score);
		}

        public int AverageTestPassed()
        {
            return ((int)(((float)CountTestPassed() / (float)TotalTest()) * 100.0f));
        }

        internal bool _ContainsGroup(Test.Result.Group Group)
        {
            if (_results.Contains(Group)) { return true; }
            foreach (Result result in _results)
            {
                if (result == null) { continue; }
                if (result is Group) { if ((result as Group)._ContainsGroup(Group)) { return true; } }
            }
            return false;
        }

        public void AddTestResult(Test.Result.Result Result)
        {
            if (Result == null) { return; }
            if (Result._Parent != null && Result._Parent != this)
            {
                throw new Exception("Impossible to store the same test in two groups");
            }
            if (Result is Group)
            {
                // Avoid recursive group
                if (Result == this) { return; }
                if (!_ContainsGroup(Result as Group))
                {
                    Result._Parent = this;
                    foreach (Result subresult in new List<Result>(_results))
                    {
                        if (subresult is Group && ((subresult as Group).Text == Result.Text))
                        {
                            // We will do a group merging here
                            Group source = (subresult as Group);
                            Group destination = (Result as Group);
                            if (source._dependencies.Contains(destination)) { return; }
                            if (source == destination) { continue; }
                            foreach (Result child in new List<Result>(destination._results))
                            {
                                source.AddTestResult(child.Copy());
                            }
                            destination._results = source._results;
                            // Update the dependencies
                            foreach (Group dep in source._dependencies)
                            { dep._results = source._results; }
                            foreach (Group dep in destination._dependencies)
                            { dep._results = source._results; }
                            source._dependencies.Add(destination);
                            destination._dependencies.Add(source);
                            return;
                        }
                    }
                    _results.Add(Result);
                }
            }
            if (!_results.Contains(Result))
            {
                Result._Parent = this;
                for (int n = 0; n < _results.Count; n++)
                {
                    if (_results[n].Text == Result.Text)
                    {
                        if (_results[n] is Fact.Test.Result.Omitted)
                        {
                            // In place replacement
                            _results[n] = Result;
                            return;
                        }
                        else
                        {
                            // Check if we have to update the ponderation
                            if ((_results[n] is Passed && Result is Passed) ||
                                (_results[n] is Error && Result is Error) ||
                                (_results[n] is Timeout && Result is Timeout) ||
                                (_results[n] is Error && Result is Timeout) ||
                                (_results[n] is Timeout && Result is Error))
                            {
                                _results[n]._Ponderation++;
                                Result._Ponderation++;
                            }

                            // Check if we have to create an intermittent test
                            if ((_results[n] is Passed && Result is Error) ||
                                (_results[n] is Error && Result is Passed) ||
                                (_results[n] is Passed && Result is Timeout))
                            {
                                Intermittent intermittent = new Intermittent(_results[n].Text);
                                intermittent._AddTest(_results[n]);
                                intermittent._AddTest(Result);
                                _results[n] = intermittent;
                            }
                            return;
                        }
                    }
                }
                _results.Add(Result);
            }
        }

        public void Merge(Fact.Test.Result.Group Group)
        {
            if (Group == null) { return; }
            foreach (Fact.Test.Result.Result resultB in Group._results)
            {
                if (resultB is Fact.Test.Result.Group)
                {
                    // Test if we can do a merge
                    for (int n = 0; n < _results.Count; n++)
                    {
                        Result resultA = _results[n];
                        if (resultA.Text == resultB.Text)
                        {
                            if (resultA is Fact.Test.Result.Group)
                            {
                                (resultA as Fact.Test.Result.Group).Merge(resultB as Fact.Test.Result.Group);
                                goto next;
                            }
                            else if (resultA is Fact.Test.Result.Omitted)
                            {
                                // We will delete the omitted and use only the group
                                Result newresult = resultB.Copy();
                                newresult._Parent = this;
                                _results[n] = newresult;
                                goto next;
                            }
                            else
                            {
                                // If we can't do the merging then A is preserved
                                goto next;
                            }
                        }
                    }
                }
                {
                    Result newresult = resultB.Copy();
                    newresult._Parent = this;
                    _results.Add(newresult);
                }
            next: ;
            }
        }

        public List<Test.Result.Result> GetTestResults()
        {
            return _results;
        }

        protected override void SaveSpecific(System.IO.Stream stream)
        {
#if DEBUG
            Fact.Log.Debug("Save test group '" + Text + "'");
#endif
            Internal.StreamTools.WriteUInt32(stream, 0x05);
            Internal.StreamTools.WriteInt32(stream, _results.Count);
            foreach (Result result in _results)
            {
                result.Save(stream);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Test.Result
{
    class Intermittent : Result
    {
        List<Result> _Results = new List<Result>();

        internal Intermittent(string Text)
        {
            base.Score = 1;
            base.Text = Text;
        }

        internal Intermittent(string Text, string Info)
        {
            base.Score = 1;
            base.Text = Text;
            base.Info = Info;
        }

        protected override Result LightCopy()
        {
            Intermittent intermittent = new Intermittent(Text, Info);
            foreach (Result result in _Results)
            {
                intermittent._AddTest(result.Copy());
            }
            return intermittent;
        }

        internal void _AddTest(Result Result)
        {
            for (int n = 0; n < Result._Ponderation + 1; n++)
            {
                Result clone = Result.Copy();
                clone._Ponderation = 0;
                _Results.Add(clone);
            }
        }

        public float Intermittency
        {
            get 
            {
                int pass = 0;
                int fail = 0;
                foreach (Result result in _Results)
                {
                    if (result is Passed) { pass++; }
                    if (result is Error || result is Timeout) { fail++; }
                }
                return ((float)pass) / ((float)(pass + fail));
            }
        }

        protected override void SaveSpecific(System.IO.Stream stream)
        {
            Internal.StreamTools.WriteUInt32(stream, 0x07);
            Internal.StreamTools.WriteInt32(stream, _Results.Count);
            foreach (Result result in _Results)
            {
                result.Save(stream);
            }
        }

        public override string ToString()
        {
            string str = "";
            if (File != null)
            {
                str = "<intermittent intermittency=\"" + Intermittency.ToString().Replace(',', '.') + "\" text=\"" + base.Text + "\" info=\"" + base.Info + "\" file=\"" + File.Directory + "/" + File.Name + "\" fromline=\"" + FromLine.ToString() + "\" fromcolumn=\"" + FromColumn.ToString() + "\" toline=\"" + ToLine.ToString() + "\" tocolumn=\"" + ToColumn.ToString() + "\">";
            }
            else
            {
                str = "<intermittent intermittency=\"" + Intermittency.ToString().Replace(',', '.') + "\" text=\"" + base.Text + "\" info=\"" + base.Info + "\" fromline=\"" + FromLine.ToString() + "\" fromcolumn=\"" + FromColumn.ToString() + "\" toline=\"" + ToLine.ToString() + "\" tocolumn=\"" + ToColumn.ToString() + "\">";
            }
            foreach (Result _ in _Results)
            {
                str += _.ToString() + "\n";
            }
            str += "</intermittent>";
            return str;
        }
    }
}

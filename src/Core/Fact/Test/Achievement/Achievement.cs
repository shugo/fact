﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Test.Achievement
{
    public abstract class Achievement
    {
        string _text = "";
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        string _info = "";
        public string Info
        {
            get { return _info; }
            set { _info = value; }
        } 
    }
}

﻿
/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Compilation
{
    static public class Tools
    {

        public static void Make(
            Fact.Processing.Project Project,
            string Flags,
            string Rule,
            out Runtime.Process.Result ProcessResult,
            string Directory)
        {
            Make (Project,
                  Flags,
                  Rule,
                  out ProcessResult,
                  Directory,
                  "");
        }

        public static void Make(
            Fact.Processing.Project Project,
            string Flags,
            string Rule,
            out Runtime.Process.Result ProcessResult,
            string Directory,
            string DirectoryToBuild)
        {
            Make (Project,
                  Flags,
                  Rule,
                  out ProcessResult,
                  Directory,
                  DirectoryToBuild,
                  new string[]{});
        }

        public static void Make(
            Fact.Processing.Project Project,
            string Flags,
            string Rule,
            out Runtime.Process.Result ProcessResult,
            string Directory,
            string DirectoryToBuild,
            params string[] Hide)
        {
            Make(Project,
                 Flags,
                 Rule,
                 -1,
                 out ProcessResult,
                 Directory,
                 DirectoryToBuild,
                 Hide);
        }

        public static void Make(
            Fact.Processing.Project Project,
            string Flags,
            string Rule,
            int Timeout,
            out Runtime.Process.Result ProcessResult,
            string Directory,
            string DirectoryToBuild,
            params string[] Hide)
        {
            string make = Internal.Information.Where ("gmake");
            if (!System.IO.File.Exists(make))
                make = Internal.Information.GetMakeBinary();

            if (!System.IO.File.Exists(make)) { ProcessResult = null; return; }

            string tmpdir = Internal.Information.GetTempFileName();
            Fact.Tools.RecursiveMakeDirectory(tmpdir);

            Project.ExtractDirectory(Directory, tmpdir, null);

            foreach (string rem in Hide)
            {
                try
                {
                    Fact.Tools.RecursiveDelete(tmpdir + "/" + rem);
                }
                catch
                {
                }
            }

            if (DirectoryToBuild == "")
            {
                using (Fact.Runtime.Process process = new Fact.Runtime.Process(make, tmpdir, Flags + " " + Rule))
                {
                    ProcessResult = process.Run(-1);
                }
            }
            else
            {
                using (Fact.Runtime.Process process = new Fact.Runtime.Process(make, tmpdir + "/" + DirectoryToBuild, Flags + " " + Rule))
                {
                    ProcessResult = process.Run(-1);
                }
            }
            Project.AddRealDirectory(Directory, tmpdir);

            Fact.Tools.RecursiveDelete(tmpdir);
        }

        public static List<Fact.Processing.File> ArExtract(Fact.Processing.File Lib)
        {
            string ar = Internal.Information.Where("ar");
            if (!System.IO.File.Exists(ar)) { return null; }
            List<Fact.Processing.File> files = new List<Fact.Processing.File> ();
            string tmpdir = Fact.Tools.CreateTempDirectory ();

            string libfile = Lib.ExtractAt (tmpdir);

            using (Runtime.Process arp = new Fact.Runtime.Process(ar, tmpdir, " -x " + Lib.Name))
            {
                arp.Run(-1);
            }
            Fact.Tools.RecursiveDelete (libfile);
            foreach (string _ in System.IO.Directory.GetFiles(tmpdir))
            {
                Fact.Processing.File newfile = new Fact.Processing.File (System.IO.File.ReadAllBytes (_), System.IO.Path.GetFileName(_));
                newfile.Type = Fact.Processing.File.FileType.Object;
                files.Add (newfile);
            }
            Fact.Tools.RecursiveDelete(tmpdir);
            return files;
        }

        public static Processing.File GXX(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            IEnumerable<Fact.Processing.File> Files)
        {
            return GXX(OutputName,
                    Flags,
                    -1,
                    out ProcessResult,
                    Files);
        }

        public static Processing.File GXX(
            string OutputName,
            string Flags,
            int Timeout,
            out Runtime.Process.Result ProcessResult,
            IEnumerable<Fact.Processing.File> Files)
        {
            return GXX(OutputName,
                    Flags,
                    Timeout,
                    out ProcessResult,
                    Files.ToArray());
        }

        public static Processing.File GXX(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            params Fact.Processing.File[] Files)
        {
            string compiler = "";
            compiler = Fact.Internal.Information.Where("g++");
            if (!System.IO.File.Exists(compiler))
            { compiler = Internal.Information.GetCXXBinary(); }
            return GCC(compiler,
                    OutputName,
                    Flags,
                    -1,
                    out ProcessResult,
                    Files);
        }

        public static Processing.File GXX(
            string OutputName,
            string Flags,
            int Timeout,
            out Runtime.Process.Result ProcessResult,
            params Fact.Processing.File[] Files)
        {
            string compiler = "";
            compiler = Fact.Internal.Information.Where("g++");
            if (!System.IO.File.Exists (compiler))
            { compiler = Internal.Information.GetCXXBinary(); }
            return GCC(compiler,
                    OutputName,
                    Flags,
                    Timeout,
                    out ProcessResult,
                    Files);
        }

        public static Processing.File GCC(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            params Fact.Processing.File[] Files)
        {
            return GCC(Fact.Internal.Information.Where("gcc"),
                    OutputName,
                    Flags,
                    out ProcessResult,
                    Files);
        }

        public static Processing.File GCC(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            IEnumerable<Fact.Processing.File> Files)
        {
            return GCC(OutputName,
                    Flags,
                    -1,
                    out ProcessResult,
                    Files);
        }

        public static Processing.File GCC(
        string OutputName,
        string Flags,
        int Timeout,
        out Runtime.Process.Result ProcessResult,
        IEnumerable<Fact.Processing.File> Files)
        {
            return GCC(Fact.Internal.Information.Where("gcc"),
                    OutputName,
                    Flags,
                    Timeout,
                    out ProcessResult,
                    Files.ToArray());
        }

        public static Processing.File GCC(
            string Compiler,
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            IEnumerable<Fact.Processing.File> Files)
        {
            return GCC(Compiler,
                    OutputName,
                    Flags,
                    out ProcessResult,
                    Files.ToArray());
        }
        public static Processing.File GCC(
            string Compiler,
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            params Fact.Processing.File[] Files)
        {
            return GCC(Compiler, OutputName, Flags, -1, out ProcessResult, Files);
        }

        public static Processing.File GCC(
            string Compiler,
            string OutputName,
            string Flags,
            int Timeout,
            out Runtime.Process.Result ProcessResult,
            params Fact.Processing.File[] Files)
        {
            string gcc = Compiler;
            if (gcc == "" || !System.IO.File.Exists(gcc)) { gcc = Internal.Information.GetCCBinary(); }
            if (!System.IO.File.Exists(gcc)) { ProcessResult = null; return null; }

            string tmpdir = Fact.Tools.CreateTempDirectory();
            string files = "";

            string libs = "";
            bool libmode = false;

            if (OutputName.EndsWith(".dll") || OutputName.EndsWith(".so"))
            {
                libmode = true;
                Internal.Information.OperatingSystem os = Internal.Information.CurrentOperatingSystem();

                if (!Flags.Contains("-shared"))
                {
                    Flags = Flags + " -shared ";
                }

                if (OutputName.EndsWith(".dll") &&
                    !(os == Fact.Internal.Information.OperatingSystem.MicrosoftDOS ||
                      os == Fact.Internal.Information.OperatingSystem.MicrosoftWindows ||
                      os == Fact.Internal.Information.OperatingSystem.MicrosoftWindowsCE ||
                      os == Fact.Internal.Information.OperatingSystem.MicrosoftXBox360))
                {
                    OutputName = OutputName.Replace(".dll", ".so");
                }
                else if (OutputName.EndsWith(".so") &&
                     (os == Fact.Internal.Information.OperatingSystem.MicrosoftDOS ||
                      os == Fact.Internal.Information.OperatingSystem.MicrosoftWindows ||
                      os == Fact.Internal.Information.OperatingSystem.MicrosoftWindowsCE ||
                      os == Fact.Internal.Information.OperatingSystem.MicrosoftXBox360))
                {
                    OutputName = OutputName.Replace(".so", ".dll");
                }
            }

            foreach (Fact.Processing.File file in Files)
            {
                if (file != null)
                {
                    string filepath = "";
                    string filename = file.Name;
                    if (filename == "" ||
                        System.IO.File.Exists(tmpdir + "/" + filename))
                    {
                        // No way we have to create a temp name for it
                        // since we can't use the realone
                        filename = Internal.Information.GetTempFileName();
                    }
                    filepath = tmpdir + "/" + filename;
                    if (file.Binary) { System.IO.File.WriteAllBytes(filepath, file.GetBytes()); }
                    else { System.IO.File.WriteAllText(filepath, file.GetText(true)); }
                    if (file.Type != Fact.Processing.File.FileType.CHeader
                    && file.Type != Fact.Processing.File.FileType.CPPHeader)
                    {
                        if (file.Type == Fact.Processing.File.FileType.DynamicLibrary ||
                            file.Type == Fact.Processing.File.FileType.StaticLibrary)
                        {
                            if (filename.StartsWith("lib"))
                            {
                                if (filename.EndsWith(".so") || file.Name.EndsWith(".ar"))
                                {
                                    libs += "-l" + filename.Substring(0, filename.Length - ".so".Length).Substring("lib".Length) + " ";
                                }
                                else if (filename.EndsWith(".a"))
                                {
                                    libs += "-l" + filename.Substring(0, filename.Length - ".a".Length).Substring("lib".Length) + " ";
                                }
                                else
                                {
                                    libs += "-l" + filename.Substring("lib".Length) + " ";
                                }
                            }
                            else
                            {
                                libs += "-l" + filename + " ";
                            }
                        }
                        else
                        {
                            files += filename + " ";
                        }
                    }
                }
            }
            if (libs != "" && (!Flags.Contains ("-L.") || !Flags.Contains ("-L .")))
                Flags += " -L. ";
            using (Runtime.Process gccp = new Fact.Runtime.Process(gcc, tmpdir, " -o \"" + OutputName + "\" " + Flags + " " + files + " " + libs))
            {
                ProcessResult = gccp.Run(Timeout);
            }
            Fact.Processing.File Result = null;
            if (System.IO.File.Exists(tmpdir + "/" + OutputName))
            {
                Result =
                    new Fact.Processing.File(
                        System.IO.File.ReadAllBytes(tmpdir + "/" + OutputName),
                        OutputName);
                if (OutputName.EndsWith(".so"))
                    Result.Type = Fact.Processing.File.FileType.DynamicLibrary;
                else if (OutputName.EndsWith(".o"))
                    Result.Type = Fact.Processing.File.FileType.Object;
                else if (OutputName.EndsWith(".a") || OutputName.EndsWith(".ar"))
                    Result.Type = Fact.Processing.File.FileType.StaticLibrary;
                else
                    Result.Type = Fact.Processing.File.FileType.Executable;

            }

            // Specific case for gcc on cygwin
            if (System.IO.File.Exists(tmpdir + "/" + OutputName + ".exe"))
            {
                Result =
                    new Fact.Processing.File(
                        System.IO.File.ReadAllBytes(tmpdir + "/" + OutputName + ".exe"),
                        OutputName);
                Result.Type = Fact.Processing.File.FileType.Executable;
            }
            Fact.Tools.RecursiveDelete(tmpdir);
            return Result;
        }

        public static Processing.File CC(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            params Fact.Processing.File[] Files)
        {
            return GCC(OutputName, Flags, out ProcessResult, Files);
        }

        public static Processing.File CC(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            IEnumerable<Fact.Processing.File> Files)
        {
            return GCC(OutputName, Flags, out ProcessResult, Files);
        }

        public static Processing.File CXX(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            params Fact.Processing.File[] Files)
        {
            return GXX(OutputName, Flags, out ProcessResult, Files);
        }

        public static Processing.File CXX(
            string OutputName,
            string Flags,
            out Runtime.Process.Result ProcessResult,
            IEnumerable<Fact.Processing.File> Files)
        {
            return GXX(OutputName, Flags, out ProcessResult, Files);
        }

        public class Symbol
        {
            string _name;
            string _lib;
            public string Name { get { return _name; } set { _name = value; } }
            public string Library { get { return _lib; } set { _lib = value; } }

            internal Symbol(string name)
            {
                string[] arg = name.Split(new string[] { "@@" }, StringSplitOptions.RemoveEmptyEntries);
                if (arg.Length == 1)
                    _name = arg[0];
                else
                {
                    _name = arg[0];
                    _lib = arg[0];
                    if (_lib.ToLower().StartsWith("fbsd_"))
                    {
                        _lib = "libc";
                    }
                    else if (_lib.ToLower().StartsWith("glibc_"))
                    {
                        _lib = "libc";
                    }
                }
            }
            public override bool  Equals(object obj)
            {
                  if (obj is Symbol)
                 {
                    Symbol sobj = obj as Symbol;
                    return (sobj.Library == Library && sobj.Name == Name);
                 }
                 else
                 { return false; }
            }
            public static bool operator ==(Symbol A, Symbol B)
            {
                return (A.Name == B.Name && A.Library == B.Library);
            }

            public static bool operator !=(Symbol A, Symbol B)
            {
                return (A.Name != B.Name || A.Name != B.Name);
            }
        }

        public static List<Symbol> GetExportedSymbols(Fact.Processing.File File)
        {
            if (File == null)
            {
                return new List<Symbol>();
            }

            string nm = Internal.Information.Where("nm");
            if (!System.IO.File.Exists(nm)) { return new List<Symbol>(); }

            string tmpdir = Internal.Information.GetTempFileName();

            if (File.Binary)
            { System.IO.File.WriteAllBytes(tmpdir, File.GetBytes()); }
            else
            { System.IO.File.WriteAllLines(tmpdir, File.GetLines(false)); }
            Fact.Tools.AddExecutionPermission (tmpdir);

            Fact.Runtime.Process nmp = new Fact.Runtime.Process(nm, "", tmpdir);
            string list = nmp.Run(-1).StdOut;
            Fact.Tools.RecursiveDelete(tmpdir);
            string[] lines = list.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            List<Symbol> symbols = new List<Symbol>();
            for (int n = 0; n < lines.Length; n++)
            {
                string[] Items = lines[n].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                if (Items.Length >= 3)
                {
                    if (Items[1] == "T" ||
                        Items[1] == "t" ||
                        Items[1] == "S" ||
                        Items[1] == "s")
                    {
                        if (Items[2].ToLower().StartsWith("_fini") ||
                            Items[2].ToLower().StartsWith("_init"))
                            continue;
                        symbols.Add(new Symbol(Items[2]));
                    }
                }
                else if (Items.Length == 2)
                {
                    if (Items [0] == "T" ||
                        Items [0] == "t" ||
                        Items [0] == "S" ||
                        Items [0] == "s")
                    {
                        if (Items [1].ToLower ().StartsWith("_fini") ||
                            Items [1].ToLower ().StartsWith("_init"))
                            continue;
                    }
                }
            }

            return symbols;
        }

        public static List<Symbol> GetUndefinedSymbols(Fact.Processing.File File)
        {
            if (File == null)
            {
                return new List<Symbol>();
            }

            string nm = Internal.Information.Where("nm");
            if (!System.IO.File.Exists(nm)) { return new List<Symbol>(); }

            string tmpdir = Internal.Information.GetTempFileName();

            if (File.Binary)
            { System.IO.File.WriteAllBytes(tmpdir, File.GetBytes()); }
            else
            { System.IO.File.WriteAllLines(tmpdir, File.GetLines(false)); }
            Fact.Tools.AddExecutionPermission (tmpdir);
            string list = "";
            using (Fact.Runtime.Process nmp = new Fact.Runtime.Process(nm, "", tmpdir))
            {
                list = nmp.Run(-1).StdOut;
            }
            Fact.Tools.RecursiveDelete(tmpdir);
            string[] lines = list.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            List<Symbol> symbols = new List<Symbol>();
            for (int n = 0; n < lines.Length; n++)
            {
                string[] Items = lines[n].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                if (Items.Length >= 3)
                {
                    if (Items[1] == "U")
                    {
                        if (Items[2].ToLower().StartsWith("__libc_start_main@@") ||
                            Items[2].ToLower().StartsWith("_init_tls@@") ||
                            Items[2].ToLower().StartsWith("atexit@@") ||
                            Items[2].ToLower().StartsWith("exit@@") ||
                            Items[2].ToLower().Contains("_global_offset_table_") ||
                            Items[2].ToLower().Contains("__assert_fail"))
                            continue;
                        symbols.Add(new Symbol(Items[2]));
                    }
                }
                else if (Items.Length == 2)
                {
                    if (Items [0] == "U")
                    {
                        if (Items [1].ToLower ().StartsWith("__libc_start_main@@") ||
                            Items [1].ToLower ().StartsWith("_init_tls@@") ||
                            Items [1].ToLower ().StartsWith("atexit@@") ||
                            Items [1].ToLower ().StartsWith("exit@@") ||
                            Items [1].ToLower().Contains("_global_offset_table_")||
                            Items [1].ToLower().Contains("__assert_fail"))
                            continue;
                    }
                    if (Items[0] == "U") { symbols.Add(new Symbol(Items[1])); }
                }
            }

            return symbols;
        }
    }
}

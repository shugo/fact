﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDAP
{
    public class LDAP : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "LDAP";
            }
        }
        public override int Run(params string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("server", "The server that should be bound to an ldap", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddStringInput("ldap", "The target ldap server", ""); CommandLine.AddShortInput("l", "ldap");
            CommandLine.AddStringInput("organizational-unit", "The organizational unit that should be mapped in the user database", ""); CommandLine.AddShortInput("o", "organizational-unit");
            CommandLine.AddStringInput("group", "The group that will be created to map the organizational unit", ""); CommandLine.AddShortInput("g", "group");
            CommandLine.AddBooleanInput("help", "Display the help message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args))
            {
                CommandLine.PrintErrors();
                return 1;
            }
            if (CommandLine["help"].AsBoolean())
            {
                CommandLine.PrintUsage();
                return 0;
            }
            string server = CommandLine["server"].AsString();

            return 0;
        }
    }
}

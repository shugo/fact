﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Structures
{
    class Graph<EdgeType, ValueType>
    {
        public class Node<EdgeType, ValueType>
        {
            ValueType _Value;
            Graph<EdgeType, ValueType> _Parent;

            internal Node(Graph<EdgeType, ValueType> Parent, ValueType Value)
            {
                _Parent = Parent;
                _Value = Value;
            }

            List<EdgeType> _Edges = new List<EdgeType>();
            List<Node<EdgeType, ValueType>> _Nodes = new List<Node<EdgeType, ValueType>>();

            public void Add(EdgeType Edge, Node<EdgeType, ValueType> Node)
            {
                lock (this)
                {
                    _Edges.Add(Edge);
                    _Nodes.Add(Node);
                }
            }
        }

        public Node<EdgeType, ValueType> CreateNode(ValueType Value)
        {
            Node<EdgeType, ValueType> node = new Node<EdgeType, ValueType>(this, Value);
            _Nodes.Add(node);
            return node;
        }

        HashSet<Node<EdgeType, ValueType>> _Nodes = new HashSet<Node<EdgeType, ValueType>>();
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Structures
{
    public class Tree<T>
    {
        protected bool _Associated = false;
        T _Value;
        List<Tree<T>> _Children = new List<Tree<T>>();

        public Tree(T Value) { }
        public virtual void AddChild(T Value) 
        {
            lock (_Children)
            {
                Tree<T> tree = new Tree<T>(Value);
                tree._Associated = true;
                _Children.Add(tree);
            }
        }
        public virtual void AddChild(Tree<T> Value)
        {
            lock (_Children)
            {
                if (Value._Associated)
                {
                    WeakTree<T> tree = new Tree<T>.WeakTree<T>(Value);
                    _Children.Add(tree);
                }
                else
                {
                    _Children.Add(Value);
                    Value._Associated = true;
                }
            }
        }

        public virtual List<Tree<T>> Children { get { return _Children; } }

        class WeakTree<T> : Tree<T>
        {
            Tree<T> _Link = null;
            public WeakTree(Tree<T> Tree) : base (Tree._Value) { _Link = Tree; }
            public override void AddChild(T Value) { _Link.AddChild(Value); }
            public override void AddChild(Tree<T> Value) { _Link.AddChild(Value); }
            public override List<Tree<T>> Children { get { return base.Children; } }
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Directory
{
    public class Group
    {
        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; ; } }
        Dictionary<string, User> _Users = new Dictionary<string, User>();
        Dictionary<string, Group> _Groups = new Dictionary<string, Group>();
        public Group() { }
        public Group(string Name) { _Name = Name; }
        public bool Contains(User User)
        {
            lock (this)
            {
                if (_Users.ContainsKey(User.Login))
                {
                    // FIX ME compare GUIDs
                    return true;
                }
                foreach (Group group in _Groups.Values)
                {
                    if (group.Contains(User)) { return true; }
                }
            }
            return false;
        }

        public void AddUser(User User) { lock (this) { _Users.Add(User.Login, User); } }
        public void AddGroup(Group Group) { lock (this) { _Groups.Add(Group._Name, Group); } }

        public bool RemoveUser(string UserLogin)
        {
            lock (this)
            {
                if (_Users.ContainsKey(UserLogin)) { _Users.Remove(UserLogin); return true; }
                foreach (Group g in _Groups.Values)
                {
                    if (g.RemoveUser(UserLogin)) { return true; }
                }
            }
            return false;
        }

        public bool RemoveGroup(string GroupName)
        {
            lock (this)
            {
                if (_Groups.ContainsKey(GroupName)) { _Groups.Remove(GroupName); return true; }
                foreach (Group g in _Groups.Values)
                {
                    if (g.RemoveUser(GroupName)) { return true; }
                }
            }
            return false;
        }

        public User GetUser(string UserLogin)
        {
            lock (this) 
            {
                User u = null;
                if (_Users.TryGetValue(UserLogin, out u)) { return u; }
                foreach (Group g in _Groups.Values)
                {
                    u = g.GetUser(UserLogin);
                    if (u != null) { return u; }
                }
            }
            return null;
        }

        public void Save(string File)
        {
            Load(System.IO.File.Open(File, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite));
        }

        public void Save(System.IO.Stream Stream)
        {
            lock (this)
            {
                Internal.StreamTools.WriteUTF8String(Stream, _Name);
                uint usercount = 0;
                foreach (string key in _Users.Keys)
                {
                    if (!_Users[key].NoAccountSerialization) { usercount++; }
                }
                Internal.StreamTools.WriteUInt32(Stream, (UInt32)usercount);

                foreach (string key in _Users.Keys)
                {
                    if (!_Users[key].NoAccountSerialization)
                    {
                        Internal.StreamTools.WriteUTF8String(Stream, key);
                        _Users[key].Save(Stream);
                    }
                }
                Internal.StreamTools.WriteUInt32(Stream, (UInt32)_Groups.Count);
                foreach (string key in _Groups.Keys)
                {
                    Internal.StreamTools.WriteUTF8String(Stream, key);
                    _Groups[key].Save(Stream);
                }
            }
        }

        public void Load(string File)
        {
            Load(System.IO.File.Open(File, System.IO.FileMode.Open, System.IO.FileAccess.Read));
        }

        public void Load(System.IO.Stream Stream)
        {
            _Name = Internal.StreamTools.ReadUTF8String(Stream);
#if DEBUG
            Fact.Log.Debug("Load group: " + _Name);
#endif

            UInt32 cbuser = Internal.StreamTools.ReadUInt32(Stream);
            while (cbuser > 0)
            {
                string key = Internal.StreamTools.ReadUTF8String(Stream);
                User user = new User("", ""); user.Load(Stream);
                _Users.Add(key, user);
                cbuser--;
            }
            UInt32 cbgroup = Internal.StreamTools.ReadUInt32(Stream);
            while (cbgroup > 0)
            {
                string key = Internal.StreamTools.ReadUTF8String(Stream);
                Group group = new Group(); group.Load(Stream);
                _Groups.Add(key, group);
                cbgroup--;
            }
        }

        public IEnumerator<User> GetEnumerator()
        {
            lock (this)
            {
                foreach (User user in _Users.Values)
                {
                    yield return user;
                }
                foreach (Group group in _Groups.Values)
                {
                    foreach (User user in group)
                    {
                        yield return user;
                    }
                }
            }
        }

        public Dictionary<string, User> GetUsers()
        {
            lock (this)
            {
                return new Dictionary<string, User>(_Users);
            }
        }

        public Dictionary<string, Group> GetGroups()
        {
            lock (this)
            {
                return new Dictionary<string, Group>(_Groups);
            }
        }

        public Group Copy(bool Strip)
        {
            Group clone = new Group(_Name);
            lock (this)
            {
                foreach (User user in _Users.Values) { clone._Users.Add(user.Login, user.Copy(Strip)); }
                foreach (Group group in _Groups.Values) { clone._Groups.Add(group.Name, group.Copy(Strip)); }
            }
            return clone;
        }

        public bool AuthenticateWithCredential(string User, byte[] Id, byte[] Password, User.AuthenticationMethod Method, out User AuthenticatedUser, out Credential Credential)
        {
            Credential = null;
            AuthenticatedUser = null;
            lock (this)
            {
                Fact.Directory.User RealUser = null;
                if (_Users.TryGetValue(User, out RealUser))
                {
                    if (RealUser.AuthenticateWithCredential(Id, Password, Method, out Credential))
                    { AuthenticatedUser = RealUser; return true; }
                }
                foreach (Group group in _Groups.Values)
                {
                    if (group.AuthenticateWithCredential(User, Id, Password, Method, out AuthenticatedUser, out Credential))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Authenticate(string User, byte[] Password, User.AuthenticationMethod Method, out User AuthenticatedUser)
        {
            AuthenticatedUser = null;
            lock (this)
            {
                Fact.Directory.User RealUser = null;
                if (_Users.TryGetValue(User, out RealUser))
                {
                    if (RealUser.Authenticate(Password, Method))
                    { AuthenticatedUser = RealUser; return true; }
                }
                foreach (Group group in _Groups.Values)
                {
                    if (group.Authenticate(User, Password, Method, out AuthenticatedUser))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

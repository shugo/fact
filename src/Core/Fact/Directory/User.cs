﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Directory
{
    public class User
    {
        static System.Security.Cryptography.SHA512 SHA512 = System.Security.Cryptography.SHA512.Create();
        public enum AuthenticationMethod
        {
            SHA512,
            SHA512_Salt,
            Fact_TimeSafe
        }

        bool _DenyPasswordAuth = false;
        public bool DenyPasswordAuth
        { get { return _DenyPasswordAuth; } set { _DenyPasswordAuth = value; } }

        bool _NoAccountSerialization = false;
        public bool NoAccountSerialization
        { get { return _NoAccountSerialization; } set { _NoAccountSerialization = value; } }

        string _FirstName = "";
        public string FirstName { get { return _FirstName; } set { _FirstName = value; } }
        string _LastName = "";
        public string LastName { get { return _LastName; } set { _LastName = value; } }
        string _Login = "";
        public string Login { get { return _Login; } set { _Login = value; } }
        string _PhoneNumber = "";
        public string PhoneNumber { get { return _PhoneNumber; } set { _PhoneNumber = value; } }
        string _UID = "";
        public string UID { get { return _UID; } set { _UID = value; } }
        byte[] _SHA512SaltPassword = null;
        public byte[] Password_SHA512_Salt { get { return _SHA512SaltPassword; } }
        public User(string User, string Password)
        {
            _Login = User;
            lock (SHA512) { _SHA512SaltPassword = SHA512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Password + "__fact__" + _Login)); }
        }

        bool AuthenticateTimeSafe(byte[] Password, byte[] ReferencePassword)
        {
            DateTime now = DateTime.UtcNow;
            List<byte> TimeSeq = new List<byte>(ReferencePassword); TimeSeq.AddRange(Internal.Information.TimeCode(now));
            byte[] TimeHash = null;
            lock (SHA512) { TimeHash = SHA512.ComputeHash(TimeSeq.ToArray()); }
            for (int n = 0; n < TimeHash.Length; n++) { if (Password[n] != TimeHash[n]) { goto nextp1; } }
            return true;
        nextp1:
            DateTime p1 = now + new TimeSpan(0, 1, 0);
            List<byte> TimeSeqp1 = new List<byte>(ReferencePassword); TimeSeqp1.AddRange(Internal.Information.TimeCode(p1));
            lock (SHA512)
            { TimeHash = SHA512.ComputeHash(TimeSeqp1.ToArray()); }
            for (int n = 0; n < TimeHash.Length; n++) { if (Password[n] != TimeHash[n]) { goto nextm1; } }
            return true;
        nextm1:
            DateTime m1 = now - new TimeSpan(0, 1, 0);
            List<byte> TimeSeqm1 = new List<byte>(ReferencePassword); TimeSeqm1.AddRange(Internal.Information.TimeCode(m1));
            lock (SHA512) { TimeHash = SHA512.ComputeHash(TimeSeqm1.ToArray()); }
            for (int n = 0; n < TimeHash.Length; n++) { if (Password[n] != TimeHash[n]) { return false; } }
            return true;
        }

        public bool AuthenticateWithCredential(byte[] Id, byte[] Password, AuthenticationMethod Method, out Credential Credential)
        {
            Credential = null;
            lock (_Credential)
            {
                foreach (Credential cred in _Credential)
                {
                    if (cred._CredId.Length != Id.Length) { continue; }
                    for (int n = 0; n < cred._CredId.Length; n++)
                    {
                        if (cred._CredId[n] != Id[n]) { goto next; }
                    }
                    Credential = cred;
                    break;
		    next: ;
                }
            }
            if (Credential == null) { return false; }
            if (Credential._Expiration < DateTime.UtcNow) { return false; }
            switch (Method)
            {
                case AuthenticationMethod.Fact_TimeSafe: return AuthenticateTimeSafe(Password, Credential._Password);
            }
            return false;
        }

        public bool Authenticate(byte[] Password, AuthenticationMethod Method)
        {
            if (_DenyPasswordAuth) { return false; }
            switch (Method)
            {
                case AuthenticationMethod.SHA512_Salt:
                    {
                        lock (_SHA512SaltPassword)
                        {
                            if (Password.Length != _SHA512SaltPassword.Length) { return false; }
                            for (int n = 0; n < Password.Length; n++) { if (Password[n] != _SHA512SaltPassword[n]) { return false; } }
                            return true;
                        }
                        break;
                    }
                case AuthenticationMethod.Fact_TimeSafe:
                    {
                        lock (_SHA512SaltPassword)
                        {
                            return AuthenticateTimeSafe(Password, _SHA512SaltPassword);
                        }
                    }
            }
            return false;
        }

        internal List<Credential> _Credential = new List<Credential>();

        internal void Save(System.IO.Stream Stream)
        {
            if (_NoAccountSerialization) { throw new Exception("This account must not be serialized"); }

            Internal.StreamTools.WriteUTF8String(Stream, _FirstName);
            Internal.StreamTools.WriteUTF8String(Stream, _LastName);
            Internal.StreamTools.WriteUTF8String(Stream, _Login);
            Internal.StreamTools.WriteUTF8String(Stream, _PhoneNumber);
            Internal.StreamTools.WriteUTF8String(Stream, _UID);
            Internal.StreamTools.WriteByteArray(Stream, _SHA512SaltPassword);
            lock (_Credential)
            {
                int count = 0;
                foreach (Credential c in _Credential)
                {
                    if (!c.NoSerialization)
                    { count++; }
                }
                Internal.StreamTools.WriteInt32(Stream, count);
                foreach (Credential c in _Credential)
                {
                    if (!c.NoSerialization)
                    {  c.Save(Stream); }
                }
            }

        }

        public User Copy(bool Strip)
        {
            User clone = new User("", "");
            lock (this)
            {
                clone._FirstName = _FirstName;
                clone._LastName = _LastName;
                clone._Login = _Login;
                clone._PhoneNumber = _PhoneNumber;
                clone._UID = _UID;
                clone._DenyPasswordAuth = _DenyPasswordAuth;
                clone._NoAccountSerialization = _NoAccountSerialization;
                if (!Strip)
                {
                    clone._Credential = new List<Credential>(_Credential);
                    clone._SHA512SaltPassword = (byte[])_SHA512SaltPassword.Clone();
                }
                else
                {
                    clone._SHA512SaltPassword = new byte[0];
                    clone._Credential = new List<Credential>();
                }
            }
            return clone;
        }

        internal void Load(System.IO.Stream Stream)
        {
            _FirstName = Internal.StreamTools.ReadUTF8String(Stream);
            _LastName = Internal.StreamTools.ReadUTF8String(Stream);
            _Login = Internal.StreamTools.ReadUTF8String(Stream);
            _PhoneNumber = Internal.StreamTools.ReadUTF8String(Stream);
            _UID = Internal.StreamTools.ReadUTF8String(Stream);
            _SHA512SaltPassword = Internal.StreamTools.ReadByteArray(Stream);

#if DEBUG
            Fact.Log.Debug("Load user: " + _Login);
#endif

            int credCount = Internal.StreamTools.ReadInt32(Stream);
            lock (_Credential)
            {
                _Credential.Clear();
                for (int n = 0; n < credCount; n++)
                {
                    Credential c = new Credential();
                    c.Load(Stream);
                    _Credential.Add(c);
                }
            }
        }
    }
}

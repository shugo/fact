﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Directory
{
    public class Credential
    {
        static System.Random Random = new Random();

        public Credential() { }
        public Credential(string File) { Load(File); }
        public Credential(User User, DateTime Expiration)
        {
        restart:
            _Expiration = Expiration;
            _Username = User.Login;
            _Password = new byte[512];
            lock (Random)
            {
                Random.NextBytes(_Password);
                _CredId = new byte[128];
                Random.NextBytes(_CredId);
            }
            lock (User._Credential)
            {
                foreach (Credential c in User._Credential)
                {
                    if (c._CredId.Length != _CredId.Length)
                        continue;
                    for (int n = 0; n < c._CredId.Length; n++)
                    { if (c._CredId[n] != _CredId[n]) { goto next; } }
                    goto restart;
                next: ;
                }
                User._Credential.Add(this);
            }

        }

        bool _NoSerialization = false;
        public bool NoSerialization
        { get { return _NoSerialization; } set { _NoSerialization = value; } }


        internal string _Username = "";
        internal byte[] _CredId = new byte[0];
        internal byte[] _Password = new byte[0];

        internal DateTime _Expiration = DateTime.Now;

        public string Username { get { return _Username; } }
        public byte[] Password { get { return _Password; } }
        public byte[] Id { get { return _CredId; } }


        public void Save(string File)
        {
            System.IO.FileStream stream = System.IO.File.Open(File, System.IO.FileMode.OpenOrCreate);
            Save(stream);
            stream.Close();
        }
        public void Save(System.IO.Stream Stream)
        {
            unchecked { Internal.StreamTools.WriteUInt32(Stream, 0xFAC1C9ED); }
            Internal.StreamTools.WriteDateTime(Stream, _Expiration);
            Internal.StreamTools.WriteUTF8String(Stream, _Username);
            Internal.StreamTools.WriteByteArray(Stream, _CredId);
            Internal.StreamTools.WriteByteArray(Stream, _Password);
        }

        public void Load(string File)
        {
            System.IO.FileStream stream = System.IO.File.Open(File, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            Load(stream);
            stream.Close();
        }
        public void Load(System.IO.Stream Stream)
        {
            unchecked
            {
                uint value = Internal.StreamTools.ReadUInt32(Stream);
                if (value != 0xFAC1C9ED && value != 0xEDC9C1FA)
                    throw new Exception("Invalid Credential Data");
            }
            try
            {
                _Expiration = Internal.StreamTools.ReadDateTime(Stream);
                _Username = Internal.StreamTools.ReadUTF8String(Stream);
                _CredId = Internal.StreamTools.ReadByteArray(Stream);
                _Password = Internal.StreamTools.ReadByteArray(Stream);
            }
            catch { throw new Exception("Corrupted credential data"); }
            if (_Expiration < System.DateTime.UtcNow)
            {
                throw new Exception("The credential has expired");
            }

        }
    }
}

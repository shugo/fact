﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Directory
{
    class AuthenticationModule : System.Net.IAuthenticationModule
    {
        Directory _Directory = null;
        public Directory Directory { get { lock (this) { return _Directory; } } set { lock (this) { _Directory = value; } } }
        public AuthenticationModule(Directory Directory)
        {
            _Directory = Directory;

        }
        public System.Net.Authorization Authenticate(string challenge, System.Net.WebRequest request, System.Net.ICredentials credentials)
        {
            if (_Directory == null) { return null; }
            try
            {
                int port = request.RequestUri.Port;
                lock (this)
                {

                }
            }
            catch { }
            return null;
        }

        public string AuthenticationType
        {
            get { return "kerberos"; }
        }

        public bool CanPreAuthenticate
        {
            get { return false; }
        }

        public System.Net.Authorization PreAuthenticate(System.Net.WebRequest request, System.Net.ICredentials credentials)
        {
            return null;
        }
    }
}

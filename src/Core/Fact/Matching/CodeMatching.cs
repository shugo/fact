﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Matching
{
    public class CodeMatching
    {
        public class Similarity
        {
            Processing.File _Source;
            Processing.File _Ref;
            Type _Type;

            public Processing.File Source { get { return _Source; } }
            public Processing.File Ref { get { return _Ref; } }
            public Type SimilarityType { get { return _Type; } }

            public enum Type
            {
                Clone,
                CopyPaste,
                Remix,
            }

            public Similarity(Processing.File Source, Processing.File Ref, Type Type)
            {
                _Source = Source; _Ref = Ref; _Type = Type;
            }
        }

        public CodeMatching(Processing.Project Project, List<Processing.Project> References)
        {

        }



        void Match(Processing.Project A, Processing.Project B)
        {
            if (A == B) { return; }
            foreach (Processing.File File in A)
            {
                foreach (Processing.File Ref in B)
                {
                    Match(A, B);
                }
            }
        }

        List<Similarity> Match(Processing.File A, Processing.File B)
        {
            A.Rebuild();
            B.Rebuild();

            List<Similarity> Similarities = new List<Similarity>();

            // Copy Paste File
            if (A.GetText(true) == B.GetText(true))
            {
                Similarities.Add(new Similarity(A, B, Similarity.Type.CopyPaste));
                return Similarities;
            }

            List<Fact.Character.MetaToken> ATokens = A.GetAsToken();
            List<Fact.Character.MetaToken> BTokens = B.GetAsToken();

            int length = ATokens.Count > BTokens.Count ? BTokens.Count : ATokens.Count;
            int rawTextCount = 0;
            int semanticCount = 0;

            for (int n = 0; n < length; n++)
            {
                if (ATokens[n].Type == Fact.Character.MetaChar.BasicCharType.Comment ||
                    BTokens[n].Type == Fact.Character.MetaChar.BasicCharType.Comment)
                {
                    length--;
                    continue;
                }

                if (ATokens[n].Type == Fact.Character.MetaChar.BasicCharType.String ||
                    BTokens[n].Type == Fact.Character.MetaChar.BasicCharType.String)
                {
                    length--;
                    continue;
                }

                if (ATokens[n].ToString() == BTokens[n].ToString())
                {
                    rawTextCount++;
                    semanticCount++;
                }

                else if (ATokens[n].Type == BTokens[n].Type)
                {
                    semanticCount++;
                }
            }

            float semanticScore = ((float)semanticCount) / ((float)length);
            float rawTextScore = ((float)rawTextCount) / ((float)length);

            if (rawTextScore > 80.0f) { Similarities.Add(new Similarity(A, B, Similarity.Type.Clone)); }
            else if (semanticScore > 80.0f) { Similarities.Add(new Similarity(A, B, Similarity.Type.CopyPaste)); }

            // Subtil plagiarism check

            // First find anchor point in the code that can leads to similarities
            return Similarities;
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Runtime
{
    public class Statistic
    {
        public class TimeValue
        {
            DateTime _At;

            public DateTime At
            {
                get { return _At; }
                set { _At = value; }
            }

            long _Value;

            public long Value
            {
                get { return _Value; }
                set { _Value = value; }
            }
            public TimeValue(DateTime At, long Value)
            {
                _At = At;
                _Value = Value;
            }
        }

        public class TimeLine
        {
            List<TimeValue> _Values = new List<TimeValue>();
            public void Add(DateTime At, long Value)
            {
                if (_Values.Count == 0 || _Values[_Values.Count - 1].At < At)
                {
                    _Values.Add(new TimeValue(At, Value));
                    return;
                }
                for (int n = 0; n < _Values.Count; n++)
                {
                    if (_Values[n].At > At)
                    {
                        _Values.Insert(n, new TimeValue(At, Value));
                        return;
                    }
                }
                _Values.Add(new TimeValue(At, Value));
            }

            public long Last()
            {
                if (_Values.Count == 0) { return 0; }
                if (_Values.Count > 100) 
                {
                    long sum = 0;
                    long count = 0;
					bool notnull = false;
                    for (int n = _Values.Count - 100; n < _Values.Count; n++)
                    {
						if (_Values [n].Value != 0)
						{
							notnull = true;
						}
                        sum += _Values[n].Value;
                        count++;
                    }
					if (notnull && ((sum / count) == 0))
						return 1;
                    return sum / count;
                }
                return _Values[_Values.Count - 1].Value;
            }

			public float FloatLast()
			{
				if (_Values.Count == 0) { return 0; }
				if (_Values.Count > 100) 
				{
					float sum = 0;
					float count = 0;
					bool notnull = false;
					for (int n = _Values.Count - 100; n < _Values.Count; n++)
					{
						if (_Values [n].Value != 0)
						{
							notnull = true;
						}
						sum += _Values[n].Value;
						count++;
					}
					if (notnull && ((sum / count) == 0))
						return 1.0f;
					return sum / count;
				}
				return _Values[_Values.Count - 1].Value;
            }
        }

        long _MemoryUsedPeak = 0;

        public long MemoryUsedPeak
        {
            get { return _MemoryUsedPeak; }
            set { _MemoryUsedPeak = value; }
        }

        TimeLine _MemoryUsed = new TimeLine();

        public TimeLine MemoryUsed
        {
            get { return _MemoryUsed; }
        }

        long _SwapUsedPeak = 0;

        public long SwapUsedPeak
        {
            get { return _SwapUsedPeak; }
            set { _SwapUsedPeak = value; }
        }

        TimeLine _SwapUsed = new TimeLine();
        public TimeLine SwapUsed
        {
            get { return _SwapUsed; }
        }

        TimeLine _ThreadCount = new TimeLine();
        public TimeLine ThreadCount
        {
            get { return _ThreadCount; }
        }

        TimeLine _WaitingThreadCount = new TimeLine();
        public TimeLine WaitingThreadCount
        {
            get { return _WaitingThreadCount; }
        }

        long _TotalUserProcessorTime = 0;
        public long TotalUserProcessorTime
        {
            get { return _TotalUserProcessorTime; }
            set { _TotalUserProcessorTime = value; }

        }

        TimeLine _UserProcessorTime = new TimeLine();

        public TimeLine UserProcessorTime
        {
            get { return _UserProcessorTime; }
        }

        TimeLine _SystemProcessorTime = new TimeLine();

        public TimeLine SystemProcessorTime
        {
            get { return _SystemProcessorTime; }
        }

        long _TotalSystemProcessorTime = 0;
        public long TotalSystemProcessorTime
        {
            get { return _TotalSystemProcessorTime; }
            set { _TotalSystemProcessorTime = value; }

        }
    }
}

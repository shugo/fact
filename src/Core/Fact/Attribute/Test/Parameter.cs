﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Attribute.Test
{
    [AttributeUsage(AttributeTargets.Field)]
    public class Parameter : System.Attribute
    {
        string _Name = "";
        public string Name { get { return _Name; } }
        public Parameter(string Name) { _Name = Name; }
    }
}

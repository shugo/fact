﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Attribute.Test
{
    [AttributeUsage(AttributeTargets.Field)]
    class Internal_ParentAccessTag : System.Attribute
    {
    }
    public class ParentAccess<T> where T : class
    {
        /// <summary>
        /// This is for an internal use Only. YOU MUST NOT SET OR ACCESS THIS FIELD DIRECTLY.
        /// Everything could happen if you touch it.
        /// </summary>
        [Internal_ParentAccessTag()]
        protected T _Parent = null;
        public T Parent { get { return _Parent; } }
    }

}

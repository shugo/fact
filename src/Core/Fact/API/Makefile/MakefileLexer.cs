﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Fact.API.Makefile
{
    public class Token
    {
        public TokenType type;
        public string value;

        public override string ToString()
        {
            return ("{ type: " + type.ToString() + ", value: " + value + " }");
        }
    }

    public enum TokenType
    {
        Word,
        RuleStatementSeparator,
        WhiteSpaces,
        Unknown,
        AssignmentSymbol,
        RuleName,
        RuleSymbol,
        RuleStatement,
        RuleDependency,
        AssignmentValue,
        Inclusion,
        NewLine,
        AssignmentID,
        RuleID,
        IncludeID,
        Variable
    }

    public class MakefileLexer
    {
        private string _text = "";
        private int _offset = 0;

        public string ExtractCurrentLine()
        {
            try
            {
                // Extract only the current line
                int n = _offset;
                StringBuilder builder = new StringBuilder();
                for (; n >= 0; n--)
                {
                    if (_text[n] == '\n' || _text[n] == '\r') { n++; break; }
                }

                for (; n < _text.Length; n++)
                {
                    if (_text[n] == '\n' || _text[n] == '\r') { break; }
                    builder.Append(_text[n]);
                }
                return builder.ToString();
            }
            catch
            {
                return "";
            }
        }

        public bool EOF
        {
            get { return _offset >= _text.Length; }
        }

        public MakefileLexer(string text)
        {
            // do some proprocessing
            Regex commentStripper = new Regex(@"#.*\n");
            Regex espacedNewline = new Regex(@"\\\n", RegexOptions.Multiline);
            _text = commentStripper.Replace(text, "");
            _text = espacedNewline.Replace(_text, "");
        }

        private Regex GetRegex(TokenType type)
        {
            switch (type)
            {
                    // New grammar, will try to be more convenient than the old one
                case TokenType.AssignmentID:
                    return new Regex(@"\G[^\n=:+?]+(?=[:+?]?[=])");
                case TokenType.RuleID:
                    return new Regex(@"\G[^\n:]+(?=:)");
                case TokenType.IncludeID:
                    return new Regex(@"\G[-]?include");
                case TokenType.Variable:
                    return new Regex(@"(?<=\$[({])[^)}](?=[)}])");
                case TokenType.AssignmentSymbol:
                    return new Regex(@"\G[:+?]?[=]");
                case TokenType.RuleSymbol:
                    return new Regex(@"\G[:]?[:]");

                case TokenType.WhiteSpaces:
                    return new Regex(@"\G\ +", RegexOptions.Multiline);
                case TokenType.NewLine:
                    return new Regex(@"\G\n", RegexOptions.Multiline);
                    // Old grammar, most of it will disapear, useful part will be moved up
                case TokenType.Word:
                    return new Regex(@"\G[-.$,(){}\w/\\]+");
                case TokenType.AssignmentValue:
                    return new Regex(@"\G.*\n", RegexOptions.Multiline);
                case TokenType.RuleDependency:
                    return new Regex(@"\G.*[\n;]", RegexOptions.Singleline);
                case TokenType.RuleStatement:
                    return new Regex(@"\G\t+.*\r?\n+");
                case TokenType.RuleStatementSeparator:
                    return new Regex(@"\G(;|\r?\n)+", RegexOptions.Multiline);
                default:
                    throw new Exception("Unexpected token, maybe you forgot to handle a newly added token");
            }
        }
        /// <summary>
        /// Only used for fast dev, this function is not intended to remain in the parser
        /// </summary>
        /// <param name="upTo"></param>
        /// <returns></returns>
        public Token FastForward(TokenType upTo)
        {
            Regex regex = GetRegex(upTo);
            StringBuilder sb = new StringBuilder();
            while (!EOF && !regex.Match(_text, _offset).Success)
            {
                sb.Append(_text[_offset++]);
            }
            return new Token {type = TokenType.Unknown, value = sb.ToString()};
        }
        /// <summary>
        /// Get the next matching expected token while skipping unwanted token
        /// </summary>
        /// <param name="expect">Expected token</param>
        /// <param name="skip">Token to skip</param>
        /// <returns></returns>
        public Token GetToken(IEnumerable<TokenType> expect, IEnumerable<TokenType> skip)
        {
            // always skip white space
            _offset += GetRegex(TokenType.WhiteSpaces).Match(_text, _offset).Length;
            if (skip != null ) foreach (TokenType type in skip)
            {
                Regex regex = GetRegex(type);
                _offset += regex.Match(_text, _offset).Length;
            }
            foreach (TokenType type in expect)
            {
                Regex regex = GetRegex(type);
                Match match = regex.Match(_text, _offset);
                if (match.Success)
                {
                    _offset += match.Length;
                    return new Token {type = type, value = match.Value};
                }
            }
            return null;
        }
        public Token GetToken(IEnumerable<TokenType> expect)
        {
            return GetToken(expect, null);
        }
        /// <summary>
        /// Get a Token of token type while skipping whitespace
        /// </summary>
        /// <param name="tokenType"></param>
        /// <returns></returns>
        public Token GetToken(TokenType tokenType)
        {
            return GetToken(new[]{tokenType}, null);
        }
        public Token GetToken(TokenType tokenType, TokenType skipTokenType)
        {
            return GetToken(new[] {tokenType}, new[] {skipTokenType});
        }
    }

}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using File = Fact.Processing.File;

namespace Fact.API.Makefile
{
    /// <summary>
    /// A makefile API for Fact
    /// </summary>
    public class Makefile
    {
        private List<MakefileEntry> _entries = new List<MakefileEntry>();
        private File _makefileFile;

        public IEnumerable<MakefileEntry> Entries
        {
            get { return _entries; }
        }

        public IEnumerable<MakefileRule> Rules
        {
            get { return _entries.OfType<MakefileRule>(); }
            set {
                _entries.RemoveAll(entry => entry is MakefileRule);
                _entries = _entries.Union(value.Cast<MakefileEntry>()).ToList();
            }
        }

        public IEnumerable<MakefileVariable> Variables
        {
            get { return _entries.OfType<MakefileVariable>(); }
            set
            {
                _entries.RemoveAll(entry => entry is MakefileVariable);
                _entries = _entries.Union(value.Cast<MakefileEntry>()).ToList();
            }
        }

        public File MakefileFile
        {
            get { return _makefileFile; }
        }

        public Makefile(File makefileFile)
        {
            _makefileFile = makefileFile;
            Load(_makefileFile);
        }

        private void Load(File file)
        {
            //Console.WriteLine("Loading file " + file.Name);
            //Console.WriteLine();
            MakefileParser parser = new MakefileParser(this);
            parser.Parse();
        }

        public void AddEntry(MakefileEntry entry)
        {
            _entries.Add(entry);

            MakefileRule rule = entry as MakefileRule;
            if (rule != null)
                rule.Statements.ForEach(stat => GetVariableReference(stat));
        }

        private string NaiveExpansion(string text, Dictionary<string, MakefileVariable> vars)
        {
            Regex regexVars = new Regex(@"\$[({][^)}]*[)}]");
            while (regexVars.IsMatch(text))
            {
                foreach (string valref in GetVariableReference(text))
                    // only get the first reference found since the replacement can contain references
                {
                    Regex regex = new Regex(@"\$[{(]" + valref + "[})]");
                    text = regex.Replace(text, vars.ContainsKey(valref) ? vars[valref].Value : "");
                    break;
                }
            }
            return text;
        }

        /// <summary>
        /// Get the expanded version of the variables according to the this.Variables
        /// definition
        /// </summary>
        /// <returns>List of expanded makefile variable</returns>
        public IEnumerable<MakefileVariable> GetExpandedVariables()
        {
            Dictionary<string, MakefileVariable> expVariables = new Dictionary<string, MakefileVariable>();
            foreach (MakefileVariable variable in Variables)
            {
                if (!expVariables.ContainsKey(variable.Name))
                    expVariables[variable.Name] = new MakefileVariable(variable.Name) { Symbol = "=", Value = ""};
                switch (variable.Symbol)
                {
                    case "=": // lazy variable expansion
                        expVariables[variable.Name].Value = variable.Value;
                        break;
                    case ":=": // strict variable expansion
                        expVariables[variable.Name].Value = NaiveExpansion(variable.Value, expVariables);
                        break;
                    case "+=": // ... append
                        expVariables[variable.Name].Value += variable.Value;
                        break;
                }
            }
            return expVariables.Select(kv => kv.Value)
                .Select(var =>
                            {
                                var.Value = NaiveExpansion(var.Value, expVariables);
                                return var;
                            });
        }

        public IEnumerable<MakefileRule> GetExpandedRules()
        {
            Dictionary<string, MakefileVariable> expandedVarsDic = GetExpandedVariables().ToDictionary(var => var.Name);
            List<MakefileRule> rules = new List<MakefileRule>();
            foreach (MakefileRule rule in Rules)
            {
                MakefileRule expendedRule = new MakefileRule(rule.Name){Symbol = rule.Symbol};
                // expand dependency
                foreach (string dependencie in rule.Dependencies)
                    foreach (string dep in NaiveExpansion(dependencie, expandedVarsDic).Split(' '))
                        expendedRule.Dependencies.Add(dep);
                // expand statements
                foreach (string stat in rule.Statements)
                    expendedRule.Statements.Add(NaiveExpansion(stat, expandedVarsDic));
                rules.Add(expendedRule);
            }
            return rules;
        }

        /// <summary>
        /// Find list of variable reference contained in a text
        /// </summary>
        /// <param name="text">Input text</param>
        /// <returns>List of reference</returns>
        private IEnumerable<string> GetVariableReference(string text)
        {
            Regex refRegex = new Regex(@"\$" +
                                       "(?<open>[({])" +
                                       "([^{()}]+?)"+
                                       "(?<-open>[)}])" +
                                       "(?(open)(?!))");
            MatchCollection matches = refRegex.Matches(text);
            List<string> references = new List<string>();
            foreach (Match match in matches)
                references.Add(match.Groups[1].Captures[0].Value);
            return references.Distinct();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (MakefileEntry entry in Entries)
                sb.AppendLine(entry.ToString());
            return sb.ToString();
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Fact.API.Makefile
{
    public class MakefileParser
    {
        private Makefile _makefile;
        private MakefileLexer lexer;
        public MakefileParser(Makefile makefile)
        {
            _makefile = makefile;
        }

        // _ : Entry* EOF
        // Entry : (Rule | Assignement | Inclusion)
        // Rule : Word ":" Word* Separator RuleStatement*
        // Assignement : Word "=" Word* Separator
        // Inclusion : Word Word*
        public void Parse()
        {
            lexer = new MakefileLexer(_makefile.MakefileFile.GetText(false));
            while (!lexer.EOF) ParseEntry();
        }

        private void ParseEntry()
        {
            // skip new line and white space
            while (lexer.GetToken(new[] { TokenType.NewLine, TokenType.WhiteSpaces }) != null) continue;
            // First we look for the entry type:
            TokenType[] idType = new[] {TokenType.AssignmentID, TokenType.RuleID, TokenType.IncludeID};
            Token idTok = lexer.GetToken(idType);
            if (idTok == null && lexer.EOF) { return; }
            if (idTok == null)
            {
                string line = lexer.ExtractCurrentLine().Trim();
                if (line != "")
                {
                    throw new Exception("Unexpected token at line: '" + line + "'");
                }
                else
                {
                    throw new Exception("Unexpected token");
                }
            }
            switch (idTok.type)
            {
                case TokenType.AssignmentID:
                    MakefileVariable variable = new MakefileVariable(idTok.value.Trim());
                    ParseAssignement(variable);
                    _makefile.AddEntry(variable);
                    break;
                case TokenType.RuleID:
                    MakefileRule rule = new MakefileRule(idTok.value.Trim());
                    ParseRule(rule);
                    _makefile.AddEntry(rule);
                    break;
                case TokenType.IncludeID:
                    MakefileInclude include = new MakefileInclude();
                    include.IsSilent = idTok.value.StartsWith("-");
                    for (Token includeFilename = lexer.GetToken(TokenType.Word);
                         includeFilename != null;
                         includeFilename = lexer.GetToken(TokenType.Word))
                        include.AddIncludeFilename(includeFilename.value);
                    _makefile.AddEntry(include);
                    break;
                default:
                    throw new Exception("Unexpected token");
            }
        }

        private void ParseAssignement(MakefileVariable variable)
        {
            Token sym = lexer.GetToken(TokenType.AssignmentSymbol);
            variable.Symbol = sym.value;
            variable.Value = lexer.GetToken(TokenType.AssignmentValue).value.Trim();
        }

        private void ParseRule(MakefileRule rule)
        {
            Token sym = lexer.GetToken(TokenType.RuleSymbol);
            rule.Symbol = sym.value;
            Token dependencies = lexer.GetToken(TokenType.RuleDependency);
            rule.RawDependency = dependencies.value;
            rule.Dependencies.AddRange(
                dependencies.value.Trim().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(dep => dep.Trim()));           
            while (true)
            {
                Token statement = lexer.GetToken(new[] {TokenType.RuleStatement}, new TokenType[]{}); // do not skip whitespace
                if (statement == null) break;
                rule.RawStatement += statement.value;
                rule.Statements.Add(statement.value.Trim());
            }
        }
    }
}

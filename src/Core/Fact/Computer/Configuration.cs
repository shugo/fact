﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Computer
{
    public class Configuration
    {
        List<Devices.CPU> _CPUs = new List<Devices.CPU>();
        public List<Devices.CPU> CPUs { get { return _CPUs; } }
        List<Devices.PCIDevice> _PCIDevices = new List<Devices.PCIDevice>();
        public List<Devices.PCIDevice> PCIDevices { get { return _PCIDevices; } }
        List<Devices.Disk> _Disks = new List<Devices.Disk>();
        public List<Devices.Disk> Disks { get { return _Disks; } }
        static Configuration()
        {
            _Configuration = _GetCurrentConfig();
        }

        static Configuration _GetCurrentConfig()
        {
            Configuration config = null;
            switch (Internal.Information.CurrentOperatingSystem() )
            {
                case Internal.Information.OperatingSystem.MicrosoftWindows: config = _GetCurrentConfigWindows(); break;
                case Internal.Information.OperatingSystem.Unix: config = _GetCurrentConfigUnix(); break;
            }
            if (config == null) { config = _GetCurrentConfigGeneric(); }
            return config;
        }

        static Configuration _GetCurrentConfigWindows()
        {
            Configuration config = new Configuration();
            config._CPUs = Devices.CPU._ListCPUWindows();
            config._PCIDevices = Devices.PCIDevice._ListPCIDevicesWindows();
            config._Disks = Devices.Disk._ListDiskDevicesWindows();
            return config;
        }

        static Configuration _GetCurrentConfigUnix()
        {
            return null;
        }

        static Configuration _GetCurrentConfigGeneric()
        {
            Configuration Configuration = new Configuration();
            return Configuration;
        }

        static Configuration _Configuration = null;
        public static Configuration Current
        {
            get
            {
                return _Configuration;
            }
        }

        internal void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteInt32(Stream, _CPUs.Count);
            for (int n = 0; n < _CPUs.Count; n++)
            {
                _CPUs[n].Save(Stream);
            }
            Internal.StreamTools.WriteInt32(Stream, _PCIDevices.Count);
            for (int n = 0; n < _PCIDevices.Count; n++)
            {

            }
        }

        internal Configuration Load(System.IO.Stream Stream)
        {
            Configuration config = new Configuration();
            return config;
        }
    }
}

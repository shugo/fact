﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Computer.Devices
{
    public class PCIDevice
    {
        PCIDeviceVendor _Vendor = PCIDeviceVendor.UNKNOWN;
        public PCIDeviceVendor Vendor { get { return _Vendor; } }
        internal string _Name = "";
        public string Name { get { return _Name; } }
        internal int _DeviceID = 0;
        public int DeviceID { get { return _DeviceID; } }
        internal string _Subsys = "";
        internal string _Rev = "";
        public enum PCIDeviceVendor
        {
            UNKNOWN,
            _3DFX,
            ACER,
            AGEIA,
            AMD,
            APPLE,
            ASUS,
            BROADCOM,
            HEWLETT_PACKARD,
            IBM,
            INTEL,
            LOGITECH,
            NEC,
            NVIDIA,
            SAMSUNG,
            SILICON_GRAPHICS,
            SONY,
            TEXAS_INSTRUMENTS,
            TOSHIBA,
        }

        static PCIDeviceVendor VendorFromName(string vendor)
        {
            switch (vendor.ToUpper())
            {
                case "0402": return PCIDeviceVendor.ACER;
                case "1002":
                case "1971": return PCIDeviceVendor.AGEIA;
                case "1022": return PCIDeviceVendor.AMD;
                case "0AC8": return PCIDeviceVendor.ASUS;
                case "106B":
                case "05AC": return PCIDeviceVendor.APPLE;
                case "0A5C":
                case "14E4": return PCIDeviceVendor.BROADCOM;
                case "10C3":
                case "A259": return PCIDeviceVendor.HEWLETT_PACKARD;
                case "04B3": return PCIDeviceVendor.IBM;
                case "8087":
                case "8086": return PCIDeviceVendor.INTEL;
                case "046D": return PCIDeviceVendor.LOGITECH;
                case "1033": return PCIDeviceVendor.NEC;
                case "10DE": return PCIDeviceVendor.NVIDIA;
                case "144D": return PCIDeviceVendor.SAMSUNG;
                case "10A9": return PCIDeviceVendor.SILICON_GRAPHICS;
                case "A304": return PCIDeviceVendor.SONY;
                case "104C": return PCIDeviceVendor.TEXAS_INSTRUMENTS;
                case "13D7":
                case "1179":
                case "102F":
                case "0B05": return PCIDeviceVendor.TOSHIBA;
                case "121A": return PCIDeviceVendor._3DFX;
            }
            return PCIDeviceVendor.UNKNOWN;
        }


        static internal List<PCIDevice> _ListPCIDevicesWindows()
        {
            List<PCIDevice> result = new List<PCIDevice>();
            List<string> devices = Win32CfgMrg.ListDevices();
            foreach (string device in devices)
            {
                if (device.ToLower().StartsWith("pci\\"))
                {
                    PCIDevice dev = new PCIDevice();
                    result.Add(dev);
                    string corepart = "";
                    for (int n = "pci\\".Length; n < device.Length && device[n] != '\\'; n++) { corepart = corepart + device[n]; }
                    string[] items = corepart.Split('&');
                    string vendor = "";
                    string devid = "";
                    string subsys = "";
                    string rev = "";



                    foreach (string item in items)
                    {
                        if (item.ToLower().StartsWith("ven_")) { vendor = item.Substring("ven_".Length); }
                        if (item.ToLower().StartsWith("dev_")) { devid = item.Substring("dev_".Length); }
                        if (item.ToLower().StartsWith("subsys_")) { subsys = item.Substring("subsys_".Length); }
                        if (item.ToLower().StartsWith("rev_")) { rev = item.Substring("rev_".Length); }



                    }
                    dev._Vendor = VendorFromName(vendor);
                    dev._DeviceID = int.Parse(devid, System.Globalization.NumberStyles.HexNumber);
                    dev._Name = corepart;
                    dev._Subsys = subsys;
                    dev._Rev = rev;

                    byte[] basicConfigVector = Win32BaseConfigVector.GetBasicConfigVector("ven_" + vendor + "&dev_" + devid + "&subsys_" + subsys + "&rev_" + rev);
                    if (basicConfigVector != null)
                    {
                        Win32BaseConfigVector.ParseBasisConfigVector(basicConfigVector);
                    }
                }
            }
            return result;
        }

        public override string ToString()
        {
            return _Name;
        }
    }
}

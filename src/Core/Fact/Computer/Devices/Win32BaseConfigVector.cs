﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Computer.Devices
{
    static class Win32BaseConfigVector
    {
        internal static byte[] GetBasicConfigVector(string FullDeviceName)
        {
            FullDeviceName = FullDeviceName.ToLower().Trim();
            try
            {
                Microsoft.Win32.RegistryKey PCIs = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Enum\PCI");
                string[] pciIndx = PCIs.GetSubKeyNames();
                foreach (string device in pciIndx)
                {
                    string fdevice = device.ToLower().Trim();
                    if (fdevice == FullDeviceName)
                    {
                        Microsoft.Win32.RegistryKey devkey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Enum\PCI\" + device);
                        string[] subkeyIndx = devkey.GetSubKeyNames();

                        foreach (string subkey in subkeyIndx)
                        {
                            try
                            {
                                Microsoft.Win32.RegistryKey logkey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Enum\PCI\" + device + @"\" + subkey);
                                string[] logkeyIndx = logkey.GetSubKeyNames();
                                foreach (string log in logkeyIndx)
                                {
                                    if (log.ToLower() == "logconf")
                                    {
                                        Microsoft.Win32.RegistryKey logconfkey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Enum\PCI\" + device + @"\" + subkey + @"\" + log);
                                        try
                                        {
                                            // FIX ME
                                            return null;
                                        }
                                        catch { }
                                    }
                                }
                            }
                            catch { }
                        }
                    }
                }
            }
            catch { }
            return null;
        }

        internal static List<ResourceDescriptor> ParseBasisConfigVector(byte[] Vector)
        {
            List<ResourceDescriptor> descriptor = new List<ResourceDescriptor>();

            return descriptor;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Computer.Devices
{
    static class Win32CfgMrg
    {
        internal const int DIGCF_ALLCLASSES = 0x00000004;
        internal const int DIGCF_PRESENT = 0x00000002;

        static IntPtr CM_Get_Device_ID_List_Size;
        static IntPtr CM_Get_Device_ID_List;

        static IntPtr CM_FindFirstVolumeW;
        static IntPtr CM_FindNextVolumeW;


        delegate int Get_Device_ID_List_Size(IntPtr a, IntPtr b, int c);
        delegate int Get_Device_ID_List(IntPtr a, IntPtr b, IntPtr c, int d);

        delegate IntPtr FindFirstVolumeW(IntPtr a, int b);
        delegate IntPtr FindNextVolumeW(IntPtr a, IntPtr b, int c);



        static Get_Device_ID_List_Size dGet_Device_ID_List_Size;
        static Get_Device_ID_List dGet_Device_ID_List;
        static FindFirstVolumeW dFindFirstVolumeW;

        static Win32CfgMrg()
        {
            IntPtr ptr = Internal.Os.Generic.OpenLibrary("C:/Windows/System32/CfgMgr32.dll");
            Fact.Reflection.Assembly assembly = Fact.Reflection.Assembly.LoadFromFile("C:/Windows/System32/CfgMgr32.dll");
            CM_Get_Device_ID_List_Size = Internal.Os.Generic.GetMethod(ptr, "CM_Get_Device_ID_List_SizeA");
            CM_Get_Device_ID_List = Internal.Os.Generic.GetMethod(ptr, "CM_Get_Device_ID_ListA");
            dGet_Device_ID_List_Size = (Get_Device_ID_List_Size)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(CM_Get_Device_ID_List_Size, typeof(Get_Device_ID_List_Size));
            dGet_Device_ID_List = (Get_Device_ID_List)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(CM_Get_Device_ID_List, typeof(Get_Device_ID_List));
            ptr = Internal.Os.Generic.OpenLibrary("C:/Windows/System32/kernel32.dll");
            CM_FindFirstVolumeW = Internal.Os.Generic.GetMethod(ptr, "FindFirstVolumeW");
            CM_FindNextVolumeW = Internal.Os.Generic.GetMethod(ptr, "FindFirstVolumeW");
            dFindFirstVolumeW = (FindFirstVolumeW)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(CM_FindFirstVolumeW, typeof(FindFirstVolumeW));
        }

        static unsafe internal List<string> ListDevices()
        {
            List<string> result = new List<string>();
            ulong size = 0;

            dGet_Device_ID_List_Size(new IntPtr(&size), new IntPtr(0), 0);
            if (size > 1)
            {
                byte* buffer = (byte*)System.Runtime.InteropServices.Marshal.AllocHGlobal((int)size).ToPointer();
                byte* ptr = buffer;
                dGet_Device_ID_List(new IntPtr(0), new IntPtr(buffer), new IntPtr((long)size), 0);
                for (ulong n = 0; n < size; n++)
                {
                    if (buffer[n] == '\0')
                    {
                        result.Add(System.Text.Encoding.ASCII.GetString(Fact.Reflection.Tools.ReadCStr(ptr)));
                        ptr = buffer + n + 1;
                    }
                }
            }
            
            return result;
        }

        static unsafe internal string FindFirstVolume()
        {
            int size = 1024;
            byte* buffer = (byte*)System.Runtime.InteropServices.Marshal.AllocHGlobal((int)size).ToPointer();
            IntPtr handle = dFindFirstVolumeW(new IntPtr(buffer), size);
            return "";

        }
    }
}

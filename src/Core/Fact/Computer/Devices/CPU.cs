﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Computer.Devices
{
    public class CPU
    {
        public enum Architecture
        {
            ARM,
            ARM64,
            x86,
            x86_64,
            Itanium,
            Mips,
            Sparc,
            Power,
            Power64,
            Any,
            Unknown
        }

        public enum Vendor
        {
            AMD,
            INTEL,
            IBM,
            NVIDIA,
            TEXAS_INSTRUMENTS,
            QUALCOM,
            SAMSUNG,
            Any,
            Unknown
        }

        string _Name = ""; public string Name { get { return _Name; } }
        Vendor _Vendor = Vendor.Unknown; public Vendor CPUVendor { get { return _Vendor; } }
        Architecture _Architecture = Architecture.Unknown; public Architecture CPUArchitecture { get { return _Architecture; } }

        int _Frequency = 0; public int Frequency { get { return _Frequency; } }
        internal static List<CPU> _ListCPUWindows()
        {
            List<CPU> result = new List<CPU>();
            Microsoft.Win32.RegistryKey CPUs = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor");
            if (CPUs != null)
            {
                string[] cpuIndx = CPUs.GetSubKeyNames();
                foreach (string cpu in cpuIndx)
                {
                    CPU cpuObject = new CPU();
                    Microsoft.Win32.RegistryKey cpuKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor\" + cpu);
                    if (cpuKey != null)
                    {
                        string name = (string)cpuKey.GetValue("ProcessorNameString", "");
                        string vendorName = (string)cpuKey.GetValue("VendorIdentifier", "").ToString();
                        int Frequency = (int)cpuKey.GetValue("~MHz", 0);
                        cpuObject._Name = name;

                        if (vendorName.ToLower().Contains("intel")) { cpuObject._Vendor = Vendor.INTEL; }
                        else if (vendorName.ToLower().Contains("ibm")) { cpuObject._Vendor = Vendor.IBM; cpuObject._Architecture = Architecture.Power64; }
                        else if (vendorName.ToLower().Contains("nv")) { cpuObject._Vendor = Vendor.NVIDIA; cpuObject._Architecture = Architecture.ARM64; }
                        else if (vendorName.ToLower().Contains("amd")) { cpuObject._Vendor = Vendor.AMD; }
                        else if (vendorName.ToLower().Contains("ti")) { cpuObject._Vendor = Vendor.TEXAS_INSTRUMENTS; }
                        else if (vendorName.ToLower().Contains("qualcom")) { cpuObject._Vendor = Vendor.QUALCOM; }
                        else if (vendorName.ToLower().Contains("samsung")) { cpuObject._Vendor = Vendor.SAMSUNG; }
                        cpuObject._Frequency = Frequency;
                        result.Add(cpuObject);
                    }
                }
            }
            return result;
        }

        internal void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, _Name);
            Internal.StreamTools.WriteInt32(Stream, _Frequency);
            Internal.StreamTools.WriteInt32(Stream, (int)_Vendor);
        }
    }
}

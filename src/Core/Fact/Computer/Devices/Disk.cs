﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Computer.Devices
{
    public class Disk
    {
        string _Name = "";
        string Name { get { return _Name; } }
        public Disk()
        {

        }


        static internal List<Disk> _ListDiskDevicesWindows()
        {
            List<Disk> result = new List<Disk>();
            List<string> devices = Win32CfgMrg.ListDevices();
            foreach (string device in devices)
            {
                if (device.ToLower().StartsWith("storage\\volume\\"))
                {
                    string devname = device.ToLower().Substring("storage\\volume\\".Length);
                    Disk dev = new Disk();
                    dev._Name = devname;
                    result.Add(dev);
                    string corepart = "";

                }
            }
            return result;
        }

        public override string ToString()
        {
            return _Name;
        }
    }
}

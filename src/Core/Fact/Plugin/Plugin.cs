﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Plugin
{
    public abstract class Plugin
    {
        string _localDirectory = "";

        public Plugin() { }

        public virtual string Name { get { return "Plugin"; } }

        /// <summary>
        /// Allow a plugin to load annother plugin.
        /// </summary>
        /// <param name="Dependency">The name of the plugin that must be loaded</param>
        /// <returns>The loaded depency or null if an error occurs</returns>
        protected Plugin LoadDependency(string Dependency)
        {
            Plugin result;
            try
            {
                result = LoadFromFile(_localDirectory + Dependency + ".dll", System.IO.Path.GetFileName(Dependency));
                if (result != null) { return result; }
            } catch { }
            try
            {
                result = LoadFromFile(_localDirectory + "/" + Dependency + ".dll", System.IO.Path.GetFileName(Dependency));
                if (result != null) { return result; }
            } catch { }
            try
            {
                result = LoadFromFile(_localDirectory + Dependency, System.IO.Path.GetFileNameWithoutExtension(Dependency));
                if (result != null) { return result; }
            } catch { }
            try
            {
                result = LoadFromFile(_localDirectory + "/" + Dependency, System.IO.Path.GetFileNameWithoutExtension(Dependency));
                if (result != null) { return result; }
            } catch { }
            try
            {
                result = LoadFromFile(Dependency, System.IO.Path.GetFileName(Dependency));
                if (result != null) { return result; }
            } catch { }
            try
            {
                result = LoadFromFile(Dependency, System.IO.Path.GetFileNameWithoutExtension(Dependency));
                if (result != null) { return result; }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Run the plugin with the specified arguments.
        /// </summary>
        /// <param name="args">The arguments passed to the plugin.</param>
        /// <returns>The error code.</returns>
        public virtual int Run(params string[] args)
        { return 0; }

        public virtual void RunAsync(params string[] args)
        {
            try
            {
                System.Threading.Thread _Thread = new System.Threading.Thread(
                    () => { Run(args); });
                _Thread.IsBackground = true;
                _Thread.Start();
            }
            catch { }
        }

        /// <summary>
        /// Load all the class that inherith from Plugin inside the specified library.
        /// </summary>
        /// <param name="File">The name of the Library</param>
        /// <returns>The list of all the plugins contained in the library</returns>
        public static List<Plugin> LoadFromFile(string File)
        {
            return LoadFromFile(File, false);
        }

        static List<Plugin> LoadFromFile(string File, bool subcall)
        {
            System.IO.FileInfo Info = new System.IO.FileInfo(File);
            string fullFile = Info.FullName;
            string fullFileLower = Info.DirectoryName + "/" + Info.Name.ToLower();
            List<Plugin> list = new List<Plugin>();

            Type pluginType = typeof(Plugin);



            if (System.IO.File.Exists(fullFile))
            {
                try
                {
                    System.Reflection.Assembly Assembly;
                    try
                    {
                        Assembly = System.Reflection.Assembly.LoadFile(fullFile);
                    }
                    catch
                    {
                        Assembly = System.Reflection.Assembly.LoadFile(fullFileLower);
                    }
                    foreach (System.Reflection.Module module in Assembly.GetModules())
                    {
                        foreach (System.Type type in module.GetTypes())
                        {
                            if (type.IsSubclassOf(pluginType))
                            {
                                try
                                {
                                    list.Add((Plugin)type.GetConstructor(new Type[0]).Invoke(new Object[0]));
                                    try
                                    {
                                        list[list.Count - 1]._localDirectory = System.IO.Path.GetDirectoryName(fullFile);
                                    }
                                    catch { }
                                }
                                catch { }
                            }
                        }
                    }
                }
                catch (Exception e) { }
            }
            else
            {
                if (File != File.ToLower()) { List<Plugin> plugins = LoadFromFile(File.ToLower(), subcall); if (plugins.Count != 0) { return plugins; } }
                if (!File.EndsWith(".dll")) { List<Plugin> plugins = LoadFromFile(File + ".dll", subcall); if (plugins.Count != 0) { return plugins; } }
            }
            if (list.Count == 0)
            {
                
                string directory = Info.Directory.FullName;
                if (System.IO.Directory.Exists(directory)) 
                foreach (string file in System.IO.Directory.GetFiles(directory))
                {
                    if (file.EndsWith(".dll"))
                    {
                        try
                        {
                            System.Reflection.Assembly Assembly;
                            try
                            {
                                Assembly = System.Reflection.Assembly.LoadFile(file);
                            }
                            catch
                            {
                                Assembly = System.Reflection.Assembly.LoadFile(file.ToLower());
                            }
                            foreach (System.Reflection.Module module in Assembly.GetModules())
                            {
                                foreach (System.Type type in module.GetTypes())
                                {
                                    if (type.IsSubclassOf(pluginType))
                                    {
                                        try
                                        {
                                            list.Add((Plugin)type.GetConstructor(new Type[0]).Invoke(new Object[0]));
                                            try
                                            {
                                                list[list.Count - 1]._localDirectory = System.IO.Path.GetDirectoryName(file);
                                            }
                                            catch { }
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        catch { }
                    }
                }
            }
            if (list.Count == 0 && !subcall) 
            {
                try
                {
                    foreach (System.Reflection.Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                    {
                        try
                        {
                            string dir = System.IO.Path.GetDirectoryName(assembly.Location);
                            if (!dir.EndsWith("/")) { dir += "/"; }
                            List<Plugin> plugins = LoadFromFile(dir + File, true);
                            if (plugins.Count > 0) { return plugins; }
                        }
                        catch { }
                    }
                }catch{}
            }
            return list;
        }

        /// <summary>
        /// Load the specified plugin from a library.
        /// </summary>
        /// <param name="File">The name of the library.</param>
        /// <param name="Name">The name of the plugin.</param>
        /// <returns>The plugin or null if no plugin has been found.</returns>
        public static Plugin LoadFromFile(string File, string Name)
        {
            List<Plugin> list = LoadFromFile(File);
            foreach (Plugin plugin in list)
            {
                if (plugin.Name == Name)
                {
                    return plugin;
                }
            }

            string lowerName = Name.ToLower();

            foreach (Plugin plugin in list)
            {
                if (plugin.Name.ToLower() == lowerName)
                {
                    return plugin;
                }
            }

            return null;
        }
    }
}

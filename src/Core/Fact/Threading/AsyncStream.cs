﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Threading
{
    class AsyncStream : System.IO.Stream
    {
        byte[] cache = new byte[4096];
        int cacheOffset = 0;
        int cacheSize = 0;
        long streamSize = 0;
        long streamPos = 0;


        int ReadFromCache(byte[] buffer, int offset, int count)
        {
            if (cacheOffset - cacheSize == 0) { return 0; }
            if (count >= (cacheSize - cacheOffset))
            {
                for (int n = cacheOffset, x = offset; n < cacheSize; n++, x++)
                {
                    buffer[x] = cache[n];
                }
                int size = cacheSize - cacheOffset;
                cacheOffset = 0;
                cacheSize = 0;
                return size;
            }
            else
            {
                for (int n = cacheOffset, x = offset; n < count + cacheOffset; n++, x++)
                {
                    buffer[x] = cache[n];
                }
                cacheOffset += count;
                return count;
            }
        }

        System.IO.Stream _Stream = null;
        public AsyncStream(System.IO.Stream Stream)
        {
            _Stream = Stream;
            streamSize = 0;
        }

        public AsyncStream(System.IO.Stream Stream, long Size)
        {
            _Stream = Stream;
            streamSize = Size;
        }

        public override void Close()
        {
            _Stream.Close();
        }

        public override bool CanRead
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanSeek
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanWrite
        {
            get { throw new NotImplementedException(); }
        }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        void ReadAsync(byte[] buffer, int offset, int count, Job.Action Callback)
        {
            Job.CreateJob(
            () =>
            {
                if (count <= 4096)
                {
                    int length = _Stream.Read(buffer, offset, count);
                    streamPos += length;
                    if (length >= count)
                    {
                        if (Callback != null) { Callback(); }
                    }
                    else
                    {
                        ReadAsync(buffer, offset + length, count - length, Callback);
                    }
                }
                else
                {
                    int length = _Stream.Read(buffer, offset, 4096);
                    ReadAsync(buffer, offset + length, count - length, Callback);
                }
                ;
            });
        }

        public void Read(byte[] buffer, int offset, int count, Job.Action Callback)
        {
            long readMax = streamSize - streamPos;
            if (readMax > 4096) { readMax = 4096; }
            if (readMax < 0) { readMax = 0; }

            if (cacheSize == 0 && count < readMax && streamSize > 0)
            {
                ReadAsync(cache, 0, (int)readMax, () =>
                    {
                        cacheSize = (int)readMax;
                        cacheOffset = 0;
                        ReadFromCache(buffer, offset, count);
                        Callback();
                    });
            }
            else
            {
                int size = ReadFromCache(buffer, offset, count);
                offset += size;
                if (size >= count) { Callback(); }
                else { ReadAsync(buffer, offset, count - size, Callback); }
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int finished = 0;
            Read(buffer, offset, count, () => { System.Threading.Interlocked.Increment(ref finished); });
            if (Job.CurrentJob == null)
            {
                while (System.Threading.Interlocked.CompareExchange(ref finished, 1, 1) != 1)
                {
                    System.Threading.Thread.Sleep(10);
                }
            }
            else
            {
                while (System.Threading.Interlocked.CompareExchange(ref finished, 1, 1) != 1)
                {
                    Job.Yield();
                }
            }
            return count;
        }

        public override long Seek(long offset, System.IO.SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        void WriteAsync(byte[] buffer, int offset, int count, Job.Action Callback)
        {
            Job.CreateJob(
            () =>
            {
                if (count <= 4096)
                {
                    _Stream.Write(buffer, offset, count);
                    if (Callback != null) { Callback(); }
                }
                else
                {
                    _Stream.Write(buffer, offset, 4096);
                    WriteAsync(buffer, offset + 4096, count - 4096, Callback);
                }
                ;
            });
        }

        public void Write(byte[] buffer, int offset, int count, Job.Action Callback)
        {
            WriteAsync(buffer, offset, count, Callback);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            WriteAsync(buffer, offset, count, null);
        }
    }
}

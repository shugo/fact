﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Threading
{
    internal class JobPool
    {
        static System.Threading.Thread[] _JobThreads = null;
        static internal PerThreadStorage _CurrentJobPool = new PerThreadStorage();
        static System.Threading.Thread _Manager = null;

        static Queue<Job> _Jobs = new Queue<Job>();

        static internal void AddJob(Job Job) 
        {
            lock (_Jobs)
            {
                _Jobs.Enqueue(Job);
                System.Threading.Monitor.PulseAll(_Jobs);
            }
        }

        static internal void AddTimer(Job Job, int Interval, bool Repeat)
        {
            Timer Timer = new Timer();
            Timer.Job = Job;
            Timer.Periodic = Repeat;
            Timer.RemainingTime = Interval;
            Timer.Interval = Interval;
            lock (_Timers)
            {
                _Timers.Add(Timer);
            }
        }

        static bool _ThreadProtected = true;
        static internal void ResizeJobPoolSize(int NewSize)
        {
            lock (_JobThreads)
            {
                if (NewSize > _JobThreads.Length)
                {
                    int OldSize = _JobThreads.Length;
                    System.Array.Resize(ref _JobThreads, NewSize);
                    for (int n = OldSize; n < _JobThreads.Length; n++)
                    {
                        _JobThreads[n] = new System.Threading.Thread(RunJob) { IsBackground = true };
                        _JobThreads[n].Start();
                    }
                }
                else
                {
                    _ThreadProtected = false;
                    for (int n = 0; n < _JobThreads.Length; n++)
                    {
                        try
                        {
                            _JobThreads[n].Abort();
                        }
                        catch (Exception e) { Fact.Log.Error(e.ToString()); }
                    }
                    _JobThreads = new System.Threading.Thread[NewSize];
                    for (int n = 0; n < NewSize; n++)
                    {
                        _JobThreads[n] = new System.Threading.Thread(RunJob) { IsBackground = true };
                        _JobThreads[n].Start();
                    }
                    _ThreadProtected = true;
                }
            }
        }

        static JobPool()
        {
            int processor = System.Environment.ProcessorCount;
            _JobThreads = new System.Threading.Thread[processor];
            for (int n = 0; n < processor; n++)
            {
                _JobThreads[n] = new System.Threading.Thread(RunJob) { IsBackground = true };
                _JobThreads[n].Start();
            }

            _Manager = new System.Threading.Thread(TimerManager) { IsBackground = true };
            _Manager.Start();

#if DEBUG
            Fact.Log.Debug("threadpool created with " + _JobThreads.Length + " threads.");
#endif
        }

        internal class Timer
        {
            internal int RemainingTime = 0;
            internal int Interval = 0;
            internal Job Job = null;
            internal bool Periodic = true;
        }

        static List<Timer> _Timers = new List<Timer>();

        static internal void TimerManager()
        {
            DateTime OldDateTime = DateTime.UtcNow;
            while (true)
            {
                try
                {
                    DateTime NewDateTime = DateTime.UtcNow;
                    int Interval = (int)(( NewDateTime - OldDateTime).TotalMilliseconds);
                    if (Interval > 0)
                    {
                        lock (_Timers)
                        {

                            bool MustCollect = false;
                            foreach (Timer timer in _Timers)
                            {
                                timer.RemainingTime -= Interval;
                                while (timer.RemainingTime <= 0 && !timer.Job._Status._Expired)
                                {
                                    AddJob(timer.Job);
                                    if (timer.Periodic)
                                    {
                                        /* Shugo: The user want a continiously running job */
                                        /* I don't know how to handle this case */
                                        if (timer.Interval <= 0) { break; }
                                        timer.RemainingTime += timer.Interval;
                                    }
                                    else
                                    {
                                        MustCollect = true;
                                        break;
                                    }
                                }
                                if (timer.Job._Status._Expired)
                                {
                                    MustCollect = true;
                                }
                            }
                            if (MustCollect)
                            {
                                for (int n = 0; n < _Timers.Count; n++)
                                {
                                    if (
                                         (!_Timers[n].Periodic && _Timers[n].RemainingTime < 0) ||
                                         (_Timers[n].Job._Status._Expired)
                                       )
                                    {
                                        _Timers.RemoveAt(n);
                                        n -= 1;
                                    }
                                }
                            }
                        }
                    }
                    OldDateTime = NewDateTime;
                }
                catch { }
                try
                {
                    System.Threading.Thread.Sleep(100);
                }
                catch { }
            }
        }

        static internal void DetachThread(int ManagedThreadID)
        {
            lock (_JobThreads)
            {
                if (!_ThreadProtected) { return; }
                for (int n = 0; n < _JobThreads.Length; n++)
                {
                    if (_JobThreads[n].ManagedThreadId == ManagedThreadID)
                    {
                        _JobThreads[n] = new System.Threading.Thread(RunJob) { IsBackground = true };
                        _JobThreads[n].Start();
                        break;
                    }
                }
            }
        }

        static internal void RunJob()
        {
            Job CurrentJob = null;
            try
            {
                while (true)
                {
                    try
                    {
                        CurrentJob = null;
                        lock (_Jobs)
                        {
                            while (_Jobs.Count == 0) { System.Threading.Monitor.Wait(_Jobs); }
                            CurrentJob = _Jobs.Dequeue();
                        }
                        if (CurrentJob != null)
                        {
#if DEBUG
                            try
                            {
                                Fact.Log.Debug("Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + " from the threadpool will launch " + CurrentJob.ToString());
                            }
                            catch { }
#endif
                            CurrentJob.Run();
                            lock (_Jobs)
                            {
                                System.Threading.Monitor.PulseAll(_Jobs);
                            }
                        }
                    }
                    catch { }
                }
            }
            catch
            {
#if DEBUG
                Fact.Log.Debug("Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + " from the threadpool has been killed.");
#endif
            }
            finally 
            {
                if (CurrentJob != null)
                {
#if DEBUG
                    Fact.Log.Debug("Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + " from the threadpool will be resheduled");
#endif
                    DetachThread(System.Threading.Thread.CurrentThread.ManagedThreadId);

                }
                else
                {
#if DEBUG
                    Fact.Log.Debug("Thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + " has been removed");
#endif
                }
            }
        }


        static internal bool Yield()
        {
            Job CurrentJob = null;
            lock (_Jobs)
            {
                if (_Jobs.Count == 0) { return false; }
                CurrentJob = _Jobs.Dequeue();
            }
            try
            {
                if (CurrentJob != null) { CurrentJob.Run(); }
                lock (_Jobs) { System.Threading.Monitor.PulseAll(_Jobs); }
            }
            catch { }
            return true;
        }

        /// <summary>
        /// Wait until no job are remaining in the pool
        /// </summary>
        static internal void Synchronize()
        {
            lock (_Jobs)
            { while (_Jobs.Count > 0) { System.Threading.Monitor.Wait(_Jobs); } }
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Right now we don't handle per Job local storage
// Shugo: Someone can fix it ? We have just to handle the
// case where a ThreadID is part of the jobpool

namespace Fact.Threading
{
    public class Job
    {
        public class Status
        {
            internal Status() { }

            public enum State
            {
                Waiting,
                Starting,
                Running,
                Ended
            }
            internal bool _Expired = false;
            public bool Expired
            {
                get { return _Expired; }
                set
                {
                    if (!_Expired)
                    {
                        _Expired = value;
                    }
                }
            }
            internal System.Threading.Thread _ParentThread = null;
            internal bool _Started = false;
            internal Job _Parent = null;
            State _CurrentState = State.Starting;
            volatile int _Completed = 0;
            public State CurrentState { get { return _CurrentState; } set { _CurrentState = value; } }
            public int Completed { get { return _Completed; } set { _Completed = value; } }
            TimeSpan _Span = TimeSpan.Zero;
            bool _TimerStarted = false;
            System.Diagnostics.Stopwatch _Stopwatch = new System.Diagnostics.Stopwatch();
            internal void StartTimer() { _TimerStarted = true; _Stopwatch.Start(); }
            internal void StopTimer() { _TimerStarted = false; _Stopwatch.Stop(); _Span += _Stopwatch.Elapsed; _Stopwatch.Reset(); }
            internal void ResetTimer() { _Span = TimeSpan.Zero; }
            internal void UpdateTimer() { if (_TimerStarted) { _Span += _Stopwatch.Elapsed; _Stopwatch.Stop(); _Stopwatch.Reset(); _Stopwatch.Start(); } }
            public TimeSpan UserTime { get { UpdateTimer(); return _Span; } }

            public void Abort()
            {
                lock (this)
                {
                    _Expired = true;
                    if (_ParentThread == null) { return; }
                    if (_CurrentState == State.Ended) {  return; }
                    System.Threading.Monitor.PulseAll(this);
                    _ParentThread.Abort();
                }
            }

            public void Join() 
            {
                lock (this)
                {
                    while (!_Expired && _CurrentState != State.Ended)
                    {
                        System.Threading.Monitor.Wait(this);
                    }
                }
            }

            public bool Join(int Timeout)
            {
                lock (this)
                {
                    if (!_Expired && _CurrentState != State.Ended)
                    {
                        return System.Threading.Monitor.Wait(this, Timeout);
                    }
                    return true;
                }
            }
        }

        public static void SetThreadPoolSize(int Size)
        {
            if (Size > 0)
            {
                JobPool.ResizeJobPoolSize(Size);
            }
        }

        public delegate void Action();
        public delegate void ActionWithParameter(object Parameter);
        public delegate void ActionWithInfo(ref Status Status);
        public delegate void ActionWithParameterWithInfo(object Parameter, ref Status Status);

        public static Status CreateJob(Action Action)
        {
            Job job = new Job(Action);
            JobPool.AddJob(job);
            return job._Status;
        }


        public static Status CreateJob(Action Action, int Timeout, Action ActionOnTimeout)
        {
            Job job = new Job(Action);
            JobPool.AddJob(job);
            int interval = Timeout;
            // No more than a 100ms quantum
            if (Timeout > 100) { interval = 100; }
            CreateTimer((ref Status Status) =>
            {
                if (job._Status._Expired || job._Status.CurrentState == Job.Status.State.Ended)
                {
                    Status.Expired = true;
                    return;
                }
                if (job._Status.UserTime.TotalMilliseconds > Timeout)
                {
                    job._Status.Abort();
                    if (ActionOnTimeout != null) { ActionOnTimeout(); }
                    Status.Expired = true;
                }
            }, interval, true);
            return job._Status;
        }

        public static Status CreateJob(ActionWithParameter Action, object Parameter)
        {
            Job job = new Job(Action, Parameter);
            JobPool.AddJob(job);
            return job._Status;
        }

        public enum CreationMethod
        {
            Create,
            ReplaceIfNotStarted,
        };

        public static Status CreateTimer(ActionWithInfo Action, int Interval, bool Repeat)
        {
            Job Job = new Job(Action);
            JobPool.AddTimer(Job, Interval, Repeat);
            return Job._Status;
        }

        public static Status CreateTimer(ActionWithParameterWithInfo Action, object Parameter, int Interval, bool Repeat)
        {
            Job Job = new Job(Action, Parameter);
            JobPool.AddTimer(Job, Interval, Repeat);
            return Job._Status;
        }


        Action _Action = null;
        ActionWithParameter _ActionWithParam = null; object _Parameter = null;
        ActionWithParameterWithInfo _ActionWithParamWithInfo = null;
        ActionWithInfo _ActionWithInfo = null;

        internal Status _Status = new Status();

        internal Job(Action Action) { _Action = Action; }
        internal Job(ActionWithParameter Action, object Parameter) { _Parameter = Parameter; _ActionWithParam = Action; }
        internal Job(ActionWithParameterWithInfo Action, object Parameter) { _Parameter = Parameter; _ActionWithParamWithInfo = Action; _Status = new Status(); }
        internal Job(ActionWithInfo Action) { _ActionWithInfo = Action; _Status = new Status(); }

        internal void Run()
        {
            lock (_Status)
            {
                _Status._ParentThread = System.Threading.Thread.CurrentThread;
                if (_Status._Expired) { return; }
                JobPool._CurrentJobPool.SetData(_Status);
                _Status.StartTimer();
            }
            if (_ActionWithParam != null) { _ActionWithParam(_Parameter); }
            else if (_ActionWithInfo != null)
            {
                lock (_Status) { _Status.CurrentState = Status.State.Running; _Status._Started = true; }
                try { _ActionWithInfo(ref _Status); } catch { }
            }
            else if (_ActionWithParamWithInfo != null)
            {
                lock (_Status) { _Status.CurrentState = Status.State.Running; _Status._Started = true; }
                try { _ActionWithParamWithInfo(_Parameter, ref _Status); } catch { }
            }
            else
            {
                try { _Action(); } catch { }
            }

            lock (_Status)
            {
                _Status.StopTimer();
                _Status.CurrentState = Status.State.Ended;
                System.Threading.Monitor.Pulse(_Status);
            }
        }

        public static Job.Status CurrentJob
        {
            get
            {
                try
                {
                    return (JobPool._CurrentJobPool.GetData() as Job.Status);
                }
                catch { return null; }
            }
        }

        /// <summary>
        ///  Give back the hand to a new job since this one may take long
        /// </summary>

        public static bool Yield()
        {
            Status job = CurrentJob;
            if (job != null) { job.StopTimer(); }
            bool result = JobPool.Yield();
            if (job != null) { job.StartTimer(); JobPool._CurrentJobPool.SetData(job); }
            return result;
        }

        /// <summary>
        /// Wait for a certain amount of time
        /// </summary>
        public static void Sleep(int Millisecond)
        {
            if (CurrentJob == null) { System.Threading.Thread.Sleep(Millisecond); }
            else
            {
                System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
                watch.Start();
                do
                {
                    Yield();
                } while (watch.ElapsedMilliseconds < Millisecond);
                watch.Stop();
            }
        }

        /// <summary>
        /// Wait until no job are remaining in the pool
        /// </summary>
        public static void Synchronize()
        {
            JobPool.Synchronize();
        }

        public override string ToString()
        {
            try
            {
                if (_Action != null) { return "job <" + _Action.Method.Name + ">"; }
                if (_ActionWithInfo != null) { return "job <" + _ActionWithInfo.Method.Name + ">"; }
                if (_ActionWithParamWithInfo != null) { return "job <" + _ActionWithParamWithInfo.Method.Name + ">"; }
            }
            catch { }
            return "job <null>";
        }
    }
}

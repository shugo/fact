﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Assert
{
    public static class Misc
    {
        static bool _GroupPass(Fact.Test.Result.Group Group)
        {
            foreach (Fact.Test.Result.Result result in Group.GetTestResults())
            {
                if (result is Fact.Test.Result.Error) { return false; }
                if (result is Fact.Test.Result.Group) { if (!_GroupPass(result as Fact.Test.Result.Group)) { return false; } }
            }
            return true;
        }

        public static void ExpectTestPass(Fact.Test.Result.Result Value) { ExpectTestPass(Value, Assert.Trigger.FATAL); }
        public static void ExpectTestPass(Fact.Test.Result.Result Value, Assert.Trigger Trigger)
        {
            bool Passed = false;
            if (Value == null) { return; }
            if (Value is Fact.Test.Result.Passed) { Passed = true; }
            else if (Value is Fact.Test.Result.Group) { if (_GroupPass(Value as Fact.Test.Result.Group)) { Passed = true; } }
            if (!Passed)
            {
                if (Value is Fact.Test.Result.Group) { Value = new Fact.Test.Result.Error(Value.Text, Value.Info); }
                Assert Assert = new Assert()
                {
                    Result = Value
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                if (Value is Fact.Test.Result.Group) { Value = new Fact.Test.Result.Passed(Value.Text, Value.Info); }
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = Value
                });
            }
        }

        public static void ExpectTestFail(Fact.Test.Result.Result Value) { ExpectTestFail(Value, Assert.Trigger.FATAL); }
        public static void ExpectTestFail(Fact.Test.Result.Result Value, Assert.Trigger Trigger)
        {
            bool Fail = false;
            if (Value == null) { return; }
            else if (Value is Fact.Test.Result.Error) { Fail = true; }
            else if (Value is Fact.Test.Result.Group) { if (!_GroupPass(Value as Fact.Test.Result.Group)) { Fail = true; } }
            if (!Fail)
            {
                Fact.Test.Result.Error Error = new Fact.Test.Result.Error(Value.Text, Value.Info);
                Assert Assert = new Assert()
                {
                    Result = Error
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Test.Result.Passed Pass = new Fact.Test.Result.Passed(Value.Text, Value.Info);
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = Pass
                });
            }
        }

        /// <summary>
        /// Skip the current test.
        /// </summary>
        public static void Skip() {  Skip("This test has been omitted"); }

        /// <summary>
        /// Skip the current test.
        /// </summary>
        /// <param name="Reason">The reason that will be displayed in the test.</param>
        public static void Skip(string Reason)
        {
            throw new Assert() { Result = new Fact.Test.Result.Omitted("", Reason) };
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Fact.Assert
{
    static public class Collection
    {
        public static void AreSequenceEqual<T>(ICollection<T> Expected, ICollection<T> Value) { AreSequenceEqual(Expected, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreSequenceEqual<T>(ICollection<T> Expected, ICollection<T> Value, Assert.Trigger Trigger, string AssertionName)
        { AreSequenceEqual(Expected, Value, Trigger, AssertionName, "Collections are not equal", "Collections are equal"); }
        public static void AreSequenceEqual<T>(ICollection<T> Expected, ICollection<T> Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (!Expected.SequenceEqual(Value))
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + ": expected " + CollectionToString(Expected) + " got " + CollectionToString(Value))
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreUnorderdEqual<T>(ICollection<T> Expected, ICollection<T> Value) { AreUnorderdEqual(Expected, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreUnorderdEqual<T>(ICollection<T> Expected, ICollection<T> Value, Assert.Trigger Trigger, string AssertionName)
        { AreUnorderdEqual(Expected, Value, Trigger, AssertionName, "Collections are not equal", "Collections are equal"); }
        public static void AreUnorderdEqual<T>(ICollection<T> Expected, ICollection<T> Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            List<T> ExtraItems = new List<T>();
            List<T> MissingItems = new List<T>();

            Dictionary<T, int> Map = new Dictionary<T, int>();
            foreach (T item in Expected) { if (Map.ContainsKey(item)) { Map[item]++; } else { Map.Add(item, 1); } }
            foreach (T item in Value)
            {
                if (!Map.ContainsKey(item) || Map[item] <= 0) { ExtraItems.Add(item); }
                else { Map[item]--; }
            }
            foreach (KeyValuePair<T, int> pair in Map)
            {
                for (int n = 0; n < pair.Value; n++) { MissingItems.Add(pair.Key); }
            }

            if (ExtraItems.Count() > 0 ||
                MissingItems.Count() > 0)
            {
                StringBuilder reason = new StringBuilder();
                reason.AppendLine(ReasonOnFailure + ":");
                if (ExtraItems.Count() > 0)
                {
                    reason.Append("extra unexpected items:");
                    reason.AppendLine(CollectionToString(ExtraItems));
                }
                if (MissingItems.Count() > 0)
                {
                    reason.Append("missing expected items:");
                    reason.AppendLine(CollectionToString(MissingItems));
                }
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, reason.ToString())
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreUnorderdEqualIgnoringDuplicate<T>(ICollection<T> Expected, ICollection<T> Value) { AreUnorderdEqualIgnoringDuplicate(Expected, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreUnorderdEqualIgnoringDuplicate<T>(ICollection<T> Expected, ICollection<T> Value, Assert.Trigger Trigger, string AssertionName)
        { AreUnorderdEqualIgnoringDuplicate(Expected, Value, Trigger, AssertionName, "Collections are not equal", "Collections are equal"); }
        public static void AreUnorderdEqualIgnoringDuplicate<T>(ICollection<T> Expected, ICollection<T> Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            IEnumerable<T> missingElements = Expected.Except(Value);
            IEnumerable<T> extraElements = Value.Except(Expected);
            if (missingElements.Count() > 0 ||
                extraElements.Count() > 0)
            {
                StringBuilder reason = new StringBuilder();
                reason.AppendLine(ReasonOnFailure + ":");
                if (extraElements.Count() > 0)
                {
                    reason.Append("extra unexpected items:");
                    reason.AppendLine(CollectionToString(extraElements));
                }
                if (missingElements.Count() > 0)
                {
                    reason.Append("missing expected items:");
                    reason.AppendLine(CollectionToString(missingElements));
                }
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, reason.ToString())
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        static string CollectionToString<T>(IEnumerable<T> Collection)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("[");
            bool first = true;
            foreach (T item in Collection)
            {
                if (!first) { builder.Append(", "); }
                first = false;
                builder.Append(item.ToString());
            }
            builder.Append("]");
            return builder.ToString();
        }
    }
}

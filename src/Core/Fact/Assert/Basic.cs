﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Assert
{
    static public class Basic
    {
        public static void AreEqual(int Expected, int Value) { AreEqual(Expected, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreEqual(int Expected, int Value, Assert.Trigger Trigger, string AssertionName)
        { AreEqual(Expected, Value, Trigger, AssertionName, "Values are not equal", "Values are equal"); }
        public static void AreEqual(int Expected, int Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (Expected != Value)
            {
               Assert Assert = new Assert()
                    {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + ": ref " + Expected.ToString() + " my " + Value.ToString())
                    };
               if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreEqual(string Expected, string Value) { AreEqual(Expected, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreEqual(string Expected, string Value, Assert.Trigger Trigger, string AssertionName)
        { AreEqual(Expected, Value, Trigger, AssertionName, "Content", "Values are equal"); }
        public static void AreEqual(string Expected, string Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (Expected != Value)
            {
                Fact.Runtime.Process.Result _ref = new Fact.Runtime.Process.Result(Expected, "", 0);
                Fact.Runtime.Process.Result my = new Fact.Runtime.Process.Result(Value, "", 0);

                Fact.Runtime.Diff diff = new Fact.Runtime.Diff(_ref, my);
                diff.FormatSpace = false;
                diff.FormatEndOfLine = false;
                diff.FormatTrim = false;
                diff.StdOutIsXML = false;
                Fact.Runtime.Diff.Result result = diff.Run(0);
                result.StdOutName = ReasonOnFailure;
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, result.Summary)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreEqual(Runtime.Process Expected, Runtime.Process Value, int Timeout)
        { AreEqual(Expected, Value, Timeout, Assert.Trigger.FATAL, "Assertion", "", "Process output are equal"); }
        public static void AreEqual(Runtime.Process Expected, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName)
        { AreEqual(Expected, Value, Timeout, Trigger, AssertionName, "", "Process output are equal"); }
        public static void AreEqual(Runtime.Process Expected, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(Expected, Value);
            Fact.Runtime.Diff.Result diffresult = diff.Run(Timeout);
            if (!diffresult.AreEqual)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + (ReasonOnFailure != "" ? ": " : "") + diffresult.Summary)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                    {
                        Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                    });
            }
        }

        public static void AreEqual(Runtime.Process.Result Expected, Runtime.Process Value, int Timeout)
        { AreEqual(Expected, Value, Timeout, Assert.Trigger.FATAL, "Assertion", "", "Results are equal"); }
        public static void AreEqual(Runtime.Process.Result Expected, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName)
        { AreEqual(Expected, Value, Timeout, Trigger, AssertionName, "", "Results are equal"); }
        public static void AreEqual(Runtime.Process.Result Expected, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(Expected, Value);
            Fact.Runtime.Diff.Result diffresult = diff.Run(Timeout);
            if (!diffresult.AreEqual)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + (ReasonOnFailure != "" ? ": " : "") + diffresult.Summary)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                    {
                        Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                    });
            }
        }

        public static void AreEqual(Runtime.Process.Result Expected, Runtime.Process.Result Value)
        { AreEqual(Expected, Value, Assert.Trigger.FATAL, "Assertion", "", "Results are equal"); }
        public static void AreEqual(Runtime.Process.Result Expected, Runtime.Process.Result Value, Assert.Trigger Trigger, string AssertionName)
        { AreEqual(Expected, Value, Trigger, AssertionName, "", "Results are equal"); }
        public static void AreEqual(Runtime.Process.Result Expected, Runtime.Process.Result Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(Expected, Value);
            Fact.Runtime.Diff.Result diffresult = diff.Run(-1);
            if (!diffresult.AreEqual)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + (ReasonOnFailure != "" ? ": " : "") + diffresult.Summary)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                    {
                        Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                    });
            }
        }

        public static void AreEqual(bool Expected, bool Value) { AreEqual(Expected, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreEqual(bool Expected, bool Value, Assert.Trigger Trigger, string AssertionName)
        { AreEqual(Expected, Value, Trigger, AssertionName, "Values are not equal", "Values are equal");}
        public static void AreEqual(bool Expected, bool Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (Expected != Value)
            {
                Assert Assert = new Assert()
                    {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + ": ref " + Expected.ToString() + " my " + Value.ToString())
                    };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);                    
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreNotEqual(int Unwanted, int Value) { AreNotEqual(Unwanted, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreNotEqual(int Unwanted, int Value, Assert.Trigger Trigger, string AssertionName)
        { AreNotEqual(Unwanted, Value, Trigger, AssertionName, "Values are equal", "Values are not equal"); }
        public static void AreNotEqual(int Unwanted, int Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (Unwanted == Value)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + ": " + Unwanted.ToString())
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreNotEqual(string Unwanted, string Value) { AreNotEqual(Unwanted, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreNotEqual(string Unwanted, string Value, Assert.Trigger Trigger, string AssertionName)
        { AreNotEqual(Unwanted, Value, Trigger, AssertionName, "Values are equal", "Values are not equal"); }
        public static void AreNotEqual(string Unwanted, string Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (Unwanted == Value)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, "Value must not be \"" + Unwanted + "\"")
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                     Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreNotEqual(Runtime.Process Unwanted, Runtime.Process Value, int Timeout)
        { AreNotEqual(Unwanted, Value, Timeout, Assert.Trigger.FATAL, "Assertion", "Process output must not be equal", "Process output are not equal"); }
        public static void AreNotEqual(Runtime.Process Unwanted, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName)
        { AreNotEqual(Unwanted, Value, Timeout, Trigger, AssertionName, "Process output must not be equal", "Process output are not equal"); }
        public static void AreNotEqual(Runtime.Process Unwanted, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(Unwanted, Value);
            Fact.Runtime.Diff.Result diffresult = diff.Run(Timeout);
            if (diffresult.AreEqual)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreNotEqual(Runtime.Process.Result Unwanted, Runtime.Process Value, int Timeout)
        { AreNotEqual(Unwanted, Value, Timeout, Assert.Trigger.FATAL, "Assertion", "Process output must not be equal", "Results are not equal"); }
        public static void AreNotEqual(Runtime.Process.Result Unwanted, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName)
        { AreNotEqual(Unwanted, Value, Timeout, Trigger, AssertionName, "Process output must not be equal", "Results are not equal"); }
        public static void AreNotEqual(Runtime.Process.Result Unwanted, Runtime.Process Value, int Timeout, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(Unwanted, Value);
            Fact.Runtime.Diff.Result diffresult = diff.Run(Timeout);
            if (diffresult.AreEqual)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreNotEqual(Runtime.Process.Result Unwanted, Runtime.Process.Result Value)
        { AreNotEqual(Unwanted, Value, Assert.Trigger.FATAL, "Assertion", "Results must not be equal", "Results are equal"); }
        public static void AreNotEqual(Runtime.Process.Result Unwanted, Runtime.Process.Result Value, Assert.Trigger Trigger, string AssertionName)
        { AreNotEqual(Unwanted, Value, Trigger, AssertionName, "Results must not be equal", "Results are equal"); }
        public static void AreNotEqual(Runtime.Process.Result Unwanted, Runtime.Process.Result Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(Unwanted, Value);
            Fact.Runtime.Diff.Result diffresult = diff.Run(-1);
            if (diffresult.AreEqual)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL ||
                    Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL ||
                Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        public static void AreNotEqual(bool Unwanted, bool Value) { AreNotEqual(Unwanted, Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void AreNotEqual(bool Unwanted, bool Value, Assert.Trigger Trigger, string AssertionName)
        { AreNotEqual(Unwanted, Value, Trigger, AssertionName, "Values are equal", "Values are not equal"); }
        public static void AreNotEqual(bool Unwanted, bool Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (Unwanted == Value)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure + ": " + Unwanted.ToString())
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }

        [Obsolete("MustNotOccurs is deprecated, use MustNotOccur instead.")]
        public static void MustNotOccurs() { MustNotOccurs(Assert.Trigger.FATAL, "Assertion"); }
        [Obsolete("MustNotOccurs is deprecated, use MustNotOccur instead.")]
        public static void MustNotOccurs(Assert.Trigger Trigger, string AssertionName)
        { MustNotOccurs(Trigger, AssertionName, "This part of the code must not be executed"); }
        [Obsolete("MustNotOccurs is deprecated, use MustNotOccur instead.")]
        public static void MustNotOccurs(Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure)
        {
            if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
            {
                throw new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                };
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.NON_FATAL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                });
            }
        }

        public static void MustNotOccur() { MustNotOccur(Assert.Trigger.FATAL, "Assertion"); }
        public static void MustNotOccur(Assert.Trigger Trigger, string AssertionName)
        { MustNotOccur(Trigger, AssertionName, "This part of the code must not be executed"); }
        public static void MustNotOccur(Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure)
        {
            if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
            {
                throw new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                };
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.NON_FATAL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                });
            }
        }

        public static void IsTrue(bool Value) { IsTrue(Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void IsTrue(bool Value, Assert.Trigger Trigger, string AssertionName)
        { IsTrue(Value, Trigger, AssertionName, "Value must be true", "Value is true"); }
        public static void IsTrue(bool Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (!Value)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }
        public static void IsFalse(bool Value) { IsFalse(Value, Assert.Trigger.FATAL, "Assertion"); }
        public static void IsFalse(bool Value, Assert.Trigger Trigger, string AssertionName)
        { IsFalse(Value, Trigger, AssertionName, "Value must be false", "Value is false"); }
        public static void IsFalse(bool Value, Assert.Trigger Trigger, string AssertionName, string ReasonOnFailure, string ReasonOnSuccess)
        {
            if (Value)
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, ReasonOnFailure)
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                     Result = new Fact.Test.Result.Passed(AssertionName, ReasonOnSuccess)
                });
            }
        }
    }
}

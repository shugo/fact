﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Assert
{
    public class Assert : Exception
    {
        public enum Trigger
        {
            FATAL,
            FATAL_RECORD_ALL,
            NON_FATAL,
            NON_FATAL_RECORD_ALL
        }
        static Threading.PerThreadStorage _AssertStorage = new Fact.Threading.PerThreadStorage();
        static Threading.PerThreadStorage _StackFrameLock = new Fact.Threading.PerThreadStorage();

        static public void EnableDelayedAsserts()
        {
            object FrameLock = _StackFrameLock.GetData();
            if (FrameLock != null)
                throw new Exception("Impossible to aquire the lock on the per thread assert storage");
            System.Diagnostics.StackTrace StackTrace = new System.Diagnostics.StackTrace();
            try
            {
                _StackFrameLock.SetData(StackTrace.GetFrame(1).GetMethod());
                _AssertStorage.SetData(new List<Assert>());
                return;
            }
            catch { }
            _StackFrameLock.SetData(null);
            throw new Exception("Impossible to aquire the lock on the per thread assert storage");
        }
        static public List<Assert> FlushThreadPendingAsserts()
        {
            {
                object stackchecker = _StackFrameLock.GetData();
                if (stackchecker == null || !(stackchecker is System.Reflection.MethodBase))
                    throw new Exception("EnableDelayedAsserts must be called to collect the pending asserts");
                System.Reflection.MethodBase initialCaller = stackchecker as System.Reflection.MethodBase;
                System.Reflection.MethodBase currentCaller = (new System.Diagnostics.StackTrace()).GetFrame(1).GetMethod();
                if (initialCaller.Name != currentCaller.Name ||
                    initialCaller.Module != currentCaller.Module)
                {
                    throw new Exception("Invalid caller stack trace");
                }
            }
            {
                object list = _AssertStorage.GetData();
                _AssertStorage.SetData(new List<Assert>());
                _StackFrameLock.SetData(null);
                if (list == null || !(list is List<Assert>))
                {
                    throw new Exception("EnableDelayedAsserts must be called to collect the pending asserts");
                }
                return list as List<Assert>;
            }
        }

        static internal void PushThreadAssert(Assert Assert)
        {
            object list = _AssertStorage.GetData();
            if (list == null || !(list is List<Assert>)) { return; }
            ((List<Assert>)list).Add(Assert);
        }

        Fact.Test.Result.Result _Result = null;
        public Fact.Test.Result.Result Result { get { return _Result; } set { _Result = value; } }
        public override string Message
        {
            get
            {
                if (_Result != null) { return _Result.Info; }
                return "Assertion fail";
            }
        }
        public override string ToString()
        {
            if (_Result != null) { return _Result.ToString(); }
            return "Assertion fail";

        }
    }
}

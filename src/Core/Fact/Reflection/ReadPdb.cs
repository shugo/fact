﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    class ReadPdb
    {
        static string V7_header = "Microsoft C/C++ MSF 7.00\r\n\032DS\0\0";
        public ReadPdb()
        {

        }

        static ulong ReadPdb_Word(byte[] AssemblyData, int Offset)
        {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100;
        }

        static ulong ReadPdb_DWord(byte[] AssemblyData, int Offset)
        {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000 +
                       (ulong)AssemblyData[Offset + 3] * 0x1000000;
        }

        static List<string> ReadPdb_StringList()
        {
            List<string> list = new List<string>();
            return list;
        }

        public static void ReadPdb_FromBytes(byte[] Array, int Offset)
        {
            Offset += V7_header.Length;
            ulong pageSize = ReadPdb_DWord(Array, Offset); Offset += 4;
            ulong fileSize = ReadPdb_DWord(Array, Offset); Offset += 4;
        }

    }
}

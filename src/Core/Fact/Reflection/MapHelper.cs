﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    internal class MapHelper
    {
        IntPtr _Mapping = new IntPtr();
        int _PageSize = 4096;
        internal class Section
        {
            internal int Offset = 0;
            internal int Size = 0;

            internal Memory.VirtualAddressSpace.MemoryProtection Protection = Memory.VirtualAddressSpace.MemoryProtection.None;
            internal object Tag = null;

            internal string Name = "";
            public override string ToString()
            {
                return Name;
            }
        }

        List<Section>[] _SectionMap = new  List<Section>[0];

        public IntPtr Mapping { get { return _Mapping; } }

        Memory.VirtualAddressSpace _VASpace;

        public MapHelper(ulong Size, Memory.VirtualAddressSpace VASpace)
        {
            _VASpace = VASpace;
            _Mapping = _VASpace.MapMemory((int)Size, Memory.VirtualAddressSpace.MemoryProtection.Read | Memory.VirtualAddressSpace.MemoryProtection.Write);
            if (_Mapping.ToInt64() == 0) { throw new OutOfMemoryException(); }
            _SectionMap = new List<Section>[(Size + (ulong)_PageSize - 1) / (ulong)_PageSize];
        }

        public IntPtr MapSection(Section Section)
        {
            IntPtr ptr = new IntPtr(Section.Offset + _Mapping.ToInt64());
            int page = Section.Offset / _PageSize;
            int size = (Section.Size + _PageSize - 1) / _PageSize;
            for (int n = page; n < (page + size) && n < _SectionMap.Length; n++)
            {
#if DEBUG
                Fact.Log.Debug("Associate " + Section.Name + " to region [0x" + ((long)(n * _PageSize) + ptr.ToInt64()).ToString("X") + ":0x" + ((long)(n * _PageSize) + ptr.ToInt64() + _PageSize).ToString("X") + "]");
#endif
                if (_SectionMap[n] == null) { _SectionMap[n] = new List<Section>(); }
                _SectionMap[n].Add(Section);
            }
            return ptr;
        }

        public void ApplyProtection()
        {
#if DEBUG
            Fact.Log.Trace();
#endif
            IntPtr unmapptr = new IntPtr();
            int unmapSize = 0;
            for (int n = 0; n < _SectionMap.Length; n++)
            {
                IntPtr ptr = new IntPtr((long)(n * _PageSize) + _Mapping.ToInt64());
                Memory.VirtualAddressSpace.MemoryProtection protection = Memory.VirtualAddressSpace.MemoryProtection.None;
                if (_SectionMap[n] != null)
                {
                    for (int x = 0; x < _SectionMap[n].Count; x++)
                    {
                        protection = protection | _SectionMap[n][x].Protection;
                    }
                }
                if (protection == Memory.VirtualAddressSpace.MemoryProtection.None)
                {
                    if (unmapSize == 0)
                    {
                        unmapptr = ptr;
                    }
                    unmapSize += _PageSize;
                }
                else
                {
                    if (unmapSize > 0)
                    {
#if DEBUG
                        Fact.Log.Debug("Unmap memory region [0x" + unmapptr.ToInt64().ToString("X") + ":0x" + (unmapptr.ToInt64() + unmapSize).ToString("X") + "]");
#endif
                        _VASpace.UnmapMemory(unmapptr, unmapSize);
                        unmapSize = 0;
                    }
                    _VASpace.ChangeMemoryProtection(ptr, _PageSize, protection);
#if DEBUG
                    Fact.Log.Debug("Set memory protection of region [0x" + ptr.ToInt64().ToString("X") + ":0x" + (ptr.ToInt64() + _PageSize).ToString("X") + "] to " +
                        ((protection & Memory.VirtualAddressSpace.MemoryProtection.Read) != 0 ? "R" : "-") +
                        ((protection & Memory.VirtualAddressSpace.MemoryProtection.Write) != 0 ? "W" : "-" )+
                        ((protection & Memory.VirtualAddressSpace.MemoryProtection.Execute) != 0 ? "X" : "-"));
#endif
                }
            }
            if (unmapSize > 0)
            {
#if DEBUG
                Fact.Log.Debug("Unmap memory region [0x" + unmapptr.ToInt64().ToString("X") + ":0x" + (unmapptr.ToInt64() + unmapSize).ToString("X") + "]");
#endif
                _VASpace.UnmapMemory(unmapptr, unmapSize);

            }
        }
    }
}

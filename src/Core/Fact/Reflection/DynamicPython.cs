﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    class Python
    {
        internal class PythonMethod : Method
        {
            Parser.Python.Def _Def;
            internal PythonMethod(Parser.Python.Def Def)
            {
                _Def = Def;
                _Name = Def._Name;
            }
        }
        internal class Context : Assembly
        {
            Context _Parent;
            List<Context> _SubContext = new List<Context>();
            internal Context() { _PythonDir = FindPythonDir(); }
            internal Context(Context Parent) { _Parent = Parent; }

            string _PythonDir = "";
            string FindPythonDir()
            {
                try
                {
                    if (Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftWindows)
                    {

                        foreach (string dir in System.IO.Directory.GetDirectories("C:/"))
                        {
                            if (dir.ToLower().StartsWith("c:/python"))
                            {
                                if (System.IO.File.Exists(dir + "/python.exe"))
                                {
                                    if (System.IO.Directory.Exists(dir + "/Lib") || System.IO.Directory.Exists(dir + "/libs"))
                                    {
                                        return dir;
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }
                return "";
            }


            string FindLib(string name, string directory)
            {
                if (System.IO.Directory.Exists(directory))
                {
                    foreach (string file in System.IO.Directory.GetFiles(directory))
                    {
                        string filename = System.IO.Path.GetFileName(file);
                        if (filename.ToLower() == name.ToLower() + ".py") { return file; }
                    }
                }
                return "";
            }

            string FindLib(string name)
            {
                if (_PythonDir != "")
                {
                    string lib = FindLib(name, _PythonDir + "/Lib");
                    if (lib != "") { return lib; }
                    lib = FindLib(name, _PythonDir + "/libs");
                    if (lib != "") { return lib; }
                }
                return "";
            }

            public void LoadCode(string Code, string Filename)
            {
               Parser.Python.Scope root = Parser.Python.Parse(Code, Filename);
               Parser.Python.Scope main = new Parser.Python.Scope();

               foreach (Parser.Python.Element element in root._Elements)
               {
                    if (element is Parser.Python.Import)
                    {
                        Parser.Python.Import import = (element as Parser.Python.Import);
                        string lib = FindLib(import._Library);
                        if (lib != "")
                        {
                            Context subcontext = new Context();
                            string libcode = System.IO.File.ReadAllText(lib);
                            subcontext.LoadCode(libcode, import._Library);
                            _SubContext.Add(subcontext);
                        }
                    }
                    if (element is Parser.Python.Def)
                    {
                        PythonMethod method = new PythonMethod(element as Parser.Python.Def);
                        _Methods.Add(method);
                    }
               }
            }

        }
        internal static Context CreateContext()
        {
            return new Context();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public static unsafe class Tools
    {

        static public Delegate GetDelegateForMethod(System.Reflection.MethodInfo Method)
        {
            List<System.Type> types = new List<System.Type>();
            foreach (System.Reflection.ParameterInfo parameters in Method.GetParameters()) { types.Add(parameters.ParameterType); }
            System.Type delegateType = Internal.Reflection.MSILDelegate.CreateDelegateType(Method.ReturnType, types.ToArray());
            return Delegate.CreateDelegate(delegateType, Method);
        }

        static public Delegate GetDelegateForMethod(object Target, System.Reflection.MethodInfo Method)
        {
            List<System.Type> types = new List<System.Type>();
            foreach (System.Reflection.ParameterInfo parameters in Method.GetParameters()) { types.Add(parameters.ParameterType); }
            System.Type delegateType = Internal.Reflection.MSILDelegate.CreateDelegateType(Method.ReturnType, types.ToArray());
            return Delegate.CreateDelegate(delegateType,Target, Method.Name);
        }

        static unsafe void* ObjectToPointer(object Parameter)
        {
            if (Parameter == null) { return null; }
            if (Parameter is Int16) { return (void*)((Int16)Parameter); }
            if (Parameter is Int32) { return (void*)((Int32)Parameter); }
            if (Parameter is Int64) { return (void*)((Int64)Parameter); }
            if (Parameter is UInt16) { return (void*)((UInt16)Parameter); }
            if (Parameter is UInt32) { return (void*)((UInt32)Parameter); }
            if (Parameter is UInt64) { return (void*)((UInt64)Parameter); }
            if (Parameter is IntPtr) { return ((IntPtr)Parameter).ToPointer(); }

            throw new Exception("Do not know how to unmanage type: " + Parameter.GetType().Name);

        }

        static unsafe bool IsType<A, B>()
        {
            return typeof(A) == typeof(B);
        }

        static unsafe T Cast<T>(object Input)
        {
            return (T)Input;
        }

        static public byte[] ReadCStr(byte* Parameter)
        {
            if (Parameter == null) { return new byte[0]; }
            List<byte> str = new List<byte>();
            while (*Parameter != 0)
            {
                str.Add(*Parameter);
                Parameter++;
            }
            return str.ToArray();
        }

        static public byte[] ReadCStr(byte* Parameter, ulong size)
        {
            if (Parameter == null || size == 0) { return new byte[0]; }
            List<byte> str = new List<byte>();
            for (ulong i = 0; i < size; ++i)
                str.Add(Parameter[i]);
            return str.ToArray();
        }

        static public string FromCStr(void* Parameter)
        {
            return System.Text.Encoding.UTF8.GetString(ReadCStr((byte*)Parameter));
        }

        static public string FromCStr(void* Parameter, ulong size)
        {
            return System.Text.Encoding.UTF8.GetString(ReadCStr((byte*)Parameter, size));
        }

        static public byte* ToCStr(string Parameter)
        {
            byte[] Array = System.Text.Encoding.UTF8.GetBytes(Parameter);
            byte* cstr = (byte*)System.Runtime.InteropServices.Marshal.AllocHGlobal(Array.Length + 1).ToPointer();
            for (int n = 0; n < Array.Length; n++)
            {
                cstr[n] = Array[n];
            }
            cstr[Array.Length] = 0;
            return cstr;
        }

        static unsafe T PointerToObject<T>(void* Parameter)
        {
            
            if (Parameter == null)
            {
                if (IsType<T, UInt16>()) { return Cast<T>((UInt16)0); }
                if (IsType<T, UInt32>()) { return Cast<T>((UInt32)0); }
                if (IsType<T, UInt64>()) { return Cast<T>((UInt64)0); }

                if (IsType<T, Int16>()) { return Cast<T>((Int16)0); }
                if (IsType<T, Int32>()) { return Cast<T>((Int32)0); }
                if (IsType<T, Int64>()) { return Cast<T>((Int64)0); }
            }

            if (IsType<T, UInt16>()) { return Cast<T>((UInt16)Parameter); }
            if (IsType<T, UInt32>()) { return Cast<T>((UInt32)Parameter); }
            if (IsType<T, UInt64>()) { return Cast<T>((UInt64)Parameter); }

            if (IsType<T, Int16>()) { return Cast<T>((Int16)Parameter); }
            if (IsType<T, Int32>()) { return Cast<T>((Int32)Parameter); }
            if (IsType<T, Int64>()) { return Cast<T>((Int64)Parameter); }

            if (IsType<T, String>()) { return Cast<T>(FromCStr(Parameter)); }
            if (IsType<T, IntPtr>()) { return Cast<T>(new IntPtr(Parameter)); }
            if (IsType<T, Double>()) { return Cast<T>(*((Double*)(&Parameter))); }
            if (IsType<T, Single>()) { return Cast<T>(*((Single*)(&Parameter))); }

            throw new Exception("Do not know how to manage type from pointer: " + typeof(T).Name);
        }

        internal static string ComputeNativeSignature(IntPtr Function, System.Type ReturnType, params object[] Parameters)
        {
            if (Parameters == null) { Parameters = new object[0]; }
            System.Type[] types = new System.Type[Parameters.Length];
            for (int n = 0; n < Parameters.Length; n++)
            {
                if (Parameters[n] is string) { types[n] = typeof(IntPtr); }
                else if (Parameters[n] is double) { types[n] = typeof(double); }
                else if (Parameters[n] is float) { types[n] = typeof(float); }
                else { types[n] = typeof(IntPtr); }
            }
            return ComputeSignature(Function, ReturnType, types);
        }

        internal static string ComputeSignature(IntPtr Function, System.Type ReturnType, params System.Type[] Parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(ReturnType.FullName); builder.Append("#");
            builder.Append(ReturnType.IsArray); builder.Append("#");
            builder.Append(Function.ToInt64().ToString()); builder.Append("#");
            builder.Append(Parameters.Length); builder.Append("#");
            foreach (System.Type t in Parameters)
            {
                builder.Append(t.FullName); builder.Append("#");
                builder.Append(t.IsArray); builder.Append("#");
            }
            return builder.ToString();
        }

        public enum ABI
        {
            MicrosoftAMD64,
            SytemVAMD64,
            Default
        }
        public delegate T Wrapper<T>(object[] Parameters);
        public static unsafe Wrapper<T> CreateNativeWrapper<T>(IntPtr Function, params object[] Parameters)
        {
            return CreateNativeWrapper<T>(Function, ABI.Default, Parameters);
        }

        internal static IntPtr CreateWindows64ABITwist(IntPtr Function)
        {
            List<byte> code = new List<byte>();
            AsmHelper_x86_64.ReserveStackSpace(32 + 8 * 6, code); // 32 byte of shadow space + 8 register that have to be preserved
            int StackAddrOffset = 40; // Reserve 32 Bytes of shadow space

            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, code); StackAddrOffset += 8;

            StackAddrOffset = 40;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;

            AsmHelper_x86_64.Call(Function, code);

            StackAddrOffset = 40;

            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, code); StackAddrOffset += 8;

            AsmHelper_x86_64.FreeStackSpace(code);
            AsmHelper_x86_64.Return(code);


            IntPtr twist = Fact.Internal.Os.Generic.MapMemory(4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Write);
            System.Runtime.InteropServices.Marshal.Copy(code.ToArray(), 0, twist, code.Count);
            Internal.Os.Generic.ChangeMemoryProtection(twist, 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute);
            return twist;
        }

        internal static IntPtr CreateSystemVABITwist(IntPtr Function)
        {
            List<byte> code = new List<byte>();
            AsmHelper_x86_64.ReserveStackSpace(32 + 8 * 6, code); // 32 byte of shadow space + 8 register that have to be preserved
            int StackAddrOffset = 40; // Reserve 32 Bytes of shadow space

            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, code); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, code); StackAddrOffset += 8;

            StackAddrOffset = 40;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;

            AsmHelper_x86_64.Call(Function, code);

            StackAddrOffset = 40;

            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, code); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, code); StackAddrOffset += 8;

            AsmHelper_x86_64.FreeStackSpace(code);
            AsmHelper_x86_64.Return(code);


            IntPtr twist = Fact.Internal.Os.Generic.MapMemory(4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Write);
            System.Runtime.InteropServices.Marshal.Copy(code.ToArray(), 0, twist, code.Count);
            Internal.Os.Generic.ChangeMemoryProtection(twist, 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute);
            return twist;
        }

        delegate void _start();
        internal static void SystemVABI_StartCall(Memory.VirtualAddressSpace VASpace, IntPtr Function, int Argc, char** Argv)
        {
            List<byte> code = new List<byte>();

            // Create stack
            IntPtr stackAlloc = VASpace.MapMemory(4096 * 1024 * 2, Memory.VirtualAddressSpace.MemoryProtection.Read | Memory.VirtualAddressSpace.MemoryProtection.Write); // Fact create a stack of 4MB by default (+ 4MB reserved for Fact)
            IntPtr stack = new IntPtr(stackAlloc.ToInt64() + 4096 * 1024);

            ulong* value = (ulong*)stack.ToPointer();
            *value = (ulong)Argc; value++;
            for (int n = 0; n < Argc; n++)
            {
                *value = (ulong)Argv[n]; value++;
            }
            *value = 0; value++;
            // Env pointer
            *value = 0; value++;

            // Nll auxiliaey vector entry
            *value = 0; value++;
            *value = 0; value++;

            AsmHelper_x86_64.SetRegister(AsmHelper_x86_64.Register.RSP, (ulong)stack.ToInt64(), code);
            AsmHelper_x86_64.SetRegister(AsmHelper_x86_64.Register.RBP, (ulong)stack.ToInt64(), code);

            AsmHelper_x86_64.SetRegister(AsmHelper_x86_64.Register.RAX, (ulong)Function.ToInt64(), code);
            AsmHelper_x86_64.JumpOnRetValue(code);
            //AsmHelper_x86_64.Return();


            IntPtr execChunk = Fact.Internal.Os.Generic.MapMemory(4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Write);
            System.Runtime.InteropServices.Marshal.Copy(code.ToArray(), 0, execChunk, code.Count);
            Internal.Os.Generic.ChangeMemoryProtection(execChunk, 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute);
            _start delegateCode = (_start)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(execChunk, typeof(_start));
            delegateCode();

        }


        public static unsafe Wrapper<T> CreateNativeWrapper<T>(IntPtr Function, ABI ABI, params object[] Parameters)
        {
            bool MS_AMD64_To_SystemV_Twist = false;

            if (Parameters == null) { Parameters = new object[0]; }
            System.Type[] types = new System.Type[Parameters.Length];
            for (int n = 0; n < Parameters.Length; n++)
            {
                if (Parameters[n] is string) { types[n] = typeof(IntPtr); }
                else if (Parameters[n] is double) { types[n] = typeof(double); }
                else if (Parameters[n] is float) { types[n] = typeof(float); }
                else if (Parameters[n] is int) { types[n] = typeof(int); }
                else if (Parameters[n] is uint) { types[n] = typeof(uint); }
                else if (Parameters[n] is long) { types[n] = typeof(long); }
                else if (Parameters[n] is ulong) { types[n] = typeof(ulong); }
                else { types[n] = typeof(IntPtr); }
            }

            System.Type DelegateType = null;
            bool resultIsString = false;
            if (typeof(T).FullName == typeof(string).FullName) { resultIsString = true; DelegateType = Internal.Reflection.MSILDelegate.CreateDelegateType(typeof(IntPtr), types); }
            else { DelegateType = Internal.Reflection.MSILDelegate.CreateDelegateType(typeof(T), types); }

            if (Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftWindows ||
                Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftXBoxOne)
            {
                if (ABI == ABI.SytemVAMD64)
                {
                    Function = CreateSystemVABITwist(Function);
                }
            }

            Delegate Delegate = System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(Function, DelegateType);
            return (object[] Params) =>
            {
                if (Params == null) { Params = new object[0]; }
                object[] pointers = new object[Params.Length];
                for (int n = 0; n < Params.Length; n++)
                {
                    if (Parameters[n] is string) { pointers[n] = new IntPtr(ToCStr(Params[n] as string)); types[n] = typeof(IntPtr); }
                    else if (Parameters[n] is double) { pointers[n] = Params[n]; }
                    else if (Parameters[n] is float) { pointers[n] = Params[n]; }
                    else if (Parameters[n] is int) { pointers[n] = Params[n]; }
                    else if (Parameters[n] is uint) { pointers[n] = Params[n]; }
                    else if (Parameters[n] is long) { pointers[n] = Params[n]; }
                    else if (Parameters[n] is ulong) { pointers[n] = Params[n]; }
                    else { pointers[n] = new IntPtr(ObjectToPointer(Params[n])); }
                }
                object result = Delegate.DynamicInvoke(pointers);

                if (resultIsString)
                {
                    return Cast<T>(FromCStr(((IntPtr)result).ToPointer()));
                }
                else
                {
                    if (result is IntPtr) { return PointerToObject<T>(((IntPtr)result).ToPointer()); }
                    if (result is float) { return Cast<T>(result); }
                    if (result is double) { return Cast<T>(result); }
                    if (result is int) { return Cast<T>(result); }
                    if (result is uint) { return Cast<T>(result); }
                    if (result is byte) { return Cast<T>(result); }
                    if (result is char) { return Cast<T>(result); }
                    if (result is long) { return Cast<T>(result); }
                    if (result is ulong) { return Cast<T>(result); }

                    return default(T);
                }

                // Temp ressource freeing is the last thing we need to do.
                for (int n = 0; n < Parameters.Length; n++)
                {
                    if (Parameters[n] is string) { System.Runtime.InteropServices.Marshal.FreeHGlobal((IntPtr)pointers[n]); }
                }
            };
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection.BuiltIn.Python
{
    public class Code
    {
        public class InteractiveInterpreter
        {
            public InteractiveInterpreter(object Locals) { }
            public object runsource(string source) { return runsource(source, "<input>", "single"); }
            public object runsource(string source, string filename) { return runsource(source, filename, "single"); }
            public object runsource(string source, string filename, string symbol)
            {
                Parser.Python.Scope root = Parser.Python.Parse(source);
                return true;
            }
        }
    }
}

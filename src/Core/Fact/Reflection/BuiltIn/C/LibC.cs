﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection.BuiltIn.C
{
    class LibC
    {
        Delegate GetUncheckFunctionPointer(string Name)
        {
            return Tools.GetDelegateForMethod(typeof(LibC).GetMethod(Name));
        }

        Delegate GetUncheckFunctionPointerWithThisPointer(string Name)
        {
            return Tools.GetDelegateForMethod(this, typeof(LibC).GetMethod(Name));
        }


        Memory.VirtualAddressSpace _VASpace = null;
        public LibC(Memory.VirtualAddressSpace VASpace)
        {
            _VASpace = VASpace;
        }


        public unsafe Delegate GetFunctionPointer(string Name)
        {
#if DEBUG
            Fact.Log.Debug("Trying to find " + Name + " in fact internal libc");
#endif
            switch (Name)
            {
                // C-Math:
                case "cos": return GetUncheckFunctionPointer("cos");
                case "sin": return GetUncheckFunctionPointer("sin");
                case "tan": return GetUncheckFunctionPointer("tan");

                case "acos": return GetUncheckFunctionPointer("acos");
                case "asin": return GetUncheckFunctionPointer("asin");
                case "atan": return GetUncheckFunctionPointer("atan");

                case "atan2": return GetUncheckFunctionPointer("atan2");

                case "cosh": return GetUncheckFunctionPointer("cosh");
                case "sinh": return GetUncheckFunctionPointer("sinh");
                case "tanh": return GetUncheckFunctionPointer("tanh");

                case "exp": return GetUncheckFunctionPointer("exp");
                case "log": return GetUncheckFunctionPointer("log");
                case "log10": return GetUncheckFunctionPointer("log10");

                case "pow": return GetUncheckFunctionPointer("pow");
                case "sqrt": return GetUncheckFunctionPointer("sqrt");

                case "ceil": return GetUncheckFunctionPointer("ceil");
                case "floor": return GetUncheckFunctionPointer("floor");
                case "fabs": return GetUncheckFunctionPointer("fabs");
                case "abs": return GetUncheckFunctionPointer("abs");

                /* C-Time */
                case "clock": return GetUncheckFunctionPointer("clock");

                /* Stdlib */
                case "atoi": return GetUncheckFunctionPointer("atoi");
                case "atof": return GetUncheckFunctionPointer("atof");
                case "atol": return GetUncheckFunctionPointer("atol");

                case "putchar": return GetUncheckFunctionPointer("putchar");
                case "getchar": return GetUncheckFunctionPointer("getchar");
                case "strcmp": return GetUncheckFunctionPointer("strcmp");



                /* c-type */
                case "isalnum": return GetUncheckFunctionPointer("isalnum");
                case "isalpha": return GetUncheckFunctionPointer("isalpha");
                case "iscntrl": return GetUncheckFunctionPointer("iscntrl");
                case "isdigit": return GetUncheckFunctionPointer("isdigit");
                case "isgraph": return GetUncheckFunctionPointer("isgraph");
                case "islower": return GetUncheckFunctionPointer("islower");
                case "isupper": return GetUncheckFunctionPointer("isupper");
                case "isspace": return GetUncheckFunctionPointer("isspace");
                case "ispunct": return GetUncheckFunctionPointer("ispunct");
                case "isprint": return GetUncheckFunctionPointer("isprint");
                case "isxdigit": return GetUncheckFunctionPointer("isxdigit");



                /* allocator */
                case "malloc": return GetUncheckFunctionPointerWithThisPointer("malloc");
                case "free": return GetUncheckFunctionPointerWithThisPointer("free");

                /* locale */
                case "bindtextdomain": return GetUncheckFunctionPointerWithThisPointer("bindtextdomain");
                case "textdomain": return GetUncheckFunctionPointerWithThisPointer("textdomain");

                /* GLIBC */
                case "__gmon_start__": return GetUncheckFunctionPointer("nop");
                case "_ITM_registerTMCloneTable": return GetUncheckFunctionPointer("nop");

                default: return null;
            }
        }

        Dictionary<UInt64, UInt64> _malloc_huge_blocks = new Dictionary<UInt64, UInt64>();

        unsafe void* mallocSuballocator(int size)
        {
            IntPtr result = _VASpace.MapMemory((int)size, Memory.VirtualAddressSpace.MemoryProtection.Read | Memory.VirtualAddressSpace.MemoryProtection.Write);
            return result.ToPointer();
        }

        unsafe void freeSuballocator(void* pointer)
        {
        }

        public unsafe delegate void* mallocDelegate(void* size);
        public unsafe void* malloc(void* size)
        {
            if (size == null) { return null; }
            UInt64 realSize = (UInt64)size;
            if (realSize >= 4096)
            {
                IntPtr result = _VASpace.MapMemory((int)realSize, Memory.VirtualAddressSpace.MemoryProtection.Read | Memory.VirtualAddressSpace.MemoryProtection.Write);
                lock (_malloc_huge_blocks)
                {
                    _malloc_huge_blocks.Add((UInt64)result.ToInt64(), realSize);
                    return result.ToPointer();
                }
            }
            return mallocSuballocator((int)realSize);
        }

        public unsafe delegate void freeDelegate(void* pointer);
        public unsafe void free(void* pointer)
        {
            if (pointer == null) { return; }
            lock (_malloc_huge_blocks)
            {
                if (_malloc_huge_blocks.ContainsKey((ulong)pointer))
                {
                    _VASpace.UnmapMemory(new IntPtr(pointer), (int)_malloc_huge_blocks[(ulong)pointer]);
                    _malloc_huge_blocks.Remove((ulong)pointer);
                    return;
                }
            }

            freeSuballocator(pointer);
        }

        public static void nop() { }

        public static int putchar(int c) { try { System.Console.Out.Write(((char)c).ToString()); System.Console.Out.Flush(); return c; } catch { return -1; } }
        public static int getchar() { try { return System.Console.Read(); } catch { return -1; } }

        public static double cos(double x) { return System.Math.Cos(x); }
        public static double sin(double x) { return System.Math.Sin(x); }
        public static double tan(double x) { return System.Math.Tan(x); }

        public static double acos(double x) { return System.Math.Acos(x); }
        public static double asin(double x) { return System.Math.Asin(x); }
        public static double atan(double x) { return System.Math.Atan(x); }

        public static double atan2(double y, double x) { return System.Math.Atan2(y, x); }

        public static double cosh(double x) { return System.Math.Cosh(x); }
        public static double sinh(double x) { return System.Math.Sinh(x); }
        public static double tanh(double x) { return System.Math.Tanh(x); }

        public static double exp(double x) { return System.Math.Exp(x); }
        public static double log(double x) { return System.Math.Log(x); }
        public static double log10(double x) { return System.Math.Log10(x); }

        public static double pow(double x, double y) { return System.Math.Pow(x, y); }
        public static double sqrt(double x) { return System.Math.Sqrt(x); }


        public static double ceil(double x) { return System.Math.Ceiling(x); }
        public static double floor(double x) { return System.Math.Floor(x); }
        public static double fabs(double x) { return System.Math.Abs(x); }
        public static double abs(double x) { return System.Math.Abs(x); }

        public static long clock() { return DateTime.UtcNow.Ticks; }

        public static int atoi(string text) { int result = 0;  if (int.TryParse(text, out result)) { return result; } return 0; }
        public static double atof(string text) { double result = 0; if (double.TryParse(text, out result)) { return result; } return 0; }
        public static long atol(string text) { long result = 0; if (long.TryParse(text, out result)) { return result; } return 0; }

        public static bool isalnum(int c) { try { return char.IsLetterOrDigit((char)c); } catch { return false; } }
        public static bool isalpha(int c) { try { return char.IsLetter((char)c); } catch { return false; } }
        public static bool isdigit(int c) { try { return char.IsDigit((char)c); } catch { return false; } }
        public static bool iscntrl(int c) { try { return char.IsControl((char)c); } catch { return false; } }
        public static bool isgraph(int c) { try { return char.IsSymbol((char)c); } catch { return false; } }
        public static bool islower(int c) { try { return char.IsLower((char)c); } catch { return false; } }
        public static bool isupper(int c) { try { return char.IsUpper((char)c); } catch { return false; } }
        public static bool isspace(int c) { try { return char.IsWhiteSpace((char)c); } catch { return false; } }
        public static bool ispunct(int c) { try { return char.IsPunctuation((char)c); } catch { return false; } }
        public static bool isprint(int c) { try { return !char.IsWhiteSpace((char)c) && !char.IsControl((char)c); } catch { return false; } }

        public static bool isxdigit(int c)
        {
            try
            {
                if (char.IsDigit((char)c)) { return true; }
                char cc = char.ToLower((char)c);
                return cc == 'a' || cc == 'b' || cc == 'c' || cc == 'd' || cc == 'e' || cc == 'f';
            }
            catch { return false; }
        }

        public static unsafe int strcmp(byte* a, byte* b)
        {
            if (a == null && b == null) { return 0; }
            if (a == null) { return -1; }
            if (b == null) { return 1; }
            while (*a != '\0' && *b != '\0' && *a == *b)
            {
                a++;
                b++;
            }
            int diff = ((int)*a) - ((int)*b);
            if (diff > 0) { return 1; }
            if (diff < 0) { return -1; }
            return 0;
        }

        public unsafe char* bindtextdomain()
        {
            return null;
        }

        public unsafe char* textdomain(string domain)
        {
            return null;
        }

    }
}

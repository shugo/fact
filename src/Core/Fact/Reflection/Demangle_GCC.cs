﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    internal class Demangle_GCC
    {
        internal static void demangle(string name, out string FunctionName, out string[] Namespaces, out string ClassName, out string[] Parameters, out string ReturnType, out bool IsConst)
        {
            FunctionName = name;
            ClassName = "";
            Namespaces = null;
            Parameters = null;
            ReturnType = "";
            IsConst = false;
            if (!name.StartsWith("_Z")) { FunctionName = name; return; }
            name = name.Substring("_Z".Length);
            if (name.StartsWith("N"))
            {
                List<string> chunks = new List<string>();

                int offset = 0;
                // Namespace parsing
                name = name.Substring("N".Length);

                if (name.StartsWith("St"))
                {
                    chunks.Add("std");
                    name = name.Substring("St".Length);
                }
                if (name.StartsWith("Ss"))
                {
                    chunks.Add("std");
                    chunks.Add("string");
                    name = name.Substring("Ss".Length);
                }

                while (offset < name.Length && char.IsDigit(name[offset]))
                {
                    string chunk = ReadString(ref offset, name);
                    chunks.Add(chunk);
                }

                if (offset < name.Length && name[offset] == 'C' && chunks.Count >= 1)
                {
                    chunks.Add(chunks[chunks.Count - 1]);
                }
                else if (offset < name.Length && name[offset] == 'D' && chunks.Count >= 1)
                {
                    chunks.Add("~" + chunks[chunks.Count - 1]);
                }

                if (chunks.Count > 1)
                {
                    List<string> namespaces = new List<string>();
                    for (int n = 0; n < chunks.Count - 1; n++)
                    {
                        namespaces.Add(chunks[n]);
                    }
                    Namespaces = namespaces.ToArray();
                    FunctionName = chunks[chunks.Count - 1];
                }
                if (offset < name.Length)
                {
                    name = name.Substring(offset);
                }
                if (name.StartsWith("E")) { name = name.Substring("E".Length); }
            }
            else
            {
                int offset = 0;
                FunctionName = ReadString(ref offset, name);
                if (offset < name.Length)
                {
                    name = name.Substring(offset);
                }
            }

            while (name.Length > 0)
            {
                int offset = 0;



                if (offset < name.Length && offset != 0)
                {
                    name = name.Substring(offset);
                }
                else { break; }
            }
        }

        internal static string ReadString(ref int Offset, string Input)
        {
            string length = "";
            for (; Offset < Input.Length; Offset++ )
            {
                if (char.IsDigit(Input[Offset]))
                {
                    length += Input[Offset];
                }
                else
                {
                    if (length != "")
                    {
                        break;
                    }
                }
            }

            int len = 0;
            string str = "";
            if (int.TryParse(length, out len))
            {
                for (int n = 0; n < len && Offset < Input.Length; n++, Offset++)
                {
                    str += Input[Offset];
                }
                return str;
            }
            else
            {
                return "";
            }
        }
    }
}

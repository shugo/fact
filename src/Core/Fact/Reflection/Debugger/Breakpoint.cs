﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection.Debugger
{
    public unsafe class Breakpoint
    {
        internal IntPtr _TrapCode = new IntPtr();
        internal IntPtr _Scratchpad = new IntPtr();
        Memory.VirtualAddressSpace _VASpace = null;
        delegate void BreakpointEnter();

        void Parse_x86_64_Scratchpad(BreakpointInfo BreakpointInfo)
        {
        }

        void Generate_x86_64_Trapcode()
        {
            BreakpointEnter _enter = () => { Enter(); };

            List<byte> breakpointEnter = new List<byte>();
            AsmHelper_x86_64.ReserveStackSpace(32 + 8 * 6, breakpointEnter); // 32 byte of shadow space + 8 register that have to be preserved
            int StackAddrOffset = 40; // Reserve 32 Bytes of shadow space

            // Save the registers

            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, breakpointEnter); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, breakpointEnter); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, breakpointEnter); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, breakpointEnter); StackAddrOffset += 8;
            if (sizeof(void*) == 8)
            {
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, breakpointEnter); StackAddrOffset += 8;
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, breakpointEnter); StackAddrOffset += 8;
            }

            AsmHelper_x86_64.CallDelegate(_enter, breakpointEnter);

            // Restore the registers
            StackAddrOffset = 40;

            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, breakpointEnter); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, breakpointEnter); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, breakpointEnter); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, breakpointEnter); StackAddrOffset += 8;
            if (sizeof(void*) == 8)
            {
                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, breakpointEnter); StackAddrOffset += 8;
                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, breakpointEnter); StackAddrOffset += 8;
            }

            AsmHelper_x86_64.FreeStackSpace(breakpointEnter);

            System.Runtime.InteropServices.Marshal.Copy(breakpointEnter.ToArray(), 0, _TrapCode, breakpointEnter.Count);
            _VASpace.ChangeMemoryProtection(_TrapCode, 4096, Memory.VirtualAddressSpace.MemoryProtection.Read | Memory.VirtualAddressSpace.MemoryProtection.Execute);

        }

        volatile bool _Continue = false;
        volatile bool _Pending = false;

        void Enter()
        {
            lock (this)
            {
                _Pending = true;
                _Continue = false;
                BreakpointInfo info = new BreakpointInfo();
                Parse_x86_64_Scratchpad(info);
                while (!_Continue) { System.Threading.Thread.Sleep(5); }
                _Pending = false;
                _Continue = false;
            }
        }

        public void Continue()
        {
            if (_Pending) { _Continue = true; }
        }

        internal Breakpoint(Memory.VirtualAddressSpace VASpace)
        {
            _TrapCode = VASpace.MapMemory(4096, Memory.VirtualAddressSpace.MemoryProtection.Read | Memory.VirtualAddressSpace.MemoryProtection.Write);
            _Scratchpad = VASpace.MapMemory(4096, Memory.VirtualAddressSpace.MemoryProtection.Read | Memory.VirtualAddressSpace.MemoryProtection.Write);

            Generate_x86_64_Trapcode();
        }
    }
}

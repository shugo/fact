﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection.Debugger
{
    public unsafe class Stack
    { 
        static IntPtr _ReadSpChunk;
        delegate void* _ReadSpDelegate();
        static _ReadSpDelegate _ReadSp;
        delegate void* _ReadSpInsideDelegate(void* sp);

        static Stack()
        {
            _ReadSpInsideDelegate readSpInside = (void* sp) =>
            {
                if (sizeof(void*) == 8)
                {
                    UInt64* rsp = (UInt64*)sp;
                    UInt64 current = (UInt64)(rsp);
                    while (true)
                    {
                        Fact.Log.Error(current.ToString("X"));
                        current = (UInt64)(rsp);
                        while (*rsp <= current || (*rsp - current > (4096 * 256))) { rsp++; }
                        rsp = (UInt64*)*rsp;
                    }
                    return (void*)(*(rsp));
                }
                else
                {
                    UInt32* esp = (UInt32*)sp;
                    esp = (UInt32*)*esp;
                    return (void*)(*(esp));
                }
            };

            _ReadSpChunk = Internal.Os.Generic.MapMemory(4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Write);
            List<byte> _ReadSpCode = new List<byte>();


            AsmHelper_x86_64.ReserveStackSpace(32 + 8 * 6, _ReadSpCode); // 32 byte of shadow space + 8 register that have to be preserved
            int StackAddrOffset = 40; // Reserve 32 Bytes of shadow space

            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, _ReadSpCode); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, _ReadSpCode); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, _ReadSpCode); StackAddrOffset += 8;
            AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, _ReadSpCode); StackAddrOffset += 8;
            if (sizeof(void*) == 8)
            {
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, _ReadSpCode); StackAddrOffset += 8;
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, _ReadSpCode); StackAddrOffset += 8;
            }
            AsmHelper_x86_64.Mov(AsmHelper_x86_64.Register.RCX, AsmHelper_x86_64.Register.RBP, _ReadSpCode);


            AsmHelper_x86_64.CallDelegate(readSpInside, _ReadSpCode);

            // Restore the registers
            StackAddrOffset = 40;

            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, _ReadSpCode); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, _ReadSpCode); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, _ReadSpCode); StackAddrOffset += 8;
            AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, _ReadSpCode); StackAddrOffset += 8;
            if (sizeof(void*) == 8)
            {
                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, _ReadSpCode); StackAddrOffset += 8;
                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, _ReadSpCode); StackAddrOffset += 8;
            }

            AsmHelper_x86_64.FreeStackSpace(_ReadSpCode);

            AsmHelper_x86_64.Return(_ReadSpCode);



            System.Runtime.InteropServices.Marshal.Copy(_ReadSpCode.ToArray(), 0, _ReadSpChunk, _ReadSpCode.Count);
            Internal.Os.Generic.ChangeMemoryProtection(_ReadSpChunk, 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute);
            _ReadSp = (_ReadSpDelegate)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(_ReadSpChunk, typeof(_ReadSpDelegate));

        }

        static internal ulong ReadSp()
        {
           void* sp = _ReadSp();
            return (ulong)sp;
        }

        public Stack()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection.Debugger
{
    public class BreakpointInfo
    {
        public class x86_64
        {
            public ulong RAX = 0;
            public ulong RBX = 0;
            public ulong RCX = 0;
            public ulong RDX = 0;

        }

        Breakpoint _Breakpoint = null;
        public Breakpoint Breakpoint {
            get { return _Breakpoint; }
        }

        Stack _Stack = null;
        public Stack Stack {
            get { return _Stack; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    unsafe partial class Java
    {
        class JavaType : Type
        {
            string _SuperClass = "";
            internal List<object> _Constants = new List<object>();
            internal List<object> _Interfaces = new List<object>();
            internal List<object> _Fields = new List<object>();

            internal void SetName(string Name) { _Name = Name; }
            internal void SetSuperClass(string Name) { _SuperClass = Name; }

        }

        static ulong ReadJava_Word(byte[] AssemblyData, int Offset)
        {
            return (ulong)AssemblyData[Offset] * 0x100 +
                    (ulong)AssemblyData[Offset + 1];
        }

        static string ReadJava_String(byte[] AssemblyData, ref int Offset)
        {
            ulong size = ReadJava_Word(AssemblyData, Offset); Offset += 2;
            string text = System.Text.UTF8Encoding.UTF8.GetString(AssemblyData, Offset, (int)size);
            Offset += (int)size;
            return text;
        }

        static ulong ReadJava_DWord(byte[] AssemblyData, int Offset)
        {
            return (ulong)AssemblyData[Offset + 3] +
                   (ulong)AssemblyData[Offset + 2] * 0x100 +
                   (ulong)AssemblyData[Offset + 1] * 0x10000 +
                   (ulong)AssemblyData[Offset] * 0x1000000;
        }



        public static Reflection.Type ReadJavaClass(byte[] Data)
        {
            JavaType javaType = new JavaType();

            int offset = 4; // Skip CAFEBABE
            ulong minorVersion = ReadJava_Word(Data, offset); offset += 2;
            ulong majorVersion = ReadJava_Word(Data, offset); offset += 2;
            ReadConstantPool(Data, ref offset, javaType);
            SolveConstantPool(javaType);
            ulong access = ReadJava_Word(Data, offset); offset += 2;
            ulong thisRef = ReadJava_Word(Data, offset); offset += 2;
            if ((int)thisRef >= javaType._Constants.Count)
            { throw new Exception("Invalid Java class"); }
            else
            {
                object thisEntry = javaType._Constants[(int)thisRef];
                if (thisEntry is string) { javaType.SetName(thisEntry as string); }
                else if (thisEntry is JavaClassRef) { javaType.SetName((thisEntry as JavaClassRef)._Name); }
            }
            ulong superRef = ReadJava_Word(Data, offset); offset += 2;
            if ((int)superRef >= javaType._Constants.Count)
            { throw new Exception("Invalid Java class"); }
            else
            {
                object superClass = javaType._Constants[(int)superRef];
                if (superClass is string) { javaType.SetSuperClass(superClass as string); }
                else if (superClass is JavaClassRef) { javaType.SetSuperClass((superClass as JavaClassRef)._Name); }
            }
            ReadInterface(Data, ref offset, javaType);
            ReadField(Data, ref offset, javaType);
            ReadMethod(Data, ref offset, javaType);


            return javaType;
        }

        static int Read_I32(byte[] Data, int Offset)
        { fixed (byte* pData = &Data[Offset]) { int* pTypeData = (int*)pData; return *pTypeData; } }
        static long Read_I64(byte[] Data, int Offset)
        { fixed (byte* pData = &Data[Offset]) { long* pTypeData = (long*)pData; return *pTypeData; } }
        static float Read_F32(byte[] Data, int Offset)
        { fixed (byte* pData = &Data[Offset]) { float* pTypeData = (float*)pData; return *pTypeData; } }
        static double Read_F64(byte[] Data, int Offset)
        { fixed (byte* pData = &Data[Offset]) { double* pTypeData = (double*)pData; return *pTypeData; } }


        class JavaClassRef
        {
            internal JavaType _Parent = null;
            internal int _Ref = 0;
            internal string _Name = "";
        }

        class JavaStringRef
        {
            internal JavaType _Parent = null;
            internal int _Ref = 0;
        }

        class JavaFieldRef
        {
            internal JavaType _Parent = null;
            internal int _ClassRef = 0;
            internal int _NameTypeRef = 0;
        }
        class JavaMethodRef
        {
            internal JavaType _Parent = null;
            internal int _ClassRef = 0;
            internal int _NameTypeRef = 0;
        }
        class JavaInterfaceRef
        {
            internal JavaType _Parent = null;
            internal int _ClassRef = 0;
            internal int _NameTypeRef = 0;
        }
        class JavaNameType
        {
            internal JavaType _Parent = null;
            internal int _NameRef = 0;
            internal int _TypeRef = 0;
            internal string _Name = "";
            internal string _Type = "";



            public override string ToString()
            {
                return "NameType: " + _Name + ";" + _Type;
            }
        }

        static void ReadConstantPool(byte[] Data, ref int Offset, JavaType Type)
        {
            unchecked
            {
                ulong size = ReadJava_Word(Data, Offset) - 2; Offset += 2;
                while (size > 0)
                {
                    byte tag = Data[Offset]; Offset++;
                    switch (tag)
                    {
                        case 1: Type._Constants.Add(ReadJava_String(Data, ref Offset)); break;
                        case 3: Type._Constants.Add(Read_I32(Data, Offset)); Offset += 4; break;
                        case 4: Type._Constants.Add(Read_F32(Data, Offset)); Offset += 4; break;
                        case 5: Type._Constants.Add(Read_I64(Data, Offset)); Offset += 8; break;
                        case 6: Type._Constants.Add(Read_F64(Data, Offset)); Offset += 8; break;
                        case 7: Type._Constants.Add(new JavaClassRef() { _Parent = Type, _Ref = (int)ReadJava_Word(Data, Offset) - 2}); Offset += 2; break;
                        case 8: Type._Constants.Add(new JavaStringRef() { _Parent = Type, _Ref = (int)ReadJava_Word(Data, Offset) - 2 }); Offset += 2; break;
                        case 9:
                            {
                                int classRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                int nameTypeRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                Type._Constants.Add(new JavaFieldRef() { _Parent = Type, _ClassRef = (int)classRef - 2, _NameTypeRef = (int)nameTypeRef - 2 });
                                break;
                            }
                        case 10:
                            {
                                int classRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                int nameTypeRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                Type._Constants.Add(new JavaMethodRef() { _Parent = Type, _ClassRef = (int)classRef - 2, _NameTypeRef = (int)nameTypeRef - 2});
                                break;
                            }
                        case 11:
                            {
                                int classRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                int nameTypeRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                Type._Constants.Add(new JavaInterfaceRef() { _Parent = Type, _ClassRef = (int)classRef - 2, _NameTypeRef = (int)nameTypeRef - 2 });
                                break;
                            }
                        case 12:
                            {
                                int nameRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                int typeRef = (int)ReadJava_Word(Data, Offset); Offset += 2;
                                Type._Constants.Add(new JavaNameType() { _Parent = Type, _NameRef = (int)nameRef - 2, _TypeRef = (int)typeRef - 2 });
                                break;
                            }
                        default: throw new Exception("Unsupported java constant type.");
                    }
                    size--;
                }
            }
        }

        static void ReadInterface(byte[] Data, ref int Offset, JavaType Type)
        {
            unchecked
            {
                ulong size = ReadJava_Word(Data, Offset); Offset += 2;
                while (size > 0)
                {
                    int interf = (int)ReadJava_Word(Data, Offset) - 2;
                    if (Type._Constants.Count >= interf) { Type._Interfaces.Add(Type._Constants[interf]); }
                    size--;
                }
            }
        }

        static void ReadField(byte[] Data, ref int Offset, JavaType Type)
        {
            unchecked
            {
                ulong size = ReadJava_Word(Data, Offset); Offset += 2;
                while (size > 0)
                {
                    int field = (int)ReadJava_Word(Data, Offset) - 2;
                    ulong accessflag = ReadJava_Word(Data, Offset);

                    if (Type._Constants.Count >= field)
                    {
                        Type._Fields.Add(Type._Constants[field]);
                    }
                    size--;
                }
            }
        }


        static void ReadMethod(byte[] Data, ref int Offset, JavaType Type)
        {
            unchecked
            {
                ulong size = ReadJava_Word(Data, Offset); Offset += 2;
                while (size > 0)
                {
                    int interf = (int)ReadJava_Word(Data, Offset) - 2;
                    if (Type._Constants.Count >= interf) { Type._Interfaces.Add(Type._Constants[interf]); }
                    size--;
                }
            }
        }


        static void SolveConstantPool(JavaType Type)
        {
            for (int n = 0; n < Type._Constants.Count; n++)
            {
                if (Type._Constants[n] is JavaNameType)
                {
                    JavaNameType nameType = Type._Constants[n] as JavaNameType;
                    if (nameType._TypeRef >= Type._Constants.Count) { throw new Exception("Invalid Java class"); }
                    if (nameType._NameRef >= Type._Constants.Count) { throw new Exception("Invalid Java class"); }
                    if (Type._Constants[nameType._NameRef] is string) { nameType._Name = Type._Constants[nameType._NameRef] as string; }
                    else { throw new Exception("Invalid Java class"); }
                    if (Type._Constants[nameType._TypeRef] is string) { nameType._Type = Type._Constants[nameType._TypeRef] as string; }
                    else { throw new Exception("Invalid Java class"); }


                }
            }

            for (int n = 0; n < Type._Constants.Count; n++)
            {
                if (Type._Constants[n] is JavaClassRef)
                {
                    JavaClassRef classRef = Type._Constants[n] as JavaClassRef;
                    if (classRef._Ref >= Type._Constants.Count) { throw new Exception("Invalid Java class"); }
                    if (Type._Constants[classRef._Ref] is string) { classRef._Name = Type._Constants[classRef._Ref - 1] as string; }
                    else { throw new Exception("Invalid Java class"); }
                }
            }
        }
    }
}

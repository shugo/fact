﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    internal class Demangle_CL
    {
        internal static void demangle(string name, out string FunctionName, out string[] Namespaces, out string[] Parameters, out string ReturnType)
        {
            if (name.Length == 0) 
            {
                Parameters = new string[] { "params void*" };
                Namespaces = null;
                FunctionName = name;
                ReturnType = "void*";
            }
            if (name[0] == '?') { demangle_CXX(name, out FunctionName, out Namespaces, out Parameters,  out ReturnType); return; }
            demangle_C(name, out FunctionName, out Namespaces, out Parameters, out ReturnType);
        }

        static void demangle_CXX(string name, out string FunctionName, out string[] Namespaces, out string[] Parameters, out string ReturnType)
        {
            int Offset = 1;
            List<string> fragments = demangle_fragments(name, ref Offset);
            if (fragments.Count > 0) { FunctionName = fragments[0]; } else { FunctionName = ""; }
            List<string> NamespacesList = new List<string>();
            Namespaces = NamespacesList.ToArray();
            Parameters = new string[] { "params void*" };
            ReturnType = "void*";
        }

        static List<string> demangle_fragments(string name, ref int Offset)
        {
            List<string> fragments = new List<string>();
            string functionname = "";
            do
            {
                for (; Offset < name.Length; Offset++)
                {
                    if (name[Offset] == '@') { Offset++; break; }
                    functionname += name[Offset];
                }
                if (functionname.Trim() != "")
                {
                    if (functionname.StartsWith("?$"))
                    {
                        functionname = functionname.Substring(2);
                        List<string> subfragments = demangle_fragments(name, ref Offset);
                        functionname += "<";

                        functionname += ">";
                    }
                    fragments.Add(functionname);
                }
            } while (functionname != "@");
            Offset++;
            return fragments;
        }

        static string demangle_Type(string name)
        {
            bool pointer = false;
            bool reference = false;

            string typename = "";
            if (name.Length == 0) { return ""; }
            int Offset = 0;
            while (Offset < name.Length)
            {
                if (name[Offset] == 'P') { pointer = true; Offset++; continue; }
                if (name[Offset] == 'A') { reference = true; Offset++; continue; }

                if (name[Offset] == 'C') { typename = "sbyte"; Offset++; continue; }
                if (name[Offset] == 'D') { typename = "byte"; Offset++; continue; }
                if (name[Offset] == 'E') { typename = "byte"; Offset++; continue; }
                if (name[Offset] == 'F') { typename = "short"; Offset++; continue; }
                if (name[Offset] == 'G') { typename = "ushort"; Offset++; continue; }
                if (name[Offset] == 'H') { typename = "int"; Offset++; continue; }
                if (name[Offset] == 'I') { typename = "uint"; Offset++; continue; }
                if (name[Offset] == 'J') { typename = "long"; Offset++; continue; }
                if (name[Offset] == 'k') { typename = "ulong"; Offset++; continue; }
                if (name[Offset] == 'L') { typename = "long long"; Offset++; continue; }
                if (name[Offset] == 'M') { typename = "float"; Offset++; continue; }
                if (name[Offset] == 'N') { typename = "double"; Offset++; continue; }
                if (name[Offset] == 'O') { typename = "long double"; Offset++; continue; }
                if (name[Offset] == 'W') { typename = "enum"; Offset++; continue; }
                if (name[Offset] == 'X') { if (pointer) { typename = "void"; Offset++; } else { return ""; } continue; }
                if (name[Offset] == 'Y') { return ""; }
                if (name[Offset] == 'Z') { return ""; }
            }
            if (pointer) { typename = typename + "*"; }
            if (reference) { typename = typename + "&"; }
            return typename;
        }

        static void demangle_C(string name, out string FunctionName, out string[] Namespaces, out string[] Parameters, out string ReturnType)
        {
            Parameters = new string[] { "params void*" };
            Namespaces = null;
            FunctionName = name;
            ReturnType = "void*";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    class AsmHelper_x86_64
    {
        public unsafe static void Return(List<byte> Output)
        {
            Output.Add(0xC3); // ret
        }

        public unsafe static void ReserveStackSpace(byte Size,  List<byte> Output)
        {
            Output.Add(0x55); // push bp
            if (sizeof(void*) == 8) { Output.Add(0x48); }
            Output.Add(0x89); Output.Add(0xE5); // mov bp, sp
            if (sizeof(void*) == 8) { Output.Add(0x48); }
            Output.Add(0x83); Output.Add(0xEC); Output.Add(Size); // reserve space
            if (sizeof(void*) == 8)
            {
                Output.Add(0x48); Output.Add(0x81); Output.Add(0xE4);
                Output.Add(0x00); Output.Add(0xFF); Output.Add(0xFF); Output.Add(0xFF);
            }
        }

        public unsafe static void FreeStackSpace(List<byte> Output)
        {
            if (sizeof(void*) == 8) { Output.Add(0x48); }
            Output.Add(0x89); Output.Add(0xEC); // mov sp, bp
            Output.Add(0x5D); // pop bp
        }

        public enum Register
        {
            RAX,
            RCX,
            RDX,
            RDI,
            RSI,
            R8,
            R9,
            R10,
            R11,
            RSP,
            RBP,
        }

        public unsafe static void SetRegister(Register Register, UInt64 Value, List<byte> Output)
        {
            if (sizeof(void*) == 8)
            {
                switch (Register)
                {
                    case AsmHelper_x86_64.Register.RAX:
                    case AsmHelper_x86_64.Register.RCX:
                    case AsmHelper_x86_64.Register.RDX:
                    case AsmHelper_x86_64.Register.RDI:
                    case AsmHelper_x86_64.Register.RSP:
                    case AsmHelper_x86_64.Register.RBP:
                    case AsmHelper_x86_64.Register.RSI: Output.Add(0x48); break;
                    case AsmHelper_x86_64.Register.R8:
                    case AsmHelper_x86_64.Register.R9:
                    case AsmHelper_x86_64.Register.R10:
                    case AsmHelper_x86_64.Register.R11: Output.Add(0x49); break;

                }
            }
            
            switch (Register)
            {
                case Register.RAX: Output.Add(0xB8); break;
                case Register.RSP: Output.Add(0xBC); break;
                case Register.RBP: Output.Add(0xBD); break;
                default: throw new Exception("Invalid register");
            }

            if (sizeof(void*) == 8)
            {
                Output.AddRange(BitConverter.GetBytes(Value));
            }
            else
            {
                Output.AddRange(BitConverter.GetBytes((UInt32)Value));
            }
        }

        public unsafe static void SaveRegisterToStack(int StackOffset, Register Register, List<byte> Output)
        {
            if (sizeof(void*) == 8)
            {
                switch (Register)
                {
                    case AsmHelper_x86_64.Register.RAX:
                    case AsmHelper_x86_64.Register.RCX:
                    case AsmHelper_x86_64.Register.RDX:
                    case AsmHelper_x86_64.Register.RDI:
                    case AsmHelper_x86_64.Register.RSI: Output.Add(0x48); break;
                    case AsmHelper_x86_64.Register.R8:
                    case AsmHelper_x86_64.Register.R9:
                    case AsmHelper_x86_64.Register.R10:
                    case AsmHelper_x86_64.Register.R11: Output.Add(0x4C); break;

                }
                
            }
            Output.Add(0x89);
            switch (Register)
            {
                case AsmHelper_x86_64.Register.RAX: Output.Add(0x44); break;
                case AsmHelper_x86_64.Register.RCX: Output.Add(0x4C); break;
                case AsmHelper_x86_64.Register.RDX: Output.Add(0x54); break;
                case AsmHelper_x86_64.Register.RDI: Output.Add(0x7C); break;
                case AsmHelper_x86_64.Register.RSI: Output.Add(0x74); break;
                case AsmHelper_x86_64.Register.R8: Output.Add(0x44); break;
                case AsmHelper_x86_64.Register.R9: Output.Add(0x4C); break;
                case AsmHelper_x86_64.Register.R10: Output.Add(0x54); break;
                case AsmHelper_x86_64.Register.R11: Output.Add(0x5C); break;
            }
            Output.Add(0x24);
            Output.Add((byte)StackOffset);
        }

        public unsafe static void Mov(Register Destination, Register Source, List<byte> Output)
        {
            if (Source == Destination) { return; }
            if (sizeof(void*) == 8) { Output.Add(0x48); }
            Output.Add(0x89);

            switch (Destination)
            {
                case Register.RAX:
                    switch (Source)
                    {
                        case Register.RCX: Output.Add(0xC8); break;
                        case Register.RDX: Output.Add(0xD0); break;
                        case Register.RDI: Output.Add(0xF8); break;
                        case Register.RSI: Output.Add(0xF0); break;
                        case Register.RSP: Output.Add(0xE0); break;
                        case Register.RBP: Output.Add(0xE8); break;

                        default: throw new Exception("Invalid source register");

                    }
                    break;
                case Register.RCX:
                    switch (Source)
                    {
                        case Register.RAX: Output.Add(0xC1); break;
                        case Register.RSP: Output.Add(0xE1); break;
                        case Register.RBP: Output.Add(0xE9); break;


                        default: throw new Exception("Invalid source register");

                    }
                    break;
                default:
                    throw new Exception("Invalid destination register");
            }

        }

        public unsafe static void Push(UInt64 Value, List<byte> Output)
        {

        }
        public unsafe static void AbsoluteJump(IntPtr Pointer, List<byte> Output)
        {
            Push((UInt64)Pointer.ToInt64(), Output);
            Return(Output); 
        }

        public unsafe static void LoadRegisterToStack(int StackOffset, Register Register, List<byte> Output)
        {
            if (sizeof(void*) == 8)
            {
                switch (Register)
                {
                    case AsmHelper_x86_64.Register.RAX:
                    case AsmHelper_x86_64.Register.RCX:
                    case AsmHelper_x86_64.Register.RDX:
                    case AsmHelper_x86_64.Register.RDI:
                    case AsmHelper_x86_64.Register.RSI: Output.Add(0x48); break;
                    case AsmHelper_x86_64.Register.R8:
                    case AsmHelper_x86_64.Register.R9:
                    case AsmHelper_x86_64.Register.R10:
                    case AsmHelper_x86_64.Register.R11: Output.Add(0x4C); break;

                }

            }
            Output.Add(0x8B);
            switch (Register)
            {
                case AsmHelper_x86_64.Register.RAX: Output.Add(0x44); break;
                case AsmHelper_x86_64.Register.RCX: Output.Add(0x4C); break;
                case AsmHelper_x86_64.Register.RDX: Output.Add(0x54); break;
                case AsmHelper_x86_64.Register.RDI: Output.Add(0x7C); break;
                case AsmHelper_x86_64.Register.RSI: Output.Add(0x74); break;
                case AsmHelper_x86_64.Register.R8: Output.Add(0x44); break;
                case AsmHelper_x86_64.Register.R9: Output.Add(0x4C); break;
                case AsmHelper_x86_64.Register.R10: Output.Add(0x54); break;
                case AsmHelper_x86_64.Register.R11: Output.Add(0x5C); break;
            }
            Output.Add(0x24);
            Output.Add((byte)StackOffset);
        }

        public unsafe static void CallDelegate(Delegate Delegate, List<byte> Output)
        {
            if (sizeof(void*) == 8)
            {
                Int64 ptr = System.Runtime.InteropServices.Marshal.GetFunctionPointerForDelegate(Delegate).ToInt64();
                Output.Add(0x48); Output.Add(0xB8); Output.AddRange(BitConverter.GetBytes(ptr));  // mov R1, link
            }
            else
            {
                Int32 ptr = System.Runtime.InteropServices.Marshal.GetFunctionPointerForDelegate(Delegate).ToInt32();
                Output.Add(0xB8); Output.AddRange(BitConverter.GetBytes(ptr));  // mov R1, link
            }
            Output.Add(0xFF); Output.Add(0xD0); // call R1
        }

        public unsafe static void Call(IntPtr ptr, List<byte> Output)
        {
            if (sizeof(void*) == 8)
            {
                Output.Add(0x48); Output.Add(0xB8); Output.AddRange(BitConverter.GetBytes(ptr.ToInt64()));  // mov R1, link
            }
            else
            {
                Output.Add(0xB8); Output.AddRange(BitConverter.GetBytes(ptr.ToInt32()));  // mov R1, link
            }
            Output.Add(0xFF); Output.Add(0xD0); // call R1
        }

        public unsafe static void JumpOnRetValue(List<byte> Output)
        {
            Output.Add(0xFF); Output.Add(0xE0); // jmp R1
        }

        public unsafe static void Break(List<byte> Output)
        {
            Output.Add(0xCC); // break
        }
    }
}

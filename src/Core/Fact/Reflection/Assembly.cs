﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public class Assembly
    {
        static Dictionary<string, WeakReference> _LoadedAssembly = new Dictionary<string, WeakReference>();

        public enum LoadingStatus
        {
            NotLoaded,
            LoadedForReflection,
            LoadedForReflectionWithDependencies,
            Loaded
        }

        protected LoadingStatus _LoadingStatus = LoadingStatus.NotLoaded;
        public LoadingStatus CurrentLoadingStatus { get { return _LoadingStatus; } }

        protected Dictionary<string, Type> _types = new Dictionary<string, Type>();
        protected Dictionary<string, Namespace> _Namespaces = new Dictionary<string, Namespace>();
        protected Dictionary<string, Assembly> _Depends = new Dictionary<string, Assembly>();
        protected List<Method> _Methods = new List<Method>();
        protected List<Method> _ImportedMethods = new List<Method>();

        protected string _Name = "";
        public string Name { get { return _Name; } }

        Memory.VirtualAddressSpace _VirtualAddressSpace = new Memory.VirtualAddressSpace();
        public Memory.VirtualAddressSpace VirtualAddressSpace  { get { return _VirtualAddressSpace; } }

        protected string _File = "";
        public string File { get { return _File; } }
        protected byte[] _Data = null;
        public byte[] Data { get { return _Data; } }

        internal string _UUID = Internal.Information.GenerateUUID();

        protected virtual void _Load() { }
        internal void __Load() { _Load(); }
        protected virtual void _LoadForReflection() { }
        internal void __LoadForReflection() { _LoadForReflection(); }
        protected virtual void _LoadDependencies() { }
        internal void __LoadDependencies() { __LoadDependencies(); }

        public List<Namespace> Namespaces
        {
            get
            {
                if (_LoadingStatus == LoadingStatus.NotLoaded)
                { _LoadForReflection(); }
                return new List<Namespace>(_Namespaces.Values);
            }
        }

        public List<Method> Methods
        {
            get
            {
                if (_LoadingStatus == LoadingStatus.NotLoaded)
                { _LoadForReflection(); }
                return new List<Method>(_Methods);
            }
        }

        public Method GetMethod(string MethodName)
        {
            foreach (Method method in _Methods)
            {
                if (method.Name == MethodName) { return method; }
            }
            return null;
        }

        public List<Method> ImportedMethods
        {
            get
            {
                if (_LoadingStatus == LoadingStatus.NotLoaded)
                { _LoadForReflection(); }
                if (_LoadingStatus == LoadingStatus.LoadedForReflection)
                { _LoadDependencies(); }
                return new List<Method>(_ImportedMethods);
            }
        }
        public Method GetImportedMethod(string MethodName)
        {
            foreach (Method method in ImportedMethods)
            {
                if (method.Name == MethodName) { return method; }
            }
            return null;
        }

        public static Assembly LoadFromFile(string File)
        {
            byte[] data = System.IO.File.ReadAllBytes(File);
            Assembly Assembly = LoadFromBytes(data);
            if (Assembly != null)
            {
                if (Assembly._Name == "")
                {
                    Assembly._Name = System.IO.Path.GetFileName(File);
                    try { Assembly._File = System.IO.Path.GetFullPath(File); }
                    catch { Assembly._File = File; }
                    Assembly._Data = data;
                }
            }
            return Assembly;
        }

        public static Assembly LoadFromFile(Fact.Processing.File File)
        {
            byte[] data = File.GetBytes();
            Assembly Assembly = LoadFromBytes(data);
            if (Assembly != null)
            {
                if (Assembly._Name == "")
                {
                    Assembly._Name = File.Name;
                    Assembly._Data = data;
                }
            }
            return Assembly;
        }

        internal string _Hash = "";
        public static Assembly LoadFromBytes(byte[] AssemblyData)
        {
            string strhash = "";

            System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512.Create();
            byte[] hash = sha512.ComputeHash(AssemblyData);
            for (int n = 0; n < hash.Length; n++) { strhash += hash[n].ToString("X2"); }
            lock (_LoadedAssembly)
            {
                if (_LoadedAssembly.ContainsKey(strhash))
                {
                    if (_LoadedAssembly[strhash].IsAlive)
                    {
                        return _LoadedAssembly[strhash].Target as Assembly;
                    }
                    else
                    {
                        _LoadedAssembly.Remove(strhash);
                    }
                }
            }


            System.IO.MemoryStream Stream = new System.IO.MemoryStream(AssemblyData);
            Assembly Assembly = null;
#if DEBUG
            Fact.Log.Debug("Loading new assembly ...");
#endif
            switch (Internal.File.GetExecutableType(Stream))
            {
                case Processing.File.ExecutableFormat.ExecutableAndLinkableFormat:
                    Assembly = Elf.ReadElf(AssemblyData);
                    break;
                case Processing.File.ExecutableFormat.PortableExecutable:
                    Assembly = Pe.ReadPE(AssemblyData);
                    break;
                case Processing.File.ExecutableFormat.JavaClass:
                    Type t = Java.ReadJavaClass(AssemblyData);
                    if (t != null)
                    {
                        Assembly = new Assembly();
                        Assembly._Name = t.Name;
                        Assembly._types.Add(t.Name, t);
                        break;
                    }
                    break;
                case Processing.File.ExecutableFormat.Python:
                    Python.Context ctx = Python.CreateContext();
                    ctx.LoadCode(System.Text.Encoding.UTF8.GetString(AssemblyData), "<input>");
                    Assembly = ctx;
                    break;
            }


            if (Assembly != null)
            {
                Assembly._Data = AssemblyData;
                Assembly._Hash = strhash;

#if DEBUG
                Fact.Log.Debug("Assembly " + Assembly.Name + " correctly loaded");
#endif
            }
            else
            {
#if DEBUG
                Fact.Log.Error("Impossible to load the specified assembly");
#endif
            }
            lock (_LoadedAssembly)
            {
                if (_LoadedAssembly.ContainsKey(strhash))
                {
                    if (!_LoadedAssembly[strhash].IsAlive)
                    {
                        _LoadedAssembly.Remove(strhash);
                    }
                }
                if (!_LoadedAssembly.ContainsKey(strhash))
                {
                    WeakReference weakref = new WeakReference(Assembly);
                    _LoadedAssembly.Add(strhash, weakref);
                }
            }
            return Assembly;
        }

        internal static Assembly GetAssemblyFromHash(string Hash)
        {
            lock (_LoadedAssembly)
            {
                if (_LoadedAssembly.ContainsKey(Hash))
                {
                    if (!_LoadedAssembly[Hash].IsAlive)
                    {
                        _LoadedAssembly.Remove(Hash);
                        return null;
                    }
                    else
                    {
                        return (_LoadedAssembly[Hash].Target as Assembly);
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public class ResolveImportedMethodEventArgs : EventArgs
        {
            Method _ResolvedMethod;
            public Method ResolvedMethod
            {
                get { lock (this) { return _ResolvedMethod; } }
                set { lock (this) { _ResolvedMethod = value; } }
            }

            string _AssemblyName = "";
            public string AssemblyName
            {
                get {  return _AssemblyName; }
            }

            string _MethodName = "";
            public string MethodName
            {
                get { return _MethodName; }
            }

            internal ResolveImportedMethodEventArgs(string MethodName, string AssemblyName)
            {
                _MethodName = MethodName;
                _AssemblyName = AssemblyName;
            }

        }
        public delegate void ResolveImportedMethodHandler(object sender, ResolveImportedMethodEventArgs e);
        public event ResolveImportedMethodHandler OnResolveImportedMethod;
        protected Method RaiseOnResolveImportedMethod(string MethodName, string AssemblyName)
        {
            ResolveImportedMethodEventArgs args = new ResolveImportedMethodEventArgs(MethodName, AssemblyName);
            lock (this)
            {
                if (OnResolveImportedMethod != null)
                {
                    OnResolveImportedMethod(this, args);
                }
            }
            return args.ResolvedMethod;
        }

        internal virtual void Serialize(System.IO.Stream Stream)
        {
            throw new Exception("Impossible to serialize this assembly");
        }

        internal static Assembly Deserialize(System.IO.Stream Stream)
        {
            Assembly assembly = null;
            switch (Stream.ReadByte())
            {
                case 0x2: assembly = Elf.Assembly_Elf.Deserialize(Stream); break;
                default: throw new Exception("Invalid method serialized");
            }

            lock (_LoadedAssembly)
            {
                if (_LoadedAssembly.ContainsKey(assembly._Hash))
                {
                    if (!_LoadedAssembly[assembly._Hash].IsAlive)
                    {
                        _LoadedAssembly.Remove(assembly._Hash);
                    }
                }
                if (!_LoadedAssembly.ContainsKey(assembly._Hash))
                {
                    WeakReference weakref = new WeakReference(assembly);
                    _LoadedAssembly.Add(assembly._Hash, weakref);
                }
            }

            return assembly;
        }

        protected MethodSolver _MethodSolver = null;
        public delegate Method MethodSolver(string MethodName, string TypeName, string AssemblyName);
        public virtual MethodSolver DefaultMethodSolver { get { return (string a, string b, string c) => { return null; }; } }
        public void SetMethodSolver(MethodSolver MethodSolver)
        {
            _MethodSolver = MethodSolver;
        }

        /// <summary>
        /// Call the entry  point of this assembly if there is one. Otherwise this function will throw an exception.
        /// </summary>
        /// <param name="Arguments">The arguments that should be sent to the program</param>
        /// <returns>The exit code of the program</returns>
        public virtual int Run(string[] Arguments)
        {
            throw new NotSupportedException("Not supported with this kind of assembly");
        }

        public override string ToString()
        {
            return _Name;
        }
    }
}

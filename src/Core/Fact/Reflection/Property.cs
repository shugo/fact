﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public class Property
    {
        protected Assembly _ParentAssembly = null;
        public Assembly ParentAssembly { get { return _ParentAssembly; } }
        protected string _Name = "";
        public string Name { get { return _Name; } }

        protected Method _Get = null;
        protected Method _Set = null;

        public Method Get { get { return _Get; } }
        public Method Set { get { return _Set; } }

        protected Dictionary<string, object> _attributes = new Dictionary<string, object>();
    }
}

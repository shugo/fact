﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public class Type
    {
        protected Assembly _ParentAssembly = null;
        public Assembly ParentAssembly { get { return _ParentAssembly; } }
        protected string _Name = "";
        public string Name { get { return _Name; } }

        protected Dictionary<string, Method> _methods = new Dictionary<string, Method>();
        protected Dictionary<string, Type> _types = new Dictionary<string, Type>();
        protected Dictionary<string, object> _attributes = new Dictionary<string, object>();

        public List<Method> Methods { get { return new List<Method>(_methods.Values); } }

    }
}

﻿// #define FULL_EMULATION
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Fact.Reflection.Debugger;

namespace Fact.Reflection
{
    class Elf
    {
        class Namespace_Elf : Namespace
        {
            internal Namespace_Elf(string Name) { _Name = Name; }
            internal bool ContainsNamespace(string Name) { return _Namespaces.ContainsKey(Name); }
            internal void AddNamespace(string Name, Namespace Namespace) { _Namespaces.Add(Name, Namespace); }
            internal Namespace_Elf GetElfNamespace(string Name) { return _Namespaces[Name] as Namespace_Elf; }
            internal void AddMethod(Method Method) { _methods.Add(Method); }
        }

        unsafe internal class Method_Elf : Method
        {
            bool _ThisCall = false;
            internal IntPtr _MethodPtr = new IntPtr(0);
            IntPtr _HookPtr = new IntPtr(0);
            internal Assembly_Elf.Symbol _Symbol = null;

            internal Dictionary<string, object> _Wrappers = new Dictionary<string, object>();
            static Fact.Emulation.Processor.Model _x86_64_Generic = new Emulation.Processor.Model(new Fact.Emulation.Processor.Models.x86_64_Generic());
            static Fact.Emulation.Processor.Model _ARM64_Generic = new Emulation.Processor.Model(new Fact.Emulation.Processor.Models.ARM64_Generic());
            static Fact.Emulation.Processor.Model _SPARC64_Generic = new Emulation.Processor.Model(new Fact.Emulation.Processor.Models.SPARC64_Generic());

            internal void SetName(string Name) { _Name = Name; }

            internal Method_Elf(Assembly_Elf.Symbol Symbol, Assembly_Elf Parent)
            {
                _Name = Symbol._Name;
                _ParentAssembly = Parent;
                _Symbol = Symbol;
            }

            protected override T _Invoke<T>(object This, params object[] Parameters)
            {
                IntPtr pointer = GetFunctionPointer();
                if (pointer.ToPointer() == null) { throw new Exception("Method " + _Name + " is not valid"); }
                if (!_ThisCall)
                {
#if DEBUG
                    Fact.Log.Debug("Invoking function " + _Name + " at 0x" + pointer.ToInt64().ToString("X"));
#endif
#if FULL_EMULATION
                     return Emulation.Execute.Call<T>(_ParentAssembly.VirtualAddressSpace, pointer, Emulation.Execute.ProcessorType.x86_64, Emulation.Execute.CallingConvention.SytemVAMD64, This, Parameters);
#endif
                    {
                        // Native Wrapper path
                        string signature = Tools.ComputeNativeSignature(pointer, typeof(T), Parameters);
                        Tools.Wrapper<T> wrapper = null;
                        lock (_Wrappers)
                        {
                            if (_Wrappers.ContainsKey(signature)) { wrapper = (Tools.Wrapper<T>)_Wrappers[signature]; }
                            else
                            {
                                wrapper = Tools.CreateNativeWrapper<T>(pointer, Tools.ABI.SytemVAMD64, Parameters);
                                _Wrappers.Add(signature, wrapper);
                            }
                        }
                        return wrapper(Parameters);
                    }
                }
                return default(T);
            }
            delegate void* FunctionPointer();

            internal IntPtr _GetFunctionPointer_NoResolve()
            {
                if (_HookMethod != null) { return _HookMethod.GetFunctionPointer(); }
                if (_Hook != null) { return System.Runtime.InteropServices.Marshal.GetFunctionPointerForDelegate(_Hook); }
                return _Symbol._MappedAddress;
            }

            public override IntPtr GetFunctionPointer()
            {
#if DEBUG
                Fact.Log.Debug("Getting a function pointer for method " + _Name + " ...");
#endif
                if (_HookMethod != null)
                {
                    if (_HookPtr.ToPointer() != null) { return _HookPtr; }

#if DEBUG
                    Fact.Log.Debug("Method is hooked on " + _HookMethod.Name + " call will be forwarded");
#endif
                    _HookPtr = _HookMethod.GetFunctionPointer();
                }
                if (_Hook != null)
                {
                    if (_HookPtr.ToPointer() != null) { return _HookPtr; }
#if DEBUG
                    Fact.Log.Debug("Method is hooked on on anonymous delegate");
#endif
                    _HookPtr = System.Runtime.InteropServices.Marshal.GetFunctionPointerForDelegate(_Hook);
                    Memory.MMU.RegisterSpecialAddress(_HookPtr, _Hook);
                    return _HookPtr;
                }
                if (_Symbol._MappedAddress.ToPointer() == null)
                {
                    Assembly assembly = this.ParentAssembly;
                    if (assembly == null) { return new IntPtr(0); }
                    if (assembly is Assembly_Elf)
                    {
#if DEBUG
                        Fact.Log.Debug("The assembly containing " + _Name + " has not been mapped yet");
#endif
                        (assembly as Assembly_Elf).Map();
                    }
                    if (_Symbol._MappedAddress.ToPointer() == null)
                    {
#if DEBUG
                        Fact.Log.Warning("Falling back on the native linked since fact has failed");
#endif
                        IntPtr asmptr = (assembly as Assembly_Elf)._AssemblyPtr;
                        if (asmptr.ToPointer() != null)
                        {
                            _Symbol._MappedAddress = Internal.Os.Generic.GetMethod(asmptr, _Name);
                        }
                    }
                    return _Symbol._MappedAddress;
                }
                return _Symbol._MappedAddress;
            }

            public override Breakpoint AddBreakpoint(int Offset)
            {
                Assembly assembly = this.ParentAssembly;

                if (_Symbol._MappedAddress.ToPointer() == null)
                {
                    if (assembly == null) { throw new Exception("Invalid method"); }
                    if (assembly is Assembly_Elf)
                    {
#if DEBUG
                        Fact.Log.Debug("The assembly containing " + _Name + " has not been mapped yet");
#endif
                        (assembly as Assembly_Elf).Map();
                    }

                }
                if (_Symbol._MappedAddress.ToPointer() != null)
                {
                    Debugger.Breakpoint breakpoint = new Breakpoint((assembly as Assembly_Elf).VirtualAddressSpace);
                    (assembly as Assembly_Elf).AddBreakpoint((ulong)_Symbol._MappedAddress.ToInt64() + (ulong)Offset, breakpoint);
                    return breakpoint;
                }
                else
                {
                    throw new Exception("Invalid method for breakpoint");
                }
            }


            protected override object _Invoke(object This, params object[] Parameters)
            {
                return (object)_Invoke<int>(This, Parameters);
            }

            internal Delegate _Hook = null;
            internal Method _HookMethod = null;
            public override void Hook(Delegate Method)
            {
                _HookPtr = new IntPtr(0);
                _Hook = Method;
                _HookMethod = null;
            }
            public override void Hook(Method Method)
            {
                _HookPtr = new IntPtr(0);
                _HookMethod = Method;
                _Hook = null;
            }

            internal override void Serialize(Stream Stream)
            {
                Internal.StreamTools.WriteUTF8String(Stream, _ParentAssembly._Hash);
                Stream.WriteByte(0x02);
                Internal.StreamTools.WriteUTF8String(Stream, _Symbol._Name);
                Internal.StreamTools.WriteBoolean(Stream, _ThisCall);
                Internal.StreamTools.WriteUInt64(Stream, (UInt64)_MethodPtr.ToInt64());
                Internal.StreamTools.WriteUInt64(Stream, (UInt64)_HookPtr.ToInt64());
            }

            internal static Method_Elf Deserialize(Stream Stream, Assembly ParentAssembly)
            {
                if (!(ParentAssembly is Assembly_Elf)) { throw new Exception("Invalid assembly: " + ParentAssembly.Name); }
                string name = Internal.StreamTools.ReadUTF8String(Stream);
                bool thisCall = Internal.StreamTools.ReadBoolean(Stream);
                UInt64 methodPtr = Internal.StreamTools.ReadUInt64(Stream);
                UInt64 hookPtr = Internal.StreamTools.ReadUInt64(Stream);
                Assembly_Elf.Symbol symbol = null;
                foreach (Assembly_Elf.Symbol sym in (ParentAssembly as Assembly_Elf)._Symbols)
                {
                    if (sym._Name == name) { symbol = sym; }
                }
                if (symbol == null) { throw new Exception("Invalid assembly: " + ParentAssembly.Name); }

                Method_Elf method = new Method_Elf(symbol, ParentAssembly as Assembly_Elf);
                return method;
            }
        }
        unsafe internal class Assembly_Elf : Assembly
        {
            internal enum SymbolType
            {
                STT_NOTYPE,
                STT_OBJECT,
                STT_FUNC,
                STT_SECTION,
                STT_FILE,
                STT_LOPROC,
                STT_HIPROC
            }
            internal enum RelocationType
            {
                R_NONE,
                R_32,
                R_PC32,
                R_GOT32,
                R_PLT32,
                R_COPY,
                R_GLOB_DAT,
                R_JMP_SLOT,
                R_RELATIVE,
                R_GOTOOFF,
                R_GOTPC,

                R_64,
                R_GOTPCREL,
                R_32S,
                R_16,
                R_PC16,
                R_8,
                R_PC8,
                R_DTPMOD64,
                R_DTPOFF64,
                R_TPOFF64,
                R_TLSGD,
                R_TLSLD,
                R_DTPOFF32,
                R_GOTTPOFF,
                R_TPOFF32,
                R_PC64,
                R_GOTOFF64,
                R_GOTPC32,
                R_SIZE32,
                R_SIZE64,
                R_GOTPC32_TLSDESC,
                R_TLSDESC_ALL,
                R_TLSDESC,
                R_IRELATIVE
            }
           
            internal BuiltIn.C.LibC _LibC = null;
            internal MapHelper _Map = null;
            internal IntPtr _AssemblyPtr = new IntPtr(0);
            internal bool _Mapped = false;
            internal bool _LittleEndian = true;
            internal bool _64BitsImage = false;
            internal List<Symbol> _Symbols = new List<Symbol>();
            internal List<Symbol> _FuntionSymbols = new List<Symbol>();
            internal List<Relocation> _Relocations = new List<Relocation>();
            internal ulong _EntryPoint = 0;
            internal Symbol _EntryPointSymbol = null;
            internal Got _Got = new Got();
            internal List<Section> _Sections = new List<Section>();



            internal List<string> _NeededDynamicDependencis = new List<string>();
            internal List<Assembly> _Dependencies = new List<Assembly>();

            internal override void Serialize(Stream Stream)
            {
                Stream.WriteByte(0x02);
                Internal.StreamTools.WriteUTF8String(Stream, _Hash);
                Internal.StreamTools.WriteUTF8String(Stream, _Name);
                Internal.StreamTools.WriteBoolean(Stream, _Mapped);
                if (_Mapped)
                {
                    throw new Exception("Impossible to serialize an already mapped assembly");
                }
                else
                {
                    Internal.StreamTools.WriteByteArray(Stream, _Data);
                }
            }

            internal static Assembly_Elf Deserialize(Stream Stream)
            {
                string hash = Internal.StreamTools.ReadUTF8String(Stream);
                string name = Internal.StreamTools.ReadUTF8String(Stream);
                bool mapped = Internal.StreamTools.ReadBoolean(Stream);
                if (mapped)
                {
                    throw new Exception("Impossible to deserialize an already mapped assembly");
                }
                else
                {
                    byte[] data = Internal.StreamTools.ReadByteArray(Stream);
                    Assembly_Elf elf = ReadElf(data) as Assembly_Elf;
                    elf._Name = name;
                    elf._Data = data;
                    elf._Hash = hash;
                    return elf;

                }
            }



            public Method EntryPoint
            {
                get
                {
                    if (_EntryPoint == 0) { return null; }
                    foreach (Symbol method in _FuntionSymbols)
                    {
                        if (method.__st_value == _EntryPoint) { return method._Method; }
                    }
                    return new Method_Elf(_EntryPointSymbol, this);
                }
            }

            public override int Run(string[] Arguments)
            {
                Method entryPoint = EntryPoint;
                if (entryPoint == null) { throw new Exception("This assembly is not an executable"); }
                byte** args = (byte**)System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeof(char*) * (Arguments.Length + 1)).ToPointer();
                for (int n = 0; n < Arguments.Length; n++)
                {
                    args[n] = Tools.ToCStr(Arguments[n]);
                }
                args[Arguments.Length] = null;

                Tools.SystemVABI_StartCall(this.VirtualAddressSpace, entryPoint.GetFunctionPointer(), Arguments.Length, (char**)args);
                for (int n = 0; n < Arguments.Length; n++)
                {
                    System.Runtime.InteropServices.Marshal.FreeHGlobal(new IntPtr(args[n]));
                }
                System.Runtime.InteropServices.Marshal.FreeHGlobal(new IntPtr(args));
                return 0;
            }

            protected override void _LoadDependencies()
            {
                if (_LoadingStatus != LoadingStatus.Loaded &&
                    _LoadingStatus != LoadingStatus.LoadedForReflectionWithDependencies)
                {
                    _SolveDependencies();
                    _LoadingStatus = LoadingStatus.LoadedForReflectionWithDependencies;
                }
            }


            internal void AddBreakpoint(ulong Address, Breakpoint Breakpoint)
            {
#if DEBUG
                Fact.Log.Debug("adding a new breakpoint at 0x" + Address.ToString("X"));
#endif


                List<byte> breakpointCode = new List<byte>();
                //AsmHelper_x86_64.JumpOnRetValueBreakpoint._TrapCode);

                ulong breakpointPage = Address & 4096;
                ulong breakpointPage2 = (Address + (ulong)breakpointCode.Count) & 4096;

                Internal.Os.Generic.ChangeMemoryProtection(new System.IntPtr((long)breakpointPage), 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute | Internal.Os.Generic.Protection.Write);
                Internal.Os.Generic.ChangeMemoryProtection(new System.IntPtr((long)breakpointPage2), 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute | Internal.Os.Generic.Protection.Write);

                // The code is short enought to be copied in one instr. by any smart memcopy.
                System.Runtime.InteropServices.Marshal.Copy(breakpointCode.ToArray(), 0, new IntPtr((long)Address), breakpointCode.Count);

                Internal.Os.Generic.ChangeMemoryProtection(new System.IntPtr((long)breakpointPage), 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute);
                Internal.Os.Generic.ChangeMemoryProtection(new System.IntPtr((long)breakpointPage2), 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute);
            }

            void _BuildDefaultLibs()
            {
                _LibC = new BuiltIn.C.LibC(VirtualAddressSpace);
            }

            delegate int __libc_start_main(void* main, int argc, char** ubp_av, void* init, void* rtld_fini, void* stack_end);
            delegate int _main(int argc, char** argv, char** env);


            void _SolveDependencies()
            {
#if DEBUG
                Fact.Log.Trace();
                Fact.Log.Debug("Start dependency solving ...");
#endif

                _BuildDefaultLibs();

                foreach (string lib in _NeededDynamicDependencis)
                {
                    try
                    {
                        Assembly assembly = _FindLibray(lib);
                        if (assembly != null)
                        {
                            _Dependencies.Add(assembly);
#if DEBUG
                            Fact.Log.Debug("Assembly " + assembly.Name + " added as a dependency");
#endif
                        }
                    }
                    catch (Exception e)
                    {
#if DEBUG
                        Fact.Log.Exception(e);
#endif
                    }
                }

#if DEBUG
                Fact.Log.Debug("Start foreign symbol solving ...");
#endif
                foreach (Symbol symbol in _Symbols)
                {
                    if (symbol.__st_value != 0) { continue; }
                    if (symbol._Name == "") { continue; }
                    if (symbol._Method != null)
                    {
                        if (symbol._Method is Method_Elf)
                        {
                            if ((symbol._Method as Method_Elf)._GetFunctionPointer_NoResolve().ToPointer() != null)
                            {
#if DEBUG
                                Fact.Log.Debug("Skipping symbol " + symbol._Name + " since it has already been resolved");
#endif
                                continue;
                            }
                            if ((symbol._Method as Method_Elf)._Hook != null ||
                                (symbol._Method as Method_Elf)._HookMethod != null)
                            {
#if DEBUG
                                Fact.Log.Debug("Skipping symbol " + symbol._Name + " since it is hooked");
#endif
                                continue;
                            }
                        }
                    }

                    {
#if DEBUG
                        Fact.Log.Debug("Call user event for to resolve method pointer on " + symbol._Name);
#endif
                        Method method = RaiseOnResolveImportedMethod(symbol._Name, "");
                        if (method != null) { symbol._Method = method; continue; }
                    }

                    // Special handler for libc init in _start
                    if (symbol!= null && symbol._Name == "__libc_start_main")
                    {
                        if (symbol._Method != null)
                        {
#if DEBUG
                            Fact.Log.Debug("Assembly is calling '__libc_start_main': Creating the bootstrab hook");
#endif
                            // __libc_start_main(int (*main) (int, char * *, char * *), int argc, char * * ubp_av, void (*init) (void), void (*fini) (void), void (*rtld_fini) (void), void (* stack_end));
                            symbol._Method.Hook((__libc_start_main)((void* main, int argc, char** ubp_av, void* init, void* rtld_fini, void* stack_end) =>
                            {
#if DEBUG
                                Fact.Log.Debug("Setting up program environment ...");
#endif
                                byte* mainptr = (byte*)main + _Map.Mapping.ToInt64();
#if DEBUG
                                Fact.Log.Debug("Computing main address (0x" + ((ulong)mainptr).ToString("X") + ")");
#endif
                                Method_Elf mainMethod = null;
                                foreach (Method_Elf _method in _Methods)
                                {
                                    if (_method.GetFunctionPointer().ToPointer() == mainptr)
                                    {
                                        mainMethod = _method;
                                        break;
                                    }
                                }

                                if (mainMethod != null)
                                {
#if DEBUG
                                    Fact.Log.Debug("Main method found at: " + mainMethod.GetFunctionPointer());
#endif
                                    return mainMethod.Invoke<int>(null, argc, new IntPtr(ubp_av), null);
                                }
                                else
                                {
#if DEBUG
                                    Fact.Log.Debug("Main method not found, creating a main methon on the fly " + new IntPtr(mainptr).ToInt64().ToString("X"));
#endif
                                    Symbol _FakeMain = new Symbol();
                                    _FakeMain._Name = "main";
                                    _FakeMain.__st_value = (ulong)main;
                                    _FakeMain._MappedAddress = new IntPtr(mainptr);
                                    mainMethod = new Method_Elf(_FakeMain, this);
                                    _Methods.Add(mainMethod);
                                    return mainMethod.Invoke<int>(null, argc, new IntPtr(ubp_av), null);
                                }
                            }));
                        }
                    }


                    if (_LibC != null)
                    {
                        Delegate func = _LibC.GetFunctionPointer(symbol._Name);
                        if (func != null)
                        {
                            if (symbol._Method == null)
                            {
                                symbol._Method = new Method_Elf(symbol, this);
                            }

#if DEBUG
                            Fact.Log.Debug("Symbols " + symbol._Name + " solved in internal fact libc");
#endif
                            symbol._Method.Hook(func);
                            continue;
                        }
                    }

                    foreach (Assembly assembly in _Dependencies)
                    {
                        if (assembly is Assembly_Elf)
                        {
                            foreach (Symbol targetSym in (assembly as Assembly_Elf)._Symbols)
                            {
                                if (targetSym == null) { continue; }
                                if (targetSym._Name == "") { continue; }
                                if (targetSym.__st_value == 0) { continue; }

                                if (targetSym._Name == symbol._Name)
                                {
#if DEBUG
                                    Fact.Log.Debug("Symbols " + symbol._Name + " solved in assembly " + assembly.Name);
#endif
                                    symbol._Assembly = assembly;
                                    if (symbol._Method is Method_Elf)
                                    {
                                        (symbol._Method as Method_Elf)._Symbol = targetSym;
                                    }
                                    else
                                    {
                                        symbol._Method = targetSym._Method;
                                    }
                                    symbol._MappedAddress = targetSym._MappedAddress;
                                    goto next;
                                }
                            }
                        }
                        else
                        {
                            foreach (Method targetSym in assembly.Methods)
                            {
                                if (targetSym.Name == symbol._Name)
                                {
                                    symbol._Assembly = assembly;
                                    symbol._Method = targetSym;
                                    goto next;
                                }
                            }
                        }
                    }
#if DEBUG
                    Fact.Log.Warning("Symbols " + symbol._Name + " has not been resolved yet");
#endif
                next: ;
                }

#if DEBUG
                Fact.Log.Debug("Dependency solving is now done");
#endif
            }

            Assembly _FindLibray(string Library)
            {
#if DEBUG
                Fact.Log.Debug("Locating library " + Library + " ...");
#endif
                string file = Fact.Internal.Information.Where(Library);
                if (file != "" && System.IO.File.Exists(file))
                {
#if DEBUG
                    Fact.Log.Debug("Library " + Library + " found in " + file);
#endif
                    return Assembly.LoadFromFile(file);
                }
#if DEBUG
                Fact.Log.Debug("Library " + Library + " not found");
#endif
                return null;
            }

            delegate void* DelayedLink();
            IntPtr _CreateDelayedLink(Symbol Symbol, void* Site)
            {
#if DEBUG
                Fact.Log.Trace();
#endif
                IntPtr trampoline = Fact.Internal.Os.Generic.MapMemory(4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Write);
                // Trampoline is a way to invoke a .NET Solver go back in the same context as the caller and then do again the invoke

                DelayedLink link = () =>
                {
#if DEBUG
                    Fact.Log.Trace();
                    if (Symbol == null || Symbol._Name == null)
                    {
                        Fact.Log.Debug("Invalid call. This usually occurs because the stack is corrupted");
                        return null;
                    }
                    Fact.Log.Debug("Start delayed symbol resolution for '" + Symbol._Name + "'");
#endif

                    // At that point the .NET is a complete mess don't play with it
                    Method method = Symbol._Method;

                    if (method == null || method.GetFunctionPointer().ToPointer() == null)
                    {
#if DEBUG
                        Fact.Log.Trace();
                        Fact.Log.Debug("run dependency solving for symbol '" + Symbol._Name + "' start with user resolution");
#endif
    
                        method = RaiseOnResolveImportedMethod(Symbol._Name, "");
                    }

                    if (method == null || method.GetFunctionPointer().ToPointer() == null)
                    {
#if DEBUG
                        Fact.Log.Trace();
                        Fact.Log.Debug("run dependency solving for symbol '" + Symbol._Name + "' (last chance)");
#endif
                        _SolveDependencies();
                        method = Symbol._Method;
                    }
                    if (method == null || method.GetFunctionPointer().ToPointer() == null)
                    {
#if DEBUG
                        Fact.Log.Error("Symbol '" + Symbol._Name + "' not resolved.");
#endif
                        throw new Exception("Fact Linker: Dynamic method not resolved: " + Symbol._Name);
                    }
                    void* ptr = method.GetFunctionPointer().ToPointer();
#if DEBUG
                    Fact.Log.Debug("Symbol '" + Symbol._Name + "' resolved at 0x" + ((ulong)ptr).ToString("X"));
#endif

                    if (sizeof(void*) == 8)
                    {
                        if (Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftWindows ||
                            Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftXBoxOne)
                        {
                            if (((method is Pe.Assembly_PE.Method_PE) ||
                                ((method is Method_Elf) &&
                                    ((method as Method_Elf)._Hook != null ||
                                     (method as Method_Elf)._HookMethod != null))))
                            {
                                ptr = Tools.CreateWindows64ABITwist(new IntPtr(ptr)).ToPointer();
                            }
                        }
                    }

                    if (sizeof(void*) == 4) { *((Int32*)Site) = (Int32)ptr; }
                    else { *((Int64*)Site) = (Int64)ptr; }

                    return ptr;
                };

                IntPtr linkPointer = System.Runtime.InteropServices.Marshal.GetFunctionPointerForDelegate(link);
                Memory.MMU.RegisterSpecialAddress(linkPointer, link);

                // Trampoline call
                List<byte> code = new List<byte>();
                AsmHelper_x86_64.ReserveStackSpace(48 + 8 * 8, code); // 40 byte of shadow space + 8 register that have to be preserved
                int StackAddrOffset = 48; // Reserve 40 Bytes of shadow space

                // Save the registers in the first crave-out
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, code); StackAddrOffset += 8;
                AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, code); StackAddrOffset += 8;
                if (sizeof(void*) == 8)
                {
                    AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, code); StackAddrOffset += 8;
                    AsmHelper_x86_64.SaveRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, code); StackAddrOffset += 8;
                }
                // For safty reason create a new clean crave-out
                AsmHelper_x86_64.ReserveStackSpace(64, code); // 64 bytes of pure shadow-space
                AsmHelper_x86_64.Call(linkPointer, code);
                AsmHelper_x86_64.FreeStackSpace(code);

                // Restore the registers
                StackAddrOffset = 48;

                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RCX, code); StackAddrOffset += 8;
                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDX, code); StackAddrOffset += 8;
                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RSI, code); StackAddrOffset += 8;
                AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.RDI, code); StackAddrOffset += 8;
                if (sizeof(void*) == 8)
                {
                    AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R8, code); StackAddrOffset += 8;
                    AsmHelper_x86_64.LoadRegisterToStack(StackAddrOffset, AsmHelper_x86_64.Register.R9, code); StackAddrOffset += 8;
                }

                AsmHelper_x86_64.FreeStackSpace(code);
                AsmHelper_x86_64.JumpOnRetValue(code);
#if DEBUG
                {
                    StringBuilder asm = new StringBuilder();
                    for (int n = 0; n < code.Count; n++)
                    {
                        asm.Append(" " + code[n].ToString("X2") + " ");
                    }
                    Fact.Log.Debug("Trampoilne code: " + asm.ToString().Trim());
                }
#endif
                System.Runtime.InteropServices.Marshal.Copy(code.ToArray(), 0, trampoline, code.Count);



                Internal.Os.Generic.ChangeMemoryProtection(trampoline, 4096, Internal.Os.Generic.Protection.Read | Internal.Os.Generic.Protection.Execute);
                return trampoline;

            }

            unsafe void _RelocSymbols(Symbol Symbol, ulong Location, RelocationType Type, long BaseAddress, long Addend)
            {
                unchecked
                {
                    UInt32* u32loc = (UInt32*)Location;
                    UInt64* u64loc = (UInt64*)Location;
                    Int32* s32loc = (Int32*)Location;
                    Int64* s64loc = (Int64*)Location;



                    switch (Type)
                    {
                        case RelocationType.R_RELATIVE:
                            if (_64BitsImage) { *u64loc = (ulong)(BaseAddress + Addend); }
                            else { *u32loc = (uint)(BaseAddress + Addend); }
                            break;
                        case RelocationType.R_64: *s64loc = ((long)Symbol.__st_value + Addend); break;
                        case RelocationType.R_PC32: *s32loc = (Int32)((long)Symbol.__st_value + Addend - (long)Location); break;
                        // case RelocationType.R_PC32: *s64loc = ((long)Symbol.__st_value + Addend - Location.ToInt64()); break;
                        case RelocationType.R_GLOB_DAT:
                        case RelocationType.R_JMP_SLOT:
                            if (Symbol.__st_value == 0)
                            {
#if DEBUG
                                Fact.Log.Debug("Solving at call will be setup for '" + Symbol._Name + "'");
#endif
                                IntPtr ptr = _CreateDelayedLink(Symbol, (void*)u64loc);
                                if (ptr.ToInt64() == 0)
                                {
                                    throw new Exception("Impossible to create the reloaction for " + Symbol._Name);
                                }
                                *u64loc = (ulong)ptr.ToInt64(); break;
                            }
                            else
                            {
                                if (Symbol._MappedAddress.ToInt64() != 0)
                                {
#if DEBUG
                                    Fact.Log.Debug("Jump slot for '" + Symbol._Name + "' created");
#endif
                                    *u64loc = (ulong)Symbol._MappedAddress.ToInt64(); break;
                                }
                                else
                                {
                                    Fact.Log.Warning("Trying to use an unmapped symbol '" + Symbol._Name + "' as a jump slot. It may segfault");
                                    *u64loc = Symbol.__st_value; break;
                                }
                            }
                            break;
                        default:
#if DEBUG
                            Fact.Log.Warning("Ignored reloc for symbol '" + Symbol._Name + "' at 0x" + Location.ToString("X") + " Type " + Type.ToString());
#endif
                            break;
                    }
                }
            }

            bool _MapSections()
            {
#if DEBUG
                Fact.Log.Trace();
#endif
                _LoadDependencies();
                Section _LastMappedSection = null;
                foreach (Section section in _Sections)
                {
                    if (section.__sh_addr != 0 && section._Protection != Memory.VirtualAddressSpace.MemoryProtection.None)
                    {
                        if (_LastMappedSection == null) { _LastMappedSection = section; }
                        if (_LastMappedSection.__sh_addr + _LastMappedSection.__sh_size < section.__sh_addr + section.__sh_size) { _LastMappedSection = section; }
                    }
                }

                ulong size = _LastMappedSection.__sh_size + _LastMappedSection.__sh_addr;
                _Map = new MapHelper(size, VirtualAddressSpace);
                IntPtr mapping = _Map.Mapping;
#if DEBUG
                Fact.Log.Debug("Map memory for assembly from 0x" + mapping.ToInt64().ToString("X") + " to 0x" + (mapping.ToInt64() + (long)size).ToString("X"));
#endif
                // Now place the section inside
                foreach (Section section in _Sections)
                {
                    if (section == null)
                    {
#if DEBUG
                        Fact.Log.Error("A null section has been added to the section list.");
#endif
                        continue;

                    }

                    if (section.__sh_addr != 0 && section._Protection != Memory.VirtualAddressSpace.MemoryProtection.None)
                    {
                        MapHelper.Section mappedSection = new MapHelper.Section();
                        mappedSection.Offset = (int)section.__sh_addr;
                        mappedSection.Protection = section._Protection;
                        mappedSection.Size = (int)section.__sh_size;
                        mappedSection.Name = section._Name;
                        _Map.MapSection(mappedSection);
                        section._Mapping = new IntPtr((long)section.__sh_addr + mapping.ToInt64());
#if DEBUG
                        Fact.Log.Debug("Section '" + section._Name + "' sh_addr 0x" + ((ulong)section.__sh_addr).ToString("X"));
                        Fact.Log.Debug("Section '" + section._Name + "' sh_offset 0x" + ((ulong)section.__sh_offset).ToString("X"));
                        Fact.Log.Debug("Section '" + section._Name + "' sh_size 0x" + ((ulong)section.__sh_size).ToString("X"));
                        Fact.Log.Debug("Section '" + section._Name + "' has protection " + mappedSection.Protection.ToString());
                        Fact.Log.Debug("Map section '" + section._Name + "' at 0x" + ((ulong)section._Mapping).ToString("X"));
#endif
                        if ((ulong)_Data.Length < section.__sh_offset + section.__sh_size)
                        {
#if DEBUG
                            Fact.Log.Warning("Padding section " + section.__sh_name + " with 0");
#endif
                        }
                        else
                        {
                            System.Runtime.InteropServices.Marshal.Copy(_Data, (int)section.__sh_offset, section._Mapping, (int)section.__sh_size);
                        }
                    }
                }

#if DEBUG
                Fact.Log.Debug("Starting symbols location solving ...");
#endif
                // Solve the symbols locations
                foreach (Symbol symbol in _Symbols)
                {
                    if (symbol._Section != null)
                    {
                        Section section = _FindSectionFromRequestedAddress(new IntPtr((long)symbol.__st_value));
                        if (section != null)
                        {
                            symbol._MappedAddress = new IntPtr((long)symbol.__st_value - (long)section.__sh_addr + section._Mapping.ToInt64());
                        }
                    }
                }

#if DEBUG
                Fact.Log.Debug("Starting symbols relocation ...");
#endif
                // Solve the relocations
                _SolveDependencies();
                foreach (Relocation reloc in _Relocations)
                {
                    if (reloc != null && reloc._TargetSection != null)
                    {
                        _RelocSymbols(reloc._Symbol, (ulong)reloc._TargetSection._Mapping.ToInt64() + reloc._r_Offset - reloc._TargetSection.__sh_addr, reloc._Type, _Map.Mapping.ToInt64(), reloc._r_AddEnd);
                    }
                }

                // Patch the entrypoint address
                if (_EntryPointSymbol != null)
                {
                    _EntryPointSymbol._MappedAddress = new IntPtr((long)_Map.Mapping.ToInt64() + (long)_EntryPointSymbol.__st_value);
                }

                _Map.ApplyProtection();
                return true;
            fail:
                foreach (Section section in _Sections)
                {
                    if (section.__sh_addr != 0 && section._Mapping.ToInt64() != 0)
                    {
                        Internal.Os.Generic.UnmapMemory(section._Mapping, (int)section.__sh_size);
                    }
                }
                return false;
            }

            internal void Map()
            {
                if (_Mapped) { return; }
                if (_MapSections())
                {
                    _Mapped = true;
                    return;
                }
                if (_AssemblyPtr.ToInt64() == 0)
                {
                    if (File != "")
                    {
                        _AssemblyPtr = Internal.Os.Generic.OpenLibrary(File);
                    }
                    else if (Data != null)
                    {
                        string emptyfolder = Fact.Tools.CreateTempDirectory();
                        System.IO.File.WriteAllBytes(emptyfolder + "/libloaded.so", Data);
                        _AssemblyPtr = Internal.Os.Generic.OpenLibrary(emptyfolder + "/libloaded.so");
                    }
                    _Mapped = true;
                }
            }

            internal Section _FindSectionFromFileAddress(IntPtr Address)
            {
                foreach (Section section in _Sections)
                {
                    if ((ulong)Address.ToInt64() >= section.__sh_offset &&
                        (ulong)Address.ToInt64() < section.__sh_offset + section.__sh_size)
                    {
                        return section;
                    }
                }
                return null;
            }

            internal Section _FindSectionFromRequestedAddress(IntPtr Address)
            {
                foreach (Section section in _Sections)
                {
                    if (section.__sh_addr == 0) continue;
                    if ((ulong)Address.ToInt64() >= section.__sh_addr &&
                        (ulong)Address.ToInt64() < section.__sh_addr + section.__sh_size)
                    {
                        return section;
                    }
                }
                return null;
            }

            internal class Got
            {
                internal Dictionary<ulong, ulong> _Locations = new Dictionary<ulong, ulong>();
                internal IntPtr _MappedAddress = new IntPtr();
            }

            internal class Symbol
            {
                internal ulong __st_name = 0;
                internal ulong __st_value = 0;
                internal ulong __st_size = 0;
                internal ulong __st_info = 0;
                internal ulong __st_info_bind = 0;
                internal ulong __st_info_type = 0;
                internal ulong __st_other = 0;
                internal ulong __st_shndx = 0;

                internal IntPtr _MappedAddress = new IntPtr();
                internal Section _Section = null;
                internal SymbolType _Type = SymbolType.STT_NOTYPE;
                internal ulong _Section_Offset = 0;
                internal string _Name = "";

                internal Assembly _Assembly = null;
                internal Method _Method = null;
                public override string ToString()
                {
                    return _Name;
                }
            }


            internal class Section
            {
                internal bool __StringTable = false;
                internal ulong __sh_name = 0;
                internal ulong __sh_type = 0;
                internal ulong __sh_flags = 0;
                internal ulong __sh_addr = 0;
                internal ulong __sh_offset = 0;
                internal ulong __sh_size = 0;
                internal ulong __sh_link = 0;
                internal ulong __sh_info = 0;
                internal ulong __sh_addralign = 0;
                internal ulong __sh_entsize = 0;

                internal IntPtr _Mapping = new IntPtr();

                internal  Memory.VirtualAddressSpace.MemoryProtection _Protection = Memory.VirtualAddressSpace.MemoryProtection.None;

                internal string _Name = "";

                internal List<Symbol> _Symbols = new List<Symbol>();

                public Section()
                { }
                public override string ToString()
                {
                    return " " + _Name;
                }
            }

            internal class Relocation
            {
                internal ulong _r_Offset;
                internal long _r_AddEnd;
                internal ulong _r_Symbols;

                internal Symbol _Symbol;
                internal RelocationType _Type;
                internal Section _SymbolSection;
                internal Section _TargetSection;

            }

            internal void _AddRelocation(Relocation Relocation)
            {
                _Relocations.Add(Relocation);
            }

            HashSet<string> _AddedMethodSym = new HashSet<string>();
            internal void _AddMethod(Symbol Symbol)
            {
                if (!_AddedMethodSym.Contains(Symbol._Name))
                {
                    _AddedMethodSym.Add(Symbol._Name);
                    if (Symbol._Name.StartsWith("_Z"))
                    {
                        string classname = "";
                        string functionName = "";
                        string[] _namespaces = null;
                        string[] parameterTypes = null;
                        Namespace_Elf currentNamespace = null;
                        string returnType = "";
                        bool isConst = false;

                        Demangle_GCC.demangle(Symbol._Name, out functionName, out _namespaces, out classname, out parameterTypes, out returnType, out isConst);
                        if (_namespaces != null && _namespaces.Length > 0)
                        {
                            if (!_Namespaces.ContainsKey(_namespaces[0]))
                            {
                                currentNamespace = new Namespace_Elf(_namespaces[0]);
                                _Namespaces.Add(currentNamespace.Name, currentNamespace);
                            }
                            else
                            {
                                currentNamespace = _Namespaces[_namespaces[0]] as Namespace_Elf;
                            }

                            for (int n = 1; n < _namespaces.Length; n++)
                            {
                                if (!currentNamespace.ContainsNamespace(_namespaces[n]))
                                {
                                    Namespace_Elf newNamespace = new Namespace_Elf(_namespaces[n]);
                                    currentNamespace.AddNamespace(_namespaces[n], newNamespace);
                                    currentNamespace = newNamespace;
                                }
                                else
                                {
                                    currentNamespace = currentNamespace.GetElfNamespace(_namespaces[n]);
                                }
                            }
                        }

                        Method_Elf method = new Method_Elf(Symbol, this);
                        if (functionName != "")
                        {
                            method.SetName(functionName);
                        }

                        if (currentNamespace == null)
                        {
                            _Methods.Add(method);
                        }
                        else
                        {
                            currentNamespace.AddMethod(method);
                        }
                    }
                    else
                    {
                        Method_Elf method = new Method_Elf(Symbol, this);
                        _Methods.Add(method);
                    }
                }
            }

            internal void _AddDependency(Assembly Assembly)
            {
                _Depends.Add(Assembly.Name, Assembly);
            }

            internal void _AddImportedMethod(Symbol Symbol)
            {
                Assembly parent = null;
                foreach (Assembly assembly in _Depends.Values)
                {
                    foreach (Method method in assembly.Methods)
                    {
                        Method _WAR_method = method;
                        if (method.Name == Symbol._Name)
                        {
                            Symbol._Method = _WAR_method;
                            _ImportedMethods.Add(_WAR_method);
                        }
                    }
                }
            end: ;
                {
                    Method_Elf method = new Method_Elf(Symbol, null);
                    Symbol._Method = method;
                    _ImportedMethods.Add(method);
                }
            }

            ~Assembly_Elf()
            {
                Internal.Os.Generic.CloseLibrary(_AssemblyPtr);
            }
        }

        internal static Assembly ReadElf(byte[] AssemblyData)
        {
            Assembly_Elf elf = new Assembly_Elf();
            return ReadElf(AssemblyData, elf);
        }

        static ulong ReadElf_Word(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            if (Elf._LittleEndian)
            {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100;
            }
            else
            {
                return (ulong)AssemblyData[Offset] * 0x100 +
                       (ulong)AssemblyData[Offset + 1];
            }
        }

        static ulong ReadElf_DWord(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            if (Elf._LittleEndian)
            {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000 +
                       (ulong)AssemblyData[Offset + 3] * 0x1000000;
            }
            else
            {
                return (ulong)AssemblyData[Offset + 3] +
                       (ulong)AssemblyData[Offset + 2] * 0x100 +
                       (ulong)AssemblyData[Offset + 1] * 0x10000 +
                       (ulong)AssemblyData[Offset] * 0x1000000;
            }
        }

        static ulong ReadElf_QWord(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            if (Elf._LittleEndian)
            {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000 +
                       (ulong)AssemblyData[Offset + 3] * 0x1000000 +
                       (ulong)AssemblyData[Offset + 4] * 0x100000000 +
                       (ulong)AssemblyData[Offset + 5] * 0x10000000000 +
                       (ulong)AssemblyData[Offset + 6] * 0x1000000000000 +
                       (ulong)AssemblyData[Offset + 7] * 0x100000000000000;
            }
            else
            {
                return (ulong)AssemblyData[Offset + 7] +
                       (ulong)AssemblyData[Offset + 6] * 0x100 +
                       (ulong)AssemblyData[Offset + 5] * 0x10000 +
                       (ulong)AssemblyData[Offset + 4] * 0x1000000 +
                       (ulong)AssemblyData[Offset + 3] * 0x100000000 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000000000 +
                       (ulong)AssemblyData[Offset + 1] * 0x1000000000000 +
                       (ulong)AssemblyData[Offset] * 0x100000000000000;
            }
        }

        static Assembly ReadElf(byte[] AssemblyData, Assembly_Elf Elf)
        {
            if (AssemblyData.Length < 0x32) { return null; }
            int bits = 0;
            bool LittleEndian = true;
            switch (AssemblyData[4])
            {
                case 1: Elf._64BitsImage = false; break;
                case 2: Elf._64BitsImage = true; break;
                default: return null;
            }
            if (AssemblyData.Length < 0x3E) { return null; }
            switch (AssemblyData[5])
            {
                case 1: Elf._LittleEndian = true; break;
                case 2: Elf._LittleEndian = false; break;
                default: return null;
            }

            ulong SectionTablePtr = 0;
            ulong SectionTableEntrySize = 0;
            ulong SectionTableEntryCount = 0;
            ulong SectionStringIndex = 0;

            if (Elf._64BitsImage) { Elf._EntryPoint = ReadElf_QWord(AssemblyData, 0x18, Elf); }
            else { Elf._EntryPoint = ReadElf_DWord(AssemblyData, 0x18, Elf); }

            if (Elf._EntryPoint > 0)
            {
                Elf._EntryPointSymbol = new Assembly_Elf.Symbol();
                Elf._EntryPointSymbol._Assembly = Elf;
                Elf._EntryPointSymbol._Name = "_start";
                Elf._EntryPointSymbol.__st_value = Elf._EntryPoint;
            }

            if (Elf._64BitsImage) { SectionTablePtr = ReadElf_QWord(AssemblyData, 0x28, Elf); }
            else { SectionTablePtr = ReadElf_DWord(AssemblyData, 0x20, Elf); }
            if (Elf._64BitsImage) { SectionTableEntrySize = ReadElf_Word(AssemblyData, 0x3A, Elf); }
            else { SectionTableEntrySize = ReadElf_Word(AssemblyData, 0x2E, Elf); }
            if (Elf._64BitsImage) { SectionTableEntryCount = ReadElf_Word(AssemblyData, 0x3C, Elf); }
            else { SectionTableEntryCount = ReadElf_Word(AssemblyData, 0x30, Elf); }
            if (Elf._64BitsImage) { SectionStringIndex = ReadElf_Word(AssemblyData, 0x3E, Elf); }
            else { SectionStringIndex = ReadElf_Word(AssemblyData, 0x32, Elf); }
            ReadElf_Sections(AssemblyData, (int)SectionTablePtr, SectionTableEntrySize, SectionTableEntryCount, SectionStringIndex, Elf);
            ReadElf_Symbols(AssemblyData, Elf);
            ReadElf_TagSymbols(Elf);
            return Elf;
        }

        static string ReadElf_ASCIIZ(byte[] AssemblyData, ulong Offset)
        {
            string value = "";
            for (int n = (int)Offset; n < AssemblyData.Length; n++)
            {
                if (AssemblyData[n] == 0) { return value; }
                value += (char)(AssemblyData[n]);
            }
            return value;
        }

        static void ReadElf_Sections(byte[] AssemblyData, int Offset, ulong EntrySize, ulong EntryCount, ulong StringTableIndex, Assembly_Elf Elf)
        {
            for (ulong n = 0; n < EntryCount; n++)
            {
                ReadElf_Section(AssemblyData, Offset, Elf);
                Offset += (int)EntrySize;
            }
            if (Elf._Sections.Count > (int)StringTableIndex)
            {
                Assembly_Elf.Section StringSection = Elf._Sections[(int)StringTableIndex];
                for (int n = 0; n < Elf._Sections.Count; n++)
                {
                    if (n == 0 && Elf._Sections[0].__sh_name == 0) { continue; }
                    ulong offset = (StringSection.__sh_offset) + (Elf._Sections[n].__sh_name);
                    Elf._Sections[n]._Name = ReadElf_ASCIIZ(AssemblyData, offset);
                }
            }
        }

        static void ReadElf_Section(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            Assembly_Elf.Section Section = new Assembly_Elf.Section();
            if (Elf._64BitsImage)
            {
                Section.__sh_name = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_type = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_flags = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_addr = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_offset = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_size = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_link = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_info = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_addralign = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_entsize = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
            }
            else
            {
                Section.__sh_name = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_type = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_flags = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_addr = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_offset = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_size = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_link = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_info = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_addralign = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_entsize = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
            }

            if ((Section.__sh_flags & 0x2) != 0x0)
            {
                Section._Protection = Section._Protection | Memory.VirtualAddressSpace.MemoryProtection.Read;
                if ((Section.__sh_flags & 0x1) != 0x0) { Section._Protection = Section._Protection | Memory.VirtualAddressSpace.MemoryProtection.Write; }
                if ((Section.__sh_flags & 0x4) != 0x0) { Section._Protection = Section._Protection | Memory.VirtualAddressSpace.MemoryProtection.Execute; }
            }




            Elf._Sections.Add(Section);
        }

        class DepTag { internal ulong Tag; internal ulong Value; }

        static void ReadElf_Symbols(byte[] AssemblyData, Assembly_Elf Elf)
        {
            // Find the dynamic sym section
            Assembly_Elf.Section SymTable = null;
            Assembly_Elf.Section DynSym = null;
            Assembly_Elf.Section Dynamic = null;

            Assembly_Elf.Section GotPlt = null;
            Assembly_Elf.Section Got = null;
            Assembly_Elf.Section Plt = null;
            Assembly_Elf.Section DynamicRelocations_Type0 = null;
            Assembly_Elf.Section PltRelocations_Type0 = null;
            Assembly_Elf.Section DynamicRelocations_Type1 = null;
            Assembly_Elf.Section PltRelocations_Type1 = null;



            for (int n = 0; n < Elf._Sections.Count; n++)
            {
                if (Elf._Sections[n]._Name == ".dynsym")
                {
                    DynSym = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".symtab")
                {
                    SymTable = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".got.plt")
                {
                    GotPlt = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".got")
                {
                    Got = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".plt")
                {
                    Plt = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".rel.dyn")
                {
                    DynamicRelocations_Type0 = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".rela.dyn")
                {
                    DynamicRelocations_Type1 = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".rel.plt")
                {
                    PltRelocations_Type0 = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".rela.plt")
                {
                    PltRelocations_Type1 = Elf._Sections[n];
                }
                if (Elf._Sections[n]._Name == ".dynamic")
                {
                    Dynamic = Elf._Sections[n];
                }
            }
            if (SymTable != null)
            {
                ReadElf_SymbolsTable(AssemblyData, SymTable.__sh_offset, SymTable.__sh_size, SymTable, Elf);
            }
            if (DynSym != null)
            {
                ReadElf_SymbolsTable(AssemblyData, DynSym.__sh_offset, DynSym.__sh_size, DynSym, Elf);
            }
            if (Got != null)
            {
                ReadElf_ReadGot(AssemblyData, Got.__sh_offset, Got.__sh_size, Got, Elf);
            }
            if (DynamicRelocations_Type0 != null)
            {
                ReadElf_ReadRelocation(AssemblyData, DynamicRelocations_Type0.__sh_offset, DynamicRelocations_Type0.__sh_size, false, DynamicRelocations_Type0, Elf);
            }
            if (DynamicRelocations_Type1 != null)
            {
                ReadElf_ReadRelocation(AssemblyData, DynamicRelocations_Type1.__sh_offset, DynamicRelocations_Type1.__sh_size, true, DynamicRelocations_Type1, Elf);
            }

            if (PltRelocations_Type0 != null)
            {
                ReadElf_ReadRelocation(AssemblyData, PltRelocations_Type0.__sh_offset, PltRelocations_Type0.__sh_size, false, PltRelocations_Type0, Elf);
            }
            if (PltRelocations_Type1 != null)
            {
                ReadElf_ReadRelocation(AssemblyData, PltRelocations_Type1.__sh_offset, PltRelocations_Type1.__sh_size, true, PltRelocations_Type1, Elf);
            }
            if (Dynamic != null)
            {
                ReadElf_Dependency(AssemblyData, Dynamic.__sh_offset, Dynamic.__sh_size, Dynamic, Elf);
            }
        }

        static Assembly_Elf.SymbolType ToSymbolType(ulong Type)
        {
            switch (Type)
            {
                case 0: return Assembly_Elf.SymbolType.STT_NOTYPE;
                case 1: return Assembly_Elf.SymbolType.STT_OBJECT;
                case 2: return Assembly_Elf.SymbolType.STT_FUNC;
                case 3: return Assembly_Elf.SymbolType.STT_SECTION;
                case 4: return Assembly_Elf.SymbolType.STT_FILE;
                case 13: return Assembly_Elf.SymbolType.STT_LOPROC;
                case 15: return Assembly_Elf.SymbolType.STT_HIPROC;
                default: return Assembly_Elf.SymbolType.STT_NOTYPE;
            }
        }

        static void ReadElf_Dependency(byte[] AssemblyData, ulong Offset, ulong Size, Assembly_Elf.Section Parent, Assembly_Elf Elf)
        {
            // Find the string table
            Assembly_Elf.Section StringTable = null;

            for (int n = 0; n < Elf._Sections.Count; n++)
            {
                if (Elf._Sections[n]._Name == ".dynstr")
                {
                    StringTable = Elf._Sections[n];
                    break;
                }
            }
            ulong end = Offset + Size;
            while (Offset < end)
            {
                if (Elf._64BitsImage)
                {
                    ulong d_val = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    ulong d_ptr = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    if (d_val != 0)
                    {
                        switch (d_val)
                        {
                            case 1:
                            {
                                if (d_ptr != 0 && StringTable != null)
                                {
                                    ulong nameoffset = StringTable.__sh_offset + d_ptr;
                                    string dep = ReadElf_ASCIIZ(AssemblyData, nameoffset);
                                    Elf._NeededDynamicDependencis.Add(dep);
                                }
                                break;
                            }
                        }
                    }
                }
                else
                {
                    ulong d_val = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    ulong d_ptr = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    ulong d_off = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    if (d_val != 0)
                    {
                        switch (d_val)
                        {
                            case 1:
                                {
                                    if (d_ptr != 0 && StringTable != null)
                                    {
                                        ulong nameoffset = StringTable.__sh_offset + d_ptr;
                                        string dep = ReadElf_ASCIIZ(AssemblyData, nameoffset);
                                        Elf._NeededDynamicDependencis.Add(dep);
                                    }
                                    break;
                                }
                        }
                    }

                }
            }
        }

        static void ReadElf_SymbolsTable(byte[] AssemblyData, ulong Offset, ulong Size, Assembly_Elf.Section Parent, Assembly_Elf Elf)
        {
            // Find the symbol string table
            Assembly_Elf.Section StringTable = null;

            for (int n = 0; n < Elf._Sections.Count; n++)
            {
                if (Parent._Name == ".symtab" && Elf._Sections[n]._Name == ".strtab")
                {
                    StringTable = Elf._Sections[n];
                    break;
                }
                if (Parent._Name == ".dynsym" && Elf._Sections[n]._Name == ".dynstr")
                {
                    StringTable = Elf._Sections[n];
                    break;
                }
                /* As specified in the arm documentation */
                if (Parent._Name == ".dynsym" && Elf._Sections[n]._Name == ".dynstrtab")
                {
                    StringTable = Elf._Sections[n];
                    break;
                }
            }
            ulong end = Offset + Size;
            while (Offset < end)
            {
                Assembly_Elf.Symbol Symbol = new Assembly_Elf.Symbol();
                Symbol._Section_Offset = Offset - Parent.__sh_offset;
                Symbol._Section = Parent;
                if (Elf._64BitsImage)
                {
                    Symbol.__st_name = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_info = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_other = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_shndx = ReadElf_Word(AssemblyData, (int)Offset, Elf); Offset += 2;
                    Symbol.__st_value = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    Symbol.__st_size = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;

                    Symbol.__st_info_bind = Symbol.__st_info >> 4;
                    Symbol.__st_info_type = Symbol.__st_info & 0xF;
                    Symbol._Type = ToSymbolType(Symbol.__st_info_type);

                }
                else
                {
                    Symbol.__st_name = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_value = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_size = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_info = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_other = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_shndx = ReadElf_Word(AssemblyData, (int)Offset, Elf); Offset += 2;

                    Symbol.__st_info_bind = Symbol.__st_info >> 4;
                    Symbol.__st_info_type = Symbol.__st_info & 0xF;
                    Symbol._Type = ToSymbolType(Symbol.__st_info_type);
                }
                if (Symbol.__st_name != 0 && StringTable != null)
                {
                    ulong nameoffset = StringTable.__sh_offset + Symbol.__st_name;
                    Symbol._Name = ReadElf_ASCIIZ(AssemblyData, nameoffset);
                }
                Elf._Symbols.Add(Symbol);
                Parent._Symbols.Add(Symbol);
            }
        }

        static void ReadElf_ReadGot(byte[] AssemblyData, ulong Offset, ulong Size, Assembly_Elf.Section Parent, Assembly_Elf Elf)
        {
            ulong end = Offset + Size;
            while (Offset < end)
            {
                if (Elf._64BitsImage)
                {
                    ulong gotOffset = Offset - Parent.__sh_offset;
                    ulong absoluteRef = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    Elf._Got._Locations.Add(gotOffset, absoluteRef);
                }
                else
                {
                    ulong gotOffset = Offset - Parent.__sh_offset;
                    ulong absoluteRef = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Elf._Got._Locations.Add(gotOffset, absoluteRef);
                }
            }
        }

        static Assembly_Elf.RelocationType ToRelocationType_32(ulong Value)
        {
            switch (Value)
            {
                case 0: return Assembly_Elf.RelocationType.R_NONE;
                case 1: return Assembly_Elf.RelocationType.R_32;
                case 2: return Assembly_Elf.RelocationType.R_PC32;
                case 3: return Assembly_Elf.RelocationType.R_GOT32;
                case 4: return Assembly_Elf.RelocationType.R_PLT32;
                case 5: return Assembly_Elf.RelocationType.R_COPY;
                case 6: return Assembly_Elf.RelocationType.R_GLOB_DAT;
                case 7: return Assembly_Elf.RelocationType.R_JMP_SLOT;
                case 8: return Assembly_Elf.RelocationType.R_RELATIVE;
                case 9: return Assembly_Elf.RelocationType.R_GOTOOFF;
                case 10: return Assembly_Elf.RelocationType.R_GOTPC;
                default: return Assembly_Elf.RelocationType.R_NONE;
            }
        }

        static Assembly_Elf.RelocationType ToRelocationType_64(ulong Value)
        {
            switch (Value)
            {
                case 0: return Assembly_Elf.RelocationType.R_NONE;
                case 1: return Assembly_Elf.RelocationType.R_64;
                case 2: return Assembly_Elf.RelocationType.R_PC32;
                case 3: return Assembly_Elf.RelocationType.R_GOT32;
                case 4: return Assembly_Elf.RelocationType.R_PLT32;
                case 5: return Assembly_Elf.RelocationType.R_COPY;
                case 6: return Assembly_Elf.RelocationType.R_GLOB_DAT;
                case 7: return Assembly_Elf.RelocationType.R_JMP_SLOT;
                case 8: return Assembly_Elf.RelocationType.R_RELATIVE;
                case 9: return Assembly_Elf.RelocationType.R_GOTPCREL;
                case 10: return Assembly_Elf.RelocationType.R_32;
                case 11: return Assembly_Elf.RelocationType.R_32S;
                case 12: return Assembly_Elf.RelocationType.R_16;
                case 13: return Assembly_Elf.RelocationType.R_PC16;
                case 14: return Assembly_Elf.RelocationType.R_8;
                case 15: return Assembly_Elf.RelocationType.R_PC8;
                case 16: return Assembly_Elf.RelocationType.R_DTPMOD64;
                case 17: return Assembly_Elf.RelocationType.R_DTPOFF64;
                case 18: return Assembly_Elf.RelocationType.R_TPOFF64;
                case 19: return Assembly_Elf.RelocationType.R_TLSGD;
                case 20: return Assembly_Elf.RelocationType.R_TLSLD;
                case 21: return Assembly_Elf.RelocationType.R_DTPOFF32;
                case 22: return Assembly_Elf.RelocationType.R_GOTTPOFF;
                case 23: return Assembly_Elf.RelocationType.R_TPOFF32;
                case 24: return Assembly_Elf.RelocationType.R_PC64;
                case 25: return Assembly_Elf.RelocationType.R_GOTOFF64;
                case 26: return Assembly_Elf.RelocationType.R_GOTPC32;
                case 32: return Assembly_Elf.RelocationType.R_SIZE32;
                case 33: return Assembly_Elf.RelocationType.R_SIZE64;
                case 34: return Assembly_Elf.RelocationType.R_GOTPC32_TLSDESC;
                case 35: return Assembly_Elf.RelocationType.R_TLSDESC_ALL;
                case 36: return Assembly_Elf.RelocationType.R_TLSDESC;
                case 37: return Assembly_Elf.RelocationType.R_IRELATIVE;
                default: return Assembly_Elf.RelocationType.R_NONE;
            }
        }
        static unsafe void ReadElf_ReadRelocation(byte[] AssemblyData, ulong Offset, ulong Size, bool hasAddEnd, Assembly_Elf.Section Parent, Assembly_Elf Elf)
        {
            Assembly_Elf.Section SourceSymbolSection = null;
            Assembly_Elf.Section DestSection = null;

            if ((ulong)Elf._Sections.Count > Parent.__sh_link) { SourceSymbolSection = Elf._Sections[(int)Parent.__sh_link]; }
            if ((ulong)Elf._Sections.Count > Parent.__sh_info) { DestSection = Elf._Sections[(int)Parent.__sh_info]; }


            ulong end = Offset + Size;
            while (Offset < end)
            {
                if (Elf._64BitsImage)
                {
                    ulong r_offset = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    ulong r_info = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    ulong r_sym = r_info >> 32;
                    ulong r_type = r_info & 0xFFFFFFFF;

                    Assembly_Elf.Symbol symbol = null;
                    if (SourceSymbolSection != null && (ulong)SourceSymbolSection._Symbols.Count > r_sym)
                    { symbol = SourceSymbolSection._Symbols[(int)r_sym]; }
                    Assembly_Elf.RelocationType type = ToRelocationType_64(r_type);
                    Assembly_Elf.Section section = DestSection;
                    ulong r_addEnd = 0;

                    if ((DestSection != null && DestSection.__sh_addr == 0) ||
                        r_offset != 0)
                    {
                        // We need to find the true section from the requested mapping
                        section = Elf._FindSectionFromRequestedAddress(new IntPtr((long)r_offset));
                    }

                    if (hasAddEnd)
                    {
                        r_addEnd = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    }
                    Elf._AddRelocation(new Assembly_Elf.Relocation()
                    {
                        _r_AddEnd = *(Int64*)&r_addEnd,
                        _r_Symbols = r_sym,
                        _r_Offset = r_offset,
                        _Type = type,
                        _Symbol = symbol,
                        _SymbolSection = SourceSymbolSection,
                        _TargetSection = section
                    });
                }
                else
                {
                    ulong r_offset = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    ulong r_info = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    ulong r_sym = r_info >> 8;
                    ulong r_type = r_info & 0xFF;
                    Assembly_Elf.Symbol symbol = null;
                    if (SourceSymbolSection != null && (ulong)SourceSymbolSection._Symbols.Count > r_sym)
                    { symbol = SourceSymbolSection._Symbols[(int)r_sym]; }
                    Assembly_Elf.RelocationType type = ToRelocationType_32(r_type);
                    Assembly_Elf.Section section = DestSection;
                    ulong r_addEnd = 0;

                    if (DestSection != null && DestSection.__sh_addr == 0)
                    {
                        // We need to find the true section from the requested mapping
                        section = Elf._FindSectionFromRequestedAddress(new IntPtr((long)r_offset));
                    }

                    if (hasAddEnd)
                    {
                        r_addEnd = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    }
                    Elf._AddRelocation(new Assembly_Elf.Relocation()
                    {
                        _r_AddEnd = *(Int32*)&r_addEnd,
                        _r_Symbols = r_sym,
                        _r_Offset = r_offset,
                        _Type = type,
                        _Symbol = symbol,
                        _SymbolSection = SourceSymbolSection,
                        _TargetSection = section
                    });
                }
            }
        }

        static void ReadElf_BindReloc(Assembly_Elf Elf)
        {
            foreach (Assembly_Elf.Relocation reloc in Elf._Relocations)
            {

            }
        }

        static void ReadElf_TagSymbols(Assembly_Elf Elf)
        {
            for (int n = 0; n < Elf._Symbols.Count; n++)
            {
                if (Elf._Symbols[n]._Name != "" && Elf._Symbols[n].__st_info_type == 0x02)
                {
                    if (Elf._Symbols[n].__st_value == 0 && Elf._Symbols[n].__st_size == 0)
                    {
                        // Undefined symbols
                        Elf._AddImportedMethod(Elf._Symbols[n]);
                    }
                    else
                    {
                        Elf._AddMethod(Elf._Symbols[n]);
                    }
                }
            }
        }
    }
}

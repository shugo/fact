﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection.Memory
{
    static class MMU
    {

        unsafe class PDE<PTE_Type>
        {
            UInt64 _From;
            UInt64 _To;
            UInt64 _EntrySize;
            UInt64 _Mask;
            public UInt64 EntrySize { get { return _EntrySize; } }
            public UInt64 From { get { return _From; } }

            PTE_Type[] _PTEs = null;
            public PDE(UInt64 From, UInt64 To, UInt64 EntrySize, UInt64 Mask)
            {
                _From = From;
                _To = To;
                _EntrySize = EntrySize;
                _Mask = Mask;
                UInt64 entryCount = ((_To + 1) - _From) / EntrySize;
                _PTEs = new PTE_Type[entryCount];

#if DEBUG
                Fact.Log.Debug("Creating PDE [" + From.ToString("X") + ", " + To.ToString("X") + "] (" + EntrySize.ToString("X") + ")");
#endif
            }

            public PTE_Type this[void* Address]
            {
                get
                {
                    if ((UInt64)Address < _From || (UInt64)Address > _To) { return default(PTE_Type); }
                    return _PTEs[(((UInt64)Address - _From) & _Mask) / _EntrySize];
                }
                set
                {
                    if ((UInt64)Address < _From || (UInt64)Address > _To) { throw new Exception("Invalid address"); }
                    _PTEs[(((UInt64)Address - _From) & _Mask) / _EntrySize] = value;
                }
            }

            public PTE_Type GetEntry(UInt64 Index) { return _PTEs[Index]; }
            public void SetEntry(UInt64 Index, PTE_Type PTE) { _PTEs[Index] = PTE; }

        }

        class PTE
        {
            UInt64 _From;
            UInt64 _To;
            public PTE(UInt64 From, UInt64 To)
            {
#if DEBUG
                Fact.Log.Debug("Creating PTE [" + From.ToString("X") + ", " + To.ToString("X") + "]");
#endif
            }


        }
        static UInt64 _From = 0x0ul;
        static UInt64 _To = 0xfffffffffffffffful;
        static UInt64 _PDESize = 1024ul * 1024ul * 1024ul * 1024ul * 16ul; // 16 To
        static PDE<PDE<PDE<PTE>>>[] _PDE = null;

        static MMU()
        {
            _PDE = new PDE<PDE<PDE<PTE>>>[(((_To - _From) / _PDESize) + 1)];
        }

        public class MappingInfo
        {
            VirtualAddressSpace.MemoryProtection _Protection = VirtualAddressSpace.MemoryProtection.None;
            public class MallocInfo
            {

            }
            MallocInfo _MallocInfo = null;
        }

        internal static void RegisterMapping(IntPtr Address, UInt64 Size, MappingInfo MappingInfo)
        {
#if DEBUG
            Fact.Log.Debug("Registering VA Space [" + Address.ToInt64().ToString("X") + ", " + (Address.ToInt64() + (Int64)Size - 1L).ToString("X") + "]");
#endif
            UInt64 PDE3Indx = (UInt64)((UInt64)Address.ToInt64() / _PDESize);
            if (_PDE[PDE3Indx] == null)
            {
                _PDE[PDE3Indx] = new PDE<PDE<PDE<PTE>>>(_From + _PDESize * PDE3Indx, _From + _PDESize * (PDE3Indx + 1ul) - 1ul, _PDESize / 1024ul, _PDESize - 1ul); // 16GB Sub PDE
            }
            PDE<PDE<PDE<PTE>>> PDE3 = _PDE[PDE3Indx];

            UInt64 PDE2Indx = (UInt64)((((UInt64)Address.ToInt64() - PDE3.From) & (_PDESize - 1ul)) / PDE3.EntrySize);
            if (PDE3.GetEntry(PDE2Indx) == null)
            {
                PDE3.SetEntry(PDE2Indx, new PDE<PDE<PTE>>(PDE3.From + PDE2Indx * PDE3.EntrySize, PDE3.From + (PDE2Indx + 1ul) * (PDE3.EntrySize) - 1ul, PDE3.EntrySize / 1024ul, PDE3.EntrySize - 1ul));  // 16MB Sub PDE 
            }
            PDE<PDE<PTE>> PDE2 = PDE3.GetEntry(PDE2Indx);


            UInt64 PDE1Indx = (UInt64)((((UInt64)Address.ToInt64() - PDE2.From) & (PDE3.EntrySize - 1ul)) / PDE2.EntrySize);
            if (PDE2.GetEntry(PDE1Indx) == null)
            {
                PDE2.SetEntry(PDE1Indx, new PDE<PTE>(PDE2.From + PDE1Indx * PDE2.EntrySize, PDE2.From + (PDE1Indx + 1ul) * PDE2.EntrySize - 1ul, 4096ul, PDE2.EntrySize - 1ul));  // Last level
            }
            PDE<PTE> PDE1 = PDE2.GetEntry(PDE1Indx);


            UInt64 PTEIndx = (UInt64)((((UInt64)Address.ToInt64() - PDE1.From) & (PDE2.EntrySize - 1ul)) / PDE1.EntrySize);
            if (PDE1.GetEntry(PTEIndx) == null)
            {
                PDE1.SetEntry(PDE1Indx, new PTE(PDE1.From + PTEIndx * PDE1.EntrySize, PDE1.From + (PTEIndx + 1ul) * PDE1.EntrySize - 1ul));
            }
            PTE PTE = PDE1.GetEntry(PTEIndx);

        }

        internal static void UnregisterMapping(IntPtr Address, UInt64 Size)
        {

        }

        internal static void ReserveVa(IntPtr Address, UInt64 Size)
        {

        }

        static internal Dictionary<ulong, Delegate> _SpecialAddress = new Dictionary<ulong, Delegate>();
        static internal void RegisterSpecialAddress(IntPtr Address, Delegate Function)
        {
            _SpecialAddress.Add((ulong)Address.ToInt64(), Function);
        }
    }
}

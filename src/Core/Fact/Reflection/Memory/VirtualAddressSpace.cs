﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection.Memory
{
    public class VirtualAddressSpace
    {
        public enum MemoryProtection
        {
            None = 0x00,
            Read = 0x01,
            Write = 0x02,
            Execute = 0x04
        }

        public VirtualAddressSpace()
        {
            
        }

        public IntPtr MapMemory(int Size, MemoryProtection Protection)
        {
            IntPtr mapping = Internal.Os.Generic.MapMemory(Size, (Internal.Os.Generic.Protection)(Protection));
            MMU.RegisterMapping(mapping, (UInt64)Size, null);
            return mapping;
        }

        public bool ChangeMemoryProtection(IntPtr Memory, int Size, MemoryProtection Protection)
        {
            bool result = Internal.Os.Generic.ChangeMemoryProtection(Memory, Size, (Internal.Os.Generic.Protection)(Protection));
            MMU.RegisterMapping(Memory, (UInt64)Size, null);
            return result;
        }

        public bool UnmapMemory(IntPtr Memory, int Size)
        {
            bool result = Internal.Os.Generic.UnmapMemory(Memory, Size);
            MMU.UnregisterMapping(Memory, (UInt64)Size);
            return result;
        }
    }
}

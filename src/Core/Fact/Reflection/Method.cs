﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public class Method
    {
        protected Assembly _ParentAssembly = null;
        public Assembly ParentAssembly { get { return _ParentAssembly; } }
        internal void SetParentAssembly(Assembly ParentAssembly) { _ParentAssembly = ParentAssembly; }
        protected string _Name = "";
        public string Name { get { return _Name; } }
        public object Invoke(object This, params object[] Parameters)
        {
            if (_ParentAssembly != null)
            {
                if (_ParentAssembly.CurrentLoadingStatus != Assembly.LoadingStatus.Loaded)
                {
                    _ParentAssembly.__Load();
                }
            }
            return _Invoke(This, Parameters);
        }
        public T Invoke<T>(object This, params object[] Parameters)
        {
            if (_ParentAssembly != null)
            {
                if (_ParentAssembly.CurrentLoadingStatus != Assembly.LoadingStatus.Loaded)
                {
                    _ParentAssembly.__Load();
                }
            }
            return _Invoke<T>(This, Parameters);
        }

        public T Invoke<T>(int Timeout, object This, params object[] Parameters)
        {
            T result = default(T);
            Threading.Job.Status job = Threading.Job.CreateJob(() => { result = Invoke<T>(This, Parameters); });
            if (!job.Join(Timeout)) { try { job.Abort(); } catch { } throw new Exception("Timeout"); }
            return result;
        }
        protected virtual object _Invoke(object This, params object[] Parameters) { throw new NotImplementedException(); }
        protected virtual T _Invoke<T>(object This, params object[] Parameters) { throw new NotImplementedException(); }

        public virtual void Hook(Delegate Method) { throw new NotImplementedException(); }
        public virtual void Hook(Method Method) { throw new NotImplementedException(); }
        
        public virtual IntPtr GetFunctionPointer() { throw new NotImplementedException(); }

        protected Dictionary<string, object> _attributes = new Dictionary<string, object>();

        internal virtual void Serialize(System.IO.Stream Stream)
        {
            throw new Exception("This method is not serialazable");
        }

        internal static Method Deserialize(System.IO.Stream Stream)
        {
            string parentAssemblyHash = Internal.StreamTools.ReadUTF8String(Stream);
            Assembly parentAssembly = Assembly.GetAssemblyFromHash(parentAssemblyHash);
            if (parentAssembly == null) { throw new Exception("Invalid assembly: " + parentAssemblyHash); }
            Method method = null;
            switch (Stream.ReadByte())
            {
                //case 0x1: method = Pe.Method_PE.Deserialize(Stream); break;
                case 0x2: method = Elf.Method_Elf.Deserialize(Stream, parentAssembly); break;
                default: throw new Exception("Invalid method serialized");
            }
            return method;
        }

        public virtual Debugger.Breakpoint AddBreakpoint(int Offset)
        {
            return null;
        }

        public override string ToString()
        {
            return _Name;
        }
    }
}

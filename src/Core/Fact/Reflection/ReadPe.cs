﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    class Pe
    {
        internal class Assembly_PE : Assembly
        {
            public Assembly_PE() { _LoadingStatus = LoadingStatus.LoadedForReflection; }
            internal string _File = "";
            internal ulong _Size = 0;
            internal int _ImageFileHeaderOffset = 0;
            internal int _ImageOptionalHeaderOffset = 0;
            internal ulong _ImageRequestedBase = 0;
            internal int _NumberOfSymbols = 0;
            internal ulong _EntryPointOffset = 0;
            internal bool _64BitsImage = false;

            internal List<Section> _Sections = new List<Section>();
            internal class Section
            {
                internal string _Name = "";
                internal ulong _VirtualAddress = 0;
                internal ulong _SizeOfRawData = 0;
                internal ulong _PointerToRawData = 0;
                internal IntPtr _MapAddress;

                internal Memory.VirtualAddressSpace.MemoryProtection _Protection = Memory.VirtualAddressSpace.MemoryProtection.None;
            }
            internal class Symbol
            {
                internal string _Name = "";
                internal ulong _SectionOffset = 0;
                internal Section _Section;
            }

            internal ulong RVAToOffset(ulong RVA)
            {
                Section section = FindRVASection(RVA);
                if (section == null) { return 0; }
                return (RVA - section._VirtualAddress) + section._PointerToRawData;
            }
            internal Section FindRVASection(ulong RVA)
            {
                for (int n = 0; n < _Sections.Count; n++)
                {
                    if (RVA >= _Sections[n]._VirtualAddress &&
                        RVA < _Sections[n]._VirtualAddress + _Sections[n]._SizeOfRawData)
                    {
                        return _Sections[n];
                    }
                }
                return null;
            }

            internal class Relocation_PE
            {
                internal enum RelocationType
                {
                    IMAGE_REL_BASED_ABSOLUTE = 0,
                    IMAGE_REL_BASED_HIGH = 1,
                    IMAGE_REL_BASED_LOW = 2,
                    IMAGE_REL_BASED_HIGHLOW = 3,
                    IMAGE_REL_BASED_HIGHADJ = 4,
                    IMAGE_REL_BASED_MIPS_JMPADDR = 5,
                    IMAGE_REL_BASED_ARM_MOV32A = 5,
                    IMAGE_REL_RESERVED = 6,
                    IMAGE_REL_BASED_ARM_MOV32T = 7,
                    IMAGE_REL_BASED_MIPS_JMPADDR16 = 9,
                    IMAGE_REL_BASED_DIR64 = 10
                }

                internal ulong VirtualAddress = 0;
                internal RelocationType Type = RelocationType.IMAGE_REL_RESERVED;
            }
            internal List<Relocation_PE> _Relocations = new List<Relocation_PE>();

            internal class Method_PE : Method
            {
                Assembly_PE _AssemblyPe;
                Symbol _Symbol;
                IntPtr _FunctionPtr = new IntPtr(0);
                internal Dictionary<string, object> _Wrappers = new Dictionary<string, object>();

                internal Method_PE(string Name, Symbol Symbol, Assembly_PE Parent)
                {
                    _Name = Name;
                    _ParentAssembly = Parent;
                    _AssemblyPe = Parent;
                    _Symbol = Symbol;
                }

                IntPtr _GetNativeFunctionPointer()
                {
                    if (_FunctionPtr.ToInt64() != 0) { return _FunctionPtr; }
                    _AssemblyPe._Map();
                    if (_Symbol != null)
                    {
                        _FunctionPtr = new IntPtr(_Symbol._Section._MapAddress.ToInt64() + (long)_Symbol._SectionOffset);
                        return _FunctionPtr;
                    }
                    return new IntPtr(0);
                }

                protected override T _Invoke<T>(object This, params object[] Parameters)
                {
                    IntPtr ptr = _GetNativeFunctionPointer();
                    if (ptr.ToInt64() == 0) { throw new InvalidProgramException(); }
                    string signature = Tools.ComputeNativeSignature(ptr, typeof(T), Parameters);
                    Tools.Wrapper<T> wrapper = null;
#if FULL_EMULATION
                    return Emulation.Execute.Call(pointer.ToInt64(), Emulation.Execute.ProcessorType.x86_64, Emulation.Execute.CallingConvention.MicrosoftAMD64, This, Parameters);
#else
                    lock (_Wrappers)
                    {
                        if (_Wrappers.ContainsKey(signature)) { wrapper = (Tools.Wrapper<T>)_Wrappers[signature]; }
                        else
                        {
                            wrapper = Tools.CreateNativeWrapper<T>(ptr, Tools.ABI.MicrosoftAMD64, Parameters);
                            _Wrappers.Add(signature, wrapper);
                        }
                    }
#endif

                    return wrapper(Parameters);
                }


               
            }

            MapHelper _MapHelper = null;

            protected override void _Load() { _Map(); }
            ulong delta = 0;
            internal void _SolveRelocation()
            {
                long imagebase = _MapHelper.Mapping.ToInt64();
                long expectedbase = _MapHelper.Mapping.ToInt64();

                foreach (Relocation_PE relocation in _Relocations)
                {
                    Section section = FindRVASection(relocation.VirtualAddress);
                    switch (relocation.Type)
                    {
                    }
                }
            }


            internal void _Map()
            {
                lock (this)
                {

                    if (CurrentLoadingStatus != LoadingStatus.Loaded)
                    {
                        ulong firstVa = ulong.MaxValue;
                        ulong lastVa = 0;
                        foreach (Section section in _Sections)
                        {
                            if (section._VirtualAddress < firstVa) { firstVa = section._VirtualAddress; }
                            if (section._VirtualAddress + section._SizeOfRawData > lastVa) { lastVa = section._VirtualAddress + section._SizeOfRawData; }
                        }
                        ulong size = lastVa - firstVa;

                        _LoadForReflection();
                        _MapHelper = new MapHelper(size, VirtualAddressSpace);
                        foreach(Section section in _Sections)
                        {
                            if (section._Protection != Memory.VirtualAddressSpace.MemoryProtection.None)
                            {
                                MapHelper.Section mapSection = new MapHelper.Section();
                                mapSection.Name = section._Name;
                                mapSection.Offset = (int)(section._VirtualAddress - firstVa);
                                mapSection.Size = (int)section._SizeOfRawData;
                                mapSection.Protection = section._Protection;
                                mapSection.Tag = section;
                                IntPtr ptr = _MapHelper.MapSection(mapSection);
                                System.Runtime.InteropServices.Marshal.Copy(_Data, (int)section._PointerToRawData, ptr, mapSection.Size);
                                section._MapAddress = ptr;
                            }
                        }
                        _MapHelper.ApplyProtection();
                        _SolveRelocation();
                        _LoadingStatus = LoadingStatus.Loaded;
                    }
                }
            }

            internal void _AddDependency(string Dll, string Method)
            {
                string name = System.IO.Path.GetFileName(Dll);
                Assembly_PE dep  = null;
                if (!_Depends.ContainsKey(name))
                {
                    dep = new Assembly_PE();
                    dep._File = Dll;
                    dep._SetName(name);
                    dep._LoadingStatus = LoadingStatus.NotLoaded;
                    _Depends.Add(name, dep);
                }
                else
                {
                    dep = _Depends[name] as Assembly_PE;
                }
                if (dep == null) { return; }
                Method_PE method_pe = dep._AddMethod(Method);
                _ImportedMethods.Add(method_pe);
            }

            internal void _SetName(string Name)
            {
                _Name = Name;
            }
            internal Method_PE _EntryPoint = null;
            public override int Run(string[] Arguments)
            {
                if (_EntryPoint != null)
                {
                    return _EntryPoint.Invoke<int>(null, null);
                }
                else
                {
                    throw new Exception("This assembly is not an executable");
                }
            }

            internal Method_PE _AddMethod(string Name)
            {
                Method_PE method = new Method_PE(Name, null, this);
                _Methods.Add(method);
                return method;
            }

            protected override void _LoadForReflection()
            {
                lock (this)
                {
                    if (_LoadingStatus == LoadingStatus.LoadedForReflection ||
                        _LoadingStatus == LoadingStatus.Loaded)
                    {
                        return;
                    }
                    if (System.IO.File.Exists(_File))
                    {
                        ReadPE(System.IO.File.ReadAllBytes(_File), this);
                    }
                }
            }
        }
        static internal Assembly ReadPE(byte[] AssemblyData)
        {
            Assembly_PE assembly = new Assembly_PE();
            return ReadPE(AssemblyData, assembly);
        }

        static internal Assembly ReadPE(byte[] AssemblyData, Assembly_PE Assembly)
        {
            int index = (16 + 14) * 2;
            int NewHeader = AssemblyData[index] +
                AssemblyData[index + 1] * 0x100 +
                AssemblyData[index + 2] * 0x10000 +
                AssemblyData[index + 3] * 0x1000000;
            if (CheckPENTHeader(AssemblyData, NewHeader)) { return ReadPE_NT(AssemblyData, NewHeader, Assembly); }
            NewHeader = AssemblyData[index + 3] +
                        AssemblyData[index + 2] * 0x100 +
                        AssemblyData[index + 1] * 0x10000 +
                        AssemblyData[index + 0] * 0x1000000;
            if (CheckPENTHeader(AssemblyData, NewHeader)) { return ReadPE_NT(AssemblyData, NewHeader, Assembly); }
            return null;
        }
        static internal bool CheckPENTHeader(byte[] AssemblyData, int Offset)
        {
            if (Offset + 3 >= AssemblyData.Length) { return false; }
            return (AssemblyData[Offset] == 0x50 &&
                    AssemblyData[Offset + 1] == 0x45 &&
                    AssemblyData[Offset + 2] == 0x00 &&
                    AssemblyData[Offset + 3] == 0x00) ||
                   (AssemblyData[Offset + 3] == 0x50 &&
                    AssemblyData[Offset + 2] == 0x45 &&
                    AssemblyData[Offset + 1] == 0x00 &&
                    AssemblyData[Offset] == 0x00);
        }

        static internal Assembly ReadPE_NT(byte[] AssemblyData, int Offset, Assembly_PE Assembly)
        {
            Assembly._ImageFileHeaderOffset = Offset;
            Offset += 4; // Skip the magik number
            int NumberOfSection = AssemblyData[Offset + 2] + AssemblyData[Offset + 3] * 0x100;
            int SymbolsTable =
                AssemblyData[Offset + 8] +
                AssemblyData[Offset + 9] * 0x100 +
                AssemblyData[Offset + 10] * 0X10000 +
                AssemblyData[Offset + 11] * 0X1000000;
            Assembly._NumberOfSymbols =
                AssemblyData[Offset + 12] +
                AssemblyData[Offset + 13] * 0x100 +
                AssemblyData[Offset + 14] * 0X10000 +
                AssemblyData[Offset + 15] * 0X1000000;
            int SizeOfOptionalHeader =
                AssemblyData[Offset + 16] +
                AssemblyData[Offset + 17] * 0x100;
            int Charateristic =
                AssemblyData[Offset + 18] +
                AssemblyData[Offset + 19] * 0x100;
            if (NumberOfSection > 0)
            {
                ReadPE_NT_Sections(AssemblyData, Offset + 20 + SizeOfOptionalHeader, NumberOfSection, Assembly);
                
            }
            if (SizeOfOptionalHeader != 0)
            {
                ReadPE_NT_OptionalHeader(AssemblyData, Offset + 20, Assembly);
            }

            if (Assembly._EntryPointOffset != 0)
            {
                Assembly_PE.Symbol symbol = new Assembly_PE.Symbol();
                symbol._Name = "_start";
                symbol._Section = Assembly.FindRVASection(Assembly._EntryPointOffset);
                if (symbol._Section != null)
                {
                    symbol._SectionOffset = Assembly._EntryPointOffset - symbol._Section._VirtualAddress;
                }
                Assembly_PE.Method_PE method = new Assembly_PE.Method_PE("_start", symbol, Assembly);
                Assembly._EntryPoint = method;
            }

            return Assembly;
        }
        static internal void ReadPE_NT_Sections(byte[] AssemblyData, int Offset, int Count, Assembly_PE Assembly)
        {
            while (Count > 0)
            {
                Assembly._Sections.Add(ReadPE_NT_Section(AssemblyData, ref Offset, Assembly));
                Count--;
            }
        }

        static internal Assembly_PE.Section ReadPE_NT_Section(byte[] AssemblyData, ref int Offset, Assembly_PE Assembly)
        {
            Assembly_PE.Section Section = new Assembly_PE.Section();
            if (AssemblyData[Offset] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset];
            if (AssemblyData[Offset + 1] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset + 1];
            if (AssemblyData[Offset + 2] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset + 2];
            if (AssemblyData[Offset + 3] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset + 3];
            if (AssemblyData[Offset + 4] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset + 4];
            if (AssemblyData[Offset + 5] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset + 5];
            if (AssemblyData[Offset + 6] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset + 6];
            if (AssemblyData[Offset + 7] == 0) { goto next; } Section._Name += (char)AssemblyData[Offset + 7];
            next:;
            Offset += 8;
            Offset += 4; // Physical Address
            Section._VirtualAddress = (ulong)AssemblyData[Offset] +
                                      (ulong)AssemblyData[Offset + 1] * 0x100 +
                                      (ulong)AssemblyData[Offset + 2] * 0X10000 +
                                      (ulong)AssemblyData[Offset + 3] * 0X1000000;
            Offset += 4; // Virtual Address
            Section._SizeOfRawData = (ulong)AssemblyData[Offset] +
                                     (ulong)AssemblyData[Offset + 1] * 0x100 +
                                     (ulong)AssemblyData[Offset + 2] * 0X10000 +
                                     (ulong)AssemblyData[Offset + 3] * 0X1000000;
            Offset += 4; // Size of raw data
            Section._PointerToRawData = (ulong)AssemblyData[Offset] +
                                        (ulong)AssemblyData[Offset + 1] * 0x100 +
                                        (ulong)AssemblyData[Offset + 2] * 0X10000 +
                                        (ulong)AssemblyData[Offset + 3] * 0X1000000;
            Offset += 4; // Pointer to raw data
            Offset += 4; // Pointer to relocations
            Offset += 4; // Pointer to linenumber
            Offset += 2; // Number of relocation
            Offset += 2; // Number of linenumber

            ulong Characteristic = (ulong)AssemblyData[Offset] +
                         (ulong)AssemblyData[Offset + 1] * 0x100 +
                         (ulong)AssemblyData[Offset + 2] * 0X10000 +
                         (ulong)AssemblyData[Offset + 3] * 0X1000000;
            Memory.VirtualAddressSpace.MemoryProtection SectionProtection = Memory.VirtualAddressSpace.MemoryProtection.None;
            if ((Characteristic & 0x20000000) != 0) { SectionProtection |= Memory.VirtualAddressSpace.MemoryProtection.Execute; }
            if ((Characteristic & 0x40000000) != 0) { SectionProtection |= Memory.VirtualAddressSpace.MemoryProtection.Read; }
            if ((Characteristic & 0x80000000) != 0) { SectionProtection |= Memory.VirtualAddressSpace.MemoryProtection.Write; }
            Section._Protection = SectionProtection;
            Offset += 4; // Characteristic

            return Section;
        }

        static internal void ReadPE_NT_OptionalHeader(byte[] AssemblyData, int Offset, Assembly_PE Assembly)
        {
            Assembly._ImageOptionalHeaderOffset = Offset;
            int magik = AssemblyData[Offset] + AssemblyData[Offset + 1] * 0x100;
            if (magik == 0x20b) { Assembly._64BitsImage = true; }
            Offset += 2; // Magik
            Offset += 1; // Versions info Major
            Offset += 1; // Versions info Minor
            Offset += 4; // Size of Code
            Offset += 4; // Size of Initialized Data
            Offset += 4; // Size of Uninitialized Data
            Assembly._EntryPointOffset = (ulong)AssemblyData[Offset] +
                                         (ulong)AssemblyData[Offset + 1] * 0x100 +
                                         (ulong)AssemblyData[Offset + 2] * 0X10000 +
                                         (ulong)AssemblyData[Offset + 3] * 0X1000000;
            Offset += 4; // Address Of Entry Point
            Offset += 4; // Base of code
            if (!Assembly._64BitsImage) { Offset += 4; } // Base of data
            if (Assembly._64BitsImage)
            {
                Assembly._ImageRequestedBase = (ulong)AssemblyData[Offset] +
                                               (ulong)AssemblyData[Offset + 1] * 0x100 +
                                               (ulong)AssemblyData[Offset + 2] * 0X10000 +
                                               (ulong)AssemblyData[Offset + 3] * 0X1000000 +
                                               (ulong)AssemblyData[Offset + 4] * 0X100000000 +
                                               (ulong)AssemblyData[Offset + 5] * 0x10000000000 +
                                               (ulong)AssemblyData[Offset + 6] * 0X1000000000000 +
                                               (ulong)AssemblyData[Offset + 7] * 0X100000000000000;
                Offset += 8;
            }
            else
            {
                Offset += 4;
            } // Image base
            Offset += 4; // Section Alignement
            Offset += 4; // File Alignement

            Offset += 2; // Major OS Version
            Offset += 2; // Minor OS Version
            Offset += 2; // Major Image Version
            Offset += 2; // Minor Image Version
            Offset += 2; // Major Subsystem Version
            Offset += 2; // Minor Subsystem Version
            Offset += 4; // Win32 Version
            Offset += 4; // Size of Image
            Offset += 4; // Size of Headers
            Offset += 4; // Checksum
            Offset += 2; // Subsystem
            Offset += 2; // DLLCharacteristic

            if (Assembly._64BitsImage) { Offset += 8; } else { Offset += 4; } // Stack Reserve
            if (Assembly._64BitsImage) { Offset += 8; } else { Offset += 4; } // Stack Commit
            if (Assembly._64BitsImage) { Offset += 8; } else { Offset += 4; } // Heap reserve
            if (Assembly._64BitsImage) { Offset += 8; } else { Offset += 4; } // Heap Commit

            Offset += 4; // LOader flags
            Offset += 4; // Number of Rva And Size

            ReadPE_NT_Data_Directories(AssemblyData, Offset, Assembly);
        }

        static internal void ReadPE_NT_Data_Directories(byte[] AssemblyData, int Offset, Assembly_PE Assembly)
        {
            int VirtualAddress = 0;
            int Size = 0;

            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { ReadPE_NT_ExportTable(AssemblyData, Assembly.RVAToOffset((ulong)VirtualAddress), Size, Assembly); }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { ReadPE_NT_ImportTable(AssemblyData, Assembly.RVAToOffset((ulong)VirtualAddress), Size, Assembly); }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read PE Ressources */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read Exception table */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read Certificate table */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { ReadPE_NT_Relocation(AssemblyData, Assembly.RVAToOffset((ulong)VirtualAddress), Size, Assembly); }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read Debug */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read Global Ptr */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read TLS Table */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read Load Config Table */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Read Bound Import Table */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Import Address Table */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /* Delayed import descriptor */ }
            ReadPE_NT_DataDirectory(AssemblyData, ref Offset, out VirtualAddress, out Size);
            if (VirtualAddress > 0) { /*CLR Runtime */ }
        }

        static internal void ReadPE_NT_DataDirectory(byte[] AssemblyData, ref int Offset, out int VirtualAddress, out int Size)
        {
            VirtualAddress = AssemblyData[Offset] +
                             AssemblyData[Offset + 1] * 0x100 +
                             AssemblyData[Offset + 2] * 0x10000 +
                             AssemblyData[Offset + 3] * 0x1000000;
            Size = AssemblyData[Offset + 4] +
                   AssemblyData[Offset + 5] * 0x100 +
                   AssemblyData[Offset + 6] * 0x10000 +
                   AssemblyData[Offset + 7] * 0x1000000;
            Offset += 8;
        }

        static internal void ReadPE_NT_ExportTable(byte[] AssemblyData, ulong Offset, int Size, Assembly_PE Assembly)
        {
            Offset += 4; // Characteristic
            Offset += 4; // Timestamp
            Offset += 2; // Major Version
            Offset += 2; // Minor Version
            ulong namerva = (ulong)AssemblyData[Offset] +
                            (ulong)AssemblyData[Offset + 1] * 0x100 +
                            (ulong)AssemblyData[Offset + 2] * 0x10000 +
                            (ulong)AssemblyData[Offset + 3] * 0x1000000;

            string ModuleName = ReadPE_ASCIIZ(AssemblyData, Assembly.RVAToOffset(namerva));
            Assembly._SetName(ModuleName);
            Offset += 4; // Name
            Offset += 4; // Base
            ulong FunctionNumber = (ulong)AssemblyData[Offset] +
                                   (ulong)AssemblyData[Offset + 1] * 0x100 +
                                   (ulong)AssemblyData[Offset + 2] * 0x10000 +
                                   (ulong)AssemblyData[Offset + 3] * 0x1000000;
            Offset += 4; // Number of function
            ulong NameNumber = (ulong)AssemblyData[Offset] +
                               (ulong)AssemblyData[Offset + 1] * 0x100 +
                               (ulong)AssemblyData[Offset + 2] * 0x10000 +
                               (ulong)AssemblyData[Offset + 3] * 0x1000000;
            Offset += 4; // Number of name

            ulong FunctionRVA = (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000 +
                       (ulong)AssemblyData[Offset + 3] * 0x1000000;
            FunctionRVA = Assembly.RVAToOffset(FunctionRVA);
            Offset += 4; // Function RDA
            ulong NameRVA = (ulong)AssemblyData[Offset] +
                            (ulong)AssemblyData[Offset + 1] * 0x100 +
                            (ulong)AssemblyData[Offset + 2] * 0x10000 +
                            (ulong)AssemblyData[Offset + 3] * 0x1000000;
            Offset += 4; // Name RDA
            NameRVA = Assembly.RVAToOffset(NameRVA);

            List<string> Names = new List<string>();
            for (int n = 0; n < (int)NameNumber; n++)
            {
                ulong FunctionNameRVA = (ulong)AssemblyData[NameRVA] +
                    (ulong)AssemblyData[NameRVA + 1] * 0x100 +
                    (ulong)AssemblyData[NameRVA + 2] * 0x10000 +
                    (ulong)AssemblyData[NameRVA + 3] * 0x1000000;
                FunctionNameRVA = Assembly.RVAToOffset(FunctionNameRVA);
                if (FunctionNameRVA != 0)
                {
                    string name = ReadPE_ASCIIZ(AssemblyData, FunctionNameRVA);
                    Assembly._AddMethod(name);
                }
                NameRVA += 4;
            }
            

        }



        static internal string ReadPE_ASCIIZ(byte[] AssemblyData, ulong Offset)
        {
            string value = "";
            for (int n = (int)Offset; n < AssemblyData.Length; n++)
            {
                if (AssemblyData[n] == 0) { return value; }
                value += (char)(AssemblyData[n]);
            }
            return value;
        }

        static internal void ReadPE_NT_Relocation(byte[] AssemblyData, ulong Offset, int Size, Assembly_PE Assembly)
        {
            ulong start = Offset;
            ulong end = (ulong)(Offset + (ulong)Size);

            while (Offset < end)
            {
                ReadPE_NT_RelocationChunk(AssemblyData, ref Offset, Assembly);
            }
        }

        static internal void ReadPE_NT_RelocationChunk(byte[] AssemblyData, ref ulong Offset, Assembly_PE Assembly)
        {
            int VirtualAddress = AssemblyData[Offset] +
                 AssemblyData[Offset + 1] * 0x100 +
                 AssemblyData[Offset + 2] * 0x10000 +
                 AssemblyData[Offset + 3] * 0x1000000;
            int EntryCount = AssemblyData[Offset + 4] +
                   AssemblyData[Offset + 5] * 0x100 +
                   AssemblyData[Offset + 6] * 0x10000 +
                   AssemblyData[Offset + 7] * 0x1000000;
            EntryCount -= 8;
            EntryCount /= 2;
            Offset += 8;

            ReadPE_NT_RelocationEntryList(AssemblyData, ref Offset, (ulong)VirtualAddress, EntryCount, Assembly);
        }

        static internal void ReadPE_NT_RelocationEntryList(byte[] AssemblyData, ref ulong Offset, ulong VirtualAddress, int EntryCount, Assembly_PE Assembly)
        {
            while (EntryCount > 0)
            {
                Assembly_PE.Relocation_PE relocation = new Assembly_PE.Relocation_PE();

                int complexEntry = AssemblyData[Offset] +
                                   AssemblyData[Offset + 1] * 0x100;
                relocation.Type = (Assembly_PE.Relocation_PE.RelocationType)((complexEntry & 0xF000) >> 12);
                relocation.VirtualAddress = VirtualAddress;
                relocation.VirtualAddress += (ulong)(complexEntry & 0x0FFF);
                Assembly._Relocations.Add(relocation);
                Offset += 2;
                EntryCount--;
            }
        }

        static internal void ReadPE_NT_ImportTable(byte[] AssemblyData, ulong Offset, int Size, Assembly_PE Assembly)
        {
            ulong end = Offset + (ulong)Size;
            while (Offset < end)
            {
                ulong rva = (ulong)AssemblyData[Offset] +
                            (ulong)AssemblyData[Offset + 1] * 0x100 +
                            (ulong)AssemblyData[Offset + 2] * 0x10000 +
                            (ulong)AssemblyData[Offset + 3] * 0x1000000;
                rva = Assembly.RVAToOffset(rva);
                if (rva == 0) { return; }
                Offset += 4; // rva
                Offset += 4; // ForwardChain
                Offset += 4; // timestamp
                ulong namerva = (ulong)AssemblyData[Offset] +
                                (ulong)AssemblyData[Offset + 1] * 0x100 +
                                (ulong)AssemblyData[Offset + 2] * 0x10000 +
                                (ulong)AssemblyData[Offset + 3] * 0x1000000;
                namerva = Assembly.RVAToOffset(namerva);
                string dllname = ReadPE_ASCIIZ(AssemblyData, namerva);
                string fullpath = Fact.Internal.Information.Where(dllname);
                if (fullpath == "") { fullpath = dllname; }
                Offset += 4; // name
                Offset += 4; // FirstTrunk
                if (rva != 0)
                {
                    List<ulong> thunkdata = new List<ulong>();
                    ulong data = 0;
                    do
                    {
                        if (Assembly._64BitsImage)
                        {
                            data = (ulong)AssemblyData[rva] +
                               (ulong)AssemblyData[rva + 1] * 0x100 +
                               (ulong)AssemblyData[rva + 2] * 0x10000 +
                               (ulong)AssemblyData[rva + 3] * 0x1000000 +
                               (ulong)AssemblyData[rva + 4] * 0x100000000 +
                               (ulong)AssemblyData[rva + 5] * 0x10000000000 +
                               (ulong)AssemblyData[rva + 6] * 0x1000000000000 +
                               (ulong)AssemblyData[rva + 7] * 0x100000000000000;
                            rva += 8;
                        }
                        else
                        {
                            data = (ulong)AssemblyData[rva] +
                                   (ulong)AssemblyData[rva + 1] * 0x100 +
                                   (ulong)AssemblyData[rva + 2] * 0x10000 +
                                   (ulong)AssemblyData[rva + 3] * 0x1000000;
                            rva += 4;
                        }
                        data = Assembly.RVAToOffset(data);
                        if (data != 0)
                        {
                            thunkdata.Add(data);
                        }
                    } while (data != 0);
                    List<string> imports = new List<string>();
                    for (int n = 0; n < thunkdata.Count; n++)
                    {
                        int Hint = AssemblyData[thunkdata[n]] +
                                   AssemblyData[thunkdata[n] + 1] * 0x10;
                        string name = ReadPE_ASCIIZ(AssemblyData, thunkdata[n] + 2);
                        Assembly._AddDependency(fullpath, name);
                    }
                }
            }
        }
    }
}

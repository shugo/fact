﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    class MSILMetadataToken
    {
        public enum TokenType
        {
            Module = 0x00,
            TypeReference = 0x01,
            TypeDefinition = 0x02,
            Field = 0x04,
            Method = 0x06,
            Param = 0x08,
            InterfaceImplementation = 0x09,
            MemberReference = 0x0A,
            CustomAttibute = 0x0C,
            Permission = 0x0E,
            Signature = 0x11,
            Event = 0x14,
            Property = 0x17,
            ModuleReference = 0x1A,
            TypeSpecifier = 0x1B,
            Assembly = 0x20,
            AssemblyReference = 0x23,
            File = 0x26,
            ExportedType = 0x27,
            ManifestResource = 0x28,
            GenericParameter = 0x2A,
            MethodSpecifier = 0x2B,
            String = 0x70,
            Name = 0x71,
            BaseType = 0x72
        }
        uint _Token = 0;
        public MSILMetadataToken(uint Token) { _Token = Token; }
        public TokenType Type { get { unchecked { return (TokenType)((_Token & 0xFF000000) >> 24); } } }
        public uint ID { get { unchecked { return (_Token & 0x00FFFFFF); } } }
        public uint Token { get { return _Token; } }
        internal void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUInt32(Stream, _Token);
        }
        internal static MSILMetadataToken Load(System.IO.Stream Stream)
        {
            uint token = Internal.StreamTools.ReadUInt32(Stream);
            return new MSILMetadataToken(token);
        }
    }
}

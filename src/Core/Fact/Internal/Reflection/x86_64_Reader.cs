﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    class x86_64_Reader
    {
        static unsafe public List<x86_64_Instruction> ReadMethod(Fact.Reflection.Method Method)
        {
#if DEBUG
            Fact.Log.Trace(Method.Name);
#endif
            List<x86_64_Instruction> opcodes = new List<x86_64_Instruction>();
            byte* data = (byte*)Method.GetFunctionPointer().ToPointer();
            int offset = 0;
            while (true)
            {
                int oldoffset = offset;
                try
                {
                    x86_64_Instruction opcode = x86_64_Dasm.GetOpCodeInfo(data, ref offset);
                    if (opcode == null) { break; }
                    opcode._Offset = oldoffset;
                    opcodes.Add(opcode);
                }
                catch { break; }
            }
            return opcodes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public static class MSILWriter
    {
        static public void WriteMethod(System.Reflection.Emit.MethodBuilder Method, List<MSILOpCode> OpCodes)
        {
            System.Reflection.Emit.ILGenerator generator = Method.GetILGenerator();
            WriteMethod(generator, OpCodes);
        }
        static public void WriteMethod(System.Reflection.Emit.ILGenerator ILGenerator, List<MSILOpCode> OpCodes)
        {
            Dictionary<int, System.Reflection.Emit.Label> labels = new Dictionary<int, System.Reflection.Emit.Label>();
            foreach (MSILOpCode opcode in OpCodes)
            {
                switch (opcode.OpCode.FlowControl)
                {
                    case System.Reflection.Emit.FlowControl.Branch:
                    case System.Reflection.Emit.FlowControl.Cond_Branch:
                        {
                            MSILOpCode target = (MSILOpCode)opcode._Data;
                            if (!labels.ContainsKey(target._Offset))
                            {
                                labels.Add(target._Offset, ILGenerator.DefineLabel());
                            }
                        }
                        break;
                }
                if (opcode.Data is MSILOpCode)
                {
                }
            }
            Dictionary<int, System.Reflection.Emit.LocalBuilder> localbuilders = new Dictionary<int, System.Reflection.Emit.LocalBuilder>();
            {
                int local = 0;
                Dictionary<int, System.Reflection.LocalVariableInfo> locals = new Dictionary<int, System.Reflection.LocalVariableInfo>();
                foreach (MSILOpCode opcode in OpCodes)
                {
                    if (opcode._Data is System.Reflection.LocalVariableInfo)
                    {
                        System.Reflection.LocalVariableInfo variableinfo = opcode._Data as System.Reflection.LocalVariableInfo;
                        if (!locals.ContainsKey(variableinfo.LocalIndex))
                        {
                            locals.Add(variableinfo.LocalIndex, variableinfo);
                        }
                    }
                }
                for (int n = 0; n < locals.Count; n++)
                {
                    if (!locals.ContainsKey(n)) { throw new Exception("Uncontigius locals variable indices"); }
                    localbuilders.Add(n, ILGenerator.DeclareLocal(locals[n].LocalType, locals[n].IsPinned));
                }
            }

            for (int n = 0; n < OpCodes.Count; n++)
            {
                if (labels.ContainsKey(OpCodes[n]._Offset))
                {
                    ILGenerator.MarkLabel(labels[OpCodes[n]._Offset]);
                }
                switch (OpCodes[n].OpCode.FlowControl)
                {
                    case System.Reflection.Emit.FlowControl.Branch:
                    case System.Reflection.Emit.FlowControl.Cond_Branch:
                        {
                            MSILOpCode target = (MSILOpCode)OpCodes[n]._Data;
                            ILGenerator.Emit(OpCodes[n].OpCode, labels[target._Offset]);
                        }
                        break;
                    case System.Reflection.Emit.FlowControl.Call:
                        if (OpCodes[n]._Data is System.Reflection.MethodInfo)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, OpCodes[n]._Data as System.Reflection.MethodInfo);
                        }
                        else if (OpCodes[n]._Data is System.Reflection.ConstructorInfo)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, OpCodes[n]._Data as System.Reflection.ConstructorInfo);
                        }
                        else
                        {
                            throw new Exception("Invalid instruction: " + OpCodes[n]);
                        }
                        break;
                    default:
                        if (OpCodes[n]._Data == null) { ILGenerator.Emit(OpCodes[n].OpCode); }
                        else if (OpCodes[n]._Data is System.Reflection.LocalVariableInfo)
                        {
                           System.Reflection.LocalVariableInfo localInfo = OpCodes[n]._Data as System.Reflection.LocalVariableInfo;
                            if (OpCodes[n].OpCode == System.Reflection.Emit.OpCodes.Stloc ||
                                OpCodes[n].OpCode == System.Reflection.Emit.OpCodes.Ldloc ||
                                OpCodes[n].OpCode == System.Reflection.Emit.OpCodes.Stloc_S ||
                                OpCodes[n].OpCode == System.Reflection.Emit.OpCodes.Ldloc_S)
                            {
                                ILGenerator.Emit(OpCodes[n].OpCode, localbuilders[localInfo.LocalIndex]);
                            }
                            else
                            {
                                ILGenerator.Emit(OpCodes[n].OpCode);
                            }
                        }
                        else if (OpCodes[n]._Data is string)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, (OpCodes[n]._Data as string));
                        }
                        else if (OpCodes[n]._Data is System.Reflection.FieldInfo)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, OpCodes[n]._Data as System.Reflection.FieldInfo);
                        }
                        else if (OpCodes[n]._Data is System.Reflection.MethodInfo)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, OpCodes[n]._Data as System.Reflection.MethodInfo);
                        }
                        else if (OpCodes[n]._Data is System.Reflection.ConstructorInfo)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, OpCodes[n]._Data as System.Reflection.ConstructorInfo);
                        }
                        else if (OpCodes[n]._Data is Int32)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, (Int32)OpCodes[n]._Data);
                        }
                        else if (OpCodes[n]._Data is UInt32)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, (UInt32)OpCodes[n]._Data);
                        }
                        else if (OpCodes[n]._Data is Byte)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, (Byte)OpCodes[n]._Data);
                        }
                        else if (OpCodes[n]._Data is SByte)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, (SByte)OpCodes[n]._Data);
                        }
                        else if (OpCodes[n]._Data is Type)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, (Type)OpCodes[n]._Data);
                        }
                        else if (OpCodes[n]._Data is MSILMetadataToken)
                        {
                            ILGenerator.Emit(OpCodes[n].OpCode, (OpCodes[n]._Data as MSILMetadataToken).Token);
                        }
                        else
                        {
                            throw new Exception("Invalid instruction: " + OpCodes[n]);
                        }
                        break;
                }
            }
        }
    }
}

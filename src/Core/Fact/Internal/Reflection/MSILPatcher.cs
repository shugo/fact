﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public abstract class MSILPatcher
    {
        public MSILPatcher(System.Reflection.Assembly Assembly, System.Reflection.AssemblyName PatchedAssemblyName, System.Reflection.Emit.AssemblyBuilderAccess PatchedAssemblyAccess)
        {
            Build(Assembly, PatchedAssemblyName, PatchedAssemblyAccess);
        }

        Dictionary<int, System.Reflection.Emit.ModuleBuilder> _Modules = new Dictionary<int, System.Reflection.Emit.ModuleBuilder>();
        Dictionary<int, System.Reflection.Emit.TypeBuilder> _Types = new Dictionary<int, System.Reflection.Emit.TypeBuilder>();
        Dictionary<int, System.Reflection.Emit.FieldBuilder> _Fields = new Dictionary<int, System.Reflection.Emit.FieldBuilder>();
        Dictionary<int, System.Reflection.Emit.PropertyBuilder> _Properties = new Dictionary<int, System.Reflection.Emit.PropertyBuilder>();


        System.Reflection.Emit.AssemblyBuilder Build(System.Reflection.Assembly Assembly, System.Reflection.AssemblyName PatchedAssemblyName, System.Reflection.Emit.AssemblyBuilderAccess PatchedAssemblyAccess)
        {
            System.Reflection.Emit.AssemblyBuilder builder = System.AppDomain.CurrentDomain.DefineDynamicAssembly(PatchedAssemblyName, PatchedAssemblyAccess);
            foreach (System.Reflection.Module module in Assembly.GetModules())
            {
                Build(module, builder);
            }
            return builder;
        }

        void Build(System.Reflection.Module Module, System.Reflection.Emit.AssemblyBuilder AssemblyBuilder)
        {
            System.Reflection.Emit.ModuleBuilder builder = AssemblyBuilder.DefineDynamicModule(Module.Name);
            _Modules.Add(Module.MetadataToken, builder);
            foreach (Type type in Module.GetTypes())
            {
                if (type.IsNested) { continue; }
                if (type.IsSubclassOf(typeof(System.Enum)))
                {
                    BuildEnum(type, builder);
                    continue;
                }
                BuildType(type, builder);
            }
        }

        void BuildType(Type Type, System.Reflection.Emit.ModuleBuilder ModuleBuilder)
        {
            System.Reflection.Emit.TypeBuilder builder = ModuleBuilder.DefineType(Type.FullName, Type.Attributes, Type.BaseType);
            _Types.Add(Type.MetadataToken, builder);
            BuildTypeCore(Type, builder);
        }

        void BuildNeastedType(Type Type, System.Reflection.Emit.TypeBuilder TypeBuilder)
        {
            System.Reflection.Emit.TypeBuilder builder = TypeBuilder.DefineNestedType(Type.FullName, Type.Attributes, Type.BaseType);
            _Types.Add(Type.MetadataToken, builder);
            BuildTypeCore(Type, builder);
        }

        void BuildTypeCore(Type Type, System.Reflection.Emit.TypeBuilder TypeBuilder)
        {
            foreach (Type neastedType in Type.GetNestedTypes(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic))
            {
                if (neastedType.IsSubclassOf(typeof(System.Enum)))
                {
                    BuildEnum(neastedType, TypeBuilder);
                    continue;
                }
                BuildNeastedType(neastedType, TypeBuilder);
            }
            foreach (System.Reflection.ConstructorInfo constructor in Type.GetConstructors(System.Reflection.BindingFlags.Public |
                                                                                            System.Reflection.BindingFlags.NonPublic |
                                                                                            System.Reflection.BindingFlags.Instance))
            {
                if (constructor.DeclaringType != Type) { continue; }
                Build(constructor, TypeBuilder);
            }
            foreach (System.Reflection.MethodInfo method in Type.GetMethods(System.Reflection.BindingFlags.Public |
                                                                            System.Reflection.BindingFlags.NonPublic |
                                                                            System.Reflection.BindingFlags.Instance))
            {
                if (method.DeclaringType != Type) { continue; }
                Build(method, TypeBuilder);
            }
            foreach (System.Reflection.PropertyInfo property in Type.GetProperties(System.Reflection.BindingFlags.Public |
                                                                                   System.Reflection.BindingFlags.NonPublic |
                                                                                   System.Reflection.BindingFlags.Instance))
            {
                if (property.DeclaringType != Type) { continue; }
                Build(property, TypeBuilder);
            }
            foreach (System.Reflection.FieldInfo field in Type.GetFields(System.Reflection.BindingFlags.Public |
                                                                         System.Reflection.BindingFlags.NonPublic |
                                                                         System.Reflection.BindingFlags.Instance))
            {
                if (field.DeclaringType != Type) { continue; }
                Build(field, TypeBuilder);
            }
        }

        void BuildEnum(Type Type, System.Reflection.Emit.ModuleBuilder ModuleBuilder)
        {
            //FIX ME
        }

        void BuildEnum(Type Type, System.Reflection.Emit.TypeBuilder TypeBuilder)
        {
            //FIX ME
        }


        void Build(System.Reflection.MethodInfo Method, System.Reflection.Emit.TypeBuilder TypeBuilder)
        {
            //FIX ME
        }

        void Build(System.Reflection.ConstructorInfo Constructor, System.Reflection.Emit.TypeBuilder TypeBuilder)
        {
            // FIX ME
        }


        void Build(System.Reflection.FieldInfo Field, System.Reflection.Emit.TypeBuilder TypeBuilder)
        {
            System.Reflection.Emit.FieldBuilder builder = TypeBuilder.DefineField(Field.Name, Field.FieldType, Field.Attributes);
            _Fields.Add(Field.MetadataToken, builder);
        }

        void Build(System.Reflection.PropertyInfo Property, System.Reflection.Emit.TypeBuilder TypeBuilder)
        {
            System.Reflection.Emit.PropertyBuilder builder = TypeBuilder.DefineProperty(Property.Name, Property.Attributes, Property.PropertyType, null);
            _Properties.Add(Property.MetadataToken, builder);
        }

    }
}

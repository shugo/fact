﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public static class MSILReader
    {
        static public List<MSILOpCode> ReadMethod(System.Delegate Method)
        {
            return ReadMethod(Method.Method);
        }

        internal class ModuleResolver : MSILResolver
        {
            System.Reflection.Module _Module;
            public ModuleResolver(System.Reflection.Module Module) { _Module = Module; }
            public override System.Reflection.FieldInfo ResolveField(int Metadatatoken)
            {
                return _Module.ResolveField(Metadatatoken);
            }
            public override System.Reflection.MethodBase ResolveMethod(int Metadatatoken)
            {
                return _Module.ResolveMethod(Metadatatoken);
            }
            public override Type ResolveType(int Metadatatoken)
            {
                return _Module.ResolveType(Metadatatoken);
            }
            public override string ResolveString(int Metadatatoken)
            {
                return _Module.ResolveString(Metadatatoken);
            }
        }
        static public List<MSILOpCode> ReadMethod(System.Reflection.MethodBase MethodBase)
        {
            ModuleResolver resolver = new ModuleResolver(MethodBase.DeclaringType.Module);
            return ReadMethod(MethodBase, resolver, true, true);
        }

        static public List<MSILOpCode> ReadMethod(System.Reflection.MethodBase MethodBase, MSILResolver Resolver)
        {
            return ReadMethod(MethodBase, Resolver, true, true);
        }

        static internal List<MSILOpCode> ReadMethod(System.Reflection.MethodBase MethodBase, MSILResolver Resolver, bool MustPatchToken, bool MustPatchJump)
        {
#if DEBUG
            Fact.Log.Trace(MethodBase);
#endif
            List<MSILOpCode> opcodes = new List<MSILOpCode>();
            byte[] data = MethodBase.GetMethodBody().GetILAsByteArray();
            int offset = 0;
            while (offset < data.Length)
            {
                int oldoffset = offset;
                MSILOpCode opcode = MSILDasm.GetOpCodeInfo(data, ref offset);
                opcode._Offset = oldoffset;
                opcodes.Add(opcode);
            }
            if (MustPatchToken) { PatchToken(opcodes, Resolver); }
            if (MustPatchJump) { PatchJump(opcodes); }
            PatchLocals(opcodes, MethodBase.GetMethodBody());

            return opcodes;
        }

        static public List<MSILOpCode> ReadMethod(byte[] MethodByteCode, MSILResolver Resolver)
        {
            return ReadMethod(MethodByteCode, Resolver, true, true);
        }

        static internal List<MSILOpCode> ReadMethod(byte[] MethodByteCode, MSILResolver Resolver, bool MustPatchToken, bool MustPatchJump)
        {
#if DEBUG
            Fact.Log.Trace();
#endif
            List<MSILOpCode> opcodes = new List<MSILOpCode>();
            int offset = 0;
            while (offset < MethodByteCode.Length)
            {
                int oldoffset = offset;
                MSILOpCode opcode = MSILDasm.GetOpCodeInfo(MethodByteCode, ref offset);
                opcode._Offset = oldoffset;
                opcodes.Add(opcode);
            }
            if (MustPatchToken) { PatchToken(opcodes, Resolver); }
            if (MustPatchJump) { PatchJump(opcodes); }
            return opcodes;
        }

        static System.Reflection.MethodInfo _FindMethodInfoInType(int Id, Type Type)
        {
            if (Type == null) { return null; }
            foreach (System.Reflection.MethodInfo method in Type.GetMethods())
            {
                if ((method.MetadataToken & 0x00FFFFFF) == Id)
                {
                    return method;
                }
            }

            return null;
        }

        static internal void PatchToken(List<MSILOpCode> opcodes, MSILResolver Resolver)
        {
            foreach (MSILOpCode opcode in opcodes)
            {
                if (opcode.OpCode.FlowControl == System.Reflection.Emit.FlowControl.Call)
                {
                    opcode._Data = Resolver.ResolveMethod((int)((MSILMetadataToken)opcode.Data).Token);
                }
                if (opcode.OpCode.OperandType == System.Reflection.Emit.OperandType.InlineField)
                {
                    opcode._Data = Resolver.ResolveField((int)((MSILMetadataToken)opcode.Data).Token);
                }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldftn)
                {
                    opcode._Data = Resolver.ResolveMethod((int)((MSILMetadataToken)opcode.Data).Token);
                }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldstr)
                {
                    opcode._Data = Resolver.ResolveString((int)((MSILMetadataToken)opcode.Data).Token);
                }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldtoken)
                {
                    // Keep the token
                }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Newarr ||
                    opcode.OpCode == System.Reflection.Emit.OpCodes.Constrained)
                {
                    opcode._Data = Resolver.ResolveType((int)((MSILMetadataToken)opcode.Data).Token);
                }
            }
        }

        static internal void PatchLocals(List<MSILOpCode> opcodes, System.Reflection.MethodBody Body)
        {
            Dictionary<int, System.Reflection.LocalVariableInfo> locals = new Dictionary<int, System.Reflection.LocalVariableInfo>();
            foreach (System.Reflection.LocalVariableInfo localsBody in Body.LocalVariables)
            {
                System.Reflection.LocalVariableInfo _WAR_local = localsBody;
                locals.Add(_WAR_local.LocalIndex, _WAR_local);
            }
            foreach (MSILOpCode opcode in opcodes)
            {
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldloc_0) { opcode._Data = locals[0]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldloc_1) { opcode._Data = locals[1]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldloc_2) { opcode._Data = locals[2]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldloc_3) { opcode._Data = locals[3]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldloc) { opcode._Data = locals[(int)opcode._Data]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Ldloc_S) { opcode._Data = locals[(byte)opcode._Data]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Stloc_0) { opcode._Data = locals[0]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Stloc_1) { opcode._Data = locals[1]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Stloc_2) { opcode._Data = locals[2]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Stloc_3) { opcode._Data = locals[3]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Stloc) { opcode._Data = locals[(int)opcode._Data]; }
                if (opcode.OpCode == System.Reflection.Emit.OpCodes.Stloc_S) { opcode._Data = locals[(byte)opcode._Data]; }
            }
        }
       static internal void PatchJump(List<MSILOpCode> opcodes)
        {
            for (int n = 0; n < opcodes.Count; n++)
            {
                if (opcodes[n].OpCode.FlowControl == System.Reflection.Emit.FlowControl.Branch ||
                    opcodes[n].OpCode.FlowControl == System.Reflection.Emit.FlowControl.Cond_Branch)
                {
                    if (opcodes[n]._Data is List<Int32>)
                    {
                        List<Int32> offsets = (List<Int32>)opcodes[n]._Data;
                        List<MSILOpCode> jumps = new List<MSILOpCode>();
                        opcodes[n]._Data = null;
                        for (int l = 0; l < offsets.Count; l++)
                        {
                            for (int x = 0; x < opcodes.Count; x++)
                            {
                                if (offsets[l] == opcodes[x]._Offset) { jumps.Add(opcodes[x]); break; }
                            }
                        }
                        if (offsets.Count != jumps.Count)
                        {
                            throw new Exception("Invalid switch labels");
                        }
                        opcodes[n]._Data = jumps;
                    }
                    else
                    {
                        int offset = (int)opcodes[n]._Data;
                        opcodes[n]._Data = null;
                        for (int x = 0; x < opcodes.Count; x++)
                        {
                            if (offset == opcodes[x]._Offset) { opcodes[n]._Data = opcodes[x]; break; }
                        }
                        if (opcodes[n]._Data == null)
                        {
                            throw new Exception("Invalid jump: '" + offset.ToString("X") + "'");
                        }
                    }
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public abstract class MSILResolver
    {
        public abstract Type ResolveType(int Metatoken);
        public abstract System.Reflection.MethodBase ResolveMethod(int Metatoken);
        public abstract System.Reflection.FieldInfo ResolveField(int Metatoken);
        public abstract string ResolveString(int Metatoken);
    }
}

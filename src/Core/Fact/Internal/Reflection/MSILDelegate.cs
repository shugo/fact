﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public static class MSILDelegate
    {
        static Dictionary<string, Type> _TypeCache = new Dictionary<string, Type>();
        static MSILDelegate() { }

        static System.Reflection.Emit.AssemblyBuilder _AssemblyBuilder = null;
        static System.Reflection.Emit.ModuleBuilder _ModuleBuilder = null;
        static ulong _Id = 0;
        static void SetupAssemblyBuilder()
        {
            if (_AssemblyBuilder == null)
            {
                System.Reflection.AssemblyName _Name = new System.Reflection.AssemblyName("___fact___internal___msil_dynamic_delegate");
                _AssemblyBuilder = System.AppDomain.CurrentDomain.DefineDynamicAssembly(_Name, System.Reflection.Emit.AssemblyBuilderAccess.Run);
                _ModuleBuilder = _AssemblyBuilder.DefineDynamicModule("dynamic_delegates");
            }
        }

        public static Type CreateDelegateType(Type ReturnType, params Type[] ParametersType)
        {
            lock (_TypeCache)
            {
                SetupAssemblyBuilder();
                unchecked { _Id++; }
                System.Reflection.Emit.TypeBuilder type = _ModuleBuilder.DefineType(
                    "_wrap_" + _Id.ToString(),
                    System.Reflection.TypeAttributes.Class | System.Reflection.TypeAttributes.Public |
                    System.Reflection.TypeAttributes.Sealed | System.Reflection.TypeAttributes.AnsiClass |
                    System.Reflection.TypeAttributes.AutoClass, typeof(System.MulticastDelegate));
                System.Reflection.Emit.MethodBuilder invokeMethod = type.DefineMethod(
                    "Invoke",
                    System.Reflection.MethodAttributes.Public | System.Reflection.MethodAttributes.FamANDAssem |
                    System.Reflection.MethodAttributes.Family | System.Reflection.MethodAttributes.Virtual |
                    System.Reflection.MethodAttributes.HideBySig |
                    System.Reflection.MethodAttributes.VtableLayoutMask, ReturnType, ParametersType);
                invokeMethod.SetImplementationFlags(
                    System.Reflection.MethodImplAttributes.Runtime |
                    System.Reflection.MethodImplAttributes.Managed);

                System.Reflection.Emit.ConstructorBuilder constructor = type.DefineConstructor(System.Reflection.MethodAttributes.Public,
                    System.Reflection.CallingConventions.Any,
                    new Type[] { typeof(object), typeof(IntPtr) });
                constructor.SetImplementationFlags(
                    System.Reflection.MethodImplAttributes.Runtime |
                    System.Reflection.MethodImplAttributes.Managed);

                return type.CreateType();
            }

        }
        public static Delegate CreateDelegateForNativeFunction(IntPtr NativeFunctionPtr, Type ReturnType, params Type[] ParametersType)
        {
            return System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(NativeFunctionPtr, CreateDelegateType(ReturnType, ParametersType));
        }
    }
}

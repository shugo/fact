﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    class x86_64_OpCode
    {
        static public x86_64_OpCode Mov = new x86_64_OpCode() { };
        static public x86_64_OpCode Push = new x86_64_OpCode() { };
        static public x86_64_OpCode Push_Imm8 = new x86_64_OpCode() { };
        static public x86_64_OpCode Push_Imm16 = new x86_64_OpCode() { };
        static public x86_64_OpCode Push_Imm32 = new x86_64_OpCode() { };
        static public x86_64_OpCode Push_Imm64 = new x86_64_OpCode() { };

        static public x86_64_OpCode Pop = new x86_64_OpCode() { };
        static public x86_64_OpCode Pop_mem16 = new x86_64_OpCode() { };
        static public x86_64_OpCode Pop_mem32 = new x86_64_OpCode() { };
        static public x86_64_OpCode Pop_mem64 = new x86_64_OpCode() { };



        static public x86_64_OpCode Ret = new x86_64_OpCode() { };

    }
}

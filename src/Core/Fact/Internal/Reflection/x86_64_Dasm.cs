﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    class x86_64_Dasm
    {
        internal unsafe static x86_64_Instruction GetOpCodeInfo(byte* bytecode, ref int offset)
        {

            byte opcode = bytecode[offset]; offset++;
            byte rexPrefix = 0x00;
            if (opcode >= 0x48 && opcode <= 0x4F) { rexPrefix = opcode; opcode = bytecode[offset]; offset++; }
            switch (opcode)
            {
                case 0x50: return new x86_64_Instruction() { _OpCode = x86_64_OpCode.Push };
                case 0x58: return new x86_64_Instruction() { _OpCode = x86_64_OpCode.Pop };
                case 0xC3: return new x86_64_Instruction() { _OpCode = x86_64_OpCode.Ret };
            }
            return null;
        }
    }
}

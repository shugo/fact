﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public class MSILOpCode
    {
        System.Reflection.Emit.OpCode _OpCode = System.Reflection.Emit.OpCodes.Unaligned;
        internal object _Data = null;
        internal int _Offset = 0;
        public System.Reflection.Emit.OpCode OpCode { get { return _OpCode; } }
        public object Data { get { return _Data; } set { _Data = value; } }
        public MSILOpCode(System.Reflection.Emit.OpCode OpCode, object Data)
        { _Data = Data; _OpCode = OpCode; }
        public override string ToString()
        {
            if (_Data == null) { return _OpCode.Name; }
            else { return _OpCode.Name + " " + _Data.ToString(); }
        }

        internal void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteInt16(Stream, _OpCode.Value);
            Internal.StreamTools.WriteInt32(Stream, _Offset);
            Internal.StreamTools.Serialize(Stream, _Data);
        }
        internal static MSILOpCode Load(System.IO.Stream Stream)
        {
            System.Reflection.Emit.OpCode opcode = MSILDasm.GetOpCodeFromValue(Internal.StreamTools.ReadInt16(Stream));
            int offset = Internal.StreamTools.ReadInt32(Stream);
            object data = Internal.StreamTools.Deserialize(Stream);
            if (data is MSILOpCode) { data = (data as MSILOpCode)._Offset; }
            return new MSILOpCode(opcode, data) { _Data = data };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    internal class MSILPortableToken
    {
        internal enum TokenType
        {
            FIELD,
            TYPE,
            METHOD,
            NONE
        }
        internal TokenType _Type = TokenType.NONE;
        internal string _AssemblyName = "";
        internal string _AssemblyFullName = "";
        internal Version _AssemblyVersion = new Version(0, 0, 0);
        internal string _ModuleName = "";
        internal string _TypeName = "";
        internal string _FieldName = "";
        internal string _MethodName = "";





        internal static MSILPortableToken Field(int MetadataToken, System.Reflection.Module Module)
        {
            MSILPortableToken token = new MSILPortableToken();
            System.Reflection.FieldInfo fieldInfo = Module.ResolveField(MetadataToken);
            token._Type = TokenType.FIELD;
            token._AssemblyFullName = fieldInfo.DeclaringType.Assembly.GetName().FullName;
            token._AssemblyName = fieldInfo.DeclaringType.Assembly.GetName().Name;
            token._AssemblyVersion = fieldInfo.DeclaringType.Assembly.GetName().Version;
            token._ModuleName = Module.FullyQualifiedName;
            token._TypeName = fieldInfo.DeclaringType.FullName;
            token._FieldName = fieldInfo.Name;
            return token;
        }

        internal static MSILPortableToken Method(int MetadataToken, System.Reflection.Module Module)
        {
            MSILPortableToken token = new MSILPortableToken();
            System.Reflection.MethodBase methodInfo = Module.ResolveMethod(MetadataToken);
            token._Type = TokenType.METHOD;
            token._AssemblyFullName = methodInfo.DeclaringType.Assembly.GetName().FullName;
            token._AssemblyName = methodInfo.DeclaringType.Assembly.GetName().Name;
            token._AssemblyVersion = methodInfo.DeclaringType.Assembly.GetName().Version;
            token._ModuleName = Module.FullyQualifiedName;
            token._TypeName = methodInfo.DeclaringType.FullName;
            token._MethodName = methodInfo.Name;
            return token;
        }

        internal static MSILPortableToken Type(int MetadataToken, System.Reflection.Module Module)
        {
            MSILPortableToken token = new MSILPortableToken();
            System.Type typeInfo = Module.ResolveType(MetadataToken);
            token._Type = TokenType.TYPE;
            token._AssemblyFullName = typeInfo.DeclaringType.Assembly.GetName().FullName;
            token._AssemblyName = typeInfo.DeclaringType.Assembly.GetName().Name;
            token._AssemblyVersion = typeInfo.DeclaringType.Assembly.GetName().Version;
            token._ModuleName = Module.FullyQualifiedName;
            token._TypeName = typeInfo.DeclaringType.FullName;
            return token;
        }

        internal void Save(System.IO.Stream Stream)
        {
            StreamTools.WriteInt32(Stream, (int)_Type);
            StreamTools.WriteUTF8String(Stream, _AssemblyName);
            StreamTools.WriteUTF8String(Stream, _AssemblyFullName);
            StreamTools.WriteVersion(Stream, _AssemblyVersion);
            StreamTools.WriteUTF8String(Stream, _ModuleName);
            StreamTools.WriteUTF8String(Stream, _TypeName);
            StreamTools.WriteUTF8String(Stream,_FieldName);



        }
    }
}

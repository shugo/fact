﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal
{
    public class Monitor
    {
        public class ProcessInfo
        {
            public string Name {get; internal set;}
			public string CommandLine {get; internal set;}
            public ProcessInfo Parent {get; internal set;}
            public int Id {get; internal set;}
            public ulong MemoryUsed { get; internal set; }
            public Dictionary<int, ProcessInfo> Children { get { return _Children; } }
			public object Tag { get; set; }
            Dictionary<int, ProcessInfo> _Children = new Dictionary<int, ProcessInfo>();
            public DateTime StartDate { get; internal set; }
            internal void AddChild(ProcessInfo child)
            {
                if (!_Children.ContainsKey(child.Id)) { _Children.Add(child.Id, child); }
            }
        }

		public ulong TotalMemory { get; internal set; }
		public ulong UsedMemory { get; internal set; }

		static ulong _LinuxGetFreeMemory()
		{
			try
			{
				string[] lines = new string[0];
				if (System.IO.Directory.Exists("/compat/linux/proc/meminfo")) { lines = System.IO.File.ReadAllLines ("/compat/linux/proc/meminfo"); }
				else if (System.IO.Directory.Exists("/proc/meminfo")) { lines = System.IO.File.ReadAllLines ("/proc/meminfo"); }

				for (int n = 0; n < lines.Length; n++)
				{
					lines[n] = lines[n].ToLower();
					string[] words = lines[n].Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					if (words.Length == 2)
					{
						if (words[0] == "memfree:") { return ulong.Parse(words[1]); }
					}
					if (words.Length == 3)
					{
						ulong scaler = 1;
						if (words[2] == "kb") { scaler = 1024; }
						if (words[2] == "mb") { scaler = 1024 * 1024; }

						if (words[0] == "memfree:") { return ulong.Parse(words[1]) * scaler; }
					}
				}
			}
			catch { }
			return 0;
		}

		static ulong _LinuxGetTotalMemory()
		{
			try
			{
				string[] lines = new string[0];
				if (System.IO.Directory.Exists("/compat/linux/proc/meminfo")) { lines = System.IO.File.ReadAllLines ("/compat/linux/proc/meminfo"); }
				else if (System.IO.Directory.Exists("/proc/meminfo")) { lines = System.IO.File.ReadAllLines ("/proc/meminfo"); }

				for (int n = 0; n < lines.Length; n++)
				{
					lines[n] = lines[n].ToLower();
					string[] words = lines[n].Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					if (words.Length == 2)
					{
						if (words[0] == "memtotal:") { return ulong.Parse(words[1]); }
					}
					if (words.Length == 3)
					{
						ulong scaler = 1;
						if (words[2] == "kb") { scaler = 1024; }
						if (words[2] == "mb") { scaler = 1024 * 1024; }

						if (words[0] == "memtotal:") { return ulong.Parse(words[1]) * scaler; }
					}
				}

			}
			catch { }
			return 0;
		}


        Dictionary<int, ProcessInfo> _ProcessTree = new Dictionary<int, ProcessInfo>();

        public Dictionary<int, ProcessInfo> Processes { get { return _ProcessTree; } }

        static string[] _LinuxGetProcessInfo(int PID)
        {
            if (System.IO.Directory.Exists("/compat/linux/proc/" + PID)) { return _LinuxGetProcessInfo("/compat/linux/proc/" + PID); }
            return _LinuxGetProcessInfo("/proc/" + PID);
        }

		static string[] _LinuxGetProcessInfo(string PIDDir)
        {
            try
            {
                string value = System.IO.File.ReadAllText(PIDDir + "/stat");
                string[] info = value.Split(new char[] { ' ', '\t', '\n', '\r' }, System.StringSplitOptions.RemoveEmptyEntries);
                return info;
            }
            catch { }
            return new string[0];
        }

        ProcessInfo _LinuxAddProcess(string[] procinfo)
        {
            if (procinfo == null || procinfo.Length == 0)
                return null;
            ProcessInfo p = new ProcessInfo();
            p.StartDate = DateTime.MinValue;

            string name = procinfo[1];

			if (name.StartsWith("(")) { name = name.Substring(1); }
            if (name.EndsWith(")")) { name = name.Substring(0, name.Length - 1); }
			p.Name = name;



            int PID = 0;
            try
            {
                PID = int.Parse(procinfo[0]);
            }
            catch { }
            p.Id = PID;

			try
			{
				if (System.IO.Directory.Exists("/compat/linux/proc/" + p.Id + "/cmdline" ))
				{ p.CommandLine = System.IO.File.ReadAllText ("/compat/linux/proc/" + p.Id + "/cmdline").Trim(); }
				else if (System.IO.Directory.Exists("/proc/" + p.Id + "/cmdline"))
				{ p.CommandLine = System.IO.File.ReadAllText ("/proc/" + p.Id + "/cmdline").Trim(); }
			}
			catch { }


            if (_ProcessTree.ContainsKey(PID)) { return _ProcessTree[PID]; }
			_ProcessTree.Add (PID, p);
            int PPID = 0;
            try
            {
                PPID = int.Parse(procinfo[3]);
            }
            catch { }
            try
            {
                // Parse the start date

            }
            catch { }
            try
            {
                if (!_ProcessTree.ContainsKey(PPID))
                {
                    _LinuxAddProcess(_LinuxGetProcessInfo(PPID));
                }
                if (_ProcessTree.ContainsKey(PPID))
                {
                    try
                    {
                        // The parent start date must be before the child
                        if (_ProcessTree[PPID].StartDate <= p.StartDate)
                        {
                            p.Parent = _ProcessTree[PPID];
                            p.Parent.AddChild(p);
                        }
                    }
                    catch { }
                }
            }
            catch { }
            return p;
        }

        bool _LinuxRefresh()
        {
            if (System.IO.Directory.Exists("/compat/linux/proc"))
            {
                foreach (string director in System.IO.Directory.GetDirectories("/compat/linux/proc"))
                {
                    try
                    {
                        _LinuxAddProcess(_LinuxGetProcessInfo(director));
                        return true;
                    }
                    catch {  }
                }
                return _ProcessTree.Count > 0;
            }
            else if (System.IO.Directory.Exists("/proc"))
            {
                foreach (string director in System.IO.Directory.GetDirectories("/proc"))
                {
                    try
                    {
                        _LinuxAddProcess(_LinuxGetProcessInfo(director));
                    }
                    catch { }
                }
                return _ProcessTree.Count > 0;
            }

            return false;
        }

        static ProcessInfo _LinuxGetCurrentProcess()
        {
            try
            {
                string[] procinfo = null;
                if (System.IO.File.Exists("/compat/linux/proc/self/stat"))
                {
                    procinfo = _LinuxGetProcessInfo("/compat/linux/proc/self/stat");
                }
                else if (System.IO.File.Exists("/proc/self/stat"))
                {
                    procinfo = _LinuxGetProcessInfo("/proc/self/stat");
                }

                if (procinfo == null || procinfo.Length < 3) { return null; }
                Monitor m = new Monitor();
                return m._LinuxAddProcess(procinfo);
            }
            catch
            { }
            return null;
        }

		void _UpdateMemory()
		{
			ulong total = 0;
			try
			{
				total = _LinuxGetTotalMemory ();
			}
			catch { }
			ulong free = 0;
			try
			{
				free = _LinuxGetFreeMemory ();
			}
			catch { }

			if (total != 0) { TotalMemory = total; }
			if (free != 0) { UsedMemory = total - free; }

			if (UsedMemory > TotalMemory) { UsedMemory = TotalMemory; }
		}

        public static ProcessInfo GetCurrentProcess()
        {
            try
            {
				ProcessInfo linuxinfo = _LinuxGetCurrentProcess();
                if (linuxinfo != null) { return linuxinfo; }
            }
            catch { }
            ProcessInfo info = new ProcessInfo();
            try
            {
                System.Diagnostics.Process Process = System.Diagnostics.Process.GetCurrentProcess();
                info.Id = Process.Id;
                info.Parent = null;
                info.Name = Process.ProcessName;
            }
            catch { }
            return info;
        }

        static bool _EnableHack_PPIDForWindows = false;

        void _BasicRefresh()
        {
            Dictionary<int, ProcessInfo> tree = new Dictionary<int, ProcessInfo>();
            Dictionary<int, int> parentTree = new Dictionary<int,int>();
            try
            {
                foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
                {
                    try
                    {
                        ProcessInfo ProcessInfo = new ProcessInfo();
                        try { ProcessInfo.Id = p.Id; } catch { ProcessInfo.Id = 0; }
                        try { ProcessInfo.Name = p.ProcessName; } catch { ProcessInfo.Name = ""; }
                        try { ProcessInfo.Parent = null; } catch { }
                        try { ProcessInfo.StartDate = p.StartTime; } catch { ProcessInfo.StartDate = DateTime.MinValue; }
                        long parentpid = 0;
                        try
                        {
                            // The parent relationship on windows is not documented
                            // but we will still use a dirty hack to find it
                            if (_EnableHack_PPIDForWindows && ProcessInfo.StartDate != DateTime.MinValue)
                            {
                            retry:
                                // This part of the code is Windows specific
                                // Ok this is a risky job here since data can change between calls
                                string name = p.ProcessName;
                                System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName(name);
                                int id = 0;
                                for (; id < processes.Length; id++)
                                {
                                    try
                                    {
                                        if (processes[id].Id == p.Id)
                                        {
                                            // WIndows can recycle the PID we need to be sure that it is the same
                                            // Process
                                            if (processes[id].StartTime == p.StartTime)
                                            {
                                                // Ok so it should be this process call the performence counter asap
                                                string fullname = name + (id == 0 ? "" : "#" + id.ToString());
                                                System.Diagnostics.PerformanceCounter counter =
                                                    new System.Diagnostics.PerformanceCounter("Process", "Creating Process Id", fullname);
                                                parentpid = counter.RawValue;
                                                // The job is not done now we need to check that during the time
                                                // we have collected the data nothing has change
                                                System.Diagnostics.Process[] checkprocesses = System.Diagnostics.Process.GetProcessesByName(name);
                                                if (processes.Length != checkprocesses.Length)
                                                {
                                                    parentpid = 0;
                                                    goto retry;
                                                }
                                                if (checkprocesses[id].Id != p.Id || checkprocesses[id].StartTime != p.StartTime) { parentpid = 0; goto retry; }
                                                // We should now have the correct info

                                            }
                                        }
                                    }
                                    catch { }
                                }
                            }
                        }
                        catch { }
                        try
                        {
                            tree.Add(ProcessInfo.Id, ProcessInfo);
                            if (parentpid != 0)
                            {
                                parentTree.Add(ProcessInfo.Id, (int)parentpid);
                            }
                        }
                        catch { }
                    }
                    catch { }

                }

                foreach (KeyValuePair<int, int> link in parentTree)
                {
                    if (link.Key != link.Value && link.Key != 0 && link.Value != 0)
                    {
                        if (tree.ContainsKey(link.Key) &&
                            tree.ContainsKey(link.Value))
                        {
                            tree[link.Key].Parent = tree[link.Value];
                            tree[link.Key].Parent.AddChild(tree[link.Key]);
                        }
                    }
                }
            }
            catch { }
        }

        public void Refresh()
        { 
            _ProcessTree = new Dictionary<int, ProcessInfo>();
            try
            {
                if (!_LinuxRefresh()) { _BasicRefresh(); }
            }
            catch { }
			_UpdateMemory();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal
{
    class ASN1
    {
        static void EncodeBERLength(System.IO.Stream Stream, int Length)
        {
            if (Length < 128) { Stream.WriteByte((byte)Length); }
            else
            {
                Stream.WriteByte(0x84); // X690 Extended size 1:[ 4 ]
                // The length in big endian
                EncodeInt32BE(Stream, Length);
            }
        }

        static void EncodeInt32BE(System.IO.Stream Stream, Int32 Value)
        {
            byte[] data = System.BitConverter.GetBytes(Value);
            if (!System.BitConverter.IsLittleEndian) { Stream.Write(data, 0, 4); }
            Stream.WriteByte(data[3]);
            Stream.WriteByte(data[2]);
            Stream.WriteByte(data[1]);
            Stream.WriteByte(data[0]);
        }

        class Item
        {
            public virtual void EncodeBER(System.IO.Stream Stream)
            {

            }
        }
        class SEQUENCE : Item
        {
            public SEQUENCE(params Item[] Seq)
            {
                foreach (Item it in Seq)
                {
                    Item _WAR_Item = it;
                    Add(_WAR_Item);
                }
            }
            List<Item> _Items = new List<Item>();
            public void Add(Item Item) { _Items.Add(Item); }
            public override void EncodeBER(System.IO.Stream Stream)
            {
                Stream.WriteByte(30);
                System.IO.MemoryStream memstream = new System.IO.MemoryStream();
                foreach (Item item in _Items)
                {
                    item.EncodeBER(memstream);
                }
                byte[] data = memstream.ToArray();
                EncodeBERLength(Stream, data.Length);
                Stream.Write(data, 0, data.Length);
            }
        }

        class UTF8String : Item
        {
            string _String = "";
            public UTF8String(string String) { _String = String; }
            public override void EncodeBER(System.IO.Stream Stream)
            {
                Stream.WriteByte(0x0C); // UTF8String Type
                byte[] data = System.Text.Encoding.UTF8.GetBytes(_String);
                EncodeBERLength(Stream, data.Length);
                Stream.Write(data, 0, data.Length);
            }
        }

        class IA5String : Item
        {
            string _String = "";
            public IA5String(string String) { _String = String; }
            public override void EncodeBER(System.IO.Stream Stream)
            {
                Stream.WriteByte(0x16); // UTF8String Type
                byte[] data = System.Text.Encoding.ASCII.GetBytes(_String);
                EncodeBERLength(Stream, data.Length);
                Stream.Write(data, 0, data.Length);
            }
        }

        class BIT_STRING : Item
        {
            byte[] _Data = new byte[0];
            public BIT_STRING(byte[] Data) { _Data = Data; }
            public override void EncodeBER(System.IO.Stream Stream)
            {
                Stream.WriteByte(0x03); // BITSTRING Type
                EncodeBERLength(Stream, _Data.Length);
                Stream.Write(_Data, 0, _Data.Length);
            }
        }

        class OCTET_STRING : Item
        {
            byte[] _Data = new byte[0];
            public OCTET_STRING(byte[] Data) { _Data = Data; }
            public override void EncodeBER(System.IO.Stream Stream)
            {
                Stream.WriteByte(0x03); // OCTETSTRING Type
                EncodeBERLength(Stream, _Data.Length);
                Stream.Write(_Data, 0, _Data.Length);
            }
        }

        class INTEGER : Item
        {
            int _Value = 0;
            public INTEGER(int Value) { _Value = Value; }
            public override void EncodeBER(System.IO.Stream Stream)
            {
                Stream.WriteByte(0x02); // Integer Type
                EncodeBERLength(Stream, 4);
                // Encode the value as BE
                EncodeInt32BE(Stream, _Value);
            }
        }

        class BOOLEAN : Item
        {
            bool _Value = false;
            public BOOLEAN(bool Value) { _Value = Value; }
            public override void EncodeBER(System.IO.Stream Stream)
            {
                Stream.WriteByte(0x01); // Boolean Type
                EncodeBERLength(Stream, 1);
                if (_Value) { Stream.WriteByte(1); }
                else { Stream.WriteByte(0); }
            }
        }
    }
}

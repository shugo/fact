﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Debugger
{
    public class Debugger
    {
        public class Event
        {
            public virtual int PID { get { return 0; } }
            public virtual void Continue() { }
        }
        public class StartDebuggerEvent : Event
        {
        }

        public class BreakpointEvent : Event
        {
            public virtual ulong Address { get { return 0; } }
        }

        static Debugger _Debugger = null;
        static Debugger()
        {
            if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
            { _Debugger = new Mac(); }
            if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
            { _Debugger = new Linux(); }
            if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            { _Debugger = new Windows(); }
        }

        internal virtual void _Break(int ProcessId)
        {
            throw new Exception("Not Supported");
        }

        internal virtual bool _Attach(int ProcessId)
        {
            throw new Exception("Not Supported");
        }

        internal virtual Debugger.Event _WaitForEvent(int Timeout)
        {
            throw new Exception("Not Supported");
        }

        internal virtual bool _ReadMemory(int ProcessId, IntPtr Address, byte[] Buffer, out int NumberOfBytesRead)
        {
            throw new Exception("Not Supported");
        }

        internal virtual bool _WriteMemory(int ProcessId, IntPtr Address, byte[] Buffer, out int NumberOfBytesWritten)
        {
            throw new Exception("Not Supported");
        }

        public static void Break(int ProcessId)
        {
            _Debugger._Break(ProcessId);
        }

        public static bool Attach(int ProcessId)
        {
            return _Debugger._Attach(ProcessId);
        }

        public static bool Detach()
        {
            return false;
        }

        public static Debugger.Event WaitForEvent(int Timeout)
        {
            return _Debugger._WaitForEvent(Timeout);
        }

        public static bool ReadMemory(int ProcessId, IntPtr Address, byte[] Buffer, out int NumberOfBytesRead)
        {
            return _Debugger._ReadMemory(ProcessId, Address, Buffer, out NumberOfBytesRead);
        }

        public static bool WriteMemory(int ProcessId, IntPtr Address, byte[] Buffer, out int NumberOfBytesWritten)
        {
            return _Debugger._WriteMemory(ProcessId, Address, Buffer, out NumberOfBytesWritten);
        }
    }
}

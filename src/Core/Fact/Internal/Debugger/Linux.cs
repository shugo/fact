﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Debugger
{
    unsafe class Linux : Debugger
    {
        class AttachedProcess
        {
            System.Threading.Thread Thread;
            int _PID = 0;
            public AttachedProcess(int PID, ThreadState Parent)
            {
                Thread = new System.Threading.Thread(Run);
                Thread.IsBackground = true;
                Thread.Start();
                _PID = PID;
            }

            void Run()
            {
                while (true)
                {
                    _Waitpid(_PID, null, 0);
                    // FIX MEs
                }
            }
        }

        class ThreadState
        {
            Queue<Debugger.Event> _Events = new Queue<Event>();
            public bool Attach(int ProcessId)
            {
                if (_Ptrace(PTRACE_ATTACH, ProcessId, null, null) == 0)
                {
                    AttachedProcess attachedProcess = new AttachedProcess(ProcessId, this);
                    return true;
                }
                return false;
            }
            public Debugger.Event WaitForEvent()
            {
                if (_Events.Count > 0) { return null; }
                return _Events.Dequeue();
            }
        }

        static ThreadState GetThreadState()
        {
            lock (_PerThreadStorage)
            {
                object data = _PerThreadStorage.GetData();
                if (data != null && data is ThreadState)
                {
                    return data as ThreadState;
                }
                else
                {
                    ThreadState ThreadState = new Linux.ThreadState();
                    _PerThreadStorage.SetData(ThreadState);
                    return ThreadState;
                }
            }
        }

        internal static IntPtr _pwaitpid;
        internal static IntPtr _pkill;
        internal static IntPtr _pptrace;

        const int PTRACE_ATTACH = 0;

        internal delegate void _dwaitpid(int pid, int* status, int options);
        internal delegate void _dkill(int pid, int signal);
        internal delegate int _dptrace(int request, int pid,  void *addr, void *data);

        internal static _dwaitpid _Waitpid;
        internal static _dkill _Kill;
        internal static _dptrace _Ptrace;

        const int SIGSTOP = 19;

        static Threading.PerThreadStorage _PerThreadStorage;
        static Linux()
        {
            // First find the libc
            string libc = Information.GetLibC();
            IntPtr libcptr = Os.Generic.OpenLibrary(libc);
            _pwaitpid = Os.Generic.GetMethod(libcptr, "waitpid");
            _pkill = Os.Generic.GetMethod(libcptr, "kill");
            _pptrace = Os.Generic.GetMethod(libcptr, "ptrace");
            if (_pkill.ToPointer() != null)
            {
                 _Kill = (_dkill)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(_pkill, typeof(_dkill));
            }
            if (_pwaitpid.ToPointer() != null)
            {
                _Waitpid = (_dwaitpid)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(_pwaitpid, typeof(_dwaitpid));
            }
            if (_pptrace.ToPointer() != null)
            {
                _Ptrace = (_dptrace)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(_pptrace, typeof(_dptrace));
            }
            _PerThreadStorage = new Threading.PerThreadStorage();
        }

        public static void Break(int ProcessId)
        {
            _Kill(ProcessId, SIGSTOP);
        }

        public static bool Attach(int ProcessId)
        {
            ThreadState state = GetThreadState();
            return state.Attach(ProcessId);
        }

        public static Debugger.Event WaitForEvent(int Timeout)
        {
            ThreadState state = GetThreadState();
            // FIXME Implement the timout part
            return state.WaitForEvent();
        }
    }
}

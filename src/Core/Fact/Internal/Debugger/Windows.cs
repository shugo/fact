﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Debugger
{
    unsafe class Windows : Debugger
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool DebugBreakProcess(void* handle);

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool DebugActiveProcess(int ProcessId);

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool DebugSetProcessKillOnExit(bool KillOnExit);

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool WaitForDebugEvent(void* lpDebugEvent, int dwMilliseconds);

        internal const uint DBG_CONTINUE = 0x00010002;
        internal const uint DBG_EXCEPTION_NOT_HANDLED = 0x80010001;

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool ContinueDebugEvent(int ProcessId, int ThreadId, uint dwContinueStatus);

        internal override void _Break(int ProcessId)
        {
            IntPtr ptr = new IntPtr();
            try
            {
              ptr = System.Diagnostics.Process.GetProcessById(ProcessId).Handle;
              DebugBreakProcess(ptr.ToPointer());
            }
            catch { }
        }


        internal override bool _Attach(int ProcessId)
        {
            bool result = DebugActiveProcess(ProcessId);
            if (result)
            {
                DebugSetProcessKillOnExit(false);
            }
            return result;
        }

        internal override bool _ReadMemory(int ProcessId, IntPtr Address, byte[] Buffer, out int NumberOfBytesRead)
        {
            NumberOfBytesRead = 0;
            return false;
        }

        internal override bool _WriteMemory(int ProcessId, IntPtr Address, byte[] Buffer, out int NumberOfBytesWritten)
        {
            NumberOfBytesWritten = 0;
            return false;
        }

        internal struct NTUnionDebugEventInfo
        {
            void* _1;
            void* _2;
            void* _3;
            void* _4;
            void* _5;
            void* _6;
            void* _7;
            void* _8;
            void* _9;
            void* _10;
            void* _11;
            void* _12;
            void* _13;
            void* _14;
            void* _15;
            void* _16;
            void* _17;
            void* _18;
            void* _19;
            void* _20;
            void* _21;
        }

        internal struct NTExceptionRecord
        {
            internal Int32 ExceptionCode;
            internal Int32 ExceptionFlags;
            internal void* ExceptionRecord;
            internal void* ExceptionAddress;
            internal Int32 NumberParameters;
            internal void* ExceptionInformation0;
            void* ExceptionInformation1;
            void* ExceptionInformation2;
            void* ExceptionInformation3;
            void* ExceptionInformation4;
            void* ExceptionInformation5;
            void* ExceptionInformation6;
            void* ExceptionInformation7;
            void* ExceptionInformation8;
            void* ExceptionInformation9;
            void* ExceptionInformation10;
            void* ExceptionInformation11;
            void* ExceptionInformation12;
            void* ExceptionInformation13;
            void* ExceptionInformation14;
        }

        internal struct NTExceptionDebugInfo
        {
            internal NTExceptionRecord ExceptionRecord;
            internal Int32 dwFirstChance;
        }

        struct NTCreateThreadDebugInfo
        {
            internal void* hThread;
            internal void* lpThreadLocalBase;
            internal void* lpStartAddress;
        }

        internal struct NTCreateProcessDebugInfo
        {
            internal void* hFile;
            internal void* hProcess;
            internal void* hThread;
            internal void* lpBaseOfImage;
            internal Int32 dwDebugInfoFileOffset;
            internal Int32 nDebugInfoSize;
            internal void* lpThreadLocalBase;
            internal void* lpStartAddress;
            internal void* lpImageName;
            internal Int16 fUnicode;
        }

        internal struct NTExitThreadDebugInfo
        {
            internal Int32 dwExitCode;
        }

        internal struct NTLoadDllDebugInfo
        {
            internal void* hFile;
            internal void* lpBaseOfDll;
            internal Int32 dwDebugInfoFileOffset;
            internal Int32 nDebugInfoSize;
            internal void* lpImageName;
            internal Int16 fUnicode;
        }

        internal struct NTUnloadDllDebugInfo
        {
            internal void* lpBaseOfDll;
        }

        internal struct NTOutputStringDebugInfo
        {
            internal void* lpDebugStringData;
            internal Int16 fUnicode;
            internal Int16 nDebugStringLength;
        }

        internal struct NTDebugEvent
        {
            internal Int32 dwDebugEventCode;
            internal Int32 dwProcessId;
            internal Int32 dwThreadId;
            internal NTUnionDebugEventInfo u;
        }

        class NTEvent : Debugger.Event
        {
            internal int _PID = 0;
            public override int PID { get { return _PID; } }
            internal int _ThreadID = 0;
            public override void Continue()
            {
                ContinueDebugEvent(_PID, _ThreadID, DBG_CONTINUE);
            }
        }

        class NTStartDebuggerEvent : Debugger.StartDebuggerEvent
        {
            internal int _PID = 0;
            public override int PID { get { return _PID; } }
            internal int _ThreadID = 0;
            public override void Continue()
            {
                ContinueDebugEvent(_PID, _ThreadID, DBG_CONTINUE);
            }
        }


        class NTExceptionDebuggerEvent : Debugger.Event
        {
            internal bool _FirstChance = false;
            internal int _PID = 0;
            public override int PID { get { return _PID; } }
            internal int _ThreadID = 0;
            internal ulong _Address = 0;

            public override void Continue()
            {
                ContinueDebugEvent(_PID, _ThreadID, DBG_EXCEPTION_NOT_HANDLED);
            }
        }

        class NTBreakpointException : Debugger.BreakpointEvent
        {
            internal bool _FirstChance = false;
            internal int _PID = 0;
            public override int PID { get { return _PID; } }
            internal int _ThreadID = 0;
            internal ulong _Address = 0;

            public override void Continue()
            {
                ContinueDebugEvent(_PID, _ThreadID, DBG_EXCEPTION_NOT_HANDLED);
            }
        }

        internal Event _CreateProcessDebugEvent(NTDebugEvent ntevent)
        {
            NTStartDebuggerEvent startDebugger = new NTStartDebuggerEvent();
            startDebugger._PID = ntevent.dwProcessId;
            startDebugger._ThreadID = ntevent.dwThreadId;
            return startDebugger;
        }

        internal Event _CreateThreadDebugEvent(NTDebugEvent ntevent)
        {
            return null;
        }

        internal Event _CreateExceptionDebugEvent(NTDebugEvent ntevent)
        {
            unchecked
            {
                NTExceptionDebugInfo info = *(NTExceptionDebugInfo*)(&ntevent.u);
                if ((uint)info.ExceptionRecord.ExceptionCode == 0x80000003)
                {
                    NTBreakpointException exception = new NTBreakpointException();
                    exception._PID = ntevent.dwProcessId;
                    exception._ThreadID = ntevent.dwThreadId;
                    exception._FirstChance = info.dwFirstChance != 0;
                    exception._Address = (ulong)info.ExceptionRecord.ExceptionAddress;
                    return exception;
                }
                else
                {
                    NTExceptionDebuggerEvent exception = new NTExceptionDebuggerEvent();
                    exception._PID = ntevent.dwProcessId;
                    exception._ThreadID = ntevent.dwThreadId;
                    exception._FirstChance = info.dwFirstChance != 0;
                    exception._Address = (ulong)info.ExceptionRecord.ExceptionAddress;
                    return exception;
                }
            }
        }

        internal Event _CreateExitProcessDebugEvent(NTDebugEvent ntevent)
        {
            return null;
        }

        internal Event _CreateExitThreadDebugEvent(NTDebugEvent ntevent)
        {
            return null;
        }

        internal Event _CreateLoadDllDebugEvent(NTDebugEvent ntevent)
        {
            return null;
        }
        internal Event _CreateRIPDebugEvent(NTDebugEvent ntevent)
        {
            return null;
        }
        internal Event _UnloadDllDebugEvent(NTDebugEvent ntevent)
        {
            return null;
        }
        internal Event _CreateEvent(NTDebugEvent ntevent)
        {
            NTEvent evt = new NTEvent();
            evt._PID = ntevent.dwProcessId;
            evt._ThreadID = ntevent.dwThreadId;
            return evt;
        }
        internal override Debugger.Event _WaitForEvent(int Timeout)
        {
            NTDebugEvent dgbevent;
            if (WaitForDebugEvent(&dgbevent, Timeout))
            {
                switch (dgbevent.dwDebugEventCode)
                {
                    case 3: return _CreateProcessDebugEvent(dgbevent);
                    // case 2: return _CreateThreadDebugEvent(dgbevent);
                    case 1: return _CreateExceptionDebugEvent(dgbevent);
                    // case 5: return _CreateExitProcessDebugEvent(dgbevent);
                    // case 4: return _CreateExitThreadDebugEvent(dgbevent);
                    // case 6: return _CreateLoadDllDebugEvent(dgbevent);
                    // case 9: return _CreateRIPDebugEvent(dgbevent);
                    // case 7: return _UnloadDllDebugEvent(dgbevent);
                    default:
                        return _CreateEvent(dgbevent);
                }
            }
            return null;
        }


    }
}

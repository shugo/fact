﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal
{
    static public class Information
    {
        public enum OperatingSystem
        {
            Unspecified,
            Unix,
            MicrosoftWindows,
            MicrosoftDOS,
            MicrosoftWindowsCE,
            MicrosoftXBox360,
            MicrosoftXBoxOne,
            SonyPS4,
            AppleMacOSX,
            AppleMacOSOld,
            Unknow,
        }

        public enum OperatingSystemVersion
        {
            Unspecified,
            MicrosoftDOS6_2,
            MicrosoftWindowsCE6,
            MicrosoftWindows,
            MicrosoftWindowsXP,
            MicrosoftWindowsXPSP1,
            MicrosoftWindowsXPSP2,
            MicrosoftWindowsXPSP3,
            MicrosoftWindowsVista,
            MicrosoftWindowsVistaSP1,
            MicrosoftWindows7,
            MicrosoftWindows7SP1,
            MicrosoftWindows8,
            MicrosoftWindows8_1,
            MicrosoftWindows10,
            MicrosoftXBox360,
            MicrosoftXBoxOne,
            Linux,
            LinuxUbuntu,
            LinuxFedora,
            LinuxOpenSUSE,
            FreeBSD,
            FreeBSDSonyPS4,
            OpenBSD,
            NetBSD,
            MacOSX
        }
        // These info come at build

        static public string FactVersionName() { return FactVersion().Major.ToString() + "." + FactVersion().Minor.ToString() + "." + FactVersion().Build.ToString() + " beta1 (Yggdrasil)"; }
        static public Version FactVersion() { return typeof(Information).Assembly.GetName().Version; }
        static public Version FactFileVersion() { return new Version(3, 2); }

        public enum ServiceManager
        {
            Initd,
            Systemd,
            Svchost,
            Unknown,
        }

        public static ServiceManager CurrentServiceManager()
        {
            if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows ||
                CurrentOperatingSystem() == OperatingSystem.MicrosoftWindowsCE ||
                CurrentOperatingSystem() == OperatingSystem.MicrosoftXBox360 ||
                CurrentOperatingSystem() == OperatingSystem.MicrosoftXBoxOne)
            {
                return ServiceManager.Svchost;
            }

            if (CurrentOperatingSystem() == OperatingSystem.Unix)
            {
                Monitor monitor = new Monitor();
                if (monitor.Processes.ContainsKey(1))
                {
                    if (monitor.Processes[1].Name.EndsWith("systemd"))
                    {
                        return ServiceManager.Systemd;
                    }
                }
                else
                {
                    if (!System.IO.File.Exists("/sbin/init"))
                    {
                        return ServiceManager.Unknown;
                    }
                    else
                    {
                        return ServiceManager.Initd;
                    }
                }
            }
            return ServiceManager.Unknown;
        }

        static public bool IsMonoInUse()
        {
            Type t = Type.GetType("Mono.Runtime");
            return (t != null);
        }

        static Random _UUIDRandom = new Random();
        static public string GenerateUUID()
        {
            lock (_UUIDRandom)
            {
                byte[] first = new byte[4]; _UUIDRandom.NextBytes(first);
                byte[] h1 = new byte[2]; _UUIDRandom.NextBytes(h1);
                byte[] h2 = new byte[2]; _UUIDRandom.NextBytes(h2);
                byte[] h3 = new byte[2]; _UUIDRandom.NextBytes(h3);
                byte[] last = new byte[4]; _UUIDRandom.NextBytes(last);

                string uuid = "";
                for (int n = 0; n < first.Length; n++) { uuid += first[n].ToString("X2"); } uuid += "-";
                for (int n = 0; n < h1.Length; n++) { uuid += h1[n].ToString("X2"); } uuid += "-";
                for (int n = 0; n < h2.Length; n++) { uuid += h2[n].ToString("X2"); } uuid += "-";
                for (int n = 0; n < h3.Length; n++) { uuid += h3[n].ToString("X2"); } uuid += "-";
                for (int n = 0; n < last.Length; n++) { uuid += last[n].ToString("X2"); }
                return uuid;
            }
        }

        static public byte[] TimeCode(System.DateTime DateTime)
        {
            List<byte> code = new List<byte>();
            code.Add((byte)(DateTime.Year & 0xFF)); 
            code.Add((byte)DateTime.Month);            
            code.Add((byte)DateTime.Day);
            code.Add((byte)DateTime.Hour);
            code.Add((byte)DateTime.Minute);
            return code.ToArray();
        }

        static public string GetLibC()
        {
#if DEBUG
            Fact.Log.Debug("Locating libc ...");
#endif
            try
            {
                if (System.IO.File.Exists("/usr/lib/libc.so.6")) { return "/usr/lib/libc.so.6"; }
                if (System.IO.File.Exists("/usr/lib/libc.so")) { return "/usr/lib/libc.so"; }
                if (System.IO.File.Exists("/usr/lib/libc-2.18.so")) { return "/usr/lib/libc-2.18.so"; }
                if (System.IO.Directory.Exists("/usr/lib"))
                {
                    foreach (string file in System.IO.Directory.GetFiles("/usr/lib"))
                    {
                        if (System.IO.Path.GetFileName(file).StartsWith("libc-"))
                            return file;
                        if (System.IO.Path.GetFileName(file).StartsWith("libc.so"))
                            return file;
                    }
                }
                if (System.IO.Directory.Exists("/lib"))
                {
                    foreach (string file in System.IO.Directory.GetFiles("/lib"))
                    {
                        if (System.IO.Path.GetFileName(file).StartsWith("libc-"))
                            return file;
                        if (System.IO.Path.GetFileName(file).StartsWith("libc.so"))
                            return file;
                    }
                }
                if (System.IO.Directory.Exists("/lib64"))
                {
                    foreach (string file in System.IO.Directory.GetFiles("/lib64"))
                    {
                        if (System.IO.Path.GetFileName(file).StartsWith("libc-"))
                            return file;
                        if (System.IO.Path.GetFileName(file).StartsWith("libc.so"))
                            return file;
                    }
                }
            }
            catch
            {

            }
#if DEBUG
            Fact.Log.Debug("libc not found");
#endif
            return "";
        }

        // Global Value used for random uid. Do not remove !!
        static ulong uid = 0;
        static public string GetTempFileName() 
        {
            Random Rnd = new Random((int)(System.DateTime.Now.Millisecond % 10000));
            uint id1 = (uint)Rnd.Next();
            uint id2 = (uint)System.DateTime.Now.Year;
            uint id3 = (uint)System.DateTime.Now.Month;
            uint id4 = (uint)System.DateTime.Now.Day;
            uint id5 = (uint)System.DateTime.Now.Hour;
            uint id6 = (uint)System.DateTime.Now.Minute;
            uint id7 = (uint)System.DateTime.Now.Second;

            uint mix = id1 ^ id2 ^ id3 ^ id5 ^ id6 ^ id7;
            uid++;
            ulong id8 = uid;

            string fileName = "tmp_" + mix.ToString() + "_" + id8.ToString();

            try 
            {
                if (System.IO.File.Exists(fileName)) { return GetTempFileName(); }
            }
            catch { }
            return fileName;
        }

        public enum SpecialFolder
        {
            FactCredentials,
            FactLocalServerData,
            FactConfiguration,
            Home,
        }

        static public string GetSpecialFolders(SpecialFolder folder)
        {
            try
            {
                switch (folder)
                {
                    case SpecialFolder.FactCredentials:
                        {
                            string config = GetSpecialFolders(SpecialFolder.FactConfiguration);
                            if (config != "" && System.IO.Directory.Exists(config))
                            {
                                if (System.IO.Directory.Exists(config + "/credentials")) { return config + "/credentials"; }
                            }
                            return "";
                        }
                    case SpecialFolder.FactConfiguration:
                        {
                            string home = GetSpecialFolders(SpecialFolder.Home);
                            if (home != "" && System.IO.Directory.Exists(home))
                            {
                                if (System.IO.Directory.Exists(home + "/.fact_config")) { return home + "/.fact_config"; }
                            }
                            return "";
                        }
                    case SpecialFolder.Home:
                        if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows)
                        {
                            string home = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                            if (System.IO.Directory.Exists(home)) { return home; }
                        }
                        else
                        {
                            string home = GetEnvVariable("HOME");
                            if (System.IO.Directory.Exists(home)) { return home; }
                            home = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                            if (System.IO.Directory.Exists(home)) { return home; }
                        }
                        return "";
                    case SpecialFolder.FactLocalServerData:
                        if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows)
                        {
                            string localdata = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                            return localdata + "/Fact/Server";
                        }
                        else
                        {
                            string home = GetSpecialFolders(SpecialFolder.Home);
                            string appdata = home + "/.AppData/Fact/Server";
                            return appdata;
                        }
                        return "";
                }
            }
            catch { }
            return "";
        }

		static public bool IsDotNetExecutable(string executable)
		{
			if (!executable.EndsWith (".exe") && !executable.EndsWith (".dll")) 
			{
				return false;
			}

			try 
			{
				System.Reflection.Assembly assbly =
				System.Reflection.Assembly.ReflectionOnlyLoadFrom(executable);
				if (assbly != null)
				{
					assbly = null;
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
			return false;
		}

        static public string GetMSBuild()
        {
            if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows ||
                CurrentOperatingSystem() == OperatingSystem.MicrosoftWindowsCE ||
                CurrentOperatingSystem() == OperatingSystem.MicrosoftXBox360 || 
                CurrentOperatingSystem() == OperatingSystem.MicrosoftXBoxOne)
            {
                if (System.IO.File.Exists("C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe"))
                { return "C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe"; }
                if (System.IO.File.Exists("C:\\Windows\\Microsoft.NET\\Framework64\\v3.5\\MSBuild.exe"))
                { return "C:\\Windows\\Microsoft.NET\\Framework64\\v3.5\\MSBuild.exe"; }
                if (System.IO.File.Exists("C:\\Windows\\Microsoft.NET\\Framework64\\v2.0.50727\\MSBuild.exe"))
                { return "C:\\Windows\\Microsoft.NET\\Framework64\\v2.0.50727\\MSBuild.exe"; }


                if (System.IO.File.Exists("C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\MSBuild.exe"))
                { return "C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\MSBuild.exe"; }
                if (System.IO.File.Exists("C:\\Windows\\Microsoft.NET\\Framework\\v3.5\\MSBuild.exe"))
                { return "C:\\Windows\\Microsoft.NET\\Framework\\v3.5\\MSBuild.exe"; }
                if (System.IO.File.Exists("C:\\Windows\\Microsoft.NET\\Framework\\v2.0.50727\\MSBuild.exe"))
                { return "C:\\Windows\\Microsoft.NET\\Framework\\v2.0.50727\\MSBuild.exe"; }
            }
            return _WhereNoRecurse("xbuild");
        }

        static Stack<Dictionary<string, string>> _EnvStack = new Stack<Dictionary<string, string>>();

        /// <summary>
        /// Make a complete copy of the current environement and push it into a stack.
        /// This method is threadsafe.
        /// </summary>
        static public void PushEnv()
        {
            lock (_EnvStack)
            {
                Dictionary<string, string> env = new Dictionary<string, string>();
                try
                {
                    foreach (KeyValuePair<string, string> variable in System.Environment.GetEnvironmentVariables())
                    {
                        try
                        {
                            env.Add(variable.Key, variable.Value);
                        }
                        catch { }
                    }
                }
                catch { env = null; }
                _EnvStack.Push(env);
            }
        }

        /// <summary>
        /// Restaure a copy of the environement ceated with PushEnv.
        /// This method is threadsafe.
        /// </summary>
        static public void PopEnv()
        {
            lock (_EnvStack)
            {
                if (_EnvStack.Count == 0) { return; }
                Dictionary<string, string> env = _EnvStack.Pop();
                if (env == null) { return; }
                foreach (KeyValuePair<string, string> variable in env)
                {
                    try
                    {
                        System.Environment.SetEnvironmentVariable(variable.Key, variable.Value);
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Completely clear the environement variables. You should use PushEnv before calling this method.
        /// This method is threadsafe.
        /// </summary>
        static public void ClearEnv()
        {
            lock (_EnvStack)
            {
                foreach (KeyValuePair<string, string> variable in System.Environment.GetEnvironmentVariables())
                {
                    try
                    {
                        System.Environment.SetEnvironmentVariable(variable.Key, null);
                    }
                    catch 
                    {
                        try
                        {
                            System.Environment.SetEnvironmentVariable(variable.Key, "");
                        }
                        catch { }
                    }
                }
            }
        }

        static public string GetEnvVariable(string Name)
        {
            try
            {
                foreach (object key in System.Environment.GetEnvironmentVariables().Keys)
                {
                    try
                    {
                        if (key.ToString() == Name) { return System.Environment.GetEnvironmentVariable(Name); }
                    }
                    catch { }
                }
            }
            catch { }
            return "";
        }

        static public void SetEnvVariable(string Name, string Value)
        {
            try
            {
                System.Environment.SetEnvironmentVariable(Name, Value);
            }
            catch
            { }
        }

        static public string GetEnvPath()
        {
            try
            {
                string value = GetEnvVariable("PATH") + GetEnvVariable("Path") + GetEnvVariable("path");
                return value;
            }
            catch { }
            return "";
        }

        static public void AppendEnvPath(string path)
        {
            if (path.EndsWith("/") || path.EndsWith("\\"))
            {
                if (path.Length == 1) { return; }
                path = path.Substring(0, path.Length - 1);
            }
            try
            {
                if (GetEnvVariable("PATH") != "") 
                {
					if (CurrentOperatingSystem() == OperatingSystem.Unix ||
					    CurrentOperatingSystem() == OperatingSystem.AppleMacOSX)
					{
						System.Environment.SetEnvironmentVariable("PATH", GetEnvVariable("PATH") + ":" + path);
						return;
					}
					else
					{
	                    System.Environment.SetEnvironmentVariable("PATH", GetEnvVariable("PATH") + ";" + path);
	                    return;
					}
                }
            }
            catch { }

            try
            {
                if (GetEnvVariable("Path") != "")
                {
					if (CurrentOperatingSystem() == OperatingSystem.Unix ||
					    CurrentOperatingSystem() == OperatingSystem.AppleMacOSX)
					{
						System.Environment.SetEnvironmentVariable("Path", GetEnvVariable("PATH") + ":" + path);
						return;
					}
					else
					{
	                    System.Environment.SetEnvironmentVariable("Path", GetEnvVariable("Path") + ";" + path);
	                    return;
					}
                }
            }
            catch { }

            try
            {
                if (GetEnvVariable("path") != "")
                {
					if (CurrentOperatingSystem() == OperatingSystem.Unix ||
					    CurrentOperatingSystem() == OperatingSystem.AppleMacOSX)
					{
	                    System.Environment.SetEnvironmentVariable("path", GetEnvVariable("path") + ":" + path);
	                    return;
					}
					else
					{
						System.Environment.SetEnvironmentVariable("path", GetEnvVariable("path") + ";" + path);
						return;;
					}
                }
            }
            catch { }

            System.Environment.SetEnvironmentVariable("PATH", path);
        }

        static string _RemoveSheBangOption(string SheBang)
        {
            SheBang = SheBang.Trim();
            string newSheBang = "";
            bool esc = false;
            for (int n = 0; n < SheBang.Length; n++)
            {
                if (esc) { newSheBang += SheBang[n]; esc = false; }
                else
                {
                    if (SheBang[n] == ' ' || SheBang[n] == '\t') { return newSheBang; }
                    else if (SheBang[n] == '\\') { esc = true; }
                    else { newSheBang += SheBang[n]; }
                }
            }
            return newSheBang;
        }

        static public string SolveSheBang(string SheBang)
        {
            SheBang.Replace('\t', ' ');
            SheBang = SheBang.Trim();
            if (SheBang.StartsWith("#!")) { SheBang = SheBang.Substring(2); }
            else { return GetUnixShell(); }
            SheBang = SheBang.Trim();
            if (SheBang.StartsWith("/usr/bin/env ") || SheBang.StartsWith("/bin/env "))
            {

                if (SheBang.StartsWith("/usr/bin/env ")) { SheBang = SheBang.Substring("/usr/bin/env ".Length).Trim(); }
                else if (SheBang.StartsWith("/bin/env ")) { SheBang = SheBang.Substring("/bin/env ".Length).Trim(); }
                SheBang = _RemoveSheBangOption(SheBang);
                string file = Where(SheBang);
                if (System.IO.File.Exists(file))
                {
                    return file;
                }
                if (SheBang == "perl") { return Where("perl"); }
                if (SheBang == "python2") { return Where("python2"); }
                if (SheBang == "ksh") { return GetUnixShell(); }
                if (SheBang == "zsh") { return GetUnixShell(); }
                if (SheBang == "bash") { return GetUnixShell(); }
            }

            SheBang = _RemoveSheBangOption(SheBang);
            if (System.IO.File.Exists(SheBang))
            {
                return SheBang;
            }

            if (SheBang == "/usr/bin/bash" ||
                SheBang == "/bin/bash" ||
                SheBang == "/usr/local/bin/bash")
            {
                string file = Where("bash");
                if (System.IO.File.Exists(file))
                    return file;
                return GetUnixShell();
            }
            if (SheBang == "/usr/bin/ksh" ||
                SheBang == "/bin/ksh" ||
                SheBang == "/usr/local/bin/ksh")
            {
                string file = Where("ksh");
                if (System.IO.File.Exists(file))
                    return file;
                return Where("sh");
            }
            if (SheBang == "/usr/bin/zsh" ||
                SheBang == "/bin/zsh" ||
                SheBang == "/usr/local/bin/zsh")
            {
                string file = Where("zsh");
                if (System.IO.File.Exists(file))
                    return file;
                return GetUnixShell();
            }
            if (SheBang == "/usr/bin/sh" ||
                SheBang == "/bin/sh" ||
                SheBang == "/usr/local/bin/sh")
            {
                return GetUnixShell();
            }

            if (SheBang == "/usr/bin/python" ||
                SheBang == "/bin/python" ||
                SheBang == "/usr/local/bin/python")
            {
                return GetPythonBinary();
            }
            return SheBang;
        }

        static string _WhereNoRecurse(string Binary)
        {
#if DEBUG
            Fact.Log.Debug("Locating file " + Binary + " (non recursive)");
#endif
            if (Binary.EndsWith(".so") || Binary.EndsWith(".dll"))
            {
                if (CurrentOperatingSystem() == OperatingSystem.Unix)
                {
                    string ldlibpath = GetEnvVariable("LD_LIBRARY_PATH");
                    string[] libpaths = ldlibpath.Split(':');
                    foreach (string libpath in libpaths)
                    {
                        if (libpath.Trim() != "")
                        {
                            if (System.IO.File.Exists(libpath + "/" + Binary))
                            {
                                return libpath + "/" + Binary;
                            }
                        }
                    }
                }
            }

            string path = GetEnvPath();
            if (path.Contains(";"))
            {
                string[] paths = path.Split(';');
                foreach (string cstdir in paths)
                {
                    string dir = cstdir;
                    if (!dir.EndsWith("/") && !dir.EndsWith("\\"))
                    {
                        dir += "/";
                    }
                    if (System.IO.File.Exists(dir + Binary))
                    {
                        return dir + Binary;
                    }
                }
            }
            else if (path.Contains(":"))
            {
                string[] paths = path.Split(':');
                foreach (string cstdir in paths)
                {
                    string dir = cstdir;
                    if (!dir.EndsWith("/") && !dir.EndsWith("\\"))
                    {
                        dir += "/";
                    }
                    if (System.IO.File.Exists(dir + Binary))
                    {
                        return dir + Binary;
                    }
                }
            }
            else
            {
                if (!path.EndsWith("/") && !path.EndsWith("\\"))
                {
                    path += "/";
                }
                if (System.IO.File.Exists(path + Binary)) { return path + Binary; }
            }

            if (System.IO.File.Exists("/bin/" + Binary)) { return "/bin/" + Binary; }
            if (System.IO.File.Exists("/usr/bin/" + Binary)) { return "/usr/bin/" + Binary; }
            if (System.IO.File.Exists("/usr/local/bin/" + Binary)) { return "/usr/local/bin/" + Binary; }
            if (System.IO.File.Exists("/usr/sbin/" + Binary)) { return "/usr/sbin/" + Binary; }
            if (System.IO.File.Exists("/usr/local/sbin/" + Binary)) { return "/usr/local/sbin/" + Binary; }
            if (System.IO.File.Exists("/usr/share/bin/" + Binary)) { return "/usr/share/bin/" + Binary; }
            if (System.IO.File.Exists("/usr/share/sbin/" + Binary)) { return "/usr/share/sbin/" + Binary; }
            if (System.IO.File.Exists("C:\\Windows\\System32\\" + Binary + ".exe")) { return "C:\\Windows\\System32\\" + Binary + ".exe"; }
            if (System.IO.File.Exists("C:\\Windows\\SysWOW64\\" + Binary + ".exe")) { return "C:\\Windows\\SysWOW64\\" + Binary + ".exe"; }
            if (System.IO.File.Exists("C:\\" + Binary + "\\" + Binary + ".exe")) { return "C:\\" + Binary + "\\" + Binary + ".exe"; }
            if (System.IO.File.Exists("C:\\cygwin\\bin\\" + Binary + ".exe")) { return "C:\\cygwin\\bin\\" + Binary + ".exe"; }
            if (System.IO.File.Exists("C:\\cygwin\\usr\\local\\bin\\" + Binary + ".exe")) { return "C:\\cygwin\\usr\\local\\bin\\" + Binary + ".exe"; }
            if (System.IO.File.Exists("C:\\cygwin\\usr\\sbin\\" + Binary + ".exe")) { return "C:\\cygwin\\usr\\sbin\\" + Binary + ".exe"; }

#if DEBUG
            Fact.Log.Debug("File " + Binary + " not found (non recursive)");
#endif
            return "";
        }

        static public string Where(string Binary)
        {
#if DEBUG
            Fact.Log.Debug("Locating file " + Binary + " ...");
#endif
            string Path = _WhereNoRecurse(Binary);
            if (Path != "" && System.IO.File.Exists(Path)) { return Path; }
            if (Binary == "gcc")
            {
                if (System.IO.File.Exists("C:\\cygwin\\bin\\" + Binary + "-5.exe")) { return "C:\\cygwin\\bin\\" + Binary + "-5.exe"; }
                if (System.IO.File.Exists("C:\\cygwin\\bin\\" + Binary + "-4.exe")) { return "C:\\cygwin\\bin\\" + Binary + "-4.exe"; }
                if (System.IO.File.Exists("C:\\cygwin\\bin\\" + Binary + "-3.exe")) { return "C:\\cygwin\\bin\\" + Binary + "-3.exe"; }
                string gcc = GetCCBinary();
                if (System.IO.File.Exists(gcc) && gcc.Contains("gcc")) { return gcc; }
            }

            if (Binary == "libc" || Binary == "libc.so" || Binary == "libc.so.6")
            {
                string libc = GetLibC(); if (libc != "" && System.IO.File.Exists(libc)) { return libc; }
            }
            if (Binary == "cl" || Binary == "cl.exe")
            {
                string cl = GetCCBinary(); if (cl != "" && System.IO.File.Exists(cl) && cl.ToLower().EndsWith("cl.exe")) { return cl; }
            }
            if (Binary == "make" || Binary == "make.exe")
            {
                string make = GetMakeBinary(); if (make != "" && System.IO.File.Exists(make)) { return make; }
            }

            if (Binary == "mono" || Binary == "mono.exe")
            {
                string mono = GetMonoBinary(); if (mono != "" && System.IO.File.Exists(mono)) { return mono; }
            }
            if (Binary == "chmod" || Binary == "chmod.exe")
            {
                string chmod = GetChmodBinary(); if (chmod != "" && System.IO.File.Exists(chmod)) { return chmod; }
            }
            if (Binary == "p4" || Binary == "p4.exe")
            {
                string perforce = GetPerforceBinary(); if (perforce != "" && System.IO.File.Exists(perforce)) { return perforce; }
            }
#if DEBUG
            Fact.Log.Debug("File " + Binary + " not found");
#endif
            return "";
        }

        static public string GetChmodBinary()
        {
			if (System.IO.File.Exists ("/bin/chmod"))
				return "/bin/chmod";
            string file = _WhereNoRecurse("chmod");
            if (file == "")
                return "chmod";
            return file;
        }

        static public string GetPerforceBinary()
        {
            string file = _WhereNoRecurse("p4");
            if (file != "" && System.IO.File.Exists(file)) { return file; }
            if (System.IO.File.Exists("/bin/p4")) { return "/bin/p4"; }
            if (System.IO.File.Exists("/usr/bin/p4")) { return "/usr/bin/p4"; }
            if (System.IO.File.Exists(@"C:\Program Files (x86)\Perforce\bin\p4.exe")) { return @"C:\Program Files (x86)\Perforce\bin\p4.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Perforce\bin\p4.exe")) { return @"C:\Program Files\Perforce\bin\p4.exe"; }
            return "p4";
        }

        static public string GetGitBinary()
        {
            string file = _WhereNoRecurse("git");
            if (file != "" && System.IO.File.Exists(file)) { return file; }
            if (System.IO.File.Exists("/bin/git")) { return "/bin/git"; }
            if (System.IO.File.Exists("/usr/bin/git")) { return "/usr/bin/git"; }
            if (System.IO.File.Exists(@"C:\Program Files (x86)\Git\bin\git.exe")) { return @"C:\Program Files (x86)\Git\bin\git.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Git\bin\git.exe")) { return @"C:\Program Files\Git\bin\git.exe"; }
            if (System.IO.File.Exists(@"C:\cygwin\bin\git.exe")) { return @"C:\cygwin\bin\git.exe"; }
            return "git";
        }

        static public string GetSVNBinary()
        {
            string file = _WhereNoRecurse("svn");
            if (file != "" && System.IO.File.Exists(file)) { return file; }
            if (System.IO.File.Exists("/bin/svn")) { return "/bin/svn"; }
            if (System.IO.File.Exists("/usr/bin/svn")) { return "/usr/bin/svn"; }
            return "svn";
        }

        static public string GetPythonBinary()
        {
            string file = _WhereNoRecurse("python");
            if (file != "" && System.IO.File.Exists(file)) { return file; }
            if (System.IO.File.Exists("/bin/python")) { return "/bin/python"; }
            if (System.IO.File.Exists("/usr/bin/python")) { return "/usr/bin/python"; }
            if (System.IO.File.Exists(@"C:\Python27\python.exe")) { return @"C:\Python27\python.exe"; }
            if (System.IO.File.Exists(@"C:\Python26\python.exe")) { return @"C:\Python26\python.exe"; }
            return "python";
        }

        static public string GetCUDADriver()
        {
            if (CurrentOperatingSystem() == OperatingSystem.Unix)
            {
                string file = _WhereNoRecurse("libcuda.so");
                if (file != "" && System.IO.File.Exists(file)) { return file; }
            }
            else if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows)
            {
                string file = _WhereNoRecurse("nvcuda.dll");
                if (file != "" && System.IO.File.Exists(file)) { return file; }
            }
            if (System.IO.File.Exists("/lib/libcuda.so")) { return "/lib/libcuda.so"; }
            if (System.IO.File.Exists("/usr/lib/libcuda.so")) { return "/usr/lib/libcuda.so"; }
            if (System.IO.File.Exists("/lib/libcuda.so.1")) { return "/lib/libcuda.so.1"; }
            if (System.IO.File.Exists("/usr/lib/libcuda.so.1")) { return "/usr/lib/libcuda.so.1"; }
            if (System.IO.File.Exists(@"C:\Windows\System32\nvcuda.dll")) { return @"C:\Windows\System32\nvcuda.dll"; }
            return "nvcuda.dll";
        }

        static public string GetMonoBinary()
        {
            string mono = _WhereNoRecurse("mono");
            if (mono != "" && System.IO.File.Exists(mono))
            { return mono; }

            if (GetEnvVariable("MONO") != "")
            {
                string path = GetEnvVariable("MONO");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }

            if (System.IO.File.Exists("/bin/mono")) { return "/bin/mono"; }
            if (System.IO.File.Exists("/usr/bin/mono")) { return "/usr/bin/mono"; }
            if (System.IO.File.Exists("/usr/local/bin/mono")) { return "/usr/local/bin/mono"; }

            if (System.IO.File.Exists("C:\\Program Files (x86)\\Mono-3.2.1\\bin\\mono.exe"))
            { return "C:\\Program Files (x86)\\Mono-3.2.1\\bin\\mono.exe"; }
            if (System.IO.File.Exists("C:\\Program Files (x86)\\Mono-3.2.9\\bin\\mono.exe"))
            { return "C:\\Program Files (x86)\\Mono-3.2.9\\bin\\mono.exe"; }
            if (System.IO.File.Exists("C:\\Program Files (x86)\\Mono-2.10.10\\bin\\mono.exe"))
            { return "C:\\Program Files (x86)\\Mono-2.10.10\\bin\\mono.exe"; }
            if (System.IO.File.Exists("C:\\Program Files (x86)\\Mono-2.10.9\\bin\\mono.exe"))
            { return "C:\\Program Files (x86)\\Mono-2.10.9\\bin\\mono.exe"; }
            if (System.IO.File.Exists("C:\\Program Files (x86)\\Mono-2.10.8\\bin\\mono.exe"))
            { return "C:\\Program Files (x86)\\Mono-2.10.8\\bin\\mono.exe"; }
            if (System.IO.File.Exists("C:\\Program Files (x86)\\Mono-2\\bin\\mono.exe"))
            { return "C:\\Program Files (x86)\\Mono-2\\bin\\mono.exe"; }
            if (System.IO.File.Exists("C:\\Program Files (x86)\\Mono\\bin\\mono.exe"))
            { return "C:\\Program Files (x86)\\Mono\\bin\\mono.exe"; }
            return "mono";
        }

        static public string GetJAVABinary()
        {
            // Try to find a variable that contain the path of JAVA

            if (GetEnvVariable("JAVA") != "")
            {
                string path = GetEnvVariable("JAVA");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }
            if (GetEnvVariable("java") != "")
            {
                string path = GetEnvVariable("java");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }
            if (System.IO.File.Exists("/usr/bin/java")) { return "/usr/bin/java"; }
            if (System.IO.File.Exists("/usr/java/jre1.7.0_09/bin/java")) { return "/usr/java/jre1.7.0_09/bin/java"; }
            if (System.IO.File.Exists("/usr/java/jre1.7.0_06/bin/java")) { return "/usr/java/jre1.7.0_06/bin/java"; }
            if (System.IO.File.Exists("/usr/java/jre1.7.0/bin/java")) { return "/usr/java/jre1.7.0/bin/java"; }
            if (System.IO.File.Exists("/usr/java/jre1.6.0_06/bin/java")) { return "/usr/java/jre1.6.0_06/bin/java"; }
            if (System.IO.File.Exists("/usr/java/jre1.6.0/bin/java")) { return "/usr/java/jre1.6.0/bin/java"; }
            if (System.IO.File.Exists("/usr/java/jre1.5.0_06/bin/java")) { return "/usr/java/jre1.5.0_06/bin/java"; }
            if (System.IO.File.Exists("/usr/java/jre1.5.0/bin/java")) { return "/usr/java/jre1.5.0/bin/java"; }
            if (System.IO.File.Exists("/usr/lib/SunJava2-1.4.2/jre/bin/java")) { return "/usr/lib/SunJava2-1.4.2/jre/bin/java"; }

            if (System.IO.Directory.Exists("/usr/java"))
            {
                foreach (string directory in System.IO.Directory.GetDirectories("/usr/java/"))
                {
                    if (System.IO.File.Exists(directory + "/bin/java")) { return directory + "/bin/java"; }
                    if (System.IO.File.Exists(directory + "bin/java")) { return directory + "bin/java"; }
                    if (System.IO.File.Exists(directory + "/lib/java")) { return directory + "/lib/java"; }
                    if (System.IO.File.Exists(directory + "lib/java")) { return directory + "lib/java"; }
                }
            }

            string systemDir = System.Environment.SystemDirectory;

            if (CurrentOperatingSystem() == OperatingSystem.Unix ||
                CurrentOperatingSystem() == OperatingSystem.AppleMacOSX)
            {
                if (System.IO.File.Exists(systemDir + "java")) { return systemDir + "java"; }
                if (System.IO.File.Exists(systemDir + "/java")) { return systemDir + "/java"; }
            }
            else if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows)
            {
                if (System.IO.File.Exists(@"C:\Program Files\Java\jdk1.7.0_03\bin\java.exe"))
                { return @"C:\Program Files\Java\jdk1.7.0_03\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Java\jdk1.7\bin\java.exe"))
                { return @"C:\Program Files\Java\jdk1.7\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Java\jdk1.6\bin\java.exe"))
                { return @"C:\Program Files\Java\jdk1.6\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Java\jre7\bin\java.exe"))
                { return @"C:\Program Files\Java\jre7\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Java\jre6\bin\java.exe"))
                { return @"C:\Program Files\Java\jre6\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Java\jre7\bin\java.exe"))
                { return @"C:\Program Files (x86)\Java\jre7\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Java\jre6\bin\java.exe"))
                { return @"C:\Program Files (x86)\Java\jre6\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Java\jre5\bin\java.exe"))
                { return @"C:\Program Files (x86)\Java\jre5\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Java\jre5\bin\java.exe"))
                { return @"C:\Program Files\Java\jre5\bin\java.exe"; }
                if (System.IO.File.Exists(@"C:\Windows\System32\java.exe")) { return @"C:\Windows\System32\java.exe"; }
                if (System.IO.File.Exists(@"C:\Windows\SysWOW64\java.exe")) { return @"C:\Windows\SysWOW64\java.exe"; }
            }

            return "";
        }

        static public string GetJAVACBinary()
        {
            // Try to find a variable that contain the path of JAVAC

            if (GetEnvVariable("JAVAC") != "")
            {
                string path = GetEnvVariable("JAVAC");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
                else if (System.IO.File.Exists("/usr/local/bin/" + path)) { return "/usr/local/bin/" + path; }
                else if (System.IO.File.Exists("/usr/local/bin" + path)) { return "/usr/local/bin" + path; }
            }
            if (GetEnvVariable("javac") != "")
            {
                string path = GetEnvVariable("javac");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
                else if (System.IO.File.Exists("/usr/local/bin/" + path)) { return "/usr/local/bin/" + path; }
                else if (System.IO.File.Exists("/usr/local/bin" + path)) { return "/usr/local/bin" + path; }
            }

            if (GetEnvVariable("JAVA-COMPILER") != "")
            {
                string path = GetEnvVariable("JAVA-COMPILER");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
                else if (System.IO.File.Exists("/usr/local/bin/" + path)) { return "/usr/local/bin/" + path; }
                else if (System.IO.File.Exists("/usr/local/bin" + path)) { return "/usr/local/bin" + path; }
            }

            if (GetEnvVariable("JAVA_COMPILER") != "")
            {
                string path = GetEnvVariable("JAVA_COMPILER");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
                else if (System.IO.File.Exists("/usr/local/bin/" + path)) { return "/usr/local/bin/" + path; }
                else if (System.IO.File.Exists("/usr/local/bin" + path)) { return "/usr/local/bin" + path; }
            }

            if (System.IO.File.Exists("/usr/bin/javac")) { return "/usr/bin/javac"; }
            if (System.IO.File.Exists("/usr/javac")) { return "/usr/javac"; }
            if (System.IO.File.Exists("/usr/java/javac")) { return "/usr/java/javac"; }
            if (System.IO.File.Exists("/usr/local/java/javac")) { return "/usr/local/java/javac"; }
            if (System.IO.File.Exists("/usr/local/bin/java/javac")) { return "/usr/local/bin/java/javac"; }


            if (System.IO.Directory.Exists("/usr/java"))
            {
                foreach (string directory in System.IO.Directory.GetDirectories("/usr/java/"))
                {
                    if (System.IO.File.Exists(directory + "/bin/javac")) { return directory + "/bin/javac"; }
                    if (System.IO.File.Exists(directory + "bin/javac")) { return directory + "bin/javac"; }
                    if (System.IO.File.Exists(directory + "/lib/javac")) { return directory + "/lib/javac"; }
                    if (System.IO.File.Exists(directory + "lib/javac")) { return directory + "lib/javac"; }
                }
            }
            if (System.IO.Directory.Exists("/usr/local/java"))
            {
                foreach (string directory in System.IO.Directory.GetDirectories("/usr/local/java/"))
                {
                    if (System.IO.File.Exists(directory + "/bin/javac")) { return directory + "/bin/javac"; }
                    if (System.IO.File.Exists(directory + "bin/javac")) { return directory + "bin/javac"; }
                    if (System.IO.File.Exists(directory + "/lib/javac")) { return directory + "/lib/javac"; }
                    if (System.IO.File.Exists(directory + "lib/javac")) { return directory + "lib/javac"; }
                }
            }

            string systemDir = System.Environment.SystemDirectory;

            if (CurrentOperatingSystem() == OperatingSystem.Unix ||
                CurrentOperatingSystem() == OperatingSystem.AppleMacOSX)
            {
                if (System.IO.File.Exists(systemDir + "javac")) { return systemDir + "javac"; }
                if (System.IO.File.Exists(systemDir + "/javac")) { return systemDir + "/javac"; }
            }
            else if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows)
            {
                if (System.IO.Directory.Exists(@"C:\Program Files\Java\"))
                {
                    foreach (string directory in System.IO.Directory.GetDirectories(@"C:\Program Files\Java\"))
                    {
                        if (System.IO.File.Exists(directory + "/bin/javac.exe")) { return directory + "/bin/javac.exe"; }
                        if (System.IO.File.Exists(directory + "bin/javac.exe")) { return directory + "bin/javac.exe"; }
                        if (System.IO.File.Exists(directory + "/lib/javac.exe")) { return directory + "/lib/javac.exe"; }
                        if (System.IO.File.Exists(directory + "lib/javac.exe")) { return directory + "lib/javac.exe"; }
                    }
                }
            }

            return "";
        }

        
        static public string GetCCBinary()
        {
            // Try to find a variable that contain the path of CC

            if (GetEnvVariable("CC") != "")
            {
                string path = GetEnvVariable("CC");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }
            if (GetEnvVariable("cc") != "")
            {
                string path = GetEnvVariable("cc");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }

            if (System.IO.File.Exists("/bin/gcc")) { return "/bin/gcc"; }
            if (System.IO.File.Exists("/usr/bin/gcc")) { return "/usr/bin/gcc"; }

            if (System.IO.File.Exists("/bin/clang")) { return "/bin/clang"; }
            if (System.IO.File.Exists("/usr/bin/clang")) { return "/usr/bin/clang"; }

            string systemDir = System.Environment.SystemDirectory;

            if (CurrentOperatingSystem() == OperatingSystem.Unix ||
                CurrentOperatingSystem() == OperatingSystem.AppleMacOSX)
            {
                if (System.IO.File.Exists(systemDir + "gcc")) { return systemDir + "gcc"; }
                if (System.IO.File.Exists(systemDir + "/gcc")) { return systemDir + "/gcc"; }

                if (System.IO.File.Exists(systemDir + "clang")) { return systemDir + "clang"; }
                if (System.IO.File.Exists(systemDir + "/clang")) { return systemDir + "/clang"; }
            }
            else if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows)
            {
                if (System.IO.File.Exists(@"C:\Windows\System32\cl.exe")) { return @"C:\Windows\System32\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Windows\SysWOW64\cl.exe")) { return @"C:\Windows\SysWOW64\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 12.0\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 12.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 11.0\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 11.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 10.0\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 10.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 9.0\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 9.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 8\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 8\VC\bin\cl.exe"; }

                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 8\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 8\VC\bin\cl.exe"; }
            }

            return "";
        }

        static public string GetUnixShell()
        {
            if (GetEnvVariable("SH") != "")
            {
                string path = GetEnvVariable("SH");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
            }

            string sh = _WhereNoRecurse("sh");
            if (System.IO.File.Exists(sh)) { return sh; }
            sh = _WhereNoRecurse("bash");
            if (System.IO.File.Exists(sh)) { return sh; }
            sh = _WhereNoRecurse("ksh");
            if (System.IO.File.Exists(sh)) { return sh; }
            sh = _WhereNoRecurse("zsh");
            if (System.IO.File.Exists(sh)) { return sh; }

            if (System.IO.File.Exists("C:/cygwin/bin/sh.exe")) { return "C:/cygwin/bin/sh.exe"; }
            if (System.IO.File.Exists("C:/cygwin/bin/bash.exe")) { return "C:/cygwin/bin/bash.exe"; }

            return "";
        }

        static public string GetCXXBinary()
        {
            // Try to find a variable that contain the path of CXX

            if (GetEnvVariable("CXX") != "")
            {
                string path = GetEnvVariable("CXX");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }
            if (GetEnvVariable("cxx") != "")
            {
                string path = GetEnvVariable("cxx");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }

            if (System.IO.File.Exists("/bin/g++")) { return "/bin/g++"; }
            if (System.IO.File.Exists("/usr/bin/g++")) { return "/usr/bin/g++"; }

            if (System.IO.File.Exists("/bin/clang++")) { return "/bin/clang++"; }
            if (System.IO.File.Exists("/usr/bin/clang++")) { return "/usr/bin/clang++"; }

            string systemDir = System.Environment.SystemDirectory;

            if (CurrentOperatingSystem() == OperatingSystem.Unix ||
                CurrentOperatingSystem() == OperatingSystem.AppleMacOSX)
            {
                if (System.IO.File.Exists(systemDir + "g++")) { return systemDir + "g++"; }
                if (System.IO.File.Exists(systemDir + "/g++")) { return systemDir + "/g++"; }

                if (System.IO.File.Exists(systemDir + "clang++")) { return systemDir + "clang++"; }
                if (System.IO.File.Exists(systemDir + "/clang++")) { return systemDir + "/clang++"; }
            }
            else if (CurrentOperatingSystem() == OperatingSystem.MicrosoftWindows ||
                     CurrentOperatingSystem() == OperatingSystem.MicrosoftDOS )
            {
                if (System.IO.File.Exists(@"C:\Windows\System32\cl.exe")) { return @"C:\Windows\System32\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Windows\SysWOW64\cl.exe")) { return @"C:\Windows\SysWOW64\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 10.0\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 10.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 9.0\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 9.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 8\VC\bin\cl.exe"))
                { return @"C:\Program Files\Microsoft Visual Studio 8\VC\bin\cl.exe"; }

                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\cl.exe"; }
                if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 8\VC\bin\cl.exe"))
                { return @"C:\Program Files (x86)\Microsoft Visual Studio 8\VC\bin\cl.exe"; }
            }

            return "";
        }
        
        static public string GetMakeBinary()
        {
            // Try to find a variable that contain the path of Make

            if (GetEnvVariable("MAKE") != "")
            {
                string path = GetEnvVariable("MAKE");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }
            if (GetEnvVariable("make") != "")
            {
                string path = GetEnvVariable("make");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }
            if (GetEnvVariable("Make") != "")
            {
                string path = GetEnvVariable("Make");
                if (System.IO.File.Exists(path)) { return path; }
                else if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                else if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
                else if (System.IO.File.Exists("/bin/" + path)) { return "/bin/" + path; }
                else if (System.IO.File.Exists("/bin" + path)) { return "/bin" + path; }
                else if (System.IO.File.Exists("/usr/bin/" + path)) { return "/usr/bin/" + path; }
                else if (System.IO.File.Exists("/usr/bin" + path)) { return "/usr/bin" + path; }
            }

			if (System.IO.File.Exists("/usr/local/bin/gmake")) { return "/usr/local/bin/gmake"; }
			if (System.IO.File.Exists("/usr/bin/gmake")) { return "/usr/bin/gmake"; }
			if (System.IO.File.Exists("/bin/gmake")) { return "/bin/gmake"; }

            if (System.IO.File.Exists("/bin/make")) { return "/bin/make"; }
            if (System.IO.File.Exists("/usr/bin/make")) { return "/usr/bin/make"; }

            if (System.IO.File.Exists("/bin/Make")) { return "/bin/Make"; }
            if (System.IO.File.Exists("/usr/bin/Make")) { return "/usr/bin/Make"; }

            string systemDir = System.Environment.SystemDirectory;

            if (System.IO.File.Exists(systemDir + "make")) { return systemDir + "make"; }
            if (System.IO.File.Exists(systemDir + "Make")) { return systemDir + "Make"; }
            if (System.IO.File.Exists(systemDir + "/make")) { return systemDir + "/make"; }
            if (System.IO.File.Exists(systemDir + "/Make")) { return systemDir + "/Make"; }
            if (System.IO.File.Exists(systemDir + "/make.exe")) { return systemDir + "/make.exe"; }
            if (System.IO.File.Exists(systemDir + "/Make.exe")) { return systemDir + "/Make.exe"; }

            if (System.IO.File.Exists(@"C:\Windows\System32\make.exe")) { return @"C:\Windows\System32\make.exe"; }
            if (System.IO.File.Exists(@"C:\Windows\SysWOW64\make.exe")) { return @"C:\Windows\SysWOW64\make.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\System32\make.exe")) { return @"C:\WINDOWS\System32\make.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\SysWOW64\make.exe")) { return @"C:\WINDOWS\SysWOW64\make.exe"; }

            if (System.IO.File.Exists(@"C:\Windows\System32\Make.exe")) { return @"C:\Windows\System32\Make.exe"; }
            if (System.IO.File.Exists(@"C:\Windows\SysWOW64\Make.exe")) { return @"C:\Windows\SysWOW64\Make.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\System32\Make.exe")) { return @"C:\WINDOWS\System32\Make.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\SysWOW64\Make.exe")) { return @"C:\WINDOWS\SysWOW64\Make.exe"; }

            // No Make executable fount let's try with NMake

            if (GetEnvVariable("NMAKE") != "")
            {
                string path = GetEnvVariable("NMAKE");
                if (System.IO.File.Exists(path)) { return path; }
                if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
            }

            if (GetEnvVariable("nmake") != "")
            {
                string path = GetEnvVariable("nmake");
                if (System.IO.File.Exists(path)) { return path; }
                if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
            }

            if (GetEnvVariable("NMake") != "")
            {
                string path = GetEnvVariable("NMake");
                if (System.IO.File.Exists(path)) { return path; }
                if (System.IO.File.Exists(path + ".exe")) { return path + ".exe"; }
                if (System.IO.File.Exists(path + ".EXE")) { return path + ".EXE"; }
            }

            if (System.IO.File.Exists(@"C:\Windows\System32\nmake.exe")) { return @"C:\Windows\System32\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Windows\SysWOW64\nmake.exe")) { return @"C:\Windows\SysWOW64\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\System32\nmake.exe")) { return @"C:\WINDOWS\System32\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\SysWOW64\nmake.exe")) { return @"C:\WINDOWS\SysWOW64\nmake.exe"; }

            if (System.IO.File.Exists(@"C:\Windows\System32\NMAKE.exe")) { return @"C:\Windows\System32\NMAKE.exe"; }
            if (System.IO.File.Exists(@"C:\Windows\SysWOW64\NMAKE.exe")) { return @"C:\Windows\SysWOW64\NMAKE.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\System32\NMAKE.exe")) { return @"C:\WINDOWS\System32\NMAKE.exe"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\SysWOW64\NMAKE.exe")) { return @"C:\WINDOWS\SysWOW64\NMAKE.exe"; }

            if (System.IO.File.Exists(@"C:\Windows\System32\NMAKE.EXE")) { return @"C:\Windows\System32\NMAKE.EXE"; }
            if (System.IO.File.Exists(@"C:\Windows\SysWOW64\NMAKE.EXE")) { return @"C:\Windows\SysWOW64\NMAKE.EXE"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\System32\NMAKE.EXE")) { return @"C:\WINDOWS\System32\NMAKE.EXE"; }
            if (System.IO.File.Exists(@"C:\WINDOWS\SysWOW64\NMAKE.EXE")) { return @"C:\WINDOWS\SysWOW64\NMAKE.EXE"; }

            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 11.0\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 11.0\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 11.0\bin\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 11.0\bin\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 11.0\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 11.0\VC\bin\nmake.exe"; }

            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 10.0\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 10.0\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 10.0\bin\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 10.0\bin\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 10.0\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 10.0\VC\bin\nmake.exe"; }

            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 9.0\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 9.0\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 9.0\bin\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 9.0\bin\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 9.0\VC\bin\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 9.0\VC\bin\nmake.exe"; }

            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 8\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 8\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 8\bin\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 8\bin\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files\Microsoft Visual Studio 8\VC\bin\nmake.exe"))
            { return @"C:\Program Files\Microsoft Visual Studio 8\VC\bin\nmake.exe"; }

            if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\nmake.exe"))
            { return @"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\nmake.exe"))
            { return @"C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\nmake.exe"; }
            if (System.IO.File.Exists(@"C:\Program Files (x86)\Microsoft Visual Studio 8\VC\bin\nmake.exe"))
            { return @"C:\Program Files (x86)\Microsoft Visual Studio 8\VC\bin\nmake.exe"; }
            return "";
        }

        static public string GetLibFact()
        {
            try
            {
                foreach (System.Reflection.Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    if (assembly.GetName().Name.ToLower() == "fact")
                    {
                        string lib = assembly.Location;
                        if (System.IO.File.Exists(lib))
                        {
                            return lib;
                        }
                    }  
                }
            }
            catch
            { }
            return _WhereNoRecurse("Fact.dll");
        }

        static public string GetFactDirectory()
        {
            try
            {
                foreach (System.Reflection.Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    if (assembly.GetName().Name.ToLower() == "factexe" ||
                        assembly.GetName().Name.ToLower() == "factexe.exe")
                    {
                        string executable = assembly.Location;
                        if (System.IO.File.Exists(executable))
                        {
                            string directory = System.IO.Path.GetDirectoryName(executable);
                            if (System.IO.File.Exists(directory + "/Fact.dll") &&
                                System.IO.File.Exists(directory + "/FactExe.exe") &&
                                System.IO.Directory.Exists(directory + "/System"))
                            {
                                return directory;
                            }
                        }
                    }
                }
            }
            catch { }
            try
            {
                string whereis = _WhereNoRecurse("FactExe.exe");
                if (whereis == "" || !System.IO.File.Exists(whereis)) { whereis = _WhereNoRecurse("FactExe"); }
                if (whereis == "" || !System.IO.File.Exists(whereis)) { whereis = _WhereNoRecurse("factexe"); }
                if (whereis == "" || !System.IO.File.Exists(whereis)) { whereis = _WhereNoRecurse("fact.exe"); }
                if (whereis == "" || !System.IO.File.Exists(whereis)) { whereis = _WhereNoRecurse("Fact.exe"); }
                if (whereis == "" || !System.IO.File.Exists(whereis)) { whereis = GetLibFact(); }
                if (whereis != "" && System.IO.File.Exists(whereis))
                {
                    string directory = System.IO.Path.GetDirectoryName(whereis);
                    if (System.IO.File.Exists(directory + "/Fact.dll") &&
                        System.IO.File.Exists(directory + "/FactExe.exe") &&
                        System.IO.Directory.Exists(directory + "/System"))
                    {
                        return directory;
                    }
                }
            }
            catch { }

            return "";
            
        }

        static public OperatingSystem CurrentOperatingSystem()
        {
			// For legacy reason mono2.2 will return 4 for macOSX
			// so we have to do a little more work
			{
				if ((System.Environment.OSVersion.Platform & PlatformID.MacOSX) ==
				   PlatformID.MacOSX)
					return OperatingSystem.AppleMacOSX;
				if (System.Environment.OSVersion.Platform == PlatformID.Unix)
				{
					// Try to look for some mac specific files
					bool mach_kernel = (System.IO.Directory.Exists ("/mach_kernel") ||
					                    System.IO.File.Exists ("/mach_kernel"));
					bool mac_libc = (System.IO.File.Exists ("/usr/lib/libc.dylib"));
					if (mach_kernel || mac_libc)
						return OperatingSystem.AppleMacOSX;
				}
			}

            switch (System.Environment.OSVersion.Platform)
            {
				case PlatformID.Unix:
                    return OperatingSystem.Unix;
                case PlatformID.Win32S:
                    return OperatingSystem.MicrosoftDOS;
                case PlatformID.Win32Windows:
                case PlatformID.Win32NT:
                    return OperatingSystem.MicrosoftWindows;
                case PlatformID.WinCE:
                    return OperatingSystem.MicrosoftWindowsCE;
                case PlatformID.Xbox:
                    return OperatingSystem.MicrosoftXBox360;
				case PlatformID.MacOSX:
                    return OperatingSystem.AppleMacOSX;
            }

            return OperatingSystem.Unknow;
        }

        static public OperatingSystemVersion CurrentOperatingSystemVersion()
        {
            switch (CurrentOperatingSystem())
            {
                case OperatingSystem.SonyPS4: return OperatingSystemVersion.FreeBSDSonyPS4; break;
                case OperatingSystem.MicrosoftXBox360: return OperatingSystemVersion.MicrosoftXBox360; break;
                case OperatingSystem.MicrosoftXBoxOne: return OperatingSystemVersion.MicrosoftXBoxOne; break;
                case OperatingSystem.MicrosoftWindowsCE: return OperatingSystemVersion.MicrosoftWindowsCE6; break;
                case OperatingSystem.AppleMacOSX: return OperatingSystemVersion.MacOSX; break;
                case OperatingSystem.MicrosoftWindows:
                    {
                        Version OsVersion = Environment.OSVersion.Version;
                        if (OsVersion.Major == 5 && OsVersion.Minor == 1) { return OperatingSystemVersion.MicrosoftWindowsXP; }
                        if (OsVersion.Major == 6 && OsVersion.Minor == 0) { return OperatingSystemVersion.MicrosoftWindowsVista; }
                        if (OsVersion.Major == 6 && OsVersion.Minor == 1) { return OperatingSystemVersion.MicrosoftWindows7; }
                        if (OsVersion.Major == 6 && OsVersion.Minor == 2) { return OperatingSystemVersion.MicrosoftWindows8; }
                        if (OsVersion.Major == 6 && OsVersion.Minor == 3) { return OperatingSystemVersion.MicrosoftWindows8_1; }
                        return OperatingSystemVersion.MicrosoftWindows;
                    }
                case OperatingSystem.Unix:
                    {
                        string uname = _WhereNoRecurse("uname");
                        if (!System.IO.File.Exists(uname)) { return OperatingSystemVersion.Unspecified; }
                        string output = Runtime.Process.Execute(uname, "-s").StdOut.ToLower().Trim();
                        if (output == "linux")
                        {
                            string version = Runtime.Process.Execute(uname, "-v").StdOut.ToLower().Trim();
                            if (version.Contains("ubuntu")) { return OperatingSystemVersion.LinuxUbuntu; }
                            return OperatingSystemVersion.Linux;
                        }
                        else if (output == "freebsd")
                        {
                            return OperatingSystemVersion.FreeBSD;
                        }
                        return OperatingSystemVersion.Unspecified;
                        break;
                    }
                default: return OperatingSystemVersion.Unspecified;


            }
        }
    }
}

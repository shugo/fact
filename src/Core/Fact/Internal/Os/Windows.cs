/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;

namespace Fact.Internal.Os
{
	internal unsafe class Windows : Generic.GenericIn
	{
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static void* VirtualAlloc(void* lpAddress, int Size, int flAllocationType, int flProtect);
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool VirtualProtect(void* lpAddress, int Size, int flnewProtect, int* lpflProtect);
		[System.Runtime.InteropServices.DllImport("kernel32.dll")]
		extern static void* LoadLibrary(string Library);
		[System.Runtime.InteropServices.DllImport("kernel32.dll")]
		extern static void* GetProcAddress(void* Module, string MethodName);
		[System.Runtime.InteropServices.DllImport("kernel32.dll")]
		extern static bool FreeLibrary(void* Module);

        static int tok32Protection(Generic.Protection Protection)
        {
            
            const int PAGE_EXECUTE = 0x10;
            const int PAGE_EXECUTE_READ = 0x20;
            const int PAGE_EXECUTE_READWRITE = 0x40;
            const int PAGE_EXECUTE_WRITECOPY = 0x40;
            const int PAGE_NOACCESS = 0x01;
            const int PAGE_READONLY = 0x02;
            const int PAGE_READWRITE = 0x04;
            const int PAGE_WRITECOPY = 0x08;

            int k32protection = 0;
            switch(Protection)
            {
                case Generic.Protection.Read | Generic.Protection.Write | Generic.Protection.Execute: k32protection = PAGE_EXECUTE_READWRITE; break;
                case Generic.Protection.Read | Generic.Protection.Execute: k32protection = PAGE_EXECUTE_READ; break;
                case Generic.Protection.Execute: k32protection = PAGE_EXECUTE; break;
                case Generic.Protection.Read: k32protection =PAGE_READONLY; break;
                case Generic.Protection.Read | Generic.Protection.Write: k32protection = PAGE_READWRITE; break;
                case Generic.Protection.None: k32protection = PAGE_READONLY; break;
                default: throw new Exception("Unsupported allocation flags"); break;
            }

            return k32protection;
        }

        public override IntPtr MapMemory(int Size, Generic.Protection Protection)
        {
            const int MEM_COMMIT = 0x00001000;
            const int MEM_RESERVE = 0x00002000;


            void* allocated = VirtualAlloc((void*)0, Size, MEM_COMMIT | MEM_RESERVE, tok32Protection(Protection));
            return new IntPtr(allocated);
        }

		public override IntPtr OpenLibrary(string Library)
		{
			return new IntPtr(LoadLibrary(Library));
		}

		public override IntPtr GetMethod (IntPtr LibraryHandle, string Method)
		{
			return new IntPtr(GetProcAddress(LibraryHandle.ToPointer(), Method));
		}

		public override void CloseLibrary (IntPtr LibraryHandle)
		{
			FreeLibrary(LibraryHandle.ToPointer());
		}

        public override bool ChangeMemoryProtection(IntPtr Memory, int Size, Generic.Protection Protection)
        {
            int ignored = 0;
            int* pignored = &ignored;
            return VirtualProtect(Memory.ToPointer(), Size, tok32Protection(Protection), pignored);
        }

		public override long GetClockTickPerSecond ()
		{
			return 0;
		}
	}
}


/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;

namespace Fact.Internal.Os
{
	public static class Generic
	{
        public class Device { }
        public enum Protection
        {
            None = 0x00,
            Read = 0x01,
            Write = 0x02,
            Execute = 0x04
        };

        public enum MountOption
        {
            None = 0x00,
            Read = 0x01,
            Write = 0x02,
            Synchronous = 0x4,
        };

        public delegate void SignalHookFunction(int value);

		internal class GenericIn
		{
			public virtual long GetClockTickPerSecond() { return 0; }
            public virtual void HookSignal(int Signal, SignalHookFunction Hook) { return; }
            public virtual IntPtr MapMemory(int Size, Protection Protection) { return new IntPtr(0); }
            public virtual bool UnmapMemory(IntPtr Memory, int Size) { return false; }
            public virtual bool ChangeMemoryProtection(IntPtr Memory, int Size, Protection Protection) { return false; }
            public virtual Device IoctlOpenDevice(string DeviceName) { return null; }
            public virtual int IoctlSendToDevice(Device Device, int RequestCode, IntPtr Data) { return 0; }
            public virtual IntPtr OpenLibrary(string Library) { return new IntPtr(0); }
            public virtual IntPtr GetMethod(IntPtr LibraryHandle, string Method) { return new IntPtr(0); }
            public virtual void CloseLibrary(IntPtr LibraryHandle) { return; }
            public virtual bool Mount(string Volume, string MountPoint, string FileSystemType, MountOption MountOptions, IntPtr data) { return false; }
            public unsafe bool Mount(string Volume, string MountPoint, string FileSystemType, MountOption MountOptions, string data)
            {
                byte* ptr = Fact.Reflection.Tools.ToCStr(data);
                bool result = Mount(Volume, MountPoint, FileSystemType, MountOptions, new IntPtr(ptr));
                System.Runtime.InteropServices.Marshal.FreeHGlobal(new IntPtr(ptr));
                return result;
            }

		}

		static GenericIn _generic;
		static Generic()
		{
			if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
			{ _generic = new Mac(); }
			if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
			{ _generic = new Linux(); }
			if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
			{ _generic = new Windows(); }
        }

		static public long GetClockTickPerSecond()
		{
            if (_generic == null) { return 1; }
			return _generic.GetClockTickPerSecond ();
		}

        static public void HookSignal(int Signal, SignalHookFunction Hook) 
        {
            if (_generic == null) { return; }
            _generic.HookSignal(Signal, Hook);
        }
        
        static public IntPtr MapMemory(int Size, Protection Protection)
        {
            if (_generic == null) { return new IntPtr(0); }
            return _generic.MapMemory(Size, Protection);
        }
        static public bool UnmapMemory(IntPtr Memory, int Size)
        {
            if (_generic == null) { return false; }
            return false;
        }
        static public bool ChangeMemoryProtection(IntPtr Memory, int Size, Protection Protection)
        {
            return _generic.ChangeMemoryProtection(Memory, Size, Protection);
        }
        static public Device IoctlOpenDevice(string DeviceName)
        {
            return _generic.IoctlOpenDevice(DeviceName);
        }
        static public int IoctlSendToDevice(Device Device, int RequestCode, IntPtr Data)
        {
            return _generic.IoctlSendToDevice(Device, RequestCode, Data);
        }
		static public IntPtr OpenLibrary(string Library)
		{
			return _generic.OpenLibrary(Library);
		}
		static public void CloseLibrary(IntPtr Handle)
		{
			_generic.CloseLibrary(Handle);
		}
		static public IntPtr GetMethod(IntPtr LibraryHandle, string Method)
		{
			return _generic.GetMethod(LibraryHandle, Method);
		}
	}
}


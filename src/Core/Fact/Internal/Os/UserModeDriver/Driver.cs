﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Os.UserModeDriver
{
    unsafe public static class Driver
    {
        enum FACT_IOCTL
        {
            IOCTL_PROTECT_PROCESS = 0x5601,
            IOCTL_UNPROTECT_PROCESS = 0x5602,
            IOCTL_REFUSE_PROCESS_PROTECTION = 0x5603,
            IOCTL_DECLARE_CHILD = 0x5604,
        }

        static Generic.Device _Device;
        static Driver()
        {
            if (Information.CurrentOperatingSystem() == Information.OperatingSystem.MicrosoftWindows ||
                Information.CurrentOperatingSystem() == Information.OperatingSystem.MicrosoftWindowsCE)
            {
                _Device = Generic.IoctlOpenDevice("//fact_kernel_module"); 
            }
            else if (Information.CurrentOperatingSystem() == Information.OperatingSystem.AppleMacOSX ||
                Information.CurrentOperatingSystem() == Information.OperatingSystem.Unix)
            {
                _Device = Generic.IoctlOpenDevice("/dev/fact_kernel_module");
            }
            else
            {
                // We don't support Fact Kernel Driver on other platform
                _Device = null;
            }
        }
        // Kernel Mode Comunication
        public static bool ProtectProcess(int Pid) { if (_Device == null) { return false; } return Generic.IoctlSendToDevice(_Device, (int)FACT_IOCTL.IOCTL_PROTECT_PROCESS, new IntPtr(Pid)) == 1; }
        public static bool UnprotectProcess(int Pid) { if (_Device == null) { return false; } return Generic.IoctlSendToDevice(_Device, (int)FACT_IOCTL.IOCTL_UNPROTECT_PROCESS, new IntPtr(Pid)) == 1; }
        public static bool RefuseProcessProtection(int Pid) { if (_Device == null) { return false; } return Generic.IoctlSendToDevice(_Device, (int)FACT_IOCTL.IOCTL_REFUSE_PROCESS_PROTECTION, new IntPtr(Pid)) == 1; }
        public static bool DeclareChildProcess(int Pid) { if (_Device == null) { return false; } return Generic.IoctlSendToDevice(_Device, (int)FACT_IOCTL.IOCTL_DECLARE_CHILD, new IntPtr(Pid)) == 1; }
    }
}

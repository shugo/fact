﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;

namespace Fact.Internal.Os
{
	internal unsafe class Mac : Generic.GenericIn
	{
		SupportedLibDl libdl = null;
		SupportedLibC libc = null;
		public Mac()
		{
			if (System.IO.File.Exists("/usr/lib/libdl.dylib")) { libdl = new MacOsXLibDl(); }
			if (System.IO.File.Exists("/usr/lib/libc.dylib")) { libdl = new MacOsXLibDl(); }
		}

		abstract unsafe class SupportedLibDl
		{
			public virtual void* call_dlopen(string FileName, int Flags) { return (void*)0; }
			public virtual void* call_dlsym(void* Handle, string Symbol) { return (void*)0; }
			public virtual int   call_dlclose(void* Handle) { return 0; }
		}

		abstract unsafe class SupportedLibC
		{
		}

		class MacOsXLibDl : SupportedLibDl
		{
			[System.Runtime.InteropServices.DllImport("/usr/lib/libdl.dylib")]
			public static extern void* dlopen(string FileName, int Flags);

			[System.Runtime.InteropServices.DllImport("/usr/lib/libdl.dylib")]
			public static extern void* dlsym(void* Handle, string Symbol);

			[System.Runtime.InteropServices.DllImport("/usr/lib/libdl.dylib")]
			public static extern int dlclose(void* Handle);

			public override void* call_dlsym (void* Handle, string Symbol) { return dlsym(Handle, Symbol); }
			public override void* call_dlopen (string FileName, int Flags) { return dlopen(FileName, Flags); }
			public override int call_dlclose (void* Handle) { return dlclose(Handle); }
		}

		class MacOsXLibC : SupportedLibC
		{

		}

		public override IntPtr OpenLibrary(string Library)
		{
			if (libdl == null) { return new IntPtr(0); }
			if (!System.IO.File.Exists(Library)) { return new IntPtr(0); }
			return new IntPtr(libdl.call_dlopen (Library, 0));
		}

		public override void CloseLibrary(IntPtr LibraryHandle)
		{
			if (libdl == null) { return; }
			if (LibraryHandle.ToPointer() == (void*)0) { return; }
			libdl.call_dlclose(LibraryHandle.ToPointer());
		}

		public override IntPtr GetMethod(IntPtr LibraryHandle, string Method)
		{
			if (libdl == null) { return new IntPtr(0); }
			if (LibraryHandle.ToPointer() == (void*)0) { return new IntPtr(0); }
			if (Method.Trim() == "")  { return new IntPtr(0); }
			return new IntPtr(libdl.call_dlsym (LibraryHandle.ToPointer(), Method));
		}
	}
}


/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Runtime.InteropServices;

namespace Fact.Internal.Os
{
    internal unsafe class Linux : Generic.GenericIn
    {
        abstract unsafe class SupportedLibDl
        {
            public virtual void* call_dlopen(string FileName, int Flags) { return (void*)0; }
            public virtual void* call_dlsym(void* Handle, string Symbol) { return (void*)0; }
            public virtual int call_dlclose(void* Handle) { return 0; }
        }

        abstract unsafe class SupportedLibC
        {
            public virtual long call_sysconf(int value) { return 0; }
            public virtual void call_signal(int signal, void* ptr) { return; }
            public virtual void* call_mmap(void* addr, ulong size, int prot, int flags, int fd, long offset) { return (void*)0; }
            public virtual int call_mprotect(void* addr, ulong len, int prot) { return 0; }
            public virtual int call_mount(string special_file, string dir, string fstype, uint options, void* data) { return -1; }
            public virtual int call_umount2(string target, int flags) { return 0; }
        }

        unsafe class DynamicLibC : SupportedLibC
        {
            delegate void signal(int signal, void* ptr);
            delegate long sysconf(int value);
            delegate void* mmap(void* addr, ulong size, int prot, int flags, int fd, long offset);
            delegate int mprotect(void* addr, ulong len, int prot);
            delegate int mount(string special_file, string dir, string fstype, uint options, void* data);
            delegate int umount2(string target, int flags);


            signal d_signal;
            sysconf d_sysconf;
            mmap d_mmap;
            mprotect d_mprotect;
            mount d_mount;
            umount2 d_umount2;


            public DynamicLibC(SupportedLibDl libdl)
            {
                string libc = Internal.Information.GetLibC();
                if (System.IO.File.Exists(libc))
                {
                    void* libcptr = libdl.call_dlopen(libc, 1);
                    void* signal_ptr = libdl.call_dlsym(libcptr, "signal");
                    d_signal = (signal)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(new IntPtr(signal_ptr), typeof(signal));
                    void* sysconf_ptr = libdl.call_dlsym(libcptr, "sysconf");
                    d_sysconf = (sysconf)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(new IntPtr(sysconf_ptr), typeof(sysconf));
                    void* mmap_ptr = libdl.call_dlsym(libcptr, "mmap");
                    d_mmap = (mmap)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(new IntPtr(mmap_ptr), typeof(mmap));
                    void* mprotect_ptr = libdl.call_dlsym(libcptr, "mprotect");
                    d_mprotect = (mprotect)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(new IntPtr(mprotect_ptr), typeof(mprotect));
                    void* mount_ptr = libdl.call_dlsym(libcptr, "mount");
                    d_mount = (mount)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(new IntPtr(mount_ptr), typeof(mount));
                    void* umount2_ptr = libdl.call_dlsym(libcptr, "umount2");
                    d_umount2 = (umount2)System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(new IntPtr(umount2_ptr), typeof(umount2));
                }
                else { throw new Exception("Libc not found"); }
            }

            public override long call_sysconf(int value) { return d_sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { d_signal(signalnum, ptr); }
            public override void* call_mmap(void* addr, ulong size, int prot, int flags, int fd, long offset)
            { return d_mmap(addr, size, prot, flags, fd, offset); }
            public override int call_mprotect(void* addr, ulong len, int prot)
            { return d_mprotect(addr, len, prot); }
            public override int call_mount(string special_file, string dir, string fstype, uint options, void* data)
            { return d_mount(special_file, dir, fstype, options, data); }
            public override int call_umount2(string target, int flags)
            { return d_umount2(target, flags); }
        }

        unsafe class GenericLibC : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
            extern static long sysconf(int value);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
            extern static void* mmap(void* addr, ulong size, int prot, int flags, int fd, long offset);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
            extern static int mprotect(void* addr, ulong len, int prot);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
            extern static int mount(string special_file, string dir, string fstype, uint options, void* data);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
            extern static int umount2(string target, int flags);

            public override long call_sysconf (int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
            public override void* call_mmap(void* addr, ulong size, int prot, int flags, int fd, long offset)
            { return mmap(addr, size, prot, flags, fd, offset); }
            public override int call_mprotect(void* addr, ulong len, int prot)
            { return mprotect(addr, len, prot); }
            public override int call_mount(string special_file, string dir, string fstype, uint options, void* data)
            { return mount(special_file, dir, fstype, options, data); }
            public override int call_umount2(string target, int flags)
            { return umount2(target, flags); }
        }

        unsafe class LibC2_18 : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
            extern static long sysconf(int value);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
            extern static void* mmap(void* addr, ulong size, int prot, int flags, int fd, long offset);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
            extern static int mprotect(void* addr, ulong len, int prot);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
            extern static int mount(string special_file, string dir, string fstype, uint options, void* data);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
            extern static int umount2(string target, int flags);

            public override long call_sysconf (int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
            public override void* call_mmap(void* addr, ulong size, int prot, int flags, int fd, long offset)
            { return mmap(addr, size, prot, flags, fd, offset); }
            public override int call_mprotect(void* addr, ulong len, int prot)
            { return mprotect(addr, len, prot); }
            public override int call_mount(string special_file, string dir, string fstype, uint options, void* data)
            { return mount(special_file, dir, fstype, options, data); }
            public override int call_umount2(string target, int flags)
            { return umount2(target, flags); }
        }

        unsafe class LibC6 : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static long sysconf(int value);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static void* mmap(void* addr, ulong size, int prot, int flags, int fd, long offset);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static int mprotect(void* addr, ulong len, int prot);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static int mount(string special_file, string dir, string fstype, uint options, void* data);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static int umount2(string target, int flags);

            public override long call_sysconf(int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
            public override void* call_mmap(void* addr, ulong size, int prot, int flags, int fd, long offset)
            { return mmap(addr, size, prot, flags, fd, offset); }
            public override int call_mprotect(void* addr, ulong len, int prot)
            { return mprotect(addr, len, prot); }
            public override int call_mount(string special_file, string dir, string fstype, uint options, void* data)
            { return mount(special_file, dir, fstype, options, data); }
            public override int call_umount2(string target, int flags)
            { return umount2(target, flags); }
        }

        unsafe class RawLibC6 : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static long sysconf(int value);
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static void* mmap(void* addr, ulong size, int prot, int flags, int fd, long offset);
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static int mprotect(void* addr, ulong len, int prot);
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static int mount(string special_file, string dir, string fstype, uint options, void* data);
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static int umount2(string target, int flags);

            public override long call_sysconf(int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
            public override void* call_mmap(void* addr, ulong size, int prot, int flags, int fd, long offset)
            { return mmap(addr, size, prot, flags, fd, offset); }
            public override int call_mprotect(void* addr, ulong len, int prot)
            { return mprotect(addr, len, prot); }
            public override int call_mount(string special_file, string dir, string fstype, uint options, void* data)
            { return mount(special_file, dir, fstype, options, data); }
            public override int call_umount2(string target, int flags)
            { return umount2(target, flags); }
        }

        unsafe class GNULibC6_x64 : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static long sysconf(int value);
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static void* mmap(void* addr, ulong size, int prot, int flags, int fd, long offset);
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static int mprotect(void* addr, ulong len, int prot);
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static int mount(string special_file, string dir, string fstype, uint options, void* data);
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static int umount2(string target, int flags);

            public override long call_sysconf(int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
            public override void* call_mmap(void* addr, ulong size, int prot, int flags, int fd, long offset)
            { return mmap(addr, size, prot, flags, fd, offset); }
            public override int call_mprotect(void* addr, ulong len, int prot)
            { return mprotect(addr, len, prot); }
            public override int call_mount(string special_file, string dir, string fstype, uint options, void* data)
            { return mount(special_file, dir, fstype, options, data); }
            public override int call_umount2(string target, int flags)
            { return umount2(target, flags); }
        }


        static SupportedLibC libc = null;
        static SupportedLibDl libdl = null;


        class GNULibDL : SupportedLibDl
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern void* dlopen(string FileName, int Flags);

            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern void* dlsym(void* Handle, string Symbol);

            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern int dlclose(void* Handle);

            public override void* call_dlsym(void* Handle, string Symbol) { return dlsym(Handle, Symbol); }
            public override void* call_dlopen(string FileName, int Flags) { return dlopen(FileName, Flags); }
            public override int call_dlclose(void* Handle) { return dlclose(Handle); }
        }

        class GNULibDL2 : SupportedLibDl
        {
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libdl.so.2")]
            public static extern void* dlopen(string FileName, int Flags);

            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libdl.so.2")]
            public static extern void* dlsym(void* Handle, string Symbol);

            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libdl.so.2")]
            public static extern int dlclose(void* Handle);

            public override void* call_dlsym(void* Handle, string Symbol) { return dlsym(Handle, Symbol); }
            public override void* call_dlopen(string FileName, int Flags) { return dlopen(FileName, Flags); }
            public override int call_dlclose(void* Handle) { return dlclose(Handle); }
        }

        class GenericLibDL : SupportedLibDl
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/libdl.so")]
            public static extern void* dlopen(string FileName, int Flags);

            [System.Runtime.InteropServices.DllImport("/usr/lib/libdl.so")]
            public static extern void* dlsym(void* Handle, string Symbol);

            [System.Runtime.InteropServices.DllImport("/usr/lib/libdl.so")]
            public static extern int dlclose(void* Handle);

            public override void* call_dlsym(void* Handle, string Symbol) { return dlsym(Handle, Symbol); }
            public override void* call_dlopen(string FileName, int Flags) { return dlopen(FileName, Flags); }
            public override int call_dlclose(void* Handle) { return dlclose(Handle); }
        }
        public Linux ()
        {
            
            if (System.IO.File.Exists("/lib/x86_64-linux-gnu/libc.so.6")) { libc = new GNULibC6_x64(); }
            else if (System.IO.File.Exists ("/usr/lib/libc.so.6")) { libc = new LibC6(); }
            else if (System.IO.File.Exists("/lib/libc.so.6")) { libc = new RawLibC6(); }
            else if (System.IO.File.Exists("/usr/lib/libc-2.18.so")) { libc = new LibC2_18(); }
            else if (System.IO.File.Exists ("/usr/lib/libc.so")) { libc = new GenericLibC (); }

            if (System.IO.File.Exists("/lib/x86_64-linux-gnu/libdl.so.2")) { libdl = new GNULibDL2(); }
            else if (System.IO.File.Exists("/usr/lib/x86_64-linux-gnu/libdl.so")) { libdl = new GNULibDL(); }
            else if (System.IO.File.Exists("/usr/lib/libdl.so")) { libdl = new GenericLibDL(); }

            try
            {
                if (libc == null && libdl != null)
                {
                    libc = new DynamicLibC(libdl);
                }
            }
            catch { libc = null; }

        }

        public override long GetClockTickPerSecond()
        {
            if (libc == null) { return 1; }
            return libc.call_sysconf (2); /* 2 is _SC_CLK_TCK */
        }

        public override void HookSignal(int Signal, Generic.SignalHookFunction Hook)
        {
            if (libc == null) { return; }
            libc.call_signal(Signal, System.Runtime.InteropServices.Marshal.GetFunctionPointerForDelegate((Generic.SignalHookFunction)Hook).ToPointer());
        }

        static int ToLinuxProtection(Generic.Protection Protection)
        {
            const int READ = 0x1;
            const int WRITE = 0x2;
            const int EXEC = 0x4;
            const int NOACCESS = 0x0;

            int flag = 0;
            switch(Protection)
            {
                case Generic.Protection.Read | Generic.Protection.Write | Generic.Protection.Execute: flag = READ | WRITE | EXEC; break;
                case Generic.Protection.Read | Generic.Protection.Execute: flag = READ | EXEC; break;
                case Generic.Protection.Execute: flag = EXEC; break;
                case Generic.Protection.Read: flag = READ; break;
                case Generic.Protection.Read | Generic.Protection.Write: flag = READ | WRITE; break;
                case Generic.Protection.None: flag = NOACCESS; break;
                default: throw new Exception("Unsupported allocation flags"); break;
            }

            return flag;
        }

        static uint ToLinuxMountOption(Generic.MountOption Option)
        {
            unchecked
            {
                const uint READONLY = 1;
                const uint SYNCHRONOUS = 16;
                const uint MGC_VAL = 0xC0ED0000;


                uint flag = 0;
                switch (Option)
                {
                    case Generic.MountOption.Read: flag = READONLY; break;
                    case Generic.MountOption.Read | Generic.MountOption.Synchronous: flag = READONLY; break;
                    case Generic.MountOption.Read | Generic.MountOption.Write: break;
                    case Generic.MountOption.Read | Generic.MountOption.Write | Generic.MountOption.Synchronous: flag = SYNCHRONOUS; break;
                    default: throw new Exception("Unsupported mount flags"); break;
                }
                flag = MGC_VAL | flag;
                return flag;
            }
        }

        public override IntPtr MapMemory(int Size, Generic.Protection Protection)
        {
            if (libc == null) { return new IntPtr(0); }
            return new IntPtr(libc.call_mmap((void*)0, (ulong)Size, ToLinuxProtection(Protection), 0x22 /*MAP_ANONYMOUS|MAP_PRIVATE*/, -1, 0));
        }

        public override bool ChangeMemoryProtection(IntPtr Memory, int Size, Generic.Protection Protection)
        {
            if (libc == null) { return false; }
            return libc.call_mprotect(Memory.ToPointer(), (ulong)Size, ToLinuxProtection(Protection)) == 0;
        }

        public override IntPtr OpenLibrary(string Library)
        {
            if (libdl == null) { return new IntPtr(0); }
            if (!System.IO.File.Exists(Library)) { return new IntPtr(0); }
            return new IntPtr(libdl.call_dlopen(Library, 1 /* RTDL_LAZY */));
        }

        public override void CloseLibrary(IntPtr LibraryHandle)
        {
            if (libdl == null) { return; }
            if (LibraryHandle.ToPointer() == (void*)0) { return; }
            libdl.call_dlclose(LibraryHandle.ToPointer());
        }

        public override IntPtr GetMethod(IntPtr LibraryHandle, string Method)
        {
            if (libdl == null) { return new IntPtr(0); }
            if (LibraryHandle.ToPointer() == (void*)0) { return new IntPtr(0); }
            if (Method.Trim() == "") { return new IntPtr(0); }
            return new IntPtr(libdl.call_dlsym(LibraryHandle.ToPointer(), Method));
        }

        public override bool Mount(string Volume, string MountPoint, string FileSystemType, Generic.MountOption MountOptions, IntPtr data)
        {
            if (libc == null) { return false; }
            int result = libc.call_mount(Volume, MountPoint, FileSystemType, ToLinuxMountOption(MountOptions), data.ToPointer());
            return result == 0;
        }
    }
}


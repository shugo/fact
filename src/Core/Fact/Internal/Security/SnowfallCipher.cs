﻿/*                                                                              *
 * Copyright © 2014, Raphaël Boissel                                            * 
 * Permission is hereby granted, free of charge, to any person obtaining        *
 * a copy of this software and associated documentation files, to deal in       *
 * the Snowfall method without restriction, including without limitation the    *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,         *
 * and/or sell copies of the Snowfall method, and to permit persons to whom the *
 * Snowfall method is furnished to do so, subject to the following conditions:  *
 *                                                                              *
 * - The above copyright notice and this permission notice shall be             *
 *   included in all copies or substantial portions of the Software.            *
 * - The Software is provided "as is", without warranty of any kind,            *
 *   express or implied, including but not limited to the warranties of         *
 *   merchantability, fitness for a particular purpose and noninfringement.     *
 *   In no event shall the authors or copyright holders. be liable for any      *
 *   claim, damages or other liability, whether in an action of contract,       *
 *   tort or otherwise, arising from, out of or in connection with the          *
 *   software or the use or other dealings in the Software.                     *
 * - Except as contained in this notice, the name of Raphaël Boissel            *
 *   shall not be used in advertising or otherwise to                           *
 *   promote the sale, use or other dealings in the Snowfall method without     *
 *   prior written authorization from Raphaël Boissel.                          *
 *                                                                              */

/* Conformement a la legislation francaies en vigueur sur les systemes cryptographiques */
/* l'algorithme snowfallCipher est ici utilise a des fin de controle d'integrite        */
/* exclusivement. Merci de toujour utiliser comme cle de chiffrement une cle publique   */
/* et visible de tous dans le code de fact. (Article 30 Section 1 de la loi 2004-575)   */

/* Algorithme cree par Raphael Boissel a des fins de verification d'itegrite uniquement.*/
/* Merci de contacter l'auteur avant tout autre usage. (raphael.boissel@orange.fr)      */

/* AUCUNE METHODE PUBLIQUE DANS FACT NE DOIT PRENDRE EN PARAMETRE UNE CLE PASSEE A CET ALGORITHME */
/* NEVER PASS AN EXTERNAL KEY FROM A PUBLIC METHOD TO THIS ALGORITHM                              */

/* This algorithm is only design to check data integrity and must only be used for it */
/* no other guarenty are provided.                                                    */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Security
{
    public class SnowfallCipher
    {
        class RandomKey
        {
            Random Rnd = new Random();
            byte[] _Key;
            byte[] _Side;
            public RandomKey(byte[] Key)
            {
                if (Key.Length <= 8)
                {
                    // Danger !!!
                    // Predictible behavior
                    _Key = new byte[Key.Length];
                    _Side = new byte[4];
                    for (int N = 0; N < Key.Length; N++)
                    { _Key[N] = Key[N]; }
                    for (int N = 0; N < _Side.Length; N++)
                    { _Side[N] = 0; }
                }
                else
                {
                    _Key = new byte[Key.Length - 4];
                    _Side = new byte[4];
                    for (int N = 0; N < _Key.Length; N++)
                    { _Key[N] = Key[N]; }
                    for (int N = Key.Length - 4; N < Key.Length; N++)
                    { _Side[N - (Key.Length - 4)] = Key[N]; }
                }            
            }
            public void Next()
            {
                
                for (int N = 0; N < _Key.Length; N++)
                {
                    if (_Key[N] % 5 == 0)
                    {
                        if (_Side[1] % 2 != 0)
                        {
                            {
                                if (_Key[N] >= 254) { _Key[N] = 0; } else { _Key[N] += 2; }
                            }
                        }
                        else
                        {
                            {
                                if (_Key[N] == 255) { _Key[N] = 0; } else { _Key[N]++; }
                            }
                        }
                    }
                    if (_Key[N] % 3 == 0)
                    {
                        if (_Side[0] % 2 == 0)
                        {
                            {
                                if (_Key[N] >= 254) { _Key[N] = 0; } else { _Key[N] += 2; }
                            }
                        }
                        else
                        {
                            {
                                if (_Key[N] == 255) { _Key[N] = 0; } else { _Key[N]++; }
                            }
                        }
                    }
                    else if (_Key[N] % 2 == 0)
                    {
                        if (N == _Key.Length - 1) { byte A = _Key[0]; _Key[0] = _Key[N]; _Key[N] = A; }
                        else { byte A = _Key[N + 1]; _Key[N + 1] = _Key[N]; _Key[N] = A; }
                    }
                    else
                    {
                        if (N == 0) { byte A = _Key[_Key.Length - 1]; _Key[_Key.Length - 1] = _Key[N]; _Key[N] = A; }
                        else { byte A = _Key[N - 1]; _Key[N - 1] = _Key[N]; _Key[N] = A; }
                    }
                }
                if (_Key.Length > 0)
                {
                    if (_Key[0] % 2 == 0) 
                    {
                        if (_Side[0] == 255) { _Side[0] = 0; }
                        else { _Side[0]++; }
                    }
                }
                if (_Key.Length > 1)
                {
                    if (_Key[1] % 2 == 0)
                    {
                        if (_Side[1] == 255) { _Side[1] = 0; }
                        else { _Side[1]++; }
                    }
                }
                if (_Key.Length > 2)
                {
                    if (_Key[1] % 2 == 0)
                    {
                        byte A = _Side[0]; _Side[0] = _Side[2]; _Side[2] = A;
                        A = _Side[1]; _Side[1] = _Side[3]; _Side[3] = A;
                    }
                }
            }

            public byte[] Decrypt(byte[] Array)
            {
                List<Byte> Out = new List<byte>(Array.Length + 32);
                int X = 0, Y = 0;
                byte[][] Bloc = new byte[4][]; for (int B = 0; B < 4; B++) { Bloc[B] = new byte[4]; }
                bool RemainInit = false;
                int Remain = 0;
                for (int N = 0; N < Array.Length; N++)
                {
                    Bloc[X][Y] = Array[N];
                    X++; if (X == 4)
                    {
                        X = 0; Y++;
                        if (Y == 4)
                        {
                            Decrypt(ref Bloc);
                            for (int YY = 0; YY < 4; YY++)
                            {
                                for (int XX = 0; XX < 4; XX++)
                                {
                                    if (!RemainInit) { Remain = Bloc[XX][YY] % 16; RemainInit = true; }
                                    else { Out.Add(Bloc[XX][YY]); }
                                }

                            }

                            X = 0; Y = 0;
                        }
                    }
                }
                Out.RemoveRange(Out.Count - (15 - Remain), (15 - Remain));
                return Out.ToArray();

            }


            public byte[] Encrypt(byte[] Array)
            {
                List<Byte> Out = new List<byte>(Array.Length + 32);
                byte Remain = (byte)(Array.Length % 16);
                int Factor = Rnd.Next(8);
                Remain += (byte)(Factor * 16);
                int X = 0, Y = 0;
                byte[][] Bloc = new byte[4][]; for (int B = 0; B < 4; B++) { Bloc[B] = new byte[4]; }
                Bloc[0][0] = Remain; X++;
               
                for (int N = 0; N < Array.Length; N++)
                {
                    Bloc[X][Y] = Array[N];
                    X++; if (X == 4)
                    {
                        X = 0; Y++; 
                        if (Y == 4)
                        {
                            Encrypt(ref Bloc);
                            for (int YY = 0; YY < 4; YY++)
                            {
                                for (int  XX = 0; XX < 4; XX++)
                                {
                                    Out.Add(Bloc[XX][YY]);
                                }

                            }
                           
                            X = 0; Y = 0;
                        }
                    }  
                }
                if (X != 0 || Y != 0)
                {
                    while (X < 4) { while (Y < 4) { Bloc[X][Y] = (byte)Rnd.Next(256); Y++; } X++; }
                    Encrypt(ref Bloc);
                    for (int YY = 0; YY < 4; YY++)
                    {
                        for (int XX = 0; XX < 4; XX++)
                        {
                            Out.Add(Bloc[XX][YY]);
                        }

                    }

                    X = 0; Y = 0;
                }
                return Out.ToArray();
                  
            }
            public void Encrypt(ref byte[][] Bloc)
            {
                byte Magik = 0x00;
                for (int X = 0; X < Bloc.Length; X++)
                    for (int Y = 0; Y < Bloc[X].Length; Y++)
                        Magik = (byte)(Magik ^ Bloc[X][Y]);

                int KeyP = 0;
            
                // Transpose
                for (int X = 0; X < Bloc.Length; X++)
                {
                    for (int Y = 1; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 2 == 0)
                        { byte T = Bloc[X][Y - 1]; Bloc[X][Y - 1] = Bloc[X][Y]; Bloc[X][Y] = T; }


                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                        Y++;
                    }
                    
                }
                KeyP = 0;
                for (int X = 1; X < Bloc.Length; X++)
                {
                    for (int Y = 0; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 2 == 1)
                        { byte T = Bloc[X - 1][Y]; Bloc[X - 1][Y] = Bloc[X][Y]; Bloc[X][Y] = T; }

                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                    }
                    X++;
                }
                KeyP = 0;

                // Substitute

                for (int X = 0; X < Bloc.Length; X++)
                {
                    for (int Y = 1; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 3 == 0)
                            Bloc[X][Y] = (byte)(Bloc[X][Y] ^ Bloc[X][Y - 1]);
                        else
                            Bloc[X][Y - 1] = (byte)(Bloc[X][Y] ^ Bloc[X][Y - 1]);
                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                        Y++;
                    }
                   
                }
                KeyP = 0;

                for (int X = 1; X < Bloc.Length; X++)
                {
                    for (int Y = 0; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 3 == 0)
                            Bloc[X][Y] = (byte)(Bloc[X][Y] ^ Bloc[X - 1][Y]);
                        else
                            Bloc[X - 1][Y] = (byte)(Bloc[X][Y] ^ Bloc[X - 1][Y]);
                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                    }
                    X++;
                }
                KeyP = 0;

                for (int N = 0; N < Bloc.Length; N++)
                {
                    for (int NN = 0; NN < Bloc.Length; NN++)
                    {
                        int A = Bloc[N][NN];
                        A += _Key[KeyP];
                        if (A > 255) { A -= 256; }
                        Bloc[N][NN] = (byte)A;
                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                    }
                }
                for (int N = 0; N < _Key.Length; N++)
                    _Key[N] = (byte)(_Key[N] ^ Magik);
                Next();
            }

            public void Decrypt(ref byte[][] Bloc)
            {
                // substitute
                int KeyP = 0;
                for (int N = 0; N < Bloc.Length; N++)
                {
                    for (int NN = 0; NN < Bloc.Length; NN++)
                    {
                        int A = Bloc[N][NN];
                        A -= _Key[KeyP];
                        if (A < 0) { A += 256; }
                        Bloc[N][NN] = (byte)A;

                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                    }
                }

               
                
                KeyP = 0;
                for (int X = 1; X < Bloc.Length; X++)
                {
                    for (int Y = 0; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 3 == 0)
                            Bloc[X][Y] = (byte)(Bloc[X][Y] ^ Bloc[X - 1][Y]);
                        else
                            Bloc[X - 1][Y] = (byte)(Bloc[X][Y] ^ Bloc[X - 1][Y]);
                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                       
                    }
                    X++;
                }

                KeyP = 0;
                for (int X = 0; X < Bloc.Length; X++)
                {
                    for (int Y = 1; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 3 == 0)
                            Bloc[X][Y] = (byte)(Bloc[X][Y] ^ Bloc[X][Y - 1]);
                        else
                            Bloc[X][Y - 1] = (byte)(Bloc[X][Y] ^ Bloc[X][Y - 1]);
                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                        Y++;
                    }
                    
                }
                // transpose
                KeyP = 0;
                for (int X = 1; X < Bloc.Length; X++)
                {
                    for (int Y = 0; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 2 == 1)
                        { byte T = Bloc[X - 1][Y]; Bloc[X - 1][Y] = Bloc[X][Y]; Bloc[X][Y] = T; }


                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                    }
                    X++;
                }
                KeyP = 0;
                for (int X = 0; X < Bloc.Length; X++)
                {
                    for (int Y = 1; Y < Bloc[X].Length; Y++)
                    {
                        if (_Key[KeyP] % 2 == 0)
                        { byte T = Bloc[X][Y - 1]; Bloc[X][Y - 1] = Bloc[X][Y]; Bloc[X][Y] = T; }


                        KeyP++;
                        if (KeyP == _Key.Length) { KeyP = 0; }
                        Y++;
                    }
                   
                }

                byte Magik = 0x00;
                for (int X = 0; X < Bloc.Length; X++)
                    for (int Y = 0; Y < Bloc[X].Length; Y++)
                        Magik = (byte)(Magik ^ Bloc[X][Y]);
                for (int N = 0; N < _Key.Length; N++)
                    _Key[N] = (byte)(_Key[N] ^ Magik);
                Next();
            }
        }
        Random Rnd = new Random();
        byte[] _PrivateKey;
        RandomKey EncryptKey;
        RandomKey DecryptKey;
        void Evolve()
        {
 
        }
        public SnowfallCipher(byte[] PrivateKey)
        {
            _PrivateKey = PrivateKey;
        }
        public SnowfallCipher(string Password)
        {
            _PrivateKey = System.Security.Cryptography.SHA512.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(Password));
        }
        public byte[] GeneratePublicKey()
        {
            byte[] PublicKey = new byte[256];
            Rnd.NextBytes(PublicKey);
            return PublicKey;
        }
        public void SetEncryptionKey(byte[] PublicKey) 
        {
            byte[] Key = new byte[PublicKey.Length];
            for (int N = 0; N < Key.Length; N++)
            {
                if (PublicKey[N] >= _PrivateKey.Length) { Key[N] = _PrivateKey[PublicKey[N] % _PrivateKey.Length]; }
                else { Key[N] = _PrivateKey[PublicKey[N]]; }
            }
            EncryptKey = new RandomKey(Key);
        }
        public void SetDecryptionKey(byte[] PublicKey) 
        {
            byte[] Key = new byte[PublicKey.Length];
            for (int N = 0; N < Key.Length; N++)
            {
                if (PublicKey[N] >= _PrivateKey.Length) { Key[N] = _PrivateKey[PublicKey[N] % _PrivateKey.Length]; }
                else { Key[N] = _PrivateKey[PublicKey[N]]; }
            }
            DecryptKey = new RandomKey(Key);
        }
        public byte[] Encrypt(byte[] Array) { if (EncryptKey == null) { EncryptKey = new RandomKey(_PrivateKey); } return EncryptKey.Encrypt(Array); }
        public byte[] Decrypt(byte[] Array) { if (DecryptKey == null) { DecryptKey = new RandomKey(_PrivateKey); } return DecryptKey.Decrypt(Array); }
    }
}

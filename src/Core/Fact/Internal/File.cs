﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal
{
    static class File
    {
        static public Fact.Processing.File.FileType GetFileType(string File)
        {
            try
            {
                // Try to solve the filetype with the magik number
                if (!System.IO.File.Exists(File)) { return GetFileTypeBasedOnName(File); }
                System.IO.Stream stream = System.IO.File.Open(File, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                if (stream == null) { return GetFileTypeBasedOnName(File); }

                Fact.Processing.File.FileType type = Processing.File.FileType.None;
                try { type = GetFileTypeBasedOnMagkNumber(stream); } catch { }
                try { stream.Close(); } catch { }

                if (type != Processing.File.FileType.None)
                {
                    if (type == Processing.File.FileType.Executable)
                    {
                        if (File.EndsWith(".dll")) { return Processing.File.FileType.DynamicLibrary; }
                    }
                    return type;
                }

                // We should now try to see if the file have a shebang
                stream = System.IO.File.Open(File, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                if (stream == null) { return GetFileTypeBasedOnName(File); }
                try { type = GetFileTypeBasedOnSheBang(stream); } catch { }
                try { stream.Close(); }  catch { }
                if (type != Processing.File.FileType.None) { return type; }
                if (type == Processing.File.FileType.None)
                { return GetFileTypeBasedOnName(File); }
            }
            catch { }
            return GetFileTypeBasedOnName(File);
        }

        static public bool IsTextFile(Fact.Processing.File.FileType FileType)
        {
            switch (FileType)
            {
                case Processing.File.FileType.Ant:
                case Processing.File.FileType.Authors:
                case Processing.File.FileType.ChangeLog:
                case Processing.File.FileType.CHeader:
                case Processing.File.FileType.CMakeLists:
                case Processing.File.FileType.Code:
                case Processing.File.FileType.Configure:
                case Processing.File.FileType.CPPHeader:
                case Processing.File.FileType.CPPSource:
                case Processing.File.FileType.CSource:
                case Processing.File.FileType.CSSource:
                case Processing.File.FileType.GdbScript:
                case Processing.File.FileType.Gitignore:
                case Processing.File.FileType.GrepFile:
                case Processing.File.FileType.JavaSource:
                case Processing.File.FileType.Latex:
                case Processing.File.FileType.LuaSource:
                case Processing.File.FileType.Makefile:
                case Processing.File.FileType.Maven:
                case Processing.File.FileType.MicrosoftSolution:
                case Processing.File.FileType.PerlSource:
                case Processing.File.FileType.PythonSource:
                case Processing.File.FileType.Readme:
                case Processing.File.FileType.SedFile:
                case Processing.File.FileType.ShSource:
                case Processing.File.FileType.Text:
                case Processing.File.FileType.Todo:
                case Processing.File.FileType.XML:
                    return true;
                default:
                    return false;
            }
        }

        static Fact.Processing.File.FileType GetFileTypeBasedOnMagkNumber(System.IO.Stream Stream)
        {
            // Try to solve a 4Bytes MagikNumber
            int v1 = Stream.ReadByte();
            int v2 = Stream.ReadByte();
            int v3 = Stream.ReadByte();
            int v4 = Stream.ReadByte();

            if (v1 == 0xCA && v2 == 0xFE && v3 == 0xBA && v4 == 0xBE) { return Processing.File.FileType.JavaClass; /* Java Class */ }
            if (v1 == 0xBE && v2 == 0xBA && v3 == 0xFE && v4 == 0xCA) { return Processing.File.FileType.JavaClass; /* Java Class */ }
            if (v1 == 0x7F && v2 == 0x45 && v3 == 0x4C && v4 == 0x46) { return Processing.File.FileType.Executable; /* ELF */ }
            if (v1 == 0x4A && v2 == 0x6F && v3 == 0x79 && v4 == 0x21) { return Processing.File.FileType.Executable; /* Mac Old */ }
            if (v1 == 0xFE && v2 == 0xED && v3 == 0xFA && v4 == 0xCE) { return Processing.File.FileType.Executable; /* Mac 32 */ }
            if (v1 == 0xFE && v2 == 0xED && v3 == 0xFA && v4 == 0xCF) { return Processing.File.FileType.Executable; /* Mac 64 */ }
            if (v1 == 0xCE && v2 == 0xFA && v3 == 0xED && v4 == 0xFE) { return Processing.File.FileType.Executable; /* Mac 32 */ }
            if (v1 == 0xCF && v2 == 0xFA && v3 == 0xED && v4 == 0xFE) { return Processing.File.FileType.Executable; /* Mac 64 */ }
            if (v1 == 0xFA && v2 == 0xC7 && v3 == 0x00 && v4 == 0x02) { return Processing.File.FileType.FactPackage; /* Fact package */ }
            if (v1 == 0x02 && v2 == 0x00 && v3 == 0xC7 && v4 == 0xFA) { return Processing.File.FileType.FactPackage; /* Fact package */ }
            if (v1 == 0x49 && v2 == 0x49 && v3 == 0x2A && v4 == 0x00) { return Processing.File.FileType.Image; /* Tiff Image */ }
            if (v1 == 0x4D && v2 == 0x4D && v3 == 0x00 && v4 == 0x2A) { return Processing.File.FileType.Image; /* Tiff Image */ }
            if (v1 == 0xFF && v2 == 0xD8 && v3 == 0xFF) { return Processing.File.FileType.Image; /* Jpeg Image */ }
            if (v1 == 0x42 && v2 == 0x4D) { return Processing.File.FileType.Image; /* BMP*/ }
            if (v1 == 0x42 && v2 == 0x43) { return Processing.File.FileType.Executable; /* LLVM BC */ }
            if (v1 == 0x4D && v2 == 0x5A) { return Processing.File.FileType.Executable; /* MSDOS Executable */ }
            return Processing.File.FileType.None;
        }

        internal static Fact.Processing.File.ExecutableFormat GetExecutableType(System.IO.Stream Stream)
        {
            // Try to solve a 4Bytes MagikNumber
            int v1 = Stream.ReadByte();
            int v2 = Stream.ReadByte();
            int v3 = Stream.ReadByte();
            int v4 = Stream.ReadByte();

            if (v1 == 0xCA && v2 == 0xFE && v3 == 0xBA && v4 == 0xBE) { return Processing.File.ExecutableFormat.JavaClass; /* Java Class */ }
            if (v1 == 0xBE && v2 == 0xBA && v3 == 0xFE && v4 == 0xCA) { return Processing.File.ExecutableFormat.JavaClass; /* Java Class */ }
            if (v1 == 0x7F && v2 == 0x45 && v3 == 0x4C && v4 == 0x46) { return Processing.File.ExecutableFormat.ExecutableAndLinkableFormat; /* ELF */ }
            if (v1 == 0x4D && v2 == 0x5A) { return Processing.File.ExecutableFormat.PortableExecutable; /* MSDOS Executable */ }

            if (v1 > 0)
            {
                char chr1 = (char)v1;
                char chr2 = (char)v2;
                char chr3 = (char)v3;
                char chr4 = (char)v4;

                if (chr1 == '#' && chr2 == '!')
                {
                    string file = "";
                    if (chr3 != ' ' && chr3 != '\t') { file += chr3; }
                    if (chr4 != ' ' && chr4 != '\t') { file += chr4; }

                    char c = ' ';
                    do
                    {
                        c = (char)Stream.ReadByte();
                        file += c;
                    }
                    while (c != '\n' && c != '\r');
                    file = file.Trim().Replace('\t', ' ');
                    if (file.StartsWith("/usr/bin/env ") && file.EndsWith(" python")) { return Processing.File.ExecutableFormat.Python; }
                    if (file.StartsWith("/usr/bin/env ") && file.EndsWith(" python2")) { return Processing.File.ExecutableFormat.Python; }
                    if (file.StartsWith("/usr/bin/env ") && file.EndsWith(" python3")) { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/bin/python") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/bin/python2") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/bin/python26") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/bin/python27") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/bin/python3") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/usr/share/bin/python") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/usr/share/bin/python2") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/usr/share/bin/python26") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/usr/share/bin/python27") { return Processing.File.ExecutableFormat.Python; }
                    if (file == "/usr/share/bin/python3") { return Processing.File.ExecutableFormat.Python; }

                }
            }

            return Processing.File.ExecutableFormat.None;
        }

        static Fact.Processing.File.FileType GetFileTypeBasedOnSheBang(System.IO.Stream Stream)
        {
            string shebang = "";
            int bytevalue = 0;
            bool space = true;
            bool first = true;
            bool bom = false;
            do
            {
                bytevalue = Stream.ReadByte();
                if (first)
                {
                    first = false;
                    // BOM detection for custom UTF8-Files UTF-16 Files
                    if (bytevalue == 0xEF)
                    {
                        Stream.ReadByte(); // Skip the 0xBB
                        Stream.ReadByte(); // Skip the 0xBF
                        bom = true;
                        continue;
                    }
                    else if (bytevalue == 0xFE || bytevalue == 0xFF)
                    {
                        Stream.ReadByte(); // Skip the 0xFE or the 0xFF
                        bom = true;
                        continue;
                    }
                }

                if (bom)
                {
                    bom = false;
                    if (bytevalue == 0x00)
                    {
                        Stream.ReadByte(); // Skip the 0x00 for BOM UTF-32
                        continue;
                    }
                }
                if (bytevalue > 0)
                {
                    char chr = (char)bytevalue;
                    if (space)
                    {
                        if (chr == ' ' || chr == '\t' || chr == '\n' || chr == '\r') { continue; }
                        else if (chr == '#') { shebang = "#"; space = false; continue; }
                        else { break; }
                    }
                    else
                    {
                        if (chr == '\n' || chr == '\r') { break; }
                        shebang += chr;
                    }
                }
            } while (bytevalue > 0);

            if (shebang == "") { return Processing.File.FileType.None; }

            shebang = shebang.Trim();
            if (!shebang.StartsWith("#!")) { return Processing.File.FileType.None; }
            string shell = Information.SolveSheBang(shebang);
            if (shell == "") { return Processing.File.FileType.None; }
            try
            {
                string namewithouext = System.IO.Path.GetFileNameWithoutExtension(shell);
                namewithouext = namewithouext.Replace("-", "");
                namewithouext = namewithouext.Replace("_", "");
                namewithouext = namewithouext.Replace(".", "");
                namewithouext = namewithouext.Replace(" ", "");
                switch (namewithouext.ToLower())
                {
                    case "sh": return Processing.File.FileType.ShSource;
                    case "zsh": return Processing.File.FileType.ShSource;
                    case "bash": return Processing.File.FileType.ShSource;
                    case "ksh": return Processing.File.FileType.ShSource;
                    case "tcsh": return Processing.File.FileType.ShSource;
                    case "lua": return Processing.File.FileType.LuaSource;
                    case "lua4": return Processing.File.FileType.LuaSource;
                    case "lua5": return Processing.File.FileType.LuaSource;
                    case "lua51": return Processing.File.FileType.LuaSource;
                    case "lua52": return Processing.File.FileType.LuaSource;
                    case "python": return Processing.File.FileType.PythonSource;
                    case "python2": return Processing.File.FileType.PythonSource;
                    case "python26": return Processing.File.FileType.PythonSource;
                    case "python3": return Processing.File.FileType.PythonSource;
                    case "perl": return Processing.File.FileType.PerlSource;
                }
            }
            catch { }
            switch (shell)
            {
                case "/usr/bin/sh": return Processing.File.FileType.ShSource;
                case "/bin/sh": return Processing.File.FileType.ShSource;
                case "sh": return Processing.File.FileType.ShSource;
                case "/usr/bin/zsh": return Processing.File.FileType.ShSource;
                case "/bin/zsh": return Processing.File.FileType.ShSource;
                case "zsh": return Processing.File.FileType.ShSource;
                case "/usr/bin/ksh": return Processing.File.FileType.ShSource;
                case "/bin/ksh": return Processing.File.FileType.ShSource;
                case "ksh": return Processing.File.FileType.ShSource;
                case "/usr/bin/bash": return Processing.File.FileType.ShSource;
                case "/bin/bash": return Processing.File.FileType.ShSource;
                case "bash": return Processing.File.FileType.ShSource;
            }
            return Processing.File.FileType.None;
        }


        static Fact.Processing.File.FileType GetFileTypeBasedOnName(string File)
        {
            try
            {
                string ext = System.IO.Path.GetExtension(File);
                string name = System.IO.Path.GetFileName(File);
                string namewithouext = System.IO.Path.GetFileNameWithoutExtension(File);
                string directory = System.IO.Path.GetDirectoryName(File);

                if (ext.StartsWith(".")) { ext = ext.Substring(1); }
                ext = ext.ToLower();

                if (namewithouext.StartsWith("#") && namewithouext.EndsWith("#")) { return Processing.File.FileType.Trash; }
                if (namewithouext.EndsWith("~")) { return Processing.File.FileType.Trash; }

                switch (name)
                {
                    case "AUTHOR":
                    case "AUTHORS":
                    case "Author":
                    case "Authors":
                    case "author":
                    case "authors":
                    case "AUTHOR.txt":
                    case "AUTHORS.txt":
                    case "Author.txt":
                    case "Authors.txt":
                    case "author.txt":
                    case "authors.txt":
                        return Processing.File.FileType.Authors;
                    case "CMakeList.txt":
                    case "cmakelist.txt":
                    case "CMakelist.txt":
                    case "CMakeLists.txt":
                    case "cmakelists.txt":
                    case "CMakelists.txt":
                        return Processing.File.FileType.CMakeLists;
                    case "MAKEFILE":
                    case "Makefile":
                    case "MakeFile":
                    case "makefile":
                        return Processing.File.FileType.Makefile;

                    case "TODO":
                    case "todo":
                    case "TODO.txt":
                    case "todo.txt":
                        return Processing.File.FileType.Todo;
                }

                switch (ext)
                {
                    case "elf": return Processing.File.FileType.Executable;
                    case "cubin": return Processing.File.FileType.Executable;
                    case "exe": return Processing.File.FileType.Executable;

                    case "dll": return Processing.File.FileType.DynamicLibrary;
                    case "so": return Processing.File.FileType.DynamicLibrary;
                    case "dylib": return Processing.File.FileType.DynamicLibrary;

                    case "lib": return Processing.File.FileType.StaticLibrary;
                    case "a": return Processing.File.FileType.StaticLibrary;

                    case "o": return Processing.File.FileType.Object;
                    case "obj": return Processing.File.FileType.Object;

                    case "jpeg": return Processing.File.FileType.Image;
                    case "jpg": return Processing.File.FileType.Image;
                    case "png": return Processing.File.FileType.Image;
                    case "gif": return Processing.File.FileType.Image;
                    case "ppm": return Processing.File.FileType.Image;
                    case "pbm": return Processing.File.FileType.Image;
                    case "pgm": return Processing.File.FileType.Image;
                    case "bmp": return Processing.File.FileType.Image;
                    case "tiff": return Processing.File.FileType.Image;
                    case "wpg": return Processing.File.FileType.Image;
                    case "pcx": return Processing.File.FileType.Image;
                    case "cur": return Processing.File.FileType.Image;
                    case "dds": return Processing.File.FileType.Image;
                    case "dxf": return Processing.File.FileType.Image;
                    case "exr": return Processing.File.FileType.Image;
                    case "ico": return Processing.File.FileType.Image;
                    case "ptc": return Processing.File.FileType.Image;
                    case "pnm": return Processing.File.FileType.Image;
                    case "psd": return Processing.File.FileType.Image;
                    case "tga": return Processing.File.FileType.Image;

                    case "svg": return Processing.File.FileType.Image;
                    case "eps": return Processing.File.FileType.Image;
                    case "ps": return Processing.File.FileType.Image;
                    case "cdr": return Processing.File.FileType.Image;

                    case "wav": return Processing.File.FileType.Sound;
                    case "mp3": return Processing.File.FileType.Sound;
                    case "m4a": return Processing.File.FileType.Sound;
                    case "ogg": return Processing.File.FileType.Sound;
                    case "wma": return Processing.File.FileType.Sound;
                    case "flac": return Processing.File.FileType.Sound;

                    case "cs": return Processing.File.FileType.CSSource;

                    case "cpp": return Processing.File.FileType.CPPSource;
                    case "cxx": return Processing.File.FileType.CPPSource;
                    case "cc": return Processing.File.FileType.CPPSource;
                    case "c++": return Processing.File.FileType.CPPSource;
                    case "hpp": return Processing.File.FileType.CPPHeader;
                    case "hh": return Processing.File.FileType.CPPHeader;
                    case "hxx": return Processing.File.FileType.CPPHeader;
                    case "h++": return Processing.File.FileType.CPPHeader;
                    case "h":
                        try
                        {
                            if (System.IO.File.Exists(directory + "/" + namewithouext + ".cpp"))
                            {
                                return Processing.File.FileType.CPPHeader;
                            }
                            else if (System.IO.File.Exists(directory + "/" + namewithouext + ".cxx"))
                            {
                                return Processing.File.FileType.CPPHeader;
                            }
                            else if (System.IO.File.Exists(directory + "/" + namewithouext + ".cc"))
                            {
                                return Processing.File.FileType.CPPHeader;
                            }
                        }
                        catch { }
                        return Processing.File.FileType.CHeader;

                    case "c": return Processing.File.FileType.CSource;

                    case "py": return Processing.File.FileType.PythonSource;
                    case "pyw": return Processing.File.FileType.PythonSource;

                    case "pl": return Processing.File.FileType.PerlSource;
                    case "perl": return Processing.File.FileType.PerlSource;


                    case "dex": return Processing.File.FileType.JavaPackage;
                    case "odex": return Processing.File.FileType.JavaPackage;
                    case "jar": return Processing.File.FileType.JavaPackage;
                    case "class": return Processing.File.FileType.JavaClass;
                    case "java": return Processing.File.FileType.JavaSource;

                    case "sh": return Processing.File.FileType.ShSource;

                    case "lua": return Processing.File.FileType.LuaSource;

                    case "pdf": return Processing.File.FileType.PDF;

                    case "xpf": return Processing.File.FileType.XPF;

                    case "sln": return Processing.File.FileType.MicrosoftSolution;
                    case "vbproject": return Processing.File.FileType.MicrosoftSolution;
                    case "csproj": return Processing.File.FileType.MicrosoftSolution;
                    case "vbproj": return Processing.File.FileType.MicrosoftSolution;

                    case "sed": return Processing.File.FileType.SedFile;

                    case "grep": return Processing.File.FileType.GrepFile;

                    case "gdb": return Processing.File.FileType.GdbScript;

                    case "txt": return Processing.File.FileType.Text;

                    case "swp": return Processing.File.FileType.Trash;
                    case "xml": return Processing.File.FileType.XML;
                }
            }
            catch { }
            return Processing.File.FileType.None;
        }
    }

}

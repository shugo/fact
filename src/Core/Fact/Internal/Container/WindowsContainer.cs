﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Container
{
    unsafe class WindowsContainer : Container
    {
        // From Pinvoke
        public enum JOBOBJECTINFOCLASS : uint
        {
            AssociateCompletionPortInformation = 7,
            BasicLimitInformation = 2,
            BasicUIRestrictions = 4,
            EndOfJobTimeInformation = 6,
            ExtendedLimitInformation = 9,
            SecurityLimitInformation = 5,
            GroupInformation = 11
        }

        // From Pinvoke
        public enum JOBOBJECTLIMIT : uint
        {
            Workingset = 0x00000001,
            ProcessTime = 0x00000002,
            JobTime = 0x00000004,
            ActiveProcess = 0x00000008,
            Affinity = 0x00000010,
            PriorityClass = 0x00000020,
            PreserveJobTime = 0x00000040,
            SchedulingClass = 0x00000080,
            ProcessMemory = 0x00000100,
            JobMemory = 0x00000200,
            DieOnUnhandledException = 0x00000400,
            BreakawayOk = 0x00000800,
            SilentBreakawayOk = 0x00001000,
            KillOnJobClose = 0x00002000,
            SubsetAffinity = 0x00004000,
            JobReadBytes = 0x00010000,
            JobWriteBytes = 0x00020000,
            RateControl = 0x00040000,
        }

        // From Pinvoke
        struct JOBOBJECT_BASIC_LIMIT_INFORMATION
        {
            public Int64 PerProcessUserTimeLimit;
            public Int64 PerJobUserTimeLimit;
            public JOBOBJECTLIMIT LimitFlags;
            public UIntPtr MinimumWorkingSetSize;
            public UIntPtr MaximumWorkingSetSize;
            public UInt32 ActiveProcessLimit;
            public Int64 Affinity;
            public UInt32 PriorityClass;
            public UInt32 SchedulingClass;
        }

        struct IO_COUNTERS
        {
            public UInt64 ReadOperationCount;
            public UInt64 WriteOperationCount;
            public UInt64 OtherOperationCount;
            public UInt64 ReadTransferCount;
            public UInt64 WriteTransferCount;
            public UInt64 OtherTransferCount;
        }

        struct JOBOBJECT_EXTENDED_LIMIT_INFORMATION
        {
            public JOBOBJECT_BASIC_LIMIT_INFORMATION BasicLimitInformation;
            public IO_COUNTERS IoInfo;
            public UIntPtr ProcessMemoryLimit;
            public UIntPtr JobMemoryLimit;
            public UIntPtr PeakProcessMemoryUsed;
            public UIntPtr PeakJobMemoryUsed;
        }

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static IntPtr CreateJobObject(void* lpJobAttributes, char* dwMilliseconds);

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool AssignProcessToJobObject(IntPtr hJob, IntPtr hProcess);


        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool QueryInformationJobObject(IntPtr hJob, JOBOBJECTINFOCLASS JobObjectInfoClass, void* lpJobObjectInfo, int cbJobObjectInfoLength, int* returnLength);


        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool SetInformationJobObject(IntPtr hJob, JOBOBJECTINFOCLASS JobObjectInfoClass, void* lpJobObjectInfo, int cbJobObjectInfoLength);

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static bool CloseHandle(IntPtr hObject);

        IntPtr _HJob = new IntPtr();
        internal WindowsContainer()
        {
            _HJob = CreateJobObject(null, null);
            JOBOBJECT_EXTENDED_LIMIT_INFORMATION info = new JOBOBJECT_EXTENDED_LIMIT_INFORMATION();
            QueryInformationJobObject(_HJob, JOBOBJECTINFOCLASS.ExtendedLimitInformation, &info, sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION), null);
            info.BasicLimitInformation.LimitFlags = JOBOBJECTLIMIT.KillOnJobClose;
            SetInformationJobObject(_HJob, JOBOBJECTINFOCLASS.ExtendedLimitInformation, &info, sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));
        }

        public override bool AddProcess(int PID)
        {
            if (_HJob.ToInt64() == 0) { return false; }
            try
            {
                IntPtr hProcess = System.Diagnostics.Process.GetProcessById(PID).Handle;
                return AssignProcessToJobObject(_HJob, hProcess);
            }
            catch { return false; }
        }


        ~WindowsContainer()
        {
            CloseHandle(_HJob);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Container
{
    public class ContainerManager
    {
        private ContainerManager() { }
        public static Container CreateContainter()
        {
            if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
            { return new MacContainer(); }
            if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
            { return new LinuxContainer(); }
            if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            { return new WindowsContainer(); }
            return null;
        }
    }
}

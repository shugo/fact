﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Device
{
    static class Generic
    {
        static GenericIn _generic;
        internal class GenericIn
        {
            public virtual string[] GetProcessorsName() { return new string[0]; }
        }
        static Generic()
        {
            if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
            { _generic = new GenericIn(); }
            else if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
            { _generic = new GenericIn(); }
            else if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            { _generic = new Windows(); }
            else
            { _generic = new GenericIn(); }
        }

        public static string[] GetProcessorsName() { return _generic.GetProcessorsName(); }
    }
}

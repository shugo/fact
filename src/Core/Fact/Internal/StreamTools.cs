﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal
{
    static internal class StreamTools
    {

        static void GeneralizedSerialize(System.IO.Stream stream, object obj)
        {
            WriteUTF8String(stream, obj.GetType().Assembly.FullName);
            WriteUTF8String(stream, obj.GetType().FullName);
            foreach (System.Reflection.FieldInfo field in obj.GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance))
            {
                WriteUTF8String(stream, field.Name);
                Serialize(stream, field.GetValue(obj));
            }
            WriteUTF8String(stream, "");
        }

        static object GeneralizedDeserialize(System.IO.Stream stream)
        {
            string assemblyName = ReadUTF8String(stream);
            System.Reflection.Assembly assembly = null;
            foreach (System.Reflection.Assembly passembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (passembly.FullName == assemblyName)
                {
                    assembly = passembly;
                    break;
                }
            }
            if (assembly == null) { throw new Exception("The assembly " + assemblyName + " needed for the deserialization has not been loaded"); }
            Type type = assembly.GetType(ReadUTF8String(stream));

            Dictionary<string, object> fields = new Dictionary<string, object>();
            string fieldName = "";
            object fieldValue = null;
            do
            {
                fieldName = ReadUTF8String(stream);
                if (fieldName != "")
                {
                    fieldValue = Deserialize(stream);
                    fields.Add(fieldName, fieldValue);
                }
            } while (fieldName != "");

            System.Reflection.ConstructorInfo constructor = type.GetConstructor(new Type[] { });
            if (constructor == null) { throw new Exception("The type " + type.Name + " has no default constructor"); }
            object result = constructor.Invoke(new object[] { });



            foreach (System.Reflection.FieldInfo field in result.GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance))
            {
                if (fields.ContainsKey(field.Name))
                {
                    field.SetValue(result, fields[field.Name]);
                }
            }

            return result;
        }

        static internal byte[] Serialize(object obj)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            Serialize(stream, obj);
            stream.Flush();
            return stream.ToArray();
        }

        static internal void Serialize(System.IO.Stream stream, object obj)
        {
            if (obj is Exception) { WriteUTF8ShortString(stream, "@@exception");  WriteException(stream, obj as Exception); return; }
            if (obj == null) { /* WriteUTF8ShortString(stream, "@@null"); */; WriteUTF8ShortString(stream, ""); return; }
            if (obj is string || obj is String) { /* WriteUTF8ShortString(stream, "@@string"); */; WriteUTF8ShortString(stream, "s"); WriteUTF8String(stream, (string)obj); return; }
            if (obj is bool || obj is Boolean) { /* WriteUTF8ShortString(stream, "@@bool"); */ WriteUTF8ShortString(stream, "b"); WriteBoolean(stream, (bool)obj); return; }
            if (obj is Int32) { /* WriteUTF8ShortString(stream, "@@Int32"); */ WriteUTF8ShortString(stream, "i"); WriteInt32(stream, (Int32)obj); return; }
            if (obj is Int64) { WriteUTF8ShortString(stream, "@@Int64"); WriteInt64(stream, (Int64)obj); return; }
            if (obj is Int16) { WriteUTF8ShortString(stream, "@@Int16"); WriteInt16(stream, (Int16)obj); return; }
            if (obj is Byte) { WriteUTF8ShortString(stream, "@@Byte"); stream.WriteByte((Byte)obj); return; }

            if (obj is DateTime) { WriteUTF8ShortString(stream, "@@DateTime"); WriteDateTime(stream, (DateTime)obj); return; }
            if (obj is Version) { WriteUTF8ShortString(stream, "@@Version"); WriteVersion(stream, (Version)obj); return; }

            if (obj is Fact.Processing.File) { WriteUTF8ShortString(stream, "@Fact@File"); ((Fact.Processing.File)obj).Save(stream); return; }
            if (obj is Fact.Processing.Project) { WriteUTF8ShortString(stream, "@Fact@Project"); ((Fact.Processing.Project)obj).Save(stream); return; }
            if (obj is Fact.Test.Result.Result) { WriteUTF8ShortString(stream, "@Fact@Test"); ((Fact.Test.Result.Result)obj).Save(stream); return; }

            if (obj is Fact.Directory.User) { WriteUTF8ShortString(stream, "@Fact@User"); ((Fact.Directory.User)obj).Save(stream); return; }
            if (obj is Fact.Directory.Group) { WriteUTF8ShortString(stream, "@Fact@Group"); ((Fact.Directory.Group)obj).Save(stream); return; }
            if (obj is Fact.Directory.Directory) { WriteUTF8ShortString(stream, "@Fact@Directory"); ((Fact.Directory.Directory)obj).Save(stream); return; }

            if (obj is Reflection.MSILOpCode) { WriteUTF8ShortString(stream, "@Fact@MSILOpCode"); ((Reflection.MSILOpCode)obj).Save(stream); return; }
            if (obj is Reflection.MSILMetadataToken) { WriteUTF8ShortString(stream, "@Fact@MSILMetadataToken"); ((Reflection.MSILMetadataToken)obj).Save(stream); return; }
            if (obj is Fact.Reflection.Method) { WriteUTF8ShortString(stream, "@Fact@Method"); (obj as Fact.Reflection.Method).Serialize(stream); return; }
            if (obj is Fact.Reflection.Assembly) { WriteUTF8ShortString(stream, "@Fact@Assembly"); (obj as Fact.Reflection.Assembly).Serialize(stream); return; }


            if (obj is Dictionary<string, string>)
            {
                /* WriteUTF8ShortString(stream, "@@Dictionary@String.String"); */
                WriteUTF8ShortString(stream, "dss");
                WriteInt32(stream, (obj as Dictionary<string, string>).Count);
                foreach (KeyValuePair<string, string> pair in (obj as Dictionary<string, string>))
                {
                    WriteUTF8String(stream, pair.Key);
                    WriteUTF8String(stream, pair.Value);
                }
                return;
            }
            if (obj is Dictionary<string, object>)
            {
                /* WriteUTF8ShortString(stream, "@@Dictionary@String.Object"); */
                WriteUTF8ShortString(stream, "dso");
                WriteInt32(stream, (obj as Dictionary<string, object>).Count);
                foreach (KeyValuePair<string, object> pair in (obj as Dictionary<string, object>))
                {
                    WriteUTF8String(stream, pair.Key);
                    Serialize(stream, pair.Value);
                }
                return;
            }
            if (obj is string[])
            {
                /* WriteUTF8ShortString(stream, "@@Array@String"); */
                WriteUTF8ShortString(stream, "as");
                string[] str = (string[])obj;
                WriteInt32(stream, str.Length);
                foreach (string s in str) { WriteUTF8String(stream, s); }
                return;
            }
            if (obj is byte[]) 
            {
                /* WriteUTF8ShortString(stream, "@@Array@Byte"); */
                WriteUTF8ShortString(stream, "aB");
                WriteByteArray(stream, (byte[])obj);
                return;
            }
            if (obj is Int32[])
            {
                WriteUTF8ShortString(stream, "@@Array@Int32");
                Int32[] array = (Int32[])obj;
                WriteInt32(stream, array.Length);
                foreach (Int32 i in array) { WriteInt32(stream, i); }
                return;
            }
            if (obj is object[])
            {
                WriteUTF8ShortString(stream, "@@Array@Object");
                object[] array = (object[])obj;
                WriteInt32(stream, array.Length);
                foreach (object o in array) { Serialize(stream, o); }
                return;
            }
            {
                // Check now with .NET serialization
                if (obj.GetType().IsSerializable)
                {
                    WriteUTF8ShortString(stream, "@@#");
                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    formatter.Serialize(stream, obj);
                    return;
                }
            }
            {
                // Check if we can use the generalized serializer
                WriteUTF8ShortString(stream, "@@$");
                GeneralizedSerialize(stream, obj);
                return;
            }
#if DEBUG
            Fact.Log.Debug("No serialization rule for " + obj.GetType().FullName);
#endif
            throw new Exception("Impossible to serialize the object");

        }

        static internal object Deserialize(System.IO.Stream stream)
        {
            string type = ReadUTF8ShortString(stream);
            switch (type)
            {
                case "@@exception": return ReadException(stream);
                case "@@null": case "": return null;
                case "@@string": case "s": return ReadUTF8String(stream);
                case "@@bool": case "b": return ReadBoolean(stream);
                case "@@Int32": case "i": return ReadInt32(stream);
                case "@@Int64": return ReadInt64(stream);
                case "@@Int16": return ReadInt16(stream);
                case "@@Byte": case "B": return (byte)stream.ReadByte();
                case "@@DateTime": return ReadDateTime(stream);
                case "@@Version": return ReadVersion(stream);

                case "@Fact@File": Fact.Processing.File file = new Fact.Processing.File(); file.Load(stream); return file;
                case "@Fact@Project": return Fact.Processing.Project.Load(stream);
                case "@Fact@Test": return Fact.Test.Result.Result.Load(stream);

                case "@Fact@User": Fact.Directory.User user = new Fact.Directory.User("", ""); user.Load(stream); return user;
                case "@Fact@Group": Fact.Directory.Group group = new Fact.Directory.Group(""); group.Load(stream); return group;
                case "@Fact@Directory": Fact.Directory.Directory directory = new Fact.Directory.Directory(); directory.Load(stream); return directory;
                case "@Fact@MSILOpCode": Reflection.MSILOpCode opcode = Reflection.MSILOpCode.Load(stream); return opcode;
                case "@Fact@MSILMetadataToken": Reflection.MSILMetadataToken token = Reflection.MSILMetadataToken.Load(stream); return token;
                case "@Fact@Method": Fact.Reflection.Method method = Fact.Reflection.Method.Deserialize(stream); return method;
                case "@Fact@Assembly": Fact.Reflection.Assembly assembly = Fact.Reflection.Assembly.Deserialize(stream); return assembly;

                case "@@Dictionary@String.Object": case "dso":
                    {
                        Dictionary<string, object> dict = new Dictionary<string,object>();
                        int count = ReadInt32(stream);
                        while (count > 0)
                        {
                            string key = ReadUTF8String(stream);
                            object val = Deserialize(stream);
                            dict.Add(key, val);
                            count--;
                        }
                        return dict;
                    }
                case "@@Dictionary@String.String": case "dss":
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        int count = ReadInt32(stream);
                        while (count > 0)
                        {
                            string key = ReadUTF8String(stream);
                            string val = ReadUTF8String(stream);
                            dict.Add(key, val);
                            count--;
                        }
                        return dict;
                    }
                case "@@Array@String": case "as":
                    {
                        List<string> strings = new List<string>();
                        int length = ReadInt32(stream);
                        for (int n = 0; n < length; n++)
                        { strings.Add(ReadUTF8String(stream)); }
                        return strings.ToArray();
                    }
                case "@@Array@Object": case "ao":
                    {
                        List<object> objects = new List<object>();
                        int length = ReadInt32(stream);
                        for (int n = 0; n < length; n++)
                        { objects.Add(Deserialize(stream)); }
                        return objects.ToArray();
                    }
                case "@@Array@Int32": case "ai":
                    {
                        List<Int32> ints = new List<Int32>();
                        int length = ReadInt32(stream);
                        for (int n = 0; n < length; n++)
                        { ints.Add(ReadInt32(stream)); }
                        return ints.ToArray();
                    }
                case "@@Array@Byte": case "aB": return ReadByteArray(stream);
                case "@@#":
                    {
                        // Serialized with .NET
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return formatter.Deserialize(stream);
                    }
                case "@@$":
                    {
                        return GeneralizedDeserialize(stream);
                    }
            }

            throw new Exception("Impossible to serialize the object");
        }

        static internal Int32 ReadInt32(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 4);
            return BitConverter.ToInt32(buffer, 0);
        }
        static internal Int32 ReadInt32(System.IO.Stream stream)
        {
            byte[] array = new byte[4];
            stream.Read(array, 0, 4);
            return BitConverter.ToInt32(array, 0);
        }
        static internal void WriteInt32(System.IO.Stream stream, Int32 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 4);
        }
        static internal Int16 ReadInt16(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 2);
            return BitConverter.ToInt16(buffer, 0);
        }
        static internal Int16 ReadInt16(System.IO.Stream stream)
        {
            byte[] array = new byte[2];
            stream.Read(array, 0, 2);
            return BitConverter.ToInt16(array, 0);
        }
        static internal void WriteInt16(System.IO.Stream stream, Int16 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 2);
        }
        static internal Int64 ReadInt64(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 8);
            return BitConverter.ToInt64(buffer, 0);
        }
        static internal Int64 ReadInt64(System.IO.Stream stream)
        {
            byte[] array = new byte[8]; 
            stream.Read(array, 0, 8);
            return BitConverter.ToInt64(array, 0);
        }
        static internal void WriteInt64(System.IO.Stream stream, Int64 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 8);
        }
        static internal UInt32 ReadUInt32(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 4);
            return BitConverter.ToUInt32(buffer, 0);
        }
        static internal UInt32 ReadUInt32(System.IO.Stream stream)
        {
            byte[] array = new byte[4];
            stream.Read(array, 0, 4);
            return BitConverter.ToUInt32(array, 0);
        }
        static internal void WriteUInt32(System.IO.Stream stream, UInt32 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 4);
        }
        static internal UInt64 ReadUInt64(System.IO.Stream stream)
        {
            byte[] array = new byte[8];
            stream.Read(array, 0, 8);
            return BitConverter.ToUInt64(array, 0);
        }
        static internal void WriteUInt64(System.IO.Stream stream, UInt64 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 8);
        }
        static internal bool ReadBoolean(System.IO.Stream stream)
        {
			return stream.ReadByte() != 0x00;
        }
        static internal void WriteBoolean(System.IO.Stream stream, Boolean value)
        {
			stream.WriteByte(value ? (byte)0xFF : (byte)0x00);
        }

        static internal byte[] ReadByteArray(System.IO.Stream stream)
        {
            Int32 length = ReadInt32(stream);
            byte[] array = new byte[length];
            stream.Read(array, 0, length);
            return array;
        }
        static internal void WriteByteArray(System.IO.Stream stream, byte[] value)
        {
            // DO NOT USE LongLength
            WriteInt32(stream, value.Length);
            stream.Write(value, 0, value.Length);
        }

        static internal string ReadUTF8String(System.IO.Stream stream)
        {
            return System.Text.Encoding.UTF8.GetString(ReadByteArray(stream));
        }

        static internal void WriteUTF8String(System.IO.Stream stream, string value)
        {
            WriteByteArray(stream, System.Text.Encoding.UTF8.GetBytes(value));
        }


        static internal void WriteUTF8ShortString(System.IO.Stream stream, string value)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(value);
            if (data.Length == 1)
            {
                if (data[0] == 0)
                {
                    stream.WriteByte(0);
                    stream.WriteByte(1);
                    stream.WriteByte(0);
                    return;
                }
                else
                {
                    stream.WriteByte(data[0]);
                }
            }
            else if (data.Length == 0)
            {
                stream.WriteByte(0);
                stream.WriteByte(0);
                return;
            }
            else if (data.Length < 255)
            {
                stream.WriteByte(0);
                stream.WriteByte((byte)data.Length);
                stream.Write(data, 0, data.Length);
            }
            else
            {
                stream.WriteByte(0);
                stream.WriteByte(255);
                WriteUTF8String(stream, value);
            }
        }

        static internal string ReadUTF8ShortString(System.IO.Stream streams)
        {
            int value = streams.ReadByte();
            if (value < 0) { return ""; }
            if (value > 0) { return ((char)value).ToString(); }
            value = streams.ReadByte();
            if (value <= 0) { return ""; }
            if (value == 255) { return ReadUTF8String(streams); }
            byte[] array = new byte[value];
            streams.Read(array, 0, value);
            return System.Text.Encoding.UTF8.GetString(array);
        }

        static internal DateTime ReadDateTime(System.IO.Stream stream)
        {
            int Millisecond = ReadInt16(stream);
            int Second = ReadInt16(stream);
            int Minute = ReadInt16(stream);
            int Hour = ReadInt16(stream);
            int Day = ReadInt16(stream);
            int Month = ReadInt16(stream);
            int Year = ReadInt16(stream);
            DateTime date = new DateTime(Year, Month, Day, Hour, Minute, Second, Millisecond);
            return date;

        }

        static internal void WriteDateTime(System.IO.Stream stream, DateTime value)
        {
            WriteInt16(stream, (short)value.Millisecond);
            WriteInt16(stream, (short)value.Second);
            WriteInt16(stream, (short)value.Minute);
            WriteInt16(stream, (short)value.Hour);
            WriteInt16(stream, (short)value.Day);
            WriteInt16(stream, (short)value.Month);
            WriteInt16(stream, (short)value.Year);
        }

        static internal void WriteVersion(System.IO.Stream stream, Version value)
        {
            WriteInt32(stream, value.Major);
            WriteInt32(stream, value.Minor);
            WriteInt32(stream, value.Build);
            WriteInt32(stream, value.Revision);
        }


        static internal Version ReadVersion(System.IO.Stream stream)
        {
            int Major = ReadInt32(stream);
            int Minor = ReadInt32(stream);
            int Build = ReadInt32(stream);
            int Revision = ReadInt32(stream);
            return new Version(Major, Minor, Build, Revision);
        }

        static internal void WriteException(System.IO.Stream stream, Exception e)
        {
            WriteUTF8String(stream, e.Message);
            WriteUTF8String(stream, e.Source);
            Serialize(stream, e.InnerException);
        }

        static internal Exception ReadException(System.IO.Stream stream)
        {
            string message = ReadUTF8String(stream);
            string source = ReadUTF8String(stream);
            return new Exception(message, Deserialize(stream) as Exception) { Source = source };
        }
    }
}

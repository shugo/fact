﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualStudio
{
    public class VS2015 : VS
    {
        public VS2015() : base() { }
        public override bool Accessible { get { return true; } }
        public override string Version { get { return "14.0"; } }
    }
}

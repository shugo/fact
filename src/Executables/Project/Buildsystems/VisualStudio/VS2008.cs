﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualStudio
{
    class VS2008 : VS
    {
        public VS2008() : base() { }
        public override bool Accessible { get { return true; } }
        public override string Version { get { return "9.0"; } }
    }
}

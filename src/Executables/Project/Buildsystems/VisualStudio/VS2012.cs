﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualStudio
{
    public class VS2012 : VS
    {
        public VS2012() : base() { }
        public override bool Accessible { get { return true; } }
        public override string Version { get { return "11.0"; } }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualStudio
{
    class VS2010 : VS
    {
        public VS2010() : base() { }
        public override bool Accessible { get { return true; } }
        public override string Version { get { return "10.0"; } }
    }
}

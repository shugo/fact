﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualStudio
{
    public class VS : Fact.Common.Buildsystems.Buildsystem
    {
        enum VisualStudioProject
        {
            CSharp,
            CPlusPlus,
            FSharp,
            JSharp,
            VisualBasic,
            MonoForAndroid,
            MonoTouch,
            MonoTouchBinding,
            Test,
            XamarinAndroid,
            XamariniOs
        }

        public VS() : base() { }
        public override bool Accessible { get  { return false; } }

        public virtual string Version { get { return "99.0"; } }

        Dictionary<int, Dictionary<VisualStudioProject, string>> _ProjectGUID = new Dictionary<int, Dictionary<VisualStudioProject, string>>();

        List<VisualStudioProject> GetProjectType(Fact.Processing.Project Project)
        {
            List<VisualStudioProject> types = new List<VisualStudioProject>();
            if (Contains_C_CPP_Sources(Project)) { types.Add(VisualStudioProject.CPlusPlus); }
            if (Contains_CS_Sources(Project)) { types.Add(VisualStudioProject.CSharp); }
            return types;
        }

        string GetProjectGUID(VisualStudioProject Type, Fact.Processing.Project Project)
        {
            if (!_ProjectGUID.ContainsKey(Project.GetHashCode()))
            {
                _ProjectGUID.Add(Project.GetHashCode(), new Dictionary<VisualStudioProject, string>());
            }
            if (!_ProjectGUID[Project.GetHashCode()].ContainsKey(Type))
            {
                _ProjectGUID[Project.GetHashCode()].Add(Type, Fact.Internal.Information.GenerateUUID());
            }
            return _ProjectGUID[Project.GetHashCode()][Type];
        }

        bool Contains_C_CPP_Sources(Fact.Processing.Project Project)
        {
            return Project.GetFiles("", true, Fact.Processing.File.FileType.CHeader,
                                              Fact.Processing.File.FileType.CPPHeader,
                                              Fact.Processing.File.FileType.CSource,
                                              Fact.Processing.File.FileType.CPPSource).Count > 0;
        }

        bool Contains_CS_Sources(Fact.Processing.Project Project)
        {
            return Project.GetFiles("", true, Fact.Processing.File.FileType.CSSource).Count > 0;
        }

        string GetGUIDForProjectType(VisualStudioProject VisualStudioProject)
        {
            switch (VisualStudioProject)
            {
                case VS.VisualStudioProject.CSharp: return "{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}";
                case VS.VisualStudioProject.CPlusPlus: return "{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}";
                case VS.VisualStudioProject.VisualBasic: return "{F184B08F-C81C-45F6-A57F-5ABD9991F28F}";
                case VS.VisualStudioProject.FSharp: return "{F2A71F9B-5D33-465A-A702-920D77279786}";
                case VS.VisualStudioProject.JSharp: return "{E6FDF86B-F3D1-11D4-8576-0002A516ECE8}";
            }
            return "{00000000-0000-0000-0000-000000000000}";
        }

        string GetOutputDirectory(VisualStudioProject Type, Fact.Processing.Project Project, string Target, string Platform)
        {
            return "bin/" + Target.Replace(" ", "_") + "/" + Platform.Replace(" ", "_") + "/";
        }

        List<string> GetTargetNames(VisualStudioProject Type, Fact.Processing.Project Project)
        {
            List<string> target = new List<string>();
            target.Add("Debug");
            target.Add("Release");
            return target;
        }

        List<string> GetPlatformNames(VisualStudioProject Type, Fact.Processing.Project Project, string Target)
        {
            List<string> platforms = new List<string>();
            switch (Type)
            {
                case VisualStudioProject.CPlusPlus:
                    platforms.Add("Win32");
                    break;
                case VisualStudioProject.FSharp:
                case VisualStudioProject.JSharp:
                case VisualStudioProject.CSharp:
                    platforms.Add("AnyCPU");
                    break;
            }
            return platforms;
        }


        void GenerateMsbuild(VisualStudioProject Type, Fact.Processing.Project Project, string Name)
        {
            List<string> builder = new List<string>();
            builder.Add("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            builder.Add("<Project ToolsVersion=\"" + Version + "\" DefaultTargets=\"Build\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">");
            builder.Add("  <PropertyGroup Label=\"Globals\">");
            builder.Add("    <ProjectGuid>" + GetProjectGUID(Type, Project) + "</ProjectGuid>");
            builder.Add("  </PropertyGroup>");

            if (Type == VisualStudioProject.CSharp)
            {
                foreach (string target in GetTargetNames(Type, Project))
                {
                    foreach (string platform in GetPlatformNames(Type, Project, target))
                    {
                        builder.Add("  <PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == '" + target + "|" + platform + "' \">");
                        builder.Add("      <ErrorReport>prompt</ErrorReport>");
                        builder.Add("      <WarningLevel>4</WarningLevel>");
                        builder.Add("      <OutputPath>" + GetOutputDirectory(Type, Project, target, platform).Replace("/", "\\") + "</OutputPath>");
                        builder.Add("  </PropertyGroup>");

                    }
                }
                builder.Add("  <ItemGroup>");
                foreach (Fact.Processing.File file in Project.GetFiles("", true, Fact.Processing.File.FileType.CSSource))
                {
                    string path = file.Directory + "/" + file.Name;
                    while (path.StartsWith("/")) { path = path.Substring(1); }
                    builder.Add("    <Compile Include=\"" + path.Replace('/', '\\') + "\" />");
                }
                builder.Add("  </ItemGroup>");
                builder.Add("<Import Project=\"$(MSBuildToolsPath)\\Microsoft.CSharp.targets\" />");
            }
            if (Type == VisualStudioProject.CPlusPlus)
            {
                builder.Add("  <ItemGroup Label=\"ProjectConfigurations\">");
                foreach (string target in GetTargetNames(Type, Project))
                {
                    foreach (string platform in GetPlatformNames(Type, Project, target))
                    {
                        builder.Add("    <ProjectConfiguration Include=\"" + target + "|" + platform + "\">");
                        builder.Add("      <Configuration>" + target + "</Configuration>");
                        builder.Add("      <Platform>" + platform + "</Platform>");
                        builder.Add("    </ProjectConfiguration>");
                    }
                }
                builder.Add("  </ItemGroup>");

                builder.Add("  <ItemGroup>");
                foreach (Fact.Processing.File file in Project.GetFiles("", true, Fact.Processing.File.FileType.CPPSource, Fact.Processing.File.FileType.CSource))
                {
                    string path = file.Directory + "/" + file.Name;
                    while (path.StartsWith("/")) { path = path.Substring(1); }
                    builder.Add("    <ClCompile Include=\"" + path.Replace('/', '\\') + "\" />");
                }
                builder.Add("  </ItemGroup>");
            }
            builder.Add("</Project>");

            Project.RemoveFile(Name);
            Project.AddFile(new Fact.Processing.File(builder.ToArray(), Name));
        }

        string FindMSBuildName(VisualStudioProject Type, Fact.Processing.Project Project)
        {
            string ext = ".proj";
            switch (Type)
            {
                case VisualStudioProject.CSharp: ext = ".csproj"; break;
                case VisualStudioProject.CPlusPlus: ext = ".vcxproj"; break;
                case VisualStudioProject.VisualBasic: ext = ".vbproj"; break;
            }
            return FindProjectName(Type, Project) + ext;
        }

        Dictionary<string, string> _ProjectName = new Dictionary<string, string>();

        string FindProjectName(VisualStudioProject Type, Fact.Processing.Project Project)
        {
            string guid = GetProjectGUID(Type, Project);
            if (!_ProjectName.ContainsKey(guid))
            {
                if (!_ProjectName.ContainsValue(Project.Name))
                {
                    _ProjectName.Add(guid, Project.Name);
                }
                else
                {
                    _ProjectName.Add(guid, Project.Name + "_" + Type.ToString());
                }
            }
            return _ProjectName[guid];
        }

        void GenerateSln(Fact.Processing.Project Project)
        {
            List<string> builder = new List<string>();
            builder.Add("Microsoft Visual Studio Solution File, Format Version " + Version);
            builder.Add("# SLN Generated by Fact " + Fact.Internal.Information.FactVersion().Major);
            builder.Add("VisualStudioVersion = 12");
            builder.Add("MinimumVisualStudioVersion = 10");

            foreach (VisualStudioProject type in GetProjectType(Project))
            {

                builder.Add("Project(\"" + GetGUIDForProjectType(type) + "\") = \"" + FindProjectName(type, Project) + "\", \"" + FindMSBuildName(type, Project) + "\", \"" + GetProjectGUID(type, Project) + "\"");
                builder.Add("EndProject");
                GenerateMsbuild(type, Project, FindMSBuildName(type, Project));
            }
            builder.Add("Global");
            builder.Add("  GlobalSection(SolutionConfigurationPlatform) = preSolution");
            builder.Add("    Debug|Any CPU = Debug|Any CPU");
            builder.Add("    Release|Any CPU = Release|Any CPU");
            builder.Add("  EndGlobalSection");
            builder.Add("  GlobalSection(ProjectConfigurationPlatforms) = postSolution");
            builder.Add("  EndGlobalSection");
            builder.Add("  GlobalSection(SolutionProperties) = preSolution");
            builder.Add("    HideSolution = FALSE");
            builder.Add("  EndGlobalSection");
            builder.Add("EndGlobal");

            Project.RemoveFile(Project.Name + "_" + Name + ".sln");
            Project.AddFile(new Fact.Processing.File(builder.ToArray(), Project.Name + "_" + Name + ".sln"));
        }

        public override void Configure(Fact.Processing.Project Project)
        {
            GenerateSln(Project);
        }
    }
}

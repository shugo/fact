﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualStudio
{
    public class VS2013 : VS
    {
        public VS2013() : base() { }
        public override bool Accessible { get { return true; } }
        public override string Version { get { return "12.0"; } }
    }
}

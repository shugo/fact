﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buildfarm
{
    public class Buildfarm : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Buildfarm";
            }
        }

        public override int Run(params string[] args)
        {

            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("server", "The address of the fact server hosting the buildfarm manager", "");
            CommandLine.AddStringInput("credential", "The credentials to use to reach the fact server", "");

            CommandLine.AddBooleanInput("list-projects", "List all the projects on the server", false);
            CommandLine.AddStringInput("list-project-versions", "List all the versions of a project on the server", "");
            CommandLine.AddStringInput("list-project-branches", "List all the branches of a project on the server", "");
            CommandLine.AddStringInput("list-submissions", "List all the submission of a project/version/branch on the server", "");



            CommandLine.AddStringInput("add-project", "Add a new project on the server", "");
            CommandLine.AddStringInput("add-project-version", "Add a new project version on the server (projectname:versionname)", "");
            CommandLine.AddStringInput("add-project-branch", "Add a new project branch on the server (projectname:branchname)", "");
            CommandLine.AddStringInput("attach-branch-to-version", "Attach a specific branch to a version (projectname:versionname:branchname)", "");
            CommandLine.AddStringInput("submit-package", "submit a specific package in the buildfarm (localprojectfile:projectname:versionname:branchname)", ""); CommandLine.AddShortInput("s", "submit-package");
            CommandLine.AddStringInput("submit-package-built", "submit a build for specific submission in the buildfarm (id:projectname:versionname:branchname)", "");
            CommandLine.AddStringInput("submit-package-tested", "submit a tested project for specific submission in the buildfarm (tested:projectname:versionname:branchname)", "");
            CommandLine.AddStringInput("get-package", "get a specific package in the buildfarm (localprojectfile:projectname:versionname:branchname)", ""); CommandLine.AddShortInput("g", "get-package");
            CommandLine.AddStringInput("get-package-built", "get a build for specific submission in the buildfarm (id:projectname:versionname:branchname)", "");
            CommandLine.AddStringInput("get-package-tested", "get a tested project for specific submission in the buildfarm (tested:projectname:versionname:branchname)", "");

            CommandLine.AddStringInput("output", "when used with get-package-* specify the location where the package will be saved", ""); CommandLine.AddShortInput("o", "output");

            CommandLine.AddStringInput("set-storage-location", "Set where the server should store the projects", "");

            CommandLine.AddBooleanInput("help", "Display this message", false); CommandLine.AddShortInput("h", "help");

            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

            string _Server = CommandLine["server"].AsString();
            if (!_Server.ToLower().StartsWith("http://") &&
                !_Server.ToLower().StartsWith("https://"))
            {
                _Server = "http://" + _Server;
            }
            string _CredentialFile = CommandLine["credential"].IsDefault ? "" : CommandLine["credential"];
            Fact.Directory.Credential _Credential = null;
            if (_CredentialFile != "")
            {
                try
                {
                    _Credential = new Fact.Directory.Credential(_CredentialFile);
                }
                catch { Fact.Log.Error("Impossible to load the credential"); return 1; }
            }

            Fact.Network.Client Client = null;
            string Username = "";
            string Password = "";
            if (_Credential == null)
            {
                Fact.Log.Login(out Username, out Password);
                Client = new Fact.Network.Client(_Server, Username, Password);
            }
            else
            {
                Client = new Fact.Network.Client(_Server, _Credential);
            }
            if (CommandLine["list-projects"].AsBoolean()) 
            {
                try
                {
                    Fact.Log.Info("Projects:");
                    foreach (string project in Client.Call("BuildfarmManager.GetProjects") as string[])
                    {
                        Fact.Log.Info(project);
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["list-project-versions"].AsString().Trim() != "")
            {
                try
                {
                    string ProjectName = CommandLine["list-project-versions"].AsString().Trim();
                    Fact.Log.Info("Versions in project " + ProjectName + ":");
                    foreach (string version in Client.Call("BuildfarmManager.GetProjectVersions", ProjectName) as string[])
                    {
                        Fact.Log.Info(version);
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["list-project-branches"].AsString().Trim() != "")
            {
                try
                {
                    string ProjectName = CommandLine["list-project-branches"].AsString().Trim();
                    Fact.Log.Info("Branches in project " + ProjectName + ":");
                    foreach (string version in Client.Call("BuildfarmManager.GetProjectBranches", ProjectName) as string[])
                    {
                        Fact.Log.Info(version);
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["list-submissions"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["list-submissions"].AsString().Trim().Split(':', '/');
                    string Project = "", Version = "", Branch = "";
                    if (Command.Length > 0) { Project = Command[0]; }
                    if (Command.Length > 1) { Version = Command[1]; }
                    if (Command.Length > 2) { Branch = Command[2]; }
                    Fact.Log.Info("Submissions in " + Project + ":" + Version + ":" + Branch);
                    foreach (string version in Client.Call("BuildfarmManager.GetSubmissionIds", Project, Version, Branch) as string[])
                    {
                        Fact.Log.Info(version);
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["add-project"].AsString().Trim() != "")
            {
                try
                {
                    string ProjectName = CommandLine["add-project"].AsString().Trim();
                    Fact.Log.Verbose("Create project " + ProjectName + "...");
                    if (!(bool)Client.Call("BuildfarmManager.CreateProject", ProjectName))
                    { Fact.Log.Error("The server has rejected the creation of the new project " + ProjectName); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["add-project-version"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["add-project-version"].AsString().Trim().Split(':', '/');
                    string Project = "", Version = "";
                    if (Command.Length > 0) { Project = Command[0]; }
                    if (Command.Length > 1) { Version = Command[1]; }

                    Fact.Log.Verbose("Create version " + Version + " in project " + Project + "...");
                    if (!(bool)Client.Call("BuildfarmManager.CreateProjectVersion", Project, Version))
                    { Fact.Log.Error("The server has rejected the creation of the new version " + Version + " in project " + Project); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["add-project-branch"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["add-project-branch"].AsString().Trim().Split(':', '/');
                    string Project = "", Branch = "";
                    if (Command.Length > 0) { Project = Command[0]; }
                    if (Command.Length > 1) { Branch = Command[1]; }

                    Fact.Log.Verbose("Create branch " + Branch + " in project " + Project + "...");
                    if (!(bool)Client.Call("BuildfarmManager.CreateProjectBranch", Project, Branch))
                    { Fact.Log.Error("The server has rejected the creation of the new branch " + Branch + " in project " + Project); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["attach-branch-to-version"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["attach-branch-to-version"].AsString().Trim().Split(':', '/');
                    string Project = "", Version = "", Branch = "";
                    if (Command.Length > 0) { Project = Command[0]; }
                    if (Command.Length > 1) { Version = Command[1]; }
                    if (Command.Length > 2) { Branch = Command[2]; }


                    Fact.Log.Verbose("Attach branch " + Branch + " to version " + Version + " in project " + Project + "...");
                    if (!(bool)Client.Call("BuildfarmManager.AttachBranchToVersion", Project, Version, Branch))
                    { Fact.Log.Error("The server has rejected the attach request of the branch " + Branch + " to the version " + Version + " in project " + Project); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }
            }
            if (CommandLine["submit-package"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["submit-package"].AsString().Trim().Split(':');
                    string Project = "", Version = "", Branch = "", LocalFile = "";
                    if (Command.Length > 0) { LocalFile = Command[0]; }
                    if (Command.Length > 1) { Project = Command[1]; }
                    if (Command.Length > 2) { Version = Command[2]; }
                    if (Command.Length > 3) { Branch = Command[3]; }
                    Fact.Processing.Project FactProject = null;
                    try
                    {
                        FactProject = new Fact.Processing.Project("");
                        FactProject.Load(LocalFile);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to load the package '" + LocalFile + "'");
                        return 1;
                    }

                    Fact.Log.Verbose("Submit package " + FactProject.Name + " in branch " + Branch + "  version " + Version + " project " + Project + "...");
                    if (!(bool)Client.Call("BuildfarmManager.Submit", Project, Version, Branch, FactProject))
                    { Fact.Log.Error("The server has rejected the submission"); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }

            }
            if (CommandLine["get-package"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["get-package"].AsString().Trim().Split(':');
                    string Project = "", Version = "", Branch = "", File = "";
                    string output = CommandLine["output"].AsString().Trim();
                    if (output == "")
                    {
                        Fact.Log.Error("No output specified");
                        return 1;
                    }
                    if (Command.Length > 0) { File = Command[0]; }
                    if (Command.Length > 1) { Project = Command[1]; }
                    if (Command.Length > 2) { Version = Command[2]; }
                    if (Command.Length > 3) { Branch = Command[3]; }
                    Fact.Processing.Project FactProject = null;
                    Fact.Log.Verbose("get package " + File + " in branch " + Branch + "  version " + Version + " project " + Project + "...");
                    FactProject = (Fact.Processing.Project)Client.Call("BuildfarmManager.GetSubmission", Project, Version, Branch, File);
                    if (FactProject == null)
                    { Fact.Log.Error("The server has rejected the download"); return 1; }
                    FactProject.Save(output);
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }

            }
            if (CommandLine["submit-package-built"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["submit-package-built"].AsString().Trim().Split(':');
                    string Project = "", Version = "", Branch = "", LocalFile = "";
                    if (Command.Length > 0) { LocalFile = Command[0]; }
                    if (Command.Length > 1) { Project = Command[1]; }
                    if (Command.Length > 2) { Version = Command[2]; }
                    if (Command.Length > 3) { Branch = Command[3]; }
                    Fact.Processing.Project FactProject = null;
                    try
                    {
                        FactProject = new Fact.Processing.Project("");
                        FactProject.Load(LocalFile);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to load the package '" + LocalFile + "'");
                        return 1;
                    }

                    Fact.Log.Verbose("Submit package built " + FactProject.Name + " in branch " + Branch + "  version " + Version + " project " + Project + "...");
                    if (!(bool)Client.Call("BuildfarmManager.SubmitBuilt", Project, Version, Branch, FactProject))
                    { Fact.Log.Error("The server has rejected the submission"); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }

            }

            if (CommandLine["submit-package-tested"].AsString().Trim() != "")
            {
                try
                {
                    string[] Command = CommandLine["submit-package-tested"].AsString().Trim().Split(':');
                    string Project = "", Version = "", Branch = "", LocalFile = "";
                    if (Command.Length > 0) { LocalFile = Command[0]; }
                    if (Command.Length > 1) { Project = Command[1]; }
                    if (Command.Length > 2) { Version = Command[2]; }
                    if (Command.Length > 3) { Branch = Command[3]; }
                    Fact.Processing.Project FactProject = null;
                    try
                    {
                        FactProject = new Fact.Processing.Project("");
                        FactProject.Load(LocalFile);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to load the package '" + LocalFile + "'");
                        return 1;
                    }

                    Fact.Log.Verbose("Submit package tested " + FactProject.Name + " in branch " + Branch + "  version " + Version + " project " + Project + "...");
                    if (!(bool)Client.Call("BuildfarmManager.SubmitTested", Project, Version, Branch, FactProject))
                    { Fact.Log.Error("The server has rejected the submission"); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }

            }

            if (CommandLine["set-storage-location"].AsString().Trim() != "")
            {
                try
                {
                    string location = CommandLine["set-storage-location"].AsString().Trim();
                    if (!(bool)Client.Call("BuildfarmManager.SetStorageLocation", location))
                    { Fact.Log.Error("The server has rejected the update"); return 1; }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Transmission error " + e.Message);
                    return 1;
                }

            }
            return 0;
        }
    }
}

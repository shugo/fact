﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadService
{
    public class Administration : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Administration";
            }
        }

        int LoadService(Fact.Network.Client Client, string Service)
        {
            if (!System.IO.File.Exists(Service)) { Fact.Log.Error("The service file does not exist"); return 1; }

            Fact.Processing.File file = null;
            try
            {
                file = new Fact.Processing.File(System.IO.File.ReadAllBytes(Service), System.IO.Path.GetFileName(Service));
            }
            catch { }

            object result = Client.Call("AdminService.LoadServiceFromFile", file);
            if (result == null || !(result is bool) || !((bool)result)) 
            {
                Fact.Log.Error("The server has rejected the transaction");
                return 1;
            }
            Fact.Log.Verbose("Service loaded");
            return 0;
        }

        int CreateUser(Fact.Network.Client Client, string Username, string Password)
        {
            if (Username == "") { Fact.Log.Error("Invalid Username"); return 1; }

            object result = Client.Call("AdminService.CreateUser", Username, Password);
            if (result == null || !(result is bool) || !((bool)result)) 
            {
                Fact.Log.Error("The server has rejected the transaction");
                return 1;
            }
            Fact.Log.Verbose("User " + Username + " created");
            return 0;
        }

        int ListActions(Fact.Network.Client Client)
        {
            object result = Client.Call("AdminService.ListActions");
            if (result == null || !(result is string[]))
            {
                Fact.Log.Error("The server has rejected the transaction");
                return 1;
            }

            string[] actions = (result as string[]);

            Console.WriteLine("ACTION");
            Fact.Log.Terminal.DoubleLine();
            for (int n = 0; n < actions.Length; n++)
            {
                Console.WriteLine(actions[n]);
            }
            return 0;
        }

        int AddAllowedUserForAction(Fact.Network.Client Client, string Action, string Username)
        {
            if (Username == "") { Fact.Log.Error("Invalid Username"); return 1; }

            object result = Client.Call("AdminService.AddAllowedUserForAction", Action, Username);
            if (result == null || !(result is bool) || !((bool)result))
            {
                Fact.Log.Error("The server has rejected the transaction");
                return 1;
            }
            Fact.Log.Verbose("User " + Username + " is allowed to call " + Action);
            return 0;
        }

        int DeleteUser(Fact.Network.Client Client, string Username)
        {
            if (Username == "") { Fact.Log.Error("Invalid Username"); return 1; }

            object result = Client.Call("AdminService.DeleteUser", Username);
            if (result == null || !(result is bool) || !((bool)result)) 
            {
                Fact.Log.Error("The server has rejected the transaction");
                return 1;
            }
            Fact.Log.Verbose("User " + Username + " deleted");
            return 0;
        }

        int CreateUserCredential(Fact.Network.Client Client, string Username, string ExpirationDate, string Output)
        {
            DateTime ExpirationDateParsed;
            if (!DateTime.TryParse(ExpirationDate, out ExpirationDateParsed)) { Fact.Log.Error("Invalid date"); return 1; }
            if (Username == "") { Fact.Log.Error("Invalid Username"); return 1; }
            if (Output == "") { Fact.Log.Error("Invalid Output file"); return 1; }

            object result = Client.Call("AdminService.CreateUserCredential", Username, ExpirationDateParsed);
            if (result == null || !(result is Fact.Processing.File)) 
            {
                Fact.Log.Error("The server has rejected the transaction");
                return 1;
            }
            (result as Fact.Processing.File).Extract(Output);
            return 0;
        }

        public override int Run(params string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("action", "(CreateUser, DeleteUser, CreateUserCredential, LoadService, AddAllowedUserForAction, ListActions)", ""); CommandLine.AddShortInput("a", "action");
            CommandLine.AddStringInput("service", "The file (a .dll) containing the service(s)", "");
            CommandLine.AddStringInput("server", "The address of the server", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddStringInput("credential", "The credential to use to communicate with the server", ""); CommandLine.AddShortInput("c", "credential");
            CommandLine.AddStringInput("username", "The name of the target user (not your username)", ""); CommandLine.AddShortInput("u", "username");
            CommandLine.AddStringInput("password", "The password of the target user (not your password)", ""); CommandLine.AddShortInput("p", "password");
            CommandLine.AddStringInput("action-name", "The name of the target action (not the action to run)", "");
            CommandLine.AddStringInput("expirationdate", "the expiration date that must be used", ""); CommandLine.AddShortInput("e", "expirationdate");
            CommandLine.AddStringInput("output", "when use with CreateUserCredential it specifies the output file", ""); CommandLine.AddShortInput("o", "output");

            CommandLine.AddBooleanInput("help", "Display this message", false); CommandLine.AddShortInput("h", "help");
            CommandLine.Parse(args);
            if (CommandLine.Errors.Count > 0) 
            {
                foreach (string error in CommandLine.Errors)
                { Fact.Log.Error(error); }
                return 1;
            }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 1; }

            Fact.Network.Client client = null;
            try
            { client = Fact.Common.Remote.Connection.Connect(CommandLine["server"].AsString(), CommandLine["credential"].AsString().Trim()); }
            catch (Exception e)
            {
                Fact.Log.Error(e.Message);
                return 1;
            }

            try
            {
                switch(CommandLine["action"].AsString().ToLower())
                {
                    case "createuser": return CreateUser(client, CommandLine["username"], CommandLine["password"]);
                    case "deleteuser": return DeleteUser(client, CommandLine["username"]);
                    case "createusercredential": return CreateUserCredential(
                                client,
                                CommandLine["username"].AsString(),
                                CommandLine["expirationdate"].AsString(),
                                CommandLine["output"].AsString());
                    case "loadservice": return LoadService(client, CommandLine["service"].AsString());
                    case "addalloweduserforaction": return AddAllowedUserForAction(client, CommandLine["action-name"].AsString(), CommandLine["username"].AsString());
                    case "listactions": return ListActions(client);
                    default: Fact.Log.Error("Invalid Action"); return 1;
                }
            }
            catch (Exception e)
            {
                Fact.Log.Error("Transmission error: " + e.Message);
                return 1;
            }
            return 0;
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentManager
{
    public class AgentManager : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "AgentManager";
            }
        }
        public AgentManager() { }

        bool Shudown(Fact.Network.Client Client, string Agent)
        {
            try
            {
                Fact.Log.Verbose("Sending the shutdown command to the agent...");
                int conflictNameCount = 0;
                string agentID = Agent;
                {
                    string[] list = Client.Call("AgentManager.GetAgentList") as string[];
                    if (list != null)
                    {
                        foreach (string id in list)
                        {
                            string agentname = Client.Call("AgentManager.GetAgentName", id) as string;
                            if (agentname == Agent)
                            {
                                conflictNameCount++;
                                agentID = id;
                            }
                        }
                    }
                }

                if (conflictNameCount > 1)
                {
                    Fact.Log.Error("The name " + Agent + " applies to " + conflictNameCount + " agents");
                    Fact.Log.Error("Please the the complete agent ID instead");
                    return false;
                }

                string targetAgentName = Client.Call("AgentManager.GetAgentName", agentID) as string; ;

                int job = (int)Client.Call("AgentManager.CreateJobOnAgent", agentID);
                Fact.Log.Verbose("Sending the shutdown command to the agent...");

                if (job < 0) { Fact.Log.Error("Impossible to target the agent: " + agentID); return false; }
                if (!(bool)Client.Call("AgentManager.PushAction_Shutdown", job))
                {
                    Fact.Log.Error("Impossible to send the shutdown instruction");
                    return false;
                }
                if (!(bool)Client.Call("AgentManager.PublishJob", job))
                {
                    Fact.Log.Error("Impossible to publish the shutdown instruction");
                    return false;
                }
                Fact.Log.Verbose("Waiting for the agent to shutdown ...");
                while (true)
                {
                    string[] list = Client.Call("AgentManager.GetAgentList") as string[];
                    if (list != null)
                    {
                        bool present = false;
                        foreach (string agent in list)
                        {
                            if (agent.Trim() == agentID.Trim()) { present = true; }
                        }
                        if (!present) { break; }
                    }
                    else
                    {
                        Fact.Log.Error("Error while waiting for the agent to shutdown");
                        return false;
                    }
                    System.Threading.Thread.Sleep(100);
                }
                Fact.Log.Verbose("The agent " + targetAgentName + " is no longer alive");
                return true;
            }
            catch
            {
                Fact.Log.Error("Transmission error ...");
                return false;
            }
        }

        string Pad(string Str, int Value)
        {
            while (Str.Length < Value) { Str += " "; }
            return Str;
        }

        bool ListJobs(Fact.Network.Client Client)
        {
            try
            {
                List<string> names = new List<string>();
                List<string> status = new List<string>();
                List<string> users = new List<string>();

                int[] list = Client.Call("AgentManager.GetJobList") as int[];
                if (list != null)
                {
                    foreach (int job in list)
                    {
                        string jobName = Client.Call("AgentManager.GetJobName", job) as string;
                        string jobStatus = Client.Call("AgentManager.GetJobStatus", job) as string;

                        names.Add(jobName);
                        status.Add(jobStatus);
                        users.Add("");
                    }
                    int NameLength = "NAME".Length + 1;
                    int StatusLength = "STATUS".Length + 1;
                    int UserLength = "USER".Length + 1;


                    foreach (string name in names) { if (name.Length + 1 > NameLength) { NameLength = name.Length + 1; } }
                    foreach (string stat in status) { if (stat.Length + 1 > StatusLength) { StatusLength = stat.Length + 1; } }
                    foreach (string user in users) { if (user.Length + 1 > UserLength) { UserLength = user.Length + 1; } }

                    string header = "";
                    header += "NAME"; header = Pad(header, NameLength);
                    header += "STATUS"; header = Pad(header, NameLength + StatusLength);
                    header += "USER"; header = Pad(header, NameLength + StatusLength + UserLength);
                    Console.WriteLine(header);
                    Fact.Log.Terminal.Line();
                    for (int n = 0; n < names.Count; n++)
                    {
                        string line = "";
                        line += names[n]; line = Pad(line, NameLength);
                        line += status[n]; line = Pad(line, NameLength + StatusLength);
                        line += users[n]; line = Pad(line, NameLength + StatusLength + UserLength);
                        Console.WriteLine(line);
                    }

                }
                return true;
            }
            catch
            {
                Fact.Log.Error("Transmission error ...");
                return false;
            }
        }


        bool ListAgents(Fact.Network.Client Client)
        {
            try
            {
                List<string> names = new List<string>();
                List<string> groups = new List<string>();
                List<string> users = new List<string>();

                string[] list = Client.Call("AgentManager.GetAgentList") as string[];
                if (list != null)
                {
                    foreach (string agent in list)
                    {
                        if (agent == "") { break; }

                        bool xmlmode = false;
                        if (Fact.Log.Terminal.Width < 10) { xmlmode = true; }

                        string hostaddress = Client.Call("AgentManager.GetAgentHostAddress", agent) as string;
                        string userid = Client.Call("AgentManager.GetAgentUserId", agent) as string;
                        string agentname = Client.Call("AgentManager.GetAgentName", agent) as string;
                        names.Add(agentname);
                        groups.Add("");
                        users.Add(userid);
                    }
                    int NameLength = "NAME".Length + 1;
                    int GroupLength = "GROUP".Length + 1;
                    int UserLength = "USER".Length + 1;


                    foreach (string name in names) { if (name.Length + 1 > NameLength) { NameLength = name.Length + 1; } }
                    foreach (string group in groups) { if (group.Length + 1 > GroupLength) { GroupLength = group.Length + 1; } }
                    foreach (string user in users) { if (user.Length + 1 > UserLength) { UserLength = user.Length + 1; } }

                    string header = "";
                    header += "NAME"; header = Pad(header, NameLength);
                    header += "GROUP"; header = Pad(header, NameLength + GroupLength);
                    header += "USER"; header = Pad(header, NameLength + GroupLength + UserLength);
                    Console.WriteLine(header);
                    Fact.Log.Terminal.Line();
                    for (int n = 0; n < names.Count; n++)
                    {
                        string line = "";
                        line += names[n]; line = Pad(line, NameLength);
                        line += groups[n]; line = Pad(line, NameLength + GroupLength);
                        line += users[n]; line = Pad(line, NameLength + GroupLength + UserLength);
                        Console.WriteLine(line);

                    }

                }
                return true;
            }
            catch
            {
                Fact.Log.Error("Transmission error ...");
                return false;
            }
        }

        public override int Run(params string[] args)
        {

            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("server", "The address of the fact server hosting the agent manager", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddStringInput("credential", "The credentials to use to reach the fact server", ""); CommandLine.AddShortInput("c", "credential");
            CommandLine.AddBooleanInput("list-agents", "List all the agents on the server", false);
            CommandLine.AddBooleanInput("list-jobs", "List all the agents on the server", false);
            CommandLine.AddStringInput("shutdown", "Shutdown the specified agent", "");

            CommandLine.AddBooleanInput("help", "Display this message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

            Fact.Network.Client client = null;
            try
            { client = Fact.Common.Remote.Connection.Connect(CommandLine["server"].AsString(), CommandLine["credential"].AsString().Trim()); }
            catch (Exception e)
            {
                Fact.Log.Error(e.Message);
                return 1;
            }

            if (CommandLine["shutdown"].AsString().Trim() != "")
            {
                if (!Shudown(client, CommandLine["shutdown"].AsString()))
                {
                    return 1;
                }
                return 0;
            }


            if (CommandLine["list-agents"].AsBoolean())
            {
                if (!ListAgents(client))
                {
                    return 1;
                }
                return 0;
            }

            if (CommandLine["list-jobs"].AsBoolean())
            {
                if (!ListJobs(client))
                {
                    return 1;
                }
                return 0;
            }

            return 0;
        }
    }
}

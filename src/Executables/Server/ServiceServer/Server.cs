﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceServer
{
    public class ServicesServer : Fact.Plugin.Plugin
    {
        class UserService : Fact.Network.Service
        {
            ServicesServer _Server = null;
            bool _Verbose = false;
            public UserService(ServicesServer Server, bool Verbose)
                : base("UserService")
            {
                _Verbose = Verbose;
                _Server = Server;
            }
            [Fact.Network.ServiceExposedMethod]
            public string GetUsername(Fact.Network.Service.RequestInfo Info)
            {
                return Info.User.Login;
            }
        }
        class AdminService : Fact.Network.Service
        {
            string _TempStorage = "";
            string _Journal = "";
            volatile bool _MustUpdateJournal = false;
            volatile int _CyclSinceLastJournalUpdate = 0;

            ServicesServer _Server = null;
            bool _Verbose = false;
            public AdminService(ServicesServer Server, string Journal, bool Verbose)
                : base("AdminService")
            {
                _Verbose = Verbose;
                _Server = Server;
                Fact.Log.Verbose("Creating server local storage ...");
                try { _TempStorage = Fact.Tools.CreateTempDirectory(); }
                catch { }
                if (_TempStorage == "" || !System.IO.Directory.Exists(_TempStorage))
                {
                    Fact.Log.Error("Impossible to create the local storage");
                    Fact.Log.Error("Check permissions and disk space");
                    Fact.Log.Warning("server will now operate in damaged mode");
                }
                if (Journal != "")
                {
                    _Journal = Journal;
                    if (System.IO.File.Exists(Journal))
                    {
                        lock (this)
                        {
                            System.IO.FileStream stream = System.IO.File.Open(Journal, System.IO.FileMode.OpenOrCreate);
                            LoadJournal(stream);
                            stream.Close();
                            if (_Verbose) { Fact.Log.Verbose("Previous data loaded from the journal file"); }
                        }
                    }
                    // The journal must be updated every 5 minutes
                    Fact.Threading.Job.CreateTimer((ref Fact.Threading.Job.Status status) => { UpdateJournal(); }, 1000 * 10, true);
                }
            }

            void GetGroupAndUser(Fact.Directory.Group Group, string Path, out Fact.Directory.Group ParentGroup, out Fact.Directory.User User)
            {
                GetGroupAndUser(Group, Path.Split('/'), out ParentGroup, out User);
            }

            void GetGroupAndUser(Fact.Directory.Group Group, string[] Path, out Fact.Directory.Group ParentGroup, out Fact.Directory.User User)
            {
                if (Path.Length == 1)
                {
                    ParentGroup = Group;
                    if (!Group.GetUsers().TryGetValue(Path[Path.Length - 1], out User)) { User = null; }
                    return;
                }
                ParentGroup = null;
                User = null;
                for (int n = 0; n < Path.Length - 1; n++)
                {
                    if (Path[n] == "") { continue; }
                    if (!Group.GetGroups().TryGetValue(Path[n], out Group)) { return; }
                }
                ParentGroup = Group;
                if (!Group.GetUsers().TryGetValue(Path[Path.Length - 1], out User)) { User = null; }
            }
            Fact.Directory.Group CreateGroupForUser(Fact.Directory.Group Group, string Path)
            {
                return CreateGroupForUser(Group, Path.Split('/'));
            }
            Fact.Directory.Group CreateGroupForUser(Fact.Directory.Group Group, string[] Path)
            {
                for (int n = 0; n < Path.Length - 1; n++)
                {
                    if (Path[n] == "") { continue; }
                    Fact.Directory.Group TempGroup = null;
                    if (!Group.GetGroups().TryGetValue(Path[n], out TempGroup))
                    {
                        // FIXME this code will except if some create th group at the same time
                        Fact.Directory.Group SubGroup = new Fact.Directory.Group(Path[n]);
                        Group.AddGroup(SubGroup);
                        if (_Verbose) { Fact.Log.Verbose("Create Group " + Path[n]); }
                        Group = SubGroup;
                    }
                    else
                    {
                        Group = TempGroup;
                    }
                }
                return Group;
            }

            void LoadJournal(System.IO.Stream Stream)
            {
                if (Stream == null) { return; }
                _Server._Server.AllowedUsers.Load(Stream);
            }

            void SaveJournal(System.IO.Stream Stream)
            {
                if (Stream == null) { return; }
                _Server._Server.AllowedUsers.Save(Stream);   
            }

            void UpdateJournal()
            {
                _CyclSinceLastJournalUpdate++;
                if (_Journal == "") { return; }
                if (!_MustUpdateJournal && _CyclSinceLastJournalUpdate <= 360) { return; }
                if (_Verbose) { Fact.Log.Verbose("Start the update of the journal file"); }
                lock (this)
                {
                    _MustUpdateJournal = false;
                    _CyclSinceLastJournalUpdate = 0;
                    try
                    {
                        string name = Fact.Internal.Information.GetTempFileName();
                        if (System.IO.File.Exists(_TempStorage + "/" + name)) { return; }
                        System.IO.FileStream stream = System.IO.File.Open(_TempStorage + "/" + name, System.IO.FileMode.OpenOrCreate);
                        SaveJournal(stream);
                        stream.Close();
                        try
                        {
                            if (!System.IO.File.Exists(_Journal))
                            {
                                System.IO.File.Move(_TempStorage + "/" + name, _Journal);
                            }
                            else
                            {
                                try
                                {
                                    System.IO.File.Replace(_TempStorage + "/" + name, _Journal, _TempStorage + "/" + name + "_backup");
                                    Fact.Tools.RecursiveDelete(_TempStorage + "/" + name, _TempStorage + "/" + name + "_backup");
                                    if (_Verbose) { Fact.Log.Verbose("The journal file has been updated"); }
                                }
                                catch (Exception e1)
                                {
                                    Fact.Log.Warning("Impossible to replace the journal file: " + e1.Message);
                                    Fact.Log.Verbose("Try to delete and copy a new journal");
                                    try
                                    {
                                        Fact.Tools.RecursiveDelete(_Journal);
                                        System.IO.File.Move(_TempStorage + "/" + name, _Journal); 
                                        Fact.Tools.RecursiveDelete(_TempStorage + "/" + name);
                                        if (_Verbose) { Fact.Log.Verbose("The journal file has been updated"); }
                                    }
                                    catch (Exception e2)
                                    {
                                        Fact.Log.Warning("Impossible to delete and copy the journal file: " + e2.Message);
                                        Fact.Log.Verbose("Try to delete the old journal and create a new journal");

                                        try
                                        {
                                            Fact.Tools.RecursiveDelete(_Journal);
                                            System.IO.FileStream directstream = System.IO.File.Open(_Journal, System.IO.FileMode.OpenOrCreate);
                                            SaveJournal(directstream);
                                            directstream.Close();
                                            if (_Verbose) { Fact.Log.Verbose("The journal file has been updated"); }
                                        }
                                        catch (Exception e3)
                                        {
                                            Fact.Log.Error("Impossible to update the journal file: " + e3.Message);
                                            _MustUpdateJournal = true;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Fact.Log.Error("Impossible to update the journal file");
                        }
                    }
                    catch { }
                }
            }

            [Fact.Network.ServiceExposedMethod]
            public bool CreateUser(
                Fact.Network.Service.RequestInfo Info,
                string Username,
                string Password)
            {
                Fact.Directory.Group Group = null;
                Fact.Directory.User User = null;
                GetGroupAndUser(Info.Server.AllowedUsers, Username, out Group, out User);
                if (User != null) { return false; }
                if (Group == null) { Group = CreateGroupForUser(Info.Server.AllowedUsers, Username); }
                if (Group == null) { return false; }
                string[] Path = Username.Split('/');
                Username = Path[Path.Length - 1];
                if (Username.Trim() == "") { return false; }
                User = new Fact.Directory.User(Username, Password);
                Group.AddUser(User);
                if (_Verbose) { Fact.Log.Verbose("Create user " + Username); }
                _MustUpdateJournal = true;
                return true;
            }

            [Fact.Network.ServiceExposedMethod]
            public bool AddAllowedUserForAction(
                Fact.Network.Service.RequestInfo Info,
                string Action,
                string Username)
            {
                Fact.Directory.User User = Info.Server.AllowedUsers.GetUser(Username);
                if (User == null) { return false; }
                Dictionary<string, Fact.Directory.Group> allowedUser = Info.Server.ActionAllowedUser;
                lock (allowedUser)
                {
                    if (!allowedUser.ContainsKey(Action))
                    {
                        allowedUser.Add(Action, new Fact.Directory.Group());
                        _MustUpdateJournal = true;
                    }
                    try
                    {
                        allowedUser[Action].AddUser(User);
                    }
                    catch { }
                }
                if (_Verbose) { Fact.Log.Verbose("Allow user " + Username + " to call " + Action); }
                return true;
            }

            [Fact.Network.ServiceExposedMethod]
            public bool DeleteUser(
                Fact.Network.Service.RequestInfo Info,
                string Username)
            {
                if (Info.Server.AllowedUsers.RemoveUser(Username))
                {
                    if (_Verbose) { Fact.Log.Verbose("Delete user " + Username); }
                    _MustUpdateJournal = true;
                    return true;
                }
                else
                {
                    if (_Verbose) { Fact.Log.Error("Fail to delete user " + Username); }
                    return false;
                }
            }

            [Fact.Network.ServiceExposedMethod]
            public Fact.Directory.Directory GetServerDirectoryStripped(Fact.Network.Service.RequestInfo Info)
            {
                Fact.Directory.Directory Directory = new Fact.Directory.Directory();
                Directory.MainGroup = Info.Server.AllowedUsers.Copy(true);
                return Directory;
            }

            [Fact.Network.ServiceExposedMethod]
            public Fact.Processing.File CreateUserCredential(
                Fact.Network.Service.RequestInfo Info,
                string Username,
                DateTime Expiration)
            {
                Fact.Directory.User User = Info.Server.AllowedUsers.GetUser(Username);
                if (User == null) { Fact.Log.Error("User " + Username + " not found"); }
                Fact.Directory.Credential Credential = new Fact.Directory.Credential(User, Expiration);
                System.IO.MemoryStream Stream = new System.IO.MemoryStream();
                Credential.Save(Stream);
                if (_Verbose) { Fact.Log.Verbose("Credential created for user " + Username); }
                _MustUpdateJournal = true;
                return new Fact.Processing.File(Stream.ToArray(), User.Login + ".fc");
            }

            public bool LoadServiceFromFile(Fact.Processing.File File, Fact.Network.Server Server)
            {
                bool loaded = false;
                bool found = false;
                if (_TempStorage == "") { Fact.Log.Error("No space to store the service"); }
                try
                {
                    string serviceFileName = File.Name;
                    string serviceFullDir = File.ExtractAt(_TempStorage);
                    serviceFullDir = System.IO.Path.GetFullPath(serviceFullDir);
                    if (_Verbose) { Fact.Log.Verbose("Service file extracted at " + serviceFullDir); }

                    System.Reflection.Assembly Assembly = System.Reflection.Assembly.LoadFile(serviceFullDir);
                    if (_Verbose) { Fact.Log.Verbose("Assembly " + Assembly.FullName + " loaded"); }
                    foreach (System.Reflection.Module module in Assembly.GetModules())
                    {
                        foreach (System.Type type in module.GetTypes())
                        {
                            if (type.IsSubclassOf(typeof(Fact.Network.Service)))
                            {
                                found = true;
                                if (_Verbose) { Fact.Log.Verbose("Service " + type.Name + " found"); }
                                try
                                {
                                    System.Reflection.ConstructorInfo Constructor = type.GetConstructor(new Type[] { });
                                    if (Constructor == null) { break; }
                                    if (_Verbose) { Fact.Log.Verbose("Service " + type.Name + " constructor found"); }

                                    try
                                    {
                                        Fact.Network.Service ServiceObj = (Fact.Network.Service)Constructor.Invoke(new object[] { });
                                        if (_Verbose) { Fact.Log.Verbose("Service " + type.Name + " instanciated"); }
                                        Server.LoadService(ServiceObj);
                                        if (_Verbose) { Fact.Log.Verbose("Service " + type.Name + " loaded"); }
                                        loaded = true;
                                    }
                                    catch (Exception e)
                                    {
                                        if (_Verbose)
                                        {
                                            Fact.Log.Error("Impossible to instanciate service " + type.Name + ": " + e.Message);
                                        }
                                    }

                                }
                                catch { }
                            }
                        }
                    }
                    if (_Verbose)
                    {
                        if (!loaded && found) { Fact.Log.Error("A service has been found but the loading has failed : " + Assembly.FullName); }
                        else if (!loaded) { Fact.Log.Error("No valid service in the assembly: " + Assembly.FullName); }
                    }
                    return loaded;
                }
                catch (Exception e)
                {
                    if (_Verbose)
                    {
                        Fact.Log.Error("Impossible to load the service");
                        Fact.Log.Exception(e);
                    }
                    return false;
                }
            }

            [Fact.Network.ServiceExposedMethod]
            public bool LoadServiceFromFile(
                Fact.Network.Service.RequestInfo Info,
                Fact.Processing.File File)
            {
                return LoadServiceFromFile(File, Info.Server);
            }

            [Fact.Network.ServiceExposedMethod]
            public string[] ListActions(Fact.Network.Service.RequestInfo Info)
            {
                List<string> actions = new List<string>();
                foreach(KeyValuePair<string, System.Reflection.MethodInfo> minfo in Info.Server.ListActions())
                {
                    Fact.Log.Terminal.WriteLine(minfo.Key);
                    string prototype = "";
                    foreach(System.Reflection.ParameterInfo param in minfo.Value.GetParameters())
                    {
                        prototype +=  param.ParameterType.FullName + " " + param.Name + " ,";
                    }
                    prototype = prototype.Trim();
                    if (prototype.EndsWith(",")) { prototype = prototype.Substring(0, prototype.Length - 1); }
                    actions.Add(minfo.Key + "(" + prototype + ")");
                }
                return actions.ToArray();
            }

            ~AdminService() 
            {
                if (_TempStorage != "") { Fact.Tools.RecursiveDelete(_TempStorage); }
            }
        }
        static List<Fact.Network.Service> PreloadServiceFile(string Service)
        {
            List<Fact.Network.Service> services = new List<Fact.Network.Service>();
            byte[] array = System.IO.File.ReadAllBytes(Service);
            System.Reflection.Assembly Assembly = System.Reflection.Assembly.Load(array);
            foreach (System.Reflection.Module module in Assembly.GetModules())
            {
                foreach (System.Type type in module.GetTypes())
                {
                    if (type.IsSubclassOf(typeof(Fact.Network.Service)))
                    {
                        try
                        {
                            System.Reflection.ConstructorInfo Constructor = type.GetConstructor(new Type[] { });
                            if (Constructor == null) { break; }
                            try
                            {
                                Fact.Network.Service ServiceObj = (Fact.Network.Service)Constructor.Invoke(new object[] { });
                                services.Add(ServiceObj);
                            }
                            catch (Exception e)
                            {
                            }

                        }
                        catch { }
                    }
                }
            }
            return services;
        }

        static List<Fact.Network.Service> PreloadServiceDirectory(string ServiceDir)
        {
            List<Fact.Network.Service> services = new List<Fact.Network.Service>();

            foreach (string dir in System.IO.Directory.GetDirectories(ServiceDir))
            {
                foreach (Fact.Network.Service service in PreloadServiceDirectory(dir))
                {
                    services.Add(service);
                }
            }

            foreach (string file in System.IO.Directory.GetFiles(ServiceDir))
            {
                if (file.ToLower().EndsWith(".dll"))
                {
                    foreach (Fact.Network.Service service in PreloadServiceFile(file))
                    {
                        services.Add(service);
                    }
                }
            }

            return services;
        }


        public override string Name
        {
            get
            {
                return "ServicesServer";
            }
        }
        public ServicesServer()
        { }
        bool _Verbose = false;
        internal Fact.Network.Server _Server;
        public override int Run(params string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddBooleanInput("verbose", "Force this module to display more information", false); CommandLine.AddShortInput("v", "verbose");
            CommandLine.AddStringInput("journal", "Store the action in a journal file to restore them on crash", ""); CommandLine.AddShortInput("j", "journal");
            CommandLine.AddStringInput("port", "Select the port used to creae the server", "9089"); CommandLine.AddShortInput("p", "port");
            CommandLine.AddStringInput("threadpool-size", "set the initialsize size of the threadpool", "");
            CommandLine.AddStringListInput("services", "List of the services that must be loaded at startup", new List<string>());

            CommandLine.AddBooleanInput("help", "Display this message", false); CommandLine.AddShortInput("h", "help");


            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }
            _Verbose = CommandLine["verbose"].AsBoolean();
            string port = CommandLine["port"].AsString().Trim();
            string journal = CommandLine["journal"].AsString().Trim();
            string threadpoolsize_str = CommandLine["threadpool-size"].AsString().Trim();
            if (threadpoolsize_str != "")
            {
                int tpsize = 0;
                if (int.TryParse(threadpoolsize_str, out tpsize))
                {
                    Fact.Threading.Job.SetThreadPoolSize(tpsize);
                }
                else
                {
                    Fact.Log.Error("Invalid Threadpool size: " + threadpoolsize_str);
                    return 1;
                }
            }

            int _InterfacePort = 9089;
            int _SSLPort = 0;

            if (!int.TryParse(port, out _InterfacePort)) { Fact.Log.Error(port + " is not a valid port number."); return 1; }
            Fact.Log.Verbose("Creating server ...");
            _Server = new Fact.Network.Server(_InterfacePort, _SSLPort);
            if (_Verbose)
            {
                _Server.Connected += (object sender, Fact.Network.Server.UserEventArgs e) => { Fact.Log.Verbose("User " + e.UserID + " connected"); };
                _Server.Disconnected += (object sender, Fact.Network.Server.UserEventArgs e) => { Fact.Log.Verbose("User " + e.UserID + " disconnected"); };
            }
            if (journal != "")
            {
                Fact.Log.Warning("The server is currently using a journal file to backup operations.");
                Fact.Log.Warning("It is up to you to make everything you can to keep this file safe.");
                Fact.Log.Warning("This file contain information about your users and about the services used.");
                Fact.Log.Warning("If you think that an unauthorized person has touch this file, delete it, use a safe copy of it,");
                Fact.Log.Warning("change the password of all your users and warn them about the incident.");
            }
            Fact.Log.Verbose("Creating the administration service ...");
            AdminService AdminService = new AdminService(this, journal, _Verbose);
            _Server.LoadService(AdminService);
            Fact.Log.Verbose("Creating the user service ...");
            UserService UserService = new UserService(this, _Verbose);
            _Server.LoadService(UserService);

            {
                Fact.Log.Verbose("Creating local access ...");
                Fact.Directory.User localuser = new Fact.Directory.User("local", "");
                localuser.DenyPasswordAuth = true;
                localuser.NoAccountSerialization = true;
                _Server.AllowedUsers.AddUser(localuser);
                Fact.Directory.Credential localusercred = new Fact.Directory.Credential(localuser, DateTime.MaxValue);

                string localAccessFolder = Fact.Internal.Information.GetSpecialFolders(Fact.Internal.Information.SpecialFolder.FactLocalServerData);
                if (!System.IO.Directory.Exists(localAccessFolder)) { Fact.Tools.RecursiveMakeDirectory(localAccessFolder); }
                if (System.IO.File.Exists(localAccessFolder + "/" + _InterfacePort.ToString()))
                {
                    System.IO.File.Delete(localAccessFolder + "/" + _InterfacePort.ToString());
                }
                if (_InterfacePort > 0)
                {
                    localusercred.Save(localAccessFolder + "/" + _InterfacePort.ToString());
                    if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
                    {
                        // This is bad we should have a way to do it at creation
                        Fact.Runtime.Process.Execute("chmod", "go-rwx " + localAccessFolder + "/" + _InterfacePort.ToString());
                    }
                }
                if (System.IO.File.Exists(localAccessFolder + "/" + _SSLPort.ToString()))
                {
                    System.IO.File.Delete(localAccessFolder + "/" + _SSLPort.ToString());
                    if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
                    {
                        // This is bad we should have a way to do it at creation
                        Fact.Runtime.Process.Execute("chmod", "go-rwx " + localAccessFolder + "/" + _SSLPort.ToString());
                    }
                }
                if (_SSLPort > 0)
                {
                    localusercred.Save(localAccessFolder + "/" + _SSLPort.ToString());
                }
            }
            List<string> services = CommandLine["services"].AsStringList();
            if (services.Count > 0)
            {
                Fact.Log.Verbose("Loading services");
                foreach (string service in services)
                {
                    try
                    {
                        if (_Verbose) { Fact.Log.Verbose("Loading service " + service); }
                        if (!System.IO.File.Exists(service))
                        {
                            Fact.Log.Warning("The service file " + service + " has not been found");
                        }
                        else
                        {
                            Fact.Processing.File file =
                                new Fact.Processing.File(System.IO.File.ReadAllBytes(service),
                                                         System.IO.Path.GetFileName(service));
                            if (AdminService.LoadServiceFromFile(file, _Server))
                            {
                                Fact.Log.Verbose("Service " + service + " loaded");
                            }
                            else
                            {
                                Fact.Log.Error("Service " + service + " not loaded");
                            }
                        }
                    }
                    catch
                    {
                        Fact.Log.Error("Unexpected error while loading the service " + service);
                    }
                }
            }

            {
                try
                {
                    string serviceDir = Fact.Internal.Information.GetLibFact();
                    if (System.IO.File.Exists(serviceDir))
                    {
                        serviceDir = System.IO.Path.GetDirectoryName(serviceDir);
                        serviceDir += "/Server/Services";

                        if (System.IO.Directory.Exists(serviceDir))
                        {
                            if (_Verbose) { Fact.Log.Verbose("Preloading default services ..."); }
                            foreach(Fact.Network.Service service in PreloadServiceDirectory(serviceDir))
                            {
                                try
                                {
                                    if (_Verbose) { Fact.Log.Verbose("Preloading service " + service.Name); }
                                    _LatentServices.Add(service.Name, service);
                                }
                                catch { }
                            }
                        }
                    }

                }
                catch
                { }
            }

            Fact.Log.Verbose("Start server ...");
            _Server.ServiceNotLoaded += _Server_ServiceNotLoaded;
            _Server.Start();
            System.Threading.Thread.Sleep(10000);
            while (_Server.IsListening) { System.Threading.Thread.Sleep(1000); }
            Fact.Log.Verbose("Stop server ...");
            return 0;
        }

        Dictionary<string, Fact.Network.Service> _LatentServices = new Dictionary<string, Fact.Network.Service>();

        void _Server_ServiceNotLoaded(object sender, Fact.Network.Server.ServiceNotLoadedEventArgs e)
        {
            if (_LatentServices.ContainsKey(e.ServiceName))
            {
                if (_Verbose) { Fact.Log.Verbose("Trying to do dynamic loading for service " + e.ServiceName); }
                e.Server.LoadService(_LatentServices[e.ServiceName]);
            }
            else { if (_Verbose) { Fact.Log.Error("Service " + e.ServiceName + " requested but not found or not loaded"); } }
        }
    }
}

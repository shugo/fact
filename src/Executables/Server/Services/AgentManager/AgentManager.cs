﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentManager
{
    public class AgentManager : Fact.Network.Service
    {
        class Action
        {
            internal string _Type = "";
            internal List<string> _Content = new List<string>();
        }
        class Job
        {
            internal bool _Deleted = false;
            internal bool _NotPublic = false;
            internal bool _Published = false;

            internal string _AgentGroup = "";
            internal string _AgentID = "";
            internal string _UserID = "";

            internal int _Token = 0;
            internal string _Status = "Creation";
            internal string _Name = "unknown";

            internal int _Tries = 0;

            internal Fact.Network.Client _Client = null;

            internal DateTime _CreationDate = DateTime.MaxValue;
            internal DateTime _FinisedDate = DateTime.MaxValue;
            internal Dictionary<string, Fact.Processing.File> _JobLocalStorage = new Dictionary<string, Fact.Processing.File>();
            internal List<Action> _Actions = new List<Action>();

            internal StringBuilder _StdOut = new StringBuilder();
            internal StringBuilder _StdErr = new StringBuilder();


            public string ToXML()
            {
                StringBuilder jobxml = new StringBuilder();
                jobxml.Append("<job>");
                jobxml.Append("<name>"); jobxml.Append(Fact.Network.Tools.EscapeURIString(_Name)); jobxml.Append("</name>");

                jobxml.Append("<token>"); jobxml.Append(_Token.ToString()); jobxml.Append("</token>");
                jobxml.Append("<local>");
                foreach (KeyValuePair<string, Fact.Processing.File> obj in _JobLocalStorage)
                {
                    jobxml.Append("<element>");
                    jobxml.Append("<name>"); jobxml.Append(obj.Key); jobxml.Append("</name>");
                    jobxml.Append("<hash>"); jobxml.Append(obj.Value.ContentHash); jobxml.Append("</hash>");

                    //jobxml.Append("<data>"); jobxml.Append(System.Convert.ToBase64String(obj.Value.GetBytes())); jobxml.Append("</data>");
                    jobxml.Append("</element>");
                }
                jobxml.Append("</local>");
                jobxml.Append("<actions>");
                foreach (Action action in _Actions)
                {
                    jobxml.Append("<action>");
                    jobxml.Append("<type>");
                    jobxml.Append(action._Type);
                    jobxml.Append("</type>");
                    jobxml.Append("<contents>");
                    foreach (string content in action._Content)
                    {
                        jobxml.Append("<content>");
                        jobxml.Append(Fact.Network.Tools.EscapeURIString(content));
                        jobxml.Append("</content>");
                    }
                    jobxml.Append("</contents>");
                    jobxml.Append("</action>");
                }
                jobxml.Append("</actions>");
                jobxml.Append("</job>");
                return jobxml.ToString();
            }
        }

        Job ParseJob(string JobXML)
        {
            try
            {
                Fact.Parser.XML XML = new Fact.Parser.XML();
                XML.Parse(JobXML);
                if (XML.Root == null) { Fact.Log.Error("Job parsing error: No root node"); return null; }
                Fact.Parser.XML.Node JobNode = XML.Root.FindNode("job", false);
                if (JobNode == null) { Fact.Log.Error("Job parsing error: No job node"); return null; }
                Fact.Parser.XML.Node Local = JobNode.FindNode("local", false);
                Fact.Parser.XML.Node Actions = JobNode.FindNode("actions", false);
                Fact.Parser.XML.Node Token = JobNode.FindNode("token", false);
                if (Actions == null) { Fact.Log.Warning("Job parsing error: No actions node"); return null; }
                AgentManager.Job Job = new Job();
                if (Local != null)
                {
                    foreach (Fact.Parser.XML.Node node in Local.Children)
                    {
                        if (node.Type.ToLower() == "element")
                        {
                            Fact.Parser.XML.Node Name = node.FindNode("name", false);
                            Fact.Parser.XML.Node Data = node.FindNode("data", false);
                            if (Name != null && Name.Children.Count == 1 && Data != null && Data.Children.Count == 1)
                            {
                                try
                                {
                                    string FileName = Name.Children[0].Text;
                                    byte[] FileData = Convert.FromBase64String(Data.Children[0].Text);
                                    Job._JobLocalStorage.Add(FileName, new Fact.Processing.File(FileData, FileName));
                                }
                                catch { }
                            }
                        }
                    }
                }
                {
                    foreach (Fact.Parser.XML.Node node in Actions.Children)
                    {
                        if (node.Type.ToLower() == "action")
                        {
                            Fact.Parser.XML.Node Type = node.FindNode("type", false);
                            Fact.Parser.XML.Node Contents = node.FindNode("contents", false);
                            if (Type != null && Type.Children.Count == 1 && Contents != null)
                            {
                                string ActionType = Type.Children[0].Text;
                                List<string> ActionContent = new List<string>();
                                foreach (Fact.Parser.XML.Node contentnode in Contents.Children)
                                {
                                    if (contentnode.Type.ToLower() == "content" && contentnode.Children.Count == 1)
                                    {
                                        ActionContent.Add(Uri.UnescapeDataString(contentnode.Children[0].Text));
                                    }
                                }
                                Job._Actions.Add(new Action() { _Type = ActionType, _Content = ActionContent });
                            }
                        }
                    }
                }
                if (Token != null && Token.Children.Count > 0) { int.TryParse(Token.Children[0].Text, out Job._Token); }
                return Job;
            }
            catch { return null; }
        }

        bool _AgentLoggonInit = false;
        void InitAgentLoggon(Fact.Network.Server Server)
        {
            lock (this)
            {
                if (_AgentLoggonInit) { return; }
                Server.Disconnected += new EventHandler<Fact.Network.Server.UserEventArgs>(UserDisconnected);
                _AgentLoggonInit = true;
            }
        }

        DateTime _LastClean = DateTime.UtcNow;

        void _NeedToClean_NotThreadSafe()
        {
            try
            {
                if ((DateTime.UtcNow - _LastClean).TotalMinutes > 1)
                {
                    List<int> _DeadJob = new List<int>();
                    foreach (KeyValuePair<int, Job> job in _FinishedJob)
                    {
                        if ((DateTime.UtcNow - job.Value._FinisedDate).TotalMinutes > 30)
                        {
                            _DeadJob.Add(job.Key);
                        }
                    }
                    foreach (int value in _DeadJob)
                    {
                        _DeleteJobNotThreadSafe(value);
                    }
                    _LastClean = DateTime.UtcNow;
                }
            }
            catch { }
        }

        public AgentManager() : base("AgentManager") 
        {
        }

        void RemoveAgentUser_NotThreadSafe(string UserID)
        {
            try
            {
                if (_AgentsByUserId.ContainsKey(UserID))
                {
                    try
                    {
                        if (_Agents.ContainsKey(_AgentsByUserId[UserID]._Token))
                        {
                            _Agents.Remove(_AgentsByUserId[UserID]._Token);
                        }
                        Agent agent = _AgentsByUserId[UserID];
                        while (agent._Jobs.Count > 0)
                        {
                            Job job = agent._Jobs.Dequeue();
                            Fact.Log.Verbose("The job " + job._Token + " has been deleted since the targeted agent is dead.");
                            _DeleteJobNotThreadSafe(job._Token);
                        }
                        _AgentsByUserId.Remove(UserID);
                    }
                    catch { }
                }
            }
            catch { }
            try
            {
                List<int> jobToDelete = new List<int>();
                foreach (Job job in _JobPool.Values)
                {
                    try
                    {
                        if (job._AgentID == UserID)
                        {
                            job._AgentID = "";
                            job._Tries++;
                            Fact.Log.Verbose("The job " + job._Token + " has been published again since the agent associated to " + UserID + " is dead");
                            _JobQueue.Enqueue(job);
                        }
                    }
                    catch { }
                }
                foreach (int deadJob in jobToDelete)
                {
                    try
                    {
                        _DeleteJobNotThreadSafe(deadJob);
                    }
                    catch { }
                }
            }
            catch { }
        }

        void RemoveUser_NotThreadSafe(string UserID)
        {
            try
            {
                List<int> jobToDelete = new List<int>();
                foreach (Job job in _JobPool.Values)
                {
                    try
                    {
                        if (job._UserID == UserID)
                        {
                            Fact.Log.Verbose("The job " + job._Token + " has been deleted since the requester " + UserID + " is dead");
                            jobToDelete.Add(job._Token);
                        }
                    }
                    catch { }
                }
                foreach (int deadJob in jobToDelete)
                {
                    try
                    {
                        _DeleteJobNotThreadSafe(deadJob);
                    }
                    catch { }
                }
            }
            catch { }
        }

        void UserDisconnected(object sender, Fact.Network.Server.UserEventArgs User)
        {
            lock (this)
            {
#if DEBUG
                Fact.Log.Debug("Managing the deconnection of  " + User.UserID);
#endif
                RemoveAgentUser_NotThreadSafe(User.UserID);
                RemoveUser_NotThreadSafe(User.UserID);
            }
        }

        ulong AgentUID = 0;
        static System.Security.Cryptography.SHA512 _SHA512 = System.Security.Cryptography.SHA512.Create();
        Dictionary<string, Agent> _Agents = new Dictionary<string, Agent>();
        Dictionary<string, Agent> _AgentsByUserId = new Dictionary<string, Agent>();

        class Agent
        {
            internal string _Name = "";
            internal string _Token = "";
            internal string _UniqueName = "";
            internal string _HostAddress = "";
            internal string _UserId = "";

            internal Queue<Job> _Jobs = new Queue<Job>();
        }

        [Fact.Network.ServiceExposedMethod]
        public string Register(Fact.Network.Service.RequestInfo Info, string Name)
        {
            Name = Name.Trim();
            if (Name == "") { Name = "Agent"; }
            string FullName = "";
            InitAgentLoggon(Info.Server);
            lock (this)
            {
                FullName = "{" + Name + "}" + "{" + AgentUID + "}";
                byte[] hash = _SHA512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(FullName));
                FullName = System.Convert.ToBase64String(hash);
                Agent agent = new Agent();
                agent._Name = Name;
                agent._UniqueName = AgentUID + "_" + Name;
                agent._Token = FullName;
                agent._HostAddress = Info.UserHostAddress;
                agent._UserId = Info.UserID;
                AgentUID++;
                _Agents.Add(FullName, agent);
                if (_AgentsByUserId.ContainsKey(Info.UserID))
                {
                    RemoveAgentUser_NotThreadSafe(Info.UserID);
                }
                _AgentsByUserId.Add(Info.UserID, agent);
            }
            return FullName;
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetAgentList(Fact.Network.Service.RequestInfo Info)
        {
            List<string> agents = new List<string>();
            lock (this)
            {
                foreach (string name in _Agents.Keys)
                {
                    agents.Add(name);
                }
            }
            return agents.ToArray();
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetAgentHostAddress(Fact.Network.Service.RequestInfo Info, string Agent)
        {
            lock (this)
            {
                if (_Agents.ContainsKey(Agent)) { return _Agents[Agent]._HostAddress; }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetAgentUserId(Fact.Network.Service.RequestInfo Info, string Agent)
        {
            lock (this)
            {
                if (_Agents.ContainsKey(Agent)) { return _Agents[Agent]._UserId; }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetAgentName(Fact.Network.Service.RequestInfo Info, string Agent)
        {
            lock (this)
            {
                if (_Agents.ContainsKey(Agent)) { return _Agents[Agent]._Name; }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public int GetFinishedJobCount(Fact.Network.Service.RequestInfo Info)
        {
            lock (this)
            {
                return _FinishedJob.Count;
            }
            return 0;
        }

        [Fact.Network.ServiceExposedMethod]
        public int GetPublishedJobCount(Fact.Network.Service.RequestInfo Info)
        {
            lock (this)
            {
                return _PublishedJob.Count;
            }
            return 0;
        }

        [Fact.Network.ServiceExposedMethod]
        public int GetJobPoolSize(Fact.Network.Service.RequestInfo Info)
        {
            lock (this)
            {
                return _JobPool.Count;
            }
            return 0;
        }

        [Fact.Network.ServiceExposedMethod]
        public int[] GetJobList(Fact.Network.Service.RequestInfo Info)
        {
            lock (this)
            {
                List<int> jobs = new List<int>(_JobPool.Keys);
                return jobs.ToArray();
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public int GetJobQueueSize(Fact.Network.Service.RequestInfo Info)
        {
            lock (this)
            {
                return _JobQueue.Count;
            }
            return 0;
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetJobStatus(Fact.Network.Service.RequestInfo Info, int Job)
        {
            lock (this)
            {
                if (_JobPool.ContainsKey(Job)) { lock (_JobPool[Job]) { return _JobPool[Job]._Status; } }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetJobName(Fact.Network.Service.RequestInfo Info, int Job)
        {
            lock (this)
            {
                if (_JobPool.ContainsKey(Job)) { lock (_JobPool[Job]) { return _JobPool[Job]._Name; } }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public bool SetJobName(Fact.Network.Service.RequestInfo Info, int Job, string Name)
        {
            lock (this)
            {
                if (_JobPool.ContainsKey(Job))
                {
                    lock (_JobPool[Job])
                    {
                        if (_JobPool[Job]._UserID == Info.UserID)
                        {
                            _JobPool[Job]._Name = Name;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool IsJobFinished(Fact.Network.Service.RequestInfo Info, int Job)
        {
            lock (this)
            {
                if (_FinishedJob.ContainsKey(Job)) { return true; }
                else if (_JobPool.ContainsKey(Job))
                {
                    return _JobPool[Job]._Deleted;
                }
            }
            return true;
        }

        bool _DeleteJob(int Job)
        {
            lock (this)
            {
                return _DeleteJobNotThreadSafe(Job);
            }
        }
        bool _DeleteJobNotThreadSafe(int Job)
        {
            try
            {
                if (_FinishedJob.ContainsKey(Job))
                {
                    // Help the gc to flush the Local Storage
                    _FinishedJob[Job]._JobLocalStorage = null;
                    _FinishedJob[Job]._Deleted = true;
                    _FinishedJob.Remove(Job);
                }
                if (_JobPool.ContainsKey(Job))
                {
                    // Help the gc to flush the Local Storage
                    _JobPool[Job]._JobLocalStorage = null;
                    _JobPool[Job]._Deleted = true;
                    _JobPool.Remove(Job);
                }
                if (_PublishedJob.ContainsKey(Job))
                {
                    // Help the gc to flush the Local Storage
                    _PublishedJob[Job]._JobLocalStorage = null;
                    _PublishedJob[Job]._Deleted = true;
                    _PublishedJob.Remove(Job);
                }
                // No more job we can flush the cache
                if (_JobPool.Count == 0)
                {
                    CahceSize = 0;
                    _CacheFile.Clear();
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Fact.Log.Debug("Exception while delete the job " + Job);
                Fact.Log.Exception(e);
#endif
            }
            return true;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool DeleteJob(Fact.Network.Service.RequestInfo Info, int Job)
        {
            return _DeleteJob(Job);
        }


        [Fact.Network.ServiceExposedMethod]
        public string GetJob(Fact.Network.Service.RequestInfo Info, string AgentToken, string Group)
        {
            InitAgentLoggon(Info.Server);
            Group = Group.Trim();
            lock (this)
            {
                _NeedToClean_NotThreadSafe();
                if (!_Agents.ContainsKey(AgentToken))
                {
                    return null;
                }
                if (_Agents[AgentToken]._Jobs.Count > 0)
                {
                    Job job = _Agents[AgentToken]._Jobs.Dequeue();
                    Queue<Job> temp = new Queue<Job>();
                    while (job._Deleted || !job._Published)
                    {
                        if (!job._Deleted)
                        {
                            temp.Enqueue(job);
                        }
                        job = _Agents[AgentToken]._Jobs.Dequeue();
                    }

                    if (temp.Count > 0)
                    {
                        while (_JobQueue.Count > 0) { temp.Enqueue(_JobQueue.Dequeue()); }
                        _JobQueue = temp;
                    }

                    job._AgentID = Info.UserID;
                    lock (job) { job._Status = "Processing"; }
                    return job.ToXML();
                }
                else if (_JobQueue.Count > 0)
                {
                    Job job = _JobQueue.Dequeue();
                    Queue<Job> temp = new Queue<Job>();
                    while (((job._AgentGroup != Group && job._AgentGroup != "") || job._Deleted) && _JobQueue.Count > 0)
                    {
                        if (job != null && !job._Deleted) { temp.Enqueue(job); }
                        job = _JobQueue.Dequeue();
                    }
                    if (temp.Count > 0)
                    {
                        while (_JobQueue.Count > 0) { temp.Enqueue(_JobQueue.Dequeue()); }
                        _JobQueue = temp;
                    }
                    if (job._AgentGroup != Group && job._AgentGroup != "")
                    {
                        if (job != null) { temp.Enqueue(job); }
                        return "";
                    }
                    if (job._Deleted) { return ""; }
                    if (job._Tries > 5)
                    {
                        Fact.Log.Verbose("The job " + job._Token + " has failed on " + job._Tries + " agents and will be killed");
                        job._FinisedDate = DateTime.UtcNow;
                        if (_PublishedJob.ContainsKey(job._Token)) { _PublishedJob.Remove(job._Token); }
                        try { _FinishedJob.Add(job._Token, job); } catch { }
                    }
                    job._AgentID = Info.UserID;
                    lock (job) { job._Status = "Processing"; }
                    return job.ToXML();
                }
            }
            return "";
        }

        Dictionary<int, Job> _JobPool = new Dictionary<int, Job>();
        Dictionary<int, Job> _PublishedJob = new Dictionary<int, Job>();
        Dictionary<int, Job> _FinishedJob = new Dictionary<int, Job>();

        Queue<Job> _JobQueue = new Queue<Job>();

        int _tokenCount = 1;

        [Fact.Network.ServiceExposedMethod]
        public int CreateJobOnAgent(Fact.Network.Service.RequestInfo Info, string Agent)
        {
            lock (this)
            {
                _NeedToClean_NotThreadSafe();
                if (!_Agents.ContainsKey(Agent)) { return -1; }
                if (_JobPool.Count == 0 &&
                    _PublishedJob.Count == 0 &&
                    _FinishedJob.Count == 0)
                {
                    _tokenCount = 1;
                }
                else
                {
                    if (_tokenCount == int.MaxValue)
                    {
                        // FIX ME: We probably have to sweep everything here
                        return 0;
                    }
                    _tokenCount++;
                }
                Job Job = new Job();
                Job._AgentGroup = Agent;
                Job._Token = _tokenCount;
                Job._Status = "Creation";
                Job._CreationDate = DateTime.UtcNow;
                Job._UserID = Info.UserID;
                Job._NotPublic = true;
                _JobPool.Add(_tokenCount, Job);
                _Agents[Agent]._Jobs.Enqueue(Job);
                return _tokenCount;
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public int CreateJob(Fact.Network.Service.RequestInfo Info, string Group)
        {
            lock (this)
            {
                _NeedToClean_NotThreadSafe();
                if (_JobPool.Count == 0 &&
                    _PublishedJob.Count == 0 &&
                    _FinishedJob.Count == 0)
                {
                    _tokenCount = 1;
                }
                else
                {
                    if (_tokenCount == int.MaxValue)
                    {
                        // FIX ME: We probably have to sweep everything here
                        return 0;
                    }
                    _tokenCount++;
                }
                Job Job = new Job();
                Job._AgentGroup = Group;
                Job._Token = _tokenCount;
                Job._Status = "Creation";
                Job._CreationDate = DateTime.UtcNow;
                Job._UserID = Info.UserID;
                _JobPool.Add(_tokenCount, Job);
                return _tokenCount;
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PublishJob(Fact.Network.Service.RequestInfo Info, int Job) 
        {
            bool publicJob = false;
            lock (this)
            {
                if (_JobPool.ContainsKey(Job) &&
                    !_PublishedJob.ContainsKey(Job) &&
                    !_FinishedJob.ContainsKey(Job))
                {
                    lock (_JobPool[Job])
                    {
                        publicJob = !_JobPool[Job]._NotPublic;
                        _JobPool[Job]._Published = true;
                        if (_JobPool[Job]._NotPublic) { _JobPool[Job]._Status = "Waiting for the targeted agent"; }
                        else { _JobPool[Job]._Status = "Waiting for an agent"; }
                    }
                    if (publicJob)
                    {
                        _JobQueue.Enqueue(_JobPool[Job]);
                        _PublishedJob.Add(Job, _JobPool[Job]);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public void SendJobLog(Fact.Network.Service.RequestInfo Info, int Job, string StdOutLog, string StdErrLog)
        {
            InitAgentLoggon(Info.Server);

            bool status = true;
            lock (this)
            {
                status = (_JobPool.ContainsKey(Job) &&
                         !_FinishedJob.ContainsKey(Job));
            }
            if (!status) { return; }

            try
            {
                lock (this)
                {
                    _JobPool[Job]._StdOut.Append(StdOutLog);
                    _JobPool[Job]._StdErr.Append(StdErrLog);
                }
            }
            catch { }
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetJobStdOutLog(Fact.Network.Service.RequestInfo Info, int Job)
        {
            InitAgentLoggon(Info.Server);

            bool status = true;
            lock (this)
            {
                status = (_JobPool.ContainsKey(Job));
            }
            if (!status) { return ""; }
            try
            {
                lock (this)
                {
                    return _JobPool[Job]._StdOut.ToString();
                }
            }
            catch { }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetJobStdErrLog(Fact.Network.Service.RequestInfo Info, int Job)
        {
            InitAgentLoggon(Info.Server);

            bool status = true;
            lock (this)
            {
                status = (_JobPool.ContainsKey(Job));
            }
            if (!status) { return ""; }

            try
            {
                lock (this)
                {
                    return _JobPool[Job]._StdErr.ToString();
                }
            }
            catch { }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public void SendJobResult(Fact.Network.Service.RequestInfo Info, int Job, string Result)
        {
            InitAgentLoggon(Info.Server);
            bool status = true;
            lock (this)
            {
                status = (_JobPool.ContainsKey(Job) &&
                          _PublishedJob.ContainsKey(Job) &&
                         !_FinishedJob.ContainsKey(Job));
            }
            if (!status) { return; }

            try
            {
                AgentManager.Job NewJob = ParseJob(Result);
                if (NewJob == null) { NewJob = new AgentManager.Job(); NewJob._Token = Job; }
                NewJob._Status = "Ended";
                lock (this)
                {
                    // The double check is needed
                    if (_JobPool.ContainsKey(Job) &&
                        _PublishedJob.ContainsKey(Job) &&
                        !_FinishedJob.ContainsKey(Job))
                    {
                        // Only the agent that has the job can push it
                        if (_JobPool[Job]._AgentID == Info.UserID)
                        {
                            // Sync the output since the output is uploaded before the finalize
                            NewJob._StdOut = _JobPool[Job]._StdOut;
                            NewJob._StdErr = _JobPool[Job]._StdErr;
                            _PublishedJob.Remove(Job);
                            _JobPool[Job] = NewJob;
                            _JobPool[Job]._FinisedDate = DateTime.UtcNow;
                            _FinishedJob.Add(Job, _JobPool[Job]);
                        }
                    }
                }
            }
            catch { return; }
            
        }

        Dictionary<string, Fact.Processing.File> _CacheFile = new Dictionary<string, Fact.Processing.File>();
        ulong CahceSize = 0;
        ulong MaxCacheSize = 1024 * 1024 * 128; // The default size of the cache is 128 Mo

        [Fact.Network.ServiceExposedMethod]
        public bool AddFileInJobLocalStorage(Fact.Network.Service.RequestInfo Info, int Job, Fact.Processing.File File, string Name)
        {
            lock (this)
            {
                if (_JobPool.ContainsKey(Job))
                {
                    // Fix me: Allow the Job local storage to store file on disk
                    if (Name.StartsWith("./")) { Name = Name.Substring("./".Length); }
                    if (_JobPool[Job]._JobLocalStorage.ContainsKey(Name)) { return false; }
                    _JobPool[Job]._JobLocalStorage.Add(Name, File);
                    if (!_CacheFile.ContainsKey(File.ContentHash))
                    {
                        if (File.Size < MaxCacheSize)
                        {
                            CahceSize += File.Size;
                            if (CahceSize > MaxCacheSize)
                            {
                                _CacheFile.Clear();
                                CahceSize = File.Size;
                            }
                            _CacheFile.Add(File.ContentHash, File);
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool AddFileInJobLocalStorageFromCache(Fact.Network.Service.RequestInfo Info, int Job, string Hash, string Name)
        {
            lock (this)
            {
                if (_JobPool.ContainsKey(Job))
                {
                    if (Name.StartsWith("./")) { Name = Name.Substring("./".Length); }
                    if (_JobPool[Job]._JobLocalStorage.ContainsKey(Name)) { return false; }
                    if (_CacheFile.ContainsKey(Hash))
                    {
                        _JobPool[Job]._JobLocalStorage.Add(Name, _CacheFile[Hash]);
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public Fact.Processing.File GetFileInJobLocalStorage(Fact.Network.Service.RequestInfo Info, int Job, string Name)
        {
            lock (this)
            {
                if (_JobPool.ContainsKey(Job))
                {
                    // Fix me: Allow the Job local storage to store file on disk
                    if (_JobPool[Job]._JobLocalStorage.ContainsKey(Name)) { return _JobPool[Job]._JobLocalStorage[Name]; }
                }
            }

            return null;
        }

        bool _PushAction(Action Action, int Job)
        {
            lock (this)
            {
                if (_JobPool.ContainsKey(Job))
                {
                    lock (_JobPool[Job])
                    {
                        _JobPool[Job]._Actions.Add(Action);
                        return true;
                    }
                }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_Shutdown(Fact.Network.Service.RequestInfo Info, int Job)
        {
            Action Action = new Action();
            Action._Type = "Shutdown";
            return _PushAction(Action, Job);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_SetEnvVariable(Fact.Network.Service.RequestInfo Info, int Job, string Variable, string Value)
        {
            Action Action = new Action();
            Action._Type = "SetEnvVariable";
            Action._Content.Add(Variable);
            Action._Content.Add(Value);
            return _PushAction(Action, Job);
        }


        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_SetTimeout(Fact.Network.Service.RequestInfo Info, int Job, int Value)
        {
            Action Action = new Action();
            Action._Type = "SetTimeout";
            Action._Content.Add(Value.ToString());
            return _PushAction(Action, Job);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_CopyFileFromAgentToJobLocalStorage(Fact.Network.Service.RequestInfo Info, int Job, string AgentPath, string Name)
        {
            Action Action = new Action();
            Action._Type = "CopyFileToJobLocalStorage";
            Action._Content.Add(AgentPath);
            Action._Content.Add(Name);
            return _PushAction(Action, Job);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_CopyFileFromJobLocalStorageToAgent(Fact.Network.Service.RequestInfo Info, int Job, string AgentPath, string Name)
        {
            Action Action = new Action();
            Action._Type = "CopyFileFromJobLocalStorage";
            Action._Content.Add(AgentPath);
            Action._Content.Add(Name);
            return _PushAction(Action, Job);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_DeleteFileFromJobLocalStorage(Fact.Network.Service.RequestInfo Info, int Job, string Name)
        {
            Action Action = new Action();
            Action._Type = "DeleteFileFromJobLocalStorage";
            Action._Content.Add(Name);
            return _PushAction(Action, Job);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_DeleteFileOnAgent(Fact.Network.Service.RequestInfo Info, int Job, string AgentPath)
        {
            Action Action = new Action();
            Action._Type = "DeleteFile";
            Action._Content.Add(AgentPath);
            return _PushAction(Action, Job);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_RunCommandOnAgent(Fact.Network.Service.RequestInfo Info, int Job, string Command, params string[] Arguments)
        {
            Action Action = new Action();
            Action._Type = "RunCommand";
            Action._Content.Add(Command);
            foreach (string arg in Arguments)
            {
                Action._Content.Add(arg);
            }
            return _PushAction(Action, Job);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_RunKernelOnAgent(Fact.Network.Service.RequestInfo Info, int Job, string Kernel, params string[] Arguments)
        {
            Action Action = new Action();
            Action._Type = "RunKernel";
            Action._Content.Add(Kernel);
            foreach (string arg in Arguments)
            {
                Action._Content.Add(arg);
            }
            return _PushAction(Action, Job);
        }


        [Fact.Network.ServiceExposedMethod]
        public bool PushAction_RunCommandOnAgentAndRedirectResult(Fact.Network.Service.RequestInfo Info, int Job, string Command, string StdOut, string StdErr, params string[] Arguments)
        {
            Action Action = new Action();
            Action._Type = "RunCommandAndRedirectResult";
            Action._Content.Add(Command);
            Action._Content.Add(StdOut);
            Action._Content.Add(StdErr);

            foreach (string arg in Arguments)
            {
                Action._Content.Add(arg);
            }
            return _PushAction(Action, Job);
        }


    }
}

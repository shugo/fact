﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buildfarm
{
    public class BuildfarmManager : Fact.Network.Service
    {
        FileManager _FileManager = new FileManager();
        string _StorageFolder = "";


        public class Submission
        {
            BuildfarmManager _Parent = null;
            string _Hash = "";
            public string Hash { get { return _Hash; } }
            DateTime _Date = DateTime.Now;
            public DateTime Date { get { return _Date; } set { _Date = value; } }
            string _Project = "";
            public Fact.Processing.Project Project { get { return _Parent._FileManager.LoadProject(_Project); } }
            string _ProjectBuilt = null;
            public Fact.Processing.Project ProjectBuilt { get { return _Parent._FileManager.LoadProject(_ProjectBuilt); ; } set { _ProjectBuilt = value.Hash; _Parent._FileManager.Save(value); } }
            string _ProjectTested = null;
            public Fact.Processing.Project ProjectTested { get { return _Parent._FileManager.LoadProject(_ProjectTested); ; } set { _ProjectTested = value.Hash; _Parent._FileManager.Save(value); } }
            public Submission(Fact.Processing.Project Project, BuildfarmManager Parent)
            {
                _Parent = Parent;
                _Hash = Project.Hash;
                _Project = _Hash;
                _Parent._FileManager.Save(Project);
            }

            internal Submission(string Hash, BuildfarmManager Parent)
            {
                _Parent = Parent;
                _Hash = Hash;
                _Project = Hash;
            }
        }
        public class Project
        {
            string _Name = "";
            public string Name { get { return _Name; } set { _Name = value; } }
            Dictionary<string, Version> _Versions = new Dictionary<string, Version>();
            public Dictionary<string, Version> Versions { get { return _Versions; } }
            Dictionary<string, Branch> _Branches = new Dictionary<string, Branch>();
            public Dictionary<string, Branch> Branches { get { return _Branches; } }

            public class Version
            {
                string _Name = "";
                public string Name { get { return _Name; } set { _Name = value; } }
                Dictionary<string, Branch> _Branches = new Dictionary<string, Branch>();
                Dictionary<string, List<Submission>> _PerBranchSubmissions = new Dictionary<string, List<Submission>>();
                Dictionary<string, Fact.Parser.XML.Node> _BranchesBuildScripts = new Dictionary<string, Fact.Parser.XML.Node>();
                public bool Submit(string Branch, Submission Submission)
                {
                    lock (this)
                    {
                        if (!_Branches.ContainsKey(Branch)) { return false; }
                        if (!_PerBranchSubmissions.ContainsKey(Branch)) { _PerBranchSubmissions.Add(Branch, new List<Submission>()); }
                        _PerBranchSubmissions[Branch].Add(Submission);
                        return true;
                    }
                }
                public List<Submission> ListSubmissions(string Branch)
                {
                    lock (this)
                    {
                        if (!_Branches.ContainsKey(Branch)) { return new List<Submission>(); }
                        if (_PerBranchSubmissions.ContainsKey(Branch)) { return new List<Submission>(_PerBranchSubmissions[Branch]); }
                    }
                    return new List<Submission>();
                }
                public Dictionary<string, Branch> Branches { get { return _Branches; } }
                string _ProjectLocaltion = "";
                Fact.Processing.Project _Project = null;
                public string ToXML()
                {
                    lock (this)
                    {
                        StringBuilder builder = new StringBuilder();
                        builder.Append("<version name='" + _Name + "'>");
                        foreach (KeyValuePair<string, List<Submission>> submissions in _PerBranchSubmissions)
                        {
                            builder.Append("<submissions name='" + submissions.Key + "'>");
                            for (int n = 0; n < submissions.Value.Count; n++)
                            {
                                builder.Append("<submission name='" + submissions.Value[n].Hash + "'>");
                                builder.Append(submissions.Value[n].Hash);
                                builder.Append("</submission>");
                            }
                            builder.Append("</submissions>");
                        }
                        foreach (KeyValuePair<string, Branch> branch in _Branches)
                        {
                            builder.Append("<link branch='" + branch.Key + "'>");
                            builder.Append("</link>");
                        }
                        builder.Append("</version>");
                        return builder.ToString();
                    }
                }
                public static Version FromXML(Fact.Parser.XML.Node Node, BuildfarmManager Parent, Project Project)
                {
                    if (Node.Type.ToLower() != "version") { Node = Node.FindNode("version", false); }
                    Version v = new Version();
                    v._Name = Node["name"];
                    foreach (Fact.Parser.XML.Node link in Node.Children)
                    {
                        if (link.Type.ToLower() == "link")
                        {
                            string branch = link["branch"];
                            if (Project._Branches.ContainsKey(branch))
                            {
                                v._Branches.Add(branch, Project._Branches[branch]);
                                Fact.Log.Debug("attach " + v._Name + " to " + branch);
                            }
                        }
                    }

                    foreach (Fact.Parser.XML.Node submissions in Node.Children)
                    {
                        if (submissions.Type.ToLower() == "submissions")
                        {
                            List<Submission> submissionsList = new List<Submission>();
                            v._PerBranchSubmissions.Add(submissions["name"], submissionsList);
                            foreach (Fact.Parser.XML.Node child in submissions.Children)
                            {
                                if (child.Type.ToLower() == "submission")
                                {
                                    Submission submission = new Submission(child.Children[0].Text.Trim(), Parent);
                                    submissionsList.Add(submission);
                                }
                            }
                        }
                    }


                    return v;
                }
            }

            public class Branch
            {
                string _Name = "";
                public string Name { get { return _Name; } set { _Name = value; } }
                public string ToXML()
                {
                    lock (this)
                    {
                        StringBuilder builder = new StringBuilder();
                        builder.Append("<branch name='" + _Name + "'>");
                        builder.Append("</branch>");
                        return builder.ToString();
                    }
                }
                public static Branch FromXML(Fact.Parser.XML.Node Node)
                {

                        if (Node.Type.ToLower() != "branch") { Node = Node.FindNode("branch", false); }
                        Branch b = new Branch();
                        b._Name = Node["name"];
                        return b;
                }
            }

            public class VersionBranchPair
            {
                Branch _Branch = null;
                Version _Version = null;
            }

            public string ToXML()
            {
                lock (this)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append("<project name='" + _Name + "'>");
                    foreach (Version version in Versions.Values) { builder.Append(version.ToXML()); }
                    foreach (Branch branch in Branches.Values) { builder.Append(branch.ToXML()); }
                    builder.Append("</project>");
                    return builder.ToString();
                }
            }
            public static Project FromXML(Fact.Parser.XML.Node Node, BuildfarmManager Parent)
            {
                if (Node.Type.ToLower() != "project") { Node = Node.FindNode("project", false); }
                if (Node == null) { return new Project(); }
                Project p = new Project();
                foreach (Fact.Parser.XML.Node child in Node.Children)
                {
                    if (child.Type.ToLower() == "branch") { Branch b = Branch.FromXML(child); p._Branches.Add(b.Name, b); }
                }
                foreach (Fact.Parser.XML.Node child in Node.Children)
                {
                    if (child.Type.ToLower() == "version") { Version v = Version.FromXML(child, Parent, p); p._Versions.Add(v.Name, v); }
                }
                p.Name = Node["name"].Trim();
                return p;
            }
        }
        public BuildfarmManager()
            : base("BuildfarmManager")
        {

        }

        Dictionary<string, Project> _Projects = new Dictionary<string, Project>();

        string ToXML()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<buildfarm>");
            lock (_Projects)
            {
                foreach (Project project in _Projects.Values)
                {
                        builder.Append(project.ToXML());
                }
            }
            builder.Append("</buildfarm>");
            return builder.ToString();
        }

        void Save()
        {
            if (_FileManager.Location == "") { return; }
            if (!System.IO.Directory.Exists(_FileManager.Location)) { return; }
            Fact.Tools.RecursiveDelete(_FileManager.Location + "/buildfarm.xml");
            Save(_FileManager.Location + "/buildfarm.xml");
        }

        void Save(string File)
        {
            if (File == "") { return; }
            Fact.Tools.RecursiveDelete(File);
            System.IO.File.WriteAllText(File, ToXML());
        }

        void FromXML(Fact.Parser.XML.Node Node)
        {
            _Projects.Clear();
            if (Node.Type.ToLower() != "buildfarm") { Node = Node.FindNode("buildfarm", false); }
            foreach (Fact.Parser.XML.Node child in Node.Children)
            {
                if (child.Type.ToLower() == "project") { Project p = Project.FromXML(child, this); _Projects.Add(p.Name, p); }
            }            
        }


        [Fact.Network.ServiceExposedMethod]
        public bool SetStorageLocation(Fact.Network.Service.RequestInfo Info, string Location)
        {
            lock (_Projects)
            {
                _FileManager.Location = Location;
                if (System.IO.File.Exists(Location + "/buildfarm.xml"))
                {
                    Fact.Parser.XML Xml = new Fact.Parser.XML();
                    Xml.Parse_File(Location + "/buildfarm.xml");
                    FromXML(Xml.Root);
                }
            }
            Save();
            return true;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool CreateProject(Fact.Network.Service.RequestInfo Info, string Name)
        {
            if (Name.Contains(":") || Name.Contains("/") || Name.Contains("'")) { return false; }
            lock (_Projects)
            {
                if (_Projects.ContainsKey(Name)) { return false; }
                _Projects.Add(Name, new Project() { Name = Name });
            }
            Save();
            return true;
        }
        [Fact.Network.ServiceExposedMethod]
        public string[] GetProjects(Fact.Network.Service.RequestInfo Info)
        {
            List<string> Projects = new List<string>();
            lock (_Projects)
            {
                Projects = new List<string>(_Projects.Keys);
            }
            return Projects.ToArray();
        }
        [Fact.Network.ServiceExposedMethod]
        public bool CreateProjectVersion(Fact.Network.Service.RequestInfo Info, string ProjectName, string VersionName)
        {
            if (Name.Contains(":") || Name.Contains("/") || Name.Contains("'")) { return false; }
            Project project = null;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return false; }

            lock (project)
            {
                if (project.Versions.ContainsKey(VersionName)) { return false; }
                project.Versions.Add(VersionName, new Project.Version() { Name = VersionName });
            }
            Save();
            return true;
        }
        [Fact.Network.ServiceExposedMethod]
        public string[] GetProjectVersions(Fact.Network.Service.RequestInfo Info, string ProjectName)
        {
            Project project = null;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return new string[0]; }

            lock (project)
            {
                List<string> names = new List<string>();
                foreach (Project.Version version in project.Versions.Values)
                {
                    names.Add(version.Name);
                }
                return names.ToArray();
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public bool CreateProjectBranch(Fact.Network.Service.RequestInfo Info, string ProjectName, string BranchName)
        {
            if (Name.Contains(":") || Name.Contains("/") || Name.Contains("'")) { return false; }
            Project project = null;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return false; }

            lock (project)
            {
                if (project.Branches.ContainsKey(BranchName)) { return false; }
                project.Branches.Add(BranchName, new Project.Branch() { Name = BranchName });
            }
            Save();
            return true;
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetProjectBranches(Fact.Network.Service.RequestInfo Info, string ProjectName)
        {
            Project project = null;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return new string[0]; }

            lock (project)
            {
                List<string> names = new List<string>();
                foreach (Project.Branch branch in project.Branches.Values)
                {
                    names.Add(branch.Name);
                }
                return names.ToArray();
            }
        }
        [Fact.Network.ServiceExposedMethod]
        public bool AttachBranchToVersion(Fact.Network.Service.RequestInfo Info, string ProjectName, string VersionName, string BranchName)
        {
            Project project = null;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return false; }

            lock (project)
            {
                if (!project.Branches.ContainsKey(BranchName) || !project.Versions.ContainsKey(VersionName))
                {
                    return false;
                }
                if (project.Versions[VersionName].Branches.ContainsKey(BranchName))
                {
                    return false;
                }
                project.Versions[VersionName].Branches.Add(BranchName, project.Branches[BranchName]);
            }
            Save();
            return true;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool Submit(Fact.Network.Service.RequestInfo Info, string ProjectName, string VersionName, string BranchName, Fact.Processing.Project Project)
        {
            Project project = null;
            bool state = false;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return false; }

            lock (project)
            {
                if (!project.Branches.ContainsKey(BranchName) || !project.Versions.ContainsKey(VersionName))
                {
                    return false;
                }
                if (!project.Versions[VersionName].Branches.ContainsKey(BranchName))
                {
                    return false;
                }
                Submission Submission = new Submission(Project, this);
                state = project.Versions[VersionName].Submit(BranchName, Submission);
            }
            if (state) { Save(); }
            return state;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool SubmitBuilt(Fact.Network.Service.RequestInfo Info, string ProjectName, string VersionName, string BranchName, string ID, Fact.Processing.Project Project)
        {
            Submission submission = GetRawSubmission(ProjectName, VersionName, BranchName, ID);
            if (submission == null) { return false; }
            submission.ProjectBuilt = Project;
            Save();
            return true;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool SubmitTest(Fact.Network.Service.RequestInfo Info, string ProjectName, string VersionName, string BranchName, string ID, Fact.Processing.Project Project)
        {
            Submission submission = GetRawSubmission(ProjectName, VersionName, BranchName, ID);
            if (submission == null) { return false; }
            submission.ProjectTested = Project;
            Save();
            return true;
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetSubmissionIds(Fact.Network.Service.RequestInfo Info, string ProjectName, string VersionName, string BranchName)
        {
            Project project = null;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return new string[0]; }

            lock (project)
            {
                if (!project.Branches.ContainsKey(BranchName) || !project.Versions.ContainsKey(VersionName))
                {
                    return new string[0];
                }
                if (!project.Versions[VersionName].Branches.ContainsKey(BranchName))
                {
                    return new string[0];
                }
                List<Submission> Submissions = project.Versions[VersionName].ListSubmissions(BranchName);
                int Index = 0;
                List<string> SubmissionsHash = new List<string>();
                foreach (Submission submission in Submissions) { SubmissionsHash.Add(submission.Hash + "#" + Index.ToString()); Index++; }
                return SubmissionsHash.ToArray();
            }
        }

        Submission GetRawSubmission( string ProjectName, string VersionName, string BranchName, string ID)
        {
            Project project = null;
            lock (_Projects)
            {
                if (_Projects.ContainsKey(ProjectName)) { project = _Projects[ProjectName]; }
            }
            if (project == null) { return null; }

            lock (project)
            {
                if (!project.Branches.ContainsKey(BranchName) || !project.Versions.ContainsKey(VersionName))
                {
                    return null;
                }
                if (!project.Versions[VersionName].Branches.ContainsKey(BranchName))
                {
                    return null;
                }

                List<Submission> Submissions = project.Versions[VersionName].ListSubmissions(BranchName);
                if (Submissions.Count == 0) { return null; }
                if (ID.Trim() == "" || ID.Trim() == "#" || ID.Trim().ToLower() == "head")
                {
                    return Submissions[Submissions.Count - 1];
                }

                int Index = 0;
                string[] ID_Index = ID.Split('#');
                int requestedIndex = 0;
                string requestedID = "";
                if (ID_Index.Length > 1) { int.TryParse(ID_Index[1], out requestedIndex); }
                if (ID_Index.Length > 0) { requestedID = ID_Index[0].Trim().ToLower(); }
                Submission selectedSubmission = null;
                int index = 0;
                foreach (Submission submission in Submissions)
                {
                    if (submission.Hash.Trim().ToLower() == requestedID)
                    {
                        if (selectedSubmission != null)
                        {
                            if (requestedIndex == index) { selectedSubmission = submission; }
                        }
                        selectedSubmission = submission;
                    }
                    index++;
                }
                if (selectedSubmission == null) { return null; }
                return selectedSubmission;
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public Fact.Processing.Project GetSubmission(Fact.Network.Service.RequestInfo Info, string ProjectName, string VersionName, string BranchName, string ID)
        {
            Submission submission = GetRawSubmission(ProjectName, VersionName, BranchName, ID);
            if (submission == null) { return null; }
            Fact.Log.Debug("Submission found");
            return submission.Project;
        }
    }
}

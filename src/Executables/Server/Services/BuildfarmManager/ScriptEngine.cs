﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buildfarm
{
    class Script
    {
        List<Action> _Actions = new List<Action>();
        public void ToXML()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<script>");
            foreach (Action action in _Actions)
            {
                builder.Append(action.ToXML());
            }
            builder.Append("</script>");
        }

        public void SentToAgentManager(Fact.Network.Server Server)
        {
            try
            {
                int job = (int)Server.InvokeAction((Fact.Directory.User)null, "AgentManager.CreateJob", "");
            }
            catch { }
        }
    }
    class Action
    {
        public virtual string ToXML() { return ""; }
    }

    class RunFact : Action
    {

    }
}

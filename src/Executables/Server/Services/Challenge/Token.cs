﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Challenge
{
    class Token
    {
        class Generator
        {
            byte[] info = new byte[256];
            byte[] xor = new byte[256];

            Random rnd = new Random();

            public Generator()
            {
                for (int n = 0; n < info.Length; n++) { info[n] = 0; }
                rnd.NextBytes(xor);
            }
            public byte[] Generate()
            {
                if (info[info.Length - 1] == 0xFF) { for (int n = 0; n < info.Length; n++) { info[n] = 0; } }
                for (int n = 0; n < info.Length; n++) { if (info[n] == 0xFF) { info[n] = 0x00; } else { info[n]++; break; } }
                byte[] output = (byte[])info.Clone();
                for (int n = 0; n < output.Length; n++) { output[n] = (byte)((output[n] ^ xor[n]) & 0xFF); }
                return output;
            }
        }

        static Generator generator = new Generator();
        byte[] tokendata = null;
        string version = "";
        string owner = "";

        public bool IsOwnedBy(string UserID) { return owner == UserID; }

        public Token() : this(null) { }

        public Token(Fact.Network.Service.RequestInfo Info)
        {
            lock (generator) { tokendata = generator.Generate(); }
            version = Convert.ToBase64String(tokendata);
            if (Info != null) { owner = Info.UserID; }
        }
        public override string ToString()
        {
            return version;
        }
    }
}

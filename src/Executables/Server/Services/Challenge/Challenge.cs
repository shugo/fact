﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Challenge
{
    public class Challenge : Fact.Network.Service
    {
        class Lobby
        {
            internal string _Name = "";
            internal bool _Open = false;
            internal Token _Token = null;
            internal Dictionary<string, Team> _Teams = new Dictionary<string, Team>();
            public Lobby(Fact.Network.Service.RequestInfo Info) { _Token = new Token(Info); }
        }

        class Team
        {
            internal string _Name = "";
            internal Token _Token = null;
            internal List<Match> _Matches = new List<Match>();
            internal Dictionary<string, Registration> _Registrations = new Dictionary<string, Registration>();
            internal List<Player> _Players = new List<Player>();
            public Team(Fact.Network.Service.RequestInfo Info) { _Token = new Token(Info); }
        }

        class Registration
        {
            internal Lobby _Lobby = null;
            internal Team _Team = null;
            internal Token _Token = new Token();
        }

        class Match
        {
            internal Token _Token = new Token();
            internal List<Team> _Teams = new List<Team>();
            internal Queue<Message> _Messages = new Queue<Message>();
            internal Dictionary<string, Queue<Message>> _MessagesToPlayer = new Dictionary<string, Queue<Message>>();
            public Match(Fact.Network.Service.RequestInfo Info) { _Token = new Token(Info); }
            public Message DequeueMessage()
            {
                lock (this)
                {
                    if (_Messages.Count == 0) { return null; }
                    return _Messages.Dequeue();
                }
            }
            public Message DequeueMessageToPlayer(string Token)
            {
                lock (this)
                {
                    if (!_MessagesToPlayer.ContainsKey(Token)) { return null; }
                    if (_MessagesToPlayer[Token].Count == 0) { return null; }
                    return _MessagesToPlayer[Token].Dequeue();
                }
            }

            public void EnqueueMessage(Message Message) { lock (this) { _Messages.Enqueue(Message); } }
            public void EnqueueMessageToPlayer(Message Message, string Token)
            {
                lock (this)
                {
                    if (!_MessagesToPlayer.ContainsKey(Token)) { _MessagesToPlayer.Add(Token, new Queue<Message>()); }
                    _MessagesToPlayer[Token].Enqueue(Message);
                }
            }

        }

        class Player
        {
            internal Token _Token = new Token();
            internal string _Name = "";
            internal Team _Team = null;
            public Player(Fact.Network.Service.RequestInfo Info) { _Token = new Token(Info); }
        }

        class Message
        {
            internal Player _Player = null;
            internal Match _Match = null;
            internal byte[] _Data = null;
        }

        Dictionary<string, Lobby> _ChallengeInstances = new Dictionary<string, Lobby>();
        Dictionary<string, Team> _Teams = new Dictionary<string, Team>();
        Dictionary<string, Registration> _Registrations = new Dictionary<string, Registration>();
        Dictionary<string, Match> _Matches = new Dictionary<string, Match>();
        Dictionary<string, Player> _Players = new Dictionary<string, Player>();

        public Challenge()
            : base("Challenge")
        {

        }

        bool _LoggonInit = false;
        void InitLoggon(Fact.Network.Server Server)
        {
            lock (this)
            {
                if (_LoggonInit) { return; }
                Server.Disconnected += new EventHandler<Fact.Network.Server.UserEventArgs>(UserDisconnected);
                _LoggonInit = true;
            }
        }

        void UserDisconnected(object sender, Fact.Network.Server.UserEventArgs User)
        {
            Fact.Log.Verbose("Challenge is checking state for user " + User.UserID);
            List<string> lobbyToDelete = new List<string>();
            lock (_ChallengeInstances)
            {
                foreach (Lobby lobby in _ChallengeInstances.Values)
                {
                    if (lobby._Token.IsOwnedBy(User.UserID))
                    {
                        Fact.Log.Verbose("The lobby " + lobby._Name + " will be deleted because the user " + User.UserID + " is dead");

                        lobbyToDelete.Add(lobby._Token.ToString());
                    }
                }
            }
            foreach (string token in lobbyToDelete)
            {
                _DeleteLobby(User.UserID, token);
            }
            List<string> playerToDelete = new List<string>();
            lock (_Players)
            {
                foreach (Player player in _Players.Values)
                {
                    if (player._Token.IsOwnedBy(User.UserID))
                    {
                        Fact.Log.Verbose("The player " + player._Name + " will be deleted because the user " + User.UserID + " is dead");

                        playerToDelete.Add(player._Token.ToString());
                    }
                }
            }
            foreach (string token in playerToDelete)
            {
                _DeletePlayer(User.UserID, token);
            }

            List<string> teamToDelete = new List<string>();
            lock (_Teams)
            {
                foreach (Team team in _Teams.Values)
                {
                    if (team._Token.IsOwnedBy(User.UserID))
                    {
                        Fact.Log.Verbose("The team " + team._Name + " will be deleted because the user " + User.UserID + " is dead");
                        teamToDelete.Add(team._Token.ToString());
                    }
                }
            }
            foreach (string token in teamToDelete)
            {
                _DeleteTeam(User.UserID, token);
            }

            lock (_Matches)
            {
                List<string> matchToDelete = new List<string>();
                foreach (Match match in _Matches.Values)
                {
                    if (match._Token.IsOwnedBy(User.UserID))
                    {
                        Fact.Log.Verbose("The match " + match._Token + " will be deleted because the user " + User.UserID + " is dead");

                        matchToDelete.Add(match._Token.ToString());
                    }
                }
                foreach (string token in matchToDelete)
                {
                    _DeleteMatchNotThreadsafe(User.UserID, token);
                }
            }
            Fact.Log.Verbose("Challenge has checked state for user " + User.UserID);
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetLobbies(Fact.Network.Service.RequestInfo Info)
        {
            List<string> tokens = new List<string>();
            lock (_ChallengeInstances)
            {
                foreach (string token in _ChallengeInstances.Keys)
                { tokens.Add(token); }
            }
            return tokens.ToArray();
        }

        bool _DeleteLobby(string UserID, string LobbyToken)
        {
            Lobby lobby = null;
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyToken))
                {
                    lobby = _ChallengeInstances[LobbyToken];
                    if (!lobby._Token.IsOwnedBy(UserID)) { return false; }
                    _ChallengeInstances.Remove(LobbyToken);
                }
            }
            return true;
        }


        bool _DeletePlayer(string UserID, string PlayerToken)
        {
            Player player = null;
            lock (_Players)
            {
                if (!_Players.ContainsKey(PlayerToken)) { return false; }
                player = _Players[PlayerToken];
                if (!player._Token.IsOwnedBy(UserID)) { return false; }
                _Players.Remove(PlayerToken);
            }
            lock (player._Team) { player._Team._Players.Remove(player); }
            return true;
        }

        bool _DeleteTeam(string UserID, string TeamToken)
        {
            Team team = null;
            lock (_Teams)
            {
                if (!_Teams.ContainsKey(TeamToken)) { return false; }
                team = _Teams[TeamToken];
                if (!team._Token.IsOwnedBy(UserID)) { return false; }
                _Teams.Remove(TeamToken);
            }

            lock (team)
            {
                foreach (Registration registration in team._Registrations.Values)
                {
                    lock (registration._Lobby)
                    {
                        if (registration._Lobby._Teams.ContainsKey(TeamToken))
                        {
                            registration._Lobby._Teams.Remove(TeamToken);
                        }
                    }
                }
            }
            return true;
        }

        bool _DeleteMatchNotThreadsafe(string UserID, string MatchToken)
        {
            if (_Matches.ContainsKey(MatchToken))
            {
                Match match = _Matches[MatchToken];
                if (!match._Token.IsOwnedBy(UserID)) { return false; }
                _Matches.Remove(MatchToken);
                return true;
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public string CreateLobby(Fact.Network.Service.RequestInfo Info, string ChallengeName)
        {
            ChallengeName = ChallengeName.Trim();
            if (ChallengeName.Trim() == "") { return ""; }
            InitLoggon(Info.Server);
            lock (_ChallengeInstances)
            {
                Lobby lobby = new Lobby(Info);
                lobby._Name = ChallengeName;
                _ChallengeInstances.Add(lobby._Token.ToString(), lobby);
                return lobby._Token.ToString();
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetLobbyName(Fact.Network.Service.RequestInfo Info, string LobbyToken)
        {
            LobbyToken = LobbyToken.Trim();
            if (LobbyToken.Trim() == "") { return ""; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyToken))
                {
                    return _ChallengeInstances[LobbyToken]._Name;
                }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public bool DeleteLobby(Fact.Network.Service.RequestInfo Info, string LobbyToken)
        {
            LobbyToken = LobbyToken.Trim();
            if (LobbyToken.Trim() == "") { return false; }
            return _DeleteLobby(Info.UserID, LobbyToken);
        }

        [Fact.Network.ServiceExposedMethod]
        public bool OpenLobby(Fact.Network.Service.RequestInfo Info, string ChallengeToken)
        {
            ChallengeToken = ChallengeToken.Trim();
            if (ChallengeToken.Trim() == "") { return false; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(ChallengeToken))
                {
                    lock (_ChallengeInstances[ChallengeToken])
                    {
                        if (!_ChallengeInstances[ChallengeToken]._Token.IsOwnedBy(Info.UserID)) { return false; }
                        _ChallengeInstances[ChallengeToken]._Open = true;
                        return true;
                    }
                }

            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool CloseLobby(Fact.Network.Service.RequestInfo Info, string LobbyToken)
        {
            LobbyToken = LobbyToken.Trim();
            if (LobbyToken.Trim() == "") { return false; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyToken))
                {
                    lock (_ChallengeInstances[LobbyToken])
                    {
                        if (!_ChallengeInstances[LobbyToken]._Token.IsOwnedBy(Info.UserID)) { return false; }
                        _ChallengeInstances[LobbyToken]._Open = false;
                        return true;
                    }
                }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetTeamsInLobby(Fact.Network.Service.RequestInfo Info, string LobbyToken)
        {
            List<string> list = new List<string>();
            Lobby lobby = null;
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyToken)) { lobby = _ChallengeInstances[LobbyToken]; }
                else { return list.ToArray(); }
            }
            lock (lobby)
            {
                foreach (Team t in lobby._Teams.Values) { list.Add(t._Token.ToString()); }
            }
            return list.ToArray();
        }

        [Fact.Network.ServiceExposedMethod]
        public bool KickTeamFromLobby(Fact.Network.Service.RequestInfo Info, string LobbyToken, string TeamToken)
        {
            Lobby lobby = null;
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyToken)) { lobby = _ChallengeInstances[LobbyToken]; }
                else { return false; }
            }
            if (!lobby._Token.IsOwnedBy(Info.UserID)) { return false; }
            lock (lobby)
            {
                if (lobby._Teams.ContainsKey(TeamToken)) { lobby._Teams.Remove(TeamToken); return true; }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetTeamsInMatch(Fact.Network.Service.RequestInfo Info, string MatchToken)
        {
            List<string> list = new List<string>();
            Match match = null;
            lock (_Matches)
            {
                if (_Matches.ContainsKey(MatchToken)) { match = _Matches[MatchToken]; }
                else { return list.ToArray(); }
            }
            lock (match)
            {
                foreach (Team t in match._Teams)
                {
                    list.Add(t._Token.ToString());
                }
            }
            return list.ToArray();
        }

        [Fact.Network.ServiceExposedMethod]
        public string CreateTeam(Fact.Network.Service.RequestInfo Info, string TeamName)
        {
            TeamName = TeamName.Trim();
            if (TeamName == "") { return ""; }
            InitLoggon(Info.Server);
            lock (_Teams)
            {
                Team team = new Team(Info);
                team._Name = TeamName;
                _Teams.Add(team._Token.ToString(), team);
                return team._Token.ToString();
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public bool DeleteTeam(Fact.Network.Service.RequestInfo Info, string TeamToken)
        {
            TeamToken = TeamToken.Trim();
            if (TeamToken == "") { return false; }
            return _DeleteTeam(Info.UserID, TeamToken);
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetTeamName(Fact.Network.Service.RequestInfo Info, string TeamToken)
        {
            lock (_Teams)
            {
                if (_Teams.ContainsKey(TeamToken))
                {
                    return _Teams[TeamToken]._Name;
                }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public string RegisterTeam(Fact.Network.Service.RequestInfo Info, string TeamToken, string LobbyToken)
        {
            TeamToken = TeamToken.Trim();
            LobbyToken = LobbyToken.Trim();
            if (TeamToken == "") { return ""; }
            if (LobbyToken == "") { return ""; }
            Lobby lobby = null;
            Team team = null;
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyToken)) { lobby = _ChallengeInstances[LobbyToken]; }
                else { return ""; }
            }
            lock (lobby) { if (!lobby._Open) { return ""; } }
            lock (_Teams)
            {
                if (_Teams.ContainsKey(TeamToken)) { team = _Teams[TeamToken]; }
                else { return ""; }
            }
            lock (lobby)
            {
                if (lobby._Teams.ContainsKey(TeamToken)) { return ""; }
                lobby._Teams.Add(TeamToken, team);
            }
            Registration registration = new Registration();
            registration._Team = team;
            registration._Lobby = lobby;
            lock (team) { team._Registrations.Add(registration._Token.ToString(), registration); }
            return registration._Token.ToString();
        }

        [Fact.Network.ServiceExposedMethod]
        public string CreateMatch(Fact.Network.Service.RequestInfo Info, string LobbyToken, string[] TeamTokens)
        {
            LobbyToken = LobbyToken.Trim();
            if (LobbyToken == "") { return ""; }
            Lobby lobby = null;
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyToken)) { lobby = _ChallengeInstances[LobbyToken]; }
                else { return ""; }
            }
            List<Team> teams = new List<Team>();
            lock (_Teams)
            {
                foreach (string token in TeamTokens)
                {
                    if (_Teams.ContainsKey(token))
                    {
                        teams.Add(_Teams[token]);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            Match match = new Match(Info);
            match._Teams = teams;
            lock (_Matches) { _Matches.Add(match._Token.ToString(), match); }
            foreach (Team team in teams)
            {
                lock (team)
                {
                    team._Matches.Add(match);
                }
            }
            return match._Token.ToString();
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public bool DeleteMatch(Fact.Network.Service.RequestInfo Info, string MatchToken)
        {
            MatchToken = MatchToken.Trim();
            if (MatchToken == "") { return false; }
            lock (_Matches)
            {
                return _DeleteMatchNotThreadsafe(Info.UserID, MatchToken);
            }
            return true;
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetMatchesForTeam(Fact.Network.Service.RequestInfo Info, string TeamToken)
        {
            Team team = null;
            lock (_Teams)
            {
                if (_Teams.ContainsKey(TeamToken)) { team = _Teams[TeamToken]; }
                else { return null; }
            }
            List<string> matches = new List<string>();
            foreach (Match match in team._Matches)
            {
                matches.Add(match._Token.ToString());
            }
            return matches.ToArray();
        }

        [Fact.Network.ServiceExposedMethod]
        public string JoinTeam(Fact.Network.Service.RequestInfo Info, string PlayerName, string TeamToken)
        {
            PlayerName = PlayerName.Trim();
            if (PlayerName == "") { return ""; }

            Team team = null;
            TeamToken = TeamToken.Trim();
            if (TeamToken == "") { return ""; }

            lock (_Teams)
            {
                if (_Teams.ContainsKey(TeamToken)) { team = _Teams[TeamToken]; }
                else { return ""; }
            }
            Player player = new Player(Info);
            player._Name = PlayerName;
            player._Team = team;
            lock (team) { team._Players.Add(player); }
            lock (_Players) { _Players.Add(player._Token.ToString(), player); }
            return player._Token.ToString();
        }

        [Fact.Network.ServiceExposedMethod]
        public string GetPlayerName(Fact.Network.Service.RequestInfo Info, string PlayerToken)
        {
            lock (_Players)
            {
                if (_Players.ContainsKey(PlayerToken))
                {
                    return _Players[PlayerToken]._Name;
                }
            }
            return "";
        }

        [Fact.Network.ServiceExposedMethod]
        public string[] GetPlayersInTeam(Fact.Network.Service.RequestInfo Info, string TeamToken)
        {
            List<string> players = new List<string>();
            Team team = null;

            lock (_Teams)
            {
                if (!_Teams.ContainsKey(TeamToken)) { return null; }
                team = _Teams[TeamToken];
            }
            lock (team)
            {
                foreach (Player player in team._Players)
                {
                    players.Add(player._Token.ToString());
                }
            }

            return players.ToArray();
        }

        [Fact.Network.ServiceExposedMethod]
        public bool EnqueueMessageFromPlayer(Fact.Network.Service.RequestInfo Info, string MatchToken, string PlayerToken, byte[] Data)
        {
            lock (_Matches)
            {
                if (_Matches.ContainsKey(MatchToken))
                {
                    lock (_Players)
                    {
                        if (_Players.ContainsKey(PlayerToken))
                        {
                            if (!_Players[PlayerToken]._Token.IsOwnedBy(Info.UserID)) { return false; }
                            Message message = new Message();
                            message._Data = Data;
                            message._Match = _Matches[MatchToken];
                            message._Player = _Players[PlayerToken];
                            _Matches[MatchToken].EnqueueMessage(message);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool EnqueueMessageToPlayer(Fact.Network.Service.RequestInfo Info, string MatchToken, string PlayerToken, byte[] Data)
        {
            lock (_Matches)
            {
                if (_Matches.ContainsKey(MatchToken))
                {
                    lock (_Players)
                    {
                        if (_Players.ContainsKey(PlayerToken))
                        {
                            if (!_Matches[MatchToken]._Token.IsOwnedBy(Info.UserID)) { return false; }
                            Message message = new Message();
                            message._Data = Data;
                            message._Match = _Matches[MatchToken];
                            message._Player = _Players[PlayerToken];
                            _Matches[MatchToken].EnqueueMessageToPlayer(message, PlayerToken);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public object[] DequeueMessageFromPlayer(Fact.Network.Service.RequestInfo Info, string MatchToken)
        {
            lock (_Matches)
            {
                if (_Matches.ContainsKey(MatchToken))
                {
                    if (!_Matches[MatchToken]._Token.IsOwnedBy(Info.UserID)) { return null; }
                    object[] array = new object[2];
                    Message message = _Matches[MatchToken].DequeueMessage();
                    if (message == null) { array[0] = null; array[1] = null; return array; }
                    array[0] = message._Player._Token.ToString();
                    array[1] = message._Data;
                    return array;
                }
            }
            return null;
        }

        [Fact.Network.ServiceExposedMethod]
        public byte[] DequeueMessageToPlayer(Fact.Network.Service.RequestInfo Info, string MatchToken, string PlayerToken)
        {
            lock (_Matches)
            {
                if (_Matches.ContainsKey(MatchToken))
                {
                    lock (_Players)
                    {
                        if (_Players.ContainsKey(PlayerToken))
                        {
                            if (!_Players[PlayerToken]._Token.IsOwnedBy(Info.UserID)) { return null; }
                            Message message = _Matches[MatchToken].DequeueMessageToPlayer(PlayerToken);
                            return message._Data;
                        }
                    }
                }
            }
            return null;
        }
    }
}

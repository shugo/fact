﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentServer
{
    public class Agent : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Agent";
            }
        }

        public Agent()  { }

        internal class Action
        {
            internal string _Type = "";
            internal List<string> _Content = new List<string>();
        }

        internal class Job
        {
            internal List<Fact.Processing.File> _ShadowLocalStorage = new List<Fact.Processing.File>();
            internal Dictionary<string, Fact.Processing.File> _JobLocalStorage = new Dictionary<string, Fact.Processing.File>();
            internal Dictionary<string, string> _JobLocalStorageHash = new Dictionary<string, string>();
            internal Dictionary<string, List<string>> _JobLocalStorageReverseHash = new Dictionary<string, List<string>>();

            public Dictionary<string, Fact.Processing.File> GetLoadedFiles()
            {
                Dictionary<string, Fact.Processing.File> loadedFiles = new Dictionary<string, Fact.Processing.File>();
                foreach (Fact.Processing.File file in _ShadowLocalStorage)
                {
                    if (!loadedFiles.ContainsKey(file.ContentHash))
                    {
                        Fact.Processing.File _WAR_File = file;
                        loadedFiles.Add(file.ContentHash, _WAR_File);
                    }
                }
                foreach (Fact.Processing.File file in _JobLocalStorage.Values)
                {
                    if (!loadedFiles.ContainsKey(file.ContentHash))
                    {
                        Fact.Processing.File _WAR_File = file;
                        loadedFiles.Add(file.ContentHash, _WAR_File);
                    }
                }
                return loadedFiles;
            }

            public void PreloadFiles(Dictionary<string, Fact.Processing.File> files)
            {
                foreach (string hash in files.Keys)
                {
                    if (_JobLocalStorageReverseHash.ContainsKey(hash))
                    {
                        foreach (string file in _JobLocalStorageReverseHash[hash])
                        {
#if DEBUG
                            Fact.Log.Debug("File '" + file + "' has been preloaded");
#endif
                            if (!_JobLocalStorage.ContainsKey(file))
                            { _JobLocalStorage.Add(file, files[hash]); }
                            else if (_JobLocalStorage[file] == null)
                            { _JobLocalStorage[file] = files[hash]; }
                        }
                    }
                }
            }

            public Fact.Processing.File GetFileInJobLocalStorage(string File)
            {
                if (!_JobLocalStorageHash.ContainsKey(File)) { return null; }

                if (_JobLocalStorage.ContainsKey(File) && _JobLocalStorage[File] != null)
                { return _JobLocalStorage[File]; }

                Fact.Processing.File FileData = _Client.CallWithTimeout("AgentManager.GetFileInJobLocalStorage", 60000, _Token, File) as Fact.Processing.File;
                if (FileData != null)
                {
#if DEBUG
                    Fact.Log.Debug("File '" + File + "' has been downloaded");
#endif
                    if (!_JobLocalStorage.ContainsKey(File)) { _JobLocalStorage.Add(File, FileData); }
                    else { _JobLocalStorage[File] = FileData; }
                }
                return FileData;

            }
            internal List<Action> _Actions = new List<Action>();
            internal Fact.Network.Client _Client = null;
            internal int _Token = 0;
            public string ToXML()
            {
                StringBuilder jobxml = new StringBuilder();
                jobxml.Append("<job>");
                jobxml.Append("<token>"); jobxml.Append(_Token.ToString()); jobxml.Append("</token>");
                jobxml.Append("<local>");
                foreach (KeyValuePair<string, Fact.Processing.File> obj in _JobLocalStorage)
                {
                    jobxml.Append("<element>");
                    jobxml.Append("<name>"); jobxml.Append(obj.Key); jobxml.Append("</name>");
                    jobxml.Append("<data>"); jobxml.Append(System.Convert.ToBase64String(obj.Value.GetBytes())); jobxml.Append("</data>");
                    jobxml.Append("</element>");
                }
                jobxml.Append("</local>");
                jobxml.Append("<actions>");
                foreach (Action action in _Actions)
                {
                    jobxml.Append("<action>");
                    jobxml.Append("<type>");
                    jobxml.Append(action._Type);
                    jobxml.Append("</type>");
                    jobxml.Append("<contents>");
                    foreach (string content in action._Content)
                    {
                        jobxml.Append("<content>");
                        jobxml.Append(Fact.Network.Tools.EscapeURIString(content));
                        jobxml.Append("</content>");
                    }
                    jobxml.Append("</contents>");
                    jobxml.Append("</action>");
                }
                jobxml.Append("</actions>");
                jobxml.Append("</job>");
                return jobxml.ToString();
            }
        }

        internal Job ParseJob(string JobXML)
        {
            try
            {
                Fact.Parser.XML XML = new Fact.Parser.XML();
                XML.Parse(JobXML);
                if (XML.Root == null) { Fact.Log.Error("Job parsing error: No root node"); return null; }
                Fact.Parser.XML.Node JobNode = XML.Root.FindNode("job", false);
                if (JobNode == null) { Fact.Log.Error("Job parsing error: No job node"); return null; }
                Fact.Parser.XML.Node Local = JobNode.FindNode("local", false);
                Fact.Parser.XML.Node Actions = JobNode.FindNode("actions", false);
                Fact.Parser.XML.Node Token = JobNode.FindNode("token", false);
                if (Actions == null) { Fact.Log.Warning("Job parsing error: No actions node"); return null; }
                Agent.Job Job = new Job();
                if (Local != null)
                {
                    foreach (Fact.Parser.XML.Node node in Local.Children)
                    {
                        if (node.Type.ToLower() == "element")
                        {
                            Fact.Parser.XML.Node Name = node.FindNode("name", false);
                            Fact.Parser.XML.Node Hash = node.FindNode("hash", false);
                            Fact.Parser.XML.Node Data = node.FindNode("data", false);
                            if (Name != null && Name.Children.Count == 1 && Data != null && Data.Children.Count == 1)
                            {
                                try
                                {
                                    string FileName = Name.Children[0].Text;
                                    byte[] FileData = Convert.FromBase64String(Data.Children[0].Text);
                                    Fact.Processing.File factfile = new Fact.Processing.File(FileData, FileName);
                                    Job._JobLocalStorage.Add(FileName, factfile);
                                    Job._JobLocalStorageHash.Add(FileName, factfile.ContentHash);
                                }
                                catch { }
                            }
                            else if (Name != null && Name.Children.Count == 1 && Hash != null && Hash.Children.Count == 1)
                            {
                                try
                                {
                                    string FileName = Name.Children[0].Text;
                                    string HashText = Hash.Children[0].Text;
                                    Job._JobLocalStorageHash.Add(FileName, HashText);
                                    if (!Job._JobLocalStorageReverseHash.ContainsKey(HashText))
                                    {
                                        Job._JobLocalStorageReverseHash.Add(HashText, new List<string>());
                                    }
                                    Job._JobLocalStorageReverseHash[HashText].Add(FileName);
                                }
                                catch { }
                            }
                        }
                    }
                }
                {
                    foreach (Fact.Parser.XML.Node node in Actions.Children)
                    {
                        if (node.Type.ToLower() == "action")
                        {
                            Fact.Parser.XML.Node Type = node.FindNode("type", false);
                            Fact.Parser.XML.Node Contents = node.FindNode("contents", false);
                            if (Type != null && Type.Children.Count == 1 && Contents != null)
                            {
                                string ActionType = Type.Children[0].Text;
                                List<string> ActionContent = new List<string>();
                                foreach (Fact.Parser.XML.Node contentnode in Contents.Children)
                                {
                                    if (contentnode.Type.ToLower() == "content" && contentnode.Children.Count == 1)
                                    {
                                        ActionContent.Add(Uri.UnescapeDataString(contentnode.Children[0].Text));
                                    }
                                }
                                Job._Actions.Add(new Action() { _Type = ActionType,  _Content = ActionContent });
                            }
                        }
                    }
                }
                if (Token != null && Token.Children.Count > 0) { int.TryParse(Token.Children[0].Text, out Job._Token); }
                return Job;
            }
            catch { return null; }
        }

        internal string PathSolver(string Path, string WorkingDir)
        {
            if (Path.StartsWith("@//")) { return WorkingDir + "/" + Path.Substring("@//".Length); }
            if (Path.StartsWith("./")) { return WorkingDir + "/" + Path.Substring("./".Length); }
            return Path;
        }

        internal string FindFactExe()
        {
            foreach (System.Reflection.Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.GetName().Name.ToLower().Contains("factexe") && assembly.EntryPoint != null)
                {
                    return assembly.Location;
                }
            }
            return "";
        }

        internal void DoJob(Job Job, Fact.Network.Client Client)
        {
            bool forwardScreen = false;
            if (Job._Client == null) { Job._Client = Client; }
            if (Job == null) { return; }
            Fact.Log.Verbose("Running Job " + Job._Token.ToString() + " ...");
            string tmp_dir = Fact.Tools.CreateTempDirectory();
            if (!System.IO.Directory.Exists(tmp_dir)) { Fact.Log.Error("Failed to create temp working directory"); return; }
            int timeoutValue = -1;
            try
            {
                foreach (Action action in Job._Actions)
                {
                    switch (action._Type.ToLower())
                    {
                        case "forwardScreen":
                            forwardScreen = true;
                            break;
                        case "copyfilefromjoblocalstorage":
                            if (action._Content.Count != 2 || !(
                                Job._JobLocalStorage.ContainsKey(action._Content[1]) ||
                                Job._JobLocalStorageHash.ContainsKey(action._Content[1]))
                                )
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            Job.GetFileInJobLocalStorage(action._Content[1]).Extract(PathSolver(action._Content[0], tmp_dir));
                            Fact.Log.Verbose("File " + action._Content[1] + " extracted at " + PathSolver(action._Content[0], tmp_dir));
                            break;
                        case "shutdown":
                            Client.CallWithTimeout("AgentManager.SendJobResult", 60000, Job._Token, Job.ToXML());
                            Fact.Log.Verbose("This agent will be shutted down");
                            System.Environment.Exit(0);
                            break;
                        case "setenvvariable":
                            if (action._Content.Count != 2)
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            string var = action._Content[0];
                            string value = action._Content[1];

                            Fact.Log.Verbose("Set the environement variable " + var + "=" + value);
                            // FIX ME: store the env
                            break;
                        case "settimeout":
                            timeoutValue = int.Parse(action._Content[0]);
                            break;
                        case "runcommand":
                        case "runcommandandredirectresult":
                            bool saveresult = (action._Type.ToLower() == "runcommandandredirectresult");
                            if (action._Content.Count == 0)
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            string arguments = "";
                            int n = 1;
                            string stdout = "";
                            string stderr = "";

                            if (saveresult)
                            {
                                stdout = action._Content[n]; n++; stdout = PathSolver(stdout, tmp_dir);
                                stderr = action._Content[n]; n++; stderr = PathSolver(stderr, tmp_dir);
                            }
                            
                            for (; n < action._Content.Count; n++) { arguments += " \"" + action._Content[n] + "\" "; }
                            string program = action._Content[0];
                            if (program.Trim().ToLower() == "fact") { program = FindFactExe(); }
                            if (program.StartsWith("./")) { program = tmp_dir + "/" + program; }
                            if (!System.IO.File.Exists(program)) { program = Fact.Internal.Information.Where(action._Content[0]); }
                            if (!System.IO.File.Exists(program)) { Fact.Log.Warning("Executable not found " + action._Content[0]); break; }
                            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix ||
                                Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
                            {
                                // Custom path for .NET Executable using mono
                                string mono = Fact.Internal.Information.GetMonoBinary();
                                if (Fact.Internal.Information.IsDotNetExecutable(program))
                                {
                                    if (System.IO.File.Exists(mono))
                                    {
                                        // Use the same runtime as the one used by this command
                                        string runtimename = "CLR 2.0";
                                        string runtime = "--runtime=\"v2.0\"";
                                        if (System.Environment.Version.Major == 2) { runtime = "--runtime=\"v2.0\""; runtimename = "CLR 2.0"; }
                                        else if (System.Environment.Version.Major >= 4) { runtime = "--runtime=\"v4.0\""; runtimename = "CLR 4.0"; }
                                        using (Fact.Runtime.Process Process = new Fact.Runtime.Process(mono, System.IO.Path.GetFullPath(tmp_dir), runtime + " " + program + " " + arguments))
                                        {
                                            Fact.Log.Verbose("Run command " + program + " " + arguments + " (" + runtimename + ")...");
                                            Fact.Runtime.Process.Result result = Process.Run(timeoutValue);
                                            if (Client != null) { Client.Call("AgentManager.SendJobLog", Job._Token, result.StdOut, result.StdErr); }
                                            Fact.Log.Auto(result.StdOut);
                                            if (saveresult)
                                            {
                                                if (stdout != "") { System.IO.File.WriteAllText(stdout, result.StdOut); }
                                                if (stderr != "") { System.IO.File.WriteAllText(stderr, result.StdErr); }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

                            using (Fact.Runtime.Process Process = new Fact.Runtime.Process(program, System.IO.Path.GetFullPath(tmp_dir), arguments))
                            {
                                Fact.Log.Verbose("Run command " + program + " " + arguments + "...");
                                Fact.Runtime.Process.Result result = Process.Run(-1);
                                if (Client != null) { Client.Call("AgentManager.SendJobLog", Job._Token, result.StdOut, result.StdErr); }
                                Fact.Log.Auto(result.StdOut);
                                if (saveresult)
                                {
                                    if (stdout != "") { System.IO.File.WriteAllText(stdout, result.StdOut); }
                                    if (stderr != "") { System.IO.File.WriteAllText(stderr, result.StdErr); }
                                }
                            }

                           
                            break;
                        case "copyfiletojoblocalstorage":
                            if (action._Content.Count != 2)
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            string localpath = PathSolver(action._Content[0], tmp_dir);
                            if (!System.IO.File.Exists(localpath))
                            { Fact.Log.Warning("File not found " + localpath); break; }
                            Fact.Processing.File FileToCopy = new Fact.Processing.File(System.IO.File.ReadAllBytes(localpath), action._Content[1]);
                            Job._JobLocalStorage.Add(action._Content[1], FileToCopy);
                            Job._JobLocalStorageHash.Add(action._Content[1], FileToCopy.ContentHash);

                            Fact.Log.Verbose("File " + action._Content[1] + " saved");
                            break;
                        case "deletefilefromjoblocalstorage":
                            if (action._Content.Count != 1)
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            if (Job._JobLocalStorage.ContainsKey(action._Content[0]))
                            {
                                Job._ShadowLocalStorage.Add(Job._JobLocalStorage[action._Content[0]]);
                                Job._JobLocalStorage.Remove(action._Content[0]);
                                Fact.Log.Verbose("File " + action._Content[0] + " deleted");
                            }
                            if (Job._JobLocalStorageHash.ContainsKey(action._Content[0]))
                            { Job._JobLocalStorageHash.Remove(action._Content[0]); }
                            break;
                        default: Fact.Log.Warning("Unknow action : " + action._Type); break;
                    }
                }
                Fact.Log.Verbose("Sending the result job the server");
                Client.CallWithTimeout("AgentManager.SendJobResult", 60000, Job._Token, Job.ToXML());
            }
            catch
            {
                Fact.Log.Error("Unexpected error while running the job");
                Client.CallWithTimeout("AgentManager.SendJobResult", Job._Token, "");
            }
            Fact.Tools.RecursiveDelete(tmp_dir);
        }

        public override int Run(params string[] args)
        {

            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("server", "The address of the fact server hosting the agent manager", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddStringInput("credential", "The credentials to use to reach the fact server", ""); CommandLine.AddShortInput("c", "credential");
            CommandLine.AddStringInput("group", "The group of the agent (leave empty if no group)", ""); CommandLine.AddShortInput("g", "group");
            CommandLine.AddStringInput("name", "The name of the agent", ""); CommandLine.AddShortInput("n", "name");
            CommandLine.AddStringInput("inactivity-shutdown", "Shutdown the agent after N millisecond of inactivity", "-1");

            CommandLine.AddBooleanInput("help", "Display this message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }
            string _Server = CommandLine["server"].AsString();
            string _CredentialFile = CommandLine["credential"].IsDefault ? "" : CommandLine["credential"];
            string group = CommandLine["group"].AsString();
            string name = CommandLine["name"].AsString();
            string shudownTimeoutString = CommandLine["inactivity-shutdown"].AsString();
            int ShutdownTimeout = -1;
            if (!int.TryParse(shudownTimeoutString, out ShutdownTimeout))
            {
                Fact.Log.Error("Ivalid value for inactivity-shutdown: " + shudownTimeoutString);
            }
            string token = "";

            Fact.Network.Client Client = Fact.Common.Remote.Connection.Connect(_Server, _CredentialFile);

            Fact.Log.Verbose("Register agent ...");
            try 
            {
                token = Client.Call("AgentManager.Register", name) as String;
            }
            catch (Exception e)
            {
                Fact.Log.Error("Impossible to register the agent: " + e.Message);
                return 1;
            }
            if (token == null || token.Trim() == "")
            {
                Fact.Log.Error("Impossible to register the agent: The server has rejected the registration");
                return 1;
            }
            Fact.Log.Verbose("Agent registered as: {" + token + "}");
            int waittime = -10;
            bool failure = false;
            Job current = null;
            Job old = null;
            DateTime lastJobDate = DateTime.Now;
            while (true)
            {
                try
                {
                    string job = Client.CallWithTimeout("AgentManager.GetJob", 60000, token, group) as string;
                    if (job == null)
                    {
                        failure = true;
                        Fact.Log.Warning("The server has rejected the request");
                    }
                    if (job != null && job != "")
                    {
                        failure = false;
                        waittime = -10;
                        try
                        {
                            current = ParseJob(job);
                            if (old != null) { current.PreloadFiles(old.GetLoadedFiles()); }
                            old = current;
                            try
                            {
                                // We don't wont to see remaining data in memory while executing the process
                                System.GC.Collect();
                            }
                            catch { }
                            Fact.Threading.Job.Status JobThread = Fact.Threading.Job.CreateJob(()=>
                            {
                                try
                                {
                                    DoJob(current, Client);
                                }
                                catch { Fact.Log.Error("Critical error during the execution of the process"); }
                            });
                            while (JobThread.CurrentState != Fact.Threading.Job.Status.State.Ended)
                            {
                                try
                                {
                                    bool finished = (bool)Client.CallWithTimeout("AgentManager.IsJobFinished", 10000, current._Token);
                                    if (finished && JobThread.CurrentState != Fact.Threading.Job.Status.State.Ended)
                                    {
                                        Fact.Log.Error("The job is no longer active on the server and will be killed");
                                        JobThread.Abort();
                                    }
                                }
                                catch 
                                {
                                    if (JobThread.CurrentState != Fact.Threading.Job.Status.State.Ended)
                                    {
                                        Fact.Log.Error("Connection with the server broken");
                                        JobThread.Abort();
                                    }
                                }
                                JobThread.Join(1000);
                            }
                        }
                        catch { Fact.Log.Error("Critical error during the execution of the process"); }

                        try
                        {
                            //Collect all the memory used by .NET
                            System.GC.Collect();
                        }
                        catch { }
                        lastJobDate = DateTime.Now;
                        continue;
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Warning("Communication error: " + e.Message);
                    // Count the communication error to be able to reinstigate the connection later
                    failure = true;
                    waittime = 1000;
                }

                if (failure)
                {
                    failure = false;
                    // Try to instigate a fresh connection
                    Fact.Log.Warning("Try to register the agent again");
                    try
                    {
                        token = Client.Call("AgentManager.Register", name) as String;
                        if (token == null || token.Trim() == "") { continue; }
                        Fact.Log.Verbose("Agent registered as: {" + token + "}");
                    }
                    catch { }
                }

                if (ShutdownTimeout > 0)
                {
                    if ((DateTime.Now - lastJobDate).TotalMilliseconds > ShutdownTimeout)
                    {
                        Fact.Log.Warning("The agent was inactive for more than " + ShutdownTimeout / 1000 + "s");
                        Fact.Log.Warning("Shutting down");
                        return 0;
                    }
                }

                if (waittime > 0)
                {
                    System.Threading.Thread.Sleep(waittime);
                }
                if (waittime <= 500) { waittime++; }
            }

            return 0;
        }
    }
}

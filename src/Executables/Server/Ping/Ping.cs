﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ping
{
    public class Ping : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Ping";
            }
        }
        public override int Run(params string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("timeout", "Timeout of the ping request", "1s"); CommandLine.AddShortInput("t", "timeout");
            CommandLine.AddStringInput("server", "The server that must respond to the ping request", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddBooleanInput("help", "Display the help message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args))
            {
                CommandLine.PrintErrors();
                return 1;
            }
            if (CommandLine["help"].AsBoolean())
            {
                CommandLine.PrintUsage();
                return 0;
            }
            string server = CommandLine["server"].AsString();
            string timeoutstr = CommandLine["timeout"].AsString();
            int timeout = 1000;
            int factor = 1;
            if (timeoutstr.EndsWith("ms")) { factor = 1; timeoutstr = timeoutstr.Substring(0, timeoutstr.Length - 2); }
            else if (timeoutstr.EndsWith("s")) { factor = 1000; timeoutstr = timeoutstr.Substring(0, timeoutstr.Length - 1); }
            if (!int.TryParse(timeoutstr, out timeout))
            {
                Fact.Log.Error("The specified timeout is not a valid");
                return 1;
            }
            timeout *= factor;
            if (server == "")
            {
                Fact.Log.Error("The specified server name is not a valid");
                return 1;
            }
retry:
            if (Fact.Network.Client.Ping(server, timeout))
            {
                Fact.Log.Verbose(server + "  responded");
                Version version = Fact.Network.Client.Version(server, timeout);
                if (version.Major != 0)
                {
                    Fact.Log.Verbose(server + " version: " +version.Major + "." + version.Minor + "." + version.Build);
                }
                return 0;
            }
            else
            {
                if (!server.Contains(":")) { server = server + ":9089"; goto retry; }

                Fact.Log.Error(server + "  did not respond");
                return 1;
            }

        }
    }
}

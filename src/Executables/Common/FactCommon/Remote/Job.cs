﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Remote
{
    public class Job
    {
        int ID = -1;
        Fact.Network.Client _Client = null;
        bool _Published = false;
        string _LastError = "";

        public string GetLastError() { return _LastError; }

        public bool Valid
        {
            get
            {
                return ID >= 0 && _Client != null;
            }
        }
        public bool Finished
        {
            get
            {
                if (_Client == null) { return true; }
                if (ID == -1) { return true; }
                try
                {
                    if (!_Published) { return false; }
                    return (bool)_Client.Call("AgentManager.IsJobFinished", ID);
                }
                catch (Exception e) { _LastError = e.Message; return true; }
            }
        }

        public string Name
        {
            get
            {
                if (_Client == null) { return ""; }
                if (ID == -1) { return ""; }
                try
                {
                    return (string)_Client.Call("AgentManager.GetJobName", ID);
                }
                catch (Exception e) { _LastError = e.Message; return ""; }
            }
            set
            {
                if (_Client == null) { return; }
                if (ID == -1) { return; }
                try
                {
                    bool done = (bool)_Client.Call("AgentManager.SetJobName", ID, value);
                    if(!done)
                    {
                        _LastError = "Impossible to set the name of the job";
                    }
                }
                catch (Exception e) { _LastError = e.Message; return; }
            }
        }

        public string StdOut
        {
            get
            {
                if (_Client == null) { return ""; }
                if (ID == -1) { return ""; }
                try
                {
                    if (!_Published) { return ""; }
                    string result = (string)_Client.Call("AgentManager.GetJobStdOutLog", ID);
                    if (result != null) { return result; }
                }
                catch (Exception e) { _LastError = e.Message; }
                return "";
            }
        }

        public string StdErr
        {
            get
            {
                if (_Client == null) { return ""; }
                if (ID == -1) { return ""; }
                try
                {
                    if (!_Published) { return ""; }
                    string result = (string)_Client.Call("AgentManager.GetJobStdErrLog", ID);
                    if (result != null) { return result; }
                }
                catch (Exception e) { _LastError = e.Message; }
                return "";
            }
        }

        public Job(Fact.Network.Client Client, string Group)
        {
            _Client = Client;
            try
            {
                ID = (int)Client.Call("AgentManager.CreateJob", Group);
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; }
        }

        public void Publish()
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }
            _Published = true;
            try
            {
                if (!(bool)_Client.Call("AgentManager.PublishJob", ID)) { ID = -1; }
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; }
        }

        public void SendFile(string File, string Path)
        {
            if (ID < 0) { return; }
            if (_Client != null) { return; }
            if (_Published) { return; }
            if (!System.IO.File.Exists(File)) { return; }
            try
            {
                SendFile(new Fact.Processing.File(System.IO.File.ReadAllBytes(File), System.IO.Path.GetFileName(Path)), Path);
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; }
        }

        public void SendFile(Fact.Processing.File File, string Path)
        {
#if DEBUG
            Fact.Log.Debug("Uploading file '" + Path + "' on the server ...");
#endif
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }

            while (Path.StartsWith("./")) { Path = Path.Substring("./".Length); }

            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.AddFileInJobLocalStorageFromCache", ID, File.ContentHash, Path));
                if (!status)
                {
#if DEBUG
                    Fact.Log.Debug("File '" + Path + "' is not present in server cache and will be uploaded");
#endif
                    status = (bool)(_Client.CallWithTimeout("AgentManager.AddFileInJobLocalStorage", 60000, ID, File, Path));
                    if (status == false) { _LastError = "Impossible to upload file"; ID = -1; return; }
                }
                status = (bool)(_Client.Call("AgentManager.PushAction_CopyFileFromJobLocalStorageToAgent", ID, "./" + Path, Path));
                if (status == false) { _LastError = "Impossible to upload file"; ID = -1; return; }
            }
            catch (Exception e)
            {
#if DEBUG
                Fact.Log.Debug("Error while uploading the file '" + Path + "'");
                Fact.Log.Exception(e);
#endif
                _LastError = e.Message; ID = -1;
            }
        }

        void SetTimeout(int Value)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }
            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.PushAction_SetTimeout", ID, Value));
                if (status == false) { _LastError = "Impossible to set the global timeout"; ID = -1; return; }
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; throw (e); }
        }

        public void Run(string Command, params string[] args)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }
            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.PushAction_RunCommandOnAgent", ID, Command, args));
                if (status == false) { _LastError = "Impossible to push a command"; ID = -1; return; }
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; }

        }

        public void ExportFile(string Path)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }

            while (Path.StartsWith("./")) { Path = Path.Substring("./".Length); }

            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.PushAction_CopyFileFromAgentToJobLocalStorage", ID, "./" + Path, Path));
                if (status == false) { _LastError = "Impossible to push a command"; ID = -1; return; }
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; }
        }


        public void DeleteFile(string Path)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }

            while (Path.StartsWith("./")) { Path = Path.Substring("./".Length); }

            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.PushAction_DeleteFileFromJobLocalStorage", ID, Path));
                if (status == false) { _LastError = "Impossible to push a command"; ID = -1; return; }
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; }
        }
        public Fact.Processing.File GetFile(string Path)
        {
            if (ID < 0) { return null; }
            if (_Client == null) { return null; }
            if (!_Published) { return null; }
            try
            {
                return _Client.CallWithTimeout("AgentManager.GetFileInJobLocalStorage", 60000, ID, "output.ff") as Fact.Processing.File;
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; return null; }
        }

        public void Delete()
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (!_Published) { return; }
            try
            {
                _Client.Call("AgentManager.DeleteJob", ID);
            }
            catch (Exception e) { _LastError = e.Message; ID = -1; return; }
        }

        public void Execute()
        {
            Publish();
            while (!Finished) { System.Threading.Thread.Sleep(50); }
        }
    }
}

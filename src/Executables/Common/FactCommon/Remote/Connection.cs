﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Remote
{
    public static class Connection
    {
        public static Fact.Network.Client Connect(string Server, string CredentialFile)
        {
            if (Server.Trim() == "") { Server = "localhost"; }

            if (!Server.ToLower().StartsWith("http://") &&
                !Server.ToLower().StartsWith("https://"))
            {
                Server = "http://" + Server;
            }
            string portString = "";
            int port = 0;
            for (int n = Server.Length - 1; n > 0 && Server[n] != ':'; n--) { portString = Server[n] + portString; }
            if (!int.TryParse(portString, out port))
            {
                port = 9089;
                if (Fact.Network.Client.Ping(Server + ":9089",      50))   { Server = Server + ":9089"; port = 9089; }
                else if (Fact.Network.Client.Ping(Server + ":80",   50))   { Server = Server + ":80"; port = 80; }
                else if (Fact.Network.Client.Ping(Server + ":9089", 1000)) { Server = Server + ":9089"; port = 9089; }
                else if (Fact.Network.Client.Ping(Server + ":80",   1000)) { Server = Server + ":80"; port = 80; }
                else { throw new Exception("No fact server found at '" + Server + "'. Try to set the port manually"); }
            }
            else
            {
                if (!Fact.Network.Client.Ping(Server, 1000))
                {
                    throw new Exception("'" + Server + "' is not responding.");
                }
            }


            if (CredentialFile == null || CredentialFile.Trim() == "")
            {
                if (Server.ToLower().StartsWith("http://localhost:") ||
                    Server.ToLower().StartsWith("http://127.0.0.1:") ||
                    Server.ToLower().StartsWith("https://localhost:") ||
                    Server.ToLower().StartsWith("https://127.0.0.1:"))
                {
                    string localCredFile = Fact.Internal.Information.GetSpecialFolders(Fact.Internal.Information.SpecialFolder.FactLocalServerData);
                    if (System.IO.File.Exists(localCredFile + "/" + port))
                    {
                        try
                        {
                            Fact.Directory.Credential credential = new Fact.Directory.Credential(localCredFile + "/" + port);
                            Fact.Network.Client client = new Fact.Network.Client(Server, credential);
                            return client;
                        }
                        catch { }
                    }
                }
                {
                    string username = "";
                    string password = "";
                    Fact.Log.Login(out username, out password);
                    Fact.Network.Client client = new Fact.Network.Client(Server, username, password);
                    return client;
                }
            }
            else
            {
                if (System.IO.File.Exists(CredentialFile))
                {
                    try
                    {
                        Fact.Directory.Credential credential = new Fact.Directory.Credential(CredentialFile);
                        Fact.Network.Client client = new Fact.Network.Client(Server, credential);
                        return client;
                    }
                    catch
                    {
                        throw new Exception("Credential file '" + CredentialFile + "' corrupted");
                    }
                }
                else
                {
                    throw new Exception("Credential file '" + CredentialFile + "' not found");
                }
            }
        }
    }
}

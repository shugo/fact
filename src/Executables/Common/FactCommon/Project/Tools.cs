﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Project
{
    public class Tools
    {
        public static Fact.Processing.Project LoadProject(string Input)
        {
            if (Input == "")
            {
                if (Fact.Processing.ProjectInFileSystem.IsPhysicalDirectoryInProject("."))
                {
                    Input = ".";
                }
                else
                { throw new Exception("The project specified as input is not valid"); }
            }

            return Fact.Processing.Project.Load(Input);
        }
    }
}

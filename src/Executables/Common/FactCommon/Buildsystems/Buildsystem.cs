﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Buildsystems
{
    public class Buildsystem
    {
        public virtual string Name { get { return (this as Object).GetType().Name; } }
        public virtual bool Accessible { get { return true; } }
        public Buildsystem()
        {
        }

        public virtual void Configure(Fact.Processing.Project Project) { }
        public virtual void Build(Fact.Processing.Project Project, Toolchain toolchain, string[] Targets) { }


        public static List<Buildsystem> GetInstalledBuildsystems()
        {
            List<Buildsystem> buildsystems = new List<Buildsystem>();
            List<string> pathToLook = new List<string>();
            foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    if (assembly.Location != "" && System.IO.File.Exists(assembly.Location))
                    {
                        string assemblyDirectory = System.IO.Path.GetDirectoryName(assembly.Location);
                        string buildsystemDirectory = assemblyDirectory + "/buildsystem";
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Buildsystems"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/buildsystem"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Buildsystem"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/configs"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Configs"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Config"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/config"; }

                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/buildsystems"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Buildsystems"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/buildsystem"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Buildsystem"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/configs"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Configs"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Config"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/config"; }

                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/buildsystems"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Buildsystems"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/buildsystem"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Buildsystem"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/configs"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Configs"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/Config"; }
                        if (!System.IO.Directory.Exists(buildsystemDirectory)) { buildsystemDirectory = assemblyDirectory + "/Project/config"; }

                        if (System.IO.Directory.Exists(buildsystemDirectory)) { pathToLook.Add(buildsystemDirectory); }
                    }
                }
                catch { }
            }

            HashSet<string> dll = new HashSet<string>();
            HashSet<string> classes = new HashSet<string>();


            foreach (string directory in pathToLook)
            {
                try
                {
                    foreach (string file in System.IO.Directory.GetFiles(directory))
                    {
                        if (file.ToLower().EndsWith(".dll"))
                        {
                            try
                            {
                                if (dll.Contains(System.IO.Path.GetFullPath(file))) { continue; }
                                dll.Add(System.IO.Path.GetFullPath(file));

                                byte[] data = System.IO.File.ReadAllBytes(file);
                                System.Reflection.Assembly assembly = System.AppDomain.CurrentDomain.Load(data);
                                foreach (Type type in assembly.GetTypes())
                                {
                                    try
                                    {
                                        if (type.IsSubclassOf(typeof(Buildsystem)))
                                        {
                                            if (classes.Contains(type.Assembly.FullName + "." + type.FullName)) { continue; }
                                            classes.Add(type.Assembly.FullName + "." + type.FullName);

                                            System.Reflection.ConstructorInfo constructor = type.GetConstructor(new Type[] { });
                                            object config = constructor.Invoke(new object[] { });
                                            if ((config as Buildsystem).Accessible)
                                            {
                                                buildsystems.Add(config as Buildsystem);
                                            }
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }
                    }
                }
                catch { }
            }

            return buildsystems;
        }
    }
}

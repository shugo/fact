﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Buildsystems
{
    public class Toolchain
    {
        public virtual bool Accessible { get { return true; } }

        public virtual void RunTool(string ToolName, object[] Parameters)
        {
            throw new Exception("Tool " + ToolName + " not supported");
        }

        public static List<Toolchain> GetInstalledToolchains()
        {
            List<Toolchain> toolchains = new List<Toolchain>();
            List<string> pathToLook = new List<string>();
            foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    if (assembly.Location != "" && System.IO.File.Exists(assembly.Location))
                    {
                        string assemblyDirectory = System.IO.Path.GetDirectoryName(assembly.Location);
                        string toolchainDirectory = assemblyDirectory + "/toolchain";
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Toolchains"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/toolchains"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Toolchain"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/toolchain"; }

                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/toolchains"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/Toolchains"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/toolchain"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/Toolchain"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/toolchains"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/Toolchains"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/toolchain"; }
                        if (!System.IO.Directory.Exists(toolchainDirectory)) { toolchainDirectory = assemblyDirectory + "/Project/Toolchain"; }

                        if (System.IO.Directory.Exists(toolchainDirectory)) { pathToLook.Add(toolchainDirectory); }
                    }
                }
                catch { }
            }

            HashSet<string> dll = new HashSet<string>();
            HashSet<string> classes = new HashSet<string>();


            foreach (string directory in pathToLook)
            {
                try
                {
                    foreach (string file in System.IO.Directory.GetFiles(directory))
                    {
                        if (file.ToLower().EndsWith(".dll"))
                        {
                            try
                            {
                                if (dll.Contains(System.IO.Path.GetFullPath(file))) { continue; }
                                dll.Add(System.IO.Path.GetFullPath(file));

                                byte[] data = System.IO.File.ReadAllBytes(file);
                                System.Reflection.Assembly assembly = System.AppDomain.CurrentDomain.Load(data);
                                foreach (Type type in assembly.GetTypes())
                                {
                                    try
                                    {
                                        if (type.IsSubclassOf(typeof(Toolchain)))
                                        {
                                            if (classes.Contains(type.Assembly.FullName + "." + type.FullName)) { continue; }
                                            classes.Add(type.Assembly.FullName + "." + type.FullName);

                                            System.Reflection.ConstructorInfo constructor = type.GetConstructor(new Type[] { });
                                            object config = constructor.Invoke(new object[] { });
                                            if ((config as Toolchain).Accessible)
                                            {
                                                toolchains.Add(config as Toolchain);
                                            }
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }
                    }
                }
                catch { }
            }

            return toolchains;
        }
    }
}

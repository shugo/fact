﻿/*                                                                           *
 * Copyright © 2015, Raphaël Boissel, Jean-Luc Bounthong, Antoine Lecubin    *
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel,        *
 *   Jean-Luc Bounthong or Antoine Lecubin shall not be used in advertising  *
 *   or otherwise to promote the sale, use or other dealings in this         *
 *   Software without prior written authorization from Raphaël Boissel,      *
 *   Jean-Luc Bounthong or Antoine Lecubin.                                  *
 *                                                                           */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Tests
{

    public abstract class Collector
    {
        public enum ParallelMode
        {
            Default,
            PreferGroupLevelParallelization,
            PreferTestLevelParallelization,
        }
        ParallelMode _Parallel = ParallelMode.Default;

        public ParallelMode PreferedParallelMode { get { return _Parallel; } set { _Parallel = value; } }

        public Collector(Fact.Processing.Project Project) { _Project = Project; }
        Fact.Processing.Project _Project = null;
        Dictionary<string, GroupInfo> _Groups = new Dictionary<string, GroupInfo>();
        bool _Verbose = false; public bool Verbose { get { return _Verbose; } set { _Verbose = value; } }

        class GroupInfo
        {
            internal GroupInfo _Parent = null;
            List<GroupInfo> _Children = new List<GroupInfo>();
            bool _SetupPass = true;
            Fact.Test.Result.Group _Group = null;
            public Fact.Test.Result.Group FactGroup { get { return _Group; } set { _Group = value; } }
            string _Name = "";
            internal string _FullName = "";
            internal Dictionary<string, object> _Setups = new Dictionary<string, object>();
            internal Dictionary<string, object> _Tests = new Dictionary<string, object>();
            internal Dictionary<string, object> _Teardowns = new Dictionary<string, object>();

            public bool SetupPass
            {
                get { if (_SetupPass && _Parent != null) { return _Parent.SetupPass; } return _SetupPass; }
                set { _SetupPass = value; }
            }
        }

        GroupInfo _CreateGroup(string Name)
        {
            GroupInfo parentGroup = null;
            string parentgroupname = "";
            string[] subgroups = Name.Split('/');
            if (subgroups.Length == 0) { return null; }
            if (subgroups.Length > 1)
            {
                for (int n = 0; n < subgroups.Length - 2; n++)
                {
                    parentgroupname += subgroups[n] + "/";
                }
                parentgroupname += subgroups[subgroups.Length - 2];
                parentGroup = _CreateGroup(parentgroupname);
            }
            {
                if (_Groups.ContainsKey(Name))
                {
                    return _Groups[Name];
                }
                GroupInfo info = new GroupInfo();
                info.FactGroup = new Fact.Test.Result.Group(subgroups[subgroups.Length - 1]);
                info._FullName = Name;
                if (parentGroup != null)
                {
                    parentGroup.FactGroup.AddTestResult(info.FactGroup);
                    info._Parent = parentGroup;
                }
                else
                {
                    if (_Project != null)
                    {
                        _Project.AddTestResult(info.FactGroup);
                    }
                }
                _Groups.Add(Name, info);

                return info;
            }
        }

        object ConfigureTest(object Test, string Name, Fact.Test.Result.Group Group, object Default)
        {
            if (Test == null) { return null; }
            if (Test is TestProject)
            {
                (Test as TestProject).Group = Group;
                (Test as TestProject).Name = Name;
                if (Default != null)
                { (Test as TestProject).Default = Default as Fact.Test.Result.Result; }
            }
            else if (Test is TestsProject)
            {
                (Test as TestsProject).Group = Group;
                (Test as TestsProject).Name = Name;
                if (Default != null)
                { (Test as TestsProject).Default = Default as List<Fact.Test.Result.Result>; }
            }
            return Test;
        }

        protected void AddTest(string Group, string TestName, object Test)
        {
            if (TestName.Contains("/") || TestName.Contains(";"))
            {
                Fact.Log.Warning("Invalid character in test name " + TestName);
                TestName = TestName.Replace("/", "_");
                TestName = TestName.Replace(";", "_");
                Fact.Log.Warning("name of the test has been changed into " + TestName);
            }
            if (Test == null) { return; }
            GroupInfo info = _CreateGroup(Group);
            if (info == null) { return; }
            if (info._Teardowns.ContainsKey(TestName))
            { throw new Exception("A test with the name " + TestName + " conflict with a teardown name"); }
            if (info._Setups.ContainsKey(TestName))
            { throw new Exception("A test with the name " + TestName + " conflict with a setup name"); }
            if (!info._Tests.ContainsKey(TestName))
            { info._Tests.Add(TestName, ConfigureTest(Test, TestName, info.FactGroup, null)); }
            else { throw new Exception("A test with the name " + TestName + " has already been registered"); }
        }

        protected void AddSetup(string Group, string TestName, object Test)
        {
            if (TestName.Contains("/") || TestName.Contains(";"))
            {
                Fact.Log.Warning("Invalid character in test name " + TestName);
                TestName = TestName.Replace("/", "_");
                TestName = TestName.Replace(";", "_");
                Fact.Log.Warning("name of the test has been changed into " + TestName);
            }
            if (Test == null) { return; }
            GroupInfo info = _CreateGroup(Group);
            if (info == null) { return; }
            if (info._Teardowns.ContainsKey(TestName))
            { throw new Exception("A setup with the name " + TestName + " conflict with a teardown name"); }
            if (info._Tests.ContainsKey(TestName))
            { throw new Exception("A setup with the name " + TestName + " conflict with a test name"); }
            if (!info._Setups.ContainsKey(TestName))
            { info._Setups.Add(TestName, ConfigureTest(Test, TestName, info.FactGroup, null)); }
            else { throw new Exception("A setup with the name " + TestName + " has already been registered"); }
        }

        protected void AddTeardown(string Group, string TestName, object Test)
        {
            if (TestName.Contains("/") || TestName.Contains(";"))
            {
                Fact.Log.Warning("Invalid character in test name " + TestName);
                TestName = TestName.Replace("/", "_");
                TestName = TestName.Replace(";", "_");
                Fact.Log.Warning("name of the test has been changed into " + TestName);
            }
            if (Test == null) { return; }
            GroupInfo info = _CreateGroup(Group);
            if (info == null) { return; }

            if (info._Setups.ContainsKey(TestName))
            { throw new Exception("A teardown with the name " + TestName + " conflict with a setup name"); }
            if (info._Tests.ContainsKey(TestName))
            { throw new Exception("A teardown with the name " + TestName + " conflict with a test name"); }
            if (!info._Teardowns.ContainsKey(TestName))
            { info._Teardowns.Add(TestName, ConfigureTest(Test, TestName, info.FactGroup, null)); }
            else { throw new Exception("A teardown with the name " + TestName + " has already been registered"); }
        }

        public bool IsSetup(string Group, string Test)
        {
            if (_Groups.ContainsKey(Group))
            {
                return _Groups[Group]._Setups.ContainsKey(Test);
            }
            return false;
        }

        public List<string> ListAllGroups()
        {
            List<string> result = new List<string>();
            foreach (string group in _Groups.Keys)
            {
                result.Add(group);
            }
            return result;
        }

        public List<string> ListAllTestsInGroup(string Group)
        {
            List<string> result = new List<string>();
            if (!_Groups.ContainsKey(Group)) { return result; }
            foreach (string test in _Groups[Group]._Tests.Keys)
            {
                result.Add(test);
            }
            return result;
        }
        public List<string> ListAllSetupsInGroup(string Group)
        {
            List<string> result = new List<string>();
            if (!_Groups.ContainsKey(Group)) { return result; }
            foreach (string test in _Groups[Group]._Setups.Keys)
            {
                result.Add(test);
            }
            return result;
        }
        public List<KeyValuePair<string, string>> ListAllSetupsAndDependenciesInGroup(string Group)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            if (!_Groups.ContainsKey(Group)) { return result; }
            if (_Groups[Group]._Parent != null)
            {
                result.AddRange(ListAllSetupsAndDependenciesInGroup(_Groups[Group]._Parent._FullName));
            }
            foreach (string test in _Groups[Group]._Setups.Keys)
            {
                result.Add(new KeyValuePair<string, string>(Group, test));
            }
            return result;
        }
        public List<string> ListAllTeardownsInGroup(string Group)
        {
            List<string> result = new List<string>();
            if (!_Groups.ContainsKey(Group)) { return result; }
            foreach (string test in _Groups[Group]._Teardowns.Keys)
            {
                result.Add(test);
            }
            return result;
        }
        public List<KeyValuePair<string, string>> ListAllTeardownsAndDependenciesInGroup(string Group)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            if (!_Groups.ContainsKey(Group)) { return result; }
            foreach (string test in _Groups[Group]._Teardowns.Keys)
            {
                result.Add(new KeyValuePair<string, string>(Group, test));
            }
            if (_Groups[Group]._Parent != null)
            {
                result.AddRange(ListAllTeardownsAndDependenciesInGroup(_Groups[Group]._Parent._FullName));
            }
            return result;
        }

        public List<string> ListTests()
        {
            List<string> restult = new List<string>();
            List<string> groups = ListAllGroups();
            foreach (string group in groups)
            {
                List<string> tests = ListAllTestsInGroup(group);
                foreach (string test in tests)
                {
                    restult.Add(group + "/" + test);
                }
            }
            return restult;
        }

        public object GetTest(string GroupName, string TestName)
        {
            object test = null;
            if (!_Groups.ContainsKey(GroupName)) { return null; }
            if (_Groups[GroupName]._Tests.ContainsKey(TestName)) { test = _Groups[GroupName]._Tests[TestName]; }
            else if (_Groups[GroupName]._Setups.ContainsKey(TestName)) { test = _Groups[GroupName]._Setups[TestName]; }
            else if (_Groups[GroupName]._Teardowns.ContainsKey(TestName)) { test = _Groups[GroupName]._Teardowns[TestName]; }
            return test;
        }

        class ContextResult
        {
            internal Fact.Test.Result.Result _Result;
        }

        Fact.Processing.Check.Check_On_Project MangleTest(GroupInfo Info, TestProject Test, bool Fatal, bool Sandboxed)
        {
            return (Fact.Processing.Project project) =>
            {
                if (Info.SetupPass)
                {
                    if (Fatal)
                    {
                        Test.OnFailure = () =>
                        {
                            Info.SetupPass = false;
                        };
                    }
                    if (Sandboxed)
                    {
                        ContextResult result = new ContextResult();
                        Fact.Context.Context Context = Fact.Context.Context.Current.CreateSubContext();
                        Context.Execute(() => { result._Result = Test.Run(project); });
                        return result._Result;
                    }
                    else
                    {
                        return Test.Run(project);
                    }
                }

                // Create the skip result
                {
                    Fact.Test.Result.Omitted result = new Fact.Test.Result.Omitted(Test.Prefix + Test.Name,
                                                                                   "This test has been omitted because the setup of the testsuite has failed");
                    Fact.Log.Auto(result);
                    if (Test.Group != null) { Test.Group.AddTestResult(result); return Test.Group; }
                    return result;
                }
            };
        }

        Fact.Processing.Check.Multi_Check_On_Project MangleTest(GroupInfo Info, TestsProject Test, bool Fatal, bool Sandboxed)
        {
            return (Fact.Processing.Project project) =>
            {
                if (Info.SetupPass)
                {
                    if (Fatal)
                    {
                        Test.OnFailure = () =>
                        {
                            Info.SetupPass = false;
                        };
                    }
                    if (Sandboxed)
                    {
                        /*
                        Fact.Context.Context Context = Fact.Context.Context.Current.CreateSubContext();
                        return Context.Execute(
                            (Func<Fact.Processing.Project, List<Fact.Test.Result.Result>>)
                            ((Fact.Processing.Project sandboxedProject) => { return Test.Run(sandboxedProject); }), project) as List<Fact.Test.Result.Result>;
    */                
                    }
                    else
                    {
                        return Test.Run(project);
                    }
                }

                // Create the skip result
                {
                    Fact.Test.Result.Result result = new Fact.Test.Result.Omitted(Test.Prefix + Test.Name,
                                                                                  "This test has been omitted because the setup of the testsuite has failed");
                    Fact.Log.Auto(result);
                    if (Test.Group != null) { Test.Group.AddTestResult(result); result = Test.Group; }
                    List<Fact.Test.Result.Result> single = new List<Fact.Test.Result.Result>();
                    single.Add(result);
                    return single;
                }
            };
        }

        void AddTestInChain(GroupInfo Info, object Test, bool Fatal, bool Sandboxed, string Prefix, Fact.Processing.Chain Chain)
        {
            if (Test == null) { return; }
            if (Test is TestProject)
            {
                TestProject testproject = Test as TestProject;
                testproject.Prefix = Prefix;
                testproject.Verbose = Verbose;
                Chain.AddCheck(MangleTest(Info, testproject, Fatal, Sandboxed));
            }
            else if (Test is TestsProject)
            {
                TestsProject testsproject = Test as TestsProject;
                testsproject.Prefix = Prefix;
                testsproject.Verbose = Verbose;
                Chain.AddMultiCheck(MangleTest(Info, testsproject, Fatal, Sandboxed));
            }
            else
            {
                throw new Exception("Unsupported test format");
            }
        }

        public List<KeyValuePair<string, string>> ComputeTestPlan(string Group, string FilterIn, string FilterOut, string FilterInGroup, string FilterOutGroup)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

            System.Text.RegularExpressions.Regex FilterInRegex = null;
            if (FilterIn != "") { FilterInRegex = new System.Text.RegularExpressions.Regex(FilterIn); }
            System.Text.RegularExpressions.Regex FilterOutRegex = null;
            if (FilterOut != "") { FilterOutRegex = new System.Text.RegularExpressions.Regex(FilterOut); }

            System.Text.RegularExpressions.Regex FilterInGroupRegex = null;
            if (FilterInGroup != "") { FilterInGroupRegex = new System.Text.RegularExpressions.Regex(FilterInGroup); }
            System.Text.RegularExpressions.Regex FilterOutGroupRegex = null;
            if (FilterOutGroup != "") { FilterOutGroupRegex = new System.Text.RegularExpressions.Regex(FilterOutGroup); }

            if (FilterInGroupRegex != null && !FilterInGroupRegex.Match(Group).Success) { return result; }
            if (FilterOutGroupRegex != null && FilterOutGroupRegex.Match(Group).Success) { return result; }

            List<string> tests = ListAllTestsInGroup(Group);
            List<KeyValuePair<string, string>> setups = ListAllSetupsAndDependenciesInGroup(Group);
            List<KeyValuePair<string, string>> teardowns = ListAllTeardownsAndDependenciesInGroup(Group);

            bool match = false;
            foreach (string test in tests)
            {
                if (FilterInRegex == null || FilterInRegex.Match(test).Success)
                {
                    if (FilterOutRegex == null || !FilterOutRegex.Match(test).Success)
                    {
                        match = true;
                        break;
                    }
                }
            }
            if (match)
            {
                foreach (KeyValuePair<string, string> test in setups)
                {
                    result.Add(new KeyValuePair<string, string>(test.Key, test.Value));
                }
            }

            foreach (string test in tests)
            {
                if (FilterInRegex == null || FilterInRegex.Match(test).Success)
                {
                    if (FilterOutRegex == null || !FilterOutRegex.Match(test).Success)
                    {
                        result.Add(new KeyValuePair<string, string>(Group, test));
                    }
                }
            }

            if (match)
            {
                foreach (KeyValuePair<string, string> test in teardowns)
                {
                    result.Add(new KeyValuePair<string, string>(test.Key, test.Value));
                }
            }

            return result;
        }

        public List<List<KeyValuePair<string, string>>> ComputeTestPlanWithIsolatedTest(string Group, string FilterIn, string FilterOut, string FilterInGroup, string FilterOutGroup)
        {
            List<List<KeyValuePair<string, string>>> result = new List<List<KeyValuePair<string, string>>>();

            System.Text.RegularExpressions.Regex FilterInRegex = null;
            if (FilterIn != "") { FilterInRegex = new System.Text.RegularExpressions.Regex(FilterIn); }
            System.Text.RegularExpressions.Regex FilterOutRegex = null;
            if (FilterOut != "") { FilterOutRegex = new System.Text.RegularExpressions.Regex(FilterOut); }

            System.Text.RegularExpressions.Regex FilterInGroupRegex = null;
            if (FilterInGroup != "") { FilterInGroupRegex = new System.Text.RegularExpressions.Regex(FilterInGroup); }
            System.Text.RegularExpressions.Regex FilterOutGroupRegex = null;
            if (FilterOutGroup != "") { FilterOutGroupRegex = new System.Text.RegularExpressions.Regex(FilterOutGroup); }

            if (FilterInGroupRegex != null && !FilterInGroupRegex.Match(Group).Success) { return result; }
            if (FilterOutGroupRegex != null && FilterOutGroupRegex.Match(Group).Success) { return result; }

            List<string> tests = ListAllTestsInGroup(Group);
            List<KeyValuePair<string, string>> setups = ListAllSetupsAndDependenciesInGroup(Group);
            List<KeyValuePair<string, string>> teardowns = ListAllTeardownsAndDependenciesInGroup(Group);

            bool match = false;
            foreach (string test in tests)
            {
                List<KeyValuePair<string, string>> testslist = new List<KeyValuePair<string, string>>();
                if (FilterInRegex == null || FilterInRegex.Match(test).Success)
                {
                    if (FilterOutRegex == null || !FilterOutRegex.Match(test).Success)
                    {
                        foreach (KeyValuePair<string, string> setup in setups)
                        {
                            testslist.Add(new KeyValuePair<string, string>(setup.Key, setup.Value));
                        }
                        testslist.Add(new KeyValuePair<string, string>(Group, test));
                        foreach (KeyValuePair<string, string> teardown in teardowns)
                        {
                            testslist.Add(new KeyValuePair<string, string>(teardown.Key, teardown.Value));
                        }
                    }
                }
                result.Add(testslist);
            }
            return result;
        }

        public List<KeyValuePair<string, string>> ComputeTestPlan(string FilterIn, string FilterOut, string FilterInGroup, string FilterOutGroup)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            foreach (KeyValuePair<string, GroupInfo> group in _Groups)
            {
                List<KeyValuePair<string, string>> subresult = ComputeTestPlan(group.Key, FilterIn, FilterOut, FilterInGroup, FilterOutGroup);
                result.AddRange(subresult);
            }
            return result;
        }

        public void CollectTestInChain(List<KeyValuePair<string, string>> TestPlan, string Prefix, Fact.Processing.Chain Chain)
        {
            foreach (KeyValuePair<string, string> test in TestPlan)
            {
                AddTestInChain(_Groups[test.Key], GetTest(test.Key, test.Value), IsSetup(test.Key, test.Value), false, Prefix, Chain);
            }
        }

        public void CollectAndSkipTestInChain(List<KeyValuePair<string, string>> TestPlan, string Prefix, Fact.Processing.Chain Chain, string Message)
        {
            foreach (KeyValuePair<string, string> test in TestPlan)
            {
                GroupInfo group = _Groups[test.Key];
                Chain.AddCheck(
                    (Fact.Processing.Check.Check_On_Project)((Fact.Processing.Project p) =>
                    {
                        group.FactGroup.AddTestResult(new Fact.Test.Result.Omitted(Prefix + test.Value, Message));
                        return group.FactGroup;
                    }));
            }
        }

        public virtual void Setup()
        {

        }

        Dictionary<string, Fact.Processing.File> _Files = new Dictionary<string, Fact.Processing.File>();

        public List<string> ListDependencies()
        {
            return new List<string>(_Files.Keys);
        }

        public List<KeyValuePair<string, Fact.Processing.File>> ListDependenciesValue()
        {
            return new List<KeyValuePair<string, Fact.Processing.File>>(_Files);
        }

        public Fact.Processing.File GetDependency(string Path)
        {
            if (!_Files.ContainsKey(Path)) { return null; }
            return _Files[Path];
        }

        public void AddDependency(Fact.Processing.File File, string Path)
        {
            if (File == null) { return; }
            if (Path == null || Path == "") { return; }
            string FullPath = System.IO.Path.GetFullPath(Path);
            string WorkDir = System.IO.Path.GetFullPath(System.Environment.CurrentDirectory);

            FullPath = FullPath.Replace("\\", "/");
            WorkDir = WorkDir.Replace("\\", "/");

            if (!WorkDir.EndsWith("/")) { WorkDir += "/"; }
            if (FullPath.StartsWith(WorkDir))
            {
                FullPath = "./" + FullPath.Substring(WorkDir.Length);
            }
            if (!_Files.ContainsKey(FullPath))
            {
                _Files.Add(FullPath, File);
            }
        }

        public abstract void Load(System.IO.FileInfo File, Dictionary<string, string> Parameters);

        public virtual bool CanCollect(System.IO.FileInfo File)
        {
            return false;
        }

        public static List<Collector> GetInstalledCollectors(Fact.Processing.Project Project)
        {
            List<Collector> _Collectors = new List<Collector>();
            List<string> _PathToLook = new List<string>();
            foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    if (assembly.Location != "" && System.IO.File.Exists(assembly.Location))
                    {
                        string assemblyDirectory = System.IO.Path.GetDirectoryName(assembly.Location);
                        string collectorDirectory = assemblyDirectory + "/collectors";
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/Collectors"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/collector"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/Collector"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/Test/Collector"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/Test/Collectors"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/Test/collector"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/Test/Collector"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/test/Collector"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/test/Collectors"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/test/collector"; }
                        if (!System.IO.Directory.Exists(collectorDirectory)) { collectorDirectory = assemblyDirectory + "/test/Collector"; }
                        if (System.IO.Directory.Exists(collectorDirectory)) { _PathToLook.Add(collectorDirectory); }
                    }
                }
                catch { }
            }

            foreach (string directory in _PathToLook)
            {
                try
                {
                    foreach (string file in System.IO.Directory.GetFiles(directory))
                    {
                        if (file.ToLower().EndsWith(".dll"))
                        {
                            try
                            {
                                byte[] data = System.IO.File.ReadAllBytes(file);
                                System.Reflection.Assembly assembly = System.AppDomain.CurrentDomain.Load(data);
                                foreach (Type type in assembly.GetTypes())
                                {
                                    try
                                    {
                                        if (type.BaseType == typeof(Collector))
                                        {
                                            System.Reflection.ConstructorInfo constructor = type.GetConstructor(new Type[] { typeof(Fact.Processing.Project) });
                                            object collector = constructor.Invoke(new object[] { Project });
                                            _Collectors.Add(collector as Collector);
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }
                    }
                }
                catch { }
            }

            return _Collectors;
        }
    }
}

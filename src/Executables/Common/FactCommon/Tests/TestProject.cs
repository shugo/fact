﻿/*                                                                           *
 * Copyright © 2015, Raphaël Boissel, Jean-Luc Bounthong, Antoine Lecubin    *
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel,        *
 *   Jean-Luc Bounthong or Antoine Lecubin shall not be used in advertising  *
 *   or otherwise to promote the sale, use or other dealings in this         *
 *   Software without prior written authorization from Raphaël Boissel,      *
 *   Jean-Luc Bounthong or Antoine Lecubin.                                  *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Tests
{
    public class TestsProject : Base<Fact.Processing.Project, List<Fact.Test.Result.Result>>
    {
        Fact.Test.Result.Result.VisibilityLevel _Visibility = Fact.Test.Result.Result.VisibilityLevel.Public;
        public Fact.Test.Result.Result.VisibilityLevel Visibility { get { return _Visibility; } set { _Visibility = value; } }
        protected Fact.Test.Result.Group _group;
        public Fact.Test.Result.Group Group { get { return _group; } set { _group = value; } }
        public TestsProject(Func<Fact.Processing.Project, List<Fact.Test.Result.Result>> testFuncion,
                           Fact.Test.Result.Group group, string prefix)
            : base(testFuncion, group, prefix)
        {
            _group = group;
            _default = new List<Fact.Test.Result.Result>();
        }

        protected override List<Fact.Test.Result.Result> InvokeTest(Fact.Processing.Project input)
        {
            DateTime start = DateTime.Now;
            List<Fact.Test.Result.Result> Results = null;
            Fact.Assert.Assert.EnableDelayedAsserts();
            try
            {
                Results = base.InvokeTest(input);
            }
            catch (Fact.Assert.Assert assert)
            {
                Results = new List<Fact.Test.Result.Result>();
                if (assert.Result == null)
                {
                    Results.Add(new Fact.Test.Result.Error(_name, "Assertion fail") { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) });
                }
                else
                {
                    assert.Result.Info = assert.Result.Text + ": " + assert.Result.Info;
                    assert.Result.Text = _name;
                    if (assert.Result.EnlapsedUserTime == 0) { assert.Result.EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds); }
                    Results.Add(assert.Result);
                }
            }
            catch (Exception e)
            {
                Results = new List<Fact.Test.Result.Result>();
                if (e.InnerException != null && e.InnerException is Fact.Assert.Assert)
                {
                    Fact.Assert.Assert assert = (Fact.Assert.Assert)e.InnerException;
                    if (assert.Result == null)
                    {
                        Results.Add(new Fact.Test.Result.Error(_name, "Assertion fail") { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) });
                    }
                    else
                    {
                        assert.Result.Info = assert.Result.Text + ": " + assert.Result.Info;
                        assert.Result.Text = _name;
                        if (assert.Result.EnlapsedUserTime == 0) { assert.Result.EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds); }
                        Results.Add(assert.Result);
                    }
                }
                else
                {
                    if (e.InnerException != null)
                    {
                        Results.Add(new Fact.Test.Result.Error(_name, "Exception occurs during the test: " + e.InnerException.ToString()) { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) });
                    }
                    else
                    {
                        Results.Add(new Fact.Test.Result.Error(_name, "Exception occurs during the test: " + e.ToString()) { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) });
                    }
                }
            }

            // Manage Visibility
            if (Results != null)
            {
                foreach (Fact.Test.Result.Result result in Results)
                {
                    if (result != null)
                    {
                        if ((int)result.Visibility < (int)_Visibility) { result.Visibility = _Visibility; }
                    }
                }
            }

            // Manage delayed asserts
            {
                try
                {
                    List<Fact.Assert.Assert> asserts = new List<Fact.Assert.Assert>(Fact.Assert.Assert.FlushThreadPendingAsserts());
                    List<Fact.Test.Result.Result> results = new List<Fact.Test.Result.Result>();
                    foreach (Fact.Assert.Assert assert in asserts)
                    {
                        if (assert.Result != null)
                        {
                            results.Add(assert.Result);
                            if ((int)assert.Result.Visibility < (int)_Visibility) { assert.Result.Visibility = _Visibility; }
                        }
                    }
                    if (Results != null) { results.AddRange(Results); }
                    if (Tools.HasFailure(results)) { _Failed = true; if (OnFailure != null) { OnFailure(); } }
                    Tools.MakeUniqueName(results);
                    return results;

                }
                catch { }
            }
            if (Tools.HasFailure(Results)) { _Failed = true; if (OnFailure != null) { OnFailure(); } }
            Tools.MakeUniqueName(Results);
            return Results;
        }

        protected override List<Fact.Test.Result.Result> FormatOutput(List<Fact.Test.Result.Result> results)
        {
            Fact.Test.Result.Group groupedTests = new Fact.Test.Result.Group(_name);
            if (results != null)
            {

                foreach (Fact.Test.Result.Result result in results)
                {
                    if (result != null)
                    {
                        result.Text = _prefix + result.Text;
                        groupedTests.AddTestResult(result);
                        Fact.Log.Auto(result, Verbose);
                    }
                    if (_group != null) { _group.AddTestResult(groupedTests); }
                }
            }
            if (_group != null)
            {
                List<Fact.Test.Result.Result> monoresult = new List<Fact.Test.Result.Result>();
                monoresult.Add(_group);
                return monoresult;
            }
            if (results != null)
            {
                List<Fact.Test.Result.Result> monoresult = new List<Fact.Test.Result.Result>();
                monoresult.Add(groupedTests);
                return monoresult;
            }
            return null;
        }
    }

    public class TestProject : Base<Fact.Processing.Project, Fact.Test.Result.Result>
    {
        Fact.Test.Result.Result.VisibilityLevel _Visibility = Fact.Test.Result.Result.VisibilityLevel.Public;
        public Fact.Test.Result.Result.VisibilityLevel Visibility { get { return _Visibility; } set { _Visibility = value; } }
        protected Fact.Test.Result.Group _group;
        public Fact.Test.Result.Group Group { get { return _group; } set { _group = value; } }
        public TestProject(Func<Fact.Processing.Project, Fact.Test.Result.Result> testFuncion,
                           Fact.Test.Result.Group group, string prefix)
            : base(testFuncion, group, prefix)
        {
            _group = group;
            _default = new Fact.Test.Result.Passed(_name, "OK");
        }
        public TestProject(Func<Fact.Processing.Project, Fact.Test.Result.Result> testFuncion,
            Fact.Test.Result.Group group, Fact.Test.Result.Result Default, string prefix)
            : base(testFuncion, group, prefix)
        {
            _group = group;
            _default = Default;
        }


        protected override Fact.Test.Result.Result InvokeTest(Fact.Processing.Project input)
        {
            DateTime start = DateTime.Now;
            Fact.Test.Result.Result result = null;
            Fact.Assert.Assert.EnableDelayedAsserts();
            List<Fact.Assert.Assert> asserts = null;
            try
            {
                result = base.InvokeTest(input);
                if (result == null)
                {
                    asserts = new List<Fact.Assert.Assert>(Fact.Assert.Assert.FlushThreadPendingAsserts());
                    if (asserts.Count == 0)
                    {
                        if (_default != null)
                        {
                            _default.Text = _name;
                            _default.EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds);
                        }
                        result = _default;
                    }
                }
            }
            catch (Fact.Assert.Assert assert)
            {
                if (assert.Result == null)
                { result = new Fact.Test.Result.Error(_name, "Assertion fail") { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) }; }
                else
                {
                    assert.Result.Info = assert.Result.Text + ": " + assert.Result.Info;
                    assert.Result.Text = _name;
                    if (assert.Result.EnlapsedUserTime == 0) { assert.Result.EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds); }
                    result = assert.Result;
                }
            }
            catch (Exception e)
            {
                if (e.InnerException != null && e.InnerException is Fact.Assert.Assert)
                {
                    Fact.Assert.Assert assert = (Fact.Assert.Assert)e.InnerException;
                    if (assert.Result == null)
                    { result = new Fact.Test.Result.Error(_name, "Assertion fail") { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) }; }
                    else
                    {
                        assert.Result.Info = assert.Result.Text + ": " + assert.Result.Info;
                        assert.Result.Text = _name;
                        if (assert.Result.EnlapsedUserTime == 0) { assert.Result.EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds); }
                        result = assert.Result;
                    }
                }
                else
                {
                    if (e.InnerException != null)
                    {
                        result = new Fact.Test.Result.Error(_name, "Exception occurs during the test: " + e.InnerException.ToString()) { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) };
                    }
                    else
                    {
                        result = new Fact.Test.Result.Error(_name, "Exception occurs during the test: " + e.ToString()) { EnlapsedUserTime = (long)((DateTime.Now - start).TotalMilliseconds) };
                    }
                }
            }

            // Manage Visibility
            if (result != null)
            {
                if ((int)result.Visibility < (int)_Visibility) { result.Visibility = _Visibility; }
            }

            // Manage delayed asserts
            {
                try
                {
                    if (asserts == null)
                    {
                        asserts = new List<Fact.Assert.Assert>(Fact.Assert.Assert.FlushThreadPendingAsserts());
                    }
                    if (asserts.Count == 0)
                    {
                        if (Tools.HasFailure(result)) { _Failed = true; if (OnFailure != null) { OnFailure(); } }
                        return result;
                    }
                    Fact.Test.Result.Group group = new Fact.Test.Result.Group(_name);
                    group.Visibility = _Visibility;
                    List<Fact.Test.Result.Result> resultlist = new List<Fact.Test.Result.Result>();
                    foreach (Fact.Assert.Assert assert in asserts)
                    {
                        if (assert.Result != null)
                        {
                            // Manage assert visibility
                            if ((int)assert.Result.Visibility < (int)_Visibility) { assert.Result.Visibility = _Visibility; }
                            resultlist.Add(assert.Result);
                        }
                    }
                    if (result != null) { resultlist.Add(result); }
                    Tools.MakeUniqueName(resultlist);
                    foreach (Fact.Test.Result.Result subresult in resultlist)
                    {
                        group.AddTestResult(subresult);
                    }
                    if (Tools.HasFailure(group)) { _Failed = true; if (OnFailure != null) { OnFailure(); } }
                    return group;
                }
                catch { }
            }
            if (Tools.HasFailure(result)) { _Failed = true; if (OnFailure != null) { OnFailure(); } }
            return result;
        }
        protected override Fact.Test.Result.Result FormatOutput(Fact.Test.Result.Result result)
        {
            Fact.Test.Result.Result single = result;
            if (result != null)
            {
                result.Text = _prefix + result.Text;
            }
            if (_group != null)
            {
                _group.AddTestResult(result);
                result = _group;
            }
            Fact.Log.Auto(single, Verbose);
            return result;
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2015, Raphaël Boissel, Jean-Luc Bounthong, Antoine Lecubin    *
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel,        *
 *   Jean-Luc Bounthong or Antoine Lecubin shall not be used in advertising  *
 *   or otherwise to promote the sale, use or other dealings in this         *
 *   Software without prior written authorization from Raphaël Boissel,      *
 *   Jean-Luc Bounthong or Antoine Lecubin.                                  *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Tests
{
    static public class Tools
    {
        static public bool HasFailure(List<Fact.Test.Result.Result> Results)
        {
            foreach (Fact.Test.Result.Result result in Results)
            {
                if (HasFailure(result)) { return true; }
            }
            return false;
        }
        static public bool HasFailure(Fact.Test.Result.Result Result)
        {
            if (Result is Fact.Test.Result.Error) { return true; }
            if (Result is Fact.Test.Result.Group) { return HasFailure((Result as Fact.Test.Result.Group).GetTestResults()); }
            return false;
        }

        static internal List<Fact.Test.Result.Result> MakeUniqueName(List<Fact.Test.Result.Result> Tests)
        {
            HashSet<string> testname = new HashSet<string>();
            foreach (Fact.Test.Result.Result test in Tests)
            {
                if (test == null) { continue; }
                string name = test.Text;
                int inc = 2;
                while (testname.Contains(test.Text))
                {
                    test.Text = name + " (" + inc.ToString() + ")";
                    inc++;
                }
                testname.Add(test.Text);
            }
            return Tests;
        }
    }
}

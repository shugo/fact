﻿/*                                                                           *
 * Copyright © 2015, Raphaël Boissel, Jean-Luc Bounthong, Antoine Lecubin    *
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel,        *
 *   Jean-Luc Bounthong or Antoine Lecubin shall not be used in advertising  *
 *   or otherwise to promote the sale, use or other dealings in this         *
 *   Software without prior written authorization from Raphaël Boissel,      *
 *   Jean-Luc Bounthong or Antoine Lecubin.                                  *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Common.Tests
{
    public abstract class Base<Input, Output>
    {
        bool _Verbose = false; public bool Verbose { get { return _Verbose; } set { _Verbose = value; } }
        public delegate void Action();
        List<Base<Input, Output>> _dependencies = new List<Base<Input, Output>>();
        protected string _prefix;
        public string Prefix { get { return _prefix; } set { _prefix = value; } }
        protected Func<Input, Output> _testFunction;
        public string _name = "[No name]";
        protected bool _Failed = false;
        public string Name { get { return _name; } set { _name = value; } }
        public bool Failed { get { return _Failed; } }
        Action _OnFailure = null;
        Action _OnTimeout = null;

        public Action OnFailure { get { return _OnFailure; } set { _OnFailure = value; } }
        public List<Base<Input, Output>> Dependencies { get { return _dependencies; } }
        protected Output _default = default(Output);
        public Output Default { get { return _default; } set { _default = value; } }
        protected Base(Func<Input, Output> testFunction, Fact.Test.Result.Group group, string prefix)
        {
            _testFunction = testFunction;
            _prefix = prefix;
        }

        protected virtual Output FormatOutput(Output results)
        {
            return results;
        }

        protected virtual Output InvokeTest(Input input)
        {
            return _testFunction(input);
        }

        class WAR_Holder<Type> { public Type hold = default(Type); }
        public virtual Output Run(Input input, long Timeout)
        {
            if (Timeout <= 0) { return Run(input); }
            WAR_Holder<Output> result = new WAR_Holder<Output>();
            Fact.Threading.Job.Status status = Fact.Threading.Job.CreateJob(() => { result.hold = Run(input); });
            while (!status.Join(100))
            {
                if ((long)status.UserTime.TotalMilliseconds > Timeout)
                {
                    status.Abort();
                    _OnTimeout();
                    return default(Output);
                }
            }
            if ((long)status.UserTime.TotalMilliseconds > Timeout)
            {
                _OnTimeout();
                return default(Output);
            }
            return result.hold;
        }

        public virtual Output Run(Input input)
        {
            if (_Verbose)
            { Fact.Log.Verbose("Running test " + _name + " ..."); }
            Output output = InvokeTest(input);
            if (_Verbose)
            { Fact.Log.Verbose("End test " + _name + " ..."); }
            return FormatOutput(output);
        }
    }
}

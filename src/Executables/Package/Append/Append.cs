﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Append
{
    public class Append : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Append";
            }
        }

        public Fact.Test.Result.Group CloneGroup(Fact.Test.Result.Group A)
        {
            if (A == null)
                return A;

            Fact.Test.Result.Group Cln = new Fact.Test.Result.Group(A.Text);

            foreach (Fact.Test.Result.Result result in A.GetTestResults())
            {
                if (result is Fact.Test.Result.Group)
                {
                    Cln.AddTestResult(CloneGroup((Fact.Test.Result.Group)result));
                }
                else
                {
                    Cln.AddTestResult(result.Copy());
                }
            }
            return Cln;
        }

        public Fact.Test.Result.Group MergeGroup(Fact.Test.Result.Group A, Fact.Test.Result.Group B)
        {
            Fact.Test.Result.Group gp = new Fact.Test.Result.Group(A.Text);
            HashSet<string> exists = new HashSet<string>();
            Dictionary<string, Fact.Test.Result.Group> groups = new Dictionary<string, Fact.Test.Result.Group>();

            foreach (Fact.Test.Result.Result _ in A.GetTestResults())
            {
                if (exists.Contains(_.Text))
                {
                    Fact.Log.Warning("Duplicated test ignored < " + _.Text + " >");
                    continue;
                }
                exists.Add(_.Text);
                if (_ is Fact.Test.Result.Group)
                {
                    groups.Add(_.Text, CloneGroup((Fact.Test.Result.Group)_));
                    continue;
                }

                gp.AddTestResult(_);
            }

            foreach (Fact.Test.Result.Result _ in B.GetTestResults())
            {

                if (exists.Contains(_.Text))
                {
                    Fact.Log.Warning("Duplicated test ignored < " + _.Text + ">");
                    continue;
                }
                exists.Add(_.Text);
                if (_ is Fact.Test.Result.Group)
                {
                    if (groups.ContainsKey(_.Text))
                    {
                        Fact.Log.Info("Merge Group < " + _.Text + ">");
                        groups[_.Text] = MergeGroup(groups[_.Text], (Fact.Test.Result.Group)_);
                    }
                    else
                    {
                        groups.Add(_.Text, CloneGroup((Fact.Test.Result.Group)_));
                    }

                }
                else
                {
                    gp.AddTestResult(_);
                }
            }

            foreach (Fact.Test.Result.Result _ in groups.Values)
            {
                gp.AddTestResult(_);
            }

            return gp;
        }

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.ProgramName = "Append";
            CommandLine.AddStringInput("input-a", "input package A", ""); CommandLine.AddShortInput("a", "input-a");
            CommandLine.AddStringInput("input-b", "input package B", ""); CommandLine.AddShortInput("b", "input-b");
            CommandLine.AddStringInput("output", "output fact package", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

            if (CommandLine["input-a"].IsDefault) { Fact.Log.Error("The package A must be set"); return 1; }
            if (CommandLine["input-b"].IsDefault) { Fact.Log.Error("The package B must be set"); return 1; }
            if (CommandLine["output"].IsDefault) { Fact.Log.Error("The output must be set"); return 1; }

            string inputA = CommandLine["input-a"].AsString();
            string inputB = CommandLine["input-b"].AsString();

            string output = "";
            if (CommandLine["output"].IsDefault) { output = CommandLine["output"].AsString() + ".ff"; }
            else { output = CommandLine["output"].AsString(); }

            string newpackage = output;
            string packageA = inputA;
            string packageB = inputB;


            if (!System.IO.File.Exists(inputA))
            {
                Fact.Log.Error("Package " + inputA + " does not exist");
                return 1;
            }

            if (!System.IO.File.Exists(inputB))
            {
                Fact.Log.Error("Package " + inputB + " does not exist");
                return 1;
            }

            Fact.Processing.Project projectA = Fact.Processing.Project.Load(inputA);
            Fact.Processing.Project projectB = Fact.Processing.Project.Load(inputB);

            Fact.Processing.Project newproject = new Fact.Processing.Project(projectA.Name);

            foreach (Fact.Processing.File file in projectA)
            {
                Fact.Log.Info("Add < " + file.Directory + "/" + file.Name + " >");
                newproject.AddFile(file.Directory, file);
            }

            foreach (Fact.Processing.File file in projectB)
            {
                if (newproject.GetFile(file.Directory + "/" + file.Name) != null)
                {
                    Fact.Log.Info("Append < " + file.Directory + "/" + file.Name + " >");
                    Fact.Processing.File fileA = newproject.GetFile(file.Directory + "/" + file.Name);
                    try
                    {
                        fileA.Append(file);
                    }
                    catch (Exception e)
                    { Fact.Log.Error(e.Message); }
                }
                else
                {
                    Fact.Log.Info("Add < " + file.Directory + "/" + file.Name + " >");
                    newproject.AddFile(file.Directory, file);
                }
            }

            Dictionary<string, Fact.Test.Result.Group> groups = new Dictionary<string, Fact.Test.Result.Group>();
            HashSet<string> texists = new HashSet<string>();
            foreach (Fact.Test.Result.Result result in projectA.GetTestResults())
            {
                if (!(result is Fact.Test.Result.Group))
                {
                    Fact.Log.Info("Add Test < " + result.Text + " >");
                    newproject.AddTestResult(result.Copy());
                }
                else
                {
                    if (!groups.ContainsKey(result.Text))
                    {
                        Fact.Log.Info("Add Group < " + result.Text + " >");
                        groups.Add(result.Text, CloneGroup((Fact.Test.Result.Group)result));
                    }
                    else
                    {
                        Fact.Log.Warning("Duplicated group < " + result.Text + " >");
                    }

                }
                texists.Add(result.Text);
            }

            foreach (Fact.Test.Result.Result result in projectB.GetTestResults())
            {
                if (!(result is Fact.Test.Result.Group))
                {
                    if (texists.Contains(result.Text))
                    {
                        Fact.Log.Warning("Ignored Test < " + result.Text + " >");
                        continue;
                    }
                    Fact.Log.Info("Add Test < " + result.Text + " >");
                    newproject.AddTestResult(result.Copy());
                }
                else
                {
                    if (!groups.ContainsKey(result.Text))
                    {
                        Fact.Log.Info("Add Group < " + result.Text + " >");
                        groups.Add(result.Text, CloneGroup((Fact.Test.Result.Group)result));
                    }
                    else
                    {
                        Fact.Log.Info("Merge Group < " + result.Text + " >");
                        groups[result.Text] = MergeGroup(groups[result.Text], CloneGroup((Fact.Test.Result.Group)result));
                    }
                }
            }

            foreach (Fact.Test.Result.Result result in groups.Values)
            {
                newproject.AddTestResult(result);
            }

            if (System.IO.File.Exists(newpackage))
            {
                try
                {
                    System.IO.File.Delete(newpackage);
                }
                catch
                {
                    Fact.Log.Error("Impossible to remove old file " + newpackage + ".");
                    return 1;
                }
            }

            newproject.Save(newpackage);
            return 0;
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Text;

namespace Remove
{
    class Remove : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Remove";
            }
        }

		public static string FindFactPackage(string fileName)
		{
			if (fileName == "") { return ""; }
			if (System.IO.Path.GetExtension(fileName).ToLower() == ".ff")
			{ return fileName; }
			return FindFactPackage(System.IO.Path.GetDirectoryName(fileName));
		}

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.ProgramName = "remove";
            CommandLine.AddStringInput("input", "input package", ""); CommandLine.AddShortInput("i", "input");
            CommandLine.AddStringInput("output", "output package", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddStringInput("path", "the path of the element that should be removed", ""); CommandLine.AddShortInput("p", "path");
            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }
            if (CommandLine["input"].IsDefault) { Fact.Log.Error("The input package must be set"); return 1; }
            string input = CommandLine["input"].AsString();
            string output = "";
            if (CommandLine["output"].IsDefault) { output = CommandLine["input"].AsString() + ".ff"; }
            else { output = CommandLine["output"].AsString(); }
            string path = CommandLine["path"].AsString();

            if (input == "")
			{
				Fact.Log.Error("No fact package specified");
				return 1;
			}
            if (!System.IO.File.Exists(input))
			{
                Fact.Log.Error("The package '" + input + "' does not exist.");
				return 1;
			}

            Fact.Processing.Project Project = null;
            try
            {
                Project = Fact.Processing.Project.Load(input);
            }
            catch
            {
                Fact.Log.Error("Impossible to load the file '" + input + "'");
                return 1;
            }
			if (Project.GetFile(path) != null)
			{
                Fact.Log.Verbose("Remove the file '" + path + "'");
                try
                {
                    Project.RemoveFile(path);
                }
                catch { }
			}
			else
			{
                Fact.Log.Verbose("Remove the directory '" + path + "'");
                try
                {
                    Project.RemoveDirectory(path);
                }
                catch { }
			}
			try
			{
                Project.Save(output);
			}
			catch
			{
				Fact.Log.Error ("Impossible to save the file '" + output + "'");
				return 1;
			}
            return 0;
        }
    }
}

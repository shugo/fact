﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Make
{
    class MakeFilePatcher
    {
        string[] _lines;
        public MakeFilePatcher(string[] Lines)
        {
            _lines = Lines;
        }

        public void RemoveComment()
        {
            for (int n = 0; n < _lines.Length; n++)
            {
                if (_lines[n].StartsWith("#")) { _lines[n] = ""; }
            }
        }

        public void PatchVariableUsage()
        {
            for (int n = 0; n < _lines.Length; n++)
            {
                string newLine = "";
                for (int x = 0; x < _lines[n].Length; x++)
                {
                    if (_lines[n][x] == '$' && (x + 1) < _lines[n].Length && _lines[n][x + 1] == '{')
                    {
                        newLine += "$(";
                        x += 2;
                        for (; x < _lines[n].Length; x++)
                        {
                            if (_lines[n][x] == '}') { newLine += ")"; break; }
                        }
                    }
                    else
                    {
                        newLine += _lines[n];
                    }
                }
                _lines[n] = newLine;
            }
        }

        public void PatchCCompiler()
        {
            string LocalCCompiler = "";
            for (int n = 0; n < _lines.Length; n++)
            {
                if (_lines[n] == "CC=gcc") { _lines[n] = "CC=" + LocalCCompiler; }
            }
        }
    }
}

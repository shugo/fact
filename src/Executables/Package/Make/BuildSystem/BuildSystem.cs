﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Make.BuildSystem
{
    class BuildSystem
    {
        /// <summary>
        /// Check if this build system is present in the specified project folder
        /// </summary>
        /// <returns>true if the buildsystem is present false otherwise</returns>
        public virtual bool IsBuildsystemPresent(Fact.Processing.Project Project, string Directory)
        {
            return false;
        }

        public virtual void Build(Fact.Processing.Project Project, string Directory, params string[] Parameters)
        {

        }

        public int Priotity
        {
            get { return 0; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Make.BuildSystem
{
    class Makefile : BuildSystem
    {
        public override bool IsBuildsystemPresent(Fact.Processing.Project Project, string Directory)
        {
            foreach (Fact.Processing.File file in Project.GetFiles(Directory, false, Fact.Processing.File.FileType.Makefile))
            {
                if (file.Name.ToLower() == "makefile") { return true; }
            }
            return false;
        }
    }
}

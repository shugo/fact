﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Make
{
    class Make : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Make";
            }
        }

		string FindCompatibleShell()
		{
			string Shell = "";
			if (System.IO.File.Exists("/bin/sh")) { Shell = "/bin/sh"; }
			else if (System.IO.File.Exists("/usr/bin/sh")) { Shell = "/usr/bin/sh"; }
			else if (System.IO.File.Exists("/usr/local/bin/sh")) { Shell = "/usr/local/bin/sh"; }
			else if (System.IO.File.Exists("/bin/zsh")) { Shell = "/bin/zsh"; }
			else if (System.IO.File.Exists("/usr/bin/zsh")) { Shell = "/usr/bin/zsh"; }
			else if (System.IO.File.Exists("/usr/local/bin/zsh")) { Shell = "/usr/local/bin/zsh"; }
			else if (System.IO.File.Exists("/bin/bash")) { Shell = "/bin/bash"; }
			else if (System.IO.File.Exists("/usr/bin/bash")) { Shell = "/usr/bin/bash"; }
			else if (System.IO.File.Exists("/usr/local/bin/bash")) { Shell = "/usr/local/bin/bash"; }
			else if (System.IO.File.Exists("/bin/tcsh")) { Shell = "/bin/tcsh"; }
			else if (System.IO.File.Exists("/usr/bin/tcsh")) { Shell = "/usr/bin/tcsh"; }
			else if (System.IO.File.Exists("usr/local/bin/tcsh")) { Shell = "usr/local/bin/tcsh"; }
			else if (System.IO.File.Exists("/bin/ksh")) { Shell = "/bin/ksh"; }
			else if (System.IO.File.Exists("/usr/bin/ksh")) { Shell = "/usr/bin/ksh"; }
			else if (System.IO.File.Exists("/usr/local/bin/ksh")) { Shell = "/usr/local/bin/ksh"; }
			else if (System.IO.File.Exists("/bin/ksh")) { Shell = "/bin/ksh"; }
			else if (System.IO.File.Exists("/usr/bin/ksh")) { Shell = "/usr/bin/ksh"; }
			else if (System.IO.File.Exists(@"C:\cygwin\bin\sh.exe")) { Shell = @"C:\cygwin\bin\sh.exe"; }
			else if (System.IO.File.Exists(@"C:\Program Files (x86)\cygwin\bin\sh.exe")) { Shell = @"C:\Program Files (x86)\cygwin\bin\sh.exe"; }
			else if (System.IO.File.Exists(@"C:\Program Files\cygwin\bin\sh.exe")) { Shell = @"C:\cygwin\bin\sh.exe"; }
			else if (System.IO.File.Exists(@"C:\MinGW\msys\1.0\bin\sh.exe")) { Shell = @"C:\MinGW\msys\1.0\bin\sh.exe"; }
			else if (System.IO.File.Exists(@"C:\MinGW2011\msys\1.0\bin\sh.exe")) { Shell = @"C:\MinGW2011\msys\1.0\bin\sh.exe"; }
			else if (System.IO.File.Exists(@"C:\MinGW2012\msys\1.0\bin\sh.exe")) { Shell = @"C:\MinGW2012\msys\1.0\bin\sh.exe"; }
			else if (System.IO.File.Exists(@"C:\MinGW2013\msys\1.0\bin\sh.exe")) { Shell = @"C:\MinGW2013\msys\1.0\bin\sh.exe"; }
			else if (System.IO.File.Exists(@"C:\MinGW2014\msys\1.0\bin\sh.exe")) { Shell = @"C:\MinGW2014\msys\1.0\bin\sh.exe"; }
			return Shell;
		}

        void DeleteDirectory(string Path)
        {
            try
            {
                Fact.Tools.RecursiveDelete(Path);
            }
            catch { }

        }

        static void Extract(Fact.Processing.Project Project, string Destination)
        {
            foreach (Fact.Processing.File file in Project)
            {
                string fullname = file.Directory + "/" + file.Name;
                string directory = "";
                if (file.Directory.StartsWith("/")) { directory = Destination + file.Directory; }
                else { directory = Destination + "/" + file.Directory; }
                Fact.Tools.RecursiveMakeDirectory(directory);
                if (file.Directory.EndsWith("/")) { directory += file.Name; }
                else { directory += "/" + file.Name; }
                if (System.IO.File.Exists(directory))
                {
                    try { System.IO.File.Delete(directory); }
                    catch { Fact.Log.Warning("Can't remove existing directory " + directory); }
                }
                if (file.Binary) { System.IO.File.WriteAllBytes(directory, file.GetBytes()); }
                else { System.IO.File.WriteAllText(directory, file.GetText(false)); }
                if (file.Type == Fact.Processing.File.FileType.PerlSource ||
                    file.Type == Fact.Processing.File.FileType.PythonSource ||
                    file.Type == Fact.Processing.File.FileType.LuaSource ||
                    file.Type == Fact.Processing.File.FileType.ShSource ||
                    file.Type == Fact.Processing.File.FileType.Executable)
                {
                    Fact.Tools.AddExecutionPermission(directory);
                }
            }
        }

        static void SaveProjectWithErrorTag(string InputProject, string OutputProject, string Message, string Description)
        {
            Fact.Processing.Project sourceProject = Fact.Processing.Project.Load(InputProject);
            sourceProject.AddTestResult(new Fact.Test.Result.Error("Makefile", (Message + "\n" + Description).Trim()));
            sourceProject.Save(OutputProject);
        }

        static bool CheckPath(string Path)
        {
            string envPath = Fact.Internal.Information.GetEnvPath();
			string[] envPaths;

			if (Fact.Internal.Information.CurrentOperatingSystem () == Fact.Internal.Information.OperatingSystem.Unix) 
			{
				envPaths = envPath.Split(':');
			}
			else
			{
				envPaths = envPath.Split(';');
			}
            foreach (string value in envPaths)
            {
                if (Path == value || Path == value + "/" || Path == value + "\\") { return true; }
            }

            return false;
        }

        static bool FindFile(string fileName, string Path, int Level, ref List<string> Makefiles)
        {
            try
            {
                if (Level <= 1)
                {
                    bool fileFound = false;
                    foreach (string file in System.IO.Directory.GetFiles(Path))
                    {
                        fileFound = true;
                        try
                        {
                            if (System.IO.Path.GetFileName(file) == fileName ||
                                System.IO.Path.GetFileName(file) == fileName.ToLower())
                            {
                                Makefiles.Add(file);
                            }
                        }
                        catch { }
                    }

                    try
                    {
                        foreach (string directory in System.IO.Directory.GetDirectories(Path))
                        {
                            fileFound = true;
                        }
                    }
                    catch { }
                    return fileFound;
                }
                if (Level > 1)
                {
                    bool fileFound = false;
                    foreach (string directory in System.IO.Directory.GetDirectories(Path))
                    {
                        try
                        {
                            if (FindFile(fileName, directory, Level - 1, ref Makefiles))
                            { fileFound = true; }
                        }
                        catch { }
                    }
                    return fileFound;
                }
            }
            catch { }
            return false;
        }

        static List<string> FindFile(string Filename, string Path)
        {
            List<string> Makefiles = new List<string>();
            
            int Level = 1;
            int maxLevel = 255;
            bool fileFound = true;

            while (fileFound && maxLevel > 0)
            {
                fileFound = FindFile(Filename, Path, Level, ref Makefiles);
                if (Makefiles.Count > 0) { return Makefiles; }
                Level++;
                maxLevel--;
            }
            return Makefiles;
        }

        static bool FindFileWithExtension(string Extension, string Path, int Level, ref List<string> Makefiles)
        {
            try
            {
                if (Level <= 1)
                {
                    bool fileFound = false;
                    foreach (string file in System.IO.Directory.GetFiles(Path))
                    {
                        fileFound = true;
                        try
                        {
                            if (System.IO.Path.GetExtension(file) == Extension ||
                                System.IO.Path.GetExtension(file).ToLower() == Extension.ToLower() ||
                                System.IO.Path.GetExtension(file).ToLower() == "." + Extension.ToLower() ||
                                "." + System.IO.Path.GetExtension(file).ToLower() == Extension.ToLower())
                            {
                                Makefiles.Add(file);
                            }
                        }
                        catch { }
                    }

                    try
                    {
                        foreach (string directory in System.IO.Directory.GetDirectories(Path))
                        {
                            fileFound = true;
                        }
                    }
                    catch { }
                    return fileFound;
                }
                if (Level > 1)
                {
                    bool fileFound = false;
                    foreach (string directory in System.IO.Directory.GetDirectories(Path))
                    {
                        try
                        {
                            if (FindFileWithExtension(Extension, directory, Level - 1, ref Makefiles))
                            { fileFound = true; }
                        }
                        catch { }
                    }
                    return fileFound;
                }
            }
            catch { }
            return false;
        }

        static List<string> FindFileWithExtension(string Extension, string Path)
        {
            List<string> Makefiles = new List<string>();

            int Level = 1;
            int maxLevel = 255;
            bool fileFound = true;

            while (fileFound && maxLevel > 0)
            {
                fileFound = FindFileWithExtension(Extension, Path, Level, ref Makefiles);
                if (Makefiles.Count > 0) { return Makefiles; }
                Level++;
                maxLevel--;
            }
            return Makefiles;
        }

        static KeyValuePair<string, string> ParseHook(string hook)
        {
            string[] parts = hook.Split ('=');
			string value = "";
			if (parts.Length < 2)
            {
                Fact.Log.Error("Invalid hook: \"" + hook + "\"");
                return new KeyValuePair<string,string>("", "");
            }
			value += parts[1];
			for (int x = 2; x < parts.Length; x++)
			{
				value += "=" + parts[x];
			}
			return new KeyValuePair<string,string>(parts[0], value);
        }

        Dictionary<string, BuildSystem.BuildSystem> _Buildsystems = new Dictionary<string, BuildSystem.BuildSystem>();

        public override int Run(string[] args)
        {
            int errorCode = 0;
            // Load the extract plugin
            string extractDirectory = "";
            List<string> rules = new List<string>();
            bool mustRemove = false;
            bool noMakeFile = true;
            bool testResult = false;
            bool testResultFail = false;
            bool verbose = false;
            bool hook = false;
            string message = "";
            List<KeyValuePair<string, string>> hooklst = new List<KeyValuePair<string, string>>();
            List<string> hidden = new List<string>();
            bool delete = false;

            bool permissive = false;

            string testResultContain = "";


            string input = "";
            string output = "";
            string server = "";
            string credential = "";
            string group = "";

            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddBooleanInput("make-as-test-result", "Store the result of the make command into a test result", false); CommandLine.AddShortInput("t", "make-as-test-result");
            CommandLine.AddBooleanInput("permissive", "The plugin try to recover from errors", false); CommandLine.AddShortInput("p", "permissive");
            CommandLine.AddBooleanInput("verbose", "Display more information during the execution", false); CommandLine.AddShortInput("v", "verbose");
            CommandLine.AddStringInput("output", "The project file that hold the result of the build", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddStringInput("input", "The project file that contains the package to build", ""); CommandLine.AddShortInput("i", "input");
            CommandLine.AddBooleanInput("help", "Display this help message", false); CommandLine.AddShortInput("h", "help");

            CommandLine.AddStringListInput("hook", "Add a hook on a command", new List<string>());
            CommandLine.AddStringListInput("hide", "Hide a specific folder during the build", new List<string>());
            CommandLine.AddStringListInput("rule", "Specify the rules to run (one or several)", new List<string>()); CommandLine.AddShortInput("r", "rule");

            CommandLine.AddStringInput("server", "Execute the build on a remote system", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddStringInput("group", "Set the agent group used to build the project (with --server)", "");
            CommandLine.AddStringInput("credential", "Use a specific credential to log in on the remote system", ""); CommandLine.AddShortInput("c", "credential");
            CommandLine.AddStringInput("timeout", "set the specified timeout will be out foreach substep of make (in ms)", "-1");



            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }
            testResult = CommandLine["make-as-test-result"].AsBoolean();
            permissive = CommandLine["permissive"].AsBoolean();
            verbose = CommandLine["verbose"].AsBoolean();
            input = CommandLine["input"].AsString();
            output = CommandLine["output"].AsString();
            group = CommandLine["group"].AsString();
            if (!CommandLine["hide"].IsDefault) { hidden = CommandLine["hide"].AsStringList(); }
            if (!CommandLine["hook"].IsDefault)
            {
                foreach (string hooks in CommandLine["hook"].AsStringList())
                {
                    hooklst.Add(ParseHook(hooks));
                }
            }
            server = CommandLine["server"].AsString();
            credential = CommandLine["credential"].AsString();
            // Fixme we should be able to handle multiple rules
            rules = CommandLine["rule"].AsStringList();

            int timeout = -1;
            if (!CommandLine["timeout"].IsDefault)
            {
                if (!int.TryParse(CommandLine["timeout"].AsString(), out timeout))
                {
                    Fact.Log.Error("The specified timeout is not a valid integer");
                    return 1;
                }
            }

            if (server != null && server.Trim() != "")
            {
                Fact.Log.Verbose("Connection to the server " + server + " ...");
                Fact.Network.Client Client = null;
                try
                { Client = Fact.Common.Remote.Connection.Connect(server, credential.Trim()); }
                catch (Exception e)
                {
                    Fact.Log.Error(e.Message);
                    return 1;
                }
                if (Client != null)
                {
                    Fact.Log.Verbose("Transmission to the server " + server + " ...");
                    try
                    {
                        Fact.Common.Remote.Job job = new Fact.Common.Remote.Job(Client, group);
                        if (!job.Valid)
                        {
                            Fact.Log.Error("Error while creating the job on the server: " + job.GetLastError());
                            return 1;
                        }
                        job.SendFile(input, "input.ff");
                        if (!job.Valid)
                        {
                            Fact.Log.Error("Error while uploading the package: " + job.GetLastError());
                            return 1;
                        }

                        {
                            {
                                List<string> Arguments = new List<string>();
                                Arguments.Add("package");
                                Arguments.Add("make");
                                Arguments.Add("--input");
                                Arguments.Add("./input.ff");
                                Arguments.Add("--output");
                                Arguments.Add("./output.ff");
                                if (rules.Count > 0)
                                {
                                    Arguments.Add("--rule");
                                    foreach (string rule in rules)
                                    {
                                        Arguments.Add(rule);
                                    }
                                }
                                foreach (string hiddenfile in hidden)
                                {
                                    Arguments.Add("--hide");
                                    Arguments.Add(hiddenfile);
                                }
                                foreach (KeyValuePair<string, string> hookp in hooklst)
                                {
                                    Arguments.Add("--hook");
                                    Arguments.Add(hookp.Key + "=" + hookp.Value);
                                }
                                if (permissive) { Arguments.Add("--permissive"); }
                                if (testResult) { Arguments.Add("--make-as-test-result"); }
                                if (timeout > 0) { Arguments.Add("--timeout"); Arguments.Add(timeout.ToString()); }
                                job.Run("fact", Arguments.ToArray());
                            }
                            job.ExportFile("output.ff");
                        }
                        job.Execute();
                        if (!job.Valid)
                        {
                            Fact.Log.Error("Error while executing the job: " + job.GetLastError());
                            return 1;
                        }
                        Fact.Processing.File outputpackage = job.GetFile("output.ff");
                        if (outputpackage == null)
                        {
                            Fact.Log.Error("Error while collecting the result from the server: " + job.GetLastError());
                            return 1;
                        }
                        job.Delete();
                        outputpackage.Extract(output);
                    }
                    catch (Exception e)
                    {
                        Fact.Log.Error(e.Message);
                        return 1;
                    }
                }
                return 0;
            }

            if (output == "" && (System.IO.Path.GetExtension(input) == ".ff"))
            {
                extractDirectory = "./";
            }
            else if (System.IO.Path.GetExtension(input) != ".ff")
            {
                if (output == "")
                {
                    Fact.Log.Info("Invalid output file");
                    return 1;
                }

                if (System.IO.Directory.Exists(input))
                {
                    extractDirectory = input;
                }
            }
            else
            {
                mustRemove = true;

                if (output == "")
                {
                    Fact.Log.Info("Invalid output file");
                    return 1;
                }

                if (!System.IO.File.Exists(input))
                {
                    Fact.Log.Error("Can't load " + input + ".");
                    return 1;
                }

                extractDirectory = Fact.Tools.CreateTempDirectory();
                {
                    try
                    {
                        Fact.Processing.Project inputProject = Fact.Processing.Project.Load(input);
                        Extract(inputProject, extractDirectory);
                        inputProject = null;
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to extract the files");
                    }
                }
                // FIXME
                // TODO
                // You should add an option to enamble or disable that
                foreach (string folder in hidden)
                {
                    try
                    {
                        Fact.Log.Verbose("Hide: " + extractDirectory + "/" + folder);
                        Fact.Tools.RecursiveDelete(extractDirectory + "/" + folder);
                    }
                    catch
                    {
                    }
                }

            }
            /*
            List<BuildSystem.BuildSystem> buildsystem = new List<BuildSystem.BuildSystem>();
            buildsystem.Sort((BuildSystem.BuildSystem a, BuildSystem.BuildSystem b) => { return a.Priotity.CompareTo(b.Priotity); });

            for (int n = 0; n < buildsystem.Count; n++)
            {
                if (buildsystem[n].IsBuildsystemPresent(input, "/"))
                {
                    buildsystem[n].Build(input, "/", rules);
                }
            }
            */
            string MSBuild = Fact.Internal.Information.GetMSBuild();
            string Make = Fact.Internal.Information.GetMakeBinary();
            string CMake = Fact.Internal.Information.Where("cmake");
            // Check and patch the environement
            if (Fact.Internal.Information.GetEnvVariable("CC") == "")
            {
                string cc = Fact.Internal.Information.GetCCBinary();
                if (cc != "")
                {
                    string ccName = System.IO.Path.GetFileName(cc);
                    string ccPath = System.IO.Path.GetDirectoryName(cc);
                    Fact.Log.Warning("No CC variable. CC will be set to : " + ccName);
                    System.Environment.SetEnvironmentVariable("CC", ccName);
                    if (System.IO.File.Exists(cc) && !CheckPath(ccPath))
                    {
                        Fact.Log.Warning("The path of CC is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(ccPath);
                    }
                }
            }
            {
                string chmod = Fact.Internal.Information.GetChmodBinary();
                if (System.IO.File.Exists(chmod))
                {
                    string chmodPath = System.IO.Path.GetDirectoryName(chmod);
                    if (System.IO.File.Exists(chmod) && !CheckPath(chmod))
                    {
                        Fact.Log.Warning("The path of chmod is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(chmodPath);
                    }
                }
                else
                {
                    Fact.Log.Warning("Chmod not found.");
                }
            }
            if (Fact.Internal.Information.GetEnvVariable("JAVAC") == "")
            {
                string javac = Fact.Internal.Information.GetJAVACBinary();
                if (javac != "")
                {
                    string javacName = System.IO.Path.GetFileName(javac);
                    string javacPath = System.IO.Path.GetDirectoryName(javac);
                    Fact.Log.Warning("No JAVAC variable. JAVAC will be set to : " + javacName);
                    System.Environment.SetEnvironmentVariable("JAVAC", javacName);
                    if (System.IO.File.Exists(javac) && !CheckPath(javacPath))
                    {
                        Fact.Log.Warning("The path of JAVAC is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(javacPath);
                    }
                }
            }

            if (System.IO.File.Exists("/bin/cmake")) { CMake = "/bin/cmake"; }
            else if (System.IO.File.Exists("/usr/bin/cmake")) { CMake = "/usr/bin/cmake"; }
            else if (System.IO.File.Exists("/usr/local/bin/cmake")) { CMake = "/usr/local/bin/cmake"; }
            else if (System.IO.File.Exists(@"C:\Program Files (x86)\CMake 2.8\bin\cmake.exe")) { CMake = @"C:\Program Files (x86)\CMake 2.8\bin\cmake.exe"; }
            else if (System.IO.File.Exists(@"C:\Program Files\CMake 2.8\bin\cmake.exe")) { CMake = @"C:\Program Files\CMake 2.8\bin\cmake.exe"; }
            else if (System.IO.File.Exists(@"C:\Program Files (x86)\CMake 2.6\bin\cmake.exe")) { CMake = @"C:\Program Files (x86)\CMake 2.6\bin\cmake.exe"; }
            else if (System.IO.File.Exists(@"C:\Program Files\CMake 2.6\bin\cmake.exe")) { CMake = @"C:\Program Files\CMake 2.6\bin\cmake.exe"; }
            if (Fact.Internal.Information.GetEnvVariable("CMAKE") == "")
            {
                if (CMake != "")
                {
                    string CMakeName = System.IO.Path.GetFileName(CMake);
                    string CMakePath = System.IO.Path.GetDirectoryName(CMake);
                    Fact.Log.Warning("No CMAKE variable. CMAKE will be set to : " + CMakeName);
                    System.Environment.SetEnvironmentVariable("CMAKE", CMakeName);
                    if (System.IO.File.Exists(CMake) && !CheckPath(CMakePath))
                    {
                        Fact.Log.Warning("The path of CMAKE is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(CMakePath);
                    }
                }
            }
            else
            {
                if (!System.IO.File.Exists(CMake))
                {
                    Fact.Log.Warning("CMake has not been found");
                    Fact.Log.Warning("The name stored in the variable CMAKE will be used");
                    string CMakeName = Fact.Internal.Information.GetEnvVariable("CMAKE");
                    string CMakeExe = Fact.Internal.Information.Where(CMakeName);
                    if (System.IO.File.Exists(CMakeExe))
                    {
                        Fact.Log.Warning("CMake has been found: " + CMakeExe);
                        CMake = CMakeExe;
                        string CMakePath = System.IO.Path.GetDirectoryName(CMake);
                        if (!CheckPath(CMakePath))
                        {
                            Fact.Log.Warning("The path of CMAKE is not in PATH and will be added.");
                            Fact.Internal.Information.AppendEnvPath(CMakePath);
                        }
                    }
                }
            }

            string AntHome = Fact.Internal.Information.GetEnvVariable("ANT_HOME");



            string Ant = Fact.Internal.Information.Where("ant");
            if (System.IO.File.Exists("/bin/ant")) { Ant = "/bin/ant"; }
            else if (System.IO.File.Exists("/usr/bin/ant")) { Ant = "/usr/bin/ant"; }
            else if (System.IO.File.Exists("/usr/local/bin/ant")) { Ant = "/usr/local/bin/ant"; }

            if (!System.IO.File.Exists(Ant)) { Ant = Fact.Internal.Information.Where("ant-launcher.jar"); }

            if (System.IO.File.Exists("/bin/ant.jar")) { Ant = "/bin/ant-launcher.jar"; }
            else if (System.IO.File.Exists("/usr/bin/ant.jar")) { Ant = "/usr/bin/ant-launcher.jar"; }
            else if (System.IO.File.Exists("/usr/local/bin/ant.jar")) { Ant = "/usr/local/bin/ant-launcher.jar"; }
            else if (System.IO.File.Exists("/usr/share/java/ant.jar")) { Ant = "/usr/share/java/ant-launcher.jar"; }
            else if (System.IO.File.Exists("/usr/local/share/java/ant.jar")) { Ant = "/usr/local/share/java/ant-launcher.jar"; }
            else if (System.IO.File.Exists(@"C:\Program Files (x86)\apache-ant-1.9.0\lib\ant-launcher.jar"))
            { Ant = @"C:\Program Files (x86)\apache-ant-1.9.0\lib\ant-launcher.jar"; }
            else if (System.IO.File.Exists(@"C:\Program Files (x86)\apache-ant\lib\ant-launcher.jar"))
            { Ant = @"C:\Program Files (x86)\apache-ant\lib\ant-launcher.jar"; }
            else if (System.IO.File.Exists(@"C:\Program Files (x86)\ant\lib\ant-launcher.jar"))
            { Ant = @"C:\Program Files (x86)\ant\lib\ant-launcher.jar"; }
            else if (System.IO.File.Exists(@"C:\apache-ant\lib\ant-launcher.jar"))
            { Ant = @"C:\apache-ant\lib\ant-launcher.jar"; }
            else if (System.IO.File.Exists(@"C:\Program Files (x86)\ant\lib\ant-launcher.jar"))
            { Ant = @"C:\ant\lib\ant-launcher.jar"; }

            else if (System.IO.File.Exists(@"C:\Program Files (x86)\JetBrains\IntelliJ IDEA Community Edition 11.1.1\lib\ant\lib\ant-launcher.jar"))
            { Ant = @"C:\Program Files (x86)\JetBrains\IntelliJ IDEA Community Edition 11.1.1\lib\ant\lib\ant-launcher.jar"; }

            if (Fact.Internal.Information.GetEnvVariable("ANT") == "")
            {
                if (Ant != "")
                {
                    string ScriptToAnt = Ant;
                    // Specific case. We need to load the jar file to run ant directly
                    // But we also need to set if it is possible the ant sh script for make

                    if (System.IO.File.Exists("/bin/ant")) { ScriptToAnt = "/bin/ant"; }
                    else if (System.IO.File.Exists("/usr/bin/ant")) { ScriptToAnt = "/usr/bin/ant"; }
                    else if (System.IO.File.Exists("/usr/local/bin/ant")) { ScriptToAnt = "/usr/local/bin/ant"; }

                    string AntName = System.IO.Path.GetFileName(ScriptToAnt);
                    string StriptAntPath = System.IO.Path.GetDirectoryName(ScriptToAnt);
                    string AntPath = System.IO.Path.GetDirectoryName(Ant);

                    Fact.Log.Warning("No ANT variable. ANT will be set to : " + AntName);
                    System.Environment.SetEnvironmentVariable("ANT", AntName);
                    if (System.IO.File.Exists(ScriptToAnt) && !CheckPath(StriptAntPath))
                    {
                        Fact.Log.Warning("The path of ANT is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(StriptAntPath);
                    }
                    if (System.IO.File.Exists(Ant) && !CheckPath(AntPath))
                    {
                        Fact.Log.Warning("The path of the ant.jar is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(StriptAntPath);
                    }
                }
            }
            else
            {
                if (!System.IO.File.Exists(Ant))
                {
                    Fact.Log.Warning("Ant has not been found");
                    Fact.Log.Warning("The name stored in the variable ANT will be used");
                    string AntName = Fact.Internal.Information.GetEnvVariable("ANT");
                    string AntExe = Fact.Internal.Information.Where(AntName);
                    if (System.IO.File.Exists(AntExe))
                    {
                        Fact.Log.Warning("Ant has been found: " + AntExe);
                        Ant = AntExe;
                        string AntPath = System.IO.Path.GetDirectoryName(Ant);
                        if (!CheckPath(AntPath))
                        {
                            Fact.Log.Warning("The path of ANT is not in PATH and will be added.");
                            Fact.Internal.Information.AppendEnvPath(AntPath);
                        }
                    }
                }
            }

            if (Fact.Internal.Information.GetEnvVariable("JAVA_HOME") == "")
            {
                string JavaHome = "";

                if (System.IO.Directory.Exists(@"C:\Program Files\Java\jdk1.8.0_01"))
                { JavaHome = @"C:\Program Files\Java\jdk1.8.0_01"; }
                else if (System.IO.Directory.Exists(@"C:\Program Files\Java\jdk1.8"))
                { JavaHome = @"C:\Program Files\Java\jdk1.8"; }
                else if (System.IO.Directory.Exists(@"C:\Program Files\Java\jdk1.7.0_03"))
                { JavaHome = @"C:\Program Files\Java\jdk1.7.0_03"; }
                else if (System.IO.Directory.Exists(@"C:\Program Files\Java\jdk1.7.0_02"))
                { JavaHome = @"C:\Program Files\Java\jdk1.7.0_02"; }
                else if (System.IO.Directory.Exists(@"C:\Program Files\Java\jdk1.7.0_01"))
                { JavaHome = @"C:\Program Files\Java\jdk1.7.0_01"; }
                else if (System.IO.Directory.Exists(@"C:\Program Files\Java\jdk1.7"))
                { JavaHome = @"C:\Program Files\Java\jdk1.7"; }
                else if (System.IO.Directory.Exists(@"C:\Program Files\Java\jdk1.6"))
                { JavaHome = @"C:\Program Files\Java\jdk1.6"; }
                else if (System.IO.Directory.Exists(@"/System/Library/Frameworks/JavaVM.framework/Home"))
                { JavaHome = @"/System/Library/Frameworks/JavaVM.framework/Home"; }
                else if (System.IO.Directory.Exists(@"/usr/lib/jvm/default-java"))
                { JavaHome = @"/usr/lib/jvm/default-java"; }
                else if (System.IO.Directory.Exists(@"/usr/lib/jvm/java"))
                { JavaHome = @"/usr/lib/jvm/java"; }
                else if (System.IO.Directory.Exists("/usr/lib/jvm/java-8-oracle"))
                { JavaHome = @"/usr/lib/jvm/java-8-oracle"; }
                else if (System.IO.Directory.Exists("/usr/lib/jvm/java-7-oracle"))
                { JavaHome = @"/usr/lib/jvm/java-7-oracle"; }
                else if (System.IO.Directory.Exists("/usr/lib/jvm/java-6-oracle"))
                { JavaHome = @"/usr/lib/jvm/java-6-oracle"; }
                else if (System.IO.Directory.Exists("/usr/lib/jvm/java-1.8.0"))
                { JavaHome = @"/usr/lib/jvm/java-1.8.0"; }
                else if (System.IO.Directory.Exists("/usr/lib/jvm/java-1.7.0"))
                { JavaHome = @"/usr/lib/jvm/java-1.7.0"; }
                else if (System.IO.Directory.Exists("/usr/lib/jvm/java-1.6.0"))
                { JavaHome = @"/usr/lib/jvm/java-1.6.0"; }
                else if (System.IO.Directory.Exists(@"/usr/local/lib/jvm/default-java"))
                { JavaHome = @"/usr/local/lib/jvm/default-java"; }
                else if (System.IO.Directory.Exists(@"/usr/local/lib/jvm/java"))
                { JavaHome = @"/usr/local/lib/jvm/java"; }
                else if (System.IO.Directory.Exists("/usr/local/lib/jvm/java-1.6.0"))
                { JavaHome = @"/usr/local/lib/jvm/java-1.6.0"; }


                if (JavaHome != "")
                {
                    Fact.Log.Warning("No JAVA_HOME variable. JAVA_HOME will be set to : " + JavaHome);
                    System.Environment.SetEnvironmentVariable("JAVA_HOME", JavaHome);

                }
            }

            if (Fact.Internal.Information.GetEnvVariable("JAVA_HOME") != "")
            {
                string Home = Fact.Internal.Information.GetEnvVariable("JAVA_HOME");
                if (!Home.EndsWith("/")) { Home += "/"; }
                if (!CheckPath(Home + "bin"))
                {
                    Fact.Log.Warning("The path of JAVA_HOME/bin is not in PATH and will be added.");
                    Fact.Internal.Information.AppendEnvPath(Home + "bin");
                }
            }

            int jdkversion = 17;
            try
            {
                string envjavahome = Fact.Internal.Information.GetEnvVariable("JAVA_HOME");
                if (envjavahome.Contains("1.9")) { jdkversion = 19; }
                else if (envjavahome.Contains("1.8")) { jdkversion = 18; }
                else if (envjavahome.Contains("1.7")) { jdkversion = 17; }
                else if (envjavahome.Contains("1.6")) { jdkversion = 16; }
            }
            catch { }

            bool oraclejdk = false;
            try
            {
                string envjavahome = Fact.Internal.Information.GetEnvVariable("JAVA_HOME");
                if (envjavahome.ToLower().Contains("oracle")) { oraclejdk = true; }
                if (Fact.Internal.Information.CurrentOperatingSystem() ==
                    Fact.Internal.Information.OperatingSystem.MicrosoftWindows &&
                    !envjavahome.Contains("open"))
                {
                    oraclejdk = true;
                }
            }
            catch { }

            if (Fact.Internal.Information.GetEnvVariable("CXX") == "")
            {
                string cxx = Fact.Internal.Information.GetCXXBinary();
                if (cxx != "")
                {
                    string cxxName = System.IO.Path.GetFileName(cxx);
                    string cxxPath = System.IO.Path.GetDirectoryName(cxx);
                    Fact.Log.Warning("No CXX variable. CXX will be set to : " + cxxName);
                    System.Environment.SetEnvironmentVariable("CXX", cxxName);
                    if (!CheckPath(cxxPath))
                    {
                        Fact.Log.Warning("The path of CXX is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(cxxPath);
                    }
                }
            }

            if (Fact.Internal.Information.GetEnvVariable("MAKE") == "")
            {
                if (Make != "")
                {
                    string makeName = System.IO.Path.GetFileName(Make);
                    string makePath = System.IO.Path.GetDirectoryName(Make);
                    Fact.Log.Warning("No MAKE variable. MAKE will be set to : " + makeName);
                    System.Environment.SetEnvironmentVariable("MAKE", makeName);
                    if (System.IO.File.Exists(Make) && !CheckPath(makePath))
                    {
                        Fact.Log.Warning("The path of MAKE is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(makePath);
                    }

                }
            }

            if (Fact.Internal.Information.GetEnvVariable("MSBUILD") == "")
            {
                if (MSBuild != "")
                {
                    string MSBuildName = System.IO.Path.GetFileName(MSBuild);
                    string MSBuildPath = System.IO.Path.GetDirectoryName(MSBuild);
                    Fact.Log.Warning("No MSBUILD variable. MSBUILD will be set to : " + MSBuildName);
                    System.Environment.SetEnvironmentVariable("MSBUILD", MSBuildPath);
                    if (System.IO.File.Exists(MSBuild) && !CheckPath(MSBuildPath))
                    {
                        Fact.Log.Warning("The path of MSBUILD is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(MSBuildPath);
                    }

                }
            }

            string Mono = Fact.Internal.Information.GetMonoBinary();
            if (System.IO.File.Exists(Mono))
            {
                if (Fact.Internal.Information.GetEnvVariable("MONO") == "")
                {
                    if (System.IO.File.Exists(Mono))
                    {
                        if (Mono != "")
                        {
                            string monoName = System.IO.Path.GetFileName(Mono);
                            string monoPath = System.IO.Path.GetDirectoryName(Mono);
                            Fact.Log.Warning("No MONO variable. MONO will be set to : " + monoName);
                            System.Environment.SetEnvironmentVariable("MONO", monoPath);
                            if (!CheckPath(monoPath))
                            {
                                Fact.Log.Warning("The path of MONO is not in PATH and will be added.");
                                Fact.Internal.Information.AppendEnvPath(monoPath);
                            }

                        }
                    }
                }
                else
                {
                    string monoPath = System.IO.Path.GetDirectoryName(Mono);
                    if (!CheckPath(monoPath))
                    {
                        Fact.Log.Warning("The path of MONO is not in PATH and will be added.");
                        Fact.Internal.Information.AppendEnvPath(monoPath);
                    }
                }
            }
            else
            {
                Fact.Log.Warning("Mono has not been found.");
                Fact.Log.Warning("Some linux specific .NET project cannot work whith other .NET VM");
            }

            // Display info
            if (verbose)
            {
                if (Make != "") { Fact.Log.Verbose(Make + " will be used as make"); }
                if (CMake != "") { Fact.Log.Verbose(CMake + " will be used as cmake"); }
                if (Ant != "") { Fact.Log.Verbose(Ant + " will be used as ant"); }
                if (MSBuild != "") { Fact.Log.Verbose(MSBuild + " will be used as msbuild"); }
            }

            // Find the bootstrap and the configure
            List<string> Bootstrap = FindFile("bootstrap", extractDirectory);
            if (Bootstrap.Count == 0) { Bootstrap = FindFile("bootstraps", extractDirectory); }
            if (Bootstrap.Count == 0)
            {
                Bootstrap = FindFile("autogen.sh", extractDirectory);
                if (Bootstrap.Count > 0) { Fact.Log.Warning("Autogen should not be used. Use bootstrap instead."); }
            }
            if (Bootstrap.Count != 0)
            {
                string Shell = FindCompatibleShell();

                foreach (string bootstrap in Bootstrap)
                {
                    Fact.Log.Info("Runing bootstrap " + bootstrap + " ...");
                    try
                    {
                        string executable = Shell;
                        try
                        {
                            string[] lines = System.IO.File.ReadAllLines(bootstrap);
                            if (lines.Length > 0 && lines[0].StartsWith("#!"))
                            {
                                executable = lines[0];
                                executable = executable.Substring(2, executable.Length - 2);
                                executable = executable.Trim();
                                if (executable.StartsWith("env") ||
                                    executable.StartsWith("/usr/bin/env ") ||
                                    executable.StartsWith("/bin/env ") ||
                                    executable.StartsWith("/usr/local/bin/env ") ||
                                    executable.StartsWith("/usr/local/sbin/env "))
                                {
                                    string[] words =
                                        executable.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                                    message += "Bootstrap: Use internal builtin where\n";

                                    if (words.Length == 2)
                                    {
                                        executable = Fact.Internal.Information.Where(words[1]);
                                        message += "Bootstrap: Using executable " + executable + "\n";

                                        if (executable == "")
                                            executable = words[1];
                                        if (!System.IO.File.Exists(executable))
                                        {
                                            Fact.Log.Warning("Executable " + executable + " not found");
                                            if (executable.ToLower() == "python") { executable = Fact.Internal.Information.GetPythonBinary(); }
                                            else if (executable.ToLower() == "sh") { executable = Shell; }
                                            else if (executable.ToLower() == "bash") { executable = Shell; }
                                            else if (executable.ToLower() == "ksh") { executable = Shell; }
                                            else if (executable.ToLower() == "zsh") { executable = Shell; }
                                        }
                                    }
                                }
                                else if (!System.IO.File.Exists(executable))
                                {
                                    Fact.Log.Warning("Bootstrap: Executable " + executable + " not found");
                                    if (executable.ToLower() == "/usr/bin/sh") { executable = Shell; }
                                    else if (executable.ToLower() == "/bin/sh") { executable = Shell; }
                                    else if (executable.ToLower() == "/usr/local/bin/sh") { executable = Shell; }
                                    else if (executable.ToLower() == "/bin/bash") { executable = Shell; }
                                    else if (executable.ToLower() == "/usr/bin/bash") { executable = Shell; }
                                    else if (executable.ToLower() == "/usr/local/bin/bash") { executable = Shell; }
                                    else if (executable.ToLower() == "/usr/bin/python") { executable = Fact.Internal.Information.GetPythonBinary(); }
                                    else if (executable.ToLower() == "/usr/bin/perl") { executable = Fact.Internal.Information.Where("perl"); }
                                    else if (executable.ToLower() == "/usr/bin/ruby") { executable = Fact.Internal.Information.Where("ruby"); }
                                    else if (executable.ToLower() == "/usr/bin/lua")
                                    {
                                        executable = Fact.Internal.Information.Where("lua");
                                        if (!System.IO.File.Exists(executable))
                                        {
                                            executable = Fact.Internal.Information.Where("lua52");
                                        }
                                        if (!System.IO.File.Exists(executable))
                                        {
                                            executable = Fact.Internal.Information.Where("lua51");
                                        }
                                    }
                                    if (executable == "") { executable = Shell; }

                                    Fact.Log.Warning("Bootstrap: Executable " + executable + " will be used instead");
                                    message += "Bootstrap: Using executable " + executable + "\n";
                                }
                            }
                        }
                        catch

                        { }
                        Fact.Runtime.Process.Result Result = null;
                        using (Fact.Runtime.Process ShellProcess = new Fact.Runtime.Process(executable, System.IO.Path.GetDirectoryName(bootstrap), (new System.IO.FileInfo(bootstrap)).FullName))
                        {
                            Result = ShellProcess.Run(timeout);
                        }
                        if (Result.ExitCode != 0)
                        {
                            if (Result.StdOut != "")
                                Fact.Log.Error(Result.StdOut);
                            if (Result.StdErr != "")
                                Fact.Log.Error(Result.StdErr);
                        }
                        if (verbose)
                        {
                            Fact.Log.Verbose(Result.StdOut);
                        }
                    }
                    catch
                    {
                        Fact.Log.Error("Error when executing bootstrap.");
                    }
                }
            }

            // Find the bootstrap and the configure
            List<string> Configures = FindFile("configure", extractDirectory);
            if (Configures.Count != 0)
            {

                string Shell = FindCompatibleShell();

                foreach (string configure in Configures)
                {
                    Fact.Log.Info("Runing configure " + configure + " ...");
                    try
                    {
                        string executable = Shell;
                        try
                        {
                            string[] lines = System.IO.File.ReadAllLines(configure);
                            if (lines.Length > 0 && lines[0].StartsWith("#!"))
                            {
                                executable = lines[0];
                                executable = executable.Substring(2, executable.Length - 2);
                                executable = executable.Trim();
                                if (executable.StartsWith("env") ||
                                    executable.StartsWith("/usr/bin/env ") ||
                                    executable.StartsWith("/bin/env ") ||
                                    executable.StartsWith("/usr/local/bin/env ") ||
                                    executable.StartsWith("/usr/local/sbin/env "))
                                {
                                    string[] words =
                                        executable.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                                    message += "Configure: Use internal builtin where\n";
                                    if (words.Length == 2)
                                    {
                                        executable = Fact.Internal.Information.Where(words[1]);
                                        message += "Configure: using executable " + executable + "\n";
                                        if (executable == "")
                                            executable = words[1];
                                        if (!System.IO.File.Exists(executable))
                                        {
                                            Fact.Log.Warning("Executable " + executable + " not found");
                                            if (executable.ToLower() == "python")
                                            {
                                                executable = Fact.Internal.Information.GetPythonBinary();
                                            }
                                            else if (executable.ToLower() == "sh")
                                            {
                                                executable = Shell;
                                            }
                                            else if (executable.ToLower() == "bash")
                                            {
                                                executable = Shell;
                                            }
                                            else if (executable.ToLower() == "ksh")
                                            {
                                                executable = Shell;
                                            }
                                            else if (executable.ToLower() == "zsh")
                                            {
                                                executable = Shell;
                                            }
                                        }
                                    }
                                }
                                else if (!System.IO.File.Exists(executable))
                                {
                                    Fact.Log.Warning("Executable " + executable + " not found");
                                    if (executable.ToLower() == "/usr/bin/sh")
                                    {
                                        executable = Shell;
                                    }
                                    else if (executable.ToLower() == "/bin/sh")
                                    {
                                        executable = Shell;
                                    }
                                    else if (executable.ToLower() == "/usr/local/bin/sh")
                                    {
                                        executable = Shell;
                                    }
                                    else if (executable.ToLower() == "/bin/bash")
                                    {
                                        executable = Shell;
                                    }
                                    else if (executable.ToLower() == "/usr/bin/bash")
                                    {
                                        executable = Shell;
                                    }
                                    else if (executable.ToLower() == "/usr/local/bin/bash")
                                    {
                                        executable = Shell;
                                    }
                                    else if (executable.ToLower() == "/usr/bin/python")
                                    {
                                        executable = Fact.Internal.Information.GetPythonBinary();
                                    }
                                    else if (executable.ToLower() == "/usr/bin/perl")
                                    {
                                        executable = Fact.Internal.Information.Where("perl");
                                    }
                                    else if (executable.ToLower() == "/usr/bin/ruby")
                                    {
                                        executable = Fact.Internal.Information.Where("ruby");
                                    }
                                    else if (executable.ToLower() == "/usr/bin/lua")
                                    {
                                        executable = Fact.Internal.Information.Where("lua");
                                        if (!System.IO.File.Exists(executable))
                                        {
                                            executable = Fact.Internal.Information.Where("lua52");
                                        }
                                        if (!System.IO.File.Exists(executable))
                                        {
                                            executable = Fact.Internal.Information.Where("lua51");
                                        }
                                    }
                                    if (executable == "")
                                    {
                                        executable = Shell;
                                    }

                                    Fact.Log.Warning("Executable " + executable + " will be used instead");
                                    message += "Configure: Using executable " + executable + "\n";


                                }
                            }
                        }
                        catch
                        {
                        }
                        Fact.Runtime.Process.Result Result = null;
                        using (Fact.Runtime.Process ShellProcess = new Fact.Runtime.Process(executable, System.IO.Path.GetDirectoryName(configure), (new System.IO.FileInfo(configure)).FullName))
                        {
                            Result = ShellProcess.Run(timeout);
                        }
                        if (verbose)
                        {
                            Fact.Log.Verbose("Configure: " + Result.StdOut);
                        }
                        if (Result.ExitCode != 0)
                        {
                            if (Result.StdOut != "")
                                Fact.Log.Error(Result.StdOut);
                            if (Result.StdErr != "")
                                Fact.Log.Error(Result.StdErr);
                        }
                        if (verbose)
                        {
                            Fact.Log.Verbose(Result.StdOut);
                        }
                    }
                    catch (Exception e)
                    {
                        Fact.Log.Error("Error when executing configure.");
                        Fact.Log.Exception(e);
                    }
                }
            }

            // Try to find build.xml
            List<string> Ants = FindFile("build.xml", extractDirectory);
            if (Ants.Count != 0)
            {
                noMakeFile = false;
                int failCount = 0;
                foreach (string makefile in Ants)
                {
                    try
                    {
                        if (!System.IO.File.Exists(Ant))
                        {
                            Fact.Log.Error("Executable Ant not found");
                            if (mustRemove)
                                DeleteDirectory(extractDirectory);
                            return 1;
                        }

                        Fact.Log.Verbose("Ant : " + makefile.Substring(extractDirectory.Length));
                        string hackclasspath = "";
                        if (oraclejdk && jdkversion >= 18)
                        {
                            try
                            {
                                string envjavahome = Fact.Internal.Information.GetEnvVariable("JAVA_HOME");
                                hackclasspath = " -cp '" + envjavahome + "/lib";
                                string classpath = Fact.Internal.Information.GetEnvVariable("CLASSPATH");
                                if (classpath.Trim() != "") { hackclasspath += hackclasspath + ":" + classpath; }
                                hackclasspath += "'";
                            }
                            catch { }
                        }
                        System.IO.FileInfo buildinfo = new System.IO.FileInfo(makefile);

                        Fact.Runtime.Process.Result Result = null;
                        foreach (string rule in rules)
                        {
                            using (Fact.Runtime.Process MakeProcess = new Fact.Runtime.Process(Ant, System.IO.Path.GetDirectoryName(makefile), " -file \"" + buildinfo.FullName + "\" " + rule + hackclasspath))
                            {
                                Result = MakeProcess.Run(-1);
                            }
                            if (Result.ExitCode != 0)
                            {

                                Fact.Log.Error("Ant: ");
                                if (testResult)
                                {
                                    testResultContain += "ant:\n" + Result.StdOut + "\n" + Result.StdErr;
                                    testResultFail = true;
                                }
                                string[] outs = Result.StdOut.Split(new char[] { '\n', '\r' });
                                foreach (string str in outs) { if (str.Length > 0) { Fact.Log.Error("  " + str); } }
                                string[] errors = Result.StdErr.Split(new char[] { '\n', '\r' });
                                foreach (string error in errors) { if (error.Length > 0) { Fact.Log.Error("  " + error); } }
                                failCount++;
                            }
                        }
                    }
                    catch { }
                }
                if (failCount >= Ants.Count)
                {
                    Fact.Log.Error("All builds have failed.");
                    if (mustRemove)
                        DeleteDirectory(extractDirectory);
                    SaveProjectWithErrorTag(input, output, message, testResultContain);
                    return 1;
                }
                if (failCount > 0)
                {
                    Fact.Log.Warning("Some builds have failed.");
                }
            }

            // Detect Makefile
            List<string> Makefiles = FindFile("Makefile", extractDirectory);
            if (Makefiles.Count == 0) { Makefiles = FindFile("makefile", extractDirectory); }

        FindMSBuild:
            List<string> CSProjs = FindFileWithExtension("csproj", extractDirectory);
            if (CSProjs.Count != 0)
            {
                int failCount = 0;
                foreach (string csp in CSProjs)
                {
                    string relativepath = csp;
                    if (relativepath.StartsWith(extractDirectory)) { relativepath = relativepath.Substring(extractDirectory.Length); }
                    if (relativepath.StartsWith("/") || relativepath.StartsWith("\\")) { relativepath = "." + relativepath; }
                    using (Fact.Runtime.Process msbuildp = new Fact.Runtime.Process(MSBuild, extractDirectory, relativepath))
                    {
                        Fact.Runtime.Process.Result Result = msbuildp.Run(-1);
                        string[] outs = Result.StdOut.Split(new char[] { '\n', '\r' });
                        if (Result.ExitCode != 0)
                        {
                            Fact.Log.Error("MSBuild: ");
                            if (testResult)
                            {
                                testResultContain += "msbuild:\n" + Result.StdOut + "\n" + Result.StdErr;
                                testResultFail = true;
                            }
                            foreach (string str in outs) { if (str.Length > 0) { Fact.Log.Error("  " + str); } }
                            string[] errors = Result.StdErr.Split(new char[] { '\n', '\r' });
                            foreach (string error in errors) { if (error.Length > 0) { Fact.Log.Error("  " + error); } }
                            failCount++;
                        }
                        else if (verbose)
                        {
                            Fact.Log.Verbose("MSBuild: ");
                            foreach (string str in outs) { if (str.Length > 0) { Fact.Log.Verbose("  " + str); } }
                        }
                    }
                }
                if (failCount >= CSProjs.Count)
                {
                    Fact.Log.Error("All builds have failed.");
                    if (mustRemove)
                        DeleteDirectory(extractDirectory);
                    SaveProjectWithErrorTag(input, output, message, testResultContain);
                    return 1;
                }
                if (failCount > 0)
                {
                    Fact.Log.Warning("Some builds have failed.");
                }
                goto GenerateOutput;
            }

        FindCmake:
            // Try to find CMakeLists.txt
            bool IgnoreCmake = false;
            List<string> CMakeLists = FindFile("CMakeLists.txt", extractDirectory);
            if (CMakeLists.Count != 0 && Makefiles.Count != 0)
            {
                Fact.Log.Warning("CMakeList.txt and Makefile has been found.");
                // Check if the CMake is in the root directory
                try
                {
                    bool CMakeInRootDir = false;
                    if (CMakeLists.Count == 1)
                    {
                        string singlecmake = CMakeLists[0].Substring(extractDirectory.Length);
                        while (singlecmake.StartsWith("/") ||
                               singlecmake.StartsWith("\\"))
                        {
                            singlecmake = singlecmake.Substring(1);
                        }
                        if (singlecmake.ToLower() == "cmakelist.txt" ||
                            singlecmake.ToLower() == "cmakelists.txt")
                        {
                            // Yes the camke is in root dir
                            CMakeInRootDir = true;
                        }
                    }
                    if (CMakeInRootDir)
                    {
                        if (Makefiles.Count == 1)
                        {
                            string singlemakefile = Makefiles[0].Substring(extractDirectory.Length);
                            while (singlemakefile.StartsWith("/") ||
                              singlemakefile.StartsWith("\\"))
                            {
                                singlemakefile = singlemakefile.Substring(1);
                            }
                            if (singlemakefile.ToLower() == "makefile" ||
                                singlemakefile.ToLower() == "makefile")
                            {
                                // Makefile is in rootdir too we cannot use the workarround
                                CMakeInRootDir = false;
                            }
                        }
                    }
                    if (CMakeInRootDir)
                    {
                        Fact.Log.Warning("However since the CMakeLists is in root dir and not the Makefiles");
                        Fact.Log.Warning("CMake will have the priority over the makefiles");
                        Makefiles = new List<string>();
                        IgnoreCmake = false;
                    }
                }
                catch
                {
                }
                if (Makefiles.Count > 0)
                {
                    Fact.Log.Warning("Only the makefile will be used.");
                    IgnoreCmake = true;
                }
            }
        RunCmake:
            if (CMakeLists.Count != 0 && !IgnoreCmake)
            {
                noMakeFile = false;
                int failCount = 0;
                foreach (string makefile in CMakeLists)
                {
                    try
                    {
                        message += "CMake: " + makefile.Substring(extractDirectory.Length);
                        Fact.Log.Verbose("CMake: " + makefile.Substring(extractDirectory.Length));
                        Fact.Runtime.Process.Result Result = null;
                        {
                            using (Fact.Runtime.Process MakeProcess = new Fact.Runtime.Process(CMake, System.IO.Path.GetDirectoryName(makefile), ". -G\"Unix Makefiles\""))
                            {
                                Result = MakeProcess.Run(timeout);
                            }
                        }

                        if (Result.ExitCode != 0)
                        {
                            testResultContain = "CMake:\n" + Result.StdErr;
                            string[] errors = Result.StdErr.Split(new char[] { '\n', '\r' });
                            Fact.Log.Error("CMake:");
                            foreach (string error in errors) { if (error.Length > 0) { Fact.Log.Error("  " + error); } }
                            failCount++;
                        }

                        {
                            string command = "";
                            foreach (string rule in rules) { command += " '" + rule + "' "; }
                            using (Fact.Runtime.Process MakeProcess = new Fact.Runtime.Process(Make, System.IO.Path.GetDirectoryName(makefile), command))
                            {
                                Result = MakeProcess.Run(-1);
                            }
                        }
                        if (verbose)
                        {
                            Fact.Log.Verbose("CMake: " + Result.StdOut);
                        }

                        if (Result.ExitCode != 0)
                        {
                            if (testResult)
                            {
                                testResultContain = "CMake:\n" + Result.StdErr;
                                testResultFail = true;
                            }
                            string[] errors = Result.StdErr.Split(new char[] { '\n', '\r' });
                            Fact.Log.Error("CMake (Unix Makefile):");
                            foreach (string error in errors) { if (error.Length > 0) { Fact.Log.Error("  " + error); } }
                            failCount++;
                        }
                    }
                    catch { }

                }
                if (failCount >= CMakeLists.Count)
                {
                    Fact.Log.Error("All CMakeLists have failed.");
                    if (mustRemove)
                        DeleteDirectory(extractDirectory);
                    SaveProjectWithErrorTag(input, output, message, testResultContain);
                    return 1;
                }
                if (failCount > 0)
                {
                    Fact.Log.Warning("Some CMakeLists have failed.");
                }
            }



            // Find the makefiles
            if (Makefiles.Count == 0 && noMakeFile)
            {
                Fact.Log.Error("No Makefile found in the specified package.");
                if (mustRemove)
                    DeleteDirectory(extractDirectory);
                message += "Makefile: No Makefile found in the specified package\n";
                if (testResult)
                {
                    SaveProjectWithErrorTag(input, output, message, testResultContain);
                }
                return 1;
            }
            if (Makefiles.Count > 0)
            {
                int failCount = 0;

                foreach (string makefile in Makefiles)
                {
                    string wrkdir = System.IO.Path.GetDirectoryName(new System.IO.FileInfo(makefile).FullName);
                    try
                    {
                        string command = "";
                        foreach (string rule in rules) { command += " '" + rule + "' "; }
                        Fact.Log.Verbose("Make : " + makefile.Substring(extractDirectory.Length));
                        Fact.Runtime.Process.Result Result = null;
                        using (Fact.Runtime.Process MakeProcess = new Fact.Runtime.Process(Make, wrkdir, command))
                        {
                            foreach (KeyValuePair<string, string> a_hook in hooklst)
                            {
                                MakeProcess.AddHook(a_hook.Key, a_hook.Value);
                            }
                            Result = MakeProcess.Run(timeout);
                        }
                        if (Result.ExitCode != 0 && permissive)
                        {

                            try
                            {

                                Fact.Log.Verbose("Make -i : " + makefile.Substring(extractDirectory.Length));
                                Fact.Runtime.Process.Result ResultI = null;
                                foreach (string rule in rules)
                                {
                                    using (Fact.Runtime.Process MakeIProcess = new Fact.Runtime.Process(Make, System.IO.Path.GetDirectoryName(makefile), "-i " + rule))
                                    {
                                        foreach (KeyValuePair<string, string> a_hook in hooklst)
                                        {
                                            MakeIProcess.AddHook(a_hook.Key, a_hook.Value);
                                        }
                                        ResultI = MakeIProcess.Run(timeout);
                                    }
                                    string[] errors = ResultI.StdErr.Split(new char[] { '\n', '\r' });
                                    if (testResult)
                                    {
                                        testResultContain += "Make:\n" + ResultI.StdErr;
                                        testResultFail = true;
                                    }
                                    Fact.Log.Error("Make (" + rule + "):" );
                                    foreach (string error in errors) { if (error.Length > 0) { Fact.Log.Error("  " + error); } }
                                    failCount++;
                                }
                            }
                            catch
                            { }
                        }
                        else
                        {
                            if (verbose)
                            {
                                Fact.Log.Verbose("Make: " + Result.StdOut);
                            }
                            if (Result.ExitCode != 0)
                            {
                                string[] errors = Result.StdErr.Split(new char[] { '\n', '\r' });
                                Fact.Log.Error("Make:");
                                foreach (string error in errors)
                                {
                                    testResultContain += "make:\n" + error;
                                    if (error.Length > 0) { Fact.Log.Error("  " + error); }

                                }
                                failCount++;
                                testResultFail = true;
                            }
                        }
                    }
                    catch { }
                }


                if (failCount >= Makefiles.Count && IgnoreCmake)
                {
                    failCount = 0;
                    IgnoreCmake = false;
                    Fact.Log.Error("Retry without ignoring CMakeLists.");
                    goto RunCmake;
                }

                if (failCount >= Makefiles.Count)
                {
                    Fact.Log.Error("All Makefiles have failed.");

                    if (!permissive)
                    {
                        Fact.Processing.Project sourceProject = Fact.Processing.Project.Load(input);
                        if (testResultFail)
                        {
                            sourceProject.AddTestResult(new Fact.Test.Result.Error("Makefile", (message + "\n" + testResultContain).Trim()));
                        }
                        sourceProject.Save(output);
                        return 1;
                    }
                    errorCode = 1;
                }
                if (failCount > 0)
                {
                    Fact.Log.Warning("Some Makefiles have failed.");
                }
            }
        GenerateOutput:
            {
                Fact.Log.Verbose("Generate the output package ...");
                Fact.Processing.Project destinationProject = Fact.Processing.Project.Load(input);
                destinationProject.AddRealDirectory("", extractDirectory, true);
                if (output == "")
                {
                    Fact.Log.Error("Destination package has not been specified");
                }
                else
                {
                    if (testResult)
                    {
                        try
                        { testResultContain = testResultContain.Replace(extractDirectory, "/"); }
                        catch { }

                        if (!testResultFail)
                            destinationProject.AddTestResult(new Fact.Test.Result.Passed("Makefile", (message + "\n" + "OK").Trim()));
                        else
                            destinationProject.AddTestResult(new Fact.Test.Result.Error("Makefile", (message + "\n" + testResultContain).Trim()));
                    }
                    destinationProject.Save(output);
                }

                if (mustRemove)
                    DeleteDirectory(extractDirectory);
                return errorCode;
            }
        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace List
{
    class List : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "List";
            }
        }

        void PrintType(Fact.Processing.File.FileType Type)
        {
            string typename = Type.ToString();
            Console.Write(typename);
            int length = typename.Length;
            while (length < 20) { Console.Write(" "); length++; }
        }

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("input", "The input package", ""); CommandLine.AddShortInput("i", "input");
            CommandLine.AddBooleanInput("help", "Display the help message", false); CommandLine.AddShortInput("h", "help");

            if (!CommandLine.Parse(args))
            {
                CommandLine.PrintErrors();
                return 1;
            }
            if (CommandLine["help"].AsBoolean())
            {
                CommandLine.PrintUsage();
                return 0;
            }
            string input = CommandLine["input"];
            if (!System.IO.File.Exists(input))
            {
                Fact.Log.Error("The file '" + input + "' does not exist");
                return 1;
            }

            try
            {
                Fact.Processing.Project Project = Fact.Processing.Project.Load(input);
                foreach (Fact.Processing.File file in Project)
                {
                    if (file.Binary) { Console.Write("Binary  "); }
                    else { Console.Write("Text    "); }
                    PrintType(file.Type);
                    if (file.Directory != "") { Console.WriteLine("/" + file.Directory + "/" + file.Name); }
                    else { Console.WriteLine(file.Directory + "/" + file.Name); }                  
                }
                return 0;

            }
            catch
            {
                Fact.Log.Error("Error while reading the package '" + input + "'");
                return 1;
            }
        }
    }
}

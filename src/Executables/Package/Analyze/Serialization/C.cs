﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyze.Serialization
{
    class C
    {
        public static Fact.Code.Element SerializeVariablesDeclaration(Language.C.VariableDeclaration Decl)
        {
            Fact.Code.VariableDeclaration FactDecl = new Fact.Code.VariableDeclaration();
            FactDecl.Name = Decl.Name;
            FactDecl.Type = Decl.Type;
            return FactDecl;
        }

        public static Fact.Code.Element SerializeStruct(Language.C.Struct Struct)
        {
            Fact.Code.Struct FactStruct = new Fact.Code.Struct(Struct.Name);
            foreach (Language.C.Code item in Struct.Elements)
            {
                FactStruct.Private.AddRange(Serialize(item));
            }
            return FactStruct;
        }

        public static Fact.Code.Element SerializeFunction(Language.C.Function Function)
        {
            Fact.Code.Function FactFunction = new Fact.Code.Function();
            FactFunction.Name = Function.Name;
            FactFunction.Body.AddRange(Serialize(Function.Body));
            return FactFunction;
        }

        public static List<Fact.Code.Element> Serialize(Language.C.Code Code)
        {
            List<Fact.Code.Element> elements = new List<Fact.Code.Element>();
            foreach (Language.C.Struct Struct in Code.Structs)
            {
                elements.Add(SerializeStruct(Struct));
            }
            foreach (Language.C.VariableDeclaration Variables in Code.VariableDeclarations)
            {
                elements.Add(SerializeVariablesDeclaration(Variables));
            }
            foreach (Language.C.Function Function in Code.Functions)
            {
                elements.Add(SerializeFunction(Function));
            }
            return elements;
        }
    }
}

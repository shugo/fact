﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyze.Serialization
{
    class CXX
    {
        public static Fact.Code.Element SerializeVariablesDeclaration(Language.CXX.VariableDeclaration Decl)
        {
            Fact.Code.VariableDeclaration FactDecl = new Fact.Code.VariableDeclaration();
            FactDecl.Name = Decl.Name;
            FactDecl.Type = Decl.Type;
            return FactDecl;
        }

        public static Fact.Code.Element SerializeStruct(Language.CXX.Struct Struct)
        {
            Fact.Code.Struct FactStruct = new Fact.Code.Struct(Struct.Name);
            foreach (Language.CXX.Code item in Struct.Private)
            {
                FactStruct.Private.AddRange(Serialize(item));
            }
            foreach (Language.CXX.Code item in Struct.Protected)
            {
                FactStruct.Protected.AddRange(Serialize(item));
            }
            foreach (Language.CXX.Code item in Struct.Public)
            {
                FactStruct.Public.AddRange(Serialize(item));
            }
            return FactStruct;
        }

        public static Fact.Code.Element SerializeFunction(Language.CXX.Function Function)
        {
            Fact.Code.Function FactFunction = new Fact.Code.Function();
            FactFunction.Name = Function.Name;
            FactFunction.Body.AddRange(Serialize(Function.Body));
            return FactFunction;
        }

        public static Fact.Code.Element SerializeClass(Language.CXX.Class Class)
        {
            Fact.Code.Class FactClass = new Fact.Code.Class(Class.Name);
            foreach (Language.CXX.Code item in Class.Private)
            {
                FactClass.Private.AddRange(Serialize(item));
            }
            foreach (Language.CXX.Code item in Class.Protected)
            {
                FactClass.Protected.AddRange(Serialize(item));
            }
            foreach (Language.CXX.Code item in Class.Public)
            {
                FactClass.Public.AddRange(Serialize(item));
            }
            return FactClass;
        }

        public static List<Fact.Code.Element> Serialize(Language.CXX.Code Code)
        {
            List<Fact.Code.Element> elements = new List<Fact.Code.Element>();
            foreach (Language.CXX.Class Class in Code.Classes)
            {
                elements.Add(SerializeClass(Class));
            }
            foreach (Language.CXX.Struct Struct in Code.Structs)
            {
                elements.Add(SerializeStruct(Struct));
            }
            foreach (Language.CXX.VariableDeclaration Variables in Code.VariableDeclarations)
            {
                elements.Add(SerializeVariablesDeclaration(Variables));
            }
            foreach (Language.CXX.Function Function in Code.Functions)
            {
                elements.Add(SerializeFunction(Function));
            }
            return elements;
        }
    }
}

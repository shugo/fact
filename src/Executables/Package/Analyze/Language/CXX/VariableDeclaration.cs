﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fact.Character;
using Fact.Processing;
using Fact.Processing.Lexer;
using Fact.Processing.Parser;

namespace Analyze.Language.CXX
{
    class VariableDeclaration
    {
        string _Name = "";
        public string Name { get { return _Name; } }
        string _Type = "";
        public string Type { get { return _Type; } }
        int _Line = -1;
        public int Line { get { return _Line; } }
        public VariableDeclaration(Lexer Type, Lexer Name)
        {
            _Name = Name.ToString().Trim();
            _Type = Type.ToString().Trim();
            Fact.Character.MetaToken TokenType = Type.GetToken(0);
            Fact.Character.MetaToken TokenName = Name.GetToken(0);
            if (!TokenType.Equals(null) && TokenType.Length > 0)
            { _Line = (int)TokenType[0].Line; }
            else if (!TokenName.Equals(null) && TokenName.Length > 0)
            { _Line = (int)TokenName[0].Line; }
        }
    }
}
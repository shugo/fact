﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fact.Character;
using Fact.Processing;
using Fact.Processing.Lexer;
using Fact.Processing.Parser;

namespace Analyze.Language.CXX
{
    class Class
    {
        string _Name = "";
        public string Name { get { return _Name; } }
        List<Code> _Public = new List<Code>();
        public List<Code> Public { get { return _Public; } }
        List<Code> _Private = new List<Code>();
        public List<Code> Private { get { return _Private; } }
        List<Code> _Protected = new List<Code>();
        public List<Code> Protected { get { return _Protected; } }


        public Class(Lexer Name, Lexer Body)
        {
            _Name = Name.ToString();
            Parser neastedclass = new Parser();
            neastedclass.AddRule((Lexer[] Captures) => { }, "class", Parser.ParseElement.Star, Parser.ParseElement.Braces("{", "}"));
            neastedclass.AddRule((Lexer[] Captures) => { }, "struct", Parser.ParseElement.Star, Parser.ParseElement.Braces("{", "}"));
            neastedclass.AddRule((Lexer[] Captures) => { }, "union", Parser.ParseElement.Star, Parser.ParseElement.Braces("{", "}"));
            neastedclass.AddRule((Lexer[] Captures) => { }, "enum", Parser.ParseElement.Star, Parser.ParseElement.Braces("{", "}"));
            neastedclass.AddRule((Lexer[] Captures) => { }, Parser.ParseElement.Braces("(", ")"));
            neastedclass.AddRule((Lexer[] Captures) => { }, "struct", Parser.ParseElement.Star, ";");
            neastedclass.AddRule((Lexer[] Captures) => { }, "enum", Parser.ParseElement.Star, ";");
            neastedclass.AddRule((Lexer[] Captures) => { }, "union", Parser.ParseElement.Star, ";");
            neastedclass.AddRule((Lexer[] Captures) => { }, "class", Parser.ParseElement.Star, ";");



            neastedclass.AddRule((Lexer[] Captures) => { }, Parser.ParseElement.GreadyStarExcept("public", "private", "protected", "class", "struct", "union", "enum", "("));

            Parser parser = new Parser();
            parser.AddRule((Lexer[] Captures) => { Code b = new Code(); b.ParseCore(Captures[0]); _Public.Add(b); },
                "public", ":", Parser.ParseElement.List(neastedclass));
            parser.AddRule((Lexer[] Captures) => { Code b = new Code(); b.ParseCore(Captures[0]); _Private.Add(b); },
                "private", ":", Parser.ParseElement.List(neastedclass));
            parser.AddRule((Lexer[] Captures) => { Code b = new Code(); b.ParseCore(Captures[0]); _Protected.Add(b); },
                "protected", ":", Parser.ParseElement.List(neastedclass));
            parser.AddRule((Lexer[] Captures) => { Code b = new Code(); b.ParseCore(Captures[0]); _Private.Add(b); },
                 Parser.ParseElement.List(neastedclass));
            parser.Parse(Body);
        }
    }
}
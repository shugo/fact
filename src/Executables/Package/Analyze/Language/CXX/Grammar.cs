﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fact.Character;
using Fact.Processing;
using Fact.Processing.Lexer;
using Fact.Processing.Parser;

namespace Analyze.Language.CXX
{
    static class Grammar
    {
        public static Parser dcl_specifier = new Parser();

        public static Parser storage_class_specifier = new Parser();
        public static Parser fct_specifier = new Parser();

        public static Parser complete_type_specifier = new Parser();
        public static Parser type_specifier = new Parser();
        public static Parser type_modifier = new Parser();
        public static Parser type_prefix = new Parser();


        public static Parser simple_type_name = new Parser();
        public static Parser elaborated_type_specifier = new Parser();

        public static Parser class_key = new Parser();
        public static Parser qualified_type_name = new Parser();

        public static Parser complete_class_name = new Parser();
        public static Parser qualified_class_name = new Parser();
        public static Parser decltype = new Parser();


        public static Parser enum_specifier = new Parser();

        static void InitGrammarOfDeclarations()
        {
            dcl_specifier.AddRule(storage_class_specifier);
            dcl_specifier.AddRule(type_specifier);
            dcl_specifier.AddRule(fct_specifier);
            dcl_specifier.AddRule("friend");
            dcl_specifier.AddRule("typedef");
            dcl_specifier.AddRule("__declspec", Parser.ParseElement.Braces("(", ")"));

            complete_type_specifier.AddRule(type_specifier);
            complete_type_specifier.AddRule(Parser.ParseElement.List(type_prefix), type_specifier);
            complete_type_specifier.AddRule(type_specifier, Parser.ParseElement.List(type_modifier));
            complete_type_specifier.AddRule(Parser.ParseElement.List(type_prefix), type_specifier, Parser.ParseElement.List(type_modifier));

            complete_type_specifier.BiggestMatch = true;

            type_specifier.AddRule(simple_type_name);
            type_specifier.AddRule(class_specifier);
            type_specifier.AddRule(enum_specifier);
            type_specifier.AddRule(elaborated_type_specifier);

            type_prefix.AddRule("unsigned");
            type_prefix.AddRule("const");
            type_prefix.AddRule("volatile");

            //type_specifier.AddRule("const");
            //type_specifier.AddRule("volatile");

            type_modifier.AddRule("*");
            type_modifier.AddRule("&");
            type_modifier.AddRule("const");


            storage_class_specifier.AddRule("auto");
            storage_class_specifier.AddRule("register");
            storage_class_specifier.AddRule("static");
            storage_class_specifier.AddRule("extern");

            fct_specifier.AddRule("inline");
            fct_specifier.AddRule("virtual");

            simple_type_name.AddRule(complete_class_name);
            simple_type_name.AddRule(qualified_type_name);
            simple_type_name.AddRule("char");
            simple_type_name.AddRule("short");
            simple_type_name.AddRule("int");
            simple_type_name.AddRule("long");
            simple_type_name.AddRule("signed");
            simple_type_name.AddRule("unsigned");
            simple_type_name.AddRule("float");
            simple_type_name.AddRule("double");
            simple_type_name.AddRule("void");

            decltype.AddRule("decltype", Parser.ParseElement.Braces("(", ")"));

            elaborated_type_specifier.AddRule(class_key, Parser.ParseElement.Any);
            //elaborated_type_specifier.AddRule(class_key, class_name); <- class_name is an identifier
            //elaborated_type_specifier.AddRule(enum_name); <- enum_name is an identifier

            class_key.AddRule("class");
            class_key.AddRule("struct");
            class_key.AddRule("union");

            // qualified_type_name.AddRule(typedef_name);
            qualified_type_name.AddRule(decltype);
            qualified_type_name.AddRule(Parser.ParseElement.Identifier, "::", qualified_type_name);

            complete_class_name.AddRule("::", qualified_class_name);
            complete_class_name.AddRule(qualified_class_name);

            qualified_class_name.AddRule(Parser.ParseElement.Identifier);
            qualified_class_name.AddRule(Parser.ParseElement.Identifier, "::", qualified_class_name);
            qualified_class_name.BiggestMatch = true;

            enum_specifier.AddRule("enum");
            enum_specifier.AddRule("enum", Parser.ParseElement.Braces("{", "}"));
            enum_specifier.AddRule("enum", Parser.ParseElement.Identifier);
            enum_specifier.AddRule("enum", Parser.ParseElement.Identifier, Parser.ParseElement.Braces("{", "}"));
            enum_specifier.BiggestMatch = true;
        }

        public static Parser class_specifier = new Parser();
        public static Parser class_head = new Parser();


        static void InitGrammarOfClasses()
        {
            class_specifier.AddRule(class_head, Parser.ParseElement.Braces("{", "}"));
            class_specifier.AddRule(class_head);
            class_specifier.BiggestMatch = true;
            class_head.AddRule(class_key, Parser.ParseElement.Any, Parser.ParseElement.Any, Parser.ParseElement.Any);
        }

        static Grammar()
        {
            InitGrammarOfDeclarations();
            InitGrammarOfClasses();
        }
    }
}
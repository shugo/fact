﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fact.Character;
using Fact.Processing;
using Fact.Processing.Lexer;
using Fact.Processing.Parser;

namespace Analyze.Language.CXX
{
    class Function
    {   
        string _Name = "";
        public string Name { get { return _Name; } }
        string _Prototype = "";
        Code _Body = null;
        public Code Body { get { return _Body; } }
        public Function(Lexer Name, Lexer Prototype, Lexer Body)
        {
            _Name = Name.ToString().Trim();
            _Prototype = Prototype.ToString();
            _Body = new Code();
            _Body.ParseFunction(Body);
        }
    }
}
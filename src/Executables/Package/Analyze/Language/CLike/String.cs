﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyze.Language.CLike
{
    static public class String
    {
        public static void Mark_String_And_Comment(List<Fact.Character.MetaChar> Text)
        {
            bool string_on = false;
            bool multiline_comment_on = false;
            bool singleline_comment_on = false;
            bool esc_char_on = false;
            string string_char = "'";

            for (int c = 0; c < Text.Count; c++)
            {
                if (singleline_comment_on)
                {
                    if (Text[c].Type == Fact.Character.MetaChar.BasicCharType.EndOfLine ||
                        Text[c] == "\n" ||
                        Text[c] == "\r" ||
                        Text[c] == "\r\n" ||
                        Text[c] == "\n\r")
                    {
                        singleline_comment_on = false;
                    }
                    else
                    {
                        Text[c].Type = Fact.Character.MetaChar.BasicCharType.Comment;
                    }
                }
                else if (multiline_comment_on)
                {
                    Text[c].Type = Fact.Character.MetaChar.BasicCharType.Comment;
                    if (c < Text.Count - 1 && Text[c] == "*" && Text[c + 1] == "/")
                    {
                        Text[c + 1].Type = Fact.Character.MetaChar.BasicCharType.Comment;
                        multiline_comment_on = false;
                    }
                }
                else if (string_on)
                {
                    Text[c].Type = Fact.Character.MetaChar.BasicCharType.String;
                    if (esc_char_on)
                    {
                        esc_char_on = false;
                    }
                    else
                    {
                        if (Text[c] == string_char) { string_on = false; }
                        else if (Text[c] == "\\") { esc_char_on = true; }
                    }
                }
                else
                {
                    if (c < Text.Count - 1 && Text[c] == "/" && Text[c + 1] == "*")
                    {
                        Text[c].Type = Fact.Character.MetaChar.BasicCharType.Comment;
                        Text[c + 1].Type = Fact.Character.MetaChar.BasicCharType.Comment;
                        c++;
                        multiline_comment_on = true;
                    }
                    else if (c < Text.Count - 1 && Text[c] == "/" && Text[c + 1] == "/")
                    {
                        Text[c].Type = Fact.Character.MetaChar.BasicCharType.Comment;
                        Text[c + 1].Type = Fact.Character.MetaChar.BasicCharType.Comment;
                        c++;
                        singleline_comment_on = true;
                    }
                    if (Text[c] == "\"" || Text[c] == "\'")
                    {
                        string_char = Text[c];
                        Text[c].Type = Fact.Character.MetaChar.BasicCharType.String;
                        string_on = true; 
                    }
                }
            }
        }
    }
}

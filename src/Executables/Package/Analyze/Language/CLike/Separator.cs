﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyze.Language.CLike
{
    static public class Separator
    {
        public static void Mark_Separators(List<Fact.Character.MetaChar> Text)
        {
            for (int n = 0; n < Text.Count; n++)
            {
                if (Text[n].Type != Fact.Character.MetaChar.BasicCharType.Comment &&
                    Text[n].Type != Fact.Character.MetaChar.BasicCharType.Preprocessor &&
                    Text[n].Type != Fact.Character.MetaChar.BasicCharType.String)
                {
                    if (Text[n] == ">")
                    {
                        if (n > 0 && Text[n - 1] == "-")
                        {
                            if (n > 1 && Text[n - 2] == "-")
                            {
                                Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Single;
                                Text[n - 1].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Implicit;
                                Text[n - 2].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.None;
                                if (n > 2 && Text[n - 3].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.None)
                                {
                                    Text[n - 3].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Implicit;
                                }
                            }
                            else
                            {
                                Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Implicit;
                                Text[n - 1].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.None;
                                if (n > 1 && Text[n - 2].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.None)
                                {
                                    Text[n - 2].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Implicit;
                                }
                            }
                        }
                        else
                        {
                            Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Single;
                        }
                    }
                    else if (n < Text.Count - 1 &&
                        (Text[n] == "-" ||
                         Text[n] == "+" ||
                         Text[n] == "=" ||
                         Text[n] == "&" ||
                         Text[n] == "|" ||
                         Text[n] == "<" ||
                         Text[n] == ">" ||
                         Text[n] == ":"))
                    {
                        if (Text[n + 1] == Text[n].ToString())
                        {
                            Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.None;
                            Text[n + 1].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Implicit;
                            if (n > 0 && Text[n - 1].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.None)
                            {
                                Text[n - 1].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Implicit;
                            }
                            n++;
                        }
                        else
                        {
                            Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Single;
                        }
                    }
                    else if (Text[n] == "," || Text[n] == ";" ||
                                Text[n] == "/" || Text[n] == "*" || Text[n] == "%" ||
                                Text[n] == "!" || Text[n] == "~" ||
                                Text[n] == "<" || Text[n] == ">" ||
                                Text[n] == ":" || Text[n] == "?" ||
                                Text[n] == "(" || Text[n] == ")" || Text[n] == "[" || Text[n] == "]" || Text[n] == "{" || Text[n] == "}")
                    {
                            Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Single;
                    }
                    else if (Text[n] == " " || Text[n] == "\t")
                    {
                        Text[n].Type = Fact.Character.MetaChar.BasicCharType.Space;
                        Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Separator;
                    }
                    else if (Text[n] == "\n" || Text[n] == "\r")
                    {
                        Text[n].Type = Fact.Character.MetaChar.BasicCharType.EndOfLine;
                        Text[n].SeparatorType = Fact.Character.MetaChar.BasicSeparatorType.Separator;
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyze.Language.CLike
{
    static public class Preprocessor
    {
        public static void Mark_Preporcessor(List<Fact.Character.MetaChar> Text)
        {
            bool newline_on = true;
            bool preprocessor_on = false;
            bool esc_on = false;
            for (int n = 0; n < Text.Count; n++) 
            {
                if (preprocessor_on)
                {
                    if (esc_on)
                    {
                        Text[n].Type = Fact.Character.MetaChar.BasicCharType.Preprocessor;
                        if (Text[n] == "\\") { esc_on = true; }
                        esc_on = false;
                    }
                    else
                    {
                        if (Text[n] == "\n" || Text[n] == "\r") { preprocessor_on = false; }
                        else
                        {
                            Text[n].Type = Fact.Character.MetaChar.BasicCharType.Preprocessor;
                            if (Text[n] == "\\") { esc_on = true; }

                        }
                    }
                }
                else if (newline_on)
                {
                    if (Text[n] == "#") 
                    {
                        Text[n].Type = Fact.Character.MetaChar.BasicCharType.Preprocessor;
                        preprocessor_on = true;
                    }
                    else if (Text[n] != " " && Text[n] != "\t" && Text[n] != "\n" && Text[n] != "\r")
                    {
                        newline_on = false;
                    }
                }
                else
                {
                    if (Text[n] == "\n" || Text[n] == "\r")
                    {
                        newline_on = true;
                    }
                }
            }
        }
    }
}
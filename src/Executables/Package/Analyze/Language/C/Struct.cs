﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fact.Character;
using Fact.Processing;
using Fact.Processing.Lexer;
using Fact.Processing.Parser;

namespace Analyze.Language.C
{
    class Struct
    {
        string _Name = "";
        public string Name { get { return _Name; } }
        List<Code> _Elements = new List<Code>();
        public List<Code> Elements { get { return _Elements; } }

        public Struct(Lexer Name, Lexer Body)
        {
            _Name = Name.ToString();
            Parser neastedelements = new Parser();
            neastedelements.AddRule((Lexer[] Captures) => { }, "struct", Parser.ParseElement.Star, Parser.ParseElement.Braces("{", "}"));
            neastedelements.AddRule((Lexer[] Captures) => { }, "union", Parser.ParseElement.Star, Parser.ParseElement.Braces("{", "}"));
            neastedelements.AddRule((Lexer[] Captures) => { }, "enum", Parser.ParseElement.Star, Parser.ParseElement.Braces("{", "}"));
            neastedelements.AddRule((Lexer[] Captures) => { }, Parser.ParseElement.Braces("(", ")"));
            neastedelements.AddRule((Lexer[] Captures) => { }, "struct", Parser.ParseElement.Star, ";");
            neastedelements.AddRule((Lexer[] Captures) => { }, "enum", Parser.ParseElement.Star, ";");
            neastedelements.AddRule((Lexer[] Captures) => { }, "union", Parser.ParseElement.Star, ";");

            neastedelements.AddRule((Lexer[] Captures) => { }, Parser.ParseElement.GreadyStarExcept("struct", "union", "enum"));

            Parser parser = new Parser();
            parser.AddRule((Lexer[] Captures) => { Code b = new Code(); b.ParseCore(Captures[0]); _Elements.Add(b); },
                 Parser.ParseElement.List(neastedelements));
            parser.Parse(Body);
        }
    }
}

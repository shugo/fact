﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyze.Language.C
{
    static public class Keyword
    {
        public static void Mark_Keywords(Fact.Character.MetaToken Token)
        {
            if (Token == "if" || Token == "else" || Token == "goto" ||
                Token == "switch" || Token == "case" ||
                Token == "break" || Token == "continue" || Token == "default" ||
                Token == "do" || Token == "while" ||
                Token == "for" ||
                Token == "struct" || Token == "enum" || Token == "union" ||
                Token == "typedef" || Token == "return" || Token == "sizeof") 
            {
                Token.TokenType = Fact.Character.MetaChar.BasicTokenType.KeyWord; 
            }
            else if (Token == "int" || Token == "float" ||
                Token == "unsigned" || Token == "signed" ||
                Token == "short" || Token == "double" ||
                Token == "long" ||
                Token == "void" ||
                Token == "char")
            {
                Token.TokenType = Fact.Character.MetaChar.BasicTokenType.Type;
            }
        }
    }
}

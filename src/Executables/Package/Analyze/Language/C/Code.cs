﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fact.Character;
using Fact.Processing;
using Fact.Processing.Lexer;
using Fact.Processing.Parser;

namespace Analyze.Language.C
{
    class Code
    {
        List<Struct> _Structs = new List<Struct>();
        public List<Struct> Structs { get { return _Structs; } }

        List<Function> _Functions = new List<Function>();
        public List<Function> Functions { get { return _Functions; } }
        List<Function> _FunctionDeclarations = new List<Function>();
        List<VariableDeclaration> _VariableDeclarations = new List<VariableDeclaration>();
        public List<VariableDeclaration> VariableDeclarations { get { return _VariableDeclarations; } }
        List<KeyValuePair<string, string>> _TypeAliases = new List<KeyValuePair<string, string>>();
        public List<KeyValuePair<string, string>> TypeAliases { get { return _TypeAliases; } }

        Lexer _Lexer = null;
        public Lexer Lexer { get { return _Lexer; } }

        int _EffectiveLineCount = 0;
        public int EffectiveLineCount { get { return _EffectiveLineCount; } }
        public Code()
        {

        }

        void _Metrics(Lexer Lexer)
        {
            Lexer = Lexer.Clone();
            Lexer.IgnoreSpaces = true;
            Lexer.IgnoreNewLines = true;
            MetaToken token = Lexer.GetToken(0);
            uint prevline = 0;
            while ((object)token != null)
            {
                if (token.Length > 0 && token[0].Line != prevline)
                {
                    _EffectiveLineCount++;
                    prevline = token[0].Line;
                }
                token = Lexer.EatToken();
            }
        }

        public void ParseCore(Lexer Lexer)
        {
            _Lexer = Lexer;
            _Metrics(Lexer);
            Lexer.IgnoreSpaces = true;
            Lexer.IgnoreNewLines = true;
            Lexer.IgnorePreprocessor = true;
            Parser parser = new Parser();
            List<Lexer> structs = new List<Lexer>();
            List<Lexer> enums = new List<Lexer>();
            List<Lexer> unions = new List<Lexer>();

            // Subparsers
            Parser initializationblock = new Parser();
            initializationblock.AddRule(Parser.ParseElement.StarExcept(";", "{", "}"), Parser.ParseElement.Braces("{", "}"));
            initializationblock.AddRule(Parser.ParseElement.GreadyStarExcept(";"));




            //Template
            parser.AddRule("template", Parser.ParseElement.Braces("<", ">"));

            // struct union ...
            parser.AddRule((Lexer[] Captures) => { _Structs.Add(new Struct(Captures[0], Captures[1])); },
                "struct", Parser.ParseElement.Star,
                Parser.ParseElement.Braces("{", "}"));
            parser.AddRule((Lexer[] Captures) => { enums.Add(Captures[0]); },
                "enum", Parser.ParseElement.Star,
                Parser.ParseElement.Braces("{", "}"));
            parser.AddRule((Lexer[] Captures) => { unions.Add(Captures[0]); },
                "union", Parser.ParseElement.Star,
                Parser.ParseElement.Braces("{", "}"));

            // struct union declaration
            parser.AddRule((Lexer[] Captures) => { },
                "struct", Parser.ParseElement.StarExcept("{", "}"),
                ";");
            parser.AddRule((Lexer[] Captures) => { },
                "enum", Parser.ParseElement.StarExcept("{", "}"),
                ";");
            parser.AddRule((Lexer[] Captures) => { },
                "union", Parser.ParseElement.StarExcept("{", "}"),
                ";");

            // Function
            parser.AddRule((Lexer[] Captures) => { _Functions.Add(new Function(Captures[0], Captures[1], Captures[2])); },
                Parser.ParseElement.Identifier,
                Parser.ParseElement.Braces("(", ")"),
                Parser.ParseElement.Braces("{", "}"));

            //Function declaration
            parser.AddRule((Lexer[] Captures) => { },
                Parser.ParseElement.Identifier,
                Parser.ParseElement.Braces("(", ")"),
                ";");
            parser.AddRule((Lexer[] Captures) => { },
                Parser.ParseElement.Identifier,
                Parser.ParseElement.Braces("(", ")"),
                "=",
                "0",
                ";");

            // Typedef
            parser.AddRule((Lexer[] Captures) => { _TypeAliases.Add(new KeyValuePair<string, string>(Captures[0].ToString(), Captures[1].ToString())); },
                "typedef",
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier,
                ";");

            // Variable declarations
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier,
                "=",
                initializationblock,
                ";");
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier, "[", "]",
                "=",
                initializationblock,
                ";");
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier,
                ";");
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier, "[", "]",
                ";");


            // Error management
            parser.AddRule(Parser.ParseElement.Braces("(", ")"));
            parser.AddRule(Parser.ParseElement.Braces("[", "]"));
            parser.AddRule(Parser.ParseElement.Braces("{", "}"));

            parser.Parse(Lexer);
        }

        public void ParseFunction(Lexer Lexer)
        {
            _Metrics(Lexer);
            Lexer.IgnoreSpaces = true;
            Lexer.IgnoreNewLines = true;
            Lexer.IgnorePreprocessor = true;
            Parser parser = new Parser();

            // Subparsers
            Parser initializationblock = new Parser();
            initializationblock.AddRule(Parser.ParseElement.StarExcept(";", "{", "}"), Parser.ParseElement.Braces("{", "}"));
            initializationblock.AddRule(Parser.ParseElement.GreadyStarExcept(";"));

            // Variable declarations
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier,
                "=",
                initializationblock,
                ";");
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier, "[", "]",
                "=",
                initializationblock,
                ";");
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier,
                ";");
            parser.AddRule((Lexer[] Captures) => { _VariableDeclarations.Add(new VariableDeclaration(Captures[0], Captures[1])); },
                Grammar.complete_type_specifier,
                Parser.ParseElement.Identifier, "[", "]",
                ";");

            // Error management
            parser.AddRule(Parser.ParseElement.Braces("(", ")"));
            parser.AddRule(Parser.ParseElement.Braces("[", "]"));
            parser.AddRule(Parser.ParseElement.Braces("{", "}"));

            parser.Parse(Lexer);
        }
    }
}

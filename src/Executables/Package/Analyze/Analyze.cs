﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analyze
{
    public class Analyze : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Analyze";
            }
        }

        public override int Run(params string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.ProgramName = "analyze";
            CommandLine.AddStringInput("input", "input directory", ""); CommandLine.AddShortInput("i", "input");
            CommandLine.AddStringInput("output", "output fact package", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddBooleanInput("verbose", "display more information", false); CommandLine.AddShortInput("v", "vebose");

            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");  
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

            bool verbose = false;
            bool remove = false;
            if (CommandLine["input"].IsDefault) { Fact.Log.Error("The input package must be set"); return 1; }
            string input = CommandLine["input"].AsString();
            string output = "";
            if (CommandLine["output"].IsDefault) { output = CommandLine["input"].AsString(); }
            else { output = CommandLine["output"].AsString(); }

            Fact.Processing.Project Project = Fact.Processing.Project.Load(input);

            List<Fact.Processing.File.FileType> C_CXX = new List<Fact.Processing.File.FileType>();
            C_CXX.Add(Fact.Processing.File.FileType.CHeader);
            C_CXX.Add(Fact.Processing.File.FileType.CSource);
            C_CXX.Add(Fact.Processing.File.FileType.CPPHeader);
            C_CXX.Add(Fact.Processing.File.FileType.CPPSource);

            List<Fact.Processing.File.FileType> C = new List<Fact.Processing.File.FileType>();
            C.Add(Fact.Processing.File.FileType.CHeader);
            C.Add(Fact.Processing.File.FileType.CSource);

            List<Fact.Processing.File.FileType> CXX = new List<Fact.Processing.File.FileType>();
            CXX.Add(Fact.Processing.File.FileType.CPPHeader);
            CXX.Add(Fact.Processing.File.FileType.CPPSource);

            Fact.Processing.Chain LexerChain = new Fact.Processing.Chain(Project);
            LexerChain.Add((Fact.Processing.Step.Step_On_Text)Language.CLike.String.Mark_String_And_Comment);
            LexerChain.Add((Fact.Processing.Step.Step_On_Text)Language.CLike.Preprocessor.Mark_Preporcessor);
            LexerChain.Add((Fact.Processing.Step.Step_On_Text)Language.CLike.Separator.Mark_Separators);
            LexerChain.Process(C_CXX);
            Fact.Processing.Chain KeywordChainC = new Fact.Processing.Chain(Project);
            KeywordChainC.Add((Fact.Processing.Step.Step_On_Token)Language.C.Keyword.Mark_Keywords);
            KeywordChainC.Process(C);
            Fact.Processing.Chain KeywordChainCXX = new Fact.Processing.Chain(Project);
            KeywordChainCXX.Add((Fact.Processing.Step.Step_On_Token)Language.CXX.Keyword.Mark_Keywords);
            KeywordChainCXX.Process(CXX);

            foreach (Fact.Processing.File file in Project)
            {
                if (file.Type == Fact.Processing.File.FileType.CPPSource ||
                    file.Type == Fact.Processing.File.FileType.CPPHeader)
                {
                    Language.CXX.Code code = new Language.CXX.Code();
                    Fact.Processing.Lexer.Lexer lexer = new Fact.Processing.Lexer.Lexer(file);
                    code.ParseCore(lexer);
                    file.CodeElements.AddRange(Serialization.CXX.Serialize(code));
                }

                if (file.Type == Fact.Processing.File.FileType.CSource ||
                    file.Type == Fact.Processing.File.FileType.CHeader)
                {
                    Language.C.Code code = new Language.C.Code();
                    Fact.Processing.Lexer.Lexer lexer = new Fact.Processing.Lexer.Lexer(file);
                    code.ParseCore(lexer);
                    file.CodeElements.AddRange(Serialization.C.Serialize(code));
                }
            }

            Project.Save(output);


            return 0;
        }
    }
}

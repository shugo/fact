﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Create
{
    class Create : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Create";
            }
        }

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.ProgramName = "create";
            CommandLine.AddStringInput("input", "input directory", ""); CommandLine.AddShortInput("i", "input");
            CommandLine.AddStringInput("output", "output fact package", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddBooleanInput("zip", "zip the package", false); CommandLine.AddShortInput("z", "zip");
            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");  
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

            bool remove = false;
            if (CommandLine["input"].IsDefault) { Fact.Log.Error("The input directory must be set"); return 1; }
            string input = CommandLine["input"].AsString();
            string output = "";
            if (!CommandLine["output"].IsDefault) { output = CommandLine["output"].AsString(); }

			if (!System.IO.Directory.Exists(input)) 
            {
				if (input.StartsWith("git://") ||
				    input.StartsWith("git+ssh://") ||
				    input.EndsWith(".git"))
                {
                    
                    remove = true;
                    string temp = Fact.Internal.Information.GetTempFileName();
                    Fact.Tools.RecursiveMakeDirectory(temp);
                    

                    string git = Fact.Internal.Information.GetGitBinary();
					string repo = output;
                    for (int n = 1; n <= 10; n++)
                    {
                        Fact.Runtime.Process.Result Result = null;
                        using (Fact.Runtime.Process Process = new Fact.Runtime.Process(git, "", "clone " + input + " " + temp))
                        {
                            Result = Process.Run(-1);
                        }
                        if (Result.ExitCode != 0 && n >= 10)
                        {
                            Fact.Log.Error(Result.StdErr);
                            Fact.Tools.RecursiveDelete(temp);
                            Fact.Log.Error("Impossible to clone " + repo);
                            return 1;
                        }
                        else if (Result.ExitCode != 0)
                        {
                            Fact.Tools.RecursiveDelete(temp);
                            Fact.Tools.RecursiveMakeDirectory(temp);
                            Fact.Log.Warning(Result.StdErr);
                            Fact.Log.Warning("Impossible to clone " + repo);
                            Fact.Log.Warning("Try again ...");
                        }
                        else
                        {
                            break;
                        }
                    }
					input = temp;
                    Fact.Log.Verbose("Repository '" + repo + "' has been clonned.");
                }
				else if (input.StartsWith("svn://"))
                {
                    remove = true;
                    string temp = Fact.Internal.Information.GetTempFileName();
                    Fact.Tools.RecursiveMakeDirectory(temp);

                    string svn = Fact.Internal.Information.GetSVNBinary();
                    Fact.Runtime.Process Process;
                    if (System.IO.File.Exists(svn))
                    {
						Process = new Fact.Runtime.Process(svn, temp, "checkout " + input);
                    }
                    else
                    {
                        Fact.Log.Warning("SVN not found git-svn will be used");
                        string git = Fact.Internal.Information.GetGitBinary();
						Process = new Fact.Runtime.Process(git, temp, "svn clone " + input);
                    }
					input = temp;
                    Fact.Runtime.Process.Result Result = Process.Run(-1);
                    if (Result.ExitCode != 0)
                    {
                        Fact.Log.Error(Result.StdErr);
                        Fact.Tools.RecursiveDelete(temp);
                        return 1;
                    }
                }
                else
                {
                    Fact.Log.Error("Can't load '" + input + "'.");
                    return 1;
                }
            }

            if (output == "")
            {
                output = input;
                while (output.EndsWith("/") || output.EndsWith("\\")) { output = output.Substring(0, output.Length - 1); }
                output += ".ff";
            }

			Fact.Processing.Project Project = new Fact.Processing.Project(System.IO.Path.GetFileName(output));
			Fact.Log.Verbose("Create package '" + output + "' ...");
            Project.AddRealDirectory("", input);
            bool zip = CommandLine["zip"].AsBoolean();
            try
            {
                Project.Save(output, zip);
            }
            catch
            {
                Fact.Log.Error("Impossible to save the package '" + output + "'.");
            }
            if (remove)
            {
                Fact.Tools.RecursiveDelete(input);
            }
            return 0;


        }
    }
}

﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extract
{
    class Extract : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Extract";
            }
        }

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.ProgramName = "extract";
            CommandLine.AddStringInput("input", "input fact package", ""); CommandLine.AddShortInput("i", "input");
            CommandLine.AddStringInput("output", "output directory", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddStringInput("filter-in", "extract only the files or the directories that match the filter", "");
            CommandLine.AddStringInput("filter-out", "do not extract the files or the directories that match the filter", "");
            CommandLine.AddStringInput("format", "format the content of the file before saving", ""); CommandLine.AddShortInput("f", "format");
            CommandLine.AddBooleanInput("verbose", "display more information about the process", false); CommandLine.AddShortInput("v", "verbose");
            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");

            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

            bool verbose = CommandLine["verbose"].AsBoolean();
            string source = CommandLine["input"].AsString();
            string destination = CommandLine["output"].AsString();
            string filter_in = CommandLine["filter-in"].AsString();
            string filter_out = CommandLine["filter-out"].AsString();
            System.Text.RegularExpressions.Regex filter_in_regexp = null;
            System.Text.RegularExpressions.Regex filter_out_regexp = null;

            if (filter_in.Trim() != "")
            {
                try
                {
                    filter_in_regexp = new System.Text.RegularExpressions.Regex(filter_in);
                }
                catch
                { Fact.Log.Error("Invalid regex: " + filter_in); return 1; }
            }

            if (filter_out.Trim() != "")
            {
                try
                {
                    filter_out_regexp = new System.Text.RegularExpressions.Regex(filter_out);
                }
                catch
                { Fact.Log.Error("Invalid regex: " + filter_out); return 1; }
            }

            string subdir = "";

            if (source == "" || destination == "")
            {
                CommandLine.PrintUsage();
                return 1;
            }

            Fact.Tools.RecursiveMakeDirectory(destination);

            if (verbose) { Fact.Log.Verbose("Loading project ..."); }
            Fact.Processing.Project Project = Fact.Processing.Project.Load(source);

            if (verbose) { Fact.Log.Verbose("Extract " + Project.Name + " into " + destination + " ..."); }
            
            foreach (Fact.Processing.File file in Project.GetFiles(subdir))
            {
                string fullname = file.Directory + "/" + file.Name;
                if (filter_in_regexp != null && !filter_in_regexp.Match(fullname).Success)
                {
                    if (verbose) { Fact.Log.Verbose("Ignored " + file.Directory + "/" + file.Name); }
                    continue;
                }
                if (filter_out_regexp != null && filter_out_regexp.Match(fullname).Success)
                {
                    if (verbose) { Fact.Log.Verbose("Ignored " + file.Directory + "/" + file.Name); }
                    continue;
                }

                string directory = "";
                if (file.Directory.StartsWith("/")) { directory = destination + file.Directory; }
                else { directory = destination + "/" + file.Directory; }
                Fact.Tools.RecursiveMakeDirectory(directory);

                if (file.Directory.EndsWith("/")) { directory += file.Name; }
                else { directory += "/" + file.Name; }

                if (System.IO.File.Exists(directory)) { try { System.IO.File.Delete(directory); } catch { return 1; } }

                if (verbose) { Fact.Log.Verbose("Extract " + file.Directory + "/" + file.Name); }
                if (file.Binary) { System.IO.File.WriteAllBytes(directory, file.GetBytes()); }
                else { System.IO.File.WriteAllText(directory, file.GetText(CommandLine["format"].AsBoolean())); }
            }
            
            return 0;

        }
    }
}

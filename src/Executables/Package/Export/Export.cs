﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Export
{
    public class Print : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Export";
            }
        }

        Dictionary<string, Printer.Printer> _Printers = new Dictionary<string, Printer.Printer>();

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringListInput("input", "list of the input porojects", new List<string>()); CommandLine.AddShortInput("i", "input");
            CommandLine.AddStringInput("output", "The output file (stdout is used otherwise)", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddStringInput("format", "The output format", "fact-xml"); CommandLine.AddShortInput("o", "output");
            
            CommandLine.AddBooleanInput("help", "Display the help message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }
            _Printers.Add("fact-xml", new Printer.FactXml());
            _Printers.Add("fact-json", new Printer.FactJson());
            _Printers.Add("fact-xml-old", new Printer.FactOld());

            Options Options = new global::Export.Options();

            List<Fact.Processing.Project> Projects = new List<Fact.Processing.Project>();
            foreach (string project in CommandLine["input"].AsStringList())
            {
                if (System.IO.File.Exists(project))
                {
                    try
                    {
                        Fact.Processing.Project FactProject = Fact.Processing.Project.Load(project);
                        Projects.Add(FactProject);
                    }
                    catch { Fact.Log.Error("Impossible to load the project " + project); return 1; }
                }
                else
                {
                    Fact.Log.Error("The project " + project + "is not present");
                    return 1;
                }
            }
            
            if (!_Printers.ContainsKey(CommandLine["format"].AsString()))
            {
                Fact.Log.Error("Format not supported " + CommandLine["format"].AsString());
                return 1;
            }

            string result = _Printers[CommandLine["format"].AsString()].PrintProjects(Projects, Options);

            if (CommandLine["output"].AsString() == "")
            { Fact.Log.Terminal.WriteLine(result); }
            else
            {
                if (System.IO.File.Exists(CommandLine["output"].AsString()))
                {
                    Fact.Tools.RecursiveDelete(CommandLine["output"]);
                }
                System.IO.File.WriteAllText(CommandLine["output"].AsString(), result);
            }
            return 0;

        }

    }
}

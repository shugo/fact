﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Export.Printer
{
    abstract class Printer
    {
        protected string ConvertVisibilityToString(Fact.Test.Result.Result.VisibilityLevel Visibility)
        {
            switch (Visibility)
            {
                case Fact.Test.Result.Result.VisibilityLevel.Public: return "public";
                case Fact.Test.Result.Result.VisibilityLevel.Internal: return "internal";
                case Fact.Test.Result.Result.VisibilityLevel.Staging: return "staging";
                default: return "default";
            }
        }
        public virtual string PrintProject(Fact.Processing.Project Project, Options Options)
        {
            List<Fact.Processing.Project> projects = new List<Fact.Processing.Project>();
            projects.Add(Project);
            return PrintProjects(projects, Options);
        }
        public virtual string PrintProjects(List<Fact.Processing.Project> Projects, Options Options)
        {
            StringBuilder builder = new StringBuilder();
            foreach (Fact.Processing.Project project in Projects)
            {
                builder.Append(PrintProject(project, Options));
            }
            return builder.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Export.Printer
{
    class FactXml : Printer
    {
        public void PrintGroup(Fact.Test.Result.Group Group, StringBuilder Builder, Options Options)
        {
            Builder.Append("<group text=\"");
            Builder.Append(System.Uri.EscapeDataString(Group.Text));
            Builder.Append("\" ");
            Builder.Append(" visibility=\"");
            Builder.Append(ConvertVisibilityToString(Group.Visibility));
            Builder.Append("\" ");
            Builder.Append(">");
            foreach (Fact.Test.Result.Result Result in Group.GetTestResults())
            {
                PrintTest(Result, Builder, Options);
            }
            Builder.Append("</group>");
        }
        public void PrintTest(Fact.Test.Result.Result Result, StringBuilder Builder, Options Options)
        {
            if (Result is Fact.Test.Result.Group) { PrintGroup(Result as Fact.Test.Result.Group, Builder, Options); return; }
            string type = "test";
            if (Result is Fact.Test.Result.Error) { type = "error"; }
            if (Result is Fact.Test.Result.Note) { type = "note"; }
            if (Result is Fact.Test.Result.Omitted) { type = "omitted"; }
            if (Result is Fact.Test.Result.Passed) { type = "passed"; }
            if (Result is Fact.Test.Result.Warning) { type = "warning"; }

            Builder.Append("<");
            Builder.Append(type);
            Builder.Append(" text=\"");
            Builder.Append(System.Uri.EscapeUriString(Result.Text));
            Builder.Append("\" ");
            Builder.Append(" visibility=\"");
            Builder.Append(ConvertVisibilityToString(Result.Visibility));
            Builder.Append("\" ");

            if (Result.EnlapsedTime > 0)
            {
                Builder.Append(" enlapsedtime=\"");
                Builder.Append(Result.EnlapsedTime.ToString());
                Builder.Append("\" ");
            }
            if (Result.EnlapsedTime > 0)
            {
                Builder.Append(" enlapsedusertime=\"");
                Builder.Append(Result.EnlapsedUserTime.ToString());
                Builder.Append("\" ");
            }
            if (Result.EnlapsedTime > 0)
            {
                Builder.Append(" enlapsedkerneltime=\"");
                Builder.Append(Result.EnlapsedKernelTime.ToString());
                Builder.Append("\" ");
            }
            Builder.Append(" score=\"");
            Builder.Append(Result.Score.ToString());
            Builder.Append("\" ");
            Builder.Append(">");

            for (int offset = 0; offset < Result.Info.Length; offset += 32766)
            {
                int End = Math.Min(32766, Result.Info.Length - offset);
                Builder.Append(System.Uri.EscapeDataString(Result.Info.Substring(offset, End)));
            }
            Builder.Append("</");
            Builder.Append(type);
            Builder.Append(">");

        }
        public void PrintProject(Fact.Processing.Project Project, StringBuilder Builder, Options Options)
        {
            Builder.Append("<project name=\""); Builder.Append(System.Uri.EscapeDataString(Project.Name)); Builder.Append("\">");
            Builder.Append("<files>");
            Builder.Append("</files>");
            Builder.Append("<tests>");
            foreach (Fact.Test.Result.Result Result in Project.GetTestResults())
            {
                PrintTest(Result, Builder, Options);
            }
            Builder.Append("</tests>");
            Builder.Append("</project>");
        }
        public override string PrintProjects(List<Fact.Processing.Project> Projects, Options Options)
        {
            int Indentation = 0;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("<?xml version=\"1.0\"?>");
            builder.Append("<projects>");
            foreach (Fact.Processing.Project Project in Projects)
            {
                PrintProject(Project, builder, Options);
            }
            builder.Append("</projects>");
            return builder.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Export.Printer
{
    class FactOld : Printer
    {
        public static string Indent(int value)
        {
            string tab = "";
            for (int n = 0; n < value; n++)
            { tab += "  "; }
            return tab;
        }
        static HashSet<string> uid = new HashSet<string>();
        static string ToID(string name)
        {
            StringBuilder builder = new StringBuilder();
            for (int n = 0; n < name.Length; n++)
            {
                if (char.IsPunctuation(name[n]))
                {
                    builder.Append("__");
                }
                else if (char.IsLetterOrDigit(name[n]))
                {
                    builder.Append(name[n]);
                }
                else
                {
                    builder.Append("_");
                }
            }
            string id = builder.ToString();

            if (uid.Contains(id)) { return ToID(name + "_u_"); }
            else { uid.Add(id); }
            return id;
        }
        static bool IsSpecialChar(char chr)
        {
            return chr == '+' || chr == '-' ||
                    chr == '*' || chr == '/' ||
                    chr == '=' ||
                    chr == '_' || chr == '&' ||
                    chr == '%' || chr == '$' ||
                    chr == '#' || chr == '!' ||
                    chr == '~' || chr == '\'' ||
                    chr == '"' || chr == '@' ||
                    chr == '/' || chr == '\\' ||
                    chr == '[' || chr == ']' ||
                    chr == '(' || chr == ')' ||
                    chr == '<' || chr == '>' ||
                    chr == '{' || chr == '}' ||
                    chr == ' ' || chr == '\t' ||
                    chr == '\n' || chr == '\r' ||
                    chr == '`';
        }
        static string Reformat(string format)
        {
            StringBuilder builder = new StringBuilder();
            for (int n = 0; n < format.Length; n++)
            {
                if (char.IsPunctuation(format[n]) ||
                    char.IsLetterOrDigit(format[n]) ||
                    IsSpecialChar(format[n]))
                {
                    builder.Append(format[n]);
                }
                else
                {
                    builder.Append('\\');
                    builder.Append(((int)format[n]).ToString());
                }
            }
            return builder.ToString();
        }

        public static string XMLFormat(string text)
        {
            return System.Web.HttpUtility.HtmlEncode(Reformat(text.Normalize()));
        }

        public static void PrintGradeGroup(string ParentName, int Indentation, Fact.Test.Result.Group Group, StringBuilder Output)
        {
            Indentation++;
            foreach (Fact.Test.Result.Result result in Group.GetTestResults())
            {
                PrintGradeTest(result, Output, ParentName + "_" + result.Text, Indentation);
            }
            Indentation--;
        }

        public static void PrintGroup(string ParentName, int Indentation, Fact.Test.Result.Group Group, StringBuilder Output)
        {
            Output.AppendLine(Indent(Indentation) + "<group id=\"" + ToID("f_g_" + ParentName + "_" + Group.Text) + "\" name=\"" + Group.Text + "\">");
            Indentation++;
            foreach (Fact.Test.Result.Result result in Group.GetTestResults())
            {
                PrintTraceTest(result, Output, ParentName + "_" + result.Text, Indentation);
            }
            Indentation--;
            Output.AppendLine(Indent(Indentation) + "</group>");
        }

        public static void PrintTraceTest(Fact.Test.Result.Result Result, StringBuilder Output, string ParentName, int Indentation)
        {
            if (Result is Fact.Test.Result.Group)
            {
                PrintGroup(ParentName + "_" + Result.Text, Indentation, Result as Fact.Test.Result.Group, Output);
                return;
            }
            Output.AppendLine(Indent(Indentation) + "<eval type=\"test\" id=\"" + ToID("f_" + ParentName + "_" + Result.Text) + "\">");
            Indentation++;
            Output.AppendLine(Indent(Indentation) + "<name>" + Result.Text + "</name>");
            if (Result.Info != "")
            {
                Output.AppendLine(Indent(Indentation) + "<log type=\"Info\">");
                Output.AppendLine(Indent(Indentation) + XMLFormat(Result.Info));
                Output.AppendLine(Indent(Indentation) + "</log>");
            }
            Output.AppendLine(Indent(Indentation) + "<value>" + Result.Score + "</value>");
            if (Result is Fact.Test.Result.Error) { Output.AppendLine(Indent(Indentation) + "<status>FAIL</status>"); }
            if (Result is Fact.Test.Result.Note) { Output.AppendLine(Indent(Indentation) + "<status>NOTE</status>"); }
            if (Result is Fact.Test.Result.Passed) { Output.AppendLine(Indent(Indentation) + "<status>PASS</status>"); }
            if (Result is Fact.Test.Result.Warning) { Output.AppendLine(Indent(Indentation) + "<status>WARNING</status>"); }
            Indentation--;
            Output.AppendLine(Indent(Indentation) + "</eval>");
        }

        public static void PrintGradeTest(Fact.Test.Result.Result Result, StringBuilder Output, string ParentName, int Indentation)
        {
            if (Result is Fact.Test.Result.Group)
            {
                PrintGradeGroup(ParentName + "_" + Result.Text, Indentation, Result as Fact.Test.Result.Group, Output);
            }
            else
            {
                Output.AppendLine(Indent(Indentation) + "<point ref=\"" + ToID("f_" + ParentName + "_" + Result.Text) + "\"></point>");
            }
        }

        public static void PrintTrace(List<Fact.Processing.Project> projects, StringBuilder Output, int Indentation, Options Options)
        {
            Output.AppendLine("<trace type=\"mill\">");
            Indentation++;
            foreach (Fact.Processing.Project project in projects)
            {
                string name = "fact_trace";

                ulong uid = 1;
                ulong guid = 1;


                Indentation++;
                try
                {
                        Output.AppendLine(Indent(Indentation) + "<group id=\"" + ToID("root_" + name) + "\" name=\"Project\">");
                        foreach (Fact.Test.Result.Result result in project.GetTestResults())
                            PrintTraceTest(result, Output, name, Indentation);
                        Output.AppendLine(Indent(Indentation) + "</group>");

                        Output.AppendLine(Indent(Indentation) + "<group id=\"" + ToID("file_root_" + name) + "\" name=\"Files\">");
                        foreach (Fact.Processing.File file in project)
                        {
                            bool init = false;
                            uid = 1;

                            foreach (Fact.Test.Result.Result result in file.GetTestResults())
                            {
                                if (!init)
                                {
                                    Output.AppendLine(Indent(Indentation) + "<group id=\"" + ToID("f_g_" + name + "_" + file.Name) + "\" name=\"" + file.Name + "\">");
                                    init = true;
                                }
                                PrintTraceTest(result, Output, name + "_" + file.Name, Indentation);
                            }
                            if (init)
                                Output.AppendLine(Indent(Indentation) + "</group>");
                        }
                        Output.AppendLine(Indent(Indentation) + "</group>");
                }
                catch
                {
                    Fact.Log.Error("Unexpected error during file parsing");
                }
                Indentation--;
            }
            Indentation--;
            Output.AppendLine("</trace>");
        }

        public override string PrintProjects(List<Fact.Processing.Project> Project, Options Options)
        {
            int Indentation = 0;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("<?xml version=\"1.0\"?>");
            PrintTrace(Project, builder, Indentation, Options);
            return builder.ToString();
        }
    }
}

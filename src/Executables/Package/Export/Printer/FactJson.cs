﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Export.Printer
{
    class FactJson : Printer
    {
        public void PrintGroup(Fact.Test.Result.Group Group, StringBuilder Builder, Options Options)
        {
            Builder.Append(" { \"type\":\"group\" , \"text\":\"");
            Builder.Append(System.Uri.EscapeDataString(Group.Text));
            Builder.Append("\", \"visibility\":\"");
            Builder.Append(ConvertVisibilityToString(Group.Visibility));
            Builder.Append("\", ");
            Builder.Append(" \"children\":[ ");
            bool start = true;
            foreach (Fact.Test.Result.Result Result in Group.GetTestResults())
            {
                if (!start) { Builder.Append(", "); }
                PrintTest(Result, Builder, Options);
                start = false;
            }
            Builder.Append(" ]  } ");

        }
        public void PrintTest(Fact.Test.Result.Result Result, StringBuilder Builder, Options Options)
        {
            if (Result is Fact.Test.Result.Group) { PrintGroup(Result as Fact.Test.Result.Group, Builder, Options); return; }
            string type = "test";
            if (Result is Fact.Test.Result.Error) { type = "error"; }
            if (Result is Fact.Test.Result.Note) { type = "note"; }
            if (Result is Fact.Test.Result.Omitted) { type = "omitted"; }
            if (Result is Fact.Test.Result.Passed) { type = "passed"; }
            if (Result is Fact.Test.Result.Warning) { type = "warning"; }

            Builder.Append(" { \"type\":\"");
            Builder.Append(type);
            Builder.Append("\", \"text\":\"");
            Builder.Append(System.Uri.EscapeDataString(Result.Text));
            Builder.Append("\", \"visibility\":\"");
            Builder.Append(ConvertVisibilityToString(Result.Visibility));
            Builder.Append("\",  \"info\":\"");
            Builder.Append(System.Uri.EscapeDataString(Result.Info));
            Builder.Append("\",  ");
            if (Result.EnlapsedTime > 0)
            {
                Builder.Append(" \"enlapsedtime\":");
                Builder.Append(Result.EnlapsedTime.ToString());
                Builder.Append(",");
            }
            if (Result.EnlapsedTime > 0)
            {
                Builder.Append(" \"enlapsedusertime\":");
                Builder.Append(Result.EnlapsedUserTime.ToString());
                Builder.Append(", ");
            }
            if (Result.EnlapsedTime > 0)
            {
                Builder.Append(" \"enlapsedkerneltime\":");
                Builder.Append(Result.EnlapsedKernelTime.ToString());
                Builder.Append(", ");
            }
            Builder.Append(" \"score\":");
            Builder.Append(Result.Score.ToString());
            Builder.Append(" } ");

        }
        public void PrintProject(Fact.Processing.Project Project, StringBuilder Builder, Options Options)
        {
            Builder.Append("{");
            Builder.Append("\"name\":\"");
            Builder.Append(System.Uri.EscapeDataString(Project.Name));
            Builder.Append("\", ");
            Builder.Append("\"files\":[ ");
            Builder.Append(" ], ");
            Builder.Append("\"tests\":[ ");
            bool start = true;
            foreach (Fact.Test.Result.Result Result in Project.GetTestResults())
            {
                if (!start) { Builder.Append(", "); }
                PrintTest(Result, Builder, Options);
                start = false;
            }
            Builder.Append(" ] } ");
        }
        public override string PrintProjects(List<Fact.Processing.Project> Projects, Options Options)
        {
            int Indentation = 0;
            StringBuilder Builder = new StringBuilder();
            Builder.Append("{ \"projects\":[ ");
            bool start = true;
            foreach (Fact.Processing.Project Project in Projects)
            {
                if (!start) { Builder.Append(", "); }
                PrintProject(Project, Builder, Options);
                start = false;
            }
            Builder.Append(" ]  }");
            return Builder.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Runtime
{
    [Fact.Attribute.Test.Group("Process Sanity")]
    public class ProcessSanity
    {
        string BasicProcess = "";
        [Fact.Attribute.Test.Setup("Initialize environement")]
        public void Setup()
        {
            BasicProcess = Fact.Internal.Information.GetCCBinary();
            if (BasicProcess == "" || !System.IO.File.Exists(BasicProcess)) { Fact.Assert.Misc.Skip(); }
        }

        [Fact.Attribute.Test.Test("Run the process")]
        public void RunProcess()
        {
            string Arguments = "";
            if (BasicProcess.EndsWith("cl.exe"))
            {
                Arguments = "/?";
            }
            else
            {
                Arguments = "--help";
            }

            Fact.Runtime.Process process = new Fact.Runtime.Process(BasicProcess, Arguments);
            Fact.Runtime.Process.Result result = process.Run(1000);
            Fact.Assert.Basic.AreEqual(0, result.ExitCode);
        }

        [Fact.Attribute.Test.Test("Run the process in asyncmode")]
        public void RunProcessAsync()
        {
            string Arguments = "";
            if (BasicProcess.EndsWith("cl.exe"))
            {
                Arguments = "/?";
            }
            else
            {
                Arguments = "--help";
            }

            Fact.Runtime.Process process = new Fact.Runtime.Process(BasicProcess, Arguments);
            Fact.Runtime.Process.Result result = process.RunAsync(1000);
            process.WaitForExit();
            Fact.Assert.Basic.AreEqual(0, result.ExitCode);
        }
    }
}

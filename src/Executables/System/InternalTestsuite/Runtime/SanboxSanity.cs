﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Runtime
{
    [Fact.Attribute.Test.Group("Context Sanity")]
    class SanboxSanity
    {
        [Fact.Attribute.Test.Setup("Initialize Context")]
        public void InitializeSandbox()
        {
            Fact.Context.Context Context = Fact.Context.Context.Current.CreateSubContext();
        }
        [Fact.Attribute.Test.Setup("Basic sanboxed functions")]
        public void BasicSandboxedFunctions()
        {
            Fact.Context.Context Context = Fact.Context.Context.Current.CreateSubContext();
            Fact.Assert.Basic.AreEqual(0, (int)Context.Execute((Func<int>)(() => { return 0; })));
            Fact.Assert.Basic.AreEqual(1, (int)Context.Execute((Func<int>)(() => { return 1; })));
            Fact.Assert.Basic.AreEqual(2, (int)Context.Execute((Func<int>)(() => { return 2; })));
        }

        int _ExternalValue = 1;

        [Fact.Attribute.Test.Setup("Basic external read function")]
        public void BasicExternalReadFunction()
        {
            using (Fact.Context.Context Context = Fact.Context.Context.Current.CreateSubContext())
            {
                for (int n = 0; n < 10; n++)
                {
                    Fact.Assert.Basic.AreEqual(n + _ExternalValue, (int)Context.Execute((Func<int>)(() => { return n + _ExternalValue; })));
                }
            }
        }
    }
}

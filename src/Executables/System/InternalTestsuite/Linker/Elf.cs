﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Linker
{
    [Fact.Attribute.Test.Group("Linker")]
    public partial class Linker
    {
        [Fact.Attribute.Test.Group("Elf")]
        public unsafe class Elf
        {
            string findLibtest()
            {
                if (System.IO.File.Exists("./libtest.so")) { return "./libtest.so"; }
                if (System.IO.File.Exists("../libtest.so")) { return "../libtest.so"; }
                if (System.IO.File.Exists("../../libtest.so")) { return "../../libtest.so"; }
                if (System.IO.File.Exists("../../external/libtest.so")) { return "../../external/libtest.so"; }
                if (System.IO.File.Exists("../../external/test/libtest.so")) { return "../../external/test/libtest.so"; }
                if (System.IO.File.Exists("./external/test/libtest.so")) { return "./external/test/libtest.so"; }
                return "libtest.so";
            }

            Fact.Reflection.Assembly _Assembly = null;

            [Fact.Attribute.Test.Setup("Load Libtest")]
            public void Setup()
            {
                // Find lib test
                string libtest = findLibtest();
                if (!System.IO.File.Exists(libtest)) { Fact.Assert.Misc.Skip("Libtest.so not found"); }

                _Assembly = Fact.Reflection.Assembly.LoadFromFile(libtest);
            }

            [Fact.Attribute.Test.Test("Symbol loading")]
            public void SymbolLoading()
            {
                Fact.Assert.Basic.IsTrue(_Assembly.GetMethod("Empty") != null);
                Fact.Assert.Basic.IsTrue(_Assembly.GetMethod("Return0") != null);
                Fact.Assert.Basic.IsTrue(_Assembly.GetMethod("Return1") != null);
                Fact.Assert.Basic.IsTrue(_Assembly.GetMethod("Print") != null);
            }

            [Fact.Attribute.Test.Test("Basic Invoke")]
            public void ClassicInvoke()
            {
                _Assembly.GetMethod("Empty").Invoke(null);
                Fact.Assert.Basic.AreEqual(0, _Assembly.GetMethod("Return0").Invoke<int>(null));
                Fact.Assert.Basic.AreEqual(1, _Assembly.GetMethod("Return1").Invoke<int>(null));
            }

            delegate void PrintfDelegate(char* formatstring, char* str);

            public void EmptyPrintf(char* formatstring, char* str) { }

            [Fact.Attribute.Test.Test("Relloc Invoke")]
            public unsafe void RellocInvoke()
            {
                _Assembly.GetImportedMethod("printf").Hook((PrintfDelegate)EmptyPrintf);
                _Assembly.GetMethod("Print").Invoke(null, "hello world !");
            }

            [Fact.Attribute.Test.Test("Invoke Malloc")]
            public unsafe void InvokeMalloc()
            {
                for (int n = 0; n < 1024 * 10; n++)
                {
                    byte* ptr = (byte*)_Assembly.GetMethod("CallMalloc").Invoke<IntPtr>(null, 1024).ToPointer();
                    ptr[0] = 0x42;
                    ptr[1023] = 0x52;
                }

            }
        }
    }
}

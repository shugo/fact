﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fact.Attribute.Test;

namespace InternalTestsuite.Processing
{
    [Fact.Attribute.Test.Group("Package Basic Sanity")]
    class PackageSanity
    {
        string _TempFolder = "";
        [Fact.Attribute.Test.Setup("Initialize environement")]
        public void Setup()
        {
            _TempFolder = Fact.Tools.CreateTempDirectory();
            Fact.Assert.IO.DirectoryExists(_TempFolder);
        }

        [Fact.Attribute.Test.Test("Create Empty Project")]
        public void CreateEmptyProject()
        {
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
        }

        [Fact.Attribute.Test.Test("Save Empty Project")]
        public void SaveEmptyProject()
        {
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            string name = Fact.Internal.Information.GetTempFileName();
            project.Save(_TempFolder + "/" + name);
            Fact.Assert.IO.FileExists(_TempFolder + "/" + name);
            System.IO.File.Delete(_TempFolder + "/" + name);
        }

        [Fact.Attribute.Test.Test("Load Empty Project")]
        public void LoadEmptyProject()
        {
            string name = Fact.Internal.Information.GetTempFileName();
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            project.Save(_TempFolder + "/" + name);
            Fact.Assert.IO.FileExists(_TempFolder + "/" + name);
            Fact.Processing.Project loadedProject = Fact.Processing.Project.Load(_TempFolder + "/" + name);
            Fact.Assert.Basic.AreEqual("testproject", loadedProject.Name);
            System.IO.File.Delete(_TempFolder + "/" + name);
        }

        [Fact.Attribute.Test.Test("Add a file")]
        public void AddFile()
        {
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File a = new Fact.Processing.File(new string[] { "a" }, "a");
            Fact.Processing.File b = new Fact.Processing.File(new string[] { "b" }, "b");
            project.AddFile("/", a, false);
            project.AddFile("/", b, false);
            Fact.Assert.Basic.IsTrue(project.Contains("/a"));
            Fact.Assert.Basic.IsTrue(project.Contains("/b"));
            Fact.Assert.Basic.IsFalse(project.Contains("/c"));
        }

        [Fact.Attribute.Test.Test("Add a file in a specific directory")]
        public void AddFileInDir()
        {
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File a = new Fact.Processing.File(new string[] { "a" }, "a");
            Fact.Processing.File b = new Fact.Processing.File(new string[] { "b" }, "b");
            project.AddFile("/test1/test2/", a, false);
            project.AddFile("/test1/test3/", b, false);
            Fact.Assert.Basic.IsTrue(project.Contains("/test1/test2/a"));
            Fact.Assert.Basic.IsTrue(project.Contains("/test1/test3/b"));
            Fact.Assert.Basic.IsFalse(project.Contains("/c"));
        }

        [Fact.Attribute.Test.Test("Create a Sub project")]
        public void CretaeSubProject()
        {
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File a = new Fact.Processing.File(new string[] { "a" }, "a");
            Fact.Processing.File b = new Fact.Processing.File(new string[] { "b" }, "b");
            Fact.Processing.File c = new Fact.Processing.File(new string[] { "c" }, "c");

            project.AddFile("/test1/test2/", a, false);
            project.AddFile("/test1/test3/", b, false);
            project.AddFile("/test1/", c, false);
            Fact.Processing.SubProject subproject = new Fact.Processing.SubProject(project, "/test1/");
            Fact.Assert.Basic.IsTrue(subproject.Contains("/test2/a"));
            Fact.Assert.Basic.IsTrue(subproject.Contains("/test3/b"));
            Fact.Assert.Basic.IsTrue(subproject.Contains("/c"));
            Fact.Assert.Basic.IsFalse(subproject.Contains("/d"));
        }

        [Fact.Attribute.Test.Test("Extract a specific directory")]
        public void ExtractDirectory()
        {
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File a = new Fact.Processing.File(new string[] { "a" }, "a");
            Fact.Processing.File b = new Fact.Processing.File(new string[] { "b" }, "b");
            project.AddFile("/test1/test2/", a, false);
            project.AddFile("/test1/test3/", b, false);
            project.ExtractDirectory("/test1/test2", _TempFolder + "/extract_test2");
            Fact.Assert.IO.DirectoryExists(_TempFolder + "/extract_test2");
            Fact.Assert.IO.FileExists(_TempFolder + "/extract_test2/a");
            Fact.Tools.RecursiveDelete(_TempFolder + "/extract_test2");
        }

        [Fact.Attribute.Test.Test("Extract with a pattern")]
        public void ExtractPattern()
        {
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File a = new Fact.Processing.File(new string[] { "a" }, "a");
            Fact.Processing.File b = new Fact.Processing.File(new string[] { "b" }, "b");
            project.AddFile("/test1/test2/", a, false);
            project.AddFile("/test1/test3/", b, false);
            project.ExtractFiles("/test1/test2", _TempFolder + "/extract_test2");
            Fact.Assert.IO.FileExists(_TempFolder + "/extract_test2/test1/test2/a");
            Fact.Tools.RecursiveDelete(_TempFolder + "/extract_test2");
        }

        [Fact.Attribute.Test.Teardown("Teardown environement")]
        public void Teardown()
        {
            Fact.Tools.RecursiveDelete(_TempFolder);
            Fact.Assert.IO.DirectoryNotExists(_TempFolder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Processing
{
    [Fact.Attribute.Test.Group("Package File Saving Sanity")]
    class FileSaving
    {
        string _TempFolder = "";
        [Fact.Attribute.Test.Setup("Initialize environement")]
        public void Setup()
        {
            _TempFolder = Fact.Tools.CreateTempDirectory();
            Fact.Assert.IO.DirectoryExists(_TempFolder);
        }

        [Fact.Attribute.Test.Test("Create Text File")]
        public void CreateTextFile()
        {
            string name = Fact.Internal.Information.GetTempFileName();
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File text = new Fact.Processing.File(new string[] { "test" }, "test.txt");
            text.Type = Fact.Processing.File.FileType.Text;
            project.AddFile(text);
            Fact.Assert.Basic.AreEqual("test", project.GetFile("test.txt").GetText(false).Trim());
        }

        [Fact.Attribute.Test.Test("Save Text File")]
        public void SaveTextFile()
        {
            string name = Fact.Internal.Information.GetTempFileName();
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File text = new Fact.Processing.File(new string[] { "test" }, "test.txt");
            text.Type = Fact.Processing.File.FileType.Text;
            project.AddFile(text);
            project.Save(_TempFolder + "/" + name);
            Fact.Assert.IO.FileExists(_TempFolder + "/" + name);
            Fact.Processing.Project loadedProject = Fact.Processing.Project.Load(_TempFolder + "/" + name);
            Fact.Assert.Basic.IsTrue(loadedProject.Contains("test.txt"));
            Fact.Assert.Basic.AreEqual(text.GetText(false), loadedProject.GetFile("test.txt").GetText(false));
            Fact.Assert.Basic.AreEqual("test", loadedProject.GetFile("test.txt").GetText(false).Trim());
            System.IO.File.Delete(_TempFolder + "/" + name);
        }

        [Fact.Attribute.Test.Test("Save Big Text File")]
        public void SaveBigTextFile()
        {
            string name = Fact.Internal.Information.GetTempFileName();
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            List<string> bigtext = new List<string>();
            for (int n = 0; n < 1024 * 1024; n++)
            {
                bigtext.Add("toto titi tata");
            }
            Fact.Processing.File text = new Fact.Processing.File(bigtext.ToArray(), "test.txt");
            text.Type = Fact.Processing.File.FileType.Text;
            project.AddFile(text);
            project.Save(_TempFolder + "/" + name);
            Fact.Assert.IO.FileExists(_TempFolder + "/" + name);
            Fact.Processing.Project loadedProject = Fact.Processing.Project.Load(_TempFolder + "/" + name);

            Fact.Assert.Basic.IsTrue(loadedProject.Contains("test.txt"));
            Fact.Assert.Basic.AreEqual(text.GetText(false), loadedProject.GetFile("test.txt").GetText(false));
            System.IO.File.Delete(_TempFolder + "/" + name);
        }

        [Fact.Attribute.Test.Test("Save a Test")]
        public void SaveTest()
        {
            string name = Fact.Internal.Information.GetTempFileName();
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            project.AddTestResult(new Fact.Test.Result.Passed("test", "passed"));
            project.Save(_TempFolder + "/" + name);
            Fact.Assert.IO.FileExists(_TempFolder + "/" + name);
            Fact.Processing.Project loadedProject = Fact.Processing.Project.Load(_TempFolder + "/" + name);
            Fact.Assert.Basic.AreEqual(1, loadedProject.GetTestResults().Count);
            Fact.Assert.Basic.AreEqual("test", loadedProject.GetTestResults()[0].Text);
            Fact.Assert.Basic.AreEqual("passed", loadedProject.GetTestResults()[0].Info);
            System.IO.File.Delete(_TempFolder + "/" + name);
        }

        [Fact.Attribute.Test.Test("Save a Null Test")]
        public void SaveNullTest()
        {
            string name = Fact.Internal.Information.GetTempFileName();
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            project.AddTestResult(null);
            project.Save(_TempFolder + "/" + name);
            Fact.Assert.IO.FileExists(_TempFolder + "/" + name);
            Fact.Processing.Project loadedProject = Fact.Processing.Project.Load(_TempFolder + "/" + name);
            Fact.Assert.Basic.AreEqual(0, loadedProject.GetTestResults().Count);
            System.IO.File.Delete(_TempFolder + "/" + name);
        }

        [Fact.Attribute.Test.Test("Save a Test Attached to a File")]
        public void SaveTestAttachToFile()
        {
            string name = Fact.Internal.Information.GetTempFileName();
            Fact.Processing.Project project = new Fact.Processing.Project("testproject");
            Fact.Processing.File text = new Fact.Processing.File(new string[] { "test" }, "test.txt");
            text.AddTestResult(new Fact.Test.Result.Passed("test", "passed"));
            text.Type = Fact.Processing.File.FileType.Text;
            project.AddFile(text);
            project.Save(_TempFolder + "/" + name);
            Fact.Assert.IO.FileExists(_TempFolder + "/" + name);
            Fact.Processing.Project loadedProject = Fact.Processing.Project.Load(_TempFolder + "/" + name);
            Fact.Assert.Basic.IsTrue(loadedProject.Contains("test.txt"));
            Fact.Assert.Basic.AreEqual(text.GetText(false), loadedProject.GetFile("test.txt").GetText(false));
            Fact.Assert.Basic.AreEqual("test", loadedProject.GetFile("test.txt").GetText(false).Trim());
            Fact.Assert.Basic.AreEqual(1, loadedProject.GetFile("test.txt").GetTestResults().Count);
            Fact.Assert.Basic.AreEqual("test", loadedProject.GetFile("test.txt").GetTestResults()[0].Text);
             Fact.Assert.Basic.AreEqual("passed", loadedProject.GetFile("test.txt").GetTestResults()[0].Info);
            System.IO.File.Delete(_TempFolder + "/" + name);
        }

        [Fact.Attribute.Test.Teardown("Teardown environement")]
        public void Teardown()
        {
            Fact.Tools.RecursiveDelete(_TempFolder);
            Fact.Assert.IO.DirectoryNotExists(_TempFolder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Internal
{
    [Fact.Attribute.Test.Group("Os Abstraction Layer")]
    class Os
    {
        [Fact.Attribute.Test.Test("Memory Mapping RW")]
        public void MemoryMappingRW()
        {
            byte[] buffer1 = new byte[4096 * 32];
            byte[] buffer2 = new byte[4096 * 32];
            for (int n = 0; n < buffer1.Length; n++) { buffer1[n] = 0; }
            for (int n = 0; n < buffer2.Length; n++) { buffer2[n] = 0x42; }

            IntPtr ptr = Fact.Internal.Os.Generic.MapMemory(4094 * 32, Fact.Internal.Os.Generic.Protection.Read | Fact.Internal.Os.Generic.Protection.Write);
            Fact.Assert.Basic.IsFalse(0 == ptr.ToInt64());
            System.Runtime.InteropServices.Marshal.Copy(buffer2, 0, ptr, 4096 * 32);
            System.Runtime.InteropServices.Marshal.Copy(ptr, buffer1, 0, 4096 * 32);
            for (int n = 0; n < buffer1.Length; n++) { Fact.Assert.Basic.AreEqual(0x42, buffer1[n]); }
        }


        [Fact.Attribute.Test.Test("Memory Mapping R")]
        public void MemoryMappingR(Fact.Processing.Project Student)
        {
            byte[] buffer1 = new byte[4096 * 32];
            Fact.Processing.File f = new Fact.Processing.File(buffer1, "test");
            byte[] buffer2 = new byte[4096 * 32];
            for (int n = 0; n < buffer1.Length; n++) { buffer1[n] = 0; }
            for (int n = 0; n < buffer2.Length; n++) { buffer2[n] = 0x42; }

            IntPtr ptr = Fact.Internal.Os.Generic.MapMemory(4096 * 32, Fact.Internal.Os.Generic.Protection.Read | Fact.Internal.Os.Generic.Protection.Write);
            Fact.Assert.Basic.IsFalse(0 == ptr.ToInt64());
            System.Runtime.InteropServices.Marshal.Copy(buffer2, 0, ptr, 4096 * 32);
            Fact.Internal.Os.Generic.ChangeMemoryProtection(ptr, 4096 * 32, Fact.Internal.Os.Generic.Protection.Read);
            try
            {
                System.Runtime.InteropServices.Marshal.Copy(buffer2, 0, ptr, 4096 * 32);
                Fact.Assert.Basic.MustNotOccur();
            }
            catch 
            {
            }
            System.Runtime.InteropServices.Marshal.Copy(ptr, buffer1, 0, 4096 * 32);
            for (int n = 0; n < buffer1.Length; n++) { Fact.Assert.Basic.AreEqual(0x42, buffer1[n]); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Assert
{
    [Fact.Attribute.Test.Group("Collections")]
    class Collections
    {
        [Fact.Attribute.Test.Test("Are Sequence Equal")]
        public void AreSequenceEqual()
        {
            List<string> list1 = new List<string>(new string[]{"a", "b", "c"});
            List<string> list2 = new List<string>(new string[]{"a", "b", "c"});
            Fact.Assert.Collection.AreSequenceEqual(list1, list2);
        }

        [Fact.Attribute.Test.Test("Are Sequence Equal2")]
        public void AreSequenceEqual2()
        {
            List<string> list1 = new List<string>(new string[] { "a", "b", "c" });
            List<string> list2 = new List<string>(new string[] { "a", "b", "c",  "c"});
            Fact.Assert.Collection.AreSequenceEqual(list1, list2);
        }

        [Fact.Attribute.Test.Test("Are Unorderd Equal")]
        public void AreUnorderdEqual()
        {
            List<string> list1 = new List<string>(new string[] { "a", "c", "b" });
            List<string> list2 = new List<string>(new string[] { "a", "b", "c" });
            Fact.Assert.Collection.AreUnorderdEqual(list1, list2);
        }

        [Fact.Attribute.Test.Test("Are Unorderd Equal2")]
        public void AreUnorderdEqual2()
        {
            List<string> list1 = new List<string>(new string[] { "a", "c", "b" });
            List<string> list2 = new List<string>(new string[] { "a", "c", "b" , "b"});
            Fact.Assert.Collection.AreUnorderdEqual(list1, list2);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Directory
{
    [Fact.Attribute.Test.Group("Directory")]
    partial class Directory
    {
        [Fact.Attribute.Test.Group("Group")]

        class Group
        {
            [Fact.Attribute.Test.Test("Add User")]
            public void AddUser()
            {
                Fact.Directory.Group group = new Fact.Directory.Group();
                group.AddUser(new Fact.Directory.User("adduser", "test"));
                Fact.Assert.Basic.IsTrue(group.GetUser("adduser") != null);
            }

            [Fact.Attribute.Test.Test("Remove User")]
            public void RemoveUser()
            {
                Fact.Directory.Group group = new Fact.Directory.Group();
                group.AddUser(new Fact.Directory.User("adduser", "test"));
                group.RemoveUser("adduser");
                Fact.Assert.Basic.IsTrue(group.GetUser("adduser") == null);
            }

            [Fact.Attribute.Test.Test("Add User in Sub Group")]
            public void AddUserInSubGroup()
            {
                Fact.Directory.Group group = new Fact.Directory.Group();
                Fact.Directory.Group subgroup = new Fact.Directory.Group();
                group.AddGroup(subgroup);
                subgroup.AddUser(new Fact.Directory.User("adduser", "test"));
                Fact.Assert.Basic.IsTrue(group.GetUser("adduser") != null);
            }

            [Fact.Attribute.Test.Test("Remove User in Sub Group")]
            public void RemoveUserInSubGroup()
            {
                Fact.Directory.Group group = new Fact.Directory.Group();
                Fact.Directory.Group subgroup = new Fact.Directory.Group();
                group.AddGroup(subgroup);
                subgroup.AddUser(new Fact.Directory.User("adduser", "test"));
                group.RemoveUser("adduser");
                Fact.Assert.Basic.IsTrue(group.GetUser("adduser") == null);
            }

            [Fact.Attribute.Test.Test("Concurent actions")]
            public void Concurent()
            {
                Fact.Directory.Group group = new Fact.Directory.Group();
                for (int n = 0; n < 1000; n++)
                {
                    Fact.Threading.Job.CreateJob(() => 
                    {
                        group.AddUser(new Fact.Directory.User("adduser" + n.ToString(), "test"));
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        Fact.Directory.User user = new Fact.Directory.User("creduser" + n.ToString(), "test");
                        group.AddUser(user);
                        Fact.Directory.Credential cred = new Fact.Directory.Credential(user, DateTime.MaxValue);
                        group.AuthenticateWithCredential("creduser" + n.ToString(), cred.Id, cred.Password, Fact.Directory.User.AuthenticationMethod.Fact_TimeSafe, out user, out cred);
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        group.AddGroup(new Fact.Directory.Group("addgroup" + n.ToString()));
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        group.AddUser(new Fact.Directory.User("addandremove" + n.ToString(), "test"));
                        group.RemoveUser("addandremove" + n.ToString());
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        System.IO.MemoryStream stream = new System.IO.MemoryStream();
                        group.Save(stream);
                    });
                    group.AddUser(new Fact.Directory.User("test" + n.ToString(), "test"));
                    Fact.Assert.Basic.IsTrue(group.GetUser("test" + n.ToString()) != null);

                    if (n % 100 == 0) { Fact.Threading.Job.Synchronize(); }
                }
            }


            [Fact.Attribute.Test.Test("Concurent actions with subgroups")]
            public void ConcurentSubGroup()
            {
                Fact.Directory.Group group = new Fact.Directory.Group();
                Fact.Directory.Group subgroupA = new Fact.Directory.Group("gpA");
                Fact.Directory.Group subgroupB = new Fact.Directory.Group("gpB");

                group.AddGroup(subgroupA);
                group.AddGroup(subgroupB);

                for (int n = 0; n < 1000; n++)
                {
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        group.AddUser(new Fact.Directory.User("adduser" + n.ToString(), "test"));
                        subgroupA.AddUser(new Fact.Directory.User("subA_adduser" + n.ToString(), "test"));
                        subgroupB.AddUser(new Fact.Directory.User("subB_adduser" + n.ToString(), "test"));
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        Fact.Directory.User user = new Fact.Directory.User("creduser" + n.ToString(), "test");
                        group.AddUser(user);
                        Fact.Directory.Credential cred = new Fact.Directory.Credential(user, DateTime.MaxValue);
                        group.AuthenticateWithCredential("creduser" + n.ToString(), cred.Id, cred.Password, Fact.Directory.User.AuthenticationMethod.Fact_TimeSafe, out user, out cred);
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        group.AddGroup(new Fact.Directory.Group("addgroup" + n.ToString()));
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        group.AddUser(new Fact.Directory.User("addandremove" + n.ToString(), "test"));
                        group.RemoveUser("addandremove" + n.ToString());
                    });
                    Fact.Threading.Job.CreateJob(() =>
                    {
                        System.IO.MemoryStream stream = new System.IO.MemoryStream();
                        group.Save(stream);
                    });
                    group.AddUser(new Fact.Directory.User("test" + n.ToString(), "test"));
                    Fact.Assert.Basic.IsTrue(group.GetUser("test" + n.ToString()) != null);

                    subgroupA.AddUser(new Fact.Directory.User("subA_test" + n.ToString(), "test"));
                    Fact.Assert.Basic.IsTrue(group.GetUser("subA_test" + n.ToString()) != null);

                    subgroupB.AddUser(new Fact.Directory.User("subB_test" + n.ToString(), "test"));
                    Fact.Assert.Basic.IsTrue(group.GetUser("subB_test" + n.ToString()) != null);

                    if (n % 100 == 0) { Fact.Threading.Job.Synchronize(); }
                }

                Fact.Threading.Job.Synchronize();
            }

            [Fact.Attribute.Test.Test("Save group")]
            public void SaveGroup()
            {
                Fact.Directory.Group group = new Fact.Directory.Group("group1");
                group.AddUser(new Fact.Directory.User("user1", "test"));
                group.AddUser(new Fact.Directory.User("user2", "test"));
                Fact.Directory.Group group2 = new Fact.Directory.Group("group2");
                Fact.Directory.Credential cred1 = new Fact.Directory.Credential(group.GetUser("user2"), DateTime.MaxValue);

                group.AddGroup(group2);
                Fact.Directory.User user = new Fact.Directory.User("user3", "test");
                Fact.Directory.Credential cred2 = new Fact.Directory.Credential(user, DateTime.MaxValue);
                Fact.Directory.Credential cred2_dup = new Fact.Directory.Credential(user, DateTime.MaxValue);

                System.IO.MemoryStream stream1 = new System.IO.MemoryStream();
                group.Save(stream1);
                System.IO.MemoryStream stream2 = new System.IO.MemoryStream(stream1.ToArray());
                group = new Fact.Directory.Group();
                group.Load(stream2);
                Fact.Assert.Basic.IsTrue(group.GetUser("user1") != null);
                Fact.Assert.Basic.IsTrue(group.GetUser("user2") != null);
            }
        }
    }
}

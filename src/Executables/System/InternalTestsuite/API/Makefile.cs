﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.API
{
    [Fact.Attribute.Test.Group("API")]
    public partial class API
    {
        public int a = 0;
        [Fact.Attribute.Test.Group("Makefile")]
        public class Makefile
        {
            [Fact.Attribute.Test.Test("Include")]
            public void Include()
            {
                Fact.Processing.File OneMakeFile = new Fact.Processing.File(new string[]
                {
                    "-include test.txt"
                });
                Fact.API.Makefile.Makefile ParsedMakefile = new Fact.API.Makefile.Makefile(OneMakeFile);
                Fact.Assert.Basic.AreEqual(1, ParsedMakefile.Entries.Count());
                foreach (Fact.API.Makefile.MakefileEntry entry in ParsedMakefile.Entries)
                {
                    if (entry.Name == "include") { return; }
                }
                Fact.Assert.Basic.MustNotOccurs();
            }

            [Fact.Attribute.Test.Test("Include with slash")]
            public void IncludeWithSlash()
            {
                Fact.Processing.File OneMakeFile = new Fact.Processing.File(new string[]
                {
                    "-include ./test.txt"
                });
                Fact.API.Makefile.Makefile ParsedMakefile = new Fact.API.Makefile.Makefile(OneMakeFile);
                Fact.Assert.Basic.AreEqual(1, ParsedMakefile.Entries.Count());
                foreach (Fact.API.Makefile.MakefileEntry entry in ParsedMakefile.Entries)
                {
                    if (entry.Name == "include") { return; }
                }
                Fact.Assert.Basic.MustNotOccurs();
            }
        }
    }
}

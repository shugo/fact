﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Emulation
{
    [Fact.Attribute.Test.Group("Processor")]
    public unsafe partial class Processor
    {
        public class _8086Lite
        {
            internal ushort ax = 0;
            internal ushort bx = 0;
            internal ulong cx = 0;
            internal ushort dx = 0;

            ushort si = 0;
            ushort di = 0;
            ushort bp = 0;
            ushort sp = 0;

            ushort cs = 0;
            ushort ds = 0;
            ushort es = 0;
            ushort ss = 0;

            byte* BaseMemory = null;

            public _8086Lite()
            {
                // Alloc 4 Mo of memory
                BaseMemory = (byte*)System.Runtime.InteropServices.Marshal.AllocHGlobal(1024 * 1024 * 4).ToPointer();
            }

            public void Set(byte[] Array, ulong Location)
            {
                 for (ulong n = 0; n < (ulong)Array.Length; n++)
                 {
                     BaseMemory[Location + n] = Array[n];
                 }
            }

            [Fact.Emulation.Processor.Instructions.Fetch]
            public byte Fetch(ulong Location)
            {
                return BaseMemory[Location];
            }

            [Fact.Emulation.Processor.Instructions.ExecuteAndReturn]
            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xC3)]
            public void Ret(byte OpCode) { }

            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x40, 0xF8)]
            public void Inc(byte OpCode)
            {
                switch (OpCode)
                {
                    case 0: ax++; break;
                    case 1: cx++; break;
                    case 2: bx++; break;
                    case 3: dx++; break;
                    case 4: sp++; break;
                }
            }

            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x48, 0xF8)]
            public void Dec(byte OpCode)
            {
                switch (OpCode)
                {
                    case 0: ax--; break;
                    case 1: cx--; break;
                    case 2: bx--; break;
                    case 3: dx--; break;
                    case 4: sp--; break;
                }
            }

            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x50, 0xF8)]
            public void Push(byte OpCode)
            {
                switch (OpCode)
                {
                    case 0: *((ushort*)(BaseMemory + sp)) = ax; break;
                    case 1: *((ushort*)(BaseMemory + sp)) = bx; break;
                    //case 2: *((ushort*)(BaseMemory + sp)) = cx; break;
                    case 3: *((ushort*)(BaseMemory + sp)) = dx; break;
                    case 4: *((ushort*)(BaseMemory + sp)) = sp; break;
                }
                sp--;
            }

            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x58, 0xF8)]
            public void Pop(byte OpCode)
            {
                switch (OpCode)
                {
                    case 0: ax = *((ushort*)(BaseMemory + sp)); break;
                    case 1: bx = *((ushort*)(BaseMemory + sp)); break;
                    case 2: cx = *((ushort*)(BaseMemory + sp)); break;
                    case 3: dx = *((ushort*)(BaseMemory + sp)); break;
                    case 4: sp = *((ushort*)(BaseMemory + sp)); break;
                }
                sp++;
            }

            [Fact.Emulation.Processor.Instructions.Parameter1Byte]
            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xB0, 0xFC)]
            public void MovLowIm(byte OpCode, byte im)
            {
                switch (OpCode)
                {
                    case 0: ax = (ushort)((ax & 0xFF00) + im); break;
                    case 1: bx = (ushort)((bx & 0xFF00) + im); break;
                    case 2: cx = (ushort)((cx & 0xFF00) + im); ; break;
                    case 3: dx = (ushort)((dx & 0xFF00) + im); ; break;
                }
            }

            [Fact.Emulation.Processor.Instructions.Parameter1Byte]
            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xB4, 0xFC)]
            public void MovHiIm(byte OpCode, byte im)
            {
                switch (OpCode)
                {
                    case 0: ax = (ushort)((ax & 0x00FF) + (im << 16)); break;
                    case 1: bx = (ushort)((bx & 0x00FF) + (im << 16)); break;
                    //case 2: cx = (ushort)((cx & 0x00FF) + (im << 16)); ; break;
                    case 3: dx = (ushort)((dx & 0x00FF) + (im << 16)); ; break;
                }
            }

            [Fact.Emulation.Processor.Instructions.Parameter2Byte]
            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xB8, 0xF8)]
            public void MovIm(byte OpCode, ushort im)
            {
                switch (OpCode)
                {
                    case 0: ax = im; break;
                    case 1: bx = im; break;
                    case 2: cx = im; break;
                    case 3: dx = im; break;
                    case 4: sp = im; break;
                }
            }

            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0x90)]
            public void Nop(byte OpCode) { }

            [Fact.Emulation.Processor.Instructions.RelativeJump]
            [Fact.Emulation.Processor.Instructions.Parameter2Byte]
            [Fact.Emulation.Processor.Instructions.OpCode1Byte(0xE9)]
            public Int64 Jmp(byte OpCode, ushort offset)
            {
                unchecked
                {
                    short soffset = (short)offset;
                    return soffset;
                }
            }
        }

        _8086Lite _8086;
        Fact.Emulation.Processor.Model _8086LiteModel;
        byte* _Scratchpad = null;

        delegate int test(int a, int b);

        [Fact.Attribute.Test.Setup("Setup")]
        public void Setup()
        {
            _8086 = new _8086Lite();
            _8086LiteModel = new Fact.Emulation.Processor.Model(_8086);
        }

        [Fact.Attribute.Test.Test("Simple Ret")]
        public void SimpleRet()
        {
            _8086.Set(new byte[] { 0xC3 }, 0);
            _8086LiteModel.DynamicExecution(0);
        }

        [Fact.Attribute.Test.Test("Inc ax")]
        public void IncAx()
        {
            ushort oldax = _8086.ax;
            _8086.Set(new byte[] { 0x40, 0xC3 }, 0);
            _8086LiteModel.DynamicExecution(0);
            Fact.Assert.Basic.AreEqual(oldax + 1, _8086.ax);
        }

        [Fact.Attribute.Test.Test("Inc bx")]
        public void IncBx()
        {
            ushort oldbx = _8086.bx;
            _8086.Set(new byte[] { 0x42, 0xC3 }, 0);
            _8086LiteModel.DynamicExecution(0);
            Fact.Assert.Basic.AreEqual(oldbx + 1, _8086.bx);
        }

        [Fact.Attribute.Test.Test("Foward jump")]
        public void ForwardJump()
        {
            ushort oldax = _8086.ax;
            _8086.Set(new byte[] { 0xE9, 0x01, 0x00, 0x40, 0xC3 }, 0);
            _8086LiteModel.DynamicExecution(0);
            // Ax should not have change since we have jump over the instruction
            Fact.Assert.Basic.AreEqual(oldax, _8086.ax);
        }
    }
}

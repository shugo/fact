﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Emulation
{
    public unsafe partial class Processor
    {
        [Fact.Attribute.Test.Group(".NET Emulation")]
        public class DotNetEmulation
        {
            int _intValue = 0;

            public delegate void Test();
            public void TestEmulator(Test test)
            {
                _intValue = 0;
                test();
                int expectedIntValue = _intValue;
                _intValue = 0;
                Fact.Emulation.Execute.Call<int>(test.Method, test.Target, new object[] { });
                Fact.Assert.Basic.AreEqual(expectedIntValue, _intValue);
            }

            void EmptyMethod_Test() { }
            [Fact.Attribute.Test.Test("Empty Method")]
            public void EmptyMethod() { TestEmulator(EmptyMethod_Test); }

            void BasicAdd_Test()
            {
                _intValue = 1;
                for (int n = 0; n < 10; n++) { _intValue++; }
            }
            [Fact.Attribute.Test.Test("Basic Add")]
            public void BasicAdd() { TestEmulator(BasicAdd_Test); }

            void Call_Test()
            {
                _intValue++;
                if (_intValue < 10) { Call_Test(); }
            }
            [Fact.Attribute.Test.Test("Call")]
            public void Call() { TestEmulator(Call_Test); }

            class DummyClass
            {
                internal int _Value = 0;
                public DummyClass() { _Value = 1; }
            }

            void NewObject_Test()
            {
                DummyClass cls = new DummyClass();
                _intValue = cls._Value;
            }
            [Fact.Attribute.Test.Test("New Object")]
            public void NewObject() { TestEmulator(NewObject_Test); }

            void EmulatedAssert_Test()
            {
                int one = 1;
                Fact.Assert.Basic.AreEqual(1, one);
            }
            [Fact.Attribute.Test.Test("Emulated Assert")]
            public void EmulatedAssert() { TestEmulator(EmulatedAssert_Test); }
        }
    }
}

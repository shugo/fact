﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install
{
    static class Tools
    {
        static public string DefaultInstallDir()
        {
            try
            {
                string relativeDirectory = System.AppDomain.CurrentDomain.BaseDirectory;

                if (System.IO.File.Exists(relativeDirectory + "/FactExe.exe"))
                {
                    if (System.IO.Directory.Exists(relativeDirectory + "/../bin"))
                    {
                        if (System.IO.Directory.Exists(relativeDirectory + "/../../bin"))
                        {
                            string fullpath = System.IO.Path.GetFullPath(relativeDirectory + "../..");
                            if (System.IO.File.Exists(fullpath))
                            { return fullpath; }
                        }
                    }
                }
            }
            catch { }
            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                if (System.IO.Directory.Exists(@"C:\Program Files")) { return @"C:\Program Files\Fact"; }
                if (System.IO.Directory.Exists(@"C:\Program Files (x86)")) { return @"C:\Program Files (x86)\Fact"; }
            }
            else if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
            {
                if (System.IO.Directory.Exists(@"/usr/share")) { return @"/usr/share/fact"; }
                if (System.IO.Directory.Exists(@"/usr/bin")) { return @"/usr/bin/fact"; }
                if (System.IO.Directory.Exists(@"/bin")) { return @"/bin/fact"; }
            }
            else if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
            {
                if (System.IO.Directory.Exists(@"/usr/share")) { return @"/usr/share/fact"; }
                if (System.IO.Directory.Exists(@"/usr/bin")) { return @"/usr/bin/fact"; }
                if (System.IO.Directory.Exists(@"/bin")) { return @"/bin/fact"; }
            }
            return "/bin/fact";
        }

        static public void RegisterAssembly(string Name, string Location)
        {
            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                Fact.Log.Verbose("Registering " + Name + " into the Hive");
                // Classic update
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\.NETFramework\AssemblyFolders\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }
                // WOW64 update
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\.NETFramework\AssemblyFolders\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }

                // 3.5 hive
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Microsoft\.NETFramework\v3.5\AssemblyFoldersEx\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\.NETFramework\v3.5\AssemblyFoldersEx\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }
            }
        }

        static public void AddToPath(string Location)
        {
            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                string paths = System.Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Machine);
                string[] binpaths = paths.Split(';');
                List<string> pathtoremove = new List<string>();
                foreach (string binpath in binpaths)
                {
                    if (binpath.ToLower().Trim() == (Location).ToLower().Trim())
                    {
                        Fact.Log.Verbose("The directory " + Location + " is already in the path");
                        return;
                    }
                }
                if (!paths.EndsWith(";")) { paths += ";"; }
                paths += Location;
                System.Environment.SetEnvironmentVariable("PATH", paths, EnvironmentVariableTarget.Machine);
                Fact.Log.Verbose("The directory " + Location + " is now in the path");
            }
        }

        static public void AddToSystemD(string ServiceName, string FactExe)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("[Unit]");
            builder.AppendLine("Description=" + ServiceName);
            builder.AppendLine("");
            builder.AppendLine("[Service]");
            string journalFile = "/var/fact/server/journal_file.fj";
            string credentialFile = "/var/fact/server/admin_credential.fc";

            Fact.Directory.Directory directory = new Fact.Directory.Directory();
            Fact.Directory.User admin = new Fact.Directory.User("admin", "");
            Fact.Directory.Credential credential = new Fact.Directory.Credential(admin, DateTime.MaxValue);
            directory.MainGroup.AddUser(admin);

            if (System.IO.File.Exists(journalFile))
            {
                // Right now nothing to do maybe in the future we want to do something
            }
            else
            {
                Fact.Tools.RecursiveMakeDirectory("/var/fact/server");
                if (System.IO.File.Exists(credentialFile))
                {
                    Fact.Tools.RecursiveDelete(credentialFile);
                }
            }

            if (System.IO.File.Exists(credentialFile))
            {
                // Right now nothing to do
            }
            else
            {
                credential.Save(credentialFile);
            }
            builder.AppendLine("ExecStart=" + FactExe + " servicesserver server --journal " + journalFile);
            builder.AppendLine("[Install]");
            builder.AppendLine("WantedBy=multi-user.target");

        }
    }
}

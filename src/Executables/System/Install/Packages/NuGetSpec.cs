﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install.Packages
{
    class NuGetSpec : PackageFormat
    {
        public override bool CreatePackage(string Input, string Output)
        {
            FactPackage sourcePackage = new FactPackage();
            Fact.Processing.Project source = sourcePackage.CreatePackage(Input);
            if (source != null)
            {
                return CreatePackage(source, Output);
            }
            else
            {
                return false;
            }
        }

        public bool CreatePackage(Fact.Processing.Project Package, string File)
        {
            StringBuilder specfile = new StringBuilder();
            specfile.Append("<?xml version=\"1.0\"?>");
            specfile.Append("<package xmlns=\"http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd\">");
            specfile.Append("<metadata>");
            specfile.Append("<id>"); specfile.Append(Package.Name); specfile.Append("</id>");
            specfile.Append("<version>"); specfile.Append(Package.Name); specfile.Append("</version>");

            specfile.Append("</metadata>");

            specfile.Append("</package>");
            System.IO.File.WriteAllText(File, specfile.ToString());
            return false;
        }
    }
}

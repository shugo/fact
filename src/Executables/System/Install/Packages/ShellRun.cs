﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install.Packages
{
    class ShellRun : PackageFormat
    {
        public ShellRun() { }

        public override bool CreatePackage(string Input, string Output)
        {
            FactPackage sourcePackage = new FactPackage();
            Fact.Processing.Project source = sourcePackage.CreatePackage(Input);
            if (source != null)
            {
                return CreatePackage(source, Output);
            }
            else
            {
                return false;
            }
        }

        public bool CreatePackage(Fact.Processing.Project Package, string File)
        {
            string uuid = Fact.Internal.Information.GenerateUUID().Replace("{", "_").Replace("}", "_").Replace("-", "_");
            System.IO.Stream file = System.IO.File.Open(File, System.IO.FileMode.OpenOrCreate);

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("#! /usr/bin/env sh");
            builder.AppendLine("echo '[VERB] Checking mono version ...'");
            builder.AppendLine("mono_version=$(mono --version)");
            builder.AppendLine("self=$0");
            
            builder.AppendLine("echo '[VERB] Extracting fact install binaries ...'");
            builder.AppendLine("rm -rf /tmp/fact/instal/" + uuid + "/ 2> /dev/null");
            builder.AppendLine("mkdir -p /tmp/fact/instal/" + uuid + "/");
            builder.AppendLine("mkdir -p /tmp/fact/instal/" + uuid + "/System");

            System.IO.MemoryStream memstream = new System.IO.MemoryStream();
            Package.Save(memstream);
            byte[] packageData = memstream.ToArray();
            byte[] fact = null;
            byte[] factexe = null;
            byte[] factinstall = null;

            string factexeLoc = "fact";

            List<Fact.Processing.File> binFiles = Package.GetFiles("/bin", true);
            List<Fact.Processing.File> systembinFiles = Package.GetFiles("/bin/system", true);
            if (systembinFiles == null || systembinFiles.Count == 0) { systembinFiles = Package.GetFiles("/bin/System", false); }


            Fact.Processing.File localFactExe = null;
            Fact.Processing.File localFactLib = null;
            Fact.Processing.File localFactInstall = null;


            if (binFiles != null)
            {
                foreach (Fact.Processing.File binFile in binFiles)
                {
                    if (binFile == null) { continue; }
                    Fact.Processing.File _WAR_File = binFile;
                    if (_WAR_File.Name.ToLower() == "factexe.exe" || _WAR_File.Name.ToLower() == "factexe")
                    { localFactExe = _WAR_File; }
                    if (_WAR_File.Name.ToLower() == "fact.dll")
                    { localFactLib = _WAR_File; }
                }
            }

            if (systembinFiles != null)
            {
                foreach (Fact.Processing.File binFile in systembinFiles)
                {
                    if (binFile == null) { continue; }
                    Fact.Processing.File _WAR_File = binFile;
                    if (_WAR_File.Name.ToLower() == "install.dll")
                    { localFactInstall = _WAR_File; }
                }
            }

            builder.AppendLine("echo '[VERB] Extracting package ...'");
            builder.AppendLine("tail --bytes " + packageData.Length + " $self > /tmp/fact/instal/" + uuid + "/package.ff");

            if (localFactExe != null)
            {
                factexe = localFactExe.GetBytes();
                builder.AppendLine("echo '[VERB] Extracting fact ...'");
                int chunkSize = 0;
                if (factexe != null) { chunkSize += factexe.Length; }
                if (packageData != null) { chunkSize += packageData.Length; }
                builder.AppendLine("tail --bytes " + chunkSize.ToString() + " $self | head --bytes " + (factexe.Length).ToString() + " > /tmp/fact/instal/" + uuid + "/FactExe.exe");
                factexeLoc = "/tmp/fact/instal/" + uuid + "/FactExe.exe";
            }

            if (localFactLib != null)
            {
                fact = localFactLib.GetBytes();
                builder.AppendLine("echo '[VERB] Extracting fact core ...'");
                int chunkSize = 0;
                if (fact != null) { chunkSize += fact.Length; }
                if (factexe != null) { chunkSize += factexe.Length; }
                if (packageData != null) { chunkSize += packageData.Length; }
                builder.AppendLine("tail --bytes " + chunkSize.ToString() + " $self | head --bytes " + (fact.Length).ToString() + " > /tmp/fact/instal/" + uuid + "/Fact.dll");
            }

            if (localFactInstall != null)
            {
                factinstall = localFactInstall.GetBytes();
                builder.AppendLine("echo '[VERB] Extracting fact install plugin ...'");
                int chunkSize = 0;
                if (factinstall != null) { chunkSize += factinstall.Length; }
                if (fact != null) { chunkSize += factinstall.Length; }
                if (factexe != null) { chunkSize += factexe.Length; }
                if (packageData != null) { chunkSize += packageData.Length; }
                builder.AppendLine("tail --bytes " + chunkSize.ToString() + " $self | head --bytes " + (factinstall.Length).ToString() + " > /tmp/fact/instal/" + uuid + "/System/Install.dll");
            }

            builder.AppendLine("mono " + factexeLoc + " System Install --package /tmp/fact/instal/" + uuid + "/package.ff");
            builder.AppendLine("rm -rf /tmp/fact/instal/" + uuid + "/");
            builder.AppendLine("exit");

            byte[] scriptData = System.Text.Encoding.UTF8.GetBytes(builder.ToString());

            file.Write(scriptData, 0, scriptData.Length);
            if (factinstall != null) { file.Write(factinstall, 0, factinstall.Length); }
            if (fact != null) { file.Write(fact, 0, fact.Length); }
            if (factexe != null) { file.Write(factexe, 0, factexe.Length); }
            file.Write(packageData, 0, packageData.Length);
            file.Close();
            Fact.Tools.AddExecutionPermission(File);
            return true;
        }
    }
}

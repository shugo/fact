﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install.Packages
{
    class DebianPackage : PackageFormat
    {
        Fact.Processing.Project GenerateControl() { return null; }
        Fact.Processing.Project GenerateData() { return null; }

        public override bool CreatePackage(string Input, string Output)
        {
            Fact.Processing.File debian_binary = new Fact.Processing.File(new string[] { "2.0" } );
            
            return base.CreatePackage(Input, Output);
        }
    }
}

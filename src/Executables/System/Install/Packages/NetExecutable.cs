﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install.Packages
{
    class NetExecutable : PackageFormat
    {
        public NetExecutable() { }

        public override bool CreatePackage(string Input, string Output)
        {
            FactPackage sourcePackage = new FactPackage();
            Fact.Processing.Project source = sourcePackage.CreatePackage(Input);
            if (source != null)
            {
                return GenerateInstallerExe(source, Output);
            }
            else
            {
                return false;
            }
        }

        void WriteLine(System.Reflection.Emit.ILGenerator Generator, string Message)
        {
#if DEBUG
            Fact.Log.Debug("Generating WriteLine: " + Message);
#endif
            Generator.Emit(System.Reflection.Emit.OpCodes.Ldstr, Message);
            Generator.EmitCall(System.Reflection.Emit.OpCodes.Call, typeof(System.Console).GetMethod("WriteLine", new Type[] { typeof(string) }), null);
        }

        void CreateTempDirectory(System.Reflection.Emit.ILGenerator Generator)
        {
            Generator.EmitCall(System.Reflection.Emit.OpCodes.Call, typeof(System.IO.Path).GetMethod("GetTempPath", new Type[] { }), null);
            Generator.EmitCall(System.Reflection.Emit.OpCodes.Call, typeof(System.IO.Path).GetMethod("GetRandomFileName", new Type[] { }), null);
            Generator.EmitCall(System.Reflection.Emit.OpCodes.Call, typeof(System.IO.Path).GetMethod("Combine", new Type[] { typeof(string), typeof(string) }), null);
            Generator.Emit(System.Reflection.Emit.OpCodes.Dup);
            Generator.EmitCall(System.Reflection.Emit.OpCodes.Call, typeof(System.IO.Directory).GetMethod("CreateDirectory", new Type[] { typeof(string) }), null);
        }

        void ExtractFactPackage(System.Reflection.Emit.ILGenerator Generator, Fact.Processing.Project sourcePackage)
        {
            // FIX ME
        }

        void LaunchExecutable(System.Reflection.Emit.ILGenerator Generator, string Executable, string CommandLine)
        {
            // FIX ME
        }

        bool GenerateInstallerExe(Fact.Processing.Project sourcePackage, string Output)
        {
            if (!Output.Contains("/") & !Output.Contains("\\")) { Output = "./" + Output; }
            string filename = System.IO.Path.GetFileName(Output);
            string directory = System.IO.Path.GetFullPath(System.IO.Path.GetDirectoryName(Output));

            System.Reflection.AssemblyName name = new System.Reflection.AssemblyName(filename);


            List<Fact.Processing.File> binFiles = sourcePackage.GetFiles("/bin", true);
            List<Fact.Processing.File> systembinFiles = sourcePackage.GetFiles("/bin/system", true);
            if (systembinFiles == null || systembinFiles.Count == 0) { systembinFiles = sourcePackage.GetFiles("/bin/System", false); }



            System.Reflection.Emit.AssemblyBuilder assembly = System.AppDomain.CurrentDomain.DefineDynamicAssembly(name, System.Reflection.Emit.AssemblyBuilderAccess.Save, directory);
            System.Reflection.Emit.ModuleBuilder installerModule = assembly.DefineDynamicModule(filename);
            System.Reflection.Emit.TypeBuilder installerType = installerModule.DefineType("factInstallerEntryPoint", System.Reflection.TypeAttributes.Class | System.Reflection.TypeAttributes.Public | System.Reflection.TypeAttributes.Sealed);


            Fact.Processing.File localFactExe = null;
            Fact.Processing.File localFactLib = null;
            Fact.Processing.File localFactInstall = null;


            if (binFiles != null)
            {
                foreach (Fact.Processing.File binFile in binFiles)
                {
                    if (binFile == null) { continue; }
                    Fact.Processing.File _WAR_File = binFile;
                    if (_WAR_File.Name.ToLower() == "factexe.exe" || _WAR_File.Name.ToLower() == "factexe")
                    { localFactExe = _WAR_File; }
                    if (_WAR_File.Name.ToLower() == "fact.dll")
                    { localFactLib = _WAR_File; }
                }
            }

            System.Reflection.FieldInfo field = installerModule.DefineInitializedData("factexe", localFactExe.GetBytes(), System.Reflection.FieldAttributes.Public | System.Reflection.FieldAttributes.Static);


            System.Reflection.Emit.MethodBuilder entryPoint = installerType.DefineMethod("main", System.Reflection.MethodAttributes.Public | System.Reflection.MethodAttributes.Static | System.Reflection.MethodAttributes.HideBySig, typeof(int), new Type[] { typeof(string[]) });
            System.Reflection.Emit.ILGenerator entryPointCode = entryPoint.GetILGenerator();


            WriteLine(entryPointCode, "[INFO] Loading fact " + Fact.Internal.Information.FactVersionName() + " installer bootstrap ...");
            WriteLine(entryPointCode, "[INFO] Creating temp directory ...");
            CreateTempDirectory(entryPointCode);
            WriteLine(entryPointCode, "[INFO] Extracting fact package ...");
            ExtractFactPackage(entryPointCode, sourcePackage);
            WriteLine(entryPointCode, "[INFO] Creating installation package from extracted package ...");
            LaunchExecutable(entryPointCode, "./FactExe.exe", "system install --create-package 'this' --output './self-installer.ff'");
            WriteLine(entryPointCode, "[INFO] Launching fact installer ...");
            LaunchExecutable(entryPointCode, "./FactExe.exe", "system install --package './self-installer.ff'");

            entryPointCode.Emit(System.Reflection.Emit.OpCodes.Pop);


            entryPointCode.Emit(System.Reflection.Emit.OpCodes.Ldc_I4_0);
            entryPointCode.Emit(System.Reflection.Emit.OpCodes.Ret);
            installerType.CreateType();
            assembly.SetEntryPoint(entryPoint, System.Reflection.Emit.PEFileKinds.ConsoleApplication);

            assembly.Save(filename);
            return true;
        }

    }
}

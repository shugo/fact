﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install.Packages
{
    class AURPackage : PackageFormat
    {
        public string GeneratePKGBUILD(string PackageURL, string Name, Version Version, string Description, string URL)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("pkgname=" + Name);
            builder.AppendLine("pkgver=" + Version.Major + "." + Version.Minor + "." + Version.Build);
            builder.AppendLine("pkgdesc=" + Version.Major);
            builder.AppendLine("pkgdesc='" + Description + "'");
            builder.AppendLine("url='" + URL + "'");

            builder.AppendLine("source=(" + PackageURL + ")");

            builder.AppendLine("prepare() {");
            builder.AppendLine("}");

            builder.AppendLine("build() {");
            builder.AppendLine("}");

            builder.AppendLine("package {");
            builder.AppendLine("}");

            return builder.ToString();
        }
    }
}

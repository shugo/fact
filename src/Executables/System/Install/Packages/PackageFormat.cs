﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install.Packages
{
    abstract class PackageFormat
    {
        public virtual bool InstallPackage(string TargetPackage, string Location, bool Force)
        {
            throw new Exception("Installation of these kind of package is not supported");
        }

        public virtual bool CreatePackage(string Input, string Output)
        {
            throw new Exception("Installation of these kind of package is not supported");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install.Packages
{
    class FactPackage : PackageFormat
    {
        public override bool InstallPackage(string TargetPackage, string Location, bool Force)
        {
            string rootpackage = Location;
            Fact.Processing.Project packageLoaded = new Fact.Processing.Project("");
            if (TargetPackage.ToLower().StartsWith("http://"))
            {
                Fact.Log.Verbose("Downloading '" + TargetPackage + "' ...");
                try
                {
                    byte[] data = Fact.Network.Tools.HttpGetRequestRawResult(TargetPackage, -1);
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Impossible to download the package: '" + TargetPackage + "'");
                    return false;
                }
            }
            else
            {
                if (!System.IO.File.Exists(TargetPackage))
                {
                    Fact.Log.Error("Missing package " + TargetPackage);
                    return false;
                }
                Fact.Log.Verbose("Checking the package");
                try
                {
                    packageLoaded = Fact.Processing.Project.Load(TargetPackage);
                }
                catch
                {
                    Fact.Log.Error("This package is not compatible with this version of fact");
                    return false;
                }
            }


            string package_hash = packageLoaded.Hash;
            if (System.IO.File.Exists(Location + "/info/packages/" + package_hash))
            {
                Fact.Log.Warning("This package has already been deployed on this system");
                if (!Force)
                {
                    Fact.Log.Warning("Nothing to do");
                    return true;
                }
                else
                {
                    Fact.Log.Warning("This package will be reinstalled");
                }
            }

            List<Fact.Processing.File> _binFiles = packageLoaded.GetFiles("/bin", true);
            List<Fact.Processing.File> _servicesFiles = packageLoaded.GetFiles("/services", true);

            List<KeyValuePair<string, string>> _InstalledFiles = new List<KeyValuePair<string, string>>();
            // Installing files
            if (_binFiles.Count > 0)
            {
                if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
                {
                    Tools.AddToPath(Location + "/bin");
                }
            }
            Fact.Log.Verbose("Installing " + packageLoaded.Name);

            if (_binFiles.Count > 0)
            {
                Fact.Log.Verbose("Installing binary files");
                foreach (Fact.Processing.File file in _binFiles)
                {
                    string relativepath = file.Directory.Substring("/bin".Length);
                    if (!System.IO.Directory.Exists(Location + "/bin")) { Fact.Tools.RecursiveMakeDirectory(Location + "/bin"); }
                    string path = Location + "/bin/" + relativepath;
                    if (System.IO.File.Exists(path))
                    {
                        Fact.Log.Verbose("Removing existing file " + path);
                    }
                    if (relativepath.ToLower() == "factexe.exe" || relativepath.ToLower() == "/factexe.exe")
                    {
                        Fact.Log.Verbose("Installing a new vesion of FactExe");
                        Fact.Log.Verbose("Installing file " + path);
                        file.Extract(path);
                        _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, path));
                        Fact.Log.Verbose("Creating short version file " + Location + "/bin/fact");
                        if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
                        {
                            try
                            {
                                if (System.IO.File.Exists(Location + "/bin/fact.exe"))
                                {
                                    System.IO.File.Delete(Location + "/bin/fact.exe");
                                }
                            }
                            catch { Fact.Log.Error("Impossinle to update the short version of " + Location + "/bin/fact.exe"); }
                            System.IO.File.Copy(path, Location + "/bin/fact.exe");
                            _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, Location + "/bin/fact.exe"));
                        }
                        else
                        {
                            if (System.IO.File.Exists(Location + "/bin/fact"))
                            {
                                System.IO.File.Delete(Location + "/bin/fact");
                            }
                            System.IO.File.Copy(path, Location + "/bin/fact");
                            string bin = "";
                            if (System.IO.Directory.Exists("/usr/bin")) { bin = "/usr/bin/"; }
                            else if (System.IO.Directory.Exists("/bin")) { bin = "/bin/"; }

                            if (bin != "")
                            {
                                string mono = Fact.Internal.Information.GetMonoBinary();
                                if (System.IO.File.Exists(mono))
                                {
                                    mono = System.IO.Path.GetFullPath(mono);
                                    Fact.Log.Verbose("Creating launch script in the bin folder");
                                    try
                                    {
                                        if (System.IO.File.Exists(bin + "/fact")) { System.IO.File.Delete(bin + "/fact"); }
                                        StringBuilder launchscrip = new StringBuilder();
                                        launchscrip.AppendLine("#! /usr/bin/env sh");
                                        launchscrip.AppendLine(mono + " --runtime=\"v4.0\" " + Location + "/bin/fact" + " $@ ");
                                        System.IO.File.WriteAllText(bin + "/fact", launchscrip.ToString());
                                    }
                                    catch { }
                                    try { Fact.Tools.AddExecutionPermission(bin + "/fact"); }
                                    catch { }
                                }
                                _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, Location + "/bin/fact"));
                            }
                        }
                    }
                    else if (relativepath.ToLower() == "fact.dll" || relativepath.ToLower() == "/fact.dll")
                    {
                        Fact.Log.Verbose("Installing a new vesion of Fact Core");
                        Fact.Log.Verbose("Installing file " + path);
                        file.Extract(path);
                        _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, path));
                        Tools.RegisterAssembly("Fact", Location + "/bin");
                    }
                    else
                    {
                        Fact.Log.Verbose("Installing file " + path);
                        file.Extract(path);
                        _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, path));
                    }

                }
            }
            if (_servicesFiles.Count > 0)
            {
                Fact.Log.Verbose("Installing services files");
                foreach (Fact.Processing.File file in _servicesFiles)
                {
                    string relativepath = file.Directory.Substring("/services".Length);
                    if (!System.IO.Directory.Exists(Location + "/services")) { Fact.Tools.RecursiveMakeDirectory(Location + "/services"); }
                    string path = Location + "/services/" + relativepath;
                    string name = "";
                    if (System.IO.File.Exists(path))
                    {
                        Fact.Log.Verbose("Removing existing service " + path);
                    }

                    Fact.Log.Verbose("Installing service " + path);
                    file.Extract(path);

                    Fact.Log.Verbose("Registering service " + path);
                    if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
                    {
                        // Looking for systemd
                        if (System.IO.Directory.Exists("/etc/systemd/system"))
                        {
                            StringBuilder builder = new StringBuilder();
                            builder.AppendLine("[Unit]");
                            builder.AppendLine("Description=" + name);
                            builder.AppendLine("After=fact-server.service");
                            builder.AppendLine("Requires=fact-server.service");
                            builder.AppendLine("");
                            builder.AppendLine("[Service]");
                            builder.AppendLine("TimeoutStartSec=0");
                            builder.AppendLine("ExecStart=/usr/bin/fact server administration --action 'loadservice' --service " + path + " --server localhost");
                        }
                    }
                }
            }

            Fact.Log.Verbose("Updating the information file");
            Fact.Tools.RecursiveMakeDirectory(rootpackage + "/info/packages");
            Package package = new Package(rootpackage + "/info/packages/" + packageLoaded.Hash);
            package.Name = packageLoaded.Name;
            foreach (KeyValuePair<string, string> file in _InstalledFiles)
            {
                package.AddFile(file.Key, file.Value);
            }
            package.Save();

            return true;
        }

        string GetFactPackageName()
        {
            return "Fact-" +
                    Fact.Internal.Information.FactVersion().Major + "-" +
                    Fact.Internal.Information.FactVersion().Minor + "-" +
                    Fact.Internal.Information.FactVersion().Build;
        }

        void EnumerateFiles(string Directory, List<string> Files)
        {
            foreach (string file in System.IO.Directory.GetFiles(Directory))
            {
                string full = System.IO.Path.GetFullPath(file);
                if (file.EndsWith(".dll") || file.EndsWith(".exe"))
                {
                    if (file.EndsWith(".vshost.exe")) { continue; }
                    Files.Add(full);
                }
            }
            foreach (string directory in System.IO.Directory.GetDirectories(Directory))
            {
                EnumerateFiles(directory, Files);
            }
        }

        public Fact.Processing.Project CreatePackage(string Input)
        {
            try
            {
                Fact.Processing.Project project = new Fact.Processing.Project(GetFactPackageName());
                List<string> files = new List<string>();
                EnumerateFiles(Input, files);
                foreach (string file in files)
                {
                    Fact.Processing.File factFile = new Fact.Processing.File(System.IO.File.ReadAllBytes(file), System.IO.Path.GetFileName(file));
                    string dir = System.IO.Path.GetDirectoryName(file);
                    dir = file.Substring(Input.Length);
                    if (dir.StartsWith("/")) { dir = dir.Substring(1); }
                    project.AddFile("/bin/" + dir, factFile);
                }
                return project;
            }
            catch (Exception e)
            {
#if DEBUG
                Fact.Log.Exception(e);
#endif
                Fact.Log.Error("Unexpected error while creating the package");
                return null;
            }
        }

        public override bool CreatePackage(string Input, string Output)
        {
            Fact.Log.Verbose("Creating the package");

            if (Input.ToLower().StartsWith("http://"))
            {
                Fact.Log.Verbose("Downloading '" + Input + "' ...");
                try
                {
                    Fact.Network.Tools.HttpGetRequestRawResult(Input, -1);
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Impossible to download the package: '" + Input + "'");
                    return false;
                }
            }

            Fact.Processing.Project project = CreatePackage(Input);
            if (project != null)
            {
                Fact.Log.Verbose("Saving the distribution package");
                try
                {
                    project.Save(Output, true);
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Unexpected error while saving the package: " + e.ToString());
                    return false;
                }
            }
            return true;
        }
    }
}

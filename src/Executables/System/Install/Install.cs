﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install
{
    class Install : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Install";
            }
        }

        void AddToPathWindows(string Path, EnvironmentVariableTarget Target)
        {
            string paths = System.Environment.GetEnvironmentVariable("PATH", Target);
            string[] binpaths = paths.Split(';');

        }

        void AddToPath(string Path)
        {
            if (Fact.Internal.Information.CurrentOperatingSystem() ==
                Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                AddToPathWindows(Path, EnvironmentVariableTarget.Machine);
                AddToPathWindows(Path, EnvironmentVariableTarget.User);
                AddToPathWindows(Path, EnvironmentVariableTarget.Process);
            }
        }

        Dictionary<string, Package> LoadPackages(string Location)
        {
            if (!System.IO.Directory.Exists(Location + "/info/packages"))
            {
                return new Dictionary<string,Package>();
            }

            Dictionary<string, Package> _packages = new Dictionary<string, Package>();
            foreach (string file in System.IO.Directory.GetFiles(Location + "/info/packages"))
            {
                Package package = new Package(file);
                if (!_packages.ContainsKey(package.Name))
                {
                    _packages.Add(package.Name, package);
                }
            }
            return _packages;
        }
        bool DeletePackage(string TargetPackage, string Location)
        {
            try
            {
                Dictionary<string, Package> packages = LoadPackages(Location);
                if (packages.ContainsKey(TargetPackage))
                {
                    Fact.Log.Verbose("Removing the package: " + TargetPackage);
                    foreach (KeyValuePair<string, string> file in packages[TargetPackage].GetFiles())
                    {
                        Fact.Log.Verbose("Deleting file: " + file.Value);
                        Fact.Tools.RecursiveDelete(file.Value);
                    }
                    Fact.Tools.RecursiveDelete(packages[TargetPackage].File);
                    return true;
                }
                else
                {
                    Fact.Log.Warning("The package " + TargetPackage + " is not installed and will not be removed");
                    return true;
                }
            }
            catch
            {
                Fact.Log.Error("Impossible to delete the package: " + TargetPackage);
                return false;
            }
        }

        void ListPackages(string Location)
        {
            try
            {
                Dictionary<string, Package> packages = LoadPackages(Location);
                foreach (string pck in packages.Keys)
                {
                    Fact.Log.Verbose("Package: " + pck);
                }
            }
            catch { }
        }

        Dictionary<string, Packages.PackageFormat> _PackageFormat = new Dictionary<string, Packages.PackageFormat>();

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();

            CommandLine.AddStringInput("fact-location", "set location of fact (the default one is usually good)", Tools.DefaultInstallDir());
            CommandLine.AddStringInput("package", "the package that should be installed", ""); CommandLine.AddShortInput("p", "package");
            CommandLine.AddStringInput("package-format", "specify a custom package format instead of fact package", "fact-package");

            CommandLine.AddStringInput("credential", "the credential that should be installed", ""); CommandLine.AddShortInput("c", "credential");

            CommandLine.AddStringInput("output", "the output filename when the tool is used to generate a file instead of installing one", ""); CommandLine.AddShortInput("o", "output");

            CommandLine.AddStringInput("create-package", "Create package that can be distributed (use 'this' as arguement to create a package for the current instance of fact)", "");
            CommandLine.AddBooleanInput("remove", "Remove the package instead of installing it", false);
            CommandLine.AddBooleanInput("list", "List all the package installed on this system", false); CommandLine.AddShortInput("l", "list");
            CommandLine.AddBooleanInput("force", "force the instalation of an already installed package", false); CommandLine.AddShortInput("f", "force");
            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");

            _PackageFormat = new Dictionary<string, Packages.PackageFormat>();
            _PackageFormat.Add("fact-package", new Packages.FactPackage());
            _PackageFormat.Add(".net-executable", new Packages.NetExecutable());
            _PackageFormat.Add("debian-package", new Packages.DebianPackage());
            _PackageFormat.Add("aur-package", new Packages.AURPackage());
            _PackageFormat.Add("shell-script", new Packages.ShellRun());


            if (!CommandLine.Parse(args))
            {
                CommandLine.PrintErrors();
                return 1;
            }
            if (CommandLine["help"].AsBoolean())
            {
                CommandLine.PrintUsage();
                return 0;
            }
            string packageFormatString = CommandLine["package-format"].AsString().ToLower().Trim();
            if (!_PackageFormat.ContainsKey(packageFormatString))
            {
                Fact.Log.Error("Format " + packageFormatString + " not supported");
                Fact.Log.Error("Please use one of the following format: ");
                foreach (string format in _PackageFormat.Keys)
                {
                    Fact.Log.Error("* " + format);

                }
                return 1;
            }

            Packages.PackageFormat packageFormat = _PackageFormat[packageFormatString];

            string createPackage = CommandLine["create-package"].AsString();
            if (createPackage != "")
            {
                string relativeDirectory = "";
                if (createPackage == "this") { relativeDirectory = System.AppDomain.CurrentDomain.BaseDirectory; }
                else { relativeDirectory = createPackage; }

                if (!relativeDirectory.ToLower().StartsWith("http://") && !System.IO.Directory.Exists(relativeDirectory))
                {
                    Fact.Log.Error("The specified directory " + relativeDirectory + " does not exist");
                    return 1;
                }
                if (CommandLine["output"].IsDefault)
                {
                    Fact.Log.Error("the option output must be use to specify the output package location");
                    return 1;
                }
                relativeDirectory = System.IO.Path.GetFullPath(relativeDirectory);

                try
                {
                    if (packageFormat.CreatePackage(relativeDirectory, CommandLine["output"].AsString()))
                    {
                        return 0;
                    }
                    else
                    {
                        Fact.Log.Error("Errors during package generation.");
                        return 1;
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Unexpected error while saving the package: " + e.Message);
                    return 1;
                }
            }
            string location = CommandLine["fact-location"].AsString();
            string package = CommandLine["package"].AsString();
            string credential = CommandLine["credential"].AsString();

            bool force = CommandLine["force"].AsBoolean();
            if (location == "")
            {
                Fact.Log.Error("Invalid installation directory");
                return 1;
            }
            if (credential != "" && package != "")
            {
                Fact.Log.Error("Impossible to install a package and a credential at once");
                return 1;
            }

            if (credential != "")
            {
                Fact.Log.Verbose("Creating the credential folders");
                string creddir = Fact.Internal.Information.GetSpecialFolders(Fact.Internal.Information.SpecialFolder.FactCredentials);
                Fact.Tools.RecursiveMakeDirectory(creddir);
                if (!System.IO.Directory.Exists(creddir))
                {
                    Fact.Log.Error("The credential directory '" + creddir + "' does not exist");
                    return 1;
                }
                try
                {
                    Fact.Directory.Credential cred = new Fact.Directory.Credential(credential);
                    System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512.Create();
                    byte[] hash = sha512.ComputeHash(System.IO.File.ReadAllBytes(credential));
                    string hashstr = "";
                    for (int n = 0; n < hash.Length; n++) { hashstr += hash[n].ToString("X2"); }
                    if (System.IO.File.Exists(creddir + "/" + hashstr + ".fc"))
                    {
                        Fact.Log.Error("The credential has already been installed");
                        return 1;
                    }
                    cred.Save(creddir + "/" + hashstr + ".fc");
                    Fact.Log.Verbose("The credential has been correctly installed at: " + creddir + "/" + hashstr + ".fc");
                    return 0;
                }
                catch (Exception e)
                {
                    Fact.Log.Error("Impossible to install credential file: " + e.Message);
                }
            }

            Fact.Log.Verbose("Creating the folders");
            string rootlocation = location;
            Fact.Tools.RecursiveMakeDirectory(location);
            Fact.Tools.RecursiveMakeDirectory(location + "/bin");
            Fact.Tools.RecursiveMakeDirectory(location + "/info");

            Fact.Log.Verbose("Checking folders");
            if (!System.IO.Directory.Exists(location) ||
                !System.IO.Directory.Exists(location + "/bin") ||
                !System.IO.Directory.Exists(location + "/info"))
            {
                Fact.Log.Error("Impossible to create the installation folders in " + location);
                Fact.Log.Error("Check the permission and try to run this program with higher priviledges");
                return 1;
            }

            if (CommandLine["remove"].AsBoolean())
            {
                 if (!DeletePackage(package, location))
                 {
                     return 1;
                 }
                 return 0;
            }
            else if (CommandLine["list"].AsBoolean())
            {
                ListPackages(location);
                return 0;
            }
            if (!packageFormat.InstallPackage(package, location, force))
            {
                return 1;
            }
            
            return 0;
        }
    } 
}

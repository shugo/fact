﻿/*                                                                           *
 * Copyright © 2015, Raphaël Boissel, Jean-Luc Bounthong, Antoine Lecubin    *
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel,        *
 *   Jean-Luc Bounthong or Antoine Lecubin shall not be used in advertising  *
 *   or otherwise to promote the sale, use or other dealings in this         *
 *   Software without prior written authorization from Raphaël Boissel,      *
 *   Jean-Luc Bounthong or Antoine Lecubin.                                  *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Scripting;
using IronPython;

namespace Python
{
    public class PythonCollector : Fact.Common.Tests.Collector
    {
        System.IO.FileInfo scriptFileInfo;
        Microsoft.Scripting.Hosting.ScriptEngine engine;
        Microsoft.Scripting.Hosting.ScriptSource source;
        Microsoft.Scripting.Hosting.ScriptScope scope;

        Dictionary<string, string> _Parameters;

        private enum TestType
        {
            Test,
            Setup,
            Teardown,
            Group,
        }

        public PythonCollector(Fact.Processing.Project Project)
            : base(Project)
        {
            // Python runtime init
            engine = IronPython.Hosting.Python.CreateEngine();
            engine.Runtime.LoadAssembly(typeof (Fact.Test.Result.Result).Assembly);
        }

        public override bool CanCollect(System.IO.FileInfo File)
        {
            return File.Extension.ToLower() == ".py" || File.Extension.ToLower() == "py";
        }

       public override void Load(System.IO.FileInfo File, Dictionary<string, string> Parameters)
       {
           scriptFileInfo = File;
           source = engine.CreateScriptSourceFromFile(scriptFileInfo.FullName);
           scope = engine.CreateScope();
           _Parameters = Parameters;
       }

        public override void Setup()
        {
            List<IronPython.Runtime.PythonFunction> testFunctions = new List<IronPython.Runtime.PythonFunction>();
            object setattr = IronPython.Hosting.Python.GetBuiltinModule(engine).GetVariable("setattr");
            object getattr = IronPython.Hosting.Python.GetBuiltinModule(engine).GetVariable("getattr");
            object hasattr = IronPython.Hosting.Python.GetBuiltinModule(engine).GetVariable("hasattr");

            // Store the test or group name as an attribute (fact_X_name) and the type of test in fact_test_type
            object testLoader = GenerateTestDecorator(testFunctions, TestType.Test);
            object setupLoader = GenerateTestDecorator(testFunctions, TestType.Setup);
            object teardownLoader = GenerateTestDecorator(testFunctions, TestType.Teardown);
            object groupLoader = (Func<string, Func<object, object>>)
                                 (groupName =>
                                 {
                                     return (Func<object, object>)
                                            (obj =>
                                            {
                                                IronPython.Runtime.PythonDictionary dict = (IronPython.Runtime.PythonDictionary)IronPython.Runtime.Operations.PythonCalls.Call(getattr, obj, "__dict__");
                                                foreach (KeyValuePair<object, object> kv in dict)
                                                {
                                                    try
                                                    {
                                                        IronPython.Runtime.Operations.PythonCalls.Call(setattr, kv.Value, "___fact___internal___group_name", groupName);
                                                    }
                                                    catch (Exception e) {}
                                                }
                                                return obj;
                                            });
                                 });

            // Put our decorators in the scope
            scope.SetVariable("FactTest", testLoader);
            scope.SetVariable("FactSetup", setupLoader);
            scope.SetVariable("FactTeardown", teardownLoader);
            scope.SetVariable("FactGroup", groupLoader);

            // Execute the test script while loading
            try
            {
                source.Execute(scope);
            }
            catch (Exception e)
            {
                if (e is Microsoft.Scripting.SyntaxErrorException ||
                    e is IronPython.Runtime.UnboundNameException)
                {
                    Fact.Log.Info(engine.GetService<Microsoft.Scripting.Hosting.ExceptionOperations>().FormatException(e));
                    return;
                }
                throw;
            }

            foreach (IronPython.Runtime.PythonFunction pythonFunction in testFunctions)
            {
                // Retrieve the added attributes to call Add{Test,Setup,Teardown}() with the right parameters
                Fact.Common.Tests.TestProject test = new Fact.Common.Tests.TestProject(Py2FactTest(pythonFunction), null, "");
                string name = IronPython.Runtime.Operations.PythonCalls.Call(getattr, pythonFunction, "___fact___internal___test_name").ToString();
                test._name = name;
                TestType type = (TestType)IronPython.Runtime.Operations.PythonCalls.Call(getattr, pythonFunction, "___fact___internal___test_type");
                bool hasGroup = (bool)IronPython.Runtime.Operations.PythonCalls.Call(hasattr, pythonFunction, "___fact___internal___group_name");
                string group = hasGroup ? (string)IronPython.Runtime.Operations.PythonCalls.Call(getattr, pythonFunction, "___fact___internal___group_name") : "";
                switch (type)
                {
                case TestType.Test:
                   AddTest(group, name, test); break;
                case TestType.Setup:
                    AddSetup(group, name, test); break;
                case TestType.Teardown:
                    AddTeardown(group, name, test); break;
                default:
                    Fact.Log.Error("Unknown test type for " + name); break;
                }
            }
        }

        private Func<Fact.Processing.Project, Fact.Test.Result.Result> Py2FactTest(
            IronPython.Runtime.PythonFunction pythonTest)
        {
            return (stdProj => Fact.Test.Result.Result.Cast(engine.Operations.Invoke(pythonTest, stdProj)));
        }


        private Func<string, Func<IronPython.Runtime.PythonFunction, IronPython.Runtime.PythonFunction>>
          GenerateTestDecorator(List<IronPython.Runtime.PythonFunction> testFunctions, TestType type)
        {
            object setattr = IronPython.Hosting.Python.GetBuiltinModule(engine).GetVariable("setattr");
            object getattr = IronPython.Hosting.Python.GetBuiltinModule(engine).GetVariable("getattr");
            object hasattr = IronPython.Hosting.Python.GetBuiltinModule(engine).GetVariable("hasattr");

            return (Func<string, Func<IronPython.Runtime.PythonFunction, IronPython.Runtime.PythonFunction>>)
                   (testName =>
                   {
                       return (Func<IronPython.Runtime.PythonFunction, IronPython.Runtime.PythonFunction>)
                              (fun =>
                              {
                                  IronPython.Runtime.Operations.PythonCalls.Call(setattr, fun, "___fact___internal___test_name", testName);
                                  IronPython.Runtime.Operations.PythonCalls.Call(setattr, fun, "___fact___internal___test_type", type);
                                  testFunctions.Add(fun);
                                  return fun;
                              });
                   });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace run
{
    class DNetPatcher
    {
        static HashSet<string> _AssemblyUID = new HashSet<string>();
        static int _MagickToken = 0;
        public static int MagickInt()
        {
            lock (_AssemblyUID)
            {
                return _MagickToken++;
            }
        }

        static System.Reflection.BindingFlags _BindingFlags = System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance; 
        class AssemblyPatcher
        {
            internal System.Reflection.Assembly _OriginalAssembly = null;
            internal System.Reflection.Emit.AssemblyBuilder _AssemblyBuilder = null;
            internal Dictionary<string, ModulePatcher> _Modules = new Dictionary<string, ModulePatcher>();
            internal DNetPatcher _Patcher = null;
            public AssemblyPatcher(DNetPatcher Patcher, System.Reflection.Assembly Assembly)
            {
                _Patcher = Patcher;
                _OriginalAssembly = Assembly;

                System.Reflection.AssemblyName assemblyName = new System.Reflection.AssemblyName(GenerateAssemblyName());
                _AssemblyBuilder =
                    System.AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, System.Reflection.Emit.AssemblyBuilderAccess.Run);

                foreach (System.Reflection.Module module in Assembly.GetModules())
                {
                    System.Reflection.Module _WAR_module = module;
                    _Modules.Add(module.Name, new ModulePatcher(this, _WAR_module));
                }
            }
            public void PostCollection()
            {
                foreach (ModulePatcher patcher in _Modules.Values)
                {
                    patcher.PostCollection();
                }
            }
            public void Patch()
            {
                foreach (ModulePatcher module in _Modules.Values)
                {
                    module.Patch();
                }
            }

            public System.Reflection.Assembly GetPatchedAssembly()
            {
                return _AssemblyBuilder;
            }

            public System.Reflection.FieldInfo ResolveField(System.Reflection.FieldInfo Field)
            {
                return _Patcher.ResolveField(Field);
            }

            public System.Reflection.MethodBase ResolveMethod(System.Reflection.MethodBase Method)
            {
                return _Patcher.ResolveMethod(Method);
            }

            public Type ResolveType(Type Type)
            {
                return _Patcher.ResolveType(Type);
            }
            public System.Reflection.FieldInfo FindField(System.Reflection.FieldInfo Field)
            {
                if (_Modules.ContainsKey(Field.DeclaringType.Module.Name))
                {
                    return _Modules[Field.DeclaringType.Module.Name].FindField(Field);
                }
                return null;
            }
            public System.Reflection.MethodBase FindMethod(System.Reflection.MethodBase Method)
            {
                if (_Modules.ContainsKey(Method.DeclaringType.Module.Name))
                {
                    return _Modules[Method.DeclaringType.Module.Name].FindMethod(Method);
                }
                return null;
            }
            public Type FindType(Type Type)
            {
                if (_Modules.ContainsKey(Type.Module.Name))
                {
                    return _Modules[Type.Module.Name].FindType(Type);
                }
                return null;
            }
        }

        class ModulePatcher
        {
            internal System.Reflection.Module _OriginalModule;
            internal System.Reflection.Emit.ModuleBuilder _ModuleBuilder = null;
            internal Dictionary<string, TypePatcher> _Types = new Dictionary<string, TypePatcher>();
            internal AssemblyPatcher _Parent = null;

            public ModulePatcher(AssemblyPatcher AssemblyPatcher, System.Reflection.Module Module)
            {
                _Parent = AssemblyPatcher;
                _OriginalModule = Module;
                _ModuleBuilder = AssemblyPatcher._AssemblyBuilder.DefineDynamicModule(Module.Name);

                foreach (Type type in Module.GetTypes())
                {
                    Type _WAR_type = type;
                    if (type.IsNested) { continue; }
                    _Types.Add(type.Name, new TypePatcher(this, _WAR_type, null));
                }
            }
            public void PostCollection()
            {
                foreach (TypePatcher patcher in _Types.Values)
                {
                    patcher.PostCollection();
                }
            }

            public void Patch()
            {
                foreach (TypePatcher type in _Types.Values)
                {
                    type.Patch();
                }
            }

            public System.Reflection.FieldInfo FindField(System.Reflection.FieldInfo Field)
            {
                if (_Types.ContainsKey(Field.DeclaringType.Name))
                {
                    return _Types[Field.DeclaringType.Name].FindField(Field);
                }
                return null;
            }
            public System.Reflection.MethodBase FindMethod(System.Reflection.MethodBase Method)
            {
                if (_Types.ContainsKey(Method.DeclaringType.Name))
                {
                    return _Types[Method.DeclaringType.Name].FindMethod(Method);
                }
                return null;
            }
            public Type FindType(Type Type)
            {
                if (_Types.ContainsKey(Type.Name))
                {
                    return _Types[Type.Name]._TypeBuilder;
                }
                return null;
            }

            public System.Reflection.FieldInfo ResolveField(System.Reflection.FieldInfo Field)
            {
                return _Parent.ResolveField(Field);
            }
            public System.Reflection.MethodBase ResolveMethod(System.Reflection.MethodBase Method)
            {
                return _Parent.ResolveMethod(Method);
            }
            public Type ResolveType(Type Type)
            {
                return _Parent.ResolveType(Type);
            }
        }

        class MethodPatcher
        {
            internal List<Fact.Internal.Reflection.MSILOpCode> _Code = null;
            internal System.Reflection.Emit.MethodBuilder _MethodBuilder = null;
            internal TypePatcher _Parent = null;
            internal int _MagickToken = MagickInt();
            internal int _Timeout = -1;
            public MethodPatcher(TypePatcher TypePatcher, System.Reflection.MethodInfo MethodInfo)
            {
                _Parent = TypePatcher;
                
                if (MethodInfo.GetMethodBody() == null) { return; }

                List<Type> parameters = new List<Type>();
                foreach (System.Reflection.ParameterInfo parameterInfo in MethodInfo.GetParameters())
                { parameters.Add(ResolveType(parameterInfo.ParameterType)); }
                _MethodBuilder = TypePatcher._TypeBuilder.DefineMethod(MethodInfo.Name, MethodInfo.Attributes, MethodInfo.CallingConvention, MethodInfo.ReturnType, parameters.ToArray());
                foreach (object attribute in MethodInfo.GetCustomAttributes(false))
                {
                    System.Reflection.Emit.CustomAttributeBuilder customAttribute = RebuildAttribute(attribute);
                    if (customAttribute != null)
                    {
                        _MethodBuilder.SetCustomAttribute(customAttribute);
                    }
                    else if (_Timeout == -1)
                    {
                        _Timeout = GetTimeout(attribute);
                    }
                }
                {
                    _Code = Fact.Internal.Reflection.MSILReader.ReadMethod(MethodInfo);
                }
            }

            public void Patch()
            {
                if (_Code == null) { return; }
                for (int n = 0; n < _Code.Count; n++)
                {
                    if (_Code[n].Data is Type) { _Code[n].Data = ResolveType(_Code[n].Data as Type); }
                    if (_Code[n].Data is System.Reflection.MethodBase) 
                    {
                        _Code[n].Data = ResolveMethod(_Code[n].Data as System.Reflection.MethodBase);
                        _Code[n] = FactMethodPatcher.Patch(_Code[n]);
                    }
                    if (_Code[n].Data is System.Reflection.FieldInfo) { _Code[n].Data = ResolveField(_Code[n].Data as System.Reflection.FieldInfo); }
                    if (_Code[n].OpCode == System.Reflection.Emit.OpCodes.Ret)
                    {
                        if (_Timeout > 0)
                        {
                            _Code.Insert(n, TimeoutManager.TimeoutExitOpCode());
                            _Code.Insert(n, new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Ldc_I4, (Int32)_MagickToken));
                            n += 2;
                        }
                        else
                        {
                            _Code.Insert(n, TimeoutManager.MethodExitOpCode());
                            _Code.Insert(n, new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Ldc_I4, (Int32)_MagickToken));
                            n += 2;
                        }
                        continue;
                    }
                }
                if (_Timeout > 0)
                {
                    _Code.Insert(0, TimeoutManager.TimeoutEnterOpCode());
                    _Code.Insert(0, new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Ldc_I4, (Int32)_Timeout));                   
                    _Code.Insert(0, new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Ldc_I4, (Int32)_MagickToken));
                }
                else
                {
                    _Code.Insert(0, TimeoutManager.MethodEnterOpCode());
                    _Code.Insert(0, new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Ldc_I4, (Int32)_MagickToken));
                }
                Fact.Internal.Reflection.MSILWriter.WriteMethod(_MethodBuilder.GetILGenerator(), _Code);
            }

            public System.Reflection.FieldInfo ResolveField(System.Reflection.FieldInfo Field)
            {
                return _Parent.ResolveField(Field);
            }
            public System.Reflection.MethodBase ResolveMethod(System.Reflection.MethodBase Method)
            {
                return _Parent.ResolveMethod(Method);
            }
            public Type ResolveType(Type Type)
            {
                return _Parent.ResolveType(Type);
            }
        }

        class ConstructorPatcher
        {
            internal List<Fact.Internal.Reflection.MSILOpCode> _Code = null;
            internal System.Reflection.Emit.ConstructorBuilder _ConstructorBuilder = null;
            internal TypePatcher _Parent = null;
            internal int _MagickToken = MagickInt();
            public ConstructorPatcher(TypePatcher TypePatcher, System.Reflection.ConstructorInfo ConstructorInfo)
            {
                _Parent = TypePatcher;
                if (ConstructorInfo.GetMethodBody() == null) { return; }

                List<Type> parameters = new List<Type>();
                foreach (System.Reflection.ParameterInfo parameterInfo in ConstructorInfo.GetParameters())
                { parameters.Add(ResolveType(parameterInfo.ParameterType)); }
                _ConstructorBuilder = TypePatcher._TypeBuilder.DefineConstructor(ConstructorInfo.Attributes, ConstructorInfo.CallingConvention, parameters.ToArray());
                foreach (object attribute in ConstructorInfo.GetCustomAttributes(false))
                {
                    System.Reflection.Emit.CustomAttributeBuilder customAttribute = RebuildAttribute(attribute);
                    if (customAttribute != null)
                    {
                        _ConstructorBuilder.SetCustomAttribute(customAttribute);
                    }
                }
                {
                    _Code = Fact.Internal.Reflection.MSILReader.ReadMethod(ConstructorInfo);
                }
            }

            public void Patch()
            {
                if (_Code == null) { return; }
                for (int n = 0; n < _Code.Count; n++)
                {
                    if (_Code[n].Data is Type) { _Code[n].Data = ResolveType(_Code[n].Data as Type); }
                    if (_Code[n].Data is System.Reflection.MethodBase) { _Code[n].Data = ResolveMethod(_Code[n].Data as System.Reflection.MethodBase); }
                    if (_Code[n].Data is System.Reflection.FieldInfo) { _Code[n].Data = ResolveField(_Code[n].Data as System.Reflection.FieldInfo); }
                    if (_Code[n].OpCode == System.Reflection.Emit.OpCodes.Ret)
                    {
                        _Code.Insert(n, TimeoutManager.MethodExitOpCode());
                        _Code.Insert(n, new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Ldc_I4, (Int32)_MagickToken));
                        n+=2;
                        continue;
                    }
                }

                _Code.Insert(0, TimeoutManager.MethodEnterOpCode());
                _Code.Insert(0, new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Ldc_I4, (Int32)_MagickToken));

                Fact.Internal.Reflection.MSILWriter.WriteMethod(_ConstructorBuilder.GetILGenerator(), _Code);
            }

            public System.Reflection.FieldInfo ResolveField(System.Reflection.FieldInfo Field)
            {
                return _Parent.ResolveField(Field);
            }
            public System.Reflection.MethodBase ResolveMethod(System.Reflection.MethodBase Method)
            {
                return _Parent.ResolveMethod(Method);
            }
            public Type ResolveType(Type Type)
            {
                return _Parent.ResolveType(Type);
            }
        }

        class TypePatcher
        {
            internal Type _OriginalType = null;
            internal System.Reflection.Emit.TypeBuilder _TypeBuilder = null;
            internal Dictionary<string, List<MethodPatcher>> _Method = new Dictionary<string, List<MethodPatcher>>();
            internal Dictionary<string, System.Reflection.Emit.FieldBuilder> _Field = new Dictionary<string, System.Reflection.Emit.FieldBuilder>();
            internal Dictionary<string, TypePatcher> _NestedType = new Dictionary<string, TypePatcher>();

            internal ModulePatcher _Parent = null;
            List<ConstructorPatcher> _Constructor = new List<ConstructorPatcher>();
            public TypePatcher(ModulePatcher ModulePatcher, Type Type, TypePatcher Parent)
            {
                _Parent = ModulePatcher;
                System.Reflection.Emit.ModuleBuilder ModuleBuilder = ModulePatcher._ModuleBuilder;
                _OriginalType = Type;
                if (Parent == null)
                {
                    _TypeBuilder = ModuleBuilder.DefineType(Type.Name, Type.Attributes);
                }
                else
                {
                    _TypeBuilder = Parent._TypeBuilder.DefineNestedType(Type.Name, Type.Attributes);
                }
                foreach (object attribute in Type.GetCustomAttributes(false))
                {
                    System.Reflection.Emit.CustomAttributeBuilder customAttribute = RebuildAttribute(attribute);
                    if (customAttribute != null)
                    {
                        _TypeBuilder.SetCustomAttribute(customAttribute);
                    }
                }
                foreach (Type neasted in Type.GetNestedTypes())
                {
                    Type _WAR_Type = neasted;
                    _NestedType.Add(neasted.Name, new TypePatcher(ModulePatcher, _WAR_Type, this));
                }
            }
            public void PostCollection()
            {
                foreach (System.Reflection.ConstructorInfo constructor in _OriginalType.GetConstructors(_BindingFlags))
                {
                    System.Reflection.ConstructorInfo _WAR_constructor = constructor;
                    _Constructor.Add(new ConstructorPatcher(this, _WAR_constructor));
                }
                foreach (System.Reflection.MethodInfo method in _OriginalType.GetMethods(_BindingFlags))
                {
                    System.Reflection.MethodInfo _WAR_method = method;
                    if (!_Method.ContainsKey(method.Name)) { _Method.Add(method.Name, new List<MethodPatcher>()); }
                    _Method[method.Name].Add(new MethodPatcher(this, _WAR_method));
                }
                foreach (System.Reflection.FieldInfo field in _OriginalType.GetFields(_BindingFlags))
                {
                    System.Reflection.FieldInfo _WAR_field = field;
                    _Field.Add(_WAR_field.Name, _TypeBuilder.DefineField(_WAR_field.Name, ResolveType(_WAR_field.FieldType), field.GetRequiredCustomModifiers(), field.GetOptionalCustomModifiers(), _WAR_field.Attributes));
                }
                foreach (TypePatcher patcher in _NestedType.Values)
                {
                    patcher.PostCollection();
                }
            }

            public System.Reflection.FieldInfo FindField(System.Reflection.FieldInfo Field)
            {
                if (_Field.ContainsKey(Field.Name))
                {
                    return _Field[Field.Name];
                }
                return null;
            }

            public System.Reflection.FieldInfo ResolveField(System.Reflection.FieldInfo Field)
            {
                return _Parent.ResolveField(Field);
            }
            public System.Reflection.MethodBase ResolveMethod(System.Reflection.MethodBase Method)
            {
                return _Parent.ResolveMethod(Method);
            }

            public System.Reflection.MethodBase FindMethod(System.Reflection.MethodBase Method)
            {
                if (Method is System.Reflection.MethodInfo)
                {
                    System.Reflection.MethodInfo methodInfo = Method as System.Reflection.MethodInfo;
                    if (_Method.ContainsKey(Method.DeclaringType.Name))
                    {
                        foreach (MethodPatcher methodPatcher in _Method[Method.Name])
                        {
                            if (methodPatcher._MethodBuilder.ReturnType != methodInfo.ReturnType)
                                continue;
                            if (methodPatcher._MethodBuilder.GetParameters().Length != methodInfo.GetParameters().Length)
                                continue;
                            // Fix Me check parameters
                            return methodPatcher._MethodBuilder;
                        }
                    }
                }
                if (Method is System.Reflection.ConstructorInfo)
                {
                    System.Reflection.ConstructorInfo constructorInfo = Method as System.Reflection.ConstructorInfo;
                    foreach (ConstructorPatcher constructorPatcher in _Constructor)
                    {
                        if (constructorPatcher._ConstructorBuilder.GetParameters().Length != constructorInfo.GetParameters().Length)
                            continue;
                        // Fix Me check parameters
                        return constructorPatcher._ConstructorBuilder;
                    }
                }
                return null;
            }
            public Type ResolveType(Type Type)
            {
                return _Parent.ResolveType(Type);
            }

            public void Patch()
            {
                foreach (ConstructorPatcher constructor in _Constructor)
                {
                    constructor.Patch();
                }
                foreach (List<MethodPatcher> methods in _Method.Values)
                {
                    foreach (MethodPatcher method in methods)
                    {
                        method.Patch();
                    }
                }
            }
        }

        public static System.Reflection.Emit.CustomAttributeBuilder RebuildAttribute(object Attribute)
        {
            if (Attribute is Fact.Attribute.Test.Setup) 
            {
                Fact.Attribute.Test.Setup setup = Attribute as Fact.Attribute.Test.Setup;
                return
                    new System.Reflection.Emit.CustomAttributeBuilder(
                        typeof(Fact.Attribute.Test.Setup).GetConstructor(new Type[] { typeof(string) }),
                        new object[] { setup.Name });
            }
            else if (Attribute is Fact.Attribute.Test.Teardown)
            {
                Fact.Attribute.Test.Teardown teardown = Attribute as Fact.Attribute.Test.Teardown;
                return
                    new System.Reflection.Emit.CustomAttributeBuilder(
                        typeof(Fact.Attribute.Test.Teardown).GetConstructor(new Type[] { typeof(string) }),
                        new object[] { teardown.Name });
            }
            else if (Attribute is Fact.Attribute.Test.Test)
            {
                Fact.Attribute.Test.Test test = Attribute as Fact.Attribute.Test.Test;
                return
                    new System.Reflection.Emit.CustomAttributeBuilder(
                        typeof(Fact.Attribute.Test.Test).GetConstructor(new Type[] { typeof(string) }),
                        new object[] { test.Name });
            }
            else if (Attribute is Fact.Attribute.Test.Group)
            {
                Fact.Attribute.Test.Group group = Attribute as Fact.Attribute.Test.Group;
                return
                    new System.Reflection.Emit.CustomAttributeBuilder(
                        typeof(Fact.Attribute.Test.Group).GetConstructor(new Type[] { typeof(string) }),
                        new object[] { group.Name });
            }
            return null;
        }

        public static int GetTimeout(object Attribute)
        {
            if (Attribute is Fact.Attribute.Test.Timeout)
            {
                Fact.Attribute.Test.Timeout timeout = Attribute as Fact.Attribute.Test.Timeout;
                return timeout.Value;
            }
            return -1;
        }

        public DNetPatcher()
        {
        }
        public static string GenerateAssemblyName()
        {
            string uuid = "";
            do
            {
                uuid = Fact.Internal.Information.GenerateUUID();
                uuid = uuid.Replace("-", "_").Replace("{", "").Replace("}", "");
                uuid = "FactFakeAssembly_" + uuid;
            } while (_AssemblyUID.Contains(uuid));
            _AssemblyUID.Add(uuid);
            return uuid;
        }

        public Type ResolveType(Type Type)
        {
            try
            {
                if (_Assemblies.ContainsKey(Type.Module.Assembly.FullName))
                {
                    Type PatchedType = _Assemblies[Type.Module.Assembly.FullName].FindType(Type);
                    if (PatchedType != null) { return PatchedType; }
                }
            }
            catch { }
            return Type;
        }

        public System.Reflection.FieldInfo ResolveField(System.Reflection.FieldInfo Field)
        {
            try
            {
                if (_Assemblies.ContainsKey(Field.DeclaringType.Module.Assembly.FullName))
                {
                    System.Reflection.FieldInfo PatchedField = _Assemblies[Field.DeclaringType.Module.Assembly.FullName].FindField(Field);
                    if (PatchedField != null) { return PatchedField; }
                }
            }
            catch { }
            return Field;
        }


        public System.Reflection.MethodBase ResolveMethod(System.Reflection.MethodBase Method)
        {
            try
            {
                if (_Assemblies.ContainsKey(Method.DeclaringType.Module.Assembly.FullName))
                {
                    System.Reflection.MethodBase PatchedMethod = _Assemblies[Method.DeclaringType.Module.Assembly.FullName].FindMethod(Method);
                    if (PatchedMethod != null) { return PatchedMethod; }
                }
            }
            catch { }
            return Method;
        }

        Dictionary<string, AssemblyPatcher> _Assemblies = new Dictionary<string, AssemblyPatcher>();

        public void Add(System.Reflection.Assembly Assembly)
        {
            AssemblyPatcher assembly = new AssemblyPatcher(this, Assembly);
            _Assemblies.Add(Assembly.FullName, assembly);
        }

        public void PatchAllAssemblies()
        {
            foreach (AssemblyPatcher patcher in _Assemblies.Values)
            {
                patcher.Patch();
            }
        }

        public void PostCollectionAllAssemblies()
        {
            foreach (AssemblyPatcher patcher in _Assemblies.Values)
            {
                patcher.PostCollection();
            }
        }

        public System.Reflection.Assembly Patch(System.Reflection.Assembly Assembly)
        {
            Add(Assembly);
            try
            {
                PostCollectionAllAssemblies();
                PatchAllAssemblies();
            }
            catch (Exception e)
            {
                Fact.Log.FatalInternalError(e);
            }
            return _Assemblies[Assembly.FullName].GetPatchedAssembly();
        }
    }
}

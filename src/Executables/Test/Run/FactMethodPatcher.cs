﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace run
{
    public static class FactMethodPatcher
    {
        static public Fact.Internal.Reflection.MSILOpCode Patch(Fact.Internal.Reflection.MSILOpCode Call)
        {
            if (Call.Data is System.Reflection.MethodBase)
            {
                System.Reflection.MethodInfo hook = PatchMethod(Call.Data as System.Reflection.MethodBase);
                if (hook != null)
                {
                    return new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Call, hook);
                }
            }
            return Call;
        }

        public static Fact.Runtime.Process.Result FakeProcessRun(Fact.Runtime.Process thisptr, int Timeout, string Input, bool CloseInput)
        {
            int minvalue = TimeoutManager.RemainingTime();
            if (minvalue > Timeout) { return thisptr.Run(Timeout); }
            return thisptr.Run(minvalue, Input, CloseInput);
        }
        public static Fact.Runtime.Process.Result FakeProcessRun(Fact.Runtime.Process thisptr, int Timeout, string Input)
        {
            int minvalue = TimeoutManager.RemainingTime();
            if (minvalue > Timeout) { return thisptr.Run(Timeout); }
            return thisptr.Run(minvalue, Input);
        }
        public static Fact.Runtime.Process.Result FakeProcessRun(Fact.Runtime.Process thisptr, int Timeout)
        {
            int minvalue = TimeoutManager.RemainingTime();
            if (minvalue > Timeout) { return thisptr.Run(Timeout); }
            return thisptr.Run(minvalue);
        }
        public static Fact.Runtime.Process.Result FakeProcessRun(Fact.Runtime.Process thisptr, string Input, bool CloseInput)
        {
            int minvalue = TimeoutManager.RemainingTime();
            return thisptr.Run(minvalue, Input, CloseInput);
        }
        public static Fact.Runtime.Process.Result FakeProcessRun(Fact.Runtime.Process thisptr, string Input)
        {
            int minvalue = TimeoutManager.RemainingTime();
            return thisptr.Run(minvalue, Input);
        }
        public static Fact.Runtime.Process.Result FakeProcessRun(Fact.Runtime.Process thisptr)
        {
            int minvalue = TimeoutManager.RemainingTime();
            return thisptr.Run(minvalue);
        }

        static System.Reflection.MethodInfo PatchMethod(System.Reflection.MethodBase Call)
        {
            if (Call == null || Call.DeclaringType == null) { return null; }
            string method = "";
            method = Call.DeclaringType.FullName + "." + Call.Name;

            List<Type> parameters = new List<Type>();

            parameters.Add(Call.DeclaringType);
            foreach (System.Reflection.ParameterInfo parameter in Call.GetParameters())
            {
                parameters.Add(parameter.ParameterType);
            }

            switch (method)
            {
                case "Fact.Runtime.Process.Run": return null;
            }
            return null;
        }
    }
}

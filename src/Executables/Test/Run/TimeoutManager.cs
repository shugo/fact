﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace run
{
    public class TimeoutManager
    {
        static Dictionary<int, Thread> _Traces = new Dictionary<int, Thread>();
        static System.Diagnostics.Stopwatch _Stopwatch = new System.Diagnostics.Stopwatch();
        class Thread
        {
            List<Point> _Points = new List<Point>();
            Point _CurrentPoint = null;
            public void MethodEnter(System.Reflection.MethodBase Method)
            {
                if (_CurrentPoint == null) 
                {
                    _CurrentPoint = new Point() { _Method = Method, _EnterTime = _Stopwatch.Elapsed };
                    _Points.Add(_CurrentPoint);
                    return;
                }
                Point target = _CurrentPoint;
                while (target._CurrentChild != null) { target = target._CurrentChild; }
                target._Children.Add(new Point() { _Method = Method, _EnterTime = _Stopwatch.Elapsed });
                target._CurrentChild = target._Children[target._Children.Count - 1];
            }
            public void MethodExit()
            {
                if (_CurrentPoint == null) { _CurrentPoint = new Point(); }
                Point target = _CurrentPoint;
                while (target._CurrentChild != null) { target = target._CurrentChild; }
                target._ExitTime = _Stopwatch.Elapsed;
                if (target._Parent != null) { target._Parent._CurrentChild = null; }
                if (target == _CurrentPoint) { _CurrentPoint = null; }
            }
        }
        class Point
        {
            internal System.Reflection.MethodBase _Method;
            internal TimeSpan _EnterTime;
            internal TimeSpan _ExitTime;

            internal List<Point> _Children = new List<Point>();
            internal Point _CurrentChild = null;
            internal Point _Parent = null;


        }
        static TimeoutManager()
        {
            _Stopwatch.Start();
        }

        public static void Clear()
        {
            lock (_Traces)
            {
                _Traces.Clear();
                _Stopwatch.Reset();
            }
        }

        public static Fact.Internal.Reflection.MSILOpCode TimeoutEnterOpCode()
        {
            System.Reflection.MethodInfo info = typeof(TimeoutManager).GetMethod("TimeoutEnter");
            return new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Call, info);
        }
        public static Fact.Internal.Reflection.MSILOpCode TimeoutExitOpCode()
        {
            System.Reflection.MethodInfo info = typeof(TimeoutManager).GetMethod("TimeoutExit");
            return new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Call, info);
        }
        public static Fact.Internal.Reflection.MSILOpCode MethodEnterOpCode()
        {
            System.Reflection.MethodInfo info = typeof(TimeoutManager).GetMethod("MethodEnter");
            return new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Call, info);
        }
        public static Fact.Internal.Reflection.MSILOpCode MethodExitOpCode()
        {
            System.Reflection.MethodInfo info = typeof(TimeoutManager).GetMethod("MethodExit");
            return new Fact.Internal.Reflection.MSILOpCode(System.Reflection.Emit.OpCodes.Call, info);
        }

        public static void DisplayStackInfo()
        {
            System.Diagnostics.StackTrace StackTrace = new System.Diagnostics.StackTrace();
            StackTrace.GetFrame(2).GetMethod();
            Fact.Log.Info("**** Method: " + StackTrace.GetFrame(2).GetMethod().Name);
            Fact.Log.Info("**** Native Code Offset in Current VA: " + StackTrace.GetFrame(2).GetNativeOffset());
            Fact.Log.Info("**** Common Language Offset: " + StackTrace.GetFrame(2).GetILOffset());
            Fact.Log.Info("**** TimeStamp: " + DateTime.UtcNow.ToString());
            Fact.Log.Info("**** Fact Magick TimeStamp: " +  System.Convert.ToBase64String(Fact.Internal.Information.TimeCode(DateTime.UtcNow)));
        }

        public static void MethodPreEnter(System.Reflection.MethodBase Method)
        {
            int id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            lock (_Traces)
            {
                _Stopwatch.Stop();
                if (!_Traces.ContainsKey(id)) { _Traces.Add(id, new Thread()); }
                System.Diagnostics.StackTrace StackTrace = new System.Diagnostics.StackTrace();
                _Traces[id].MethodEnter(StackTrace.GetFrame(1).GetMethod());
                _Stopwatch.Start();
            }
        }

        public static void MethodPostExit(System.Reflection.MethodBase Method)
        {
            int id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            lock (_Traces)
            {
                _Stopwatch.Stop();
                if (!_Traces.ContainsKey(id)) { _Traces.Add(id, new Thread()); }
                System.Diagnostics.StackTrace StackTrace = new System.Diagnostics.StackTrace();
                _Traces[id].MethodEnter(StackTrace.GetFrame(1).GetMethod());
                _Stopwatch.Start();
            }
        }

        static Queue<int> _TimeoutMethod = new Queue<int>();
        static Queue<int> _TimeoutValue = new Queue<int>();
        static Queue<int> _TimeoutThread = new Queue<int>();
        static Queue<TimeSpan> _TimeoutTimestamp = new Queue<TimeSpan>();


        static Queue<int> _TraceMethod = new Queue<int>();
        static Queue<int> _TraceThread = new Queue<int>();
        static Queue<TimeSpan> _TraceTimestamp = new Queue<TimeSpan>();



        public static void TimeoutEnter(int Value, int Timeout)
        {
            int id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            lock (_Traces)
            {
                _Stopwatch.Stop();
                _TimeoutMethod.Enqueue(Value);
                _TimeoutValue.Enqueue(Timeout);
                _TimeoutTimestamp.Enqueue(_Stopwatch.Elapsed);
                _TraceMethod.Enqueue(Value);
                _TraceThread.Enqueue(id);
                _TraceTimestamp.Enqueue(_Stopwatch.Elapsed);

                _Stopwatch.Start();
            }
        }

        public static void TimeoutExit(int Value)
        {
            int id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            lock (_Traces)
            {
                _Stopwatch.Stop();
                int check = _TimeoutMethod.Dequeue();
                if (check != Value)
                {
                    throw new Exception("Timeout corruption: the function has exit a timeout without passing in check post condition");
                }
                int value = _TimeoutValue.Dequeue();
                TimeSpan time = _Stopwatch.Elapsed - _TimeoutTimestamp.Dequeue();
                if (time.TotalMilliseconds > value)
                {
                    throw new Fact.Assert.Assert() { Result = new Fact.Test.Result.Error("Timeout") };
                }
                _TraceTimestamp.Enqueue(_Stopwatch.Elapsed);
                _Stopwatch.Start();
            }
        }

        public static int RemainingTime()
        {
            int remaining = -1;
            int id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            lock (_Traces)
            {
                _Stopwatch.Stop();
                if (_TimeoutMethod.Count == 0) { return -1; }
                int value = _TimeoutValue.Dequeue();
                TimeSpan time = _Stopwatch.Elapsed - _TimeoutTimestamp.Dequeue();
                remaining = value - (int)time.TotalMilliseconds;
                _Stopwatch.Start();
            }
            return remaining;
        }

        public static void MethodEnter(int Value)
        {
            int id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            lock (_Traces)
            {
                _Stopwatch.Stop();
                _TraceMethod.Enqueue(Value);
                _TraceThread.Enqueue(id);
                _TraceTimestamp.Enqueue(_Stopwatch.Elapsed);
                _Stopwatch.Start();
            }
        }

        public static void MethodExit(int Value)
        {
            int id = System.Threading.Thread.CurrentThread.ManagedThreadId;
            lock (_Traces)
            {
                _Stopwatch.Stop();
                _TraceMethod.Enqueue(Value);
                _TraceThread.Enqueue(id);
                _TraceTimestamp.Enqueue(_Stopwatch.Elapsed);
                _Stopwatch.Start();
            }
        }
    }
}

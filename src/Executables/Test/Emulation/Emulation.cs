﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Emulation
{
    public class Emulation : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Emulation";
            }
        }


        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.ProgramName = "emulation";

            CommandLine.AddStringInput("program", "the program that will be emulated", ""); CommandLine.AddShortInput("p", "program");
            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");

            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

            string Program = CommandLine["program"].AsString();
            if (!System.IO.File.Exists(Program))
            {
                Fact.Log.Error("Program not found: '" + Program + "'");
                return 1;
            }

            if (true)
            {
                string file = System.IO.Path.GetFullPath(Program);
                System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFile(file);
                assembly.ModuleResolve += assembly_ModuleResolve;
                if (assembly.EntryPoint == null)
                {
                    Fact.Log.Error("Program '" + Program + "' has no entry point");
                    return 1;
                }
                return Fact.Emulation.Execute.Call<int>(assembly.EntryPoint, null, args);
            }
            else
            {
                Fact.Log.Error("This executable format is not supported");
            }

            return 0;
        }

        System.Reflection.Module assembly_ModuleResolve(object sender, ResolveEventArgs e)
        {
            Fact.Log.Debug("Dynamic module resolution");
            return null;
        }
    }
}

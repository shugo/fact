﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor
{
    public class MonitorPlugin : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Monitor";
            }
        }

         public override int Run(string[] args)
         {

             Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
             CommandLine.AddStringListInput("arguments", "The arguments of the executables", new List<string>()); CommandLine.AddShortInput("a", "arguments");
             CommandLine.AddStringInput("executable", "The executable that will be executed", ""); CommandLine.AddShortInput("e", "executable");
             CommandLine.AddBooleanInput("help", "Display this help message", false); CommandLine.AddShortInput("h", "help");

             CommandLine.AddStringInput("server", "Execute the program on a remote system", ""); CommandLine.AddShortInput("s", "server");
             CommandLine.AddStringInput("group", "Set the agent group used to execute the program (with --server)", "");
             CommandLine.AddStringInput("credential", "Use a specific credential to log in on the remote system", ""); CommandLine.AddShortInput("c", "credential");

             if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
             if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }

             string executable = CommandLine["executable"].AsString();
             List<string> arguments = CommandLine["arguments"].AsStringList();

             string argumentOneLine = "";
             foreach (string arg in arguments) { argumentOneLine += " " + arg + " ";}
             argumentOneLine = argumentOneLine.Trim();

             MainForm form = new MainForm();
             if (executable != "")
             {
                 Fact.Runtime.Process process = new Fact.Runtime.Process(executable, argumentOneLine);
                 form.AddProcess(process);
             }
             System.Windows.Forms.Application.Run(form);
             return 0;
         }
    }
}

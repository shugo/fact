﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor
{
    public partial class MainForm : Form
    {
        Process _Process = null;
        public MainForm()
        {
            InitializeComponent();
        }

        DataSource _Memory = new DataSource();

        public void AddProcess(Fact.Runtime.Process Process)
        {
            Viewers.ProcessWatcher processWatcher = new Viewers.ProcessWatcher();
            _Process = new LocalProcess(Process);
            processWatcher.Process = _Process;
            _Process.Start();
            tabView1.Add(processWatcher);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {


        }

        private void lineChart1_Load(object sender, EventArgs e)
        {

        }

        private void processWatcher1_Load(object sender, EventArgs e)
        {

        }
    }
}

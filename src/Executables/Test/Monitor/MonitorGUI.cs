﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor
{
    public partial class MonitorGUI : Form
    {
        public MonitorGUI()
        {
            InitializeComponent();
			DoubleBuffered = true;
			timer1.Interval = 100;
        }

        private void MonitorGUI_Load(object sender, EventArgs e)
        {
            diagram4.UnitType = Diagram.Type.Byte;
            diagram3.UnitType = Diagram.Type.Tick;
            diagram5.UnitType = Diagram.Type.Thread;
        }

        Fact.Runtime.Process.Result _Result;
		Fact.Runtime.Process _Process;
		long _MemLimit = 0;
        public void Attach(Fact.Runtime.Process.Result Result, Fact.Runtime.Process Process, long Memlimit)
        {
            _Result = Result;
			_Process = Process;
			_MemLimit = Memlimit;
        }

        public void UpdateResult()
        {
            if (_Result.Terminated) { this.Close(); }

			try
			{
                diagram4.Push(_Result.Statistic.MemoryUsed.FloatLast(), 0.0f);
				diagram3.Push(_Result.Statistic.UserProcessorTime.FloatLast(), _Result.Statistic.SystemProcessorTime.FloatLast());
				diagram5.Push(_Result.Statistic.ThreadCount.FloatLast() - _Result.Statistic.WaitingThreadCount.FloatLast(), _Result.Statistic.WaitingThreadCount.FloatLast());
			}
			catch (Exception e)
			{
				Fact.Log.Exception (e);
			}
       		try
			{
				if (_MemLimit > -1)
				{
					if ((long)_MemLimit < _Result.Statistic.MemoryUsed.Last())
					{
						Fact.Log.Error("Memory Limit : " + _MemLimit);
						Fact.Log.Error("Program will be killed");
						_Process.Kill();
					}
				}
			}
			catch 
			{
			}
		}

        private void diagram4_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();
            UpdateResult();
        }

        private void diagram4_Load_1(object sender, EventArgs e)
        {

        }

        private void diagram3_Load(object sender, EventArgs e)
        {

        }
    }
}

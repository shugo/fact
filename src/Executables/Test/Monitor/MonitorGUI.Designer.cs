﻿namespace Monitor
{
    partial class MonitorGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.diagram4 = new Monitor.Diagram();
            this.diagram3 = new Monitor.Diagram();
            this.diagram5 = new Monitor.Diagram();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // diagram4
            // 
            this.diagram4.Location = new System.Drawing.Point(12, 255);
            this.diagram4.Name = "diagram4";
            this.diagram4.Size = new System.Drawing.Size(593, 214);
            this.diagram4.TabIndex = 1;
            this.diagram4.UnitType = Monitor.Diagram.Type.None;
            this.diagram4.Load += new System.EventHandler(this.diagram4_Load_1);
            // 
            // diagram3
            // 
            this.diagram3.Location = new System.Drawing.Point(12, 26);
            this.diagram3.Name = "diagram3";
            this.diagram3.Size = new System.Drawing.Size(593, 223);
            this.diagram3.TabIndex = 0;
            this.diagram3.UnitType = Monitor.Diagram.Type.None;
            this.diagram3.Load += new System.EventHandler(this.diagram3_Load);
            // 
            // diagram5
            // 
            this.diagram5.Location = new System.Drawing.Point(12, 475);
            this.diagram5.Name = "diagram5";
            this.diagram5.Size = new System.Drawing.Size(593, 214);
            this.diagram5.TabIndex = 2;
            this.diagram5.UnitType = Monitor.Diagram.Type.None;
            // 
            // MonitorGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(617, 726);
            this.Controls.Add(this.diagram5);
            this.Controls.Add(this.diagram4);
            this.Controls.Add(this.diagram3);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MonitorGUI";
            this.Text = "MonitorGUI";
            this.Load += new System.EventHandler(this.MonitorGUI_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private Diagram diagram1;
        private Diagram diagram2;
        private Diagram diagram3;
        private Diagram diagram4;
        private Diagram diagram5;
    }
}
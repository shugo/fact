﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor
{
    public partial class Diagram : UserControl
    {
        float[] _Value = new float[200];
        float[] _SecondValue = new float[200];
        System.Drawing.Font _Font = new Font("arial", 12);
		int _Limit = -1;
        public enum Type
        {
            Byte,
            Thread,
            Tick,
            None
        }
        Type _UnitType = Type.None;
        public Type UnitType
        {
            get { return _UnitType; }
            set { _UnitType = value; }
        }
		long TickToMs = 0;
        public Diagram()
        {
            InitializeComponent();
            for (int n = 0; n < 200; n++)
            {
                _Value[n] = 0.0f;
                _SecondValue[n] = 0.0f;
            }
			try
			{
				TickToMs = Fact.Internal.Os.Generic.GetClockTickPerSecond();
			}
			catch
			{
			}
        }

        public void Push(float value, float secondValue)
        {
            for (int n = 0; n < 199; n++)
            {
                _Value[n] = _Value[n + 1];
                _SecondValue[n] = _SecondValue[n + 1];
            }
            _Value[199] = value;
            _SecondValue[199] = secondValue;
            this.Invalidate();
        }

        private void Diagram_Paint(object sender, PaintEventArgs e)
        {
			e.Graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
			e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            float max = 0.0f;
            for (int n = 0; n < 200; n++)
            {
                if ((_Value[n] + _SecondValue[n]) > max)
                    max = _Value[n] + _SecondValue[n];
            }
            if (max <= float.Epsilon)
            {
                max = float.Epsilon;
            }
            for (int n = 0; n < 199; n++)
            {
                e.Graphics.DrawLine(
                    System.Drawing.Pens.Blue, 
                    ((float)n / 200.0f) * Width,
                    Height - (int)((_Value[n] / max) * (Height - 1)),
                    ((float)(n + 1) / 200.0f) * Width,
                    Height - (int)((_Value[n + 1] / max) * (Height - 1)));
                if (_SecondValue[n] != 0.0f || _SecondValue[n + 1] != 0.0f)
                {
                    e.Graphics.DrawLine(
                        System.Drawing.Pens.Red,
                        ((float)n / 200.0f) * Width,
                        Height - (int)(((_SecondValue[n] + _Value[n]) / max) * (Height - 1)),
                        ((float)(n + 1) / 200.0f) * Width,
                        Height - (int)(((_SecondValue[n + 1] + _Value[n + 1]) / max) * (Height - 1)));
                }
            }
            float current = _Value[199] + _SecondValue[199];
            if (_UnitType == Type.Byte)
            {
                if (max > 1024.0f * 1024.0f)
                {
                    e.Graphics.DrawString((current / (1024.0f * 1024.0f)).ToString() + "/Mo", _Font, System.Drawing.Brushes.Black, 5.0f, 20.0f);
                }
                else if (max > 1024.0f)
                {
                    e.Graphics.DrawString((current / 1024.0f).ToString() + "/Ko", _Font, System.Drawing.Brushes.Black, 5.0f, 20.0f);
                }
                else
                {
                    e.Graphics.DrawString(current.ToString() + "/o", _Font, System.Drawing.Brushes.Black, 5.0f, 20.0f);
                }

            }
            else if (_UnitType == Type.Tick)
            {
				if (TickToMs != 0) 
				{
					current *= 1024.0f;
					e.Graphics.DrawString(current.ToString() + "/Ms", _Font, System.Drawing.Brushes.Black, 5.0f, 20.0f);
				}
				else
				{
                	e.Graphics.DrawString(current.ToString() + "/Ticks", _Font, System.Drawing.Brushes.Black, 5.0f, 20.0f);
				}
            }
            else if (_UnitType == Type.Thread)
            {
                e.Graphics.DrawString(current.ToString() + "/Threads", _Font, System.Drawing.Brushes.Black, 5.0f, 20.0f);
            }
            else
            {
                e.Graphics.DrawString(current.ToString(), _Font, System.Drawing.Brushes.Black, 5.0f, 20.0f);
            }
        }

        private void Diagram_Load(object sender, EventArgs e)
        {

        }
    }
}

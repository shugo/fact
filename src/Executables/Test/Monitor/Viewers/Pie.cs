﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor.Viewers
{
    public partial class Pie : UserControl
    {
        DataSource _DataSource = null;
        public DataSource DataSource
        {
            get { return _DataSource; }
            set { _DataSource = value; }
        }

        public Pie()
        {
            InitializeComponent();
        }

        private void Pie_Load(object sender, EventArgs e)
        {

        }

        private void Pie_Paint(object sender, PaintEventArgs e)
        {
            double total = 0.0;
            foreach (Data data in _DataSource.Last().Values)
            {
                total += data._Value;
            }
        }
    }
}

﻿namespace Monitor.Viewers
{
    partial class ProcessWatcher
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Monitor.DataSource dataSource1 = new Monitor.DataSource();
            Monitor.DataSource dataSource2 = new Monitor.DataSource();
            Monitor.DataSource dataSource3 = new Monitor.DataSource();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lineChart1 = new Monitor.Viewers.LineChart();
            this.lineChart2 = new Monitor.Viewers.LineChart();
            this.lineChart3 = new Monitor.Viewers.LineChart();
            this.toolbox1 = new Monitor.Viewers.Toolbox();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 42);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lineChart1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1000, 858);
            this.splitContainer1.SplitterDistance = 286;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lineChart2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.lineChart3);
            this.splitContainer2.Size = new System.Drawing.Size(1000, 568);
            this.splitContainer2.SplitterDistance = 285;
            this.splitContainer2.TabIndex = 0;
            // 
            // lineChart1
            // 
            this.lineChart1.BackColor = System.Drawing.Color.White;
            this.lineChart1.DataSource = dataSource1;
            this.lineChart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lineChart1.Location = new System.Drawing.Point(0, 0);
            this.lineChart1.Name = "lineChart1";
            this.lineChart1.Size = new System.Drawing.Size(1000, 286);
            this.lineChart1.TabIndex = 0;
            this.lineChart1.Title = "";
            this.lineChart1.Load += new System.EventHandler(this.lineChart1_Load);
            // 
            // lineChart2
            // 
            this.lineChart2.BackColor = System.Drawing.Color.White;
            this.lineChart2.DataSource = dataSource2;
            this.lineChart2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lineChart2.Location = new System.Drawing.Point(0, 0);
            this.lineChart2.Name = "lineChart2";
            this.lineChart2.Size = new System.Drawing.Size(1000, 285);
            this.lineChart2.TabIndex = 0;
            this.lineChart2.Title = "";
            this.lineChart2.Load += new System.EventHandler(this.lineChart2_Load);
            // 
            // lineChart3
            // 
            this.lineChart3.BackColor = System.Drawing.Color.White;
            this.lineChart3.DataSource = dataSource3;
            this.lineChart3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lineChart3.Location = new System.Drawing.Point(0, 0);
            this.lineChart3.Name = "lineChart3";
            this.lineChart3.Size = new System.Drawing.Size(1000, 279);
            this.lineChart3.TabIndex = 1;
            this.lineChart3.Title = "";
            this.lineChart3.Load += new System.EventHandler(this.lineChart3_Load);
            // 
            // toolbox1
            // 
            this.toolbox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolbox1.Location = new System.Drawing.Point(0, 0);
            this.toolbox1.Name = "toolbox1";
            this.toolbox1.Process = null;
            this.toolbox1.Size = new System.Drawing.Size(1000, 42);
            this.toolbox1.TabIndex = 2;
            this.toolbox1.Load += new System.EventHandler(this.toolbox1_Load);
            // 
            // ProcessWatcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolbox1);
            this.Name = "ProcessWatcher";
            this.Size = new System.Drawing.Size(1000, 900);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private LineChart lineChart1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private LineChart lineChart2;
        private LineChart lineChart3;
        private System.Windows.Forms.Timer timer1;
        private Toolbox toolbox1;
    }
}

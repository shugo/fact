﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor.Viewers
{
    public partial class Toolbox : UserControl
    {
        public Toolbox()
        {
            InitializeComponent();
        }

        Process _Process = null;
        public Process Process
        {
            get { return _Process; }
            set { _Process = value; }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }


        void PlayPause()
        {
            if (_Process == null) { return; }
            if (_Process.Playing)
            {
                _Process.Pause();
            }
            else
            {
                _Process.Continue();
            }
        }

        private void panel2_MouseEnter(object sender, EventArgs e) { panel2.BackColor = System.Drawing.Color.DarkGray; }
        private void panel2_MouseLeave(object sender, EventArgs e) { panel2.BackColor = BackColor; }
        private void panel2_MouseClick(object sender, MouseEventArgs e) { PlayPause(); panel2.Invalidate(); }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            if (_Process == null) { return; }
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            if (_Process.Playing)
            {
                e.Graphics.FillRectangle(palette.GetBrush(2), new Rectangle(16, 4, panel2.Width / 5, panel2.Height - 8));
                e.Graphics.FillRectangle(palette.GetBrush(2), new Rectangle(panel2.Width - (panel2.Width / 5) - 16, 4, panel2.Width / 5, panel2.Height - 8));
            }
            else
            {
                e.Graphics.FillPolygon(palette.GetBrush(2),
                    new Point[] { new Point(4, 4), new Point(panel2.Width - 4, 4 + (panel2.Height - 8) / 2), new Point(4, panel2.Height - 4) });
            }
        }

        private void Toolbox_Load(object sender, EventArgs e)
        {

        }
    }
}

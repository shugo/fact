﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor.Viewers
{
    static class palette
    {
        public static System.Drawing.Color GetColor(int Color)
        {
            switch(Color)
            {
                case 0: return System.Drawing.Color.FromArgb(0x1A, 0xBC, 0x9C);
                case 1: return System.Drawing.Color.FromArgb(0x34, 0x98, 0xDB);
                case 2: return System.Drawing.Color.FromArgb(0x34, 0x49, 0x5E);
                case 3: return System.Drawing.Color.FromArgb(0x2E, 0xCC, 0x71);
                case 4: return System.Drawing.Color.FromArgb(0x9B, 0x59, 0xB6);
            }
            return System.Drawing.Color.FromArgb(Color, Color, Color);
        }


        public static System.Drawing.Color GetGray(int Color)
        {
            switch(Color)
            {
                case 0: return System.Drawing.Color.FromArgb(0x2C, 0x3E, 0x50);
                case 1: return System.Drawing.Color.FromArgb(0x34, 0x49, 0x5E);
                case 2: return System.Drawing.Color.FromArgb(0x58, 0x6B, 0x7E);
                case 3: return System.Drawing.Color.FromArgb(0x7F, 0x92, 0x9E);
                case 4: return System.Drawing.Color.FromArgb(0xB0, 0xBA, 0xBE);
                case 5: return System.Drawing.Color.FromArgb(0xDA, 0xDE, 0xDE);


            }
            return System.Drawing.Color.FromArgb(Color, Color, Color);
        }

        public static System.Drawing.Brush GetBrush(int Color)
        {
            return new System.Drawing.SolidBrush(GetColor(Color));
        }

        public static System.Drawing.Brush GetGrayBrush(int Color)
        {
            return new System.Drawing.SolidBrush(GetGray(Color));
        }

        public static System.Drawing.Pen GetPen(int Color, int Width)
        {
            return new System.Drawing.Pen(GetColor(Color), Width);
        }

        public static System.Drawing.Pen GetGrayPen(int Color, int Width)
        {
            return new System.Drawing.Pen(GetGray(Color), Width);
        }
    }
}

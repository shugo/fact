﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor.Viewers
{
    public partial class LineChart : UserControl
    {
        string _Title = "";
        public string Title { get { return _Title; } set { _Title = value; } }
        DataSource _DataSource = new DataSource();
        public DataSource DataSource
        {
            get { return _DataSource; }
            set { _DataSource = value; }
        }
        System.Drawing.Font _Font;
        public LineChart()
        {
            InitializeComponent();
            _Font = new System.Drawing.Font("Arial", 15);
        }

        private void LineChart_Paint(object sender, PaintEventArgs e)
        {
            int height = this.Height - 60;
            int height_offset = 50;


            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            if (_DataSource == null) { return; }
            DateTime now = DateTime.Now;
            now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, (now.Millisecond / 50) * 50);
            double max = 0;

            Dictionary<string, List<double>> plots = new Dictionary<string, List<double>>();
            for (int n = 0; n < 300 + 1; n++)
            {
                Dictionary<string, Data> data = _DataSource.Get(now - new TimeSpan(0, 0, 0, 0, n * 50));
                foreach(KeyValuePair<string, Data> entry in data)
                {
                    if (!plots.ContainsKey(entry.Key)) { plots.Add(entry.Key, new List<double>()); }
                    plots[entry.Key].Add(entry.Value._Value);
                    if (entry.Value._Value > max) { max = entry.Value._Value; }
                }
                foreach (KeyValuePair<string, List<double>> entry in plots)
                {
                    while (entry.Value.Count <= n)
                    {
                        entry.Value.Add(0);
                    }
                }
            }


            float scaling = (float)(max + max / 50) / (float)height;
            float iscaling = (float)this.Height / (float)(max + max / 50);
            e.Graphics.DrawString(_Title, _Font, palette.GetGrayBrush(2), 10.0f, 15.0f);
            if (height < 120)
            {
            }
            else
            {
                for (int n = 0; n < 10; n++)
                {
                    e.Graphics.DrawLine(palette.GetGrayPen(5, 1), 0, height_offset + n * (height / 10.0f), this.Width, height_offset + n * (height / 10.0f));
                }
            }

                scaling = (float)System.Math.Pow(2.0, (double)((int)System.Math.Log((double)scaling, 2.0) + 1));
            if (scaling < float.Epsilon) { scaling = float.Epsilon; }

            double xstep = (double)this.Width / (300.0 + 1.0);
            double value = 0;
            int color = 0;
            foreach (KeyValuePair<string, List<double>> curve in plots)
            {
                List<System.Drawing.PointF> point = new List<PointF>();
                double x = 0;
                for (int n = 0; n < curve.Value.Count; n++)
                {
                    point.Add(new PointF(this.Width - (int)x, height_offset + height - (int)((float)curve.Value[n] / scaling)));
                    x += xstep;
                }
                e.Graphics.DrawCurve(palette.GetPen(color, 2), point.ToArray());
                color++;
            }
        }

        private void LineChart_Load(object sender, EventArgs e)
        {
          
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}

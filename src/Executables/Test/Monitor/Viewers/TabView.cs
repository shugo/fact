﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor.Viewers
{
    public partial class TabView : UserControl
    {
        System.Drawing.Font _Font;
        System.Drawing.Font _FontBig;


        public TabView()
        {
            InitializeComponent();
        }

        private void TabView_Load(object sender, EventArgs e)
        {
            _Font = new System.Drawing.Font("Arial", 12);
            _FontBig = new System.Drawing.Font("Arial", 23);

        }

        List<Control> _Controls = new List<Control>();

        public void Add(Control Control)
        {
            _Controls.Add(Control);
            Control.Parent = panel2;
            Control.Dock = DockStyle.Fill;
            if (_Controls.Count == 0)
            {
                Control.Show();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
        
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            int TabSize = 300;
            if (TabSize > panel1.Width - 50) { TabSize = panel1.Width - 50; }
            e.Graphics.Clear(palette.GetGray(1));
            lock (this)
            {
                int xpos = 0;
                for (int n = 0; n < _Controls.Count; n++)
                {
                    e.Graphics.FillRectangle(System.Drawing.Brushes.White, new Rectangle(xpos, 2, TabSize, panel1.Height));
                    e.Graphics.DrawString(_Controls[n].Name, _Font, palette.GetGrayBrush(1), new RectangleF(xpos + 5, 7, TabSize, panel1.Height));
                    xpos += TabSize + 2;
                }
                e.Graphics.FillRectangle(System.Drawing.Brushes.White, new Rectangle(xpos, 2, 30, panel1.Height));
                e.Graphics.DrawString("+", _FontBig, palette.GetGrayBrush(1), new RectangleF(xpos, -3, TabSize, panel1.Height));

            }
        }

        int mouseClickX = 0;
        int mouseClickY = 0;
        int mousePositionX = 0;
        int mousePositionY = 0;
        bool mouseClick = false;
        Control selectedTab = null;

        void Click()
        {
            int TabSize = 300;
            if (TabSize > panel1.Width - 50) { TabSize = panel1.Width - 50; }
            int xpos = 0;

            for (int n = 0; n < _Controls.Count; n++)
            {
                if (mouseClickX >= xpos && mouseClickX <= xpos + TabSize)
                {
                    selectedTab = _Controls[n];
                }
                xpos += TabSize + 2;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            bool oldStatus = mouseClick;
            mouseClick = true;
            mousePositionX = e.X;
            mousePositionY = e.Y;
            if (oldStatus != mouseClick)
            {
                mouseClickX = mousePositionX;
                mouseClickY = mousePositionY;
                Click();
            }
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {

        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            bool oldStatus = mouseClick;
            mouseClick = e.Clicks != 0;
            mousePositionX = e.X;
            mousePositionY = e.Y;
            if (oldStatus != mouseClick)
            {
                if (mouseClick)
                {
                    mouseClickX = mousePositionX;
                    mouseClickY = mousePositionY;
                    Click();
                }
            }
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {

        }
    }
}

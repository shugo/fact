﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor.Viewers
{
    public partial class ProcessWatcher : UserControl
    {
        Fact.Runtime.Process.Result _Result = null;
        public Fact.Runtime.Process.Result ProcessResult
        {
            get { return _Result; }
            set { _Result = value; }
        }

        Process _Process = null;
        public Process Process
        {
            get { return _Process; }
            set 
            {
                _Process = value;
                toolbox1.Process = value;
                if (_Process != null)
                {
                    lineChart1.DataSource = _Process.Memory;
                    lineChart2.DataSource = _Process.Threads;
                    lineChart3.DataSource = _Process.CPU;
                }
            }
        }

        public ProcessWatcher()
        {
            InitializeComponent();
            lineChart1.DataSource = null;
            lineChart1.Title = "Memory";
            lineChart2.DataSource = null;
            lineChart2.Title = "Threads";
            lineChart3.DataSource = null;
            lineChart3.Title = "CPU";
        }

        private void lineChart1_Load(object sender, EventArgs e)
        {

        }

        private void lineChart2_Load(object sender, EventArgs e)
        {

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void lineChart3_Load(object sender, EventArgs e)
        {

        }

        private void toolbox1_Load(object sender, EventArgs e)
        {

        }
    }
}

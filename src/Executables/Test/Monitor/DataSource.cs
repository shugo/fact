﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor
{
    public class DataSource
    {
        List<Data> _Data = new List<Data>();

        public void Feed(DateTime StartTimestamp, DateTime EndTimestamp, double Value, string Label)
        {
            Data data = new Data();
            data._Value = Value;
            data._Label = Label;
            data._EndTimestamp = EndTimestamp;
            data._StartTimestamp = StartTimestamp;
            lock (this)
            {
                _Data.Add(data);
            }
        }

        bool Closer(DateTime A, DateTime Old, DateTime New)
        {
            long intervalOld = A.Ticks - Old.Ticks;
            if (intervalOld < 0) { intervalOld = -intervalOld; }
            long intervalNew = A.Ticks - New.Ticks;
            if (intervalNew < 0) { intervalNew = -intervalNew; }
            if (intervalNew < intervalOld) { return true; }
            return false;
        }

        public Dictionary<string, Data> Get(DateTime Stamp)
        {

            Dictionary<string, Data> labels = new Dictionary<string, Data>();
            lock (this)
            {
                for (int n = 0; n < _Data.Count; n++)
                {
                    string label = _Data[n]._Label;
                    if (!labels.ContainsKey(label))
                    {
                        labels.Add(label, _Data[n]);
                    }
                    else
                    {
                        if (Closer(Stamp, labels[label]._EndTimestamp, _Data[n]._EndTimestamp))
                        {
                            labels[label] = _Data[n];
                        }
                        if (Closer(Stamp, labels[label]._StartTimestamp, _Data[n]._StartTimestamp))
                        {
                            labels[label] = _Data[n];
                        }
                    }
                }
            }
            return labels;
        }

        public void DropBefore(DateTime Value)
        {
            lock (this)
            {
                for (int n = 0; n < _Data.Count; n++)
                {
                    if (_Data[n]._StartTimestamp < Value)
                    {
                        _Data.RemoveAt(n); n--;
                    }
                }
            }
        }

        public Dictionary<string, Data> Last()
        {
            Dictionary<string, Data> labels = new Dictionary<string,Data>();
            lock (this)
            {
                for (int n = 0; n < _Data.Count; n++)
                {
                    if (labels.ContainsKey(_Data[n]._Label))
                    {
                        labels.Add(_Data[n]._Label, _Data[n]);
                    }
                    if (labels[_Data[n]._Label]._EndTimestamp.Ticks > _Data[n]._EndTimestamp.Ticks)
                    {
                        labels[_Data[n]._Label] = _Data[n];
                    }
                }
            }
            return labels;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor
{
    public class Data
    {
        internal DateTime _StartTimestamp;
        internal DateTime _EndTimestamp;
        internal double _Value;
        internal string _Label;
    }
}

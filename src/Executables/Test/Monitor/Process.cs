﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor
{
    public abstract class Process
    {
        public abstract void Start();
        public abstract void Pause();
        public abstract void Continue();
        public abstract void Stop();

        public abstract DataSource CPU { get; }
        public abstract DataSource Memory { get; }
        public abstract DataSource Threads { get; }


        public abstract bool Playing { get; }
    }

    public class RemoteProcess : Process
    {
        Fact.Common.Remote.Job _Job;

        public RemoteProcess(Fact.Common.Remote.Job Job)
        {
            _Job = Job;
        }

        public override void Start()
        {
            _Job.Publish();
        }

        public override void Pause()
        {
            // Not supported
        }

        public override void Continue()
        {
            // Not Supported
        }

        public override void Stop()
        {
            _Job.Delete();
        }

        void _Update()
        {
            while (!_Job.Finished && _Job.Valid)
            {
                System.Threading.Thread.Sleep(100);
            }
            _Playing = false;
        }

        volatile bool _Playing = false;
        public override bool Playing { get { return _Playing; } }


        public override DataSource CPU { get { return null; } }
        public override DataSource Memory { get { return null; } }
        public override DataSource Threads { get { return null; } }
    }

    public class LocalProcess : Process
    {
        Fact.Runtime.Process _Process = null;
        Fact.Runtime.Process.Result _Result = null;

        public LocalProcess(Fact.Runtime.Process Process)
        {
            _Memory = new DataSource();
            _Threads = new DataSource();
            _CPU = new DataSource();

            _Process = Process;
            _Process.Breakpoint += _Process_Breakpoint;
        }

        Fact.Runtime.Process.BreakpointEventArgs _LastBreakpoint = null;

        void _Process_Breakpoint(object sender, Fact.Runtime.Process.BreakpointEventArgs e)
        {
            _Playing = false;
            _LastBreakpoint = e;
        }

        public override void Start()
        {
            _Result = _Process.RunAsync(-1);
            _Playing = true;
            System.Threading.Thread Thread = new System.Threading.Thread(_Update);
            Thread.Start();
        }

        public override void Pause()
        {
            _Process.Break();
        }

        public override void Continue()
        {
            if (_LastBreakpoint != null)
            {
                _LastBreakpoint.Continue();
                _LastBreakpoint = null;
                _Playing = true;
            }
        }

        public override void Stop()
        {
            _Process.Kill();
        }

        DataSource _Memory;
        DataSource _Threads;
        DataSource _CPU;

        public override DataSource CPU { get { return _CPU; } }
        public override DataSource Memory { get { return _Memory; } }
        public override DataSource Threads { get { return _Threads; } }

        volatile bool _Playing = false;
        public override bool Playing { get { return _Playing; } }

        void _Update()
        {
            while (_Process.WaitForExit(50) == null)
            {
                if (_Result != null)
                {
                    DateTime now = DateTime.Now;
                    now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, (now.Millisecond / 50) * 50);

                    _Memory.Feed(now, now + new TimeSpan(0, 0, 0, 0, 50), (double)_Result.Statistic.MemoryUsed.Last(), "Memory");
                    _Threads.Feed(now, now + new TimeSpan(0, 0, 0, 0, 50), (double)_Result.Statistic.ThreadCount.Last(), "Threads");
                    _Threads.Feed(now, now + new TimeSpan(0, 0, 0, 0, 50), (double)_Result.Statistic.WaitingThreadCount.Last(), "Waiting Threads");
                    _CPU.Feed(now, now + new TimeSpan(0, 0, 0, 0, 50), (double)_Result.Statistic.SystemProcessorTime.Last(), "System Processor Time");
                    _CPU.Feed(now, now + new TimeSpan(0, 0, 0, 0, 50), (double)_Result.Statistic.UserProcessorTime.Last(), "User Processor Time");

                    _Memory.DropBefore(now - new TimeSpan(0, 0, 20));
                    _Threads.DropBefore(now - new TimeSpan(0, 0, 20));
                    _CPU.DropBefore(now - new TimeSpan(0, 0, 20));
                }
            }
            _Playing = false;
        }


    }
}

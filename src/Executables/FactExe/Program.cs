﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactExe
{
    class Program
    {
        class Couple<Type1, Type2>
        {
            Type1 _Left;
            Type2 _Right;

            public Couple(Type1 Left, Type2 Right)
            {
                _Left = Left;
                _Right = Right;
            }

            public Type1 Left { get { return _Left; } set { _Left = value; } }
            public Type2 Right { get { return _Right; } set { _Right = value; } }

        }

        static string FindSubDirectoryInDirectory(string Directory, string SubDiretory)
        {
            try
            {
                foreach (string directory in System.IO.Directory.GetDirectories(Directory))
                {
                   try
                   {
                        string name = System.IO.Path.GetFileName(directory);
                        if (name.ToLower() == SubDiretory.ToLower()) { return directory; }
                   }   catch { }
                }
            }
            catch { }
            return null;
        }

        static List<Couple<string, int>> getPotentialLib(string[] args)
        {
            List<Couple<string, int>> value = new List<Couple<string, int>>();

            string relativeDirectory = System.AppDomain.CurrentDomain.BaseDirectory;

            string fullPath = relativeDirectory;

            for (int n = 0; n < args.Length; n++)
            {
                fullPath += "/" + args[n];
                value.Add(new Couple<string, int>(fullPath + ".dll", n));
            }

            fullPath = ".";

            for (int n = 0; n < args.Length; n++)
            {
                fullPath += "/" + args[n];
                value.Add(new Couple<string, int>(fullPath + ".dll", n));
            }

            fullPath = relativeDirectory;
            for (int n = 0; n < args.Length; n++)
            {
                fullPath += "/" + args[n].ToLower();
                value.Add(new Couple<string, int>(fullPath + ".dll", n));
            }

            fullPath = ".";
            for (int n = 0; n < args.Length; n++)
            {
                fullPath += "/" + args[n].ToLower();
                value.Add(new Couple<string, int>(fullPath + ".dll", n));
            }

            try
            {
                string plugindir = relativeDirectory;
                for (int n = 0; n < args.Length; n++)
                {
                    if (plugindir == "") { break; }
                    if (!System.IO.Directory.Exists(plugindir)) { break; }
                    value.Add(new Couple<string, int>(plugindir + "/" + args[n].ToLower() + ".dll", n));
                    value.Add(new Couple<string, int>(plugindir + "/" + args[n] + ".dll", n));
                    plugindir = FindSubDirectoryInDirectory(relativeDirectory, args[n]);
                }
            }
            catch { }
            return value;
        }

        static List<string[]> instructions(string[] args)
        {
            int n = 0;
            // Skip the command for fact exe
            for (; n < args.Length; n++)
            {
                if (args[n] == "--help" || args[n] == "/help" || args[n] == "-h" ||
                    args[n] == "--nologo" || args[n] == "/nologo" ||
                    args[n] == "--version" || args[n] == "/version") { continue; }
                break;
            }
            List<string[]> result = new List<string[]>();
            List<string> sequence = new List<string>();
            for (; n < args.Length; n++)
            {
                if (args[n] == "->" || args[n] == "=>")
                {
                    if (sequence.Count > 0)
                    { result.Add(sequence.ToArray()); sequence.Clear(); }
                }
                else
                {
                    sequence.Add(args[n]);
                }
            }
            if (sequence.Count > 0)
            { result.Add(sequence.ToArray()); }
            return result;
        }

        static int Call(string[] args)
        {            
            // Load the plugins located in the executable directory
            List<Couple<string, int>> potentialLib = getPotentialLib(args);
            Fact.Plugin.Plugin plugin = null;
            string[] pluginArgs = null;
            foreach (Couple<string, int> lib in potentialLib)
            {
                plugin = Fact.Plugin.Plugin.LoadFromFile(lib.Left, args[lib.Right]);
                if (plugin != null)
                {
                    pluginArgs = new string[args.Length - lib.Right - 1];
                    for (int n = 0; n < pluginArgs.Length; n++)
                    {
                        pluginArgs[n] = args[lib.Right + n + 1];
                    }
                    break;
                }
            }

            if (plugin != null)
            {
                return plugin.Run(pluginArgs);
            }
            else
            {
                Fact.Log.Error("Impossible to load the specified plugin");
                return 1;
            }
        }

        static void ListPlugins(string Directory, string DisplayedDirectory, HashSet<string> Result)
        {
            foreach (string subdir in System.IO.Directory.GetDirectories(Directory))
            {
                string Name = System.IO.Path.GetFileName(subdir);
                ListPlugins(subdir, DisplayedDirectory + " " + Name.ToLower(), Result);
            }
            try
            {
                foreach (string file in System.IO.Directory.GetFiles(Directory))
                {
                    if (file.ToLower().EndsWith(".dll")) 
                    {
                        try
                        {
                            foreach(Fact.Plugin.Plugin plugin in Fact.Plugin.Plugin.LoadFromFile(file))
                            {
                                Result.Add(DisplayedDirectory + " " + plugin.Name.ToLower());
                            }
                        }
                        catch { }
                    }
                }
            }
            catch { }
        }

        static void DisplayVersion()
        {
            Console.WriteLine("Version Name: " + Fact.Internal.Information.FactVersionName());
            Console.WriteLine("Version Major: " + Fact.Internal.Information.FactVersion().Major.ToString());
            Console.WriteLine("Version Minor: " + Fact.Internal.Information.FactVersion().Minor.ToString());
            Console.WriteLine("Version Build: " + Fact.Internal.Information.FactVersion().Build.ToString());
            Console.WriteLine("Fact package version Major: " + Fact.Internal.Information.FactFileVersion().Major.ToString());
            Console.WriteLine("Fact package version Minor: " + Fact.Internal.Information.FactFileVersion().Minor.ToString());
        }

        static void DisplayHelp()
        {
            string relativeDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
            HashSet<string> InstalledPlugins = new HashSet<string>();
            Console.WriteLine("Usage: fact [--help] [--nologo] [--version] [plugin name] [plugin args...]");
            Console.WriteLine("Installed Plugins:");
            ListPlugins(relativeDirectory, "", InstalledPlugins);

            foreach(string plugin in InstalledPlugins)
            {
                Console.WriteLine(plugin);
            }
        }

        class patcher : Fact.Internal.Reflection.MSILPatcher
        {
            public patcher(System.Reflection.Assembly Assembly, System.Reflection.AssemblyName PatchedAssemblyName, System.Reflection.Emit.AssemblyBuilderAccess PatchedAssemblyAccess) :
                base(Assembly, PatchedAssemblyName, PatchedAssemblyAccess)
            {

            }
        }

        static int Main(string[] args)
        {
           // Parse the arguments.
           // Since we have to laund a plugin we can't use the option parser 
            bool help = false;
            bool nologo = false;
            bool version = false;
            for (int n = 0; n < args.Length; n++)
            {
                if (args[n] == "--help" || args[n] == "-h" || args[n] == "/help") { help = true; }
                else if (args[n] == "--nologo" || args[n] == "/nologo") { nologo = true; }
                else if (args[n] == "--version" || args[n] == "/version") { version = true; }
                else { break; }
            }
            if (!nologo)
            {
                Fact.Log.Terminal.DoubleLine();
#if !DEBUG
                Fact.Log.Terminal.CenterText("Fact " + Fact.Internal.Information.FactVersionName());
#else
			    Fact.Log.Terminal.CenterText ("Fact " + Fact.Internal.Information.FactVersionName() + " (Debug)");
#endif
                Fact.Log.Terminal.DoubleLine();
            }
            if (help)
            {
                DisplayHelp();
                return 0;
            }

            if (version)
            {
                DisplayVersion();
                return 0;
            }
            try
            {
#if !DEBUG
                Fact.Internal.Os.UserModeDriver.Driver.ProtectProcess(System.Diagnostics.Process.GetCurrentProcess().Id);
#endif
            }
            catch { }

            try
            {
			    if (Fact.Internal.Information.CurrentOperatingSystem () == Fact.Internal.Information.OperatingSystem.Unix) 
			    {
				    LinuxStaticSignalHook.HookSignals();
			    }
            }
            catch { }
            List<string[]> commands = instructions(args);
            int errcode = 0;
            foreach (string[] command in commands)
            {
                if (command.Length > 0)
                {
                    string text = command[0];
                    for (int n = 1; n < command.Length; n++)
                    { text += " " + command[n]; }
                    Fact.Log.Verbose(text);
#if !DEBUG
                    try
                    {
#endif
                        int code = Call(command);
                        if (code != 0)
                        { errcode = code; }
#if !DEBUG
                    }
                    catch (Exception e)
                    {
                        Fact.Log.Exception(e);
                    }
#endif
                }
            }
            try
            {
#if !DEBUG
                Fact.Internal.Os.UserModeDriver.Driver.UnprotectProcess(System.Diagnostics.Process.GetCurrentProcess().Id);
#endif
            }
            catch { }
            try
            {
                // If some remaining nobackground thread are working we want to exit anyway
                // Fact Exe has to guarenty that all the plugin are force killed after their execution
                // you can't relly on the Non background thread behavior.
                System.Environment.Exit(errcode);
            }
            catch { }
            return errcode;
        }
    }
}
